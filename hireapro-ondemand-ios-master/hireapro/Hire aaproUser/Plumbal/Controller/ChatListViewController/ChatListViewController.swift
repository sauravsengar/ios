//
//  ChatListViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 19/01/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class ChatListViewController: RootViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var Chat_TableView: UITableView!
    var themes:Themes=Themes()
    var ChatTextArray:NSMutableArray=NSMutableArray()
    var Chat_NameArray:NSMutableArray=NSMutableArray()
    let URL_Handler:URLhandler=URLhandler()
    var nameArray:NSMutableArray=NSMutableArray()
    var p_idArray:NSMutableArray=NSMutableArray()
    var job_idArray:NSMutableArray=NSMutableArray()
    var msgArray:NSMutableArray=NSMutableArray()
    var msg_timeArray:NSMutableArray=NSMutableArray()
    var imageArray:NSMutableArray=NSMutableArray()
    var tasker_idarray : NSMutableArray = NSMutableArray()
    var Category_idArray : NSMutableArray = NSMutableArray()
    var created_dateArray:NSMutableArray = NSMutableArray()
    var tasker_statusArray:NSMutableArray = NSMutableArray()




    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = themes.setLang("chat")
        ChatTextArray=["Hello How are you","Hope You have a nice experience","Doing Great huhhh","Hello derella","The plumbing is on"]
        Chat_NameArray=["Annand","Jim","Darry","Dickens","Henry"]
         let Nb=UINib(nibName: "ChatListTableViewCell", bundle: nil)
        Chat_TableView.registerNib(Nb, forCellReuseIdentifier: "ChatCell")
         Chat_TableView.estimatedRowHeight=120
        
        Chat_TableView.rowHeight = UITableViewAutomaticDimension
        Chat_TableView.separatorColor=UIColor.lightGrayColor()
        Chat_TableView.tableFooterView=UIView()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        titleLabel.text = themes.setLang("chat")
        ChatTextArray=["Hello How are you","Hope You have a nice experience","Doing Great huhhh","Hello derella","The plumbing is on"]
        Chat_NameArray=["Annand","Jim","Darry","Dickens","Henry"]
        let Nb=UINib(nibName: "ChatListTableViewCell", bundle: nil)
        Chat_TableView.registerNib(Nb, forCellReuseIdentifier: "ChatCell")
        Chat_TableView.estimatedRowHeight=120
        
        Chat_TableView.rowHeight = UITableViewAutomaticDimension
        Chat_TableView.separatorColor=UIColor.lightGrayColor()
        Chat_TableView.tableFooterView=UIView()
        self.showProgress()
        GetDetails()
        

    }
    
    func GetDetails()
    {
        
        
        nameArray=NSMutableArray()
        p_idArray=NSMutableArray()
        job_idArray=NSMutableArray()
        msgArray=NSMutableArray()
        msg_timeArray=NSMutableArray()
        imageArray=NSMutableArray()
        tasker_idarray = NSMutableArray()
        
        created_dateArray = NSMutableArray()
        tasker_statusArray = NSMutableArray()
   
        let param=["type":"1","userId":"\(themes.getUserID())"]

        
        URL_Handler.makeCall(constant.Show_ChatList, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

               // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    
                    if(Status == "1")
                    {
                        
                        let ChatList: NSMutableArray = (Dict.objectForKey("response")!.objectForKey("message") as? NSMutableArray)!
                        if(ChatList.count > 0 )
                        {
                            self.Chat_TableView.hidden=false
                         for  Dict in ChatList
                        {
                            
                            let image=self.themes.CheckNullValue(Dict.objectForKey("tasker_image"))!
                            let name=self.themes.CheckNullValue(Dict.objectForKey("tasker_name"))!
                            let p_id=self.themes.CheckNullValue(Dict.objectForKey("task_id"))!
                            let job_id=self.themes.CheckNullValue(Dict.objectForKey("booking_id"))!
                            let providerid=self.themes.CheckNullValue(Dict.objectForKey("tasker_id"))!
                            let category=self.themes.CheckNullValue(Dict.objectForKey("category"))!
                            
                            let created_date = self.themes.CheckNullValue(Dict.objectForKey("created"))!
                            let tasker_status = self.themes.CheckNullValue(Dict.objectForKey("user_status"))!
                            
                            
                            
                            self.nameArray.addObject(name)
                            self.p_idArray.addObject(p_id)
                            self.job_idArray.addObject(job_id)
                            //self.msgArray.addObject(msg)
                            //                            self.msg_timeArray.addObject(msg_time)
                            self.imageArray.addObject(image)
                            self.tasker_idarray.addObject(providerid)
                            self.Category_idArray.addObject(category)
                            self.created_dateArray.addObject(created_date)
                            self.tasker_statusArray.addObject(tasker_status)
                         }
                        self.Chat_TableView.reloadData()
                        }
                        else
                        {
                            self.Chat_TableView.hidden=true
                        }
                        
                     }
                    else
                    {
                        let Response:NSString = self.themes.CheckNullValue(Dict.objectForKey("response"))!
                        
                        self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)

                        
                    }
                    
                }
                else
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

                }
            }
            
            
        }

        
        
        
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     @IBAction func didClickOptions(sender: UIButton) {
        if(sender.tag == 0)
        {
            self.findHamburguerViewController()?.showMenuViewController()
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.p_idArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
                  let Cell = tableView.dequeueReusableCellWithIdentifier("ChatCell") as! ChatListTableViewCell

        Cell.Provider_image.sd_setImageWithURL(NSURL(string: "\(imageArray[indexPath.row])"), placeholderImage: UIImage(named: "PlaceHolderSmall"))

        //Cell.Provider_image.sd_setImageWithURL(NSURL(string: "\(imageArray[indexPath.row])"), completed: themes.block)
        Cell.Chat_Lbl.text="\(nameArray[indexPath.row])"
        Cell.Provider_image.layer.cornerRadius = Cell.Provider_image.frame.size.height / 2;
        Cell.Provider_image.layer.masksToBounds = true;
        Cell.Provider_image.layer.borderWidth = 0;
        Cell.Provider_image.contentMode = UIViewContentMode.ScaleAspectFill
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        Cell.Time_Lab.text = "\(job_idArray[indexPath.row])"
        Cell.catagory_labl.text="\(Category_idArray[indexPath.row])"
        Cell.created_date.text = "\(created_dateArray[indexPath.row])"
        
        if (tasker_statusArray.objectAtIndex(indexPath.row) as! String == "1")
        {
            Cell.border_view.backgroundColor = PlumberThemeColor
        }
        else{
            Cell.border_view.backgroundColor = UIColor.clearColor()
        }
        Cell.border_view.layer.cornerRadius = Cell.border_view.frame.size.width/2
        Cell.border_view.clipsToBounds=true
          return Cell
     }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Message_details.taskid = self.p_idArray[indexPath.row] as! NSString
        Message_details.providerid = self.tasker_idarray[indexPath.row] as! NSString
        Message_details.name = self.nameArray[indexPath.row] as! NSString
        Message_details.image = self.imageArray[indexPath.row] as! NSString
        
        
        
        
        
        
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
        
        self.navigationController?.pushViewController(secondViewController, animated: true)
        //        }
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
