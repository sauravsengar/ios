//
//  TransactionViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 14/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class TransactionViewController: RootViewController,UIWebViewDelegate {

    @IBOutlet var TransationWebView: UIWebView!
    var new:NSString=NSString()
    @IBOutlet var Webload_progress: UIProgressView!
    var constant:Constant=Constant()
    var themes:Themes=Themes()
    
    @IBOutlet var title_lbl: CustomLabelWhite!
   
    @IBOutlet var backbtn: UIButton!
    var theBool: Bool=Bool()
    var myTimer: NSTimer=NSTimer()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("transaction")
        
 
        
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)
        
       
        
        Webload_progress.tintColor=themes.ThemeColour()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        if(Transaction_Stat.StripeStatus == true)
        {
        let url = NSURL (string: "\(constant.AppbaseUrl)/mobile/wallet-recharge/payform?user_id=\(themes.getUserID())&total_amount=\(Transaction_Stat.total_Amt)");
         let requestObj = NSURLRequest(URL: url!);
        TransationWebView.loadRequest(requestObj);
        }
        else
        
        {
            
            let url = NSURL (string:Transaction_Stat.wallet_rechargeurl as String);
            let requestObj = NSURLRequest(URL: url!);
            TransationWebView.loadRequest(requestObj);

            
        }
        

        self.Webload_progress.progress = 0.0
         TransationWebView.delegate=self
        
        let transform:CGAffineTransform = CGAffineTransformMakeScale(1.0, 2.0);
        Webload_progress.transform = transform;
         // Do any additional setup after loading the view.
    }
    


    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let URL:NSString=(request.URL?.absoluteString)!
        
        
        if (Transaction_Stat.StripeStatus == true)
            
        {
    
        if(URL.containsString("/wallet-recharge/pay-cancel"))
        {
            themes.AlertView("\(Appname)", Message: themes.setLang("payment_is_cancelled"), ButtonTitle: kOk)
            self.navigationController?.popToRootViewControllerAnimated(true)

            
        }
        else if(URL.containsString("\(constant.BaseUrl)/mobile/payment/pay-completed"))
        {
            themes.AlertView("Message", Message: themes.setLang("payment_success"), ButtonTitle: kOk)
              self.navigationController?.popToRootViewControllerAnimated(true)

        }
            
      else if(URL.containsString("\(constant.BaseUrl)/mobile/mobile/mobile/failed"))
        {
            themes.AlertView("\(Appname)", Message:themes.setLang("payment_failed"), ButtonTitle: kOk)
            self.navigationController?.popToRootViewControllerAnimated(true)

        }
        }
        else{
            
                if (URL.containsString("mobile/mobile/paypalsucess"))
                {
                    print("webview Url|\(URL)")

                    themes.AlertView("Message", Message: themes.setLang("payment_success"), ButtonTitle: self.themes.setLang("ok"))
                    self.navigationController?.popToRootViewControllerAnimated(true)
                    
                }
                else if (URL.containsString("checkout/payment/paypal/cancel"))
                {
                    self.navigationController?.popToRootViewControllerAnimated(true)
                    
                }
                else if (URL.containsString("mobile/mobile/paypalsucess"))
                {
                    themes.AlertView("\(Appname)", Message: themes.setLang("payment_failed"), ButtonTitle: self.themes.setLang("ok"))
                    self.navigationController?.popToRootViewControllerAnimated(true)
                    
                    
                }
                
            
        }
 
          return true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
       funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
    funcToCallCalledWhenUIWebViewFinishesLoading()
        
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.theBool = false
        self.myTimer = NSTimer.scheduledTimerWithTimeInterval(0.01667, target: self, selector: #selector(TransactionViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.Webload_progress.progress >= 1 {
                self.Webload_progress.hidden = true
                self.myTimer.invalidate()
            } else {
                self.Webload_progress.progress += 0.1
            }
        } else {
            self.Webload_progress.progress += 0.05
            if self.Webload_progress.progress >= 0.95 {
                self.Webload_progress.progress = 0.95
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickoption(sender: UIButton) {
        
        if(sender.tag == 0)
        {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
