//
//  MyProfileViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/6/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
let IMAGE_HEIGHT = 273
class MyProfileViewController: RootViewController,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,SMSegmentViewDelegate,UITextViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource {
    var url_handler:URLhandler=URLhandler()
 //   var theme:Theme=Theme()
    var availabilityDict : NSMutableArray = NSMutableArray()
    var AvailableDaysArray :NSMutableArray = NSMutableArray()
    var nextPageStr:NSInteger!
    var segmentView: SMSegmentView!
    @IBOutlet var radius_lbl: UILabel!
    @IBOutlet var flatRate_lbl: UILabel!
    
    @IBOutlet var segment: CustomSegmentControl!
      var getCatagoryArr : NSArray = NSArray()
    var providerid:NSString!
    var min_amount:NSString!
    var hour_amount: NSString!
    var cat_type :NSString!
    var getjob_status:NSString!

    @IBOutlet var category_typeView: UIView!
    @IBOutlet var badgecoll: UICollectionView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var availabletable: UITableView!
    @IBOutlet weak var topView: SetColorView!
    @IBOutlet weak var segmentContainerView: UIView!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var totalContainer: UIView!
    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var reviewsTblView: UITableView!
    @IBOutlet weak var userInfoTopView: UIView!
    @IBOutlet weak var myProfileTblView: UITableView!
    @IBOutlet weak var userCatLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    var ProfileContentArray:NSMutableArray = [];
    var reviewsArray:NSMutableArray = [];
    var catagoryarrcount : NSMutableArray = [];
    @IBOutlet weak var lblMyProfile: UILabel!
    @IBOutlet var lblMail: UILabel!
    @IBOutlet var minrate: UILabel!
    

    @IBOutlet var hourlyrate: UILabel!
    @IBOutlet var lblRadius: UILabel!
    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var ratingCount: UILabel!
    @IBOutlet var hourLabel: UILabel!
    var badge:NSArray = NSArray()
    var namearray:NSArray =  NSArray()
    @IBAction func didclickoption(sender: AnyObject) {
         self.navigationController?.popViewControllerAnimated(true)
    }
    @IBOutlet weak var profileTopView: UIView!
    private var loading = false {
        didSet {
           
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        radius_lbl.text = themes.setLang("radius")
        flatRate_lbl.text = themes.setLang("flat_rate")
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        self.badgecoll.delegate = self
        self.badgecoll.dataSource =  self
        
        backbtn.addSubview(themes.Back_ImageView)
        lblMyProfile.text = themes.setLang("my_profile")
        if cat_type == "Flat Rate"{
            hourLabel.text = themes.setLang("flat_rate")
            minrate.text = "\(themes.getCurrencyCode())\(min_amount)"

        }else if cat_type == "Hourly Rate"{
            hourLabel.text = themes.setLang("hourly_rate")
            minrate.text = "\(themes.getCurrencyCode())\(hour_amount)"
        }
        
        //minrate.text = "\(themes.getCurrencyCode())\(hour_amount)"
        hourlyrate.text = "\(themes.getCurrencyCode())\(min_amount)"
        if hourlyrate.text == themes.getCurrencyCode(){
            hourlyrate.text = "\(themes.getCurrencyCode())0"
        }else if minrate.text == themes.getCurrencyCode(){
            minrate.text = "\(themes.getCurrencyCode())0"
        }
        segment.setTitle(themes.setLang("Details"), forSegmentAtIndex: 0)
        segment.setTitle(themes.setLang("Availability"), forSegmentAtIndex: 1)
        segment.selectedSegmentIndex=0
        segment.tintColor=themes.ThemeColour()
        segment.hidden = true
        
        segment.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 14.0)!, NSForegroundColorAttributeName: PlumberThemeColor], forState: .Normal)
              myProfileTblView.registerNib(UINib(nibName: "ProfileDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailTableIdentifier")
        myProfileTblView.estimatedRowHeight = 58
        myProfileTblView.rowHeight = UITableViewAutomaticDimension
        
        availabletable.registerNib(UINib(nibName:"AvailableDaysTableCell", bundle: nil), forCellReuseIdentifier: "availabledayscell")
        availabletable.estimatedRowHeight = 20
        availabletable.rowHeight = UITableViewAutomaticDimension

        availabletable.hidden = false

       // barButton.addTarget(self, action: #selector(MyProfileViewController.openmenu), forControlEvents: .TouchUpInside)
        reviewsTblView.registerNib(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTblIdentifier")
        reviewsTblView.estimatedRowHeight = 120
        reviewsTblView.rowHeight = UITableViewAutomaticDimension
        myProfileTblView.tableFooterView = UIView()
        availabletable.tableFooterView = UIView()
        reviewsTblView.tableFooterView = UIView()
        
        let nibName = UINib(nibName: "BadgeCollectionViewCell", bundle:nil)
       badgecoll.registerNib(nibName, forCellWithReuseIdentifier: "badge")

        blurBannerImg()
        loadSegmentControl()
        
    }
       override func viewWillAppear(animated: Bool) {
        reviewsTblView.hidden=true
        nextPageStr=1
        if(ProfileContentArray.count>0){
            ProfileContentArray.removeAllObjects()
            self.availabilityDict.removeAllObjects()
            self.AvailableDaysArray.removeAllObjects()
            self.reviewsArray.removeAllObjects()
            self.catagoryarrcount.removeAllObjects()
        }
  
        refreshNewLeads()
        showProgress()
        //loadProfileTblView()
        GetReviews()
          GetUserDetails()
    }
    func loadProfileTblView(){
        
        if badge.count == 0
        {
            print(self.profileTopView.frame);
            print(self.badgecoll.frame);
            self.profileTopView.frame = CGRectMake(self.profileTopView.frame.origin.x, self.profileTopView.frame.origin.y, self.profileTopView.frame.size.width, self.profileTopView.frame.size.height - self.badgecoll.frame.size.height)
            self.userInfoTopView.frame = CGRectMake(self.userInfoTopView.frame.origin.x, self.userInfoTopView.frame.origin.y, self.userInfoTopView.frame.size.width, self.profileTopView.frame.size.height)
            self.bannerImg.frame = CGRectMake(self.bannerImg.frame.origin.x, self.bannerImg.frame.origin.y, self.bannerImg.frame.size.width, self.profileTopView.frame.size.height+10)
            self.lblMail.frame = CGRectMake(self.lblMail.frame.origin.x, self.userNameLbl.frame.origin.y+self.userNameLbl.frame.size.height+5, self.lblMail.frame.size.width, self.lblMail.frame.size.height)
            self.lblMobile.frame = CGRectMake(self.lblMobile.frame.origin.x, self.lblMail.frame.origin.y+self.lblMail.frame.size.height+5, self.lblMobile.frame.size.width, self.lblMobile.frame.size.height)
            self.category_typeView.frame = CGRectMake(self.category_typeView.frame.origin.x, self.lblMobile.frame.origin.y+self.lblMobile.frame.size.height+5, self.category_typeView.frame.size.width, self.category_typeView.frame.size.height)
            self.segment.frame = CGRectMake(self.segment.frame.origin.x, self.bannerImg.frame.origin.y+self.bannerImg.frame.size.height, self.segment.frame.size.width, self.segment.frame.size.height)
            self.myProfileTblView.frame = CGRectMake(self.myProfileTblView.frame.origin.x, self.segment.frame.origin.y+self.segment.frame.size.height+5, self.myProfileTblView.frame.size.width,  self.myProfileTblView.frame.size.height)
            self.availabletable.frame = CGRectMake(self.availabletable.frame.origin.x, self.segment.frame.origin.y+self.segment.frame.size.height+5, self.availabletable.frame.size.width,  self.availabletable.frame.size.height)
            
            
            print(self.profileTopView.frame);
                   }
        else{
            
        }

        self.profileScrollView.frame=CGRectMake(self.profileScrollView.frame.origin.x, self.profileScrollView.frame.origin.y, self.profileScrollView.frame.size.width, self.profileScrollView.frame.size.height);
        
        self.myProfileTblView.frame = CGRectMake(self.myProfileTblView.frame.origin.x, self.segment.frame.origin.y+self.segment.frame.size.height, self.myProfileTblView.frame.size.width, self.myProfileTblView.frame.size.height+self.myProfileTblView.contentSize.height+120+CGFloat(getCatagoryArr.count*5))
        self.availabletable.frame = CGRectMake(self.availabletable.frame.origin.x, self.segment.frame.origin.y+self.segment.frame.size.height, self.availabletable.frame.size.width, self.availabletable.frame.size.height+self.availabletable.contentSize.height+CGFloat(AvailableDaysArray.count*56))
        
        self.profileScrollView.contentSize=CGSizeMake( self.profileScrollView.frame.width,self.myProfileTblView.frame.origin.y + self.myProfileTblView.frame.size.height)
        
        self.myProfileTblView.hidden = false
        self.availabletable.hidden = true
     
        
        
    }
    
    func blurBannerImg(){
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            bannerImg.backgroundColor = UIColor.clearColor()
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bannerImg.bounds
            blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            
            bannerImg.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } 
        else {
            bannerImg.backgroundColor = UIColor.blackColor()
        }
        userImg.layer.cornerRadius=userImg.frame.size.width/2
        //userImg.layer.borderWidth=0.75
        //userImg.layer.borderColor=PlumberThemeColor.CGColor
        userImg.layer.masksToBounds=true
    }
    func loadSegmentControl(){
        self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: segmentContainerView.frame.size.width, height: segmentContainerView.frame.size.height), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5, segmentProperties: [keySegmentTitleFont: UIFont.boldSystemFontOfSize(15.0), keySegmentOnSelectionColour: PlumberThemeColor,keySegmentOffSelectionTextColour:UIColor.darkGrayColor(), keySegmentOffSelectionColour: UIColor.whiteColor(), keyContentVerticalMargin: Float(10.0)])
        
        self.segmentView.delegate = self
        
        self.segmentView.layer.cornerRadius = 0.0
        self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).CGColor
        self.segmentView.layer.borderWidth = 1.0
        
        // Add segments
        self.segmentView.addSegmentWithTitle(themes.setLang("profiles")
            , onSelectionImage: UIImage(named: "MyProfileSelect"), offSelectionImage: UIImage(named: "MyProfileUnSelect"))
        self.segmentView.addSegmentWithTitle(themes.setLang("reviews")
            , onSelectionImage: UIImage(named: "ReviewsSelect"), offSelectionImage: UIImage(named: "ReviewsUnSelect"))
        
        
        // Set segment with index 0 as selected by default
        //segmentView.selectSegmentAtIndex(0)
        self.segmentView.selectSegmentAtIndex(0)
        segmentContainerView.addSubview(self.segmentView)
    }
    // SMSegment Delegate
    func segmentView(segmentView: SMSegmentView, didSelectSegmentAtIndex index: Int) {
        switch (index){
        case 0:
            swapViews(false)
            break
        case 1:
            swapViews(true)
            break
        
        default:
            break
        }
    }
    
    func swapViews(isReviews:Bool){
        
        let transition = CATransition()
       // transition.startProgress = 0;
        //transition.endProgress = 5.0;
        transition.type = "fade";
        //transition.subtype = "fromRight";
        transition.duration = 0.4;
        transition.repeatCount = 1;
        self.totalContainer.layer.addAnimation(transition, forKey: " ")
        if(isReviews==true){
            profileView.hidden=true
            reviewsView.hidden=false
            if self.reviewsArray.count == 0
            {
                themes.AlertView("\(Appname)", Message:themes.setLang("Provider_Reviews"), ButtonTitle: kOk)


                
            }
            
        }else{
            profileView.hidden=false
            reviewsView.hidden=true
            
            
            
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView==profileScrollView){
            let offset: CGFloat = scrollView.contentOffset.y
            let percentage: CGFloat = (offset / CGFloat(IMAGE_HEIGHT))
            let value: CGFloat = CGFloat(IMAGE_HEIGHT) * percentage
            //bannerImg.frame = CGRectMake(0, value, bannerImg.bounds.size.width, CGFloat(IMAGE_HEIGHT) - value)
            let alphaValue: CGFloat = 1 - fabs(percentage)
            userInfoTopView.alpha = alphaValue * alphaValue * alphaValue
            
        }
    }
    @IBAction func didClickEditProfile(sender: AnyObject) {
        let objEditProfVc = self.storyboard!.instantiateViewControllerWithIdentifier("EditProfileVCSID") as! EditProfileViewController
        self.navigationController!.pushViewController(objEditProfVc, animated: true)
    }
    @IBAction func didClickBackBtn(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
   
    @IBAction func didClickSegment(sender: AnyObject) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;
        
        if(segmentIndex == 0)
        {

            self.availabletable.hidden=true
            self.myProfileTblView.hidden=false
        }
        if(segmentIndex == 1)
        {
//self.bannerImg.frame = CGRectMake(self.bannerImg.frame.origin.x,self.userInfoTopView.frame.origin.y, self.bannerImg.frame.size.width, self.userInfoTopView.frame.size.height + 5)

            self.availabletable.hidden=false
            self.myProfileTblView.hidden=true
            
        }
    }

      func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {   let headerView = UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 20))

        if tableView .isEqual(availabletable)
        {
           
            let headerlable : UILabel = UILabel.init(frame:CGRectMake(headerView.frame.origin.x+10,10,headerView.frame.size.width-10,25))
            
            if (AvailableDaysArray.count > 0)
            {
                headerlable.text = themes.setLang("available_days")
            }
            headerlable.textColor = UIColor(red: 255/255.0, green: 70/255.0, blue: 63/255.0, alpha: 1)
            headerlable.font = UIFont.init(name: "Roboto-Regular", size:14.0)
        
            headerView.addSubview(headerlable)
    
        return headerView
        }
        else
        {
            return headerView
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView==myProfileTblView){
            
                  return ProfileContentArray.count
        }else if(tableView==reviewsTblView){
             return reviewsArray.count
        }
        else if (tableView ==  availabletable)
        {
            
            return AvailableDaysArray.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {

        let cell3:UITableViewCell
        
        if(tableView==myProfileTblView){
            
           
                
            
            let cell1:ProfileDetailTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProfileDetailTableIdentifier") as! ProfileDetailTableViewCell
            if ProfileContentArray.count > 0
            {
            cell1.loadProfileTableCell(ProfileContentArray .objectAtIndex(indexPath.row) as! ProfileContentRecord)
            }
            cell1.selectionStyle=UITableViewCellSelectionStyle.None
            cell3=cell1
            
        }
            else if (tableView == availabletable)
        {
           let avialCell:AvailableDaysTableCell = tableView.dequeueReusableCellWithIdentifier("availabledayscell") as! AvailableDaysTableCell
            
            if indexPath.row == 0
            {
             avialCell.Morning.hidden = false
                 avialCell.afternoon.hidden = false
                 avialCell.Evning.hidden = false
                avialCell.mrnbtn.hidden = true
                avialCell.afternbtn.hidden = true
                avialCell.evebtn.hidden = true
            }
            else
            {
                avialCell.Morning.hidden = true
                avialCell.afternoon.hidden = true
                avialCell.Evning.hidden = true
                avialCell.mrnbtn.hidden = false
                avialCell.afternbtn.hidden = false
                avialCell.evebtn.hidden = false
            }
            
            
            if AvailableDaysArray.count > 0
            {
        avialCell.loadProfileTableCell(self.AvailableDaysArray .objectAtIndex(indexPath.row) as! AvailableRecord)
            
            }
           avialCell.selectionStyle=UITableViewCellSelectionStyle.None
           
            cell3=avialCell
        

        }
        else{
            let cell:ReviewsTableViewCell = tableView.dequeueReusableCellWithIdentifier("ReviewsTblIdentifier") as! ReviewsTableViewCell
            
            if (reviewsArray.count > 0)
            {
                cell.loadReviewTableCell((reviewsArray .objectAtIndex(indexPath.row) as! ReviewRecords), currentView:MyProfileViewController() as UIViewController)

            }
            cell.selectionStyle=UITableViewCellSelectionStyle.None
           cell3=cell
        }
       
        return cell3
    }
   
   
//    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        if(tableView==myProfileTblView){
//        let cellSize:CGRect = cell.frame;
//            if(indexPath.row+1==ProfileContentArray.count){
//                var frame: CGRect = self.myProfileTblView.frame;
//                frame.size.height = cellSize.origin.y + cellSize.height+10;
//                self.myProfileTblView.frame = frame;
////                 self.profileScrollView.contentSize=CGSizeMake(self.profileScrollView.frame.size.width, self.myProfileTblView.frame.origin.y+self.myProfileTblView.frame.size.height)
//            }
//        }
//    }
    func GetUserDetails(){
     
      //  let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(providerid)"]
        // print(Param)
        
        url_handler.makeCall(constant.viewProfile, param: Param) {
            (responseObject, error) -> () in
        
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        let Dic:NSDictionary=responseObject!
                     
                            self.ProfileContentArray.removeAllObjects()
                            self.availabilityDict.removeAllObjects()
                            self.AvailableDaysArray.removeAllObjects()
                        self.catagoryarrcount.removeAllObjects()
                        
                        self.profileTopView.hidden=false
                        self.userNameLbl.text=themes.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("provider_name"))
                        let doubleval:Double = Double(themes.CheckNullValue(responseObject?.objectForKey("response")!.objectForKey("avg_review"))!)!
                        let doubleStr = String(format: "%.1f", doubleval)
                        self.ratingCount.text = doubleStr
                        self.lblMail.text = themes.CheckNullValue(responseObject?.objectForKey("response")!.objectForKey("email"))
                        
                        let code = themes.CheckNullValue(responseObject?.objectForKey("response")!.objectForKey("dial_code"))
                        let mob = themes.CheckNullValue(responseObject?.objectForKey("response")!.objectForKey("mobile_number"))
                        self.lblMobile.text = "\(code!) \(mob!)"
                        self.lblRadius.text = themes.CheckNullValue(responseObject?.objectForKey("response")!.objectForKey("radius"))
                        

                        
                        let Dict : NSDictionary = (responseObject?.objectForKey("response"))! as! NSDictionary
                         self.userImg.sd_setImageWithURL(NSURL(string:(Dict.objectForKey("image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                        
                      //  self.theme.saveUserImage(self.theme.CheckNullValue(Dict.objectForKey("image")!)!)
                        
                        let badgeArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("badges") as! NSArray
                        self.badge = badgeArray
                        
//

                      
                        self.bannerImg.sd_setImageWithURL(NSURL(string:(responseObject?.objectForKey("response")?.objectForKey("image"))as! String), placeholderImage: UIImage(named: "PlaceHolderBig"))
                        
                        if(responseObject?.objectForKey("response")?.objectForKey("details")!.count>0){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("details") as! NSArray
                            
                            self.availabilityDict = responseObject?.objectForKey("response")?.objectForKey("availability_days") as! NSMutableArray
                            for (_, element) in listArr.enumerate() {
                                
                                
                                
                                if themes.CheckNullValue(element.objectForKey("desc"))! == "" || themes.CheckNullValue(element.objectForKey("title")) == "Bio"
                                {
                                    
                                    print("remove bio field")
                                }
                                else{
                                    
                                    let result1 = themes.CheckNullValue(element.objectForKey("desc"))!.stringByReplacingOccurrencesOfString("\n", withString:",")
                                    
                                    let rec = ProfileContentRecord(userTitle: themes.CheckNullValue(element.objectForKey("title"))!, desc: result1)
                                    
                                    
                                    if themes.CheckNullValue(element.objectForKey("title"))! == "Category"
                                    {
                                        
                                        self.getCatagoryArr = result1.componentsSeparatedByString(",")
                                        
                                    }
                                    
                                    
                                    //
                                    //
                                    
                                    if  self.getjob_status == "hide"{
                                        
                                        if themes.CheckNullValue(element.objectForKey("engtitle"))! == "Email" || themes.CheckNullValue(element.objectForKey("engtitle"))! == "Mobile" || themes.CheckNullValue(element.objectForKey("engtitle"))! == "Address"  {
                                            self.lblMail.hidden = true
                                            self.lblMobile.hidden = true
                                            
                                        }else{
                                            self.ProfileContentArray .addObject(rec)
                                        }
                                    }else{
                                        self.lblMail.hidden = false
                                        self.lblMobile.hidden = false
                                        
                                        self.ProfileContentArray .addObject(rec)
                                    }

                                    
                                    
                                    
                                }
                                
                            }
                           
                            
                            
                            
                            let record  = AvailableRecord (dayrec: themes.setLang("days")
                                ,mornigrec:themes.setLang("morning")
                                ,Afterrec:themes.setLang("afternoon")
                                ,eveningrec:themes.setLang("evening"))
                            self.AvailableDaysArray.addObject(record)

                            
                            for (_, element) in self.availabilityDict.enumerate() {
                                let result1 = themes.CheckNullValue(element.objectForKey("day"))!
                                
let avaialbletime  : String = themes.CheckNullValue((element.objectForKey("hour"))!.objectForKey("morning"))!
let avaialbleAftertime  : String =  themes.CheckNullValue((element.objectForKey("hour"))!.objectForKey("afternoon"))!
let avaialbleevetime  : String = themes.CheckNullValue((element.objectForKey("hour"))!.objectForKey("evening"))!
      let record  = AvailableRecord (dayrec: result1,mornigrec: avaialbletime ,Afterrec: avaialbleAftertime,eveningrec: avaialbleevetime)
        self.AvailableDaysArray.addObject(record)
                               
                                
                                
                                                           }
                            
                           
                           
                        }else{
                            //self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionCenter, title: appNameJJ)
                        }
                        
                      
                         self.myProfileTblView.reloadData()
                        self.availabletable.reloadData()
                
                       self.badgecoll.reloadData()

                        self.segment.hidden=false
                        
                        self.loadProfileTblView()

                        
                       
                    }
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
                else
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
            
        }
    }
    
    func GetReviews(){
        
        
        let Param: Dictionary = ["user_id":"\(providerid)",
                                 "role":"tasker",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]
        // print(Param)
        
        url_handler.makeCall(constant.GetUserreviews, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.reviewsTblView.hidden=false
            self.reviewsTblView.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict:NSDictionary=responseObject!.objectForKey("data") as! NSDictionary
                    let status:NSString=themes.CheckNullValue(Dict.objectForKey("status") as? NSString)!
                    
                    if(status == "1")
                    {
                        if(Dict.objectForKey("response")?.objectForKey("reviews")!.count>0){
                            let  listArr:NSArray=Dict.objectForKey("response")?.objectForKey("reviews") as! NSArray
                            if(self.nextPageStr==1){
                                self.reviewsArray.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerate() {
                                let rec = ReviewRecords(name: themes.CheckNullValue(element.objectForKey("user_name"))!, time: themes.CheckNullValue(element.objectForKey("date"))!, desc: themes.CheckNullValue(element.objectForKey("comments"))!, rate:themes.CheckNullValue(element.objectForKey("rating"))!, img: themes.CheckNullValue(element.objectForKey("user_image"))!,ratting:themes.CheckNullValue(element.objectForKey("image"))!,jobid :themes.CheckNullValue(element.objectForKey("booking_id"))!)
                                
                                [self.reviewsArray .addObject(rec)]
                            }
                            self.reviewsTblView.reloadData()
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:themes.setLang("You are at the end. No reviews to load"), duration: 3, position: HRToastPositionDefault, title:"\(Appname)")
                            }
                        }
                    }
                    else
                    {
                        //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
                    }
                }
                else
                {
                    self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
                }
            }
            
        }
    }
    
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return badge.count
    }
     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        
            let badgecell = collectionView.dequeueReusableCellWithReuseIdentifier("badge", forIndexPath: indexPath) as! BadgeCollectionViewCell
        
        
        let valueFromArray = badge[indexPath.row]
        let imageFromValue = valueFromArray.objectForKey("image")
        

        let nameFromValue = valueFromArray.objectForKey("name")
        

            badgecell.badgeLbl.setTitle(nameFromValue as? String, forState: .Normal)
        badgecell.badgeLbl.tag = indexPath.row
        badgecell.badgeLbl.addTarget(self, action: #selector(MyProfileViewController.ShowAlert(_:)), forControlEvents: .TouchUpInside)
            
            badgecell.badgeimg.sd_setImageWithURL(NSURL.init(string: imageFromValue as! String), completed: themes.block)
            badgecell.badgeLbl.sizeToFit()
        
        
        
        badgecell.badgeimg.tag = indexPath.row
        
        badgecell.badgeimg.userInteractionEnabled = true
        let tap :UITapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(MyProfileViewController.handleimgTap(_:)))
        badgecell.badgeimg.addGestureRecognizer(tap)
        
            return badgecell
        
          }
    
    
    func  handleimgTap(recognizer:UITapGestureRecognizer)
    {
        // NSString *uid = testArray[recognizer.view.tag];
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title=themes.setLang("Badge_Des")
        AlertView.message = badge.objectAtIndex(recognizer.view!.tag).objectForKey("name") as? String
        AlertView.addButtonWithTitle(themes.setLang(themes.setLang("ok")))
        AlertView.show()

        
    }
    

    func ShowAlert (sender:UIButton)  {
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title=themes.setLang("Badge_Des")
        AlertView.message = badge.objectAtIndex(sender.tag).objectForKey("name") as! String
        AlertView.addButtonWithTitle(themes.setLang("ok"))
        AlertView.show()

    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if self.badge.count  > 3
        {
             return UIEdgeInsetsMake(0, 5, 0, 5)
            
        }else
        {
        var viewWidth = badgecoll.frame.size.width
        var totalCellWidth:CGFloat = 88 * CGFloat (badge.count)
        var totalSpacingWidth:CGFloat = 10 * (CGFloat(badge.count) - 1)
        var leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
        var rightInset = leftInset
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
    }
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    func refreshNewLeads(){
        
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        reviewsTblView.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
            self.GetReviews()
            
            }, loadingView: loadingView)
        reviewsTblView.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        reviewsTblView.dg_setPullToRefreshBackgroundColor(reviewsTblView.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
            GetReviews()
        }
    }
    func UITableView_Auto_Height()
    {
        if(self.myProfileTblView.contentSize.height > self.myProfileTblView.frame.height){
            
        }
    }
    deinit {
       // reviewsTblView.dg_removePullToRefresh()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
