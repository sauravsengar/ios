//
//  ProviderListViewController.swift
//  Plumbal
//
//  Created by CASPERON on 20/07/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class ProviderListViewController: RootViewController,UITableViewDataSource,UITableViewDelegate,PopupFilterViewControllerDelegate {

    @IBOutlet var provider_List: UITableView!
    
    @IBOutlet var btnBack: UIButton!
    
    var min : String = String()
    var max : String = String()
    var dbfileobj: DBFile!
    var dataArray : NSMutableArray!
    var appDelegate : AppDelegate!
    var  managedObjectContext :NSManagedObjectContext!
    var   dictRecords :NSMutableDictionary!
    @IBOutlet weak var lblFilter: UIButton!
    @IBOutlet weak var lbl_provider: UILabel!

    var ProviderListArray:NSMutableArray=NSMutableArray()
    var taskerid:NSString=NSString()
    let themes:Themes=Themes()
    var URL_handler:URLhandler=URLhandler()
//    let activityIndicatorView = NVActivityIndicatorView(frame: CGRectMake(0, 0, 0, 0),
//                                                        type: .BallSpinFadeLoader)
//    var AlertView:JTAlertView=JTAlertView()
    
    override func viewDidLoad() {
        
       // self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        provider_List.tableFooterView = UIView.init(frame: CGRectZero)
lbl_provider.text = themes.setLang("providers")
        lblFilter.setTitle(themes.setLang("filter"), forState: UIControlState.Normal)
        lblFilter.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lblFilter.titleLabel?.numberOfLines = 2

        dbfileobj = DBFile()
        appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.managedObjectContext = appDelegate.managedObjectContext
        dictRecords = NSMutableDictionary()
        dictRecords["providerid"] = ""

        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        btnBack.addSubview(themes.Back_ImageView)
        
        ProviderListArray = Schedule_Data.ProviderListIdArray
        super.viewDidLoad()
        let nibName = UINib(nibName: "ProviderListCell", bundle:nil)
        self.provider_List.registerNib(nibName, forCellReuseIdentifier: "ProviderListCell")
        provider_List.estimatedRowHeight = 140
        provider_List.rowHeight = 140
        provider_List.separatorColor=UIColor.grayColor()
        // Do any additional setup after loading the view.
        if Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex(0) as! String == "Flat Rate"{
        
        
        let numArr :NSArray = NSArray(array:  Schedule_Data.ProviderListFlatamountArray)
        
        
        
        max =  numArr.valueForKeyPath("@max.self")! as! String
        min = numArr.valueForKeyPath( "@min.self")! as! String
        
        }
        else{
            let numArr :NSArray = NSArray(array:  Schedule_Data.ProviderListHouramountArray)
            
            
            
            max =  numArr.valueForKeyPath("@max.self")! as! String
            min = numArr.valueForKeyPath( "@min.self")! as! String
            

        }
      
    }
    //TableViewDelegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func ShowFilter(sender: AnyObject) {
        self.displayViewController(.BottomBottom)
        
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let popupFilter : PopupFilterViewController = PopupFilterViewController(nibName:"PopupFilterViewController",bundle: nil)
        popupFilter.delegate = self;
        popupFilter.ctg_type = Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex(0) as! String
        if Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex(0) as! String == "Flat Rate"{

   popupFilter.minimumpriceval = Schedule_Data.ProviderListFlatamountArray.firstObject as! String
        popupFilter.maximumpriceval =  Schedule_Data.ProviderListFlatamountArray.lastObject as! String
        }
        else{
            popupFilter.minimumpriceval = Schedule_Data.ProviderListHouramountArray.firstObject as! String
            popupFilter.maximumpriceval =  Schedule_Data.ProviderListHouramountArray.lastObject as! String
            
        }
        
        self.presentpopupViewController(popupFilter, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func pressedCancel(sender: PopupFilterViewController) {
         self.dismissPopupViewController(.BottomBottom)
        
    }
    func  passParametres(RatingValue: CGFloat, Priceval: NSString ,MaxPrice : NSString) {
        
        
            
        
            constant.showProgress()
            
            
      

            let param=["user_id":"\(themes.getUserID())","address_name":"\( Schedule_Data.RquiredAddressid)","pickup_date":"\( Schedule_Data.PickupDate )","pickup_time":"\( Schedule_Data.pickupTime)","instruction":"\( Schedule_Data.GetScheduleIstr)","code":"\( Schedule_Data.GetCoupenText)","category":"\(Home_Data.Category_id)","service":"\(Category_Data.CategoryID)","lat": Schedule_Data.getLatitude,"long": Schedule_Data.getLongtitude,"rating":RatingValue,"minrate":Priceval,"maxrate":MaxPrice]
            
            URL_handler.makeCall(constant.Book_It, param: param, completionHandler: { (responseObject, error) -> () in
                constant.DismissProgress()
                
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

                 //   self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                }
                    
                else
                {
                    if(responseObject != nil)
                    {
                        
                        let dict:NSDictionary=responseObject!
                        
                        let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        if(responseObject != nil)
                        {
                            
                          
                            
                                //let response:NSDictionary=dict.objectForKey("response") as! NSDictionary
                            
                          
                                if(Schedule_Data.ProviderListIdArray.count != 0)
                                {
                                    Schedule_Data.ProviderListIdArray.removeAllObjects()
                                }
                                if(Schedule_Data.ProviderListNameArray.count != 0)
                                {
                                    Schedule_Data.ProviderListNameArray.removeAllObjects()
                                }
                                if(Schedule_Data.ProviderListImageArray.count != 0)
                                {
                                    Schedule_Data.ProviderListImageArray.removeAllObjects()
                                }
                                if(Schedule_Data.ProviderListAvailableArray.count != 0)
                                {
                                    Schedule_Data.ProviderListAvailableArray.removeAllObjects()
                                }
                                if(Schedule_Data.ProviderListCompanyArray.count != 0)
                                {
                                    Schedule_Data.ProviderListCompanyArray.removeAllObjects()
                                }
                                if(Schedule_Data.ProviderListRatingArray.count != 0)
                                {
                                    Schedule_Data.ProviderListRatingArray.removeAllObjects()
                                }
                            
                            if(Schedule_Data.ProviderLisreviewsArray.count != 0)
                            {
                                Schedule_Data.ProviderLisreviewsArray.removeAllObjects()
                            }
                            if(Schedule_Data.ProviderListCategoryTypeArray.count != 0)
                            {
                                Schedule_Data.ProviderListCategoryTypeArray.removeAllObjects()
                            }
                            
                            if(Schedule_Data.ProviderdistanceArray.count != 0)
                            {
                                Schedule_Data.ProviderdistanceArray.removeAllObjects()
                            }
                            
                            
                            if(Schedule_Data.ProviderListMinamountArray.count != 0)
                            {
                                Schedule_Data.ProviderListMinamountArray.removeAllObjects()
                            }
                            if(Schedule_Data.ProviderListHouramountArray.count != 0)
                            {
                                Schedule_Data.ProviderListHouramountArray.removeAllObjects()
                            }
                            if(Schedule_Data.ProviderListFlatamountArray.count != 0)
                            {
                                Schedule_Data.ProviderListFlatamountArray.removeAllObjects()
                            }
                            if(Schedule_Data.ProviderListBadgeArray.count != 0)
                            {
                                Schedule_Data.ProviderListBadgeArray.removeAllObjects()
                            }


                            if(Status == "1")
                            {
                                let responseArray:NSMutableArray=dict.objectForKey("response") as! NSMutableArray
                                if(responseArray.count != 0)
                                {
                                    let taskID:NSString=self.themes.CheckNullValue(dict.objectForKey("task_id"))!
                                    Schedule_Data.TaskID="\(taskID)"

                                    for Dictionary in responseArray
                                    {
                                        let job_id:NSString=Dictionary.objectForKey("taskerid") as! NSString
                                        Schedule_Data.ProviderListIdArray.addObject(job_id)
                                        let Name:NSString=Dictionary.objectForKey("name") as! NSString
                                        Schedule_Data.ProviderListNameArray.addObject(Name)
                                        let service_icon:NSString=Dictionary.objectForKey("image_url") as! NSString
                                        Schedule_Data.ProviderListImageArray.addObject(service_icon)
                                        let available:NSString=Dictionary.objectForKey("availability") as! NSString
                                        Schedule_Data.ProviderListAvailableArray.addObject(available)
                                        let company:NSString=Dictionary.objectForKey("company") as! NSString
                                        Schedule_Data.ProviderListCompanyArray.addObject(company)
                                        let rating=self.themes.convertFloatToString(Dictionary.objectForKey("rating") as! Float)
                                        Schedule_Data.ProviderListRatingArray.addObject(rating)
                                        let reviews=self.themes.CheckNullValue(Dictionary.objectForKey("reviews"))! 
                                        Schedule_Data.ProviderLisreviewsArray.addObject(reviews)
                                        let dist=Dictionary.objectForKey("distance_mile") as! String
                                        Schedule_Data.ProviderdistanceArray.addObject(dist)
                                        let hour_amount=self.themes.CheckNullValue(Dictionary.objectForKey("hourly_amount"))!
                                        Schedule_Data.ProviderListHouramountArray.addObject(hour_amount)
                                        let flat_amount=self.themes.CheckNullValue(Dictionary.objectForKey("flat_amount"))!
                                        Schedule_Data.ProviderListFlatamountArray.addObject(flat_amount)
                                        let category_type=self.themes.CheckNullValue(Dictionary.objectForKey("category_type"))!
                                        Schedule_Data.ProviderListCategoryTypeArray.addObject(category_type)

                                        let min_amount=self.themes.CheckNullValue(Dictionary.objectForKey("min_amount"))!
                                        Schedule_Data.ProviderListMinamountArray.addObject(min_amount)
                                        
                                        let badge=Dictionary.objectForKey("badges") as! NSArray
                                        Schedule_Data.ProviderListBadgeArray.addObject(badge)
                                        
                                        
                                    }
                                  
                                    
                                
                                    
                                }
                                
                            }
                            else
                            {
                                let response:NSString=dict.objectForKey("response") as! NSString
                                self.themes.AlertView("\(Appname) ", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                            }
                            
                              self.provider_List.reloadData()
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        
                    }
                }
                
            })
        

        
    
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProviderListArray.count==0{
            lblFilter.hidden = true
            
            
        }else{
            lblFilter.hidden = false
        }
        return ProviderListArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell:ProviderListCell = tableView.dequeueReusableCellWithIdentifier("ProviderListCell") as! ProviderListCell
       
        Cell.selectionStyle = .None
        
        let strRating = "\(Schedule_Data.ProviderListRatingArray.objectAtIndex(indexPath.row))"
        let doubleval:Double = Double("\(strRating)")!
        let doubleStr = String(format: "%.1f", doubleval)
        let n = NSNumberFormatter().numberFromString(doubleStr)
        
       if n != nil {

        Cell.providerRating.emptySelectedImage = UIImage(named: "Star")
        Cell.providerRating.fullSelectedImage = UIImage(named: "StarSelected")
        Cell.providerRating.contentMode = UIViewContentMode.ScaleAspectFill
        Cell.providerRating.maxRating = 5
        Cell.providerRating.minRating = 1
        Cell.providerRating.rating = CGFloat(n!)
        Cell.providerRating.editable = false;
        Cell.providerRating.halfRatings = false;
        Cell.providerRating.floatRatings = false;
        }
        Cell.backView.layer.cornerRadius = 5
        Cell.lblProviderName.text=" \(Schedule_Data.ProviderListNameArray.objectAtIndex(indexPath.row))"
        if Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex(indexPath.row) as! String == "Flat Rate"{
            Cell.lblPerHour.text = "Flat Rate"
            Cell.perhouramount.text = "\(themes.getCurrencyCode())\(Schedule_Data.ProviderListFlatamountArray.objectAtIndex(indexPath.row))"
//            Cell.lblPerHour.frame = CGRectMake(92, 52, 80, 21)
//            Cell.perhouramount.frame = CGRectMake(175, 52, 80, 21)
//            Cell.providerRating.frame = CGRectMake(92, 33, 80, 21)
            
        }else if Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex(indexPath.row) as! String == "Hourly Rate"{
            Cell.lblPerHour.text = "Per Hour"
            Cell.perhouramount.text = "\(themes.getCurrencyCode())\(Schedule_Data.ProviderListHouramountArray.objectAtIndex(indexPath.row))"
        }
        
        Cell.lblDistance.text = "\(Schedule_Data.ProviderdistanceArray.objectAtIndex(indexPath.row))"
        
        print("dist",Cell.lblDistance.text)
        
        Cell.lblReviewsCount.text = "Reviews \(Schedule_Data.ProviderLisreviewsArray.objectAtIndex(indexPath.row))"

        let strAvailable="\(Schedule_Data.ProviderListAvailableArray.objectAtIndex(indexPath.row))"
        Cell.imgProvider.layer.cornerRadius = Cell.imgProvider.frame.width/2;
         Cell.imgProvider.layer.masksToBounds = true;
        Cell.imgProvider.sd_setImageWithURL(NSURL(string: "\(Schedule_Data.ProviderListImageArray.objectAtIndex(indexPath.row) )"), completed: themes.block)

        
        let getarray : NSArray = Schedule_Data.ProviderListBadgeArray.objectAtIndex(indexPath.row) as! NSArray
        
        if getarray.count > 0
        {
            Cell.verify_tick.hidden = false
        }
        else
        {
            Cell.verify_tick.hidden = true
        }
    Cell.imgProvider.tag = indexPath.row
         Cell.imgProvider.userInteractionEnabled = true
        let tap :UITapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(ProviderListViewController.handleTap(_:)))
        Cell.imgProvider.addGestureRecognizer(tap)
       
        Cell.btnChat.addTarget(self, action: #selector(ProviderListViewController.PushtoMessageView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.btnChat.tag=indexPath.row
        
        Cell.btnOrderConfirm.addTarget(self, action: #selector(ProviderListViewController.OrderConfirm(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.btnOrderConfirm.tag=indexPath.row
        Cell.backgroundColor = UIColor.clearColor()
        
        return Cell
        
    }
    
    func  handleTap(recognizer:UITapGestureRecognizer)
    {
   // NSString *uid = testArray[recognizer.view.tag];
        
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MYProfileVCSID") as! MyProfileViewController
        secondViewController.providerid = Schedule_Data.ProviderListIdArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.hour_amount = Schedule_Data.ProviderListHouramountArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.min_amount = Schedule_Data.ProviderListFlatamountArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.cat_type = Schedule_Data.ProviderListCategoryTypeArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
       secondViewController.getjob_status = "hide"
        self.navigationController?.pushViewController(secondViewController, animated: true)

        }
  
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.provider_List.deselectRowAtIndexPath(indexPath, animated:true)
    }

    @IBAction func btnBack(sender: AnyObject) {
         self.navigationController?.popViewControllerAnimated(true)
    }
    func PushtoMessageView(sender:UIButton)
    {
        Message_details.taskid = Schedule_Data.TaskID
        Message_details.providerid = Schedule_Data.ProviderListIdArray .objectAtIndex(sender.tag) as! NSString
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
            switch buttonIndex{
            case 0:
                bookOrder(View.tag)
                break;
            default:
                break;
                //Some code here..
                
            }
    }
    

    func bookOrder(tag:Int){
    constant.showProgress()
    
    taskerid="\(ProviderListArray[tag])"
    
    let param=["user_id":"\(themes.getUserID())","location":Schedule_Data.scheduleAddressid,"taskerid":"\(taskerid)","taskid":"\(Schedule_Data.TaskID)","tasklat":"\( Schedule_Data.tasker_lat)","tasklng":"\( Schedule_Data.tasker_lng)"]
    
    URL_handler.makeCall(constant.Order_Confirm, param: param) { (responseObject, error) -> () in
        constant.DismissProgress()
        if(error != nil)
        {
            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            
            //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
        }
        else
        {
            if(responseObject != nil)
            {
                let Dict:NSDictionary=responseObject!
                
                let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                
                if(Status == "1")
                {
                    let response:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                    let jobID:NSString=response.objectForKey("job_id") as! NSString
                    Schedule_Data.JobID="\(jobID)"
                    Schedule_Data.orderDate = response.objectForKey("booking_date") as! NSString
                    Schedule_Data.service = response.objectForKey("service_type") as! NSString
                    Schedule_Data.jobDescription = response.objectForKey("description") as! NSString
                    Schedule_Data.service_tax = self.themes.CheckNullValue(response.objectForKey("service_tax"))!

                    let providerid : NSString =  self.taskerid
                    
                    var getproviderarray : NSMutableArray = NSMutableArray()
                    
                    getproviderarray = self.dbfileobj.arr("Provider_Table")
                    
                    if getproviderarray.count == 0
                    {
                        
                        let dict: NSMutableDictionary = NSMutableDictionary()
                        dict["providerid"] = providerid
                        
                        
                        self.dataArray = NSMutableArray()
                        self.dataArray.addObject(dict)
                        self.dbfileobj.saveData("Provider_Table", ValueStr: self.dataArray)
                        
                    }
                    else
                    {
                        let containdict: NSMutableDictionary = NSMutableDictionary()
                        containdict["providerid"] = providerid
                        
                        
                        
                        if getproviderarray.containsObject(containdict) {
                            
                            
                        }
                        else
                        {
                            
                            self.dbfileobj.UpdateData(providerid as String)
                            
                            
                            
                            
                        }
                    }
                    
                    self.performSegueWithIdentifier("OrderConfirmVC", sender: nil)
                    
                    
                }
                else
                {
                    let Response = self.themes.CheckNullValue(Dict.objectForKey("response"))!
                    self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                    
                }
                
            }
            else
            {
                self.themes.AlertView("\(Appname)", Message: self.themes.setLang("Please try again"), ButtonTitle: self.themes.setLang("ok"))
            }
        }
        
    }

}
    func OrderConfirm(sender:UIButton)
    {
 
        let AlertView:UIAlertView=UIAlertView()
        AlertView.delegate=self
        AlertView.title="Please Click Confirm To Book This Provider"
        AlertView.addButtonWithTitle("Confirm")
        AlertView.addButtonWithTitle("Cancel")
        AlertView.show()
        AlertView.tag = sender.tag

        
        
    }
//    override func showProgress()
//    {
//        self.activityIndicatorView.color = themes.DarkRed()
//        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
//        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
//        self.activityIndicatorView.startAnimation()
//        self.view.addSubview(activityIndicatorView)
//    }
//    override func DismissProgress()
//    {
//        self.activityIndicatorView.stopAnimation()
//        
//        self.activityIndicatorView.removeFromSuperview()
//        
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
