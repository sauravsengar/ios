//
//  CategorylistViewController.swift
//  Plumbal
//
//  Created by Casperon on 17/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CategorylistViewController: RootViewController,UISearchBarDelegate {
    @IBOutlet var category_table: UITableView!
     @IBOutlet var searchCat: UISearchBar!
    @IBOutlet var title_lbl: CustomLabelWhite!
    var urlHandler = URLhandler()
    var seachtag : NSInteger!
    var  search_range : NSRange!
    @IBOutlet var mapView_btn: UIButton!
    var CategoryArray:NSMutableArray = NSMutableArray()
   
    @IBOutlet var mapviewbtn: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("\(Appname)")
        mapviewbtn.setTitle(themes.setLang("map_view"), forState: .Normal)

        let cellnib = UINib.init(nibName: "CategoryCellTableViewCell", bundle: nil)
        self.category_table.registerNib(cellnib, forCellReuseIdentifier: "categorycell")
        self.category_table.estimatedRowHeight = 45
        self.category_table.rowHeight = UITableViewAutomaticDimension
        self.category_table.tableFooterView=UIView()

        searchCat.backgroundColor = UIColor.clearColor()
        
        searchCat.layer.cornerRadius = 7.0
        searchCat.clipsToBounds = true
        mapviewbtn.layer.cornerRadius = 7.0
        mapviewbtn.clipsToBounds = true

        searchCat.delegate = self
        self.showProgress()
        self.Home_Datafeed()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didclickBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func DidclickMap(sender:AnyObject)
    {
        Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "HomePageVCID")

    }

    func Home_Datafeed() {
        let param:Dictionary=["location_id":"\(themes.getLocationID())"]
        urlHandler.makeCall(constant.Get_Categories, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            dispatch_async(dispatch_get_main_queue(),{
                
                if(error != nil){
                    //self.settablebackground()
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }  else {
                    if(responseObject != nil) {
                        let dict:NSDictionary=responseObject!
                        let Status:NSString?=themes.CheckNullValue(dict.objectForKey("status"))!
                        if(Status != nil){
                            if(Status! == "1")  {
                           
                                if(self.CategoryArray.count != 0) {
                                    self.EmptyArray()
                                }
                                let CategoryArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("category") as! NSArray
                                for Dictionary in CategoryArray{
                                    let categoryid1:String=themes.CheckNullValue(Dictionary.objectForKey("cat_id"))!
                                    let categoryimage1:String=themes.CheckNullValue(Dictionary.objectForKey("active_icon"))!
                                    let categoryname1:String=themes.CheckNullValue(Dictionary.objectForKey("cat_name"))!
                                    
                                    let Rec : SearchCategory = SearchCategory.init(id: categoryid1 , category: categoryname1 , Catimg: categoryimage1)
                                    self.CategoryArray.addObject(Rec)
                                }
                                
                                self.category_table.reloadData()
                            } else {
                                if (responseObject?.objectForKey("response") != nil) {
                                    let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                    themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                                }
                            }
                        }else {
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                    }else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
        }
    }
    
    func  EmptyArray() -> () {
        self.CategoryArray.removeAllObjects()
     
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CategoryArray.count > 0{
            return self.CategoryArray.count
            
        }
        else{
            return 0
            
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("categorycell") as! CategoryCellTableViewCell
        Cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        Cell.selectionStyle = .None
         Cell.loadCategoryTableCell(CategoryArray.objectAtIndex(indexPath.row) as! SearchCategory) 
        return Cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let getCat_id = self.CategoryArray.objectAtIndex(indexPath.row) as! SearchCategory
        let controller  = self.storyboard?.instantiateViewControllerWithIdentifier("SubCategorylist") as! SubCatlistViewController
        
        controller.catid = getCat_id.cat_id
        cusProviderRec.MainCatid = getCat_id.cat_id

        self.navigationController?.pushViewController(controller, animated: true)
    }

    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text != ""{
            
            let NewText = (searchBar.text! as NSString).stringByReplacingCharactersInRange(range, withString:text)
            print(NewText);
            let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
            var searchString = String()
            (NewText as String).enumerateSubstringsInRange(range, options: .ByComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
                searchString.appendContentsOf(substring!)
                searchString.appendContentsOf("*")
            })
            
            let bPredicate = NSPredicate(format: "SELF.cat LIKE[cd] %@", searchString)
            let  filteredArray  = CategoryArray.filterUsingPredicate(bPredicate)
            
         
            print("getsearcharray\(filteredArray)")
            
           
            category_table.reloadData()
            category_table.hidden = false
        }
        else{
            Home_Datafeed()
        }
        return true
        
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar!)
    {
        
        
    }
    
    
    
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
