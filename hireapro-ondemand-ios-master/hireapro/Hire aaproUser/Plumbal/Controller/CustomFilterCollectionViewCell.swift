//
//  CustomFilterCollectionViewCell.swift
//  Plumbal
//
//  Created by Casperon on 24/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CustomFilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var custom_img: UIImageView!
    @IBOutlet weak var custom_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
