//
//  TransactionPopupViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 12/12/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class TransactionPopupViewController:RatingsViewController,UIWebViewDelegate {
    @IBOutlet var TransationWebView: UIWebView!
    @IBOutlet var Webload_progress: UIProgressView!
    var theBool: Bool=Bool()
    var myTimer: NSTimer=NSTimer()
    // var themes:Themes=Themes()
 
   
    @IBOutlet var backbtn: UIButton!

    @IBOutlet var Transaction_Lbl: UILabel!

    override func viewDidLoad() {
               
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)

        Webload_progress.tintColor=themes.ThemeColour()
        
        // Do any additional setup after loading the view, typically from a nib.
        
               let paymentUrl : String

        paymentUrl = Payment_Detail.PaymentUrl as String
        
        NSLog("Get payment Url=%@", paymentUrl)
             let url = NSURL (string:paymentUrl);
            let requestObj = NSURLRequest(URL: url!);
            TransationWebView.loadRequest(requestObj);
         self.Webload_progress.progress = 0.0
        TransationWebView.delegate=self
        
        let transform:CGAffineTransform = CGAffineTransformMakeScale(1.0, 2.0);
        Webload_progress.transform = transform;


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let URL:NSString=(request.URL?.absoluteString)!
        
        
        print("contain Transaction \(URL)")
        
        
        if (Payment_Detail.paymentmode == "paypal")
        {
            if(URL.containsString("\(constant.BaseUrl)/mobile/mobile/paypalsucess"))
            {
                themes.AlertView(themes.setLang("hurray"), Message: themes.setLang("Payment Success"), ButtonTitle: kOk)
                // self.performSegueWithIdentifier("RatingVC", sender: nil)
                let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                self.navigationController?.pushViewController(Controller, animated: true)

                
            }
            else if (URL.containsString("/payment/paypal/cancel"))
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("payment_cancelled"), ButtonTitle: kOk)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
                
           
          
        }
        
        else{
        
        if(URL.containsString("\(constant.BaseUrl)/mobile/payment/pay-completed"))
        {
             print("url is \(URL)")
            themes.AlertView(themes.setLang("hurray"), Message:themes.setLang("payment_success"), ButtonTitle: kOk)
           // self.performSegueWithIdentifier("RatingVC", sender: nil)
            let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
            self.navigationController?.pushViewController(Controller, animated: true)

            
        }
        
       else if(URL.containsString("\(constant.BaseUrl)/mobile/mobile/failed"))
        {
            print("url is \(URL)")
            themes.AlertView("\(Appname)", Message: themes.setLang("payment_failed"), ButtonTitle: kOk)
            self.navigationController?.popToRootViewControllerAnimated(true)
            
            
        }
         else if(URL.containsString("/mobile/payment/pay-cancelled"))
        {
             print("url is \(URL)")
            themes.AlertView("\(Appname)", Message: themes.setLang("payment_failed"), ButtonTitle: kOk)
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        }
            
            else if(URL.containsString("\(constant.BaseUrl)/mobile/mobile/provider-disconnected"))
        {
            print("url is \(URL)")
            themes.AlertView("\(Appname)", Message: themes.setLang("Provider Not Connected With Us, Try Another Mode Of Payment"), ButtonTitle: kOk)
            self.navigationController?.popToRootViewControllerAnimated(true)
            
            }

        }
        
        return true
    }
    
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        //        themes.setLang(
        
        //        themes.setLang("Full Name",comment: nil)
        
        Transaction_Lbl.text=themes.setLang("transaction")
        
    }

    
    func webViewDidStartLoad(webView: UIWebView) {
        funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        funcToCallCalledWhenUIWebViewFinishesLoading()
        
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.theBool = false
        self.myTimer = NSTimer.scheduledTimerWithTimeInterval(0.01667, target: self, selector: #selector(TransactionPopupViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.Webload_progress.progress >= 1 {
                self.Webload_progress.hidden = true
                self.myTimer.invalidate()
            } else {
                self.Webload_progress.progress += 0.1
            }
        } else {
            self.Webload_progress.progress += 0.05
            if self.Webload_progress.progress >= 0.95 {
                self.Webload_progress.progress = 0.95
            }
        }
    }
    

    
    
    @IBAction func didClickoption(sender: UIButton) {
        
        if(sender.tag == 0)
        {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
