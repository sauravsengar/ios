//
//  reviewViewController.swift
//  Plumbal
//
//  Created by Casperon on 07/02/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class reviewViewController: RootViewController,UITextViewDelegate {
    private var loading = false {
        didSet {
            
        }
    }

    @IBOutlet var title_lbl: UILabel!
    var themes:Themes=Themes()
      var nextPageStr:NSInteger!
    let URL_Handler:URLhandler=URLhandler()
    var reviewsArray:NSMutableArray = [];


    @IBOutlet var review_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("reviews")
        nextPageStr = 0
        review_table.registerNib(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTblIdentifier")
        review_table.estimatedRowHeight = 130
        review_table.rowHeight = UITableViewAutomaticDimension
        review_table.tableFooterView = UIView()

        self.GetReviews()
        
        

        // Do any additional setup after loading the view.
    }

    @IBAction func menubtnAction(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func GetReviews(){
        
        let Param: Dictionary = ["user_id":"\(themes.getUserID())",
                                 "role":"user",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]
        // print(Param)
        self.showProgress()
        
        URL_Handler.makeCall(constant.GetUserreviews, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.review_table.hidden=false
            self.review_table.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict1:NSDictionary=responseObject!.objectForKey("data") as! NSDictionary

                    let status:NSString=self.themes.CheckNullValue(Dict1.objectForKey("status") as? NSString)!
                    
                    if(status == "1")
                    {
                        let Dict:NSDictionary=responseObject!.objectForKey("data") as! NSDictionary

                        if(Dict.objectForKey("response")?.objectForKey("reviews")!.count>0){
                            let  listArr:NSArray=Dict.objectForKey("response")?.objectForKey("reviews") as! NSArray
                            if(self.nextPageStr==1){
                                self.reviewsArray.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerate() {
                                let rec = ReviewRecords(name: self.themes.CheckNullValue(element.objectForKey("tasker_name"))!, time: self.themes.CheckNullValue(element.objectForKey("date"))!, desc: self.themes.CheckNullValue(element.objectForKey("comments"))!, rate: self.themes.CheckNullValue(element.objectForKey("rating"))!, img: self.themes.CheckNullValue(element.objectForKey("tasker_image"))!,ratting:self.themes.CheckNullValue(element.objectForKey("image"))!,jobid :self.themes.CheckNullValue(element.objectForKey("booking_id"))!)
                                
                                [self.reviewsArray .addObject(rec)]
                            }
                            self.review_table.reloadData()
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:"You are at the end. No reviews to load", duration: 3, position: HRToastPositionDefault, title:"\(Appname)")
                            }
                        }
                    }
                    else
                    {
                        let message:NSString=self.themes.CheckNullValue(responseObject!.valueForKeyPath("data.response") as? NSString)!
                        self.themes.AlertView(Appname, Message: message, ButtonTitle: kOk)
                    }
                }
                else
                {
                    self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
                }
            }
            
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
   
//    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat
//    {
//        
//        return 115
//        
//        
//    }
    

    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
            return reviewsArray.count
           }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        let cell3:UITableViewCell
        
       
            let cell:ReviewsTableViewCell = tableView.dequeueReusableCellWithIdentifier("ReviewsTblIdentifier") as! ReviewsTableViewCell
            
            if (reviewsArray.count > 0)
            {
                cell.loadReviewTableCell((reviewsArray .objectAtIndex(indexPath.row) as! ReviewRecords), currentView:MyProfileViewController() as UIViewController)
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyle.None
            cell3=cell
        
        
        return cell3
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
