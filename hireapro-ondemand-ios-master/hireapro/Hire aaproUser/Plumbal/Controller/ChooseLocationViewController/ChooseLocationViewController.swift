//
//  ChooseLocationViewController.swift
//  Plumbal
//
//  Created by Casperon iOS on 24/11/2016.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class ChooseLocationViewController: RootViewController,UISearchResultsUpdating,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate {
    @IBOutlet var title_lbl: CustomLabelWhite!
    
    @IBOutlet var drag_pick_lbl: CustomLabelWhite!
    @IBOutlet var done_btn: UIButton!
    var lat : String = ""
    var long:String = ""
    struct Place {
        let id: String
        let description: String
    }
    @IBOutlet var viewMap1:GMSMapView!
    @IBOutlet var btnPin:UIButton!
    @IBOutlet var viewService:UIView!
    @IBOutlet var btnLocation:UIButton!
    @IBOutlet var txtLocation:UITextField!


    var locationManager = CLLocationManager()
    var sampleArray = NSMutableArray()

    
    var places = [Place]()
    var selectedAddress = String()
    
    @IBOutlet weak var countryTable: UITableView!
    var searchArray = [String]()
//    var countrySearchController: UISearchController = ({
//        let controller = UISearchController(searchResultsController: nil)
//        controller.hidesNavigationBarDuringPresentation = false
//        controller.dimsBackgroundDuringPresentation = false
//        controller.searchBar.searchBarStyle = .Prominent
//        controller.searchBar.sizeToFit()
//        controller.searchBar.layer.borderColor = UIColor.darkGrayColor().CGColor
//        controller.searchBar.backgroundColor = UIColor.lightGrayColor()
//        controller.searchBar.layer.borderWidth = 1
//        controller.searchBar.layer.cornerRadius = 3.0
//        controller.searchBar.clipsToBounds = true
//        return controller
//    })()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideView()
        title_lbl.text = themes.setLang("pick_loc")
        done_btn.setTitle(themes.setLang("done"), forState: .Normal)
        drag_pick_lbl.text = themes.setLang("drag_pick")
        txtLocation.delegate = self
        txtLocation.addTarget(self, action: #selector(ChooseLocationViewController.TextfieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)

        viewMap1.delegate = self
        self.definesPresentationContext = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideView(){
        countryTable.hidden = true
        
    }
    
    func showView(){
        countryTable.hidden = false

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        switch countrySearchController.active {
//        case true:
            return places.count
//        case false:
//            return 0
//        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = countryTable.dequeueReusableCellWithIdentifier("Cell") as! SearchTableViewCell
        cell.textLabel?.text = ""
        cell.textLabel?.attributedText = NSAttributedString(string: "")
        
//        switch countrySearchController.active {
//        case true:
            let place = self.places[indexPath.row]
            cell.textLabel?.text=place.description
            return cell
//        case false:
//            return cell
//        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        hideView()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let place = self.places[indexPath.row]
        txtLocation.text = place.description
        self.view.endEditing(true)
        txtLocation.resignFirstResponder()
        getLocation(place.id)
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        searchArray.removeAll(keepCapacity: false)
        
        let range = searchController.searchBar.text!.characters.startIndex ..< searchController.searchBar.text!.characters.endIndex
        let searchString = String()
        searchController.searchBar.text?.enumerateSubstringsInRange(range, options: .ByComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
            print(searchController.searchBar.text)
            self.getPlaces(searchController.searchBar.text!)
            // searchString.appendContentsOf(substring!)
            //searchString.appendContentsOf("*")
        })
        
        _ = NSPredicate(format: "SELF LIKE[cd] %@", searchString)
        countryTable.reloadData()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        showView()
        textField.text = ""
        
    }
    
    func TextfieldDidChange(textField:UITextField)
    {
        if(textField == txtLocation)
        {
            if(txtLocation.text != "")
            {
                searchArray.removeAll(keepCapacity: false)
         self.getPlaces(textField.text!)
                countryTable.reloadData()

            }
            else
            {
                
                countryTable.hidden = true
                
                
            }
        }
    }
    
    func set_mapView(lat:NSString,long:NSString){
        delay(0.5) { () -> () in
            let zoomOut = GMSCameraUpdate.zoomTo(10)
            self.viewMap1.animateWithCameraUpdate(zoomOut)
            let UpdateLoc = CLLocationCoordinate2DMake(CLLocationDegrees(lat as String)!,CLLocationDegrees(long as String)!)
            let Camera = GMSCameraUpdate.setTarget(UpdateLoc)
            self.viewMap1.animateWithCameraUpdate(Camera)
            self.viewMap1.myLocationEnabled = true
            self.delay(0.5, completion: { () -> () in
                let zoomIn = GMSCameraUpdate.zoomTo(14)
                self.viewMap1.animateWithCameraUpdate(zoomIn)
            })
        }
        viewMap1.settings.setAllGesturesEnabled(true)
        viewMap1.settings.scrollGestures=true
    }
    
    func getAddressForLatLng(latitude: String, longitude: String) {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GoogleplacesAPI)")
        let data = NSData(contentsOfURL: url!)
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                print(result)
                if(result.count != 0){
                    if let address = result[0]["formatted_address"] as? String{
                        txtLocation.text = address
                        self.sampleArray = NSMutableArray()
                        self.sampleArray.addObject(latitude)
                        self.sampleArray.addObject(longitude)
                        self.sampleArray.addObject(address)

                    }
                }
            }
        }
    }
    func delay(seconds: Double, completion:()->()) {
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64( Double(NSEC_PER_SEC) * seconds ))
        dispatch_after(popTime, dispatch_get_main_queue()) {
            completion()
        }
    }
    
    
    func getPlaces(searchString: String) {
        let request = requestForSearch(searchString)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request) { data, response, error in
            self.handleResponse(data, response: response as? NSHTTPURLResponse, error: error)
        }
        task.resume()
    }
    
    func getLocation(placeId:String){
        let params = [
            "placeid":"\(placeId)",
            "key": "\(constant.GoogleplacesAPI)"
        ]
        let request:NSURL = NSURL(string: "https://maps.googleapis.com/maps/api/place/details/json?\(query(params))")!
        print(request)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(NSMutableURLRequest(URL: request)) { data, response, error in
            self.handleLocationResponse(data, response: response as? NSHTTPURLResponse, error: error)
        }
        task.resume()
    }
    
    func requestForSearch(searchString: String) -> NSURLRequest {
        let params = [
            "input": searchString,
            "key": "\(constant.GoogleplacesAPI)",
            "location":"\(lat),\(long)",
            "radius":"1000000"]
        
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")
        return NSMutableURLRequest(
            URL: NSURL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")!
        )
    }
    
    func query(parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sort(<)) {
            let value: AnyObject! = parameters[key]
            components += [(escape(key), escape("\(value)"))]
        }
        return components.map{"\($0)=\($1)"}.joinWithSeparator("&")
    }
    
    func escape(string: String) -> String {
        let legalURLCharactersToBeEscaped: CFStringRef = ":/?&=;+!@#$()',*"
        return CFURLCreateStringByAddingPercentEscapes(nil, string, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
    }
    
    func handleResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        if let error = error {
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            return
        }
        if response == nil {
            print("GooglePlacesAutocomplete Error: No response from API")
            return
        }
        if response.statusCode != 200 {
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            return
        }
        do {
            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(
                data,
                options: NSJSONReadingOptions.MutableContainers
                ) as! NSDictionary
            dispatch_async(dispatch_get_main_queue(), {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    print(predictions)
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        return Place(
                            id: prediction["place_id"] as! String,
                            description: prediction["description"] as! String
                        )
                    }
                    self.countryTable.reloadData()
                }
            })
        }
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
    }
    func handleLocationResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        if let error = error {
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            return
        }
        if response == nil {
            print("GooglePlacesAutocomplete Error: No response from API")
            return
        }
        if response.statusCode != 200 {
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            return
        }
        do {
            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(
                data,
                options: NSJSONReadingOptions.MutableContainers
                ) as! NSDictionary
            dispatch_async(dispatch_get_main_queue(), {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                print(json)
                if let predictions = json["result"] as? NSDictionary {
                    
                    print(predictions)
                    self.sampleArray = NSMutableArray()
                    self.sampleArray.addObject(predictions.valueForKeyPath("geometry.location.lat")!)
                    self.sampleArray.addObject(predictions.valueForKeyPath("geometry.location.lng")!)
                    self.sampleArray.addObject(self.txtLocation.text!)
                    self.set_mapView("\(self.sampleArray.objectAtIndex(0))", long: "\(self.sampleArray.objectAtIndex(1))")
                    
                    //                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                    //                        return Place(
                    //                            id: prediction["place_id"] as! String,
                    //                            description: prediction["description"] as! String
                    //                        )
                    //                    }
                    //
                    
                    //               NSNotificationCenter.defaultCenter().postNotificationName("Location", object: nil, userInfo: ["key1":])
                    //                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                    //                        return Place(
                    //                            id: prediction["id"] as! String,
                    //                            description: prediction["description"] as! String
                    //                        )
                    //                    }
                }
            })
        }
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
    }
    
    @IBAction func didClickOptions(sender: UIButton) {
        if sampleArray.count != 0{
            NSNotificationCenter.defaultCenter().postNotificationName("Location", object:sampleArray)
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func didPickLocation(sender: AnyObject) {
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func didClickBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)

    }
    
    @IBAction func didClickDone(sender: AnyObject) {
        if sampleArray.count != 0{
            NSNotificationCenter.defaultCenter().postNotificationName("Location", object:sampleArray)
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - LocationManager Delegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _ = CLGeocoder()
        let current = locations[0]
        if current.coordinate.latitude != 0 {
            let currentLatitide = "\(current.coordinate.latitude)"
            let currentLongitude = "\(current.coordinate.longitude)"
            locationManager.stopUpdatingLocation()
            set_mapView(currentLatitide, long: currentLongitude)
        }
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        getAddressForLatLng("\(mapView.camera.target.latitude)", longitude: "\(mapView.camera.target.longitude)")
    }
    
}
