//
//  AboutUsViewController.swift
//  Plumbal
//
//  Created by Casperon on 03/10/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class AboutUsViewController: RootViewController {

    @IBOutlet var Titlelable: UILabel!
    @IBOutlet var versionlabl: UILabel!
    @IBOutlet var AboutUs: UILabel!

    @IBOutlet var abtWebview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let version = ( NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String)
        self.AboutUs.text = Language_handler.VJLocalizedString("about_us", comment: nil)
        versionlabl.text = "\(themes.setLang("version")) : \(version)"
        Titlelable.text = "\(themes.setLang("powered_by")) : \(Appname)"
        
        let url : NSURL = NSURL(string: constant.About_us as String)!
        let request : NSURLRequest = NSURLRequest.init(URL: url)
        self.abtWebview.loadRequest(request)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuuact(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
