

//
//  OrdrsViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 01/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import Foundation

class OrdersViewController: UIViewController,MyPopupViewControllerDelegate,UIScrollViewDelegate,MNMBottomPullToRefreshManagerClient,OrderDetailViewControllerDelegate,PopupSortingViewControllerDelegate
{
    var Is_alertshown:Bool=Bool()
    var tField: UITextField!
    @IBOutlet var headerView: UIView!

    @IBOutlet var Selection_Segment: CustomSegmentControl!
    @IBOutlet var Myorder_lbl: UILabel!
    @IBOutlet var Order_Tableview: UITableView!
    @IBOutlet var SlideinMenu_But: UIButton!
    var margin: CGFloat = 0.0
    var URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()
    var isSegmentChanged = false
    @IBOutlet weak var lblFilter: UIButton!
    var hourlyrate:NSMutableArray=NSMutableArray()
    var flattype:NSMutableArray=NSMutableArray()
    var JobidArray:NSMutableArray=NSMutableArray()
    var JobtypeArray:NSMutableArray=NSMutableArray()
    var ServiceIconArray:NSMutableArray=NSMutableArray()
    var BookingDateArray:NSMutableArray=NSMutableArray()
    var JobStatusArray:NSMutableArray=NSMutableArray()
    var GetJobStatusArray:NSMutableArray=NSMutableArray()
   var ContactNumArray:NSMutableArray=NSMutableArray()
    var MessageStatusArray:NSMutableArray=NSMutableArray()
    var CancelStatusArray:NSMutableArray=NSMutableArray()
    var CallStatusArray : NSMutableArray = NSMutableArray()
    var OrderCompleteDetailArray:NSMutableArray=NSMutableArray()
    var service_typeArray : NSMutableArray = NSMutableArray()
    var ReasonDetailArray:NSMutableArray=NSMutableArray()
    var ReasonidArray:NSMutableArray=NSMutableArray()
    var TaskidArray:NSMutableArray=NSMutableArray()
    var TaskeridArray:NSMutableArray=NSMutableArray()
    
    var Choosedid:NSString=NSString()
    var ChoosedReasonid:NSString=NSString()
    
    var pullToRefreshManager:MNMBottomPullToRefreshManager=MNMBottomPullToRefreshManager()
    var PageCount:NSInteger=0
    var PageStatus:NSString=NSString()
    var SupportStatusArray:NSMutableArray=NSMutableArray()
    var ContactNumber:NSString=NSString()
    var refreshControl:UIRefreshControl=UIRefreshControl()
    
    
    let activityTypes: [NVActivityIndicatorType] = [
        .BallPulse]
    
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRectMake(0, 0, 0, 0),
                                                        type: .BallSpinFadeLoader)
    var AlertView:JTAlertView=JTAlertView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    deinit
    {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    
    @IBAction func SegmentAction(sender: CustomSegmentControl) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;
        
        if(segmentIndex == 0)
        {
            isSegmentChanged = true
            PageCount=0
            PageStatus="1"
            GetOrderDetails("\(PageStatus)",Page_Count: PageCount,ShowProgress: true)
            
        }
        if(segmentIndex == 1)
        {
            isSegmentChanged = true

            PageCount=0
            PageStatus="4"
            GetOrderDetails("\(PageStatus)",Page_Count: PageCount,ShowProgress: true)
            
            
        }
        if(segmentIndex == 2)
        {
            isSegmentChanged = true

            PageCount=0
            PageStatus="5"
            GetOrderDetails("\(PageStatus)",Page_Count: PageCount,ShowProgress: true)
            
            
        }
        
        
    }
    override func viewWillAppear(animated: Bool) {
        lblFilter.setTitle(themes.setLang("filter"), forState: UIControlState.Normal)
        lblFilter.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lblFilter.titleLabel?.numberOfLines = 2
        
        Myorder_lbl.text = themes.setLang("my_orders")
        
        
        pullToRefreshManager = MNMBottomPullToRefreshManager(pullToRefreshViewHeight: 60.0, tableView: Order_Tableview, withClient: self)
        
        
        Selection_Segment.frame=CGRectMake(5, headerView.frame.height+2, view.frame.size.width-10, 43)
        Selection_Segment.setTitle(themes.setLang("open"), forSegmentAtIndex: 0)
        Selection_Segment.setTitle(themes.setLang("completed"), forSegmentAtIndex: 1)
        Selection_Segment.setTitle(themes.setLang("cancelled"), forSegmentAtIndex: 2)
        Selection_Segment.selectedSegmentIndex=0
        Selection_Segment.tintColor=themes.ThemeColour()
        
        Selection_Segment.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Raleway", size: 14.0)!, NSForegroundColorAttributeName:PlumberThemeColor], forState: .Normal)
        
        
        
        let nibName = UINib(nibName: "OrderTableViewCell", bundle:nil)
        self.Order_Tableview.registerNib(nibName, forCellReuseIdentifier: "OrderCell")
        Order_Tableview.estimatedRowHeight = 190
        Order_Tableview.rowHeight = UITableViewAutomaticDimension
        Order_Tableview.separatorColor=UIColor.clearColor()
        Order_Tableview.frame = CGRectMake(Order_Tableview.frame.origin.x, Selection_Segment.frame.origin.y+Selection_Segment.frame.height+3, Order_Tableview.frame.width, Order_Tableview.frame.size.height)
        
        PageStatus="all"
        Schedule_Data.Schedule_header=self.themes.setLang("reason")
        GetOrderDetails("1",Page_Count: 0,ShowProgress: true)
        configurePulltorefresh()
        
    }
    
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        //        themes.setLang(
        
        //        themes.setLang("Full Name")
        Myorder_lbl.text=themes.setLang("My order")
        
        
        
    }
    
    
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.showPopup(_:)), name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.Show_rating(_:)), name: "ShowRating", object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("ConfigureNotification:"), name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrdersViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushPayment", object: nil)
        
        
        
        
    }
    func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    Message_details.support_chatid = refer_id!
                    Message_details.admin_id = admin_id!
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    func methodofReceivePaymentNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    func methodofReceiveRatingNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    
    
    func methodofReceivePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
                
            }else{
                
                let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                self.navigationController?.pushViewController(Controller, animated: true)
                
                
            }
            
        }
        
    }
    
    
    
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
                    alertView.addButton(self.themes.setLang(self.themes.setLang("ok")), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }

    
    func Show_rating(notification: NSNotification)
    {
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(RatingsViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    
                    let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
    }
    
    func showPopup(notification: NSNotification)
    {
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(PaymentViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    
                    let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
        
        
    }
    
    
    
    func methodOfReceivedMessagePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            
            Message_details.taskid = taskid!
            Message_details.providerid = check_userid
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        
        
    }
    
    
    func methodOfReceivedMessageNotification(notification: NSNotification){
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                    
                }else{
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_provider"))
                    alertView.addButton(self.themes.setLang("ok"), action: {
                        Message_details.taskid = taskid!
                        Message_details.providerid = check_userid
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                        
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                        
                        
                    })
                    alertView.show()
                    
                }
                
            }
        }
        
        
        
        
        
    }
    
    func Show_Alert(notification:NSNotification)
    {
        
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        let action:NSString! = userInfo["Action"]

        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){

            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    if action != "admin_notification"{
                    let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    }
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
    }
    
    
    
    func showProgress()
    {
        self.activityIndicatorView.color = PlumberThemeColor
        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        self.activityIndicatorView.startAnimation()
        self.view.addSubview(activityIndicatorView)
    }
    func DismissProgress()
    {
        self.activityIndicatorView.stopAnimation()
        
        self.activityIndicatorView.removeFromSuperview()
        
    }
    
    
    
    
    
    func configurePulltorefresh()
    {
        
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.addTarget(self, action: #selector(OrdersViewController.Order_dataFeed), forControlEvents: UIControlEvents.ValueChanged)
        self.Order_Tableview.addSubview(refreshControl)
        
        //    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        //    loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 0.5)
        //        loadingView.alpha=0.9
        //    Order_Tableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
        //
        //
        //
        //     }, loadingView: loadingView)
        //    Order_Tableview.dg_setPullToRefreshFillColor(themes.LightRed())
        //    Order_Tableview.dg_setPullToRefreshBackgroundColor(Order_Tableview.backgroundColor!)
    }
    
    func Order_dataFeed()
    {
        GetOrderDetails("\(self.PageStatus)",Page_Count: self.PageCount,ShowProgress:false)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        pullToRefreshManager.relocatePullToRefreshView()
    }
    
    func GetOrderDetails(Status:NSString,Page_Count:NSInteger,ShowProgress:Bool)
    {
        let param=["user_id":"\(themes.getUserID())","type":"\(Status)","page":"\(Page_Count)","perPage":"20"]
        Selection_Segment.enabled=false
        self.Order_Tableview.backgroundView=nil
        if(ShowProgress)
        {
            self.showProgress()
        }
        
        URL_handler.makeCall(constant.Get_Orders, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.refreshControl.endRefreshing()
            
            self.Selection_Segment.enabled=true
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                nibView.frame = self.Order_Tableview.bounds;
                if(self.isSegmentChanged == true){
                self.JobidArray.removeAllObjects()
                self.Order_Tableview.backgroundView=nibView
                }else {
                    self.Order_Tableview.backgroundView=nil

                }
                self.Order_Tableview.reloadData()
                self.isSegmentChanged = false

                self.pullToRefreshManager.tableViewReloadFinished()
                
            }
                
            else
            {
                
                let dict:NSDictionary=responseObject!
                
                let Status:NSString=dict.objectForKey("status") as! NSString
                self.isSegmentChanged = false

                if(Status == "1")
                {
                    
                    
                    print("the order is \(responseObject)....\(param)...\(constant.Get_Orders)")
                    
                    
                    
                    
                    let OrderDict:NSDictionary=responseObject?.objectForKey("response")  as! NSDictionary
                    
                    let OrderArray:NSArray?=OrderDict.objectForKey("jobs") as? NSArray
                    
                    //                    let total_jobs:NSString=OrderDict.objectForKey("total_jobs") as! NSString
                    //
                    //                    let current_page:NSString=OrderDict.objectForKey("current_page") as! NSString
                    //
                    //                    let perPage:NSString=OrderDict.objectForKey("perPage") as! NSString
                    
                    self.emptyArray()
                    
                    if(OrderArray != nil)
                    {
                        
                        if(OrderArray?.count != 0)
                        {
                            
                            for Dictionary in OrderArray!
                            {
                                let flat:NSString=Dictionary.objectForKey("type") as! NSString
                                self.flattype.addObject(flat)
                                let hour:Int=Dictionary.objectForKey("hourly_rate") as! Int
                                self.hourlyrate.addObject(hour)
                                
                                let job_id:NSString=Dictionary.objectForKey("job_id") as! NSString
                                self.JobidArray.addObject(job_id)
                                let service_type:NSString=Dictionary.objectForKey("service_type") as! NSString
                                self.JobtypeArray.addObject(service_type)
                                let service_icon:NSString=Dictionary.objectForKey("service_icon") as! NSString
                                self.ServiceIconArray.addObject(service_icon)
                                let booking_date:NSString=Dictionary.objectForKey("booking_date") as! NSString
                                
                                
                                
                                self.BookingDateArray.addObject(booking_date)
                                let job_status:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("job_status"))!
                                self.JobStatusArray.addObject(job_status)
                                let getJob_status : NSString = self.themes.CheckNullValue(Dictionary.objectForKey("status"))!
                                self.GetJobStatusArray.addObject(getJob_status)
                                let contact_number:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("contact_number"))!
                                let country_code : NSString = self.themes.CheckNullValue(Dictionary.objectForKey("country_code"))!
                                self.ContactNumArray.addObject("\(country_code)\(contact_number)")

                                self.ContactNumArray.addObject(contact_number)
                                let doMsg:NSString=Dictionary.objectForKey("doMsg") as! NSString
                                self.MessageStatusArray.addObject(doMsg)
                                let doCancel:NSString=Dictionary.objectForKey("doCancel") as! NSString
                                self.CancelStatusArray.addObject(doCancel)
                                let taskid:NSString=Dictionary.objectForKey("task_id") as! NSString
                                self.TaskidArray.addObject(taskid)
                                let provider:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("tasker_id"))!
                                self.TaskeridArray.addObject(provider)
                                let docall : NSString = Dictionary.objectForKey("doCall") as! NSString
                                self.CallStatusArray.addObject(docall)
                                
                                self.OrderCompleteDetailArray.addObject(booking_date)
                                self.service_typeArray.addObject(service_type)
                                //service_type
                                
                                let isSupport:NSString=Dictionary.objectForKey("isSupport") as! NSString
                                self.SupportStatusArray.addObject(isSupport)
                                
                            }
                            
                            
                            let numArr :NSArray = NSArray(array: self.BookingDateArray)
                            
                            let max : NSString =  numArr.valueForKeyPath("@max.self")! as! NSString
                            let min : NSString = numArr.valueForKeyPath( "@min.self")! as! NSString
                            
                            
                            print("  MAx =\(max) , min =\(min)")
                            
                            self.Order_Tableview.reloadData()
                            self.Order_Tableview.backgroundView=nil
                            
                        }
                        else
                        {
                            
                            let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                            nibView.frame = self.Order_Tableview.bounds;
                            self.Order_Tableview.backgroundView=nibView
                            
                            if(self.JobidArray.count != 0)
                            {
                                self.Order_Tableview.backgroundView=nil
                                
                            }

                            self.presentViewController(self.themes.Showtoast("No more Orders"), animated: false, completion: nil)
                        }
                    }
                    
                    self.pullToRefreshManager.tableViewReloadFinished()
                }
                else
                {
                    self.pullToRefreshManager.tableViewReloadFinished()
                    
                    let Response:NSString=responseObject?.objectForKey("response")  as! NSString
                    self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                    self.emptyArray()
                    self.Order_Tableview.reloadData()
                }
            }
            
        }
    }
    
    
    func emptyArray()
    {
        if(self.PageCount == 0)
        {
            
            if(self.JobidArray.count != 0)
            {
                self.JobidArray.removeAllObjects()
            }
            if(self.JobtypeArray.count != 0)
            {
                self.JobtypeArray.removeAllObjects()
            }
            if(self.ServiceIconArray.count != 0)
            {
                self.ServiceIconArray.removeAllObjects()
            }
            if(self.BookingDateArray.count != 0)
            {
                self.BookingDateArray.removeAllObjects()
            }
            if(self.GetJobStatusArray.count != 0)
            {
                self.GetJobStatusArray.removeAllObjects()
            }

            if(self.JobStatusArray.count != 0)
            {
                self.JobStatusArray.removeAllObjects()
            }
            if(self.TaskidArray.count != 0)
            {
                self.TaskidArray.removeAllObjects()
            }
            if(self.TaskeridArray.count != 0)
            {
                self.TaskeridArray.removeAllObjects()
            }
            if(self.ContactNumArray.count != 0)
            {
                self.ContactNumArray.removeAllObjects()
            }
            if(self.MessageStatusArray.count != 0)
            {
                self.MessageStatusArray.removeAllObjects()
            }
            if(self.CancelStatusArray.count != 0)
            {
                self.CancelStatusArray.removeAllObjects()
            }
            if (self.CallStatusArray.count  != 0)
            {
                self.CallStatusArray.removeAllObjects()
            }
            if(self.OrderCompleteDetailArray.count != 0)
            {
                self.OrderCompleteDetailArray.removeAllObjects()
            }
            if (self.service_typeArray.count  != 0)
            {
                self.service_typeArray.removeAllObjects()
            }
            if(self.SupportStatusArray.count != 0)
            {
                self.SupportStatusArray.removeAllObjects()
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableViewDelegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return JobidArray.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10
    }
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Completed" || JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Cancelled" )
            
        {
        
            return 150
        }
        else
        {
            return 180
        }
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell:OrderTableViewCell = tableView.dequeueReusableCellWithIdentifier("OrderCell") as! OrderTableViewCell
        
        Cell.frame.size.width=100.0
        Cell.bottomView.hidden = false
        if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Completed" || JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Cancelled" ){
            Cell.bottomView.hidden = true

        }
        Cell.selectionStyle = .None
        
        Cell.layer.cornerRadius = 5
        Cell.layer.shadowColor = UIColor.blackColor().CGColor
        Cell.layer.shadowOpacity = 0.5
        Cell.layer.shadowRadius = 2
        Cell.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        //        let height_lab:CGFloat = self.themes.calculateHeightForString("\(OrderCompleteDetailArray.objectAtIndex(indexPath.section))")
        //
        //        Cell.Order_Detail.frame.size.height = height_lab+30.0
        
        Cell.orderID_label.text="\(themes.setLang("job_id")): \(JobidArray.objectAtIndex(indexPath.section))"
        Cell.Order_Detail.text="\(OrderCompleteDetailArray.objectAtIndex(indexPath.section))"
        Cell.Jobtitle.text="\(JobtypeArray.objectAtIndex(indexPath.section))"
        Cell.flatRateLbl.text="\(flattype.objectAtIndex(indexPath.section)) : \(themes.getCurrencyCode())\(hourlyrate.objectAtIndex(indexPath.section))"
        Cell.Status_label.text="\(JobStatusArray.objectAtIndex(indexPath.section))"
        Cell.Message_Btn.addTarget(self, action: #selector(OrdersViewController.PushtoChatView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.Call_Btn.addTarget(self, action: #selector(OrdersViewController.Callto(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.Call_Btn.tag=indexPath.section
        Cell.Message_Btn.tag=indexPath.section
        Cell.Service_Image.layer.cornerRadius =  Cell.Service_Image.frame.width/2
        Cell.Service_Image.clipsToBounds = true

        if(ServiceIconArray.objectAtIndex(indexPath.section) as! NSString != "")
        {

            Cell.Service_Image.sd_setImageWithURL(NSURL(string: "\(ServiceIconArray.objectAtIndex(indexPath.section))"), completed: themes.block)
        }
        
        if(CancelStatusArray.objectAtIndex(indexPath.section) as! NSString == "Yes")
        {
            Cell.Cancel_But.hidden=false
            
            Cell.Cancel_But.addTarget(self, action: #selector(OrdersViewController.ShowReason(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            Cell.Cancel_But.tag=indexPath.section
        }
        else
        {
            
            Cell.Cancel_But.hidden=true
            
        }
        
        if(MessageStatusArray.objectAtIndex(indexPath.section) as! NSString == "Yes")
        {
            Cell.Message_Btn.hidden=false
            Cell.chatimg.hidden = false
            Cell.Message_Btn.addTarget(self, action: #selector(OrdersViewController.PushtoChatView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            Cell.Message_Btn.tag=indexPath.section
        }
        else
        {
            
            Cell.Message_Btn.hidden=true
            Cell.chatimg.hidden = true
        }
        
        if(CallStatusArray.objectAtIndex(indexPath.section) as! NSString == "Yes")
        {
            Cell.Call_Btn.hidden=false
            
            Cell.phoneimg.hidden = false
            
            Cell.Call_Btn.tag=indexPath.section
        }
        else
        {
            
            Cell.Call_Btn.hidden=true
            Cell.phoneimg.hidden = true
        }
        
        
        
        
        Cell.Status_label.backgroundColor=nil
        
        if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Confirmed")
        {
            Cell.Status_label.textColor=UIColor.purpleColor()
            // 0.0, 1.0, and 0.0 and whose alpha value is 1.0.
        }
            
        else if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Completed" || GetJobStatusArray.objectAtIndex(indexPath.section) as! NSString == "7")
        {
            Cell.Status_label.textColor=UIColor.greenColor()
            
            // 0.0, 1.0, and 0.0 and whose alpha value is 1.0.
        }
            
        else if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Cancelled" || GetJobStatusArray.objectAtIndex(indexPath.section) as! NSString == "8")
        {
            Cell.Status_label.textColor=UIColor.redColor()
            // 1.0, 0.0, and 0.0 and whose alpha value is 1.0.
            
        }
            
        else  if(JobStatusArray.objectAtIndex(indexPath.section) as! NSString == "Closed")
        {
            Cell.Status_label.textColor=UIColor(red: 34.0/2555.0, green: 139/255.0, blue: 34/255.0, alpha: 1.0)
            
        }
            
        else
            
        {
            Cell.Status_label.textColor=PlumberThemeColor
            
        }
        
        
        
        Cell.Order_Detail.sizeToFit()
        
        
        
        if(CallStatusArray.objectAtIndex(indexPath.section) as! NSString == "No"  && CancelStatusArray.objectAtIndex(indexPath.section) as! NSString == "No" && MessageStatusArray.objectAtIndex(indexPath.section) as! NSString == "No")
        {
            Cell.topborder.hidden = true
            Cell.sidebarf.hidden = true
            Cell.sidebarse.hidden = true
            
        }
        
        
        return Cell
        
    }
    
    
    func PushtoChatView(sender:UIButton)
    {
        
        Order_data.job_id="\(JobidArray[sender.tag])"
        
        //
        //        self.performSegueWithIdentifier("ChatVC", sender: nil)
        
        if(SupportStatusArray.objectAtIndex(sender.tag) as! String == "Yes")
        {
            ContactNumber="\(ContactNumArray[sender.tag])"
            
            let AlertView:UIAlertView=UIAlertView()
            AlertView.delegate=self
            AlertView.title="No Provider Assigned"
            AlertView.message="Are you sure you want to chat with the Support team?"
            AlertView.addButtonWithTitle("Yes")
            AlertView.addButtonWithTitle("No")
            AlertView.tag = 3
            AlertView.show()
            
        }
        else
        {
            var getproviderarray : NSMutableArray = NSMutableArray()
            
            getproviderarray = dbfileobj.arr("Provider_Table")
            if getproviderarray.count != 0
            {
                
                let providerid : NSString = getproviderarray.objectAtIndex(0).objectForKey("providerid") as! NSString
                
                Message_details.providerid = providerid
            }
            Message_details.taskid = TaskidArray[sender.tag] as! NSString
            Message_details.providerid = TaskeridArray[sender.tag] as! NSString
            Message_details.name = TaskidArray[sender.tag] as! NSString
            Message_details.image = TaskeridArray[sender.tag] as! NSString
            
            
            
            
            
            
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
            //        }
        }
        
    }
    
    func Callto(sender:UIButton)
    {
        Order_data.job_id="\(JobidArray[sender.tag])"
        
        if(SupportStatusArray.objectAtIndex(sender.tag) as! String == "Yes")
        {
            ContactNumber="\(ContactNumArray[sender.tag])"
            
            let AlertView:UIAlertView=UIAlertView()
            AlertView.delegate=self
            AlertView.title="No Provider Assigned"
            AlertView.message="Are you sure you want to Call the Support team?"
            AlertView.addButtonWithTitle("Yes")
            AlertView.addButtonWithTitle("No")
            AlertView.tag = 2
            AlertView.show()
            
        }
        else
        {
            
            let Number:String="\(ContactNumArray[sender.tag])"
            let trimmedString = Number.stringByReplacingOccurrencesOfString("\\s", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
            print("the number is  ...\(ContactNumArray[sender.tag])")
            
            UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(trimmedString)")!)
        }
        
        
    }
    
    
    func ShowReason(sender:UIButton)
    {
        
        self.showProgress()
        
        Choosedid="\(JobidArray[sender.tag])"
        
        let param=["user_id":"\(themes.getUserID())"]
        
        URL_handler.makeCall(constant.Get_Reasons, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    
                    self.ReasonDetailArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressNameArray.removeAllObjects()
                    
                    
                    self.ReasonidArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressArray.removeAllObjects()
                    
                    
                    
                    if(Status == "1")
                    {
                        //let ReasonArray:NSArray=Dict.objectForKey("response")!.objectForKey("reason") as! NSArray
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let ReasonArray : NSArray = ResponseDic.objectForKey("reason") as! NSArray
                        
                        for ReasonDict in ReasonArray
                        {
                            let Reason_Str:NSString=self.themes.CheckNullValue(ReasonDict.objectForKey("reason"))!
                            self.ReasonDetailArray.addObject(Reason_Str)
                            Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                            let Reasonid:NSString=self.themes.CheckNullValue(ReasonDict.objectForKey("id"))!
                            self.ReasonidArray.addObject(Reasonid)
                            Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                            
                            
                        }
                        
                        
                        
                        let Reason_Str:NSString="Others"
                        self.ReasonDetailArray.addObject(Reason_Str)
                        Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                        let Reasonid:NSString="1"
                        self.ReasonidArray.addObject(Reasonid)
                        Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                        
                        
                        
                        NSLog("get count=%d and another count=%d", Schedule_Data.ScheduleAddressNameArray.count,Schedule_Data.ScheduleAddressArray.count)
                        
                        self.displayReasonViewController(.BottomBottom)
                    }
                    else
                    {
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_reasons_available"), ButtonTitle: kOk)
                        
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_reasons_available"), ButtonTitle: kOk)
                }
            }
            
        }
        
        
        
    }
    
    func displayReasonViewController(animationType: SLpopupViewAnimationType){
        let myPopupViewController:MyPopupViewController = MyPopupViewController(nibName:"MyPopupViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.isDetailViewcontroller = true
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
        
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let Popupsortcontroller : PopupSortingViewController = PopupSortingViewController(nibName:"PopupSortingViewController",bundle: nil)
        
        
        if    Selection_Segment.selectedSegmentIndex == 1 ||  Selection_Segment.selectedSegmentIndex == 2{
          Popupsortcontroller.selecIndex = 1
        }else{
            Popupsortcontroller.selecIndex = 0
        }
        
        Popupsortcontroller.delegate = self;
        
        self.presentpopupViewController(Popupsortcontroller, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Order_data.job_id="\(JobidArray[indexPath.section])"
        
        self.performSegueWithIdentifier("OrderDetailVC", sender: nil)
    }
    
    func pressCancel(sender: MyPopupViewController) {
        
        
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    func pressAdd(sender: MyPopupViewController) {
        
    }
    func  pressedCancel(sender: PopupSortingViewController) {
        
        
        
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int,isToday:Int,isSortby:NSString) {
        
        
        if(isToday == 3){
            
            var from = fromdate
            var tod = todate
            var typests : NSString  = NSString ()
            if    Selection_Segment.selectedSegmentIndex == 0
                
            {
                typests = "1"
            }
            else if  Selection_Segment.selectedSegmentIndex == 1
            {
                typests = "4"
                
            }
            else if  Selection_Segment.selectedSegmentIndex == 2
            {
                typests = "5"
                
                
            }
            
            if from == "From Date" || tod == "To Date"
            {
                from  = ""
                tod = ""
            }
            
            let param=["user_id":"\(themes.getUserID())","type":"\(typests)","page":"\(self.PageCount)","perPage":"20","from":"\(from)","to":"\(tod)","orderby":"\(isAscendorDescend)","sortby":isSortby]
            
            self.showProgress()
            
            
            URL_handler.makeCall(constant.Get_SortingOrders, param: param) { (responseObject, error) -> () in
                self.DismissProgress()
                self.refreshControl.endRefreshing()
                
                
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    
                    //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                    let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                    nibView.frame = self.Order_Tableview.bounds;
                    self.Order_Tableview.backgroundView=nibView
                    self.Order_Tableview.reloadData()
                    self.pullToRefreshManager.tableViewReloadFinished()
                    
                }
                    
                else
                {
                    
                    let dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {
                        
                        let OrderDict:NSDictionary=responseObject?.objectForKey("response")  as! NSDictionary
                        
                        let OrderArray:NSArray?=OrderDict.objectForKey("jobs") as? NSArray
                        self.emptyArray()
                        
                        if(OrderArray != nil)
                        {
                            
                            if(OrderArray?.count != 0)
                            {
                                
                                for Dictionary in OrderArray!
                                {
                                    let job_id:NSString=Dictionary.objectForKey("job_id") as! NSString
                                    self.JobidArray.addObject(job_id)
                                    let service_type:NSString=Dictionary.objectForKey("service_type") as! NSString
                                    self.JobtypeArray.addObject(service_type)
                                    let service_icon:NSString=Dictionary.objectForKey("service_icon") as! NSString
                                    self.ServiceIconArray.addObject(service_icon)
                                    let booking_date:NSString=Dictionary.objectForKey("booking_date") as! NSString
                                    self.BookingDateArray.addObject(booking_date)
                                    let job_status:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("job_status"))!
                                    self.JobStatusArray.addObject(job_status)
                                    let contact_number:NSString=Dictionary.objectForKey("contact_number") as! NSString
                                    self.ContactNumArray.addObject(contact_number)
                                    let doMsg:NSString=Dictionary.objectForKey("doMsg") as! NSString
                                    self.MessageStatusArray.addObject(doMsg)
                                    let doCancel:NSString=Dictionary.objectForKey("doCancel") as! NSString
                                    self.CancelStatusArray.addObject(doCancel)
                                    let docall : NSString = Dictionary.objectForKey("doCall") as! NSString
                                    self.CallStatusArray.addObject(docall)
                                    let taskid:NSString=Dictionary.objectForKey("task_id") as! NSString
                                    self.TaskidArray.addObject(taskid)
                                    let provider:NSString=Dictionary.objectForKey("tasker_id") as! NSString
                                    self.TaskeridArray.addObject(provider)
                                    
                                    
                                    self.OrderCompleteDetailArray.addObject(booking_date)
                                    self.service_typeArray.addObject(service_type)
                                    
                                    let isSupport:NSString=Dictionary.objectForKey("isSupport") as! NSString
                                    self.SupportStatusArray.addObject(isSupport)
                                    
                                }
                                
                                self.Order_Tableview.backgroundView=nil
                                
                            }
                            else
                            {
                                
                                let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                                nibView.frame = self.Order_Tableview.bounds;
                                self.Order_Tableview.backgroundView=nibView
                                self.Order_Tableview.reloadData()
                                if(self.JobidArray.count != 0)
                                {
                                    self.Order_Tableview.backgroundView=nil
                                    
                                }
                                
                                self.presentViewController(self.themes.Showtoast("No more Orders"), animated: false, completion: nil)
                            }
                        }
                        
                        self.Order_Tableview.reloadData()
                        self.pullToRefreshManager.tableViewReloadFinished()
                    }
                    else
                    {
                        self.pullToRefreshManager.tableViewReloadFinished()
                        
                        let Response:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("response"))!
                        self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                        self.emptyArray()
                        self.Order_Tableview.reloadData()
                    }
                }
                
            }
        }else{
            dayFilter(isToday, isAsc: isAscendorDescend)
        }
    }
    
    
    func dayFilter(type:Int,isAsc:Int){
        var types = String()
        
        if(type == 0){
          types = "today"
        }else if(type == 1){
            types = "recent"
        }else if(type == 2){
            types = "upcoming"
        }
        
        let param=["user_id":"\(themes.getUserID())","type":"\(types)","page":"\(self.PageCount)","perPage":"20","orderby":"\(isAsc)","sortby":""]
        
        self.showProgress()
        
        
        URL_handler.makeCall(constant.Get_Todays_Orders, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.refreshControl.endRefreshing()
            
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                nibView.frame = self.Order_Tableview.bounds;
                self.Order_Tableview.backgroundView=nibView
                self.Order_Tableview.reloadData()
                self.pullToRefreshManager.tableViewReloadFinished()
                
            }
                
            else
            {
                
                let dict:NSDictionary=responseObject!
                
                let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                
                if(Status == "1")
                {
                    
                    let OrderDict:NSDictionary=responseObject?.objectForKey("response")  as! NSDictionary
                    
                    let OrderArray:NSArray?=OrderDict.objectForKey("jobs") as? NSArray
                    self.emptyArray()
                    
                    if(OrderArray != nil)
                    {
                        
                        if(OrderArray?.count != 0)
                        {
                            
                            for Dictionary in OrderArray!
                            {
                                let job_id:NSString=Dictionary.objectForKey("job_id") as! NSString
                                self.JobidArray.addObject(job_id)
                                let service_type:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("service_type") as! NSString)!
                                self.JobtypeArray.addObject(service_type)
                                let service_icon:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("service_icon") as! NSString)!
                                self.ServiceIconArray.addObject(service_icon)
                                let booking_date:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("booking_date") as! NSString)!
                                self.BookingDateArray.addObject(booking_date)
                                let job_status:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("job_status"))!
                                self.JobStatusArray.addObject(job_status)
                                let contact_number:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("contact_number") as! NSString)!
                                self.ContactNumArray.addObject(contact_number)
                                let doMsg:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("doMsg") as! NSString)!
                                self.MessageStatusArray.addObject(doMsg)
                                let doCancel:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("doCancel") as! NSString)!
                                self.CancelStatusArray.addObject(doCancel)
                                let docall : NSString = self.themes.CheckNullValue(Dictionary.objectForKey("doCall") as! NSString)!
                                self.CallStatusArray.addObject(docall)
                                let taskid:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("task_id") as! NSString)!
                                self.TaskidArray.addObject(taskid)
                                let provider:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("tasker_id") as! NSString)!
                                self.TaskeridArray.addObject(provider)
                                
                                
                                self.OrderCompleteDetailArray.addObject(booking_date)
                                self.service_typeArray.addObject(service_type)
                                
                                let isSupport:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("isSupport") as! NSString)!
                                self.SupportStatusArray.addObject(isSupport)
                                
                            }
                            
                            self.Order_Tableview.backgroundView=nil
                            
                        }
                        else
                        {
                            
                            let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                            nibView.frame = self.Order_Tableview.bounds;
                            self.Order_Tableview.backgroundView=nibView
                            self.Order_Tableview.reloadData()
                            if(self.JobidArray.count != 0)
                            {
                                self.Order_Tableview.backgroundView=nil
                                
                            }
                            
                            self.presentViewController(self.themes.Showtoast("No more Orders"), animated: false, completion: nil)
                        }
                    }
                    
                    self.Order_Tableview.reloadData()
                    self.pullToRefreshManager.tableViewReloadFinished()
                }
                else
                {
                    self.pullToRefreshManager.tableViewReloadFinished()
                    
                    let Response:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("response"))!
                    self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                    self.emptyArray()
                    self.Order_Tableview.reloadData()
                }
            }
            
        }

        
    }
    
    func PassSelectedAddress(Address: NSString, AddressIndexvalue: Int, latitudestr: NSString, longtitudestr: NSString,localitystr:NSString) {
        
        self.dismissPopupViewController(.BottomBottom)
        
        
        
        
        if (Address == "Others")
        {
            let alert = UIAlertController(title:themes.setLang("reason"), message: "", preferredStyle: .Alert)
            
            alert.addTextFieldWithConfigurationHandler(configurationTextField)
            alert.addAction(UIAlertAction(title: themes.setLang("cancel"), style: .Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: themes.setLang("done"), style: .Default, handler:{ (UIAlertAction) in
                
                
                self.ChoosedReasonid = self.tField.text!
                self.cancelRequest()
                
            }))
            self.presentViewController(alert, animated: true, completion: {
                print("completion block")
            })
            
            
        }
        else
        {
            
            ChoosedReasonid=Address
            let AlertView:UIAlertView=UIAlertView()
            AlertView.delegate=self
            AlertView.title=themes.setLang("cancel_confirmation")
            AlertView.addButtonWithTitle(themes.setLang("yes"))
            AlertView.addButtonWithTitle(themes.setLang("no"))
            AlertView.tag = 1
            AlertView.show()
        }
        
        
    }
    
    
    func configurationTextField(textField: UITextField!)
    {
        
        textField.placeholder = "Enter Your Reason"
        
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        if(View.tag == 1)
        {
            
            switch buttonIndex{
                
            case 0:
                self.cancelRequest()
                break;
            default:
                break;
                //Some code here..
                
            }
        }
        if(View.tag == 2)
        {
            
            switch buttonIndex{
                
            case 0:
                let Number:String="\(ContactNumber)"
                let trimmedString = Number.stringByReplacingOccurrencesOfString("\\s", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
                print("the number is  ...\(ContactNumber)")
                
                UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(trimmedString)")!)
                break;
            default:
                break;
                //Some code here..
                
            }
            
            
        }
        if(View.tag == 3)
        {
            
            switch buttonIndex{
                
            case 0:
                if (themes.canSendText()) {
                    // Obtain a configured MFMessageComposeViewController
                    let messageComposeVC = themes.configuredMessageComposeViewController("",number:"\(ContactNumber)")
                    // Present the configured MFMessageComposeViewController instance
                    // Note that the dismissal of the VC will be handled by the messageComposer instance,
                    // since it implements the appropriate delegate call-back
                    presentViewController(messageComposeVC, animated: true, completion: nil)
                } else {
                    // Let the user know if his/her device isn't able to send text messages
                    let errorAlert = UIAlertView(title: themes.setLang("Cannot Send Text Message"), message: themes.setLang("Your device is not able to send text messages."), delegate: self, cancelButtonTitle: self.themes.setLang("ok"))
                    errorAlert.show()
                }
                
                
                break;
            default:
                break;
                //Some code here..
                
            }
            
            
        }
        
        
    }
    
    func cancelRequest()
    {
        
        self.showProgress()
        let param=["user_id":"\(themes.getUserID())","reason":"\(ChoosedReasonid)","job_id":"\(Choosedid)"]
        
        
        
        URL_handler.makeCall(constant.Cancel_Reasons, param: param) { (responseObject, error) -> () in
            
            print("the param is \(param)....\(constant.Cancel_Reasons)...\(responseObject)...\(error)")
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    if(Status == "1")
                    {
                        
                        
                        self.GetOrderDetails("\(self.PageStatus)", Page_Count:self.PageCount,ShowProgress: true)
                    }
                    else
                    {
                        let Response:NSString=Dict.objectForKey("response") as! NSString
                        self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("cant_cancel"), ButtonTitle: kOk)
                }
            }
            
        }
        
        
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pullToRefreshManager.tableViewScrolled()
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        pullToRefreshManager.tableViewReleased()
        
    }
    func bottomPullToRefreshTriggered(manager: MNMBottomPullToRefreshManager!)
        
    {
        if (JobidArray.count > 20)
        {
            PageCount=PageCount+1
        }
        GetOrderDetails("\(PageStatus)", Page_Count: PageCount,ShowProgress: false)
        
    }
    func ReloadData(sender: OrderDetailViewController) {
        self.GetOrderDetails(PageStatus, Page_Count: PageCount,ShowProgress: true)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "OrderDetailVC"{
            let vc = segue.destinationViewController as! OrderDetailViewController
            vc.delegate = self
        }
    }
    
    @IBAction func didclickfilteroption(sender: AnyObject) {
        self.PageCount = 0
        self.displayViewController(.BottomBottom)
    }
    
    
}







 