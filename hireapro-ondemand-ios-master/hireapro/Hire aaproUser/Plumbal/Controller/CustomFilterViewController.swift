//
//  CustomFilterViewController.swift
//  Plumbal
//
//  Created by Casperon iOS on 22/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit
protocol CustomFilterViewControllerDelegate {
    
    func pressCancel(sender: CustomFilterViewController)
    func  passRequiredParametres(Param : NSMutableDictionary)

}


class CustomFilterViewController:RootViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,FloatRatingViewDelegate {
    @IBOutlet var upperLbl_TrailingConstrnt: NSLayoutConstraint!
    var minimumpriceval : NSString!
    var maximumpriceval : NSString!
    var getmaxRadius : NSString = NSString()
     var urlHandler = URLhandler()
      var delegate:CustomFilterViewControllerDelegate?
    @IBOutlet weak var cancle_btn: UIButton!
    
    @IBOutlet var filterscroll: UIScrollView!
    @IBOutlet weak var title_btn: UIButton!

    @IBOutlet var badgetableheightconst: NSLayoutConstraint!
    @IBOutlet weak var starRating_lbl: UILabel!
    @IBOutlet weak var reset_btn: UIButton!
    
    @IBOutlet weak var starRating: FloatRatingView!
    
    @IBOutlet weak var rating_lbl: UILabel!
    
    @IBOutlet weak var sortby_lbl: UILabel!
    @IBOutlet weak var sortby_collection: UICollectionView!
    
    @IBOutlet weak var lableSlider: NMRangeSlider!
//    @IBOutlet weak var sortby_img: UIImageView!
//    
//    @IBOutlet weak var sortby_lbl: UILabel!
    @IBOutlet weak var badge_lb: UILabel!
    
    @IBOutlet weak var distanceRange_lbl: UILabel!
    
    @IBOutlet weak var badge_tableView: UITableView!
    
    @IBOutlet weak var slider_view: NMRangeSlider!
    
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var apply_btn: UIButton!
    var sortBy_lblArray = ["Low To High","High To Low","Available First","Top Rate"]
    var sortby_imggray = ["lowtohighgray","lowtohighgray","Availablefirstgray","topRated_gray"]
     var sortby_imgcolor = ["lowtohighcolor","lowtohighcolor","AvailableFirst_color","topRated_color"]
    var badge_Array:[String] = []
    var sortby:[String] = []
    var sortingVal : String = String()
    var Badgesarray : NSMutableArray = NSMutableArray()
    @IBAction func valueChangedInSlider(sender: AnyObject)
    {
    updateSliderLabels()
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       Getpagestr = "Rating"
        
        
        if FilterRec.Ratingval != ""
        {
            starRating.rating = Float("\(FilterRec.Ratingval)")!
            self.rating_lbl.text = FilterRec.Ratingval as String
        }
        starRating.halfRatings = false
        starRating.floatRatings = false
        starRating.delegate = self
        
        badge_tableView.registerNib(UINib(nibName: "BadgeTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        sortby_collection.registerNib(UINib(nibName: "CustomFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "customcell")
       // badge_tableView.estimatedRowHeight = 54
        //badge_tableView.rowHeight = UITableViewAutomaticDimension
        if FilterRec.pricerange != ""
        {
            
            sortby.removeAll()
            
            if FilterRec.pricerange == "lowtohigh"
            {
                for i in 0..<sortby_imggray.count
                {
                    if i == 0{
                        sortby.append(sortby_imgcolor[0])
                        
                        
                    }
                    else{
                        
                        sortby.append(sortby_imggray[i])
                    }
                }
                sortby_collection.reloadData()
                
            }
            else if FilterRec.pricerange == "hightolow"
            {
                for i in 0..<sortby_imggray.count
                {
                    if i == 1{
                        sortby.append(sortby_imgcolor[1])
                        
                        
                    }
                    else{
                        
                        sortby.append(sortby_imggray[i])
                    }
                }
                sortby_collection.reloadData()
                
            }
        }
        
        if FilterRec.Toprating == "1"
        {
            for i in 0..<sortby_imggray.count
            {
                if i == 3{
                    sortby.append(sortby_imgcolor[3])
                    
                    
                }
                else{
                    
                    sortby.append(sortby_imggray[i])
                }
            }
            sortby_collection.reloadData()
            
            
        }
        
        
        if FilterRec.Available != ""
        {
            
                for i in 0..<sortby_imggray.count
                {
                    if i == 3{
                        sortby.append(sortby_imgcolor[2])
                        
                        
                    }
                    else{
                        
                        sortby.append(sortby_imggray[i])
                    }
                }
                sortby_collection.reloadData()
        }
        
        
        
        
        self.sortby_collection.reloadData()
        self.GetBadgeList()

         configureLabelSlider()
        
        
        
    
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
      //  updateSliderLabels()
       }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return sortby_imggray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("customcell", forIndexPath: indexPath) as! CustomFilterCollectionViewCell
        if sortby.count<1{
        cell.custom_img.image = UIImage(named: sortby_imggray[indexPath.item])
        cell.custom_lbl.text = sortBy_lblArray[indexPath.item]
        }
        else{
            cell.custom_img.image = UIImage(named: sortby[indexPath.item])
            cell.custom_lbl.text = sortBy_lblArray[indexPath.item]
            
        }
        return cell
    }
    
    
    func GetBadgeList() {
        self.showProgress()
        let param:Dictionary=["":""]
        urlHandler.makeCall(constant.GetBadgeList, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            dispatch_async(dispatch_get_main_queue(),{
                 self.DismissProgress()
                if(error != nil){
                    //self.settablebackground()
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }  else {
                    if(responseObject != nil) {
                        self.Badgesarray = NSMutableArray()
                        let dict:NSDictionary=responseObject!
                        let Status:NSString?=themes.CheckNullValue(dict.objectForKey("status"))!
                        if(Status != nil){
                            if(Status! == "1")  {
                                
                               
                                let BdageArray:NSArray=responseObject?.objectForKey("response")! as! NSArray
                                for Dictionary in BdageArray{
                                    
                            
                                    let badge_id:String=themes.CheckNullValue(Dictionary.objectForKey("_id"))!
                                    let badge_name:String=themes.CheckNullValue(Dictionary.objectForKey("name"))!
                                    let badge_image: String = themes.CheckNullValue(Dictionary.objectForKey("image"))!
                                  
                                    
                                    
                                    let Rec : BadgeRec = BadgeRec.init(badgeid: badge_id, badgename: badge_name,badgeimage:badge_image,upstatus: "0")
                                    self.Badgesarray.addObject(Rec)
                                }
                                
                                
                                
                                
                                for i in 0 ..< FilterRec.SelectedBadgeArray.count {
                                    
                                    let maerialDet = self.Badgesarray.objectAtIndex(i) as! BadgeRec
                                    if maerialDet.badge_id == FilterRec.SelectedBadgeArray.objectAtIndex(i) as! String
                                    {
                                        
                                        let getRec = self.Badgesarray.objectAtIndex(i) as! BadgeRec
                                        let updateRec : BadgeRec = BadgeRec.init(badgeid: getRec.badge_id, badgename: getRec.badge_name,badgeimage:getRec.badge_image,upstatus: "1")
                                        self.Badgesarray.replaceObjectAtIndex(i, withObject: updateRec)
                                        
                                     
                                    }
                                
                                }

                                self.badge_tableView.reloadData()
                               self.AdjustHeight()
                                
                                
                            } else {
                                if (responseObject?.objectForKey("response") != nil) {
                                    let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                    themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                                }
                            }
                        }else {
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                    }else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
        }
    }
    
    func AdjustHeight ()
    {
 badgetableheightconst.constant =  badge_tableView.contentSize.height
  
        
    }
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float){
        self.rating_lbl.text = "\(rating)"
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recrated.
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        sortingVal = sortBy_lblArray[indexPath.item]
        sortby.removeAll()
        for i in 0..<sortby_imggray.count
        {
            if i == indexPath.item{
               sortby.append(sortby_imgcolor[indexPath.item])
                
                
            }
            else{
                
                sortby.append(sortby_imggray[i])
            }
        }
        self.sortby_collection.reloadData()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return Badgesarray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! BadgeTableViewCell
        
        let getRec = Badgesarray.objectAtIndex(indexPath.row) as! BadgeRec
  
        cell.badge_lbl.text = getRec.badge_name
        cell.badge_lbl.sizeToFit()
        
        cell.imgbatch.sd_setImageWithURL(NSURL.init(string: "\(getRec.badge_image)"), completed: themes.block)
       
            if getRec.update_status == "1"{
                cell.badge_image.image = UIImage(named: "custom_dotCircle")
            }
            else 
            {
                cell.badge_image.image = UIImage(named: "custom_Circle")
            }
        
        
        
        return cell
    
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
       
        let getRec = Badgesarray.objectAtIndex(indexPath.row) as! BadgeRec
        if getRec.update_status == "0"
        {        let updateRec : BadgeRec = BadgeRec.init(badgeid: getRec.badge_id, badgename: getRec.badge_name,badgeimage:getRec.badge_image,upstatus: "1")
            Badgesarray.replaceObjectAtIndex(indexPath.row, withObject: updateRec)
            
        }
        else{
            let updateRec : BadgeRec = BadgeRec.init(badgeid: getRec.badge_id, badgename: getRec.badge_name,badgeimage:getRec.badge_image,upstatus: "0")
            Badgesarray.replaceObjectAtIndex(indexPath.row, withObject: updateRec)
        }


        badge_tableView.reloadData()
        
        
        
        
    }
    
    func updateSliderLabels() {
         var lowerCenter: CGPoint = CGPoint()
        lowerCenter.x = (self.lableSlider.lowerCenter.x + self.lableSlider.frame.origin.x)
         lowerCenter.y = (self.lableSlider.center.y - 30.0)
         self.lowerLabel.center = lowerCenter
         self.lowerLabel.text! = "\(Int(self.lableSlider.lowerValue))"
            var upperCenter: CGPoint = CGPoint()
            upperCenter.x = (self.lableSlider.upperCenter.x + self.lableSlider.frame.origin.x)
            upperCenter.y = (self.lableSlider.center.y - 30.0)
            self.upperLabel.center = upperCenter
        

        self.upperLabel.text! = "\(Int(self.lableSlider.upperValue))"
        
       
        
        
        getmaxRadius = self.upperLabel.text!
        
    }

    

    func configureLabelSlider() {
   
     //   NSLog ("minimum price=%f and maximum price =%f",minimumpriceval.floatValue,maximumpriceval.floatValue)
        self.lableSlider.minimumValue = 0;
     self.lableSlider.maximumValue = maximumpriceval.floatValue;
        
        
        self.lableSlider.lowerValue = 0;
//        if FilterRec.MaxradiusRange != ""
//        {
//            self.lableSlider.upperValue = FilterRec.MaxradiusRange.floatValue;
//            self.upperLabel.text! = "\(Int(FilterRec.MaxradiusRange.floatValue))"
//        }
//        else
//        {
            self.lableSlider.upperValue = maximumpriceval.floatValue;
            self.upperLabel.text! = "\(Int(maximumpriceval.floatValue))"
            
       // }
        self.lowerLabel.text! = "\(Int(minimumpriceval.floatValue))"
      
        self.lableSlider.minimumRange = 10;
        
//        if FilterRec.MaxradiusRange != ""
//        {
//            let getCGPoint = FilterRec.Slidercg as CGPoint
//            print(getCGPoint.x)
//            upperLbl_TrailingConstrnt.constant = getCGPoint.x+5
//           // self.upperLabel.frame = CGRectMake(getCGPoint.x, self.upperLabel.frame.origin.y, self.upperLabel.frame.size.width, self.upperLabel.frame.size.height)
//            //self.lowerLabel.frame = CGRectMake(getCGPoint.x, self.upperLabel.frame.origin.y, self.upperLabel.frame.size.width, self.upperLabel.frame.size.height)
//            
//        }
    }
    

    @IBAction func DidClickOption(sender: AnyObject) {
        self.delegate?.pressCancel(self)
    }
    
    
    @IBAction func DidclickReset(sender: AnyObject) {
        starRating.rating = 0
        sortby.removeAll()
      sortby_collection.reloadData()
        self.GetBadgeList()
        self.minimumpriceval = "0"
        self.maximumpriceval = "\(cusProviderRec.GetMax_radius)"
        self.rating_lbl.text = "0.0"
        
         FilterRec = SaveFilterRecords()

        self.configureLabelSlider()
        self.lowerLabel.frame = CGRectMake(self.view.frame.origin.x+5, self.upperLabel.frame.origin.y, self.upperLabel.frame.size.width, self.upperLabel.frame.size.height)
        self.upperLabel.frame = CGRectMake(self.view.frame.size.width-30, self.lowerLabel.frame.origin.y, self.lowerLabel.frame.size.width, self.lowerLabel.frame.size.height)

        
    }
    

    @IBAction func apply_act(sender: AnyObject) {
       
        let dictRecord = NSMutableDictionary()
        FilterRec.SelectedBadgeArray = NSMutableArray()
        for i in 0 ..< Badgesarray.count {
            
            let maerialDet = Badgesarray.objectAtIndex(i) as! BadgeRec
            if maerialDet.update_status == "1"
            {
            dictRecord["badges[\(i)]"] = maerialDet.badge_id
                FilterRec.SelectedBadgeArray.addObject(maerialDet.badge_id)
            }
            else {
                FilterRec.SelectedBadgeArray.addObject("")
            }
           
        }
        

       

       
        
        var  pricevalue : String = String()
        var toprate : String = String ()
        var avialble: String = String()
       
        if sortingVal == "Low To High"
        {
            pricevalue = "lowtohigh"
            
        }
        else if sortingVal == "High To Low"
        {
            pricevalue = "hightolow"
        }
        else if sortingVal == "Available First"
        {
            avialble = "avialable"
        }
        else if sortingVal == "Top Rate"
        {
            toprate = "1"
        }
        
        
        
        
        if toprate == "1"
        {
            
        }
        else{
            toprate = "0"
        }
        
        
        if FilterRec.pricerange != "" || pricevalue == ""
        {
            
            pricevalue = FilterRec.pricerange as String
        }
        
        if FilterRec.Toprating != "" || toprate == ""
        {
            toprate = FilterRec.Toprating as String
        }
        
        
        
        
        
        dictRecord["user_id"] = "\(themes.getUserID())"
        dictRecord ["lat"] = "\(cusProviderRec.searchlat)"
        dictRecord ["long"] = "\(cusProviderRec.searchlng)"
        dictRecord ["category"] = "\(cusProviderRec.SubCatid)"
        dictRecord["maincategory"] = cusProviderRec.MainCatid
    
        dictRecord ["rating"] = "\(self.rating_lbl.text!)"
        dictRecord ["kmmaxvalue"] = "\(getmaxRadius)"
        dictRecord ["price"] = "\(pricevalue)"
        dictRecord ["toprated"] = "\(toprate)"
       
        
        FilterRec.pricerange = pricevalue
        FilterRec.Toprating = toprate
        FilterRec.MaxradiusRange = getmaxRadius
        FilterRec.Ratingval = self.rating_lbl.text!
        FilterRec.Slidercg = self.lableSlider.upperCenter
        FilterRec.Available = avialble
   
        
        self.delegate?.pressCancel(self)
        
        
        self.delegate?.passRequiredParametres(dictRecord)
        
    }
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
