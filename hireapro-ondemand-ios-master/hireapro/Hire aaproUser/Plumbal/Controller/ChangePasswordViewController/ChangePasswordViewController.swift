//
//  ChangePasswordViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 12/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class ChangePasswordViewController: RootViewController {

    @IBOutlet var Done_Btn: UIButton!
    @IBOutlet var Change_Pass_ScrollView: UIScrollView!
    
    @IBOutlet var old_TextField: UITextField!

    @IBOutlet var New_TextField: UITextField!
    @IBOutlet var Confirm_textField: UITextField!
    
    @IBOutlet var Back_But: UILabel!
    
  var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate
    
    let themes:Themes=Themes()
    
    var URL_handler:URLhandler=URLhandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        old_TextField.placeholder=themes.setLang("old_password")
        New_TextField.placeholder=themes.setLang("confirm_password")
        Confirm_textField.placeholder=themes.setLang("new_password")
        Done_Btn.setTitle(themes.setLang("done"), forState: UIControlState.Normal)
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        Back_But.text = themes.setLang("change_password")
old_TextField.delegate=self
Confirm_textField.delegate=self
New_TextField.delegate=self
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(ChangePasswordViewController.DismissKeyboard(_:)))
        view.addGestureRecognizer(tapgesture)
    }
    
    func DismissKeyboard(sender:UITapGestureRecognizer) {
        
        
        
        
        view.endEditing(true)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        // Dispose of any resources that can be recreated.
    }
    
    
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        //        themes.setLang(
        
        //        themes.setLang("Full Name",comment: nil)
        
        
        

        
    }

    
    
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
              
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == old_TextField)
        {
            old_TextField.resignFirstResponder()
            New_TextField.becomeFirstResponder()
        }
        if(textField == New_TextField)
        {
            New_TextField.resignFirstResponder()
            Confirm_textField.becomeFirstResponder()
        }
        if(textField == Confirm_textField)
        {
            Confirm_textField.resignFirstResponder()
         }
            return true

    }
    
    
    func changeName()
    {
        
    }
    
    
    
    @IBAction func didClickoptions(sender: AnyObject) {
        
        if(sender.tag == 0)
        {
            
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        }
        if(sender.tag == 1)
        {
            let Password : NSString = New_TextField.text!
            
            if(old_TextField.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("Kindly Enter the old Password Field"), ButtonTitle: self.themes.setLang("ok"))
            }
            else if(New_TextField.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("Kindly Enter the New Password Field"), ButtonTitle: self.themes.setLang("ok"))

            }
            else if(Password.length < 6 )
            {
                themes.AlertView("\(Appname)",Message: themes.setLang("Password must be atleast 6 characters."),ButtonTitle: self.themes.setLang("ok"))
            }

                
            else if (Confirm_textField.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("Kindly Enter the Confirm Password Field"), ButtonTitle: self.themes.setLang("ok"))

            }
                
                
                else if(New_TextField.text != Confirm_textField.text)
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("Password doesn't match"), ButtonTitle: self.themes.setLang("ok"))

            }
            else
            {
                self.Done_Btn.enabled=false
                self.showProgress()

                
                let param=["user_id":"\(themes.getUserID())","password":"\(old_TextField.text!)","new_password":"\(New_TextField.text!)"]
                
                URL_handler.makeCall(constant.Change_pass.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), param: param, completionHandler: { (responseObject, error) -> () in
                    self.Done_Btn.enabled=true
                    self.DismissProgress()

                    
                    if(error != nil)
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

                       // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                    }
                        
                    else
                    {
                        

                    
                    if(responseObject != nil)
                    {
                    let dict:NSDictionary=responseObject!
                    
                    
                    Changepass.status=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    Changepass.response=dict.objectForKey("response") as! NSString

                    
                    if(Changepass.status == "1")
                    {
                        self.themes.AlertView("Hurray", Message: "\(Changepass.response)", ButtonTitle: self.themes.setLang("ok"))

                        
                        self.Appdel.CheckDisconnect()
                        
                        
                        SocketIOManager.sharedInstance.LeaveRoom(self.themes.getUserID())
                        
                        
                        SocketIOManager.sharedInstance.LeaveChatRoom(self.themes.getUserID())
                        
                        
                        SocketIOManager.sharedInstance.RemoveAllListener();
                        
                        dbfileobj.deleteUser("Provider_Table")
                        
                        
                        let appDomain: String = NSBundle.mainBundle().bundleIdentifier!
                       // NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                        //        themes.saveJaberID("5630701bcae2aaa80700002c@casp83")
                        //        themes.saveJaberPassword("4b01f338bf8d22a55299b369dc3a8287")
                   
                        self.Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")



                    }
                    else
                    {
                        
                        
                        self.themes.AlertView("\(Appname)", Message: "\(Changepass.response)", ButtonTitle: self.themes.setLang("ok"))
 
                    }
                    }
                    else
                    {
                         self.themes.AlertView("\(Appname)", Message: self.themes.setLang("Please Check your Internet Connection"), ButtonTitle: self.themes.setLang("ok"))

                    }
                        
                    }

                    
                
                })
                
             }
            
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChangePasswordViewController:UINavigationControllerDelegate
{
    
}
extension ChangePasswordViewController:UITextFieldDelegate
{
    
}
