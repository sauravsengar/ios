//
//  CategorySearchViewController.swift
//  Plumbal
//
//  Created by Natarajan on 20/07/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit
protocol CategorySearchDelegate {
    func searchCategory(id:String,category:String,slug:String,parentID:String)

}
class CategorySearchViewController: RootViewController,UITextFieldDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var searchBar: UISearchBar!
    var urlHandler = URLhandler()
    var search_CategoryArray:NSMutableArray = NSMutableArray()

    @IBOutlet var title_lbl: CustomLabelWhite!
    @IBOutlet var category_tableview: UITableView!
    var searchArray = [String]()
    var searchCat = [String]()
    var delegate:CategorySearchDelegate!
     var refreshControl:UIRefreshControl=UIRefreshControl()
    var themes = Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
//self.getDetails()
        title_lbl.text = self.themes.setLang("category_search")
        
        self.category_tableview.delegate = self
        self.category_tableview.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    @IBAction func DidclickbackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func getDetails()
        
    {
       self.showProgress()
        let param: NSDictionary = [:]
        urlHandler.makeCall(constant.categorySearch, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
            }
                
            else
            {
                if(responseObject != nil)
                {
                    
                    let dict:NSDictionary=responseObject!
                    let Status:NSString = self.themes.CheckNullValue(dict.objectForKey("status"))!
                    let response:NSArray=dict.objectForKey("response") as! NSArray
                    
                    if  Status == "1"{
                        self.search_CategoryArray.removeAllObjects()
                        self.searchArray.removeAll()
                        self.searchCat.removeAll()

                    for value in response
                    {
                        var checkParent_id:String
                        let category = value as! NSDictionary
    
                        let cat_value = category.valueForKey("category") as! String
                        let check = category.valueForKey("parent") as? String
                        if check != nil {
                        checkParent_id = check!
                        }
                        else{
                            checkParent_id = ""
                        }
                        
                        let rec = SearchCategory(id: category.valueForKey("_id") as! String, category: category.valueForKey("category") as! String, slugname: category.valueForKey("slug") as! String, parentid: checkParent_id)
                        self.searchArray.append(cat_value)
                        
                        self.search_CategoryArray.addObject(rec)
                    
                    }
                    for searchVal in self.searchArray{
                    self.searchCat.append(searchVal)
                    }
                   
                    
//                    if (Status == "1")
//                    {
//                        
//                    }
//                    else
//                    {
//                        self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: "Ok")
//                        
//                    }
                }
                    else{
                    self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))

                    }
                    
                }
            }
        })
        self.category_tableview.reloadData()
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print(searchArray.count)
        return searchArray.count
        
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        let cell = category_tableview.dequeueReusableCellWithIdentifier("Search_CatVCD") as!  SearchCategoryTableViewCell
        
        cell.textLabel!.text = searchArray[indexPath.row] 
    return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let selectedCategory = searchArray[indexPath.row] 
        let index_value = searchCat.indexOf(selectedCategory)!
        let objRec:SearchCategory=self.search_CategoryArray.objectAtIndex(index_value) as! SearchCategory
   
        let Controller:InitialViewController=self.storyboard?.instantiateViewControllerWithIdentifier("InitialViewControllerID") as! InitialViewController
        print(Controller.id)
        print(Controller.category)
//         Controller.id = objRec.cat_id as! String
//        Controller.category = objRec.cat as! String
//        Controller.slug = objRec.slug as! String
//        Controller.parentid = objRec.parent_id as! String
      //  Controller.statusValue = "1"
        
        if selectedCategory ==  objRec.slug{
            if objRec.parent_id == ""{
            callSearch_Cat("\(objRec.cat_id)", Search_Word: selectedCategory)
            }
            else{
                callSearch_Cat("\(objRec.parent_id)", Search_Word: selectedCategory)
              }
        }
        else{
             callSearch_Cat("\(objRec.cat_id)", Search_Word: selectedCategory)
        }
//        if objRec.parent_id == ""{
//          callSearch_Cat("\(objRec.cat_id)", Search_Word: searchBar.text!)
//        }
//        else{
//             callSearch_Cat("\(objRec.parent_id)", Search_Word: searchBar.text!)
//        }
        
        delegate.searchCategory(objRec.cat_id, category: objRec.cat, slug: objRec.slug, parentID:   objRec.parent_id)
//        if Controller.id == objRec.cat_id{
//            Controller.catBtn.setTitle(objRec.cat, forState: UIControlState.Normal)
//        }
//        else if Controller.parentid == objRec.parent_id
//        {
//            Controller.catBtn.setTitle(objRec.slug, forState: UIControlState.Normal)
//        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    //MARK: - SearchBar Delegate
    
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        self.searchArray.removeAll(keepCapacity: false)
        if text != ""{
        
        let NewText = (searchBar.text! as NSString).stringByReplacingCharactersInRange(range, withString:text)
        print(NewText);
        let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
        var searchString = String()
        (NewText as String).enumerateSubstringsInRange(range, options: .ByComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
            searchString.appendContentsOf(substring!)
            searchString.appendContentsOf("*")
        })
        let searchPredicate = NSPredicate(format: "SELF LIKE[c] %@", searchString)
        let array = (searchCat as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.searchArray = array as! [String]
        category_tableview.reloadData()
        category_tableview.hidden = false
        }
        else{
            getDetails()
        }
        return true
        
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar!)
    {
        print("dfg")

    callSearch_Cat("", Search_Word: searchBar.text!)
   
    }
    
    
    func callSearch_Cat(cat_ID:String,Search_Word:String){
        
        //        keyword:
        //        category:
        //        user:
            let param:Dictionary=["keyword":"\(Search_Word)","category":"\(cat_ID)","user":"\(themes.getUserID())"]
                urlHandler.makeCall(constant.search_Keyword, param: param) { (responseObject, error) -> () in
                    self.DismissProgress()
                    self.refreshControl.endRefreshing()
                    dispatch_async(dispatch_get_main_queue(),{
        
                        if(error != nil){
                            //self.settablebackground()
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }  else {
                            if(responseObject != nil) {
                                let dict:NSDictionary=responseObject!
                                let Status:NSString?=self.themes.CheckNullValue(dict.objectForKey("status"))!
                                if(Status != nil){
                                    if(Status! == "1")  {
        
                                        //  self.Home_tableView.backgroundView=nil
                                    } else {
                                        //self.settablebackground()
                                        if (responseObject?.objectForKey("response") != nil) {
                                            let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                            self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                                        }
                                    }
                                }else {
                                    // self.settablebackground()
                                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                                }
                            }else {
                                //self.settablebackground()
                                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                            }
                        }
                    })
                }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLayoutSubviews() {
    category_tableview.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        getDetails()
    }

}
