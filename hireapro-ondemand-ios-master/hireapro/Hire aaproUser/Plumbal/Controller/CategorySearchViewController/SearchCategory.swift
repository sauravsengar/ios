//
//  SearchCategory.swift
//  Plumbal
//
//  Created by Natarajan on 20/07/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class SearchCategory: NSObject {
    
    
     var cat_id:String = ""
    var cat:String = ""
    var cat_icon = ""
    
    var slug:String = ""
    var parent_id:String = ""
    
    
    init(id:String,category:String,Catimg:String) {
        cat_id = id
        cat = category
        cat_icon = Catimg
        super.init()
        
    }

    init(id:String,category:String,slugname:String,parentid:String) {
     cat_id = id
     cat = category
     slug = slugname
  parent_id = parentid
      super.init()
        
    }
    

}
