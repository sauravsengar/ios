//
//  ProviderDetailViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 12/01/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class ProviderDetailViewController: RootViewController {
    @IBOutlet var WrapperView:UIView!
    @IBOutlet var Message_Btn:UIButton!
    @IBOutlet var call_Btn:UIButton!
    @IBOutlet var Provider_tableView:UITableView!

    @IBOutlet var backbtn: UIButton!
    @IBOutlet var Header_btn: UIButton!
    @IBOutlet var Detail_Lab:UILabel!
    
    
    let URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Message_Btn.setTitle(themes.setLang("message"), forState: UIControlState.Normal)
        call_Btn.setTitle(themes.setLang("call"), forState: UIControlState.Normal)
        Header_btn.setTitle(themes.setLang("header"), forState: UIControlState.Normal)
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)

         let Parallax_HeaderView:ParallaxHeaderView=ParallaxHeaderView.parallaxHeaderViewWithImage(UIImage(named: "Register-bg"), forSize: CGSizeMake(self.Provider_tableView.frame.size.width, 175)) as! ParallaxHeaderView
        self.Provider_tableView.tableHeaderView=Parallax_HeaderView
         let header: ParallaxHeaderView = self.Provider_tableView.tableHeaderView as! ParallaxHeaderView
        header.refreshBlurViewForNewImage()
        self.Provider_tableView.tableHeaderView = header
         Get_Data()
         let Nb=UINib(nibName: "ProviderInfoTableViewCell", bundle: nil)
         Provider_tableView.registerNib(Nb, forCellReuseIdentifier: "InfoCell")
         let Nb1=UINib(nibName: "ProviderDetailTableViewCell", bundle: nil)
         Provider_tableView.registerNib(Nb1, forCellReuseIdentifier: "DetailCell")
         Provider_tableView.hidden=true
        Provider_tableView.rowHeight = UITableViewAutomaticDimension
        // WrapperView.layer.borderWidth=1.0
         // WrapperView.layer.borderColor=themes.ThemeColour().CGColor
         // Do any additional setup after loading the view.
    }
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        //
        //         for cell in self.Category_tableView.visibleCells as! [CategoryTableViewCell] {
        //
        //
        //
        //            let rectInSuperview: CGRect = Category_tableView.convertRect(cell.frame, toView: view)
        //            let distanceFromCenter: CGFloat = CGRectGetHeight(view.frame) / 2 - CGRectGetMinY(rectInSuperview)
        //            let difference: CGFloat = 183 - CGRectGetHeight(cell.frame)
        //            let move: CGFloat = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference
        //
        //            imageRect=cell.Category_ImageView.frame
        //
        //            imageRect.origin.y = -(difference / 2) + move
        //
        //
        //             cell.Category_ImageView.frame = imageRect
        //        }
        
        
                 let header: ParallaxHeaderView = self.Provider_tableView.tableHeaderView as! ParallaxHeaderView
                header.layoutHeaderViewForScrollViewOffset(scrollView.contentOffset)
    }
    
    func Get_Data()
    {
        self.showProgress()
        
        let param=["user_id":"\(themes.getUserID())","provider_id":"\(OrderDetail_data.provider_id)"]
        URL_handler.makeCall(constant.Get_ProviderInfo, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

               // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
                
            else
            {
                
                
                if(responseObject != nil)
                {
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                     if (Staus == "1")
                    {
                        self.Provider_tableView.hidden=false
                        Provider_Detail.provider_name=dict.objectForKey("response")!.objectForKey("provider_name") as! NSString
                        Provider_Detail.email=dict.objectForKey("response")!.objectForKey("email") as! NSString
                        //Provider_Detail.bio=dict.objectForKey("response")!.objectForKey("bio") as! NSString
                        Provider_Detail.category1=dict.objectForKey("response")!.objectForKey("category") as! NSMutableArray
                        
                        let avgInt : Float = dict.objectForKey("response")!.objectForKey("avg_review") as! Float
                        let strAverageReview = "\(avgInt)"
                        Provider_Detail.avg_review=strAverageReview
                         //Provider_Detail.avg_review=dict.objectForKey("response")!.objectForKey("avg_review") as! NSString
                        Provider_Detail.mobile_number=dict.objectForKey("response")!.objectForKey("mobile_number") as! NSString
                        Provider_Detail.image=dict.objectForKey("response")!.objectForKey("image") as! NSString
                   
                        var tempArray = [String]()
                        
                        for ReasonDict in Provider_Detail.category1
                        {
                            let Reason_Str:NSString=ReasonDict.objectForKey("name") as! NSString
                            tempArray.append(Reason_Str as String)
                        }
                        
                        Provider_Detail.category = String()
                        Provider_Detail.category = tempArray.joinWithSeparator(", ")
                        
                      
                            Provider_Detail.Complete_Detail="Category : \(Provider_Detail.category)\n"
                        print("..get information.....\(Provider_Detail.Complete_Detail).........")
                        

                                               self.Provider_tableView.reloadData()
                        
                    }
                    else
                    {
                        self.themes.AlertView("\(Appname)",Message: self.themes.setLang("no_provider_detail"),ButtonTitle: kOk)
                        self.dismissViewControllerAnimated(true, completion: nil)
                         self.navigationController?.popViewControllerAnimated(true)

                    }
                    
                }
                else
                {
                    self.DismissProgress()
                    
                    self.themes.AlertView("\(Appname)",Message: self.themes.setLang("Please try again"),ButtonTitle: self.themes.setLang("ok"))
                    
                    
                    
                }
                
            }
  
        }
    }
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        var height_Cell:CGFloat=CGFloat()
       
        
        height_Cell = self.themes.calculateHeightForString("\((Provider_Detail.Complete_Detail as String) + (Provider_Detail.bio as String))")
        
            if(indexPath.row == 0)
            {
                
                
                    height_Cell = 90
                
            }
        else
            {
                height_Cell = height_Cell + 120
        }
            
            
            
        
        
        

        
        
        //         self.Height_Return(height_Cell)
        return height_Cell
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var Cell:UITableViewCell=UITableViewCell()
        if(Provider_Detail.provider_name != "")
        {
    
        if(indexPath.row == 0)
        {
           let Cell1 = tableView.dequeueReusableCellWithIdentifier("InfoCell") as! ProviderInfoTableViewCell
            Cell1.selectionStyle=UITableViewCellSelectionStyle.None

            
            let n = NSNumberFormatter().numberFromString(Provider_Detail.avg_review as String)
            
             Cell1.ratingView.emptySelectedImage = UIImage(named: "Star")
            Cell1.ratingView.fullSelectedImage = UIImage(named: "StarSelected")
            Cell1.ratingView.contentMode = UIViewContentMode.ScaleAspectFill
            Cell1.ratingView.maxRating = 5
            Cell1.ratingView.minRating = 1
            Cell1.ratingView.rating = CGFloat(n!)
            Cell1.ratingView.editable = false;
            Cell1.ratingView.halfRatings = true;
            Cell1.ratingView.floatRatings = false;

            Cell1.Name_Lab.text=Provider_Detail.provider_name as String
            Cell1.Contact_Lab.text=Provider_Detail.mobile_number as String
            Cell1.Email_Lab.text=Provider_Detail.email as String
            
            Cell1.Provider_Image.sd_setImageWithURL(NSURL(string: "\(Provider_Detail.image)"), completed: themes.block)
            Cell1.Provider_Image.layer.borderWidth=5.0
            
            Cell1.Provider_Image.layer.cornerRadius=Cell1.Provider_Image.frame.size.width/2
            
            Cell1.Provider_Image.clipsToBounds=true
            Cell1.Provider_Image.layer.borderColor=themes.ThemeColour().CGColor

 
         Cell=Cell1
        }
        else
        {
            let Cell2 = tableView.dequeueReusableCellWithIdentifier("DetailCell") as! ProviderDetailTableViewCell
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: Provider_Detail.Complete_Detail as String, attributes: nil)
            myMutableString.addAttribute(NSFontAttributeName, value:UIFont.boldSystemFontOfSize(16) , range: NSRange(location:1,length:9))
            
            var myMutableString1 = NSMutableAttributedString()
            myMutableString1 = NSMutableAttributedString(string:"Bio : \(Provider_Detail.bio)", attributes: nil)
            myMutableString1.addAttribute(NSFontAttributeName, value:UIFont.boldSystemFontOfSize(16) , range: NSRange(location:1,length:4))
           // Cell2.bio_Lab.attributedText = myMutableString1
           // Cell2.bio_Lab.sizeToFit()
            Cell2.About_Lab.attributedText = myMutableString
            Cell2.About_Lab.sizeToFit()
            Cell2.selectionStyle=UITableViewCellSelectionStyle.None

            Cell=Cell2


        }
        }
        
        
        return Cell
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func didClickoption(sender: UIButton) {
        if(sender.tag == 0)
        {
         self.dismissViewControllerAnimated(true, completion: nil)
         self.navigationController?.popViewControllerAnimated(true)
        }
        if(sender.tag == 1)
        {
            let Storyboard:UIStoryboard=UIStoryboard(name: "Main", bundle: nil)
            let vc = Storyboard.instantiateViewControllerWithIdentifier("MessageVC")
            self.presentViewController(vc, animated: true, completion: nil)
            
            //let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
           // self.navigationController?.pushViewController(secondViewController, animated: true)


//            if (themes.canSendText()) {
//                // Obtain a configured MFMessageComposeViewController
//                let messageComposeVC = themes.configuredMessageComposeViewController("",number:"\(OrderDetail_data.provider_mobile)")
//                presentViewController(messageComposeVC, animated: true, completion: nil)
//            } else {
//                // Let the user know if his/her device isn't able to send text messages
//                let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
//                errorAlert.show()
//            }

        }

        
        if(sender.tag == 2)
        {
            UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(OrderDetail_data.provider_mobile)")!)

         }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
