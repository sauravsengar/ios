





//
//  SubCatlistViewController.swift
//  Plumbal
//
//  Created by Casperon on 21/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class SubCatlistViewController: RootViewController,UISearchBarDelegate {
    @IBOutlet var Subcategory_table: UITableView!
    @IBOutlet var subsearchCat: UISearchBar!
    @IBOutlet var map_view_btn: UIButton!
    var catid : String!
    var urlHandler = URLhandler()
    var seachtag : NSInteger!
    var  search_range : NSRange!
    var CategoryArray:NSMutableArray = NSMutableArray()
    
    @IBOutlet var title_lbl: CustomLabelWhite!
    @IBOutlet var mapviewbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map_view_btn.setTitle(themes.setLang("map_view"), forState: .Normal)
         title_lbl.text = themes.setLang("\(Appname)")
        let cellnib = UINib.init(nibName: "CategoryCellTableViewCell", bundle: nil)
        self.Subcategory_table.registerNib(cellnib, forCellReuseIdentifier: "categorycell")
        self.Subcategory_table.estimatedRowHeight = 45
        self.Subcategory_table.rowHeight = UITableViewAutomaticDimension
        self.Subcategory_table.tableFooterView=UIView()

        subsearchCat.delegate = self
  subsearchCat.backgroundColor = UIColor.clearColor()
        subsearchCat.layer.cornerRadius = 7.0
        subsearchCat.clipsToBounds = true
        mapviewbtn.layer.cornerRadius = 7.0
        mapviewbtn.clipsToBounds = true

        self.showProgress()
        self.Home_Datafeed()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didclickBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func DidclickMap(sender:AnyObject)
    {
        Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "HomePageVCID")
        
    }
    func Home_Datafeed() {
        let param:Dictionary=["location_id":"\(themes.getLocationID())","category":"\(catid)"]
        urlHandler.makeCall(constant.Get_Categories, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            dispatch_async(dispatch_get_main_queue(),{
                
                if(error != nil){
                    //self.settablebackground()
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }  else {
                    if(responseObject != nil) {
                        let dict:NSDictionary=responseObject!
                        let Status:NSString?=themes.CheckNullValue(dict.objectForKey("status"))!
                        if(Status != nil){
                            if(Status! == "1")  {
                                
                                if(self.CategoryArray.count != 0) {
                                    self.EmptyArray()
                                }
                                let CategoryArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("category") as! NSArray
                                for Dictionary in CategoryArray{
                                    let categoryid1:String=themes.CheckNullValue(Dictionary.objectForKey("cat_id"))!
                                    let categoryimage1:String=themes.CheckNullValue(Dictionary.objectForKey("active_icon"))!
                                    let categoryname1:String=themes.CheckNullValue(Dictionary.objectForKey("cat_name"))!
                                    
                                    let Rec : SearchCategory = SearchCategory.init(id: categoryid1 , category: categoryname1 , Catimg: categoryimage1)
                                    self.CategoryArray.addObject(Rec)
                                }
                                
                                self.Subcategory_table.reloadData()
                            } else {
                                if (responseObject?.objectForKey("response") != nil) {
                                    let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                    themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                                }
                            }
                        }else {
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                    }else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
        }
    }
    
    func  EmptyArray() -> () {
        self.CategoryArray.removeAllObjects()
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CategoryArray.count > 0{
            return self.CategoryArray.count
            
        }
        else{
            return 0
            
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("categorycell") as! CategoryCellTableViewCell
        Cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        Cell.selectionStyle = .None
        Cell.loadCategoryTableCell(CategoryArray.objectAtIndex(indexPath.row) as! SearchCategory)
        return Cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let SubCatRec = self.CategoryArray.objectAtIndex(indexPath.row) as! SearchCategory
        cusProviderRec.SubCatid = SubCatRec.cat_id
        cusProviderRec.SubCatname = SubCatRec.cat
        
        self.GetProviderlist()
        
    }
    
    func GetProviderlist ()  {
        
        self.showProgress()
        
        var param = NSDictionary()
        param=["user_id":"\(themes.getUserID())","lat":"\(cusProviderRec.searchlat)","long":"\(cusProviderRec.searchlng)","category":"\(cusProviderRec.SubCatid)"]
        urlHandler.makeCall(constant.Map_Providers, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }  else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString?=themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    if(Status! == "1")  {
                        var taskId = String()
                        
                        taskId=themes.CheckNullValue(dict.objectForKey("task_id"))!
                        cusProviderRec.Task_id = taskId
                         cusProviderRec.GetMax_radius = themes.CheckNullValue(dict.objectForKey("maximum_radius_value"))!
                         let providerList = dict.objectForKey("response") as! NSArray
                        self.EmptycustomProArray()
                        
                        for element in providerList
                        {
                            let taskername:String = themes.CheckNullValue(element.objectForKey("name"))!
                            cusProviderRec.ProvidersNameArray.addObject(taskername)
                            let taskerid:String = themes.CheckNullValue(element.objectForKey("taskerid"))!
                            cusProviderRec.ProvideridArray.addObject(taskerid)
                            let distance:String = themes.CheckNullValue(element.objectForKey("radius"))!
                            cusProviderRec.ProRadiusArray.addObject(distance)
                            let taskamount:String = themes.CheckNullValue(element.objectForKey("hourly_amount"))!
                            let flatamount : String = themes.CheckNullValue(element.objectForKey("flat_amount"))!
                            if taskamount == ""
                            {
                             cusProviderRec.ProAmountArray.addObject(flatamount)
                                cusProviderRec.Cat_type = "Flat"
                            }else{
                               cusProviderRec.ProAmountArray.addObject(taskamount)
                                cusProviderRec.Cat_type = "Hourly"
                            }
                            
                            
                      
                            let provrating:String = themes.CheckNullValue(element.objectForKey("rating"))!
                            cusProviderRec.ProRatingArray.addObject(provrating)
                            let taskerimg : String = themes.CheckNullValue(element.objectForKey("image_url"))!
                            cusProviderRec.ProviderimageArray.addObject(taskerimg)
                            let servicetax : String = themes.CheckNullValue(element.objectForKey("servixe_tax"))!
                            cusProviderRec.ServiceTaxArray.addObject(servicetax)
                            
                            let cat_type : String = themes.CheckNullValue(element.objectForKey("category_type"))!
                            cusProviderRec.ProvCatTypeArray.addObject(cat_type)
                            
                            let BadgeArray : NSArray = element.objectForKey("badges") as! NSArray
                            cusProviderRec.ProBadgesArray.addObject(BadgeArray)
                            
                        }
                        let controller  = self.storyboard?.instantiateViewControllerWithIdentifier("customprolist") as! CustomProviderlistViewController
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                    else {
                        
                        if (responseObject?.objectForKey("response") != nil) {
                            
                            let Errormsg : String =  themes.CheckNullValue(responseObject?.objectForKey("response"))!
                            
                            if Errormsg == "User ID is Required"
                            {
                                themes.AlertView(themes.setLang("Sorry"), Message: themes.setLang("Please Login To Continue"), ButtonTitle: kOk)

                            }
                            else{
                                themes.AlertView("\(Appname)", Message:Errormsg, ButtonTitle: kOk)

                            }
                            
                        }
                    }
                    
                }else {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }
        
    }
    func EmptycustomProArray()  {
        cusProviderRec.ProvidersNameArray = NSMutableArray()
        cusProviderRec.ProviderimageArray = NSMutableArray()
        cusProviderRec.ProvideridArray    = NSMutableArray()
        cusProviderRec.ProRadiusArray     = NSMutableArray()
        cusProviderRec.ProAmountArray     = NSMutableArray()
        cusProviderRec.ProRatingArray     = NSMutableArray()
        cusProviderRec.ProBadgesArray     = NSMutableArray()
        cusProviderRec.ServiceTaxArray    = NSMutableArray()
        cusProviderRec.ProvCatTypeArray   = NSMutableArray()
        
        
    }

    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text != ""{
            
            let NewText = (searchBar.text! as NSString).stringByReplacingCharactersInRange(range, withString:text)
            print(NewText);
            let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
            var searchString = String()
            (NewText as String).enumerateSubstringsInRange(range, options: .ByComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
                searchString.appendContentsOf(substring!)
                searchString.appendContentsOf("*")
            })
            
            let bPredicate = NSPredicate(format: "SELF.cat LIKE[cd] %@", searchString)
            let  filteredArray  = CategoryArray.filterUsingPredicate(bPredicate)
            
            
           
            
            
            Subcategory_table.reloadData()
            Subcategory_table.hidden = false
        }
        else{
            Home_Datafeed()
        }
        return true
        
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar!)
    {
        
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
