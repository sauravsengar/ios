//
//  AddaddressViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 28/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

import CoreLocation


class AddaddressViewController: RootViewController,CLLocationManagerDelegate {
    
    
    @IBOutlet var Done_Btn: CustomButton!
    var themes:Themes=Themes()
    @IBOutlet weak var lblMbl: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
   
    
    
    
    
    @IBOutlet weak var titlebtn: UIButton!


    @IBOutlet weak var fetchImageView: UIButton!
    @IBOutlet var EnterAdd_Lbl: UILabel!
    @IBOutlet var EditCon_Lbl: UILabel!
    @IBOutlet var Place_Autocomplete_tableview: UITableView!
    @IBOutlet var Addaddress_ScrollView: UIScrollView!
    @IBOutlet var MobileNum_Field: UITextField!
    @IBOutlet var Email_Field: UITextField!
    @IBOutlet var Name_Field: UITextField!
    @IBOutlet var Back_but: UIButton!
    @IBOutlet var HouseNo_Field: UITextField!
    @IBOutlet var Landmark_Field: UITextField!
    @IBOutlet var Locality_Field: UITextField!
    @IBOutlet var City_Field: UITextField!
    @IBOutlet var Pincode_Field: UITextField!
    @IBOutlet var stateField: UITextField!
    @IBOutlet var countryField: UITextField!

    @IBOutlet var CountryCode_Picker: UIPickerView!
    @IBOutlet var Picker_Wrapper: UIView!
    @IBOutlet var Country_Code_Field: UITextField!
    
     var currlatitude : String = String()
    var currlongitude :String  = String()
    
    
    let locationManager = CLLocationManager()
    
    
    var URL_handler:URLhandler=URLhandler()
    
    
    enum PlaceType: CustomStringConvertible {
        case All
        case Geocode
        case Address
        case Establishment
        case Regions
        case Cities
        
        var description : String {
            switch self {
            case .All: return ""
            case .Geocode: return "geocode"
            case .Address: return "address"
            case .Establishment: return "establishment"
            case .Regions: return "regions"
            case .Cities: return "cities"
            }
        }
    }
    
    struct Place {
        let id: String
        let description: String
    }
    
    var places = [Place]()
    var setstatus:Bool=Bool()
    
    var placeType: PlaceType = .All
    
    var State=NSString()
    var country=NSString()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPage()
        //self.locationManager.requestWhenInUseAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        Addaddress_ScrollView.contentSize.height = Done_Btn.frame.origin.y+Done_Btn.frame.height+150
        
        Place_Autocomplete_tableview.hidden=true
        Place_Autocomplete_tableview.tableFooterView=UIView()
        Place_Autocomplete_tableview.layer.borderWidth=1.0
        Place_Autocomplete_tableview.layer.borderColor=themes.Lightgray().CGColor
        Place_Autocomplete_tableview.layer.cornerRadius=5.0
        Name_Field.text=themes.getUserName()
        Email_Field.text=themes.getEmailID()
        MobileNum_Field.text=themes.getMobileNum()
        Country_Code_Field.text=themes.getCountryCode()
        
        
        //Delegate Methods
        
        Name_Field.delegate=self
        Email_Field.delegate=self
        MobileNum_Field.delegate=self
        HouseNo_Field.delegate=self
        Landmark_Field.delegate=self
        Locality_Field.delegate=self
        City_Field.delegate=self
        Pincode_Field.delegate=self
        Country_Code_Field.delegate=self
        stateField.delegate = self
        countryField.delegate = self
        
        Locality_Field.addTarget(self, action: #selector(AddaddressViewController.TextfieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        
//        if(themes.screenSize.height == 480)
//        {
//            Addaddress_ScrollView.contentSize.height=768
//        }
//        if(themes.screenSize.height == 568)
//        {
//            Addaddress_ScrollView.contentSize.height=700
//            
//        }
//        if(themes.screenSize.height == 667)
//        {
//            
//            Addaddress_ScrollView.contentSize.height=700
//        }
//        
//        if(themes.screenSize.height == 736)
//        {
//            
//            Addaddress_ScrollView.contentSize.height=700
//        }
        
        
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(AddaddressViewController.DismissKeyboard(_:)))
        
        tapgesture.delegate=self
        
        view.addGestureRecognizer(tapgesture)
        
        //Tool bar for Pickerview
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.frame.size.width, 25))
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddaddressViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        doneButton.tintColor=themes.ThemeColour()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        Picker_Wrapper.addSubview(toolBar)
        
        Picker_Wrapper.removeFromSuperview()
        
        
        //Set image for backarraow
        
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        Back_but.addSubview(themes.Back_ImageView)
        
        
        Place_Autocomplete_tableview.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
        // Do any additional setup after loading the view.
        
        
        
        
        
        
    }
    
     func applicationLanguageChangeNotification(notification:NSNotification) {
        lblMbl.text = themes.setLang("mobile_no_caps")
        lblName.text = themes.setLang("name_caps")
        lblEmail.text = themes.setLang("email_id_caps")
        titlebtn.setTitle(themes.setLang("add_address_title"), forState: UIControlState.Normal)
        EditCon_Lbl.text=themes.setLang("edit_contact")
        Name_Field.placeholder=themes.setLang("full_name")
        Email_Field.placeholder=themes.setLang("email_id_smal")
        MobileNum_Field.placeholder=themes.setLang("mobile_no_small")
        EnterAdd_Lbl.text=themes.setLang("enter_address")
        Locality_Field.placeholder=themes.setLang("ur_locality")
        HouseNo_Field.placeholder=themes.setLang("house_no")
        City_Field.placeholder=themes.setLang("city")
        Pincode_Field.placeholder=themes.setLang("pincode")
        stateField.placeholder=themes.setLang("State")
        countryField.placeholder=themes.setLang("Country")

        Locality_Field.placeholder = themes.setLang("locality")
        Landmark_Field.placeholder = themes.setLang("landmark")
        Done_Btn.setTitle(themes.setLang("done"), forState: UIControlState.Normal)
    }
    func setPage(){
        setPading(Name_Field, title: themes.setLang("name_caps"))
        setPading(Email_Field, title: themes.setLang("email_id_smal"))
        setPading(MobileNum_Field, title: themes.setLang("mobile_no_small"))

        lblMbl.text = themes.setLang("mobile_no_caps")
        lblName.text = themes.setLang("name_caps")
        lblEmail.text = themes.setLang("email_id_caps")
        titlebtn.setTitle(themes.setLang("add_address_title"), forState: UIControlState.Normal)
        EditCon_Lbl.text=themes.setLang("edit_contact")
//        Name_Field.placeholder=themes.setLang("full_name")
//        Email_Field.placeholder=themes.setLang("email_id_smal")
//        MobileNum_Field.placeholder=themes.setLang("mobile_no_small")
        EnterAdd_Lbl.text=themes.setLang("enter_address")
        Locality_Field.placeholder=themes.setLang("ur_locality")
        HouseNo_Field.placeholder=themes.setLang("house_no")
        City_Field.placeholder=themes.setLang("city")
        Pincode_Field.placeholder=themes.setLang("pincode")
        stateField.placeholder=themes.setLang("State")
        countryField.placeholder=themes.setLang("Country")

        Locality_Field.placeholder = themes.setLang("locality")
        Landmark_Field.placeholder = themes.setLang("landmark")
        Done_Btn.setTitle(themes.setLang("done"), forState: UIControlState.Normal)

    }
    
    func setPading(textField : UITextField,title:String){
        let label = UILabel()
        label.sizeToFit()
        label.text = title
        label.textColor = themes.ThemeColour()
        label.font = PlumberMediumBoldFont
        textField.leftView = label
        textField.leftViewMode = UITextFieldViewMode .Always
    }
    
    @IBAction func fetching_location(sender: AnyObject) {
        if(sender.tag == 0){
        self.locationManager.stopUpdatingLocation()
        
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(currlatitude),\(currlongitude)&key=\(constant.GoogleplacesAPI)")
        let data = NSData(contentsOfURL: url!)
        if(data != nil){
        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
       
        if let result = json["results"] as? NSArray {
            print(result)
            self.DismissProgress()
            if(result.count != 0){
            if let address = result[0]["address_components"] as? NSArray {
                print("get current location \(result[0]["address_components"])")
                let street : String = self.themes.CheckNullValue(address[1]["short_name"])!
                let city : String =  self.themes.CheckNullValue(address[2]["short_name"])!
                let locality : String = self.themes.CheckNullValue(address[3]["short_name"])!
                let state : String = self.themes.CheckNullValue(address[5]["long_name"])!
                let country : String = self.themes.CheckNullValue(address[6]["long_name"])!
                let zipcode : String = self.themes.CheckNullValue(address.lastObject!["short_name"])!
                
                
                let fulladdress = "\(street), \(city), \(locality), \(state), \(country)"
                Addaddress_Data.Latitude = "\(currlatitude)"
                Addaddress_Data.Longitude = "\(currlongitude)"
                Locality_Field.text = fulladdress.stringByReplacingOccurrencesOfString(", ,", withString: ",")
                HouseNo_Field.text = "\(city)"
                City_Field.text = "\(locality)"
                Pincode_Field.text = "\(zipcode)"
                stateField.text = state
                countryField.text = country
                self.State = "\(state)"
                
                
                
            }
        }
        
        }

    }
        }else if(sender.tag == 1){
            Locality_Field.text = ""
            HouseNo_Field.text = ""
            City_Field.text = ""
            Pincode_Field.text = ""
            Landmark_Field.text = ""
            stateField.text = ""
            countryField.text = ""


        }
    
        
    }
    
  
    override func viewWillAppear(animated: Bool) {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }else{
            themes.AlertView("", Message: "\(themes.setLang("location_service_disabled"))\n \(themes.setLang("to_reenable_location")) ", ButtonTitle: kOk)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     let locValue:CLLocationCoordinate2D? = manager.location!.coordinate
     if(locValue != nil){
     print("locations = \(locValue!.latitude) \(locValue!.longitude)")
      currlatitude = "\(locValue!.latitude)"
      currlongitude = "\(locValue!.longitude)"
        
     }
     }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int{
        return places.count
    }
    
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat
    {
        
        return 40.0
        
        
    }
    
    
    
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell{
        
        
        
        let cell:UITableViewCell = (tableView.dequeueReusableCellWithIdentifier("cell") )!
        
        
        let place = self.places[indexPath.row]
        
        cell.textLabel?.text=place.description
        
        
        
        
        
        
        
        return cell
    }
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)
    {
        
        let place = self.places[indexPath.row]
        
        
        
        self.showProgress()
        Addaddress_Data.YourLocality = "\(place.description)"
        
        let geocoder: CLGeocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            self.DismissProgress()
            if (error != nil) {
                print("Error \(error!)")
            } else if let placemark = placemarks?[0] {
                
                
                print("the responsadsase is .......\(placemark)")
                
                //                let CityName:String?=placemark.subAdministrativeArea
                let Locality:String?=placemark.subLocality
                
                let ZipCode:String?=placemark.postalCode
                
                
                if (placemark.administrativeArea != nil)
                {
                    self.State=placemark.administrativeArea!
                    self.stateField.text = self.State as String
                }
                
                if(placemark.country != nil){
                    self.country = placemark.country!
                    self.countryField.text = self.country as String

                }
                
                if (placemark.subAdministrativeArea != nil)
                {
                    self.City_Field.text = self.themes.CheckNullValue(placemark.subAdministrativeArea!)
                }
                
                if(ZipCode != nil)
                {
                    self.Pincode_Field.text=""
                    
                    self.Pincode_Field.text=ZipCode!
                    
                }
                else
                {
                    self.Pincode_Field.text=""
                    
                }
                
                
                if(Locality != nil)
                {
                    
                    self.HouseNo_Field.text=Locality!
                    
                }
                else
                {
                    self.HouseNo_Field.text=""
                    
                }
                
                
                
            }
        })
        
        
        
        URL_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(constant.GoogleplacesAPI)&address=\(place.description.stringByAddingPercentEncodingForFormUrlencoded()!)") { (responseObject) -> () in
            
            if(responseObject != nil)
            {
                
                
                let results:NSArray=(responseObject?.objectForKey("results"))! as! NSArray
                if results.count > 0
                {
                    let firstItem: NSDictionary = results.objectAtIndex(0) as! NSDictionary
                    let geometry: NSDictionary = firstItem.objectForKey("geometry") as! NSDictionary
                    let locationDict:NSDictionary = geometry.objectForKey("location") as! NSDictionary
                    let lat:NSNumber = locationDict.objectForKey("lat") as! NSNumber
                    let lng:NSNumber = locationDict.objectForKey("lng") as! NSNumber
                    
                    
                    Addaddress_Data.Latitude="\(lat)"
                    
                    Addaddress_Data.Longitude="\(lng)"
                    Addaddress_Data.City = self.themes.getLocationname()
                }
                
                
            }
            else
            {
                Addaddress_Data.Latitude="0"
                Addaddress_Data.Longitude="0"
                
            }
            
            
            
        }
        
        Locality_Field.text=Addaddress_Data.YourLocality as String
        
        
        Place_Autocomplete_tableview.hidden=true
        
        
        
    }
    
    
    
    
    
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        if (NSStringFromClass((touch.view?.classForCoder)!)=="UITableViewCellContentView")
        {
            
            
            
            return false
        }
            
        else{
            return true
        }
    }
    
    
    
    
    
    
    
    
    
    func getPlaces(searchString: String) {
        
        let request = requestForSearch(searchString)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request) { data, response, error in
            
            self.handleResponse(data, response: response as? NSHTTPURLResponse, error: error)
            
        }
        
        
        
        task.resume()
        
    }
    
    
    func TextfieldDidChange(textField:UITextField)
    {
        if(textField == Locality_Field)
        {
            if(Locality_Field.text != "")
            {
                if(places.count == 0)
                {
                    self.Place_Autocomplete_tableview.hidden=true
                    
                }
                else
                {
                    self.Place_Autocomplete_tableview.hidden=false
                    
                }
                
                getPlaces(Locality_Field.text!)
                
                
            }
            else
            {
                
                //                self.places = []
                
                places.removeAll()
                
                
                self.Place_Autocomplete_tableview.hidden=true
                
                
                
                
            }
        }
    }
    
    
    
    func handleResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            
            return
            
        }
        
        
        
        if response == nil {
            
            print("GooglePlacesAutocomplete Error: No response from API")
            
            return
            
        }
        
        
        
        if response.statusCode != 200 {
            
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            
            return
            
        }
        
        
        do {
            
            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(
                
                data,
                
                options: NSJSONReadingOptions.MutableContainers
                
                ) as! NSDictionary
            
            dispatch_async(dispatch_get_main_queue(), {
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                
                
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        
                        return Place(
                            
                            id: prediction["id"] as! String,
                            
                            description: prediction["description"] as! String
                            
                        )
                        
                    }
                    self.Place_Autocomplete_tableview.reloadData()
                    
                    
                }
                
            })
        }
            
            
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
        
        
        // Perform table updates on UI thread
        
        
        
    }
    
    func requestForSearch(searchString: String) -> NSURLRequest {
        
        
        
        let params = [
            
            "input": searchString,
            
            // "type": "(\(placeType.description))",
            
            //"type": "",
            
            "key": "\(constant.GoogleplacesAPI)"
            
        ]
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")
        
        
        
        return NSMutableURLRequest(
            
            URL: NSURL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")!
            
            
        )
        
    }
    
    
    
    
    
    
    
    func query(parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sort(<)) {
            let value: AnyObject! = parameters[key]
            
            components += [(escape(key), escape("\(value)"))]
        }
        
        return components.map{"\($0)=\($1)"}.joinWithSeparator("&")
    }
    
    
    
    func escape(string: String) -> String {
        
        let legalURLCharactersToBeEscaped: CFStringRef = ":/?&=;+!@#$()',*"
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        
    }
    
    
    
    
    
    
    func DismissKeyboard(sender:UITapGestureRecognizer)
    {
        
//        Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        donePicker()
        
        view.endEditing(true)
        Place_Autocomplete_tableview.hidden=true
        
        
        
    }
    
    
    
    func donePicker()
    {
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 260.0)
            
            }, completion: { _ in
                
                self.Picker_Wrapper.removeFromSuperview()
                
        })
        
        
        
    }
    
    
    
    
    func showPicker()
    {
        view.addSubview(self.Picker_Wrapper)
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height - 260.0, UIScreen.mainScreen().bounds.size.width, 260.0)
            
            } , completion: { _ in
                
                
                
        })
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        
        if(textField == Name_Field)
        {
            Name_Field.resignFirstResponder()
            Email_Field.becomeFirstResponder()
        }
        if(textField == Email_Field)
        {
            
            Email_Field.resignFirstResponder()
            Country_Code_Field.becomeFirstResponder()
            
            
        }
        
        if(textField == Country_Code_Field)
        {
            
            Country_Code_Field.resignFirstResponder()
        }
        
        if(textField == MobileNum_Field)
        {
            MobileNum_Field.resignFirstResponder()
            
            HouseNo_Field.becomeFirstResponder()
        }
        
        if(textField == Locality_Field)
        {
            
            Locality_Field.resignFirstResponder()
            HouseNo_Field.becomeFirstResponder()
            
            
        }
        
        
        if(textField == HouseNo_Field)
        {
            
            HouseNo_Field.resignFirstResponder()
            Landmark_Field.becomeFirstResponder()
            
            
        }
        
        
        if(textField == Landmark_Field)
        {
            Landmark_Field.resignFirstResponder()
            City_Field.becomeFirstResponder()
        }
        
        
        if(textField == City_Field)
        {
            City_Field.resignFirstResponder()
            
            stateField.becomeFirstResponder()
        }
        if(textField == stateField)
        {
            stateField.resignFirstResponder()
            
            countryField.becomeFirstResponder()
        }
        if(textField == countryField)
        {
            countryField.resignFirstResponder()
            
            Pincode_Field.becomeFirstResponder()
        }
        if(textField == Pincode_Field)
        {
            Pincode_Field.resignFirstResponder()
          //  Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
            
            
        }
        
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        
        
            if(textField == Locality_Field)
            {
                //Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 160), animated: true)
                donePicker()
                
                
                
            }
            
            
            
            if(textField == Country_Code_Field)
            {
                
                
                
                
                self.showPicker()
                
                view.endEditing(true)
                
                return false
                
            }
            
            
            
            
            
            if(textField == HouseNo_Field)
            {
                
              //  Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 260), animated: true)
                donePicker()
                
                
                
            }
            if(textField == Landmark_Field)
            {
                
                
                
             //   Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 340), animated: true)
                donePicker()
                
                
                
                
                
                
                
                
            }
            if(textField == City_Field)
            {
               // Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 400), animated: true)
                donePicker()
                
                
                
            }
            
            if(textField == Pincode_Field)
            {
                //Addaddress_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 460), animated: true)
                donePicker()
                
                
                
            }
            
              
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == Name_Field)
        {
            
            let aSet = NSCharacterSet(charactersInString: ACCEPTABLE_CHARACTERS).invertedSet
            let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            
            
            
            
            
            return string == numberFiltered
        }
            
            
            
        else
        {
            return true
            
        }
        
    }
    
    
    //PickerVView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return themes.codename.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (themes.codename[row] as! String)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        Country_Code_Field.text="\(themes.code[row])"
        Addaddress_Data.Country_code="\(themes.code[row])"
    }
    
    
    
    
    
    
    @IBAction func didClickoptions(sender: UIButton) {
        
        if(sender.tag == 0)
        {
            
            self.navigationController?.popViewControllerAnimated(true)
            
        }
        if(sender.tag == 1)
        {
            
            Add_address()
            
            
        }
    }
    
    func Add_address()
    {
        if(Name_Field.text == "") {
            themes.AlertView("\(Appname)", Message:themes.setLang("enter_ur_name"), ButtonTitle: kOk)
        }  else if(Email_Field.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_id"), ButtonTitle: kOk)
        }else if(MobileNum_Field.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_num"), ButtonTitle: kOk)
        }
            
            //        else if(HouseNo_Field.text == "")
            //        {
            //            themes.AlertView("\(Appname)", Message: "Enter your house no", ButtonTitle: "Ok")
            //        }
            
            //        else if(Landmark_Field.text == "")
            //        {
            //            themes.AlertView("\(Appname)", Message: "Enter your landmark", ButtonTitle: "Ok")
            //        }
            //
        else if(Locality_Field.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_locality"), ButtonTitle: kOk)
        } else if(City_Field.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_city"), ButtonTitle: kOk)
        }else if(stateField.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_state"), ButtonTitle: kOk)
        }else if(countryField.text == "") {
            themes.AlertView("\(Appname)", Message: themes.setLang("enter_ur_country"), ButtonTitle: kOk)
        }            //        else if(Pincode_Field.text == "")
            //        {
            //            themes.AlertView("\(Appname)", Message: "Enter your pincode", ButtonTitle: "Ok")
            //        }
        else
        {
            Addaddress_Data.Name=Name_Field.text!
            Addaddress_Data.EmailID=Email_Field.text!
            Addaddress_Data.MobileNumber=MobileNum_Field.text!
            Addaddress_Data.HouseNo=HouseNo_Field.text!
            Addaddress_Data.Landmark=Landmark_Field.text!
            Addaddress_Data.YourLocality=Locality_Field.text!
            Addaddress_Data.City=City_Field.text!
            Addaddress_Data.Pincode=Pincode_Field.text!
            Addaddress_Data.Country_code=Country_Code_Field.text!
            self.showProgress()
            
                    if Addaddress_Data.YourLocality.rangeOfString(Addaddress_Data.HouseNo as String).location == NSNotFound
            
                     {
                         print("string does not contain bla");
                     }
                     else{
                       Addaddress_Data.HouseNo = ""
                        }
            
            //
            //            if Addaddress_Data.YourLocality.rangeOfString(Addaddress_Data.City as String).location == NSNotFound
            //
            //            {
            //                print("string does not contain bla");
            //            }
            //            else{
            //                Addaddress_Data.City = ""
            //            }
            //
            //
            //            if Addaddress_Data.YourLocality.rangeOfString(self.State as String).location == NSNotFound
            //
            //            {
            //                print("string does not contain bla");
            //            }
            //            else{
            //                self.State = ""
            //            }
            //
            //
            //            if Addaddress_Data.YourLocality.rangeOfString(Addaddress_Data.Pincode as String).location == NSNotFound
            //
            //            {
            //                print("string does not contain bla");
            //            }
            //            else{
            //                Addaddress_Data.Pincode = ""
            //            }
            //
            
            
            
            
            
            
            let param:NSDictionary=["user_id":"\(self.themes.getUserID())","name":"\(Addaddress_Data.Name)","email":"\(Addaddress_Data.EmailID)",
                                    "country_code":"\(Addaddress_Data.Country_code)","mobile":"\(Addaddress_Data.MobileNumber)","street":"\( Addaddress_Data.HouseNo)",
                                    "landmark":"\(Addaddress_Data.Landmark)","locality":"\(Addaddress_Data.YourLocality)","city":"\(Addaddress_Data.City)",
                                    "zipcode":"\(Addaddress_Data.Pincode)","lng":"\(Addaddress_Data.Longitude)",
                                    "line1":"\(Addaddress_Data.City)","state":"\(self.State)","lat":"\(Addaddress_Data.Latitude)","country":"\(self.country)"]
            
            
            
            URL_handler.makeCall(constant.Add_address, param: param, completionHandler: { (responseObject, error) -> () in
                self.DismissProgress()
                print("the erroe is \(error)")
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                    
                }
                    
                else
                {
                    
                    
                    if(responseObject != nil)
                    {
                        
                        let dict:NSDictionary=responseObject!
                        
                        
                        
                        
                        let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        
                        if(Status == "1")
                            
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                            
                        }
                        else
                        {
                            self.themes.AlertView(Appname, Message: "\(self.themes.CheckNullValue(dict.objectForKey("errors"))!)", ButtonTitle: kOk)

                        }
                        
                        
                    }
                        
                    else
                    {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
            
            
            
            
        }
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension String {
    func stringByAddingPercentEncodingForFormUrlencoded() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumericCharacterSet()
        characterSet.addCharactersInString("-._* ")
        
        return stringByAddingPercentEncodingWithAllowedCharacters(characterSet)?.stringByReplacingOccurrencesOfString(" ", withString: "+")
    }
}



extension AddaddressViewController:UITextFieldDelegate
{
    
}
extension AddaddressViewController:UIGestureRecognizerDelegate
{
    
}

extension AddaddressViewController:UIPickerViewDelegate
{
    
    
    
    
}
