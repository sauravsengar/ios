//
//  PaymentViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 30/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController,UIAlertViewDelegate,UITextViewDelegate {
    
    @IBOutlet var discountlabl: UILabel!
    @IBOutlet var couponview: UIView!
    @IBOutlet var checkmark: UIButton!
    @IBOutlet var JobIdLable: UILabel!
    
    @IBOutlet var paymentbtn: ButtonColorView!
    @IBOutlet var Jobtime: UILabel!
    @IBOutlet var Jobdate: UILabel!
    @IBOutlet var Amountbtn: UIButton!
    
    var tField: UITextField!
    
    var Globalindex:NSString=NSString()
    @IBOutlet var Amount_Lab: UILabel!
    @IBOutlet var Payment_List: UITableView!
    @IBOutlet var Payment_ScrollView: UIScrollView!
    var getPaymentmode: NSString = NSString()
    
    
    var Payment_DetailArray:NSMutableArray=NSMutableArray()
    var Payment_Inactive : NSMutableArray = NSMutableArray()
    var Payment_Active : NSMutableArray = NSMutableArray()
    var taskid: NSString!
    var themes:Themes=Themes()
    var PaymentArray:NSMutableArray=NSMutableArray()
    var URL_handler:URLhandler=URLhandler()
    
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    
    @IBOutlet weak var lblIhaveaCoupenCode: UILabel!
    @IBOutlet weak var lblJobDate: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
     @IBOutlet var termtextview: UITextView!
    @IBOutlet weak var lblSelectPaymentMode: UILabel!

    
    override func viewDidLoad()
    {
        
        self.paymentbtn.setTitle(self.themes.setLang("payment"), forState:.Normal)
        lblTime.text = themes.setLang("time")
        lblPayment.text = themes.setLang("payment")
        lblIhaveaCoupenCode.text = themes.setLang("have_coupon_code")
        lblJobDate.text = themes.setLang("job_date")
        lblSelectPaymentMode.text = themes.setLang("select_payment_mode")
        btnClose.setTitle(themes.setLang("close"), forState: UIControlState.Normal)

        StripeStatus="Provider_Payment"
        
        
        let str:NSMutableAttributedString = NSMutableAttributedString.init(string:"I Agree To The Terms And Conditions.")
        str.addAttribute(NSLinkAttributeName, value:"1", range: NSRange(location:15,length:21))
        
        termtextview.attributedText = str;
        termtextview.userInteractionEnabled = true
        termtextview.font = UIFont(name: "Roboto-Regular", size: 14)
        termtextview.textAlignment = .Center
        termtextview.textColor = UIColor.blackColor()
        termtextview.linkTextAttributes = [NSForegroundColorAttributeName: PlumberThemeColor]
        termtextview.delegate = self
        termtextview.sizeToFit()
        self.view.layer.cornerRadius = 8.0;
        self.view.clipsToBounds=true
        self.view.layer.borderColor = UIColor.lightGrayColor().CGColor;
        self.view.layer.borderWidth=2.0;
        
        
        Amountbtn.layer.cornerRadius=Amountbtn.frame.size.width/2
        Amountbtn.clipsToBounds=true
        
        
        
        let Nb=UINib(nibName: "PaymentTableViewCell", bundle: nil)
        
        Payment_List.registerNib(Nb, forCellReuseIdentifier: "PaymentCell")
        
        SetFrameAccordingToSegmentIndex()
        
        get_Payment()
        self.SetFrameAccordingToSegmentIndex()
        
        let Tap:UITapGestureRecognizer=UITapGestureRecognizer()
        Tap.addTarget(self, action: #selector(PaymentViewController.Addcoupon(_:)))
        couponview.addGestureRecognizer(Tap)
        
        
    }
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        
        
        
        let get_String:NSString =  textView.text
        
        print("get character range\(get_String.substringWithRange(NSMakeRange(characterRange.location, characterRange.length)))")
        let getchastring: String  = get_String.substringWithRange(NSMakeRange(characterRange.location, characterRange.length))
        
        
        let Controller:TermsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("Termvc") as! TermsViewController
        Controller.getterms = getchastring
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        return false
    }

    func Addcoupon(sender:UITapGestureRecognizer)
    {
        
        
        
        let alert = UIAlertController(title:themes.setLang("add_coupen_code")
            , message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title:themes.setLang("cancel"), style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: themes.setLang("done"), style: .Default, handler:{ (UIAlertAction) in
            self.applycoupon()

        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("applicationLanguageChangeNotification:"), name: Language_Notification as String, object: nil)
        SetFrameAccordingToSegmentIndex()
        
        if(themes.Check_userID() != "")
        {
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
            
            
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.showPopup(_:)), name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.Show_rating(_:)), name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PaymentViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushRating", object: nil)
            
            
        }
        
        
        
        
    }
    override func viewDidDisappear(animated: Bool) {
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
        
        
        
    }
    
    deinit
    {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    
    func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    Message_details.support_chatid = refer_id!
                    Message_details.admin_id = admin_id!
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
                    alertView.addButton(self.themes.setLang(self.themes.setLang("ok")), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }
    

    func methodofReceivePaymentNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    func methodofReceiveRatingNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    
    func methodofReceivePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
                
            }else{
                
                let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                self.navigationController?.pushViewController(Controller, animated: true)
                
                
            }
            
        }
        
    }
    
    
    
    func methodOfReceivedMessagePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            
            Message_details.taskid = taskid!
            Message_details.providerid = check_userid
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        
        
    }
    
    
    func methodOfReceivedMessageNotification(notification: NSNotification){
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                    
                }else{
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("You Have A Message From The Provider"))
                    alertView.addButton(self.themes.setLang("ok"), action: {
                        Message_details.taskid = taskid!
                        Message_details.providerid = check_userid
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                        
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                        
                        
                    })
                    alertView.show()
                    
                }
                
            }
        }
        
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func Show_rating(notification: NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        let alertView = UNAlertView(title: Appname, message:messageString as String)
        alertView.addButton(self.themes.setLang("ok"), action: {
            let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
            self.navigationController?.pushViewController(Controller, animated: true)
            
            
        })
        alertView.show()
        
        
        
        
        
        
        
        
        
        
        
    }
    
    func showPopup(notification: NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        let alertView = UNAlertView(title: Appname, message:messageString as String)
        alertView.addButton(self.themes.setLang("ok"), action: {
            
            self.get_Payment()
            
            
        })
        alertView.show()
        
        
    }
    
    func Show_Alert(notification:NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        let action:NSString! = userInfo["Action"]

        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    if action != "admin_notification"{

                    let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    }
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
    }
    
    
    func applycoupon()
    {
        
        
        
        constant.showProgress()
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","booking_id":"\(Root_Base.Job_ID)","code":"\(self.tField.text!)"]
        URL_handler.makeCall(constant.Apply_Coupon_code, param: Param) { (responseObject, error) -> () in
            
            constant.DismissProgress()
            
            if(error != nil)
            {
                
                
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
            }
            else
            {
                print("\(Param) ...\(responseObject)...\(constant.Pay_Cash) ")
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    if(Status == "1")
                    {
                        //constant.showProgress()
                        self.couponview.hidden = true
                        self.couponview.userInteractionEnabled = false
                        self.discountlabl.hidden = false
                        self.discountlabl.text = "\(self.themes.setLang("Disc_lbl_txt"))\(self.themes.Currency_Symbol(Payment_Detail.currency as String))\(self.themes.CheckNullValue(Dict.objectForKey("discount"))!)"
                        let Response:NSString?=Dict.objectForKey("response") as? NSString
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                        
                        
                        self.get_Payment()
                    }
                    else
                    {
                        constant.DismissProgress()
                        let Response:NSString?=Dict.objectForKey("response") as? NSString
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                    }
                    
                }
            }
        }
    }
    
    
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = themes.setLang("enter_coupen_code")
        
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    
    override func  viewWillAppear(animated: Bool) {
        constant.DismissProgress()
    }
    @IBAction func makepayment(sender: AnyObject) {
        
        
    }
    @IBAction func didclickoption(sender: AnyObject) {
        
        if(sender.tag == 0)
        {
            
            if(checkmark.selected == true)
            {
                checkmark.selected = false
                checkmark.setImage(UIImage(named: "check"), forState: UIControlState.Normal)
            }
            else
            {
                checkmark.selected = true
                checkmark.setImage(UIImage(named: "tick"), forState: UIControlState.Normal)
            }
        }
        
        
    }
    
    @IBAction func paymentAction(sender: AnyObject) {
        if(checkmark.selected == false)
        {
            themes.AlertView("\(Appname)", Message: themes.setLang("accept_terms"), ButtonTitle: kOk)
        }
       else if ( Payment_Detail.payment_amount == "0.00")
        {
            self.Compelte_Payment()
        }

       else if (getPaymentmode == "")
        {
            themes.AlertView("\(Appname)", Message: themes.setLang("select_paymentmode"), ButtonTitle: kOk)
        }
       
        else
        {
            if(getPaymentmode == "cash")
            {
                
                pay_cash()
            }
                
            else if(getPaymentmode == "auto_detect")
            {
                Pay_Card()
                
            }
                
            else if(getPaymentmode == "wallet")
            {
                pay_Wallet()
            }
            else if (getPaymentmode == "paypal")
            {
                Paypal_Transaction(getPaymentmode as String)
            }
            else if getPaymentmode == "remita"
            {
                payment_Transaction(getPaymentmode as String)
            }
            else if getPaymentmode == "stripe-connect"
            {
                payment_Transaction(getPaymentmode as String)

            }
            
            else
            {
                payment_Transaction(getPaymentmode as String)
            }
            
            
        }
        
    }
    
    
    func  Compelte_Payment()
        {
            
            constant.showProgress()
            
            let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id:":Root_Base.Job_ID ]
            URL_handler.makeCall(constant.Complete_Payment, param: Param) { (responseObject, error) -> () in
                
                constant.DismissProgress()
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    
                    // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                    
                }
                else
                {
                    
                    
                    let Dict:NSDictionary=responseObject!
                    let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    if(Status != nil)
                    {
                        
                        if(Status == "1")
                        {
                            //  Payment_Detail.Mobile_id=self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!
                        
                            self.performSegueWithIdentifier("RatingVC", sender: nil)
                            
                        }
                        else
                        {
                            constant.DismissProgress()
                            let Response:NSString?=self.themes.CheckNullValue(Dict.objectForKey("errors"))!
                            self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                            
                        }
                    }
                }
            }

        
    }
    @IBOutlet var makepayment: ButtonColorView!
    @IBAction func DidclickOption(sender: UIButton) {
        if(sender.tag == 0)
        {
            self.navigationController?.popViewControllerAnimated(true)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    func get_Payment()
    {
        
        constant.showProgress()
        
        
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.Job_ID)"]
        URL_handler.makeCall(constant.Get_Summary_Details, param: Param) { (responseObject, error) -> () in
            
            
            constant.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                print("\(Param)...\(constant.Get_Payment_Detail)...\(responseObject)")
                
                self.PaymentArray = NSMutableArray ()
                self.Payment_DetailArray = NSMutableArray()
                self.Payment_Inactive = NSMutableArray()
                self.Payment_Active = NSMutableArray()
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    
                    if(Status == "1")
                    {
                        let Response:NSDictionary?=Dict.objectForKey("response")!.objectForKey("info") as? NSDictionary
                        
                        print("response is \(Response)")
                        
                        if(Response != nil)
                        {
                            Payment_Detail.job_date=Response!.objectForKey("job_date") as! NSString
                            Payment_Detail.job_time=Response!.objectForKey("job_time") as! NSString
                            let str : String = self.themes.CheckNullValue(Response!.objectForKey("payment_amount"))!
                            // let str = "\(myInt)"
                            Payment_Detail.payment_amount=str
                            Root_Base.task_id = self.themes.CheckNullValue(Response!.objectForKey("task_id"))!
                            //Payment_Detail.payment_amount=Response!.objectForKey("payment_amount") as! NSString
                            Payment_Detail.currency=Response!.objectForKey("currency") as! NSString
                            Payment_Detail.category_image=Response!.objectForKey("category_image") as! NSString
                            Payment_Detail.user_image=Response!.objectForKey("user_image") as! NSString
                            
                            //                            let longInt : Float = Response!.objectForKey("longitude") as! Float
                            //                            let strLong = "\(longInt)"
                            //                            Payment_Detail.longitude = strLong
                            //                            //Payment_Detail.longitude=Response!.objectForKey("longitude") as! NSString
                            //
                            //                            let latInt : Float = Response!.objectForKey("latitude") as! Float
                            //                            let strLat = "\(latInt)"
                            //                            Payment_Detail.latitude = strLat
                            //Payment_Detail.latitude=Response!.objectForKey("latitude") as! NSString
                            //  self.set_mapView(Payment_Detail.latitude,long: Payment_Detail.longitude)
                        }
                        
                        self.setdata()
                        
                        let Response_Payment:NSArray?=Dict.objectForKey("response")!.objectForKey("payment") as? NSArray
                        
                        if(Response_Payment != nil)
                        {
                            for  Dict in Response_Payment!
                            {
                                
                                self.PaymentArray.addObject(self.themes.CheckNullValue(Dict.objectForKey("name"))!)
                                self.Payment_DetailArray.addObject(self.themes.CheckNullValue(Dict.objectForKey("code"))!)
                                
                                let inactiveimage : String = self.themes.CheckNullValue(Dict.objectForKey("in_active"))!
                                //let replacedinactive = (inactiveimage as NSString).stringByReplacingOccurrencesOfString("localhost", withString: "192.168.1.251")
                                let activeimage: String = self.themes.CheckNullValue(Dict.objectForKey("active"))!
                                //let reaplacedactive = (activeimage as NSString).stringByReplacingOccurrencesOfString("localhost", withString:"192.168.1.251")
                                
                                
                                
                                self.Payment_Inactive.addObject(inactiveimage)
                                self.Payment_Active.addObject(activeimage)
                                
                                
                                
                            }
                            
                            
                            
                          if  ( Payment_Detail.payment_amount == "0.00")
                          {
                            self.PaymentArray.removeAllObjects()
                            self.Payment_DetailArray.removeAllObjects()
                            
                            
                            self.Payment_Inactive.removeAllObjects()
                            self.Payment_Active.removeAllObjects()
                            
                            self.paymentbtn.setTitle(self.themes.setLang("Complete Payment"), forState:.Normal)


                            }
                            
                            self.Payment_List.reloadData()
                            self.SetFrameAccordingToSegmentIndex()
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        
                        //  self.themes.AlertView("Network Failure", Message:Dict.objectForKey("response") as! String, ButtonTitle: self.themes.setLang("ok"))
                    }
                    
                    
                }
            }
            
            
        }
    }
    
    
    func setdata()
    {
        Jobdate.text="\(Payment_Detail.job_date)"
        Jobtime.text = "\(Payment_Detail.job_time)"
        self.themes.saveCurrencyCode("\(self.themes.Currency_Symbol(Payment_Detail.currency as String))")
        Amountbtn .setTitle(" \(self.themes.Currency_Symbol(Payment_Detail.currency as String))\(Payment_Detail.payment_amount)", forState: UIControlState.Normal)
        JobIdLable.text = "\(Root_Base.Job_ID)"
        
        
    }
    
    
    func SetFrameAccordingToSegmentIndex(){
        
        
        
        dispatch_async(dispatch_get_main_queue()) {
            //            //This code will run in the main thread:
            //            var frame: CGRect = self.Payment_List.frame
            //            frame.size.height = self.Payment_List.contentSize.height;
            //            //            frame.origin.y=
            //            self.Payment_List.frame = frame;
            self.Payment_ScrollView.contentSize=CGSizeMake(self.Payment_ScrollView.frame.size.width, self.Payment_List.frame.origin.y+self.Payment_List.frame.size.height+30)
            
            
        }
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 74
        
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        SetFrameAccordingToSegmentIndex()
        
        return PaymentArray.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell:PaymentTableViewCell  = tableView.dequeueReusableCellWithIdentifier("PaymentCell") as! PaymentTableViewCell
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        
        if PaymentArray[indexPath.row] as! String == "stripe"
        {
            Cell.Payment_Lab.text="card"
            
           
        }
        else
        {
            
            Cell.Payment_Lab.text="\(PaymentArray[indexPath.row])"
        }
        
        
        
        
        
        
        // self.UserImage.sd_setImageWithURL(NSURL(string: "\(themes.getuserDP())"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        
        if(Globalindex == "\(indexPath.row)")
        {
            
            Cell.Wallet_ImageView.sd_setImageWithURL(NSURL(string: "\(Payment_Active[indexPath.row])"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            
        }
        else
        {
            
            Cell.Wallet_ImageView.sd_setImageWithURL(NSURL(string: "\(Payment_Inactive[indexPath.row])"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        }
        
        
        return Cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Globalindex="\(indexPath.row)"
        
        getPaymentmode = "\(Payment_DetailArray[indexPath.row])"
        self.Payment_List.reloadData()
        
        //        self.Payment_List.reloadData()
        
    }
    
    
    
    func Paypal_Transaction(code:String)
    {
        constant.showProgress()
        
        let Param:NSDictionary=["user":"\(themes.getUserID())","task":Root_Base.task_id ]
        URL_handler.makeCall(constant.Pay_Paypal, param: Param) { (responseObject, error) -> () in
            
            constant.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                
                
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    
                    if(Status == "1")
                    {
                        //  Payment_Detail.Mobile_id=self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!
                        
                        Payment_Detail.PaymentUrl = self.themes.CheckNullValue(Dict.objectForKey("redirectUrl"))!
                        Payment_Detail.paymentmode = self.themes.CheckNullValue(Dict.objectForKey("payment_mode"))!
                        
                        self.themes.amount =  Payment_Detail.payment_amount as String
                        self.performSegueWithIdentifier("TransactionVC", sender: nil)
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        let Response:NSString?=self.themes.CheckNullValue(Dict.objectForKey("errors"))!
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                        
                    }
                }
            }
        }
        
    }
    func payment_Transaction(code:String)
    {
        
        
        constant.showProgress()
        
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.Job_ID)","gateway":"\(code)"]
        URL_handler.makeCall(constant.Pay_Transaction, param: Param) { (responseObject, error) -> () in
            
            constant.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                
                
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    
                    if(Status == "1")
                    {
                        Payment_Detail.Mobile_id=self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!
                        Payment_Detail.paymentmode = self.getPaymentmode
                        self.themes.amount =  Payment_Detail.payment_amount as String

                        if (Payment_Detail.paymentmode == "stripe"){
                        
                            Payment_Detail.PaymentUrl = "\(constant.Pay_Creditcard)mobileId=\(self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!)"

                        }
                            
                        else{
                            
                            Payment_Detail.PaymentUrl = "\(constant.Pay_stripeconnect)mobileId=\(self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!)"

                        }
                        self.performSegueWithIdentifier("TransactionVC", sender: nil)
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        let Response:NSString?=self.themes.CheckNullValue(Dict.objectForKey("errors"))!
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                        
                    }
                }
            }
        }
        
    }
    
    func pay_Wallet()
    {
        
        constant.showProgress()
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.Job_ID)"]
        URL_handler.makeCall(constant.Pay_Wallet, param: Param) { (responseObject, error) -> () in
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    
                    if(Status == "1")
                    {
                        constant.DismissProgress()
                        let due_amout: String = self.themes.CheckNullValue(Dict.objectForKey("due_amount"))!
                        self.themes.saveCurrency(self.themes.CheckNullValue(Dict.objectForKey("available_wallet_amount"))!)
                        
                        NSLog("Get due amount=%@", due_amout)
                        if due_amout.isEmpty {
                            
                            // self.get_Payment()
                            //self.Payment_List.reloadData()
                            constant.showProgress()
                            constant.DismissProgress()
                            self.themes.AlertView("\(Appname)", Message: self.themes.CheckNullValue(Dict.objectForKey("response"))!, ButtonTitle: self.themes.setLang("ok"))
                            
                            
                            self.performSegueWithIdentifier("RatingVC", sender: nil)
                        }
                            
                            
                        else
                        {
                            self.themes.AlertView("\(Appname)", Message: self.themes.CheckNullValue(Dict.objectForKey("response"))!, ButtonTitle: self.themes.setLang("ok"))
                            self.Globalindex = NSString()
                            self.get_Payment()
                            self.Payment_List.reloadData()
                            self.themes.saveCurrency("0")
                            
                        }
                        
                        
                        
                        
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        
                        let Response:NSString?=Dict.objectForKey("response") as? NSString
                        
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                        
                    }
                }
            }
        }
        
    }
    
    func pay_auto_detect()
    {
        constant.showProgress()
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.task_id)"]
        URL_handler.makeCall(constant.Pay_Autodetect, param: Param) { (responseObject, error) -> () in
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    
                    if(Status == "1")
                    {
                        constant.showProgress()
                        
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        
                        let Response:NSString?=Dict.objectForKey("response") as? NSString
                        
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                        
                    }
                }
            }
        }
    }
    
    func Pay_Card()
    {
        
        let param=["user_id":themes.getUserID()]
        
        constant.showProgress()
        
        URL_handler.makeCall(constant.GetStripeStatus, param: param) { (responseObject, error) -> () in
            constant.DismissProgress()
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
            }
                
            else
            {
                
                
                if(responseObject != nil)
                {
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    
                    if (Staus == "1")
                    {
                        MyWallet.stripe_keys=dict.objectForKey("response")!.objectForKey("stripe_keys") as! NSDictionary
                        MyWallet.mode=MyWallet.stripe_keys.objectForKey("mode") as! NSString
                        MyWallet.secret_key=MyWallet.stripe_keys.objectForKey("secret_key") as! NSString
                        MyWallet.publishable_key=MyWallet.stripe_keys.objectForKey("publishable_key") as! NSString
                        MyWallet.cards=dict.objectForKey("response")!.objectForKey("cards") as! NSDictionary
                        
                        MyWallet.card_status=MyWallet.cards.objectForKey("card_status") as! NSString
                        if(MyWallet.card_status == "1")
                        {
                            MyWallet.result=MyWallet.cards.objectForKey("result") as! NSArray
                            
                            if(MyWallet.card_number.count != 0)
                            {
                                MyWallet.card_number.removeAllObjects()
                                MyWallet.exp_month.removeAllObjects()
                                MyWallet.exp_year.removeAllObjects()
                                MyWallet.card_type.removeAllObjects()
                                MyWallet.customer_id.removeAllObjects()
                                MyWallet.card_id.removeAllObjects()
                            }
                            
                            for Dic in MyWallet.result
                            {
                                MyWallet.card_number.addObject(Dic.objectForKey("card_number") as! NSString)
                                MyWallet.exp_month.addObject(Dic.objectForKey("exp_month") as! NSString)
                                MyWallet.exp_year.addObject(Dic.objectForKey("exp_year") as! NSString)
                                MyWallet.card_type.addObject(Dic.objectForKey("card_type") as! NSString)
                                MyWallet.customer_id.addObject(Dic.objectForKey("customer_id") as! NSString)
                                MyWallet.card_id.addObject(Dic.objectForKey("card_id") as! NSString)
                                
                                
                                
                            }
                            
                            let Controller:CardListViewController=self.storyboard?.instantiateViewControllerWithIdentifier("cardListVC") as! CardListViewController
                            self.navigationController?.pushViewController(Controller, animated: true)
                            
                            
                            
                        }
                        else
                        {
                            //                            let Storyboard:UIStoryboard=UIStoryboard(name: "Main", bundle: nil)
                            //                            let vc = Storyboard.instantiateViewControllerWithIdentifier("StripeVC")
                            //                            self.presentViewController(vc, animated: true, completion: nil)
                            
                            let Controller:StripeViewController=self.storyboard?.instantiateViewControllerWithIdentifier("StripeVC") as! StripeViewController
                            self.navigationController?.pushViewController(Controller, animated: true)
                            
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        self.themes.AlertView("\(Appname)",Message: "\(dict.objectForKey("response"))",ButtonTitle: self.themes.setLang("ok"))
                        self.navigationController?.popViewControllerAnimated(true)
                        
                    }
                    
                }
                else
                {
                    
                    
                    self.themes.AlertView("\(Appname)",Message: self.themes.setLang("Please try again"),ButtonTitle: self.themes.setLang("ok"))
                    
                    
                    
                }
                
            }
        }
        
    }
    
    
    
    func pay_cash()
    {
        constant.showProgress()
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.Job_ID)"]
        URL_handler.makeCall(constant.Pay_Cash, param: Param) { (responseObject, error) -> () in
            
            print("\(Param) ...\(responseObject)...\(constant.Pay_Cash) ")
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                
            }
            else
            {
                print("\(Param) ...\(responseObject)...\(constant.Pay_Cash) ")
                let Dict:NSDictionary=responseObject!
                let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                if(Status != nil)
                {
                    if(Status == "1")
                    {
                        constant.showProgress()
                        
                    }
                    else
                    {
                        constant.DismissProgress()
                        let Response:NSString?=Dict.objectForKey("response") as? NSString
                        self.themes.AlertView("Message", Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                    }
                    
                }
            }
        }
    }
    
    @IBAction func didClickption(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
