//
//  CardListViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 27/01/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class CardListViewController: RootViewController {
    
    @IBOutlet var CardList_tableview: UITableView!
    @IBOutlet var wapper_View: BorderView!
    @IBOutlet var Choose_CardLabl: UIButton!
    @IBOutlet var Proceed_Btn: UIButton!

    var Globalindex:NSString=NSString()
    var URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wapper_View.layer.cornerRadius=themes.RoundView(wapper_View.frame.size.width)
        wapper_View.clipsToBounds=true
        
        let Nb=UINib(nibName: "CardListTableViewCell", bundle: nil)
        
        CardList_tableview.registerNib(Nb, forCellReuseIdentifier: "ListCell")
        CardList_tableview.estimatedRowHeight = 140
        CardList_tableview.rowHeight = UITableViewAutomaticDimension
        CardList_tableview.separatorColor=UIColor.clearColor()
        
        if(StripeStatus == "Provider_Payment")
        {
         self.view.layer.cornerRadius = 8.0;
        self.view.clipsToBounds=true
        self.view.layer.borderColor = UIColor.lightGrayColor().CGColor;
        self.view.layer.borderWidth=2.0;
        }

        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return MyWallet.card_id.count
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell:CardListTableViewCell  = tableView.dequeueReusableCellWithIdentifier("ListCell") as! CardListTableViewCell
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        
        Cell.Carddetail_Lab.text="Card Number: \(MyWallet.card_number[indexPath.section])\nExpire month: \(MyWallet.exp_month[indexPath.section])\nExpire year: \(MyWallet.exp_year[indexPath.section])\nCard Type: \(MyWallet.card_type[indexPath.section])"
        Cell.Carddetail_Lab.sizeToFit()
        Cell.selectionStyle = .None
        
        Cell.layer.cornerRadius = 5
        Cell.layer.shadowColor = UIColor.blackColor().CGColor
        Cell.layer.shadowOpacity = 0.5
        Cell.layer.shadowRadius = 2
        Cell.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        Cell.Delete_card.addTarget(self, action: #selector(CardListViewController.Deleteaction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.Delete_card.tag=indexPath.section
        if(Globalindex == "\(indexPath.section)")
        {
            
            Cell.Selected_Img.hidden=false
            
        }
        else
        {
            Cell.Selected_Img.hidden=true
            
        }
        
        return Cell
    }
    func Deleteaction(sender:UIButton)
    {
        
        self.showProgress()
        Proceed_Btn.enabled=false
        CardList_tableview.userInteractionEnabled=false

        let param:NSDictionary=["user_id":"\(themes.getUserID())","card_id":"\(MyWallet.card_id[sender.tag])","customer_id":"\(MyWallet.customer_id[sender.tag])"]
        URL_handler.makeCall(constant.Delete_card, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            self.Proceed_Btn.enabled=true
             self.CardList_tableview.userInteractionEnabled=true


            if(error != nil)
            {
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

            }
                
            else
            {
                if(responseObject != nil)
                {
                    
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    let response:NSString=dict.objectForKey("response") as! NSString
                    
                    
                    
                    if (Staus == "1")
                    {
                        
                         self.CardList_tableview.beginUpdates()
                        MyWallet.card_number.removeObjectAtIndex(sender.tag)
                        MyWallet.exp_month.removeObjectAtIndex(sender.tag)
                        MyWallet.exp_year.removeObjectAtIndex(sender.tag)
                        MyWallet.card_type.removeObjectAtIndex(sender.tag)
                        MyWallet.customer_id.removeObjectAtIndex(sender.tag)
                        MyWallet.card_id.removeObjectAtIndex(sender.tag)
                        self.CardList_tableview.deleteSections(NSIndexSet(index: sender.tag), withRowAnimation: .Fade)
                        self.CardList_tableview.endUpdates()
                        
                    }
                    else
                    {
                        
                        self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        
                        
                    }
                    
                    
                }
            }
        })
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Globalindex="\(indexPath.section)"
        self.CardList_tableview.reloadData()
        
        
        
        
        
    }
    
    
    func pay_Stripe(cardid:String)
    {
        self.showProgress()
        Proceed_Btn.enabled=false
        CardList_tableview.userInteractionEnabled=false

        
        let param:NSDictionary=["user_id":"\(themes.getUserID())","total_amount":"\(Transaction_Stat.total_Amt)","card_id":"\(cardid)","stripe_token":"","stripe_email":""]
        URL_handler.makeCall(constant.Pay_Stripe, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            self.Proceed_Btn.enabled=true
            self.CardList_tableview.userInteractionEnabled=true

            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

               // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
                
            else
            {
                if(responseObject != nil)
                {
                    
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    let response:NSString=dict.objectForKey("response") as! NSString
                    
                    
                    
                    if (Staus == "1")
                    {
                        
                        self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        
                        
                    }
                    else
                    {
                        
                        self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        
                        
                    }
                    
                    
                }
            }
        })
    }
    
     @IBAction func DidclickOptions(sender: UIButton) {
        
        if(sender.tag == 0)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        if(sender.tag == 1)
        {
//            let Storyboard:UIStoryboard=UIStoryboard(name: "Main", bundle: nil)
//            let vc = Storyboard.instantiateViewControllerWithIdentifier("StripeVC")
//            self.presentViewController(vc, animated: true, completion: nil)
            
            let Controller:StripeViewController=self.storyboard?.instantiateViewControllerWithIdentifier("StripeVC") as! StripeViewController
            self.navigationController?.pushViewController(Controller, animated: true)

        }
        if(sender.tag == 2)
        {
            
            if(MyWallet.card_number.count == 0)
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("add_a_card") , ButtonTitle: kOk)
            }
                else if(Globalindex == "")
            {
                themes.AlertView("\(Appname)", Message:  themes.setLang("select_a_card"), ButtonTitle: kOk)

            }
            else
            {
                
                if(StripeStatus == "Provider_Payment")
                {
                    self.pay_Stripe_Provider("\(MyWallet.card_id[Int(Globalindex as String)!])")
                }
                
                if(StripeStatus == "Wallet")
                {
                    self.pay_Stripe("\(MyWallet.card_id[Int(Globalindex as String)!])")
                    
                }

            }
            
        }
        
    }
    
    
    func pay_Stripe_Provider(cardid:String)
    {
        self.showProgress()
        Proceed_Btn.enabled=false
        CardList_tableview.userInteractionEnabled=false

        
        let param:NSDictionary=["user_id":"\(themes.getUserID())","job_id":"\(Root_Base.Job_ID)","card_id":"\(cardid)","stripe_token":"","stripe_email":""]
        URL_handler.makeCall(constant.Pay_Stripe_Provider, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            self.Proceed_Btn.enabled=true
            self.CardList_tableview.userInteractionEnabled=true

            if(error != nil)
            {
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

            }
                
            else
            {
                if(responseObject != nil)
                {
                    
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    let response:NSString=dict.objectForKey("response") as! NSString
                    
                    
                    
                    if (Staus == "1")
                    {
                        self.performSegueWithIdentifier("RatingVC", sender: nil)

                        
                        
                    }
                    else
                    {
                        
                        self.themes.AlertView("\(Appname)", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        
                        
                    }
                    
                    
                }
            }
        })
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
