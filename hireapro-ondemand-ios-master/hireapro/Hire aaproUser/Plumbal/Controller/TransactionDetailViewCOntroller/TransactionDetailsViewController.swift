//
//  TransactionDetailsViewController.swift
//  Plumbal
//
//  Created by Casperon on 07/02/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class TransactionDetailsViewController: RootViewController,UITableViewDelegate,UITableViewDataSource {
    var themes:Themes=Themes()
    let URL_Handler:URLhandler=URLhandler()
    var GetJob_id : NSString!
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet var detailview: UIView!
    @IBOutlet var taxamount: UILabel!
    @IBOutlet var taskamount: UILabel!
    @IBOutlet var Bookingidlabl: UILabel!
    
    @IBOutlet var mainview: UIView!
    @IBOutlet var tasktime: UILabel!
    @IBOutlet var minimum_rate: UILabel!
    @IBOutlet var perhour: UILabel!
    @IBOutlet var totalhours: UILabel!
    @IBOutlet var taskaddress: UILabel!
    @IBOutlet var taskername: UILabel!
    @IBOutlet var categoryname: UILabel!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var transacTableView: UITableView!
    var titleArray = NSMutableArray()
    var descArray = NSMutableArray()

    @IBOutlet var totalView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("view_task_dtl")
        let Nb=UINib(nibName: "TransactionDetailTableViewCell", bundle: nil)
        self.transacTableView.registerNib(Nb, forCellReuseIdentifier: "TransactionDetailTableViewCell")
        self.transacTableView.estimatedRowHeight = 20
        titleArray = ["Booking Id",
                      "Task Category",
                      "Provider Name",
                      "Task Address",
                      "Total Hours",
                      "Hourly Rate",
                      "Base Price",
                      "Task Time",
                      "Task Amount"
                      
            
        ]
        self.showProgress()

        
        
        totalView.layer.shadowOffset = CGSize(width: 2, height: 2)
       totalView.layer.shadowOpacity = 0.2
        totalView.layer.shadowRadius = 2
        mainview.layer.shadowOffset = CGSize(width: 2, height: 2)
        mainview.layer.shadowOpacity = 0.2
        mainview.layer.shadowRadius = 2

        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)

              self.GetTransaction()
        

        
        // Do any additional setup after loading the view.
    }

    @IBAction func backAct(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetTransaction()  {
        let param=["user_id":"\(themes.getUserID())","booking_id":"\(GetJob_id)"]
        URL_Handler.makeCall(constant.view_Transaction_details, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:"Network Failure", duration: 4, position: HRToastPositionDefault, title: "")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary

                        let TotalJobsArray : NSArray = ResponseDic.objectForKey("jobs") as! NSArray

                        let lat = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("location_lat"))!
                        let long = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("location_lng"))!

                        let categoryName = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("category_name"))!
                        let taskerName = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("user_name"))!
                        let taskAddress = self.getAddressForLatLng(lat, longitude: long)
                     
                        let totalHour = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("total_hrs"))!
                        let perHour = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("per_hour"))!)"
                         let basePrice = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("min_hrly_rate"))!)"
                        let taskTime = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("booking_time"))!
                        
                       let bookingId = "\(self.GetJob_id as String)"
                        let serviceAMt = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("task_amount"))!)"
                        let commision = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("service_tax"))!)"

                        let total = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("total_amount"))!)"
                        
                        let materialarray : NSArray = TotalJobsArray.objectAtIndex(0).objectForKey("meterial_details") as! NSArray
                        
                        var get_mis_amount = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("material_fee"))!)"
                        let mode = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("payment_mode"))!
                        if get_mis_amount == self.themes.getCurrencyCode(){
                            get_mis_amount = "---"
                        }
                       if  self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("category_type"))! == "Flat Rate"{
                        let perHour = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("flat_amount"))!)"
                        self.descArray = [bookingId,
                                          categoryName,
                                          taskerName,
                                          taskAddress,
                                          totalHour,
                                          perHour,
                                          taskTime,
                                          serviceAMt,
                                          
                        ]
                        self.titleArray = ["Booking Id",
                                      "Task Category",
                                      "Provider Name",
                                      "Task Address",
                                      "Total Hours",
                                      "Flat Rate",
                                      "Task Time",
                                      "Task Amount",
                                      
                            
                        ]


                       }else {
                        let perHour = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("per_hour"))!)"
                        let basePrice = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("min_hrly_rate"))!)"
                        self.descArray = [bookingId,
                                          categoryName,
                                          taskerName,
                                          taskAddress,
                                          totalHour,
                                          perHour,
                                          
                                          taskTime,
                                          serviceAMt
                                          
                        ]
                        self.titleArray = ["Booking Id",
                                      "Task Category",
                                      "Provider Name",
                                      "Task Address",
                                      "Total Hours",
                                      "Hourly Rate",
                                      
                                      "Task Time",
                                      "Task Amount"
                                      
                        ]


                    }
                        if materialarray.count > 0
                        {
                        
                        for (index, element) in materialarray.enumerate() {
                            
                            
                            var category = element as! NSDictionary
                            
                            
                            
                            let name = self.themes.CheckNullValue(category.valueForKey("name"))!
                            let price = "\(self.themes.getCurrencyCode())\(self.themes.CheckNullValue(category.valueForKey("price"))!)"
                            self.titleArray.addObject("\(self.themes.setLang("Tool")) \(index+1): \(name)")
                            self.descArray.addObject("\(price)")
                            
                            
                            
                        }
                        
                        }
                        
                        
                        let TotalCalArray : NSArray = ["Material Fee",
                                                       "Payment Mode",
                                                       "Service Fee",
                                                       "Total Amount"]
                        self.titleArray.addObjectsFromArray(TotalCalArray as [AnyObject])
                        
                        let TotalDecArray : NSArray = [ get_mis_amount,
                                                        mode,
                                                        commision,
                                                        total]
                        self.descArray.addObjectsFromArray(TotalDecArray as [AnyObject])
                        
                      
                        self.transacTableView.delegate = self
                        self.transacTableView.dataSource = self
                        self.transacTableView.reloadData()

                    }
                    else
                    {
                        let message:NSString=self.themes.CheckNullValue(responseObject!.objectForKey("response") as? NSString)!
                        self.themes.AlertView(Appname, Message: message, ButtonTitle: kOk)
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_reasons_available"), ButtonTitle: self.themes.setLang("ok"))
                }
                
            }
            
        }
        
    }
    func getAddressForLatLng(latitude: String, longitude: String)->String {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)")
        let data = NSData(contentsOfURL: url!)
        
        var fullAddress = ""
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address = result[0]["address_components"] as? NSArray {
                        print("get current location \(result[0]["address_components"])")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in address{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                            fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)"
                            if let address = result[0]["formatted_address"] as? String{
                                return address
                            }else{
                                return fullAddress
                            }
                        }
                    }
                }
            }
        }
        return ""
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCellWithIdentifier("TransactionDetailTableViewCell") as! TransactionDetailTableViewCell
        Cell.lblTitle.text = themes.setLang((titleArray.objectAtIndex(indexPath.row) as? String)!)
        Cell.lblDescL.text = descArray.objectAtIndex(indexPath.row) as? String
        Cell.lblTitle.sizeToFit()
        Cell.lblTitle.numberOfLines = 0
        if indexPath.row == 0 || indexPath.row == titleArray.count-1{
            
            
            Cell.lblTitle.textColor = UIColor.init(red: 32/250, green: 109/250, blue: 22/250, alpha: 1)
            Cell.lblDescL.textColor = UIColor.init(red: 32/250, green: 109/250, blue: 22/250, alpha: 1)
        }else{
            Cell.lblTitle.textColor = UIColor.darkGrayColor()
            Cell.lblDescL.textColor = UIColor.grayColor()
            
        }
        return Cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
