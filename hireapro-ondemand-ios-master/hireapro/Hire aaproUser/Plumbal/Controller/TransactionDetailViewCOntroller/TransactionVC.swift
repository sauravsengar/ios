//
//  TransactionVC.swift
//  Plumbal
//
//  Created by Casperon on 06/02/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class TransactionVC: RootViewController {
    var themes:Themes=Themes()
    let URL_Handler:URLhandler=URLhandler()
    var PageCount:NSInteger=0
    var refreshControl:UIRefreshControl=UIRefreshControl()
    var jobidArray : NSMutableArray = NSMutableArray()
    var categoryArray : NSMutableArray = NSMutableArray()
    var amountArray : NSMutableArray = NSMutableArray()

    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var transaction_table: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("transaction")
        let nibName = UINib(nibName: "transacationTableViewCell", bundle:nil)
        self.transaction_table.registerNib(nibName, forCellReuseIdentifier: "transactionCell")
        transaction_table.estimatedRowHeight = 80
        transaction_table.rowHeight = UITableViewAutomaticDimension
        self.showProgress()

    self.GetTransaction()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menubtnAction(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()

    }
    
    func GetTransaction()  {
        let param=["user_id":"\(themes.getUserID())"]
        URL_Handler.makeCall(constant.Get_Transaction, param: param) { (responseObject, error) -> () in
            self.DismissProgress()

            if(error != nil)
            {
                self.view.makeToast(message:"Network Failure", duration: 4, position: HRToastPositionDefault, title: "")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let TotalJobsArray : NSArray = ResponseDic.objectForKey("jobs") as! NSArray
                        
                        if TotalJobsArray.count == 0{
                            self.themes.AlertView(Appname, Message: self.themes.setLang("No transaction available"), ButtonTitle: kOk)

                        }
                        
                        for transacDict in TotalJobsArray
                        {
                            let jobid:NSString=self.themes.CheckNullValue(transacDict.objectForKey("job_id"))!
                            self.jobidArray.addObject(jobid)
                            let category:NSString=self.themes.CheckNullValue(transacDict.objectForKey("category_name"))!
                            self.categoryArray.addObject(category)
                            let amount:NSString=self.themes.CheckNullValue(transacDict.objectForKey("total_amount"))!
                            
                            self.amountArray.addObject("\(self.themes.getCurrencyCode())\(amount)")

                            
                        }
                        
                        self.transaction_table.reloadData()

                    }
                    else
                    {
                        let message:NSString=self.themes.CheckNullValue(responseObject!.objectForKey("response") as? NSString)!
                        self.themes.AlertView(Appname, Message: message, ButtonTitle: kOk)
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("No Datas available"), ButtonTitle: self.themes.setLang("ok"))
                }
            }
            
        }
    }
  
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jobidArray.count > 0{
            return self.jobidArray.count

        }
        else{
            return 0

        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("transactionCell") as! transacationTableViewCell
        
      Cell.totalview.layer.shadowOffset = CGSize(width: 2, height: 2)
       // Cell.totalview.layer.cornerRadius=14;
        Cell.totalview.layer.shadowOpacity = 0.2
        Cell.totalview.layer.shadowRadius = 2
          if jobidArray.count > 0
          {
      Cell.jobid.text = self.jobidArray.objectAtIndex(indexPath.row) as? String
         Cell.category.text = self.categoryArray.objectAtIndex(indexPath.row) as? String
         Cell.totalamount.text = self.amountArray.objectAtIndex(indexPath.row) as? String
        }
        
        
        
        return Cell
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let Controller:TransactionDetailsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("transDetail") as! TransactionDetailsViewController
        Controller.GetJob_id = self.jobidArray.objectAtIndex(indexPath.row) as! String
        self.navigationController?.pushViewController(Controller, animated: true)
    }
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
