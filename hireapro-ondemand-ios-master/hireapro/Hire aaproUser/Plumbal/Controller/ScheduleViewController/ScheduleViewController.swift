//
//  ScheduleViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 01/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import CoreLocation

class ScheduleViewController: RootViewController,CGCalendarViewDelegate,UITextFieldDelegate,AKPickerViewDataSource,AKPickerViewDelegate,UITextViewDelegate,MyPopupViewControllerDelegate {
    
    @IBOutlet var Month_Slot: UIButton!
    @IBOutlet var BookNow_Btn: CustomButton!
    @IBOutlet var calendarView: CGCalendarView!
    @IBOutlet var SelectDateTime_Lbl: UILabel!
    @IBOutlet var Schedule_Lbl: UIButton!
    @IBOutlet var Address_tableView: UITableView!
    @IBOutlet var Back_But: UIButton!
    @IBOutlet var scheduleDateView: UIView!
    @IBOutlet var Schedule_ScrollView: UIScrollView!
    
    var CustomList :NSString!
    var maincat : NSString!


    var alert = UIAlertController()
    var SelectedAddress:NSString=NSString()
    var calendar:NSCalendar?
    var Timearray:NSMutableArray=NSMutableArray()
    var ReferenceTimeArray:NSMutableArray=NSMutableArray()
    var ReferenceTimeArray1:NSMutableArray=NSMutableArray()
    var TimeDictionary:NSDictionary=NSDictionary()
    var Date_Formatter:NSDateFormatter=NSDateFormatter()
    var Confirmation_date:NSString=NSString()
    var Globalindex:NSString=NSString()
    var UpdatedAddress:NSString=NSString()
    var TextViewPlaceHolder = ""
    var fullAddress: NSString = NSString()

    var themes:Themes=Themes()
    var URL_Handler:URLhandler=URLhandler()
    var AddressIDArray:NSMutableArray=NSMutableArray()
    var NameArray:NSMutableArray=NSMutableArray()
    var EmailIDArray:NSMutableArray=NSMutableArray()
    var MobileNumArray:NSMutableArray=NSMutableArray()
    var CountryCodeArray:NSMutableArray=NSMutableArray()
    var StreetArray:NSMutableArray=NSMutableArray()
    var line1array: NSMutableArray = NSMutableArray()
    var stateArray : NSMutableArray = NSMutableArray()
    var countryArray: NSMutableArray = NSMutableArray()
    var LandmarkArray:NSMutableArray=NSMutableArray()
    var LocalityArray:NSMutableArray=NSMutableArray()
    var ZipCodeArray:NSMutableArray=NSMutableArray()
    var CityArray:NSMutableArray=NSMutableArray()
    var LongitudeArray:NSMutableArray = NSMutableArray()
    var LatitudeArray:NSMutableArray = NSMutableArray()
    var CompleteAddressArray:NSMutableArray=NSMutableArray()
    var CompleteListAddressArray : NSMutableArray = NSMutableArray()

    var Confirmed_Time:NSString=NSString()
    var Latitude:NSString=NSString()
    var Longitude:NSString=NSString()
    var SelectedAddressID:NSString=NSString()
    var height_Cell:CGFloat=CGFloat()
    var MonthYearStr:NSString=NSString()
    var  headerCell = AddressHeaderTableViewCell()
    var Reference_TimeArray_Modified:NSMutableArray=NSMutableArray()
    var InstructionTextField:UITextView=UITextView()
    
    var State=NSString()
    var country=NSString()

    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TextViewPlaceHolder=themes.setLang("enter_instruc")
        Schedule_Lbl.setTitle(themes.setLang("schedule_appointment"), forState: UIControlState.Normal)
        SelectDateTime_Lbl.text=themes.setLang("schedule_appointment")
        
    }
    
    override func viewDidDisappear(animated: Bool) {
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        setPage()
        InstructionTextField.textColor=PlumberThemeColor
        calendarView.calendar=calendar
        calendarView.rowCellClass=CGCalendarCell.classForCoder()
        calendarView.backgroundColor = UIColor.whiteColor()
        calendarView.firstDate = NSDate(timeIntervalSinceNow: -60 * 60 * 24 * 0)
        calendarView.lastDate = NSDate(timeIntervalSinceNow:60 * 60 * 24 * 1000)
        calendarView.delegate=self
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = PlumberThemeColor.CGColor
        border.frame = CGRect(x: 0, y: calendarView.frame.size.height - width, width:  414, height: calendarView.frame.size.height)
        border.borderWidth = width
        //calendarView.layer.addSublayer(border)
        calendarView.layer.masksToBounds = true
        Timearray=["8 AM - 9 AM","9 AM - 10 AM","10 AM - 11 AM","11 AM - 12 PM","12 PM - 1 PM","1 PM - 2 PM","2 PM - 3 PM","3 PM - 4 PM","4 PM - 5 PM","5 PM - 6 PM","6 PM - 7 PM","7 PM - 8 PM","8 PM - 9 PM"]
        ReferenceTimeArray1=["","","","","","","","","8 AM - 9 AM","9 AM - 10 AM","10 AM - 11 AM","11 AM - 12 PM","12 PM - 1 PM","1 PM - 2 PM","2 PM - 3 PM","3 PM - 4 PM","4 PM - 5 PM","5 PM - 6 PM","6 PM - 7 PM","7 PM - 8 PM","8 PM - 9 PM"]
        ReferenceTimeArray=["12 AM","01 AM","02 AM","03 AM","04 AM","05 AM","06 AM","07 AM","08 AM","09 AM","10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM","07 PM","08 PM"]
        Reference_TimeArray_Modified=["12 AM","01 AM","02 AM","03 AM","04 AM","05 AM","06 AM","07 AM","08 AM","09 AM","10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM","07 PM","08 PM"]
        TimeDictionary=["8 AM":"8 AM - 9 AM","9 AM":"9 AM - 10 AM","10 AM":"10 AM - 11 AM","11 AM":"11 AM - 12 PM","12 PM":"12 AM - 1 AM","1 PM":"1 PM - 2 PM","2 PM":"2 PM - 3 PM","3 PM":"3 PM - 4 PM","4 PM":"4 PM - 5 PM","5 PM":"5 PM - 6 PM","6 PM":"6 PM - 7 PM","7 PM":"7 PM - 8 PM","8 PM":"8 PM - 9 PM"]
        let Registernib=UINib(nibName: "AddressTableViewCell", bundle: nil)
        Address_tableView.registerNib(Registernib, forCellReuseIdentifier: "AddressCell")
        headerCell = Address_tableView.dequeueReusableCellWithIdentifier("AddressHeader") as! AddressHeaderTableViewCell
        Address_tableView.separatorColor=UIColor.clearColor()
        
        self.Date_Formatter=NSDateFormatter()
        self.Date_Formatter.dateFromString("yyyy-MM-dd")
        //Set image for backarraow
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        Back_But.addSubview(themes.Back_ImageView)
        Schedule_Data.Schedule_header=themes.setLang("choose_address")
        Address_tableView.scrollEnabled = false
        self.SetFrameAccordingToSegmentIndex()
        loadAddress()
        
        if self.Latitude == "" || self.Longitude == ""
        {
            
        }
        else{
            self.getAddressForLatLng(self.Latitude as String, longitude: self.Longitude as String)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - Function
    
    func SetFrameAccordingToSegmentIndex(){
        dispatch_async(dispatch_get_main_queue()) {
            var frame: CGRect = self.Address_tableView.frame
            frame.size.height = self.Address_tableView.contentSize.height;
            self.Address_tableView.frame = frame;
            if(self.AddressIDArray.count != 0) {
                frame.size.height = self.Address_tableView.contentSize.height+30;
            }else {
                frame.size.height = self.Address_tableView.contentSize.height+20;
            }
            self.Address_tableView.frame = frame;
            self.Schedule_ScrollView.contentSize=CGSizeMake(self.Schedule_ScrollView.frame.size.width, self.Address_tableView.frame.origin.y+self.Address_tableView.frame.size.height)
        }
    }
    
    func setPage(){
        scheduleDateView.layer.borderWidth = 1
        scheduleDateView.layer.borderColor = PlumberThemeColor.CGColor
        scheduleDateView.layer.cornerRadius = 5
    }
    func loadAddress(){
        self.showProgress()
        self.CompleteListAddressArray = NSMutableArray()
        self.AddressIDArray = NSMutableArray()
        self.NameArray = NSMutableArray()
        self.EmailIDArray = NSMutableArray()
        self.CountryCodeArray = NSMutableArray()
        self.MobileNumArray = NSMutableArray()
        self.StreetArray = NSMutableArray()
        self.line1array = NSMutableArray()
        self.LandmarkArray = NSMutableArray()
        self.stateArray = NSMutableArray()
        self.countryArray = NSMutableArray()
        self.LatitudeArray = NSMutableArray()
        self.LongitudeArray = NSMutableArray()
        self.ZipCodeArray = NSMutableArray()
        Address_tableView.hidden=true
        BookNow_Btn.enabled=false
        if CustomList == "1"
        {
            BookNow_Btn.setTitle(themes.setLang("Conf_Booking"), forState: UIControlState.Normal)

        }
        else{
            BookNow_Btn.setTitle(themes.setLang("search_now"), forState: UIControlState.Normal)

        }
        
        
        let param:NSDictionary=["user_id":"\(themes.getUserID())"]
        
        
        URL_Handler.makeCall(constant.List_address, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.BookNow_Btn.enabled=true
            self.Address_tableView.hidden=false
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            } else {
                if(responseObject != nil){
                    let Dict:NSDictionary=responseObject!
                    let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    if(Status == "1"){
                        Schedule_Data.ScheduleAddressNameArray = NSMutableArray ()
                        Schedule_Data.scheduleAddressid = NSString ()
                        self.Address_tableView.hidden=false
                        let AddressArray:NSArray=Dict.objectForKey("response") as! NSArray
                        for AddressDictionary in AddressArray{
                            let address_name=self.themes.convertIntToString(AddressDictionary.objectForKey("address_name") as! Int)
                            self.AddressIDArray.addObject(address_name)
                            let name=self.themes.CheckNullValue( AddressDictionary.objectForKey("name"))!
                            self.NameArray.addObject(name)
                            let email=self.themes.CheckNullValue(AddressDictionary.objectForKey("email"))!
                            self.EmailIDArray.addObject(email)
                            let country_code=self.themes.CheckNullValue(AddressDictionary.objectForKey("country_code"))!
                            self.CountryCodeArray.addObject(country_code)
                            let mobile=self.themes.CheckNullValue(AddressDictionary.objectForKey("mobile"))!
                            self.MobileNumArray.addObject(mobile)
                            let line1str = self.themes.CheckNullValue(AddressDictionary.objectForKey("line1"))!
                            self.line1array.addObject(line1str)
                            let street=self.themes.CheckNullValue(AddressDictionary.objectForKey("street"))!
                            self.StreetArray.addObject(street)
                            let city=self.themes.CheckNullValue(AddressDictionary.objectForKey("city"))!
                            self.CityArray.addObject(city)
                            let statestr = self.themes.CheckNullValue(AddressDictionary.objectForKey("state"))!
                            self.stateArray.addObject(statestr)
                            let countrystr = self.themes.CheckNullValue(AddressDictionary.objectForKey("country"))!
                            self.countryArray.addObject(countrystr)
                            let landmark=self.themes.CheckNullValue(AddressDictionary.objectForKey("landmark"))!
                            self.LandmarkArray.addObject(landmark)
                            let zipcode=self.themes.CheckNullValue(AddressDictionary.objectForKey("zipcode"))!
                            self.ZipCodeArray.addObject(zipcode)
                            let lng=self.themes.CheckNullValue(AddressDictionary.objectForKey("lng"))!
                            self.LongitudeArray.addObject(lng)
                            let lat=self.themes.CheckNullValue(AddressDictionary.objectForKey("lat"))!
                            self.LatitudeArray.addObject(lat)
                            let Locality=self.themes.CheckNullValue(AddressDictionary.objectForKey("locality"))!
                            
                            var CompleteAddress : String = "\(name)\n"+"\(line1str)\n"+"\(street)\n"+"\(landmark)\n"+"\(city)\n"+"\(Locality)\n\(zipcode)"
                            CompleteAddress = CompleteAddress.stringByReplacingOccurrencesOfString("\n \n", withString:"\n")
                            CompleteAddress = CompleteAddress.stringByReplacingOccurrencesOfString("\n\n\n", withString:"\n")
                            CompleteAddress = CompleteAddress.stringByReplacingOccurrencesOfString("\n\n", withString: "\n")
                            self.CompleteListAddressArray.addObject(CompleteAddress)
                            
                            
                        }
                        Schedule_Data.ScheduleAddressNameArray = self.AddressIDArray
                        
                        
                    } else{
                        
                        
                    }
                    
                    
                } else{
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }
    }
    
    func getAddressForLatLng(latitude: String, longitude: String){
        self.CompleteAddressArray = NSMutableArray()
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)&language=\(kLanguage)")
        let data = NSData(contentsOfURL: url!)
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    var result1 = NSArray()
                    if kLanguage == "ta"{
                        if let address = result[1]["address_components"] as? NSArray{
                            result1 = address
                        }
                    }else{
                        if let address = result[0]["address_components"] as? NSArray{
                            result1 = address
                        }
                        
                    }
                    if result1.count != 0 {
                        print("get current location \(result[1]["address_components"])")
                        var street = ""
                        var sublocality = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in result1{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }
                            else if (item1.objectAtIndex(0) as! String == "political" || item1.objectAtIndex(0) as! String == "sublocality" || item1.objectAtIndex(0) as! String == "sublocality_level_1")
                            {
                                let city1 = item["long_name"]
                                sublocality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                        }
                        fullAddress = "\(street)\n"+"\(sublocality)\n"+"\(city)\n"+"\(state)\n"+"\n\(country)"+"\n\(zipcode)"
                        fullAddress = fullAddress.stringByReplacingOccurrencesOfString("\n \n", withString:"\n")
                        fullAddress = fullAddress.stringByReplacingOccurrencesOfString("\n\n\n", withString:"\n")
                        fullAddress = fullAddress.stringByReplacingOccurrencesOfString("\n\n", withString: "\n")
                        self.CompleteAddressArray.addObject(fullAddress)
                        self.height_Cell = self.themes.calculateHeightForString("\(fullAddress))")
                        
                        self.UpdatedAddress=fullAddress
                        self.SelectedAddress=fullAddress
                        self.SelectedAddressID=""
                        Schedule_Data.scheduleAddressid = ""
                        self.Address_tableView.reloadData()
                        
                        self.SetFrameAccordingToSegmentIndex()
                        
                        
                    }
                }
            }
        }
        
    }
    func applicationLanguageChangeNotification(notification:NSNotification){
        Schedule_Lbl.setTitle(themes.setLang("schedule_appointment"), forState: UIControlState.Normal)
        SelectDateTime_Lbl.text=themes.setLang("select_date&timeSlot")
    }
    
    func ShowmoreAddress() {
        
        if self.AddressIDArray.count > 0
        {
            
            Schedule_Data.ScheduleAddressArray = NSMutableArray ()
            Schedule_Data.ScheduleLatitudeArray = NSMutableArray()
            Schedule_Data.ScheduleLongtitudeArray = NSMutableArray()
            if(CompleteListAddressArray.count != 0){
                Schedule_Data.ScheduleAddressArray=CompleteListAddressArray
                Schedule_Data.ScheduleLatitudeArray = LatitudeArray
                Schedule_Data.ScheduleLongtitudeArray = LongitudeArray
            }
            
            self.displayViewController(.BottomBottom)
        }
        else{
            self.performSegueWithIdentifier("AddaddressVC", sender: nil)
        }
        
        // NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        let myPopupViewController:MyPopupViewController = MyPopupViewController(nibName:"MyPopupViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.Globalindex =  self.SelectedAddressID
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
        })
    }
    
    func doneButtonAction() {
        Schedule_ScrollView.scrollEnabled = true
        view.endEditing(true)
        Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func PushtoAddadrressVC(sender:UITapGestureRecognizer){
        if (themes.getEmailID() == "") {   themes.saveaddresssegue("1")
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")
        } else {
            self.performSegueWithIdentifier("AddaddressVC", sender: nil)
        }
    }
    
    func Add_address()
    {
          var  param:NSDictionary = NSDictionary()
        let tempAddArray = fullAddress.componentsSeparatedByString("\n")
        for item in tempAddArray{
            print("address item\(item)")
        }
        if tempAddArray.count > 5

        {
            param = ["user_id":"\(self.themes.getUserID())","name":"\(themes.getUserName())","email":"\(themes.getEmailID())",
            "country_code":"\(themes.getCountryCode())","mobile":"\(themes.getMobileNum())","street":"\( tempAddArray[0])",
            "landmark":"","locality":"\(tempAddArray[1])","city":"\(tempAddArray[2])",
            "zipcode":"\(tempAddArray[5])","lng":"\(self.Longitude)",
            "line1":"","state":"\(tempAddArray[3])","lat":"\(self.Latitude)","country":"\(tempAddArray[4])"]
        }
        else if tempAddArray.count > 4 {
            
            param = ["user_id":"\(self.themes.getUserID())","name":"\(themes.getUserName())","email":"\(themes.getEmailID())",
                   "country_code":"\(themes.getCountryCode())","mobile":"\(themes.getMobileNum())","street":"\( tempAddArray[0])",
                   "landmark":"","city":"\(tempAddArray[1])",
                   "zipcode":"\(tempAddArray[4])","lng":"\(self.Longitude)",
                   "line1":"","state":"\(tempAddArray[2])","lat":"\(self.Latitude)","country":"\(tempAddArray[3])"]

        }
        else{
            
            param = ["user_id":"\(self.themes.getUserID())","name":"\(themes.getUserName())","email":"\(themes.getEmailID())",
                     "country_code":"\(themes.getCountryCode())","mobile":"\(themes.getMobileNum())","street":"\( tempAddArray[0])",
                     "landmark":"",
                     "zipcode":"\(tempAddArray[3])","lng":"\(self.Longitude)",
                     "line1":"","state":"\(tempAddArray[1])","lat":"\(self.Latitude)","country":"\(tempAddArray[2])"]
            
        }
        
            URL_Handler.makeCall(constant.Add_address, param: param, completionHandler: { (responseObject, error) -> () in
                print("the erroe is \(error)")
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                    
                }
                    
                else
                {
                    if(responseObject != nil)
                    {
                        let dict:NSDictionary=responseObject!
                        
                        let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        
                        if(Status == "1")
                        {
                            
                        }
                        else
                        {
                            self.themes.AlertView(Appname, Message: "\(self.themes.CheckNullValue(dict.objectForKey("errors"))!)", ButtonTitle: kOk)
                            
                        }
                        
                    }
                        
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
            
            
        }
        
    
    func bookingconfim(){
        
        
        if Schedule_Data.scheduleAddressid == ""
        {
        }else{
    
            self.UpdateAddress( Int(Schedule_Data.scheduleAddressid as String)!)
    
        }
        BookNow_Btn.enabled=false
        self.showProgress()
        let dateAsString = Confirmed_Time
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "h a"
        let date = dateFormatter.dateFromString(dateAsString as String)
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.stringFromDate(date!)
        Schedule_Data.RquiredAddressid = SelectedAddressID
        Schedule_Data.PickupDate = Confirmation_date
        Schedule_Data.pickupTime = date24
        Schedule_Data.GetScheduleIstr = InstructionTextField.text
        Schedule_Data.getLatitude = self.Latitude
        Schedule_Data.getLongtitude = self.Longitude
        
        if Category_Data.CategoryID == ""{
            let Prefcategory : NSUserDefaults = NSUserDefaults.standardUserDefaults()
            //Category_Data.CategoryID = Prefcategory.objectForKey("maincategory") as! NSString
        }
        let param=["user_id":"\(themes.getUserID())","address_name":"\(Schedule_Data.scheduleAddressid )","pickup_date":"\(Confirmation_date)","pickup_time":"\(date24)","instruction":"\(InstructionTextField.text!)","code":"","category":"\(Home_Data.Category_id)","service":"\(maincat)","lat":self.Latitude,"long":self.Longitude]
        
        
        
        URL_Handler.makeCall(constant.Book_It, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            self.BookNow_Btn.enabled=true
            if(error != nil) {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }  else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    if(responseObject != nil){
                        if(Status == "1") {
                            self.Add_address()
                            let responseArray:NSMutableArray=dict.objectForKey("response") as! NSMutableArray
                            let taskID:NSString=dict.objectForKey("task_id") as! NSString
                            Schedule_Data.TaskID="\(taskID)"
                            Schedule_Data.ProviderListIdArray.removeAllObjects()
                            Schedule_Data.ProviderListNameArray.removeAllObjects()
                            Schedule_Data.ProviderListImageArray.removeAllObjects()
                            Schedule_Data.ProviderListAvailableArray.removeAllObjects()
                            Schedule_Data.ProviderListCompanyArray.removeAllObjects()
                            Schedule_Data.ProviderListMinamountArray.removeAllObjects()
                            Schedule_Data.ProviderListHouramountArray.removeAllObjects()
                            Schedule_Data.ProviderListRatingArray.removeAllObjects()
                            Schedule_Data.ProviderListCategoryTypeArray.removeAllObjects()
                            Schedule_Data.ProviderListFlatamountArray.removeAllObjects()
                            Schedule_Data.ProviderListBadgeArray.removeAllObjects()
                            
                            
                            if(responseArray.count != 0){
                                for Dictionary in responseArray {
                                    let job_id:NSString=Dictionary.objectForKey("taskerid") as! NSString
                                    Schedule_Data.ProviderListIdArray.addObject(job_id)
                                    let Name:NSString=Dictionary.objectForKey("name") as! NSString
                                    Schedule_Data.ProviderListNameArray.addObject(Name)
                                    let service_icon:NSString=Dictionary.objectForKey("image_url") as! NSString
                                    Schedule_Data.ProviderListImageArray.addObject(service_icon)
                                    let available:NSString=Dictionary.objectForKey("availability") as! NSString
                                    Schedule_Data.ProviderListAvailableArray.addObject(available)
                                    let company:NSString=Dictionary.objectForKey("company") as! NSString
                                    Schedule_Data.ProviderListCompanyArray.addObject(company)
                                    let rating=self.themes.convertFloatToString(Dictionary.objectForKey("rating") as! Float)
                                    Schedule_Data.ProviderListRatingArray.addObject(rating)
                                    let min_amount=self.themes.CheckNullValue(Dictionary.objectForKey("min_amount"))!
                                    Schedule_Data.ProviderListMinamountArray.addObject(min_amount)
                                    let hour_amount=self.themes.CheckNullValue(Dictionary.objectForKey("hourly_amount"))!
                                    
                                    Schedule_Data.ProviderListHouramountArray.addObject(hour_amount)
                                    let flat_amount=self.themes.CheckNullValue(Dictionary.objectForKey("flat_amount"))!
                                    Schedule_Data.ProviderListFlatamountArray.addObject(flat_amount)
                                    let category_type=self.themes.CheckNullValue(Dictionary.objectForKey("category_type"))!
                                    Schedule_Data.ProviderListCategoryTypeArray.addObject(category_type)
                                    
                                    let reviews=self.themes.CheckNullValue(Dictionary.objectForKey("reviews"))
                                    Schedule_Data.ProviderLisreviewsArray.addObject(reviews!)
                                    let dist=self.themes.CheckNullValue(Dictionary.objectForKey("distance_mile"))
                                    Schedule_Data.ProviderdistanceArray.addObject(dist!)
                                    
                                    let badge=Dictionary.objectForKey("badges") as! NSArray
                                    Schedule_Data.ProviderListBadgeArray.addObject(badge)
                                    
                                }
                                
                                
                                Schedule_Data.tasker_lat = self.Latitude as String
                                Schedule_Data.tasker_lng = self.Longitude as String

                                print("get hour amount array\(Schedule_Data.ProviderListHouramountArray)")
                                self.performSegueWithIdentifier("ProviderConfirmVC", sender: nil)//OrderConfirmVC
                            }
                        }  else {
                            let response:NSString=dict.objectForKey("response") as! NSString
                            self.themes.AlertView("\(Appname) ", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        }
                    }
                }
                else {
                    self.themes.AlertView("Sorry for the inconvenience ", Message: "Please try again", ButtonTitle: self.themes.setLang("ok"))
                }
            }
        })
    }
    
    func applyCoupon(let Couponstr:String){
        BookNow_Btn.enabled=false
        if(Couponstr == ""){
            self.themes.AlertView("\(Appname)", Message: themes.setLang("enter_coupen"), ButtonTitle: kOk)
        } else {
            let param=["user_id":"\(themes.getUserID())","code":"\(Couponstr)","pickup_date":"\(Confirmation_date)"]
            URL_Handler.makeCall(constant.Coupon_Call, param: param, completionHandler: { (responseObject, error) -> () in
                self.BookNow_Btn.enabled=true
                if(error != nil) {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                } else {
                    if(responseObject != nil) {
                        let Dict:NSDictionary=responseObject!
                        print("the object is \(responseObject)")
                        let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                        if(Status == "1") {
                            self.alert.dismissViewControllerAnimated(true, completion: nil)
                        }else {
                            let Response:NSString=Dict.objectForKey("message")as! NSString
                            self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                        }
                    }  else {
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("invalid_coupon"), ButtonTitle: kOk)
                    }
                }
            })
        }
    }
    
    func pressAdd(sender: MyPopupViewController) {
        self.dismissPopupViewController(.BottomBottom)
        self.performSegueWithIdentifier("AddaddressVC", sender: nil)
    }
    
    func pressCancel(sender: MyPopupViewController) {
        self.dismissPopupViewController(.BottomBottom)
        self.loadAddress()
    }
    
    
    func PassSelectedAddress(Address: NSString, AddressIndexvalue: Int, latitudestr: NSString, longtitudestr: NSString, localitystr: NSString) {
        UpdatedAddress=Address
        self.Latitude = latitudestr
        self.Longitude = longtitudestr
        print("the latitude =\(AddressIndexvalue) and longtitude =\(longtitudestr)")
        self.UpdateAddress(AddressIndexvalue)
        dispatch_async(dispatch_get_main_queue(), {
            self.Address_tableView.reloadData()
        })
        self.dismissPopupViewController(.BottomBottom)
    }
    
    func  UpdateAddress(addressindex: Int){
        let Param: Dictionary = ["user_id":themes.getUserID(),"address_name":"\(addressindex)"]
        URL_Handler.makeCall(constant.List_address, param: Param) {
            (responseObject, error) -> () in
            if(error != nil){
            }
            else{
                if(responseObject != nil && responseObject?.count>0) {
                    let status:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1"){
                    }
                    else
                    {
                    }
                }
            }
        }
    }
    
    //MARK: - TableView Delegate
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerCell.Timepicker.delegate = self
        headerCell.Timepicker.dataSource = self
        headerCell.Timepicker.hidden = false
        if  CompleteAddressArray.count != 0{
            headerCell.AddAddress_View.hidden = true
            headerCell.backgroundColor = UIColor.clearColor()
        }  else {
            let Tap:UITapGestureRecognizer=UITapGestureRecognizer()
            Tap.addTarget(self, action: #selector(ScheduleViewController.PushtoAddadrressVC(_:)))
            headerCell.AddAddress_View.addGestureRecognizer(Tap)
        }
        return headerCell
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(CompleteAddressArray.count != 0) {
            return 80
        }else {
            return 127
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(CompleteAddressArray.count != 0){
            height_Cell = self.themes.calculateHeightForString("\(UpdatedAddress)")
            if(indexPath.row == 0) {
                if(CompleteAddressArray.count == 1) {
                    return height_Cell + 70.0
                } else {
                    return height_Cell + 100.0
                }
            } else {
                return 150
            }
        }else {
            return 150
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(CompleteAddressArray.count != 0) {
            return 2
        } else{
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell:AddressTableViewCell = tableView.dequeueReusableCellWithIdentifier("AddressCell") as! AddressTableViewCell
        Cell.selectionStyle = .None
        Cell.More_address_btn.addTarget(self,action:#selector(ScheduleViewController.ShowmoreAddress),forControlEvents:UIControlEvents.TouchUpInside)
        if(CompleteAddressArray.count != 0) {
            if(indexPath.row == 0) {
                Cell.DeleteIcon.hidden=true
                InstructionTextField.hidden=true
                Cell.Address_Label.hidden=false
                Cell.More_address_btn.hidden=false
                Cell.More_address_btn.setTitle(themes.setLang("ur_address"), forState: UIControlState.Normal)
                Cell.More_icon.hidden=false
                Cell.backgroundColor=UIColor.whiteColor()
                Cell.Address_Label.text="\(UpdatedAddress)"
                let height:CGFloat = self.themes.calculateHeightForString("\(UpdatedAddress)")
                Cell.Address_Label.frame.size.height=height+40
            } else if(indexPath.row == 1) {
                InstructionTextField.hidden=false
                Cell.Address_Label.hidden=true
                Cell.More_address_btn.hidden=true
                Cell.More_icon.hidden=true
                Cell.DeleteIcon.hidden=true
                Cell.backgroundColor=UIColor.clearColor()
                InstructionTextField.frame=CGRectMake(4, 20, self.view.frame.width-13, 150)
                InstructionTextField.backgroundColor=UIColor.whiteColor()
                InstructionTextField.delegate=self
                InstructionTextField.layer.borderWidth=1.0
                InstructionTextField.layer.cornerRadius = 5
                InstructionTextField.layer.borderColor=PlumberThemeColor.CGColor
                InstructionTextField.font=PlumberMediumFont
                var done_Toolbar: UIToolbar=UIToolbar()
                done_Toolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
                done_Toolbar.backgroundColor=UIColor.whiteColor()
                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
                let done: UIBarButtonItem = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Done, target: self, action: #selector(ScheduleViewController.doneButtonAction))
                done_Toolbar.items = [flexSpace,done]
                done_Toolbar.sizeToFit()
                InstructionTextField.inputAccessoryView = done_Toolbar
                Cell.addSubview(InstructionTextField)
                if(InstructionTextField.text == "") {
                    InstructionTextField.text=TextViewPlaceHolder
                }
            }
        } else {
            Cell.Address_Label.hidden=true
            Cell.More_address_btn.hidden=true
            Cell.More_icon.hidden=true
            Cell.DeleteIcon.hidden=true
            Cell.backgroundColor=UIColor.clearColor()
            InstructionTextField.frame=CGRectMake(2, 6, self.view.frame.width-15, 120)
            InstructionTextField.backgroundColor=UIColor.whiteColor()
            InstructionTextField.delegate=self
            InstructionTextField.layer.borderWidth=1.0
            InstructionTextField.layer.borderColor=PlumberThemeColor.CGColor
            InstructionTextField.font=UIFont.init(name: "Raleway-Regular", size: 16.0)
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
            doneToolbar.barStyle = UIBarStyle.Default
            doneToolbar.backgroundColor=UIColor.whiteColor()
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Done, target: self, action: #selector(ScheduleViewController.doneButtonAction))
            doneToolbar.items = [flexSpace,done]
            doneToolbar.sizeToFit()
            InstructionTextField.inputAccessoryView = doneToolbar
            Cell.addSubview(InstructionTextField)
            if(InstructionTextField.text == "") {
                InstructionTextField.text=TextViewPlaceHolder
            }
        }
        return Cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Globalindex="\(indexPath.row)"
        SelectedAddress=UpdatedAddress
        dispatch_async(dispatch_get_main_queue(), {
            self.Address_tableView.reloadData()
        })
        self.Address_tableView.reloadData()
        
    }
    
    //MARK:- TextView Delegate
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        Schedule_ScrollView.scrollEnabled = false
        if(AddressIDArray.count != 0)  {
            if(themes.screenSize.height == 480){
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 460), animated: true)
            }
            if(themes.screenSize.height == 568) {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 395), animated: true)
            }
            if(themes.screenSize.height == 667) {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 330), animated: true)
            }
            if(themes.screenSize.height == 736) {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
            }
        } else {
            
            if(themes.screenSize.height == 480)
            {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
            }
            
            if(themes.screenSize.height == 568)
            {
                
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 220), animated: true)
            }
            
            if(themes.screenSize.height == 667)
            {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 220), animated: true)
            }
            
            if(themes.screenSize.height == 736)
            {
                Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 190), animated: true)
            }
            
            
        }
        if(InstructionTextField.text == TextViewPlaceHolder) {
            InstructionTextField.textColor=UIColor.blackColor()
            InstructionTextField.text=""
        }
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        Schedule_ScrollView.scrollEnabled=true
        Schedule_ScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        if(InstructionTextField.text == "") {
            InstructionTextField.textColor=PlumberThemeColor
            InstructionTextField.text=TextViewPlaceHolder
        }
        return true
    }
    
    
    //MARK: - PickerView Delegates
    
    func numberOfItemsInPickerView(pickerView: AKPickerView!) -> UInt {
        return UInt(Timearray.count)
    }
    
    func pickerView(pickerView: AKPickerView!, titleForItem item: Int) -> String! {
        return Timearray[item] as! String
    }
    
    func pickerView(pickerView: AKPickerView, didSelectItem item: Int) {
        Confirmed_Time="\(Timearray[item])"
        Confirmed_Time="\(Reference_TimeArray_Modified[item])"
    }
    
    //MARK: -  CGCalendar View Delegate
    func calendarView(calendarView: CGCalendarView!, didSelectDate date: NSDate!) {
        let components: NSDateComponents = NSCalendar.currentCalendar().components(NSCalendarUnit.Day.union(NSCalendarUnit.Month).union(NSCalendarUnit.Year), fromDate: date)
        let monthNumber: Int = components.month
        let Year:Int=components.year
        Confirmation_date="\(components.month)/\(components.day)/\(components.year)"
        let dateAsString = Confirmation_date
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "M/dd/yyyy"
        let date = dateFormatter.dateFromString(dateAsString as String)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        Confirmation_date = dateFormatter.stringFromDate(date!)
        print("the date issssss \(Confirmation_date)")
        let df: NSDateFormatter = NSDateFormatter()
        let monthName: String = df.monthSymbols[(monthNumber - 1)]
        MonthYearStr = "\(monthName), \(Year)"
        Month_Slot.setTitle("\(MonthYearStr)", forState: .Normal)
        Month_Slot.setTitleColor(UIColor.orangeColor(), forState:UIControlState.Normal)
        let HourFromat:NSLocale=NSLocale(localeIdentifier: "en_US_POSIX")
        df.locale = HourFromat
        df.dateFormat = "a"
        dateFormatter.dateFormat = "hh"
        var dateComp = dateFormatter.stringFromDate(NSDate()).uppercaseString
        let currentAMPMFormat = df.stringFromDate(NSDate()).uppercaseString
        let currentdate:NSDate=NSDate()
        let datefromatter:NSDateFormatter=NSDateFormatter()
        datefromatter.dateFormat = "d.M.yyyy";
        let CurrentdateString:NSString=datefromatter.stringFromDate(currentdate)
        let SelecteddateString:NSString=datefromatter.stringFromDate(date!)
        if(CurrentdateString == SelecteddateString) {
            
            switch dateComp{
            case "13":
                dateComp = "01"
                break;
            case "14":
                dateComp = "02"
                break;
            case "15":
                dateComp = "03"
                break;
            case "16":
                dateComp = "04"
                break;
            case "17":
                dateComp = "05"
                break;
            case "18":
                dateComp = "06"
                break;
            case "19":
                dateComp = "07"
                break;
            case "20":
                dateComp = "08"
                break;
            case "21":
                dateComp = "09"
                break;
            case "22":
                dateComp = "10"
                break;
            case "23":
                dateComp = "11"
                break;
            case "24":
                dateComp = "12"
                break;
            default:
                break;
            }
            
            let CurrentHour:NSString="\(dateComp) \(currentAMPMFormat)"
            
            if(ReferenceTimeArray.containsObject(CurrentHour))
            {
                let indexpath:NSInteger=ReferenceTimeArray.indexOfObject(CurrentHour)
                
                print("get indexpath of  refeerane time\(indexpath)")
                
                if indexpath > 8
                    
                {
                    if(Timearray.count != 0)
                    {
                        Timearray.removeAllObjects()
                        Reference_TimeArray_Modified.removeAllObjects()
                    }
                    for var i=indexpath+1;i<ReferenceTimeArray.count;i += 1
                    {
                        Reference_TimeArray_Modified.addObject("\(ReferenceTimeArray[i])")
                        Timearray.addObject("\(ReferenceTimeArray1[i])")
                    }
                    
                }
                else
                {
                    ReferenceTimeArray1=["8 AM - 9 AM","9 AM - 10 AM","10 AM - 11 AM","11 AM - 12 PM","12 PM - 1 PM","1 PM - 2 PM","2 PM - 3 PM","3 PM - 4 PM","4 PM - 5 PM","5 PM - 6 PM","6 PM - 7 PM","7 PM - 8 PM","8 PM - 9 PM"]
                    ReferenceTimeArray=["08 AM","09 AM","10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM","07 PM","08 PM"]
                    
                    
                    if(Timearray.count != 0)
                    {
                        Timearray.removeAllObjects()
                        Reference_TimeArray_Modified.removeAllObjects()
                    }
                    for i in 0 ..< ReferenceTimeArray.count
                    {
                        Reference_TimeArray_Modified.addObject("\(ReferenceTimeArray[i])")
                    }
                    
                    for i in 0 ..< ReferenceTimeArray1.count
                    {
                        Timearray.addObject("\(ReferenceTimeArray1[i])")
                    }
                }
                
            }
            else
            {
                Timearray.removeAllObjects()
                Reference_TimeArray_Modified.removeAllObjects()
                themes.AlertView(self.themes.setLang("Sorry no time slot available today"), Message: self.themes.setLang("Please choose another date"), ButtonTitle: self.themes.setLang("ok"))
                
            }
            
            Timearray.removeObject("")
            headerCell.Timepicker.reloadData()
            
        }else{
            Timearray=["8 AM - 9 AM","9 AM - 10 AM","10 AM - 11 AM","11 AM - 12 PM","12 PM - 1 PM","1 PM - 2 PM","2 PM - 3 PM","3 PM - 4 PM","4 PM - 5 PM","5 PM - 6 PM","6 PM - 7 PM","7 PM - 8 PM","8 PM - 9 PM"]
            Reference_TimeArray_Modified=["08 AM","09 AM","10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM","07 PM","08 PM"]
            headerCell.Timepicker.reloadData()
        }
        
        print("\(Reference_TimeArray_Modified)>>>>>>>>")
        if Reference_TimeArray_Modified.count > 0 {
            Confirmed_Time = "\(Reference_TimeArray_Modified[0])"
        }    }
    
    //MARK: - Button Action
    
    @IBAction func didClickOptions(sender: UIButton) {
        
        let InstructionField_Data:NSString=InstructionTextField.text!
        let whitespace:NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        let trimmed:NSString =  InstructionField_Data.stringByTrimmingCharactersInSet(whitespace)
        if(sender.tag == 0) {
            
            if CustomList == "1"
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            else
            {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "HomePageVCID")
            }
        }
        if(sender.tag == 2){
            if(Confirmation_date == ""){
                themes.AlertView(Appname, Message: themes.setLang("choose_date"), ButtonTitle: kOk)
            }else if(Confirmed_Time == "") {
                themes.AlertView(Appname, Message: themes.setLang("choose_time"), ButtonTitle: kOk)
            } else if(CompleteAddressArray.count == 0)  {
                if themes.getEmailID() == ""{
                    themes.saveaddresssegue("1")
                    Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")
                } else{
                    self.performSegueWithIdentifier("AddaddressVC", sender: nil)
                }
            } else if(trimmed == "" || InstructionField_Data == TextViewPlaceHolder){
                themes.AlertView(Appname, Message: themes.setLang("enter_instruc"), ButtonTitle: kOk)
            } else{
                
             if CustomList == "1"
              
             {
                let AlertView:UIAlertView=UIAlertView()
                AlertView.delegate=self
                AlertView.title="Please Click Confirm To Book This Provider"
                AlertView.addButtonWithTitle("Confirm")
                AlertView.addButtonWithTitle("Cancel")
                
                AlertView.show()
                AlertView.tag = sender.tag
                                
                
                
             }
                else
               
             {
                self.bookingconfim()
               
                }
            }
        }
    }
    
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
        switch buttonIndex{
        case 0:
           self.ListviewBooking()
            break;
        default:
            break;
            //Some code here..
            
        }
    }

    
    func ListviewBooking ()  {
        
        
        
        self.showProgress()
        let dateAsString = Confirmed_Time
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "h a"
        let date = dateFormatter.dateFromString(dateAsString as String)
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.stringFromDate(date!)
        Schedule_Data.RquiredAddressid = SelectedAddressID
        Schedule_Data.PickupDate = Confirmation_date
        Schedule_Data.pickupTime = date24
        Schedule_Data.GetScheduleIstr = InstructionTextField.text
        Schedule_Data.getLatitude = self.Latitude
        Schedule_Data.getLongtitude = self.Longitude
        
 
        let param=["user_id":"\(themes.getUserID())","taskid":"\(cusProviderRec.Task_id )","taskerid":"\(cusProviderRec.Tasker_id)","pickup_date":"\(Confirmation_date)","pickup_time":"\(date24)","instruction":"\(InstructionTextField.text!)","booking_id":"\(Schedule_Data.JobID)"]
        
        
        
        URL_Handler.makeCall(constant.LisviewBooking, param: param, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            self.BookNow_Btn.enabled=true
            if(error != nil) {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }  else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    if(responseObject != nil){
                        if(Status == "1") {
                            
                                let response:NSDictionary=dict.objectForKey("response") as! NSDictionary
                                let jobID:NSString=response.objectForKey("job_id") as! NSString
                                Schedule_Data.JobID="\(jobID)"
                                Schedule_Data.orderDate = response.objectForKey("booking_date") as! NSString
                                Schedule_Data.service = response.objectForKey("service_type") as! NSString
                                Schedule_Data.jobDescription = self.InstructionTextField.text!
                                Schedule_Data.service_tax = self.themes.CheckNullValue(response.objectForKey("service_tax"))!
                                //let providerid : NSString =  self.Selected_taskerid
                                let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmPageVCID") as! OrderConfirmationViewController
                                self.navigationController?.pushViewController(secondViewController, animated: true)
                                
                            
                        }  else {
                            let response:NSString=dict.objectForKey("response") as! NSString
                            self.themes.AlertView("\(Appname) ", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                        }
                    }
                }
                else {
                    self.themes.AlertView(self.themes.setLang("Sorry for the inconvenience"), Message: self.themes.setLang("Please try again"), ButtonTitle: self.themes.setLang("ok"))
                }
            }
        })
    }
    
    
    
}
