//
//  CustomProviderlistViewController.swift
//  Plumbal
//
//  Created by Casperon on 22/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CustomProviderlistViewController: RootViewController,ListBookingViewDelegate, CustomFilterViewControllerDelegate {
    var urlHandler = URLhandler()
    @IBOutlet var customProvidertable: UITableView!
    @IBOutlet var SubCat: UILabel!
    @IBOutlet var mapviewbtn: UIButton!
    
    @IBOutlet var map_btn: UIButton!
    @IBOutlet var title_lbl: CustomLabelWhite!
    @IBOutlet var filter: UIButton!
    var Selected_taskername : String = String()
    var Service_tax : String = String()
    var Selected_taskerid : String = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        map_btn.setTitle(themes.setLang("map_view"), forState: .Normal)
         title_lbl.text = themes.setLang("\(Appname)")
        SubCat.layer.borderWidth = 1.0
        SubCat.layer.borderColor = UIColor.lightGrayColor().CGColor
        SubCat.layer.cornerRadius = 5.0
        SubCat.clipsToBounds = true
        mapviewbtn.layer.cornerRadius = 5.0
        mapviewbtn.clipsToBounds = true
        SubCat.text = cusProviderRec.SubCatname as String
       // filter.layer.cornerRadius = filter.frame.size.height/2
        //filter.layer.masksToBounds = true;

        filter.layer.shadowColor = UIColor.blackColor().CGColor
        filter.layer.shadowOpacity = 0.5
        filter.layer.shadowRadius = 2
        filter.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        
        let cellnib = UINib.init(nibName: "CustomProlistTableViewCell", bundle: nil)
        self.customProvidertable.registerNib(cellnib, forCellReuseIdentifier: "customproCell")
        self.customProvidertable.rowHeight = UITableViewAutomaticDimension
        self.customProvidertable.estimatedRowHeight = 150
        
        self.customProvidertable.tableFooterView=UIView()
       
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func DisplayFilter(sender: AnyObject) {
        self.displayCustomViewController(.BottomBottom)
        
    }
    
    func displayCustomViewController(animationType: SLpopupViewAnimationType) {
        
        let popupFilter : CustomFilterViewController = CustomFilterViewController(nibName:"CustomFilterViewController",bundle: nil)
        popupFilter.delegate = self;
        popupFilter.minimumpriceval = "0"
        popupFilter.maximumpriceval = cusProviderRec.GetMax_radius
        popupFilter.view.frame = CGRectMake(self.view.frame.origin.x-10, self.view.frame.origin.y-10, self.view.frame.size.width-20, self.view.frame.size.height-20)
        
        self.presentpopupViewController(popupFilter, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func pressCancel(sender: CustomFilterViewController) {
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    func passRequiredParametres(Param : NSMutableDictionary) {
        
        self.showProgress()
        
        var paramDic = Param

        urlHandler.makeCall(constant.Apply_filter, param: paramDic) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }  else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString?=themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    if(Status! == "1")  {
                        var taskId = String()
                        
                        taskId=themes.CheckNullValue(dict.objectForKey("task_id"))!
                        cusProviderRec.Task_id = taskId
                        let providerList = dict.objectForKey("response") as! NSArray
                        self.EmptycustomProArray()
                        
                        for element in providerList
                        {
                            let taskername:String = themes.CheckNullValue(element.objectForKey("name"))!
                            cusProviderRec.ProvidersNameArray.addObject(taskername)
                            let taskerid:String = themes.CheckNullValue(element.objectForKey("taskerid"))!
                            cusProviderRec.ProvideridArray.addObject(taskerid)
                            let distance:String = themes.CheckNullValue(element.objectForKey("radius"))!
                            cusProviderRec.ProRadiusArray.addObject(distance)
                            let taskamount:String = themes.CheckNullValue(element.objectForKey("hourly_amount"))!
                            let flatamount : String = themes.CheckNullValue(element.objectForKey("flat_amount"))!
                            if taskamount == ""
                            {
                                cusProviderRec.ProAmountArray.addObject(flatamount)
                                cusProviderRec.Cat_type = "Flat"
                            }else{
                                cusProviderRec.ProAmountArray.addObject(taskamount)
                                cusProviderRec.Cat_type = "Hourly"
                            }
                            
                            let provrating:String = themes.CheckNullValue(element.objectForKey("rating"))!
                            cusProviderRec.ProRatingArray.addObject(provrating)
                            let taskerimg : String = themes.CheckNullValue(element.objectForKey("image_url"))!
                            cusProviderRec.ProviderimageArray.addObject(taskerimg)
                            let servicetax : String = themes.CheckNullValue(element.objectForKey("servixe_tax"))!
                            cusProviderRec.ServiceTaxArray.addObject(servicetax)
                            
                            let cat_type : String = themes.CheckNullValue(element.objectForKey("category_type"))!
                            cusProviderRec.ProvCatTypeArray.addObject(cat_type)
                            
                            let BadgeArray : NSArray = element.objectForKey("badges") as! NSArray
                            cusProviderRec.ProBadgesArray.addObject(BadgeArray)
                            
                        }
                        self.customProvidertable.reloadData()
                    }
                    else {
                        
                        if (responseObject?.objectForKey("response") != nil) {
                            
                            let Errormsg : String =  themes.CheckNullValue(responseObject?.objectForKey("response"))!
                            
                            themes.AlertView("\(Appname)", Message:Errormsg, ButtonTitle: kOk)
                            
                        }
                    }
                    
                }else {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }
        
    }
    
    func EmptycustomProArray()  {
        cusProviderRec.ProvidersNameArray = NSMutableArray()
        cusProviderRec.ProviderimageArray = NSMutableArray()
        cusProviderRec.ProvideridArray    = NSMutableArray()
        cusProviderRec.ProRadiusArray     = NSMutableArray()
        cusProviderRec.ProAmountArray     = NSMutableArray()
        cusProviderRec.ProRatingArray     = NSMutableArray()
        cusProviderRec.ProBadgesArray     = NSMutableArray()
        cusProviderRec.ServiceTaxArray    = NSMutableArray()
        cusProviderRec.ProvCatTypeArray   = NSMutableArray()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didclickBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func DidclickMap(sender:AnyObject)
    {
        Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "HomePageVCID")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat
    {
        
        return 150
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return cusProviderRec.ProvideridArray.count
            
          }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("customproCell") as! CustomProlistTableViewCell
        Cell.selectionStyle = .None
        Cell.prov_name.text = cusProviderRec.ProvidersNameArray.objectAtIndex(indexPath.row) as? String
        Cell.providerimg.sd_setImageWithURL(NSURL(string: "\(cusProviderRec.ProviderimageArray[indexPath.row])"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        if cusProviderRec.Cat_type == "Flat"
        {
            Cell.prov_detail.text = "Radius: \(cusProviderRec.ProRadiusArray.objectAtIndex(indexPath.row))MI - Flat Rate: \(themes.getCurrencyCode())\(cusProviderRec.ProAmountArray.objectAtIndex(indexPath.row))"

        }
        else{
            Cell.prov_detail.text = "Radius: \(cusProviderRec.ProRadiusArray.objectAtIndex(indexPath.row))MI - Hourly Rate: \(themes.getCurrencyCode())\(cusProviderRec.ProAmountArray.objectAtIndex(indexPath.row))"
        }
        
        Cell.prov_detail.adjustsFontSizeToFitWidth = true;

        Cell.providerimg.layer.cornerRadius = Cell.providerimg.frame.width/2;
        Cell.providerimg.layer.masksToBounds = true;
        
        
        let doubleval:Double = Double("\(cusProviderRec.ProRatingArray.objectAtIndex(indexPath.row))")!
        let doubleStr = String(format: "%.1f", doubleval)
        Cell.badgeCollection.restorationIdentifier="\(indexPath.row)"
        Cell.ratingcount.text =  doubleStr
        Cell.chatbtn.layer.cornerRadius = 12.0
        Cell.chatbtn.layer.masksToBounds = true
        Cell.booknow.layer.cornerRadius = 12.0
        Cell.booknow.layer.masksToBounds = true
        Cell.booklater.layer.cornerRadius = 12.0
        Cell.booklater.layer.masksToBounds = true
        Cell.RatView.layer.borderWidth = 1.0
        Cell.RatView.layer.borderColor = UIColor.lightGrayColor().CGColor
        Cell.RatView.layer.cornerRadius = 12.0
        Cell.RatView.layer.masksToBounds = true
        
        Cell.chatbtn.tag=indexPath.row
        Cell.chatbtn.addTarget(self, action: #selector(PushtoMessageVC(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.providerimg.tag = indexPath.row
        Cell.providerimg.userInteractionEnabled = true
        let tap :UITapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(PushtoDetailVC(_:)))
        Cell.providerimg.addGestureRecognizer(tap)
        Cell.booknow.tag = indexPath.row
        Cell.booknow.addTarget(self, action: #selector(BooknowAct(_:)), forControlEvents:.TouchUpInside)
        Cell.booklater.tag = indexPath.row
        Cell.booklater.addTarget(self, action: #selector(BookLateAct(_:)), forControlEvents: .TouchUpInside)
        let valueFromArray: NSArray = cusProviderRec.ProBadgesArray[indexPath.row] as! NSArray

        if valueFromArray.count == 0
        {
            Cell.badgecol_heightconst.constant = 0
            Cell.badgecoll_bottomConst.constant = 50

            
        }
        else{
            Cell.badgecol_heightconst.constant = 70
            Cell.badgecoll_bottomConst.constant = 11
        }
        
        return Cell
    }
    
    func BookLateAct(sender:UIButton)  {
        
        
        let fullAddress = self.getFullAddressForLatLng(cusProviderRec.searchlat as String, longitude: cusProviderRec.searchlng as String)
        
        
        let tempAddArray = fullAddress.componentsSeparatedByString("$")
        for item in tempAddArray{
        }
        let date = NSDate()
        let formatter = NSDateFormatter()
        
        formatter.dateFormat = "MM/dd/yyyy"
        
        let result = formatter.stringFromDate(date)
        
        formatter.dateFormat = "HH:mm"
        let time  = formatter.stringFromDate(date)
        
        let  Confirmtime : String = time
        formatter.dateFormat = "h:mm a"
        let timenew  = formatter.stringFromDate(date)
        
        let confimDate: String = result

        
        
        let  param = ["user_id":"\(themes.getUserID())",
                      "street":"\(tempAddArray[0])",
                      "locality":"\(tempAddArray[1])",
                      "city":"\(tempAddArray[2])",
                      "state":"\(tempAddArray[3])",
                      "country":"\(tempAddArray[4])",
                      "zipcode":"\(tempAddArray[5])",
                      "lat":"\(cusProviderRec.searchlat)",
                      "lng":"\(cusProviderRec.searchlng)",
                      "taskerid":"\(cusProviderRec.ProvideridArray.objectAtIndex(sender.tag))",
                      "taskid":"\(cusProviderRec.Task_id)",
                      "instruction" :"",
                      "pickup_date":"\(confimDate)",
                      "pickup_time":"\(Confirmtime)"]
        
        urlHandler.makeCall(constant.ListView_booklater, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            } else{
                
                
                if(responseObject != nil){
                    let Dict:NSDictionary=responseObject!
                    let Status:NSString=themes.CheckNullValue(Dict.objectForKey("status"))!
                    if(Status == "1"){
                        let response:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let jobID:NSString=themes.CheckNullValue(response.objectForKey("job_id"))  as! NSString
                        Schedule_Data.JobID="\(jobID)"
                        Schedule_Data.orderDate = response.objectForKey("booking_date") as! NSString
                        Schedule_Data.service = response.objectForKey("service_type") as! NSString
                        Schedule_Data.service_tax = themes.CheckNullValue(response.objectForKey("service_tax"))!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ScheduleViewControllerID") as! ScheduleViewController
                        secondViewController.Latitude = cusProviderRec.searchlat
                        secondViewController.Longitude = cusProviderRec.searchlng
                        
                        secondViewController.CustomList = "1"
                        cusProviderRec.Tasker_id = "\(cusProviderRec.ProvideridArray.objectAtIndex(sender.tag))"
                        self.navigationController?.pushViewController(secondViewController, animated: true)
//                        let providerid : NSString =  self.Selected_taskerid
//                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmPageVCID") as! OrderConfirmationViewController
//                        self.navigationController?.pushViewController(secondViewController, animated: true)
                        
                        
                        
                    }
                    else {
                        
                        let Response = themes.CheckNullValue(Dict.objectForKey("response"))!
                        themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: themes.setLang("ok"))
                    }
                }
                else {
                    themes.AlertView("\(Appname)", Message: themes.setLang("No Reasons available"), ButtonTitle: themes.setLang("ok"))
                }
            }
        }
    }
    
    func BooknowAct(sender: UIButton)
    {
        Selected_taskername = cusProviderRec.ProvidersNameArray.objectAtIndex(sender.tag) as! String
        Service_tax = cusProviderRec.ServiceTaxArray.objectAtIndex(sender.tag) as! String
        Selected_taskerid = cusProviderRec.ProvideridArray.objectAtIndex(sender.tag) as! String
        self.displayViewController(.BottomBottom)
        
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        let book:ListBookingView = ListBookingView(nibName:"ListBookingView", bundle: nil)
        book.delegate = self
        book.taskernamestr = Selected_taskername
        book.servicetax = Service_tax
        self.presentpopupViewController(book, animationType: animationType, completion: { () -> Void in
        })
    }

    func pressedCancel(sender: ListBookingView) {
        
        
        self.dismissPopupViewController(.BottomBottom)
    }
    func pressBooking(confimDate: NSString, Confirmtime: NSString, Instructionstr: NSString) {
        
        self.dismissPopupViewController(.BottomBottom)
        self.showProgress()
        if Instructionstr == themes.setLang("enter_instruc") || Instructionstr == ""{
            
            self.view.makeToast(message: "Kindly Enter Your Instructions", duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            
        }else{
            
            
            let fullAddress = self.getFullAddressForLatLng(cusProviderRec.searchlat as String, longitude: cusProviderRec.searchlng as String)
            
            
            let tempAddArray = fullAddress.componentsSeparatedByString("$")
            for item in tempAddArray{
            }
            
            
            
            let  param = ["user_id":"\(themes.getUserID())",
                          "street":"\(tempAddArray[0])",
                          "locality":"\(tempAddArray[1])",
                           "city":"\(tempAddArray[2])",
                          "state":"\(tempAddArray[3])",
                          "country":"\(tempAddArray[4])",
                          "zipcode":"\(tempAddArray[5])",
                          "lat":"\(cusProviderRec.searchlat)",
                          "lng":"\(cusProviderRec.searchlng)",
                          "taskerid":"\(Selected_taskerid)",
                          "taskid":"\(cusProviderRec.Task_id)",
                          "instruction" :"\(Instructionstr)",
                          "pickup_date":"\(confimDate)",
                          "pickup_time":"\(Confirmtime)"]
            
            urlHandler.makeCall(constant.MapOrder_confirm, param: param) { (responseObject, error) -> () in
                self.DismissProgress()
                if(error != nil){
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                } else{
                    
                    
                    if(responseObject != nil){
                        let Dict:NSDictionary=responseObject!
                        let Status:NSString=themes.CheckNullValue(Dict.objectForKey("status"))!
                        if(Status == "1"){
                            let response:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                            let jobID:NSString=response.objectForKey("job_id") as! NSString
                            Schedule_Data.JobID="\(jobID)"
                            Schedule_Data.orderDate = response.objectForKey("booking_date") as! NSString
                            Schedule_Data.service = response.objectForKey("service_type") as! NSString
                            Schedule_Data.jobDescription = response.objectForKey("description") as! NSString
                            Schedule_Data.service_tax = themes.CheckNullValue(response.objectForKey("service_tax"))!
                            
                            let providerid : NSString =  self.Selected_taskerid
                            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmPageVCID") as! OrderConfirmationViewController
                            self.navigationController?.pushViewController(secondViewController, animated: true)
                            
                        }
                        else {
                            let Response = themes.CheckNullValue(Dict.objectForKey("response"))!
                            themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: themes.setLang("ok"))
                        }
                    }
                    else {
                        themes.AlertView("\(Appname)", Message: themes.setLang("No Reasons available"), ButtonTitle: themes.setLang("ok"))
                    }
                }
            }
  }
        
    }
    

    
    func getFullAddressForLatLng(latitude: String, longitude: String)->String {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)")
        var fullAddress = ""
        let data = NSData(contentsOfURL: url!)
        do {
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    var result1 = NSArray()
                    if kLanguage == "ta"{
                        if let address = result[1]["address_components"] as? NSArray{
                            result1 = address
                        }
                    }else{
                        if let address = result[0]["address_components"] as? NSArray{
                            result1 = address
                        }
                        
                    }
                    if result1.count != 0 {
                        print("get current location \(result[1]["address_components"])")
                        var street = ""
                        var sublocality = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in result1{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }
                            else if (item1.objectAtIndex(0) as! String == "political" || item1.objectAtIndex(0) as! String == "sublocality" || item1.objectAtIndex(0) as! String == "sublocality_level_1")
                            {
                                let city1 = item["long_name"]
                                sublocality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                        }
                        fullAddress = "\(street)$"+"\(sublocality)$"+"\(city)$"+"\(state)$"+"\(country)$"+"\(zipcode)"

                        }
                    }
                }
            }
         catch {
            print(error)
            return fullAddress
            
        }
        return fullAddress
    }

    func PushtoMessageVC(sender:UIButton)
    {
        Message_details.taskid = cusProviderRec.Task_id
        Message_details.providerid = cusProviderRec.ProvideridArray .objectAtIndex(sender.tag) as! NSString
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    func PushtoDetailVC(recognizer:UITapGestureRecognizer)  {
        
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MYProfileVCSID") as! MyProfileViewController
        secondViewController.providerid = cusProviderRec.ProvideridArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.hour_amount = cusProviderRec.ProAmountArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.min_amount = cusProviderRec.ProAmountArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.cat_type = cusProviderRec.ProvCatTypeArray.objectAtIndex((recognizer.view?.tag)!) as! NSString
        secondViewController.getjob_status = "hide"
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
func tableView(tableView: UITableView,willDisplayCell cell: UITableViewCell,
                                            forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let tableViewCell = cell as? CustomProlistTableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }
          /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
  


}


extension CustomProviderlistViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if cusProviderRec.ProBadgesArray.count > 0
        {
            
            let getrow:Int = Int(collectionView.restorationIdentifier!)!
            let valueFromArray: NSArray = cusProviderRec.ProBadgesArray[getrow] as! NSArray
            print(valueFromArray)
            if valueFromArray.count > 0
            {
                 return valueFromArray.count
            }
            else{
                return 0
            }

        }
        else{
            return 0
        }
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {

        let badgecell = collectionView.dequeueReusableCellWithReuseIdentifier("Custombadge", forIndexPath: indexPath) as! custombadgeCollectionViewCell

        if cusProviderRec.ProBadgesArray.count > 0
        {
            let getrow:Int = Int(collectionView.restorationIdentifier!)!

            let valueFromArray: NSArray = cusProviderRec.ProBadgesArray[getrow] as! NSArray

            if valueFromArray.count > 0
            {
        let imageFromValue = valueFromArray[indexPath.item].objectForKey("image")
        let nameFromValue = valueFromArray[indexPath.item].objectForKey("name")
        //badgecell.badgetitle.setTitle(nameFromValue as? String, forState: .Normal)
        badgecell.badgetitle.tag = getrow
                badgecell.badgetitle.addTarget(self, action: #selector(ShowAlert(_:)), forControlEvents: .TouchUpInside)
        badgecell.badgeimg.sd_setImageWithURL(NSURL.init(string: imageFromValue as! String), completed: themes.block)
        badgecell.badgetitle.sizeToFit()
      badgecell.badgetitle.accessibilityIdentifier = "\(indexPath.item)"
            }
        }


    
        

        return badgecell

    }

    
    func ShowAlert (sender:UIButton)  {
        
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title="Badge Description"
      
        let genint = Int(sender.accessibilityIdentifier!)
        let valueFromArray: NSArray = cusProviderRec.ProBadgesArray[sender.tag] as! NSArray
      
        AlertView.message = valueFromArray[genint!].objectForKey("name") as! String
        
        AlertView.addButtonWithTitle(themes.setLang("ok"))
        AlertView.show()
        
    }

}

