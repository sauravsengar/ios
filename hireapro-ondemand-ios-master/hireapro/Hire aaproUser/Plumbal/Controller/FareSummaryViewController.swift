//
//  FareSummaryViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/30/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class FareSummaryViewController: RootViewController,UITableViewDataSource,UITableViewDelegate {
    var jobIDStr:NSString!
    var url_handler:URLhandler=URLhandler()
   // var theme:Theme=Theme()
    var fareSummaryArr:NSMutableArray = [];
    @IBOutlet weak var headerFare: UILabel!

    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet weak var fareTblView: UITableView!
    @IBOutlet weak var jobIdLbl: UILabel!
    @IBOutlet weak var fareScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerFare.text = themes.setLang("fare_summary")

        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)
        fareTblView.registerNib(UINib(nibName: "FareDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "fareDetailCellIdentifier")
        fareTblView.estimatedRowHeight = 56
        fareTblView.rowHeight = UITableViewAutomaticDimension
        fareTblView.tableFooterView = UIView()
         GetFareDetails()
        jobIdLbl.text="\(themes.setLang("job_id")) : \(jobIDStr)"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
       print("OK")
    }
    func GetFareDetails(){
        
        
        let Param: Dictionary = ["user_id":"\(themes.getUserID())",
            "job_id":"\(jobIDStr)"]
        // print(Param)
        self.showProgress()
        url_handler.makeCall(constant.Get_Summary_Details, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    var needPaymentStr:NSString="0"
                    if(status == "1")
                    {
                       
                        
                        if(responseObject?.objectForKey("response")?.objectForKey("billing")!.count>0){
                            
                            let currencyStr=themes.getCurrencyCode()
                            // (self.themes.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("job")?.objectForKey("currency") as! String))
                            
                            
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("billing") as! NSArray
                            
                            for (_, element) in listArr.enumerate() {
                                let getResponseDic : NSDictionary = element.objectForKey("response")! as! NSDictionary
                                NSLog("get list arrya =%@", getResponseDic)
                                let result1:JobDetailRecord=JobDetailRecord()
                                let tit = themes.CheckNullValue(getResponseDic.objectForKey("title"))!
                                result1.jobTitle="\(tit)"
                                result1.jobStatus=themes.CheckNullValue(getResponseDic.objectForKey("dt"))!
                                if result1.jobStatus == "0"
                                {
                                    
                                    result1.jobDesc=themes.CheckNullValue(getResponseDic.objectForKey("amount"))!
                                }
                                    
                                else
                                {
                                    result1.jobDesc="\(currencyStr)\(themes.CheckNullValue(getResponseDic.objectForKey("amount"))!)"
                                    
                                }
                                self.fareSummaryArr .addObject(result1)
                            }
                            
                            
                        }
                        else{
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                        self.fareTblView.reloadData()
                        //This code will run in the main thread:
                        
                        var frame: CGRect = self.fareTblView.frame
                        frame.origin.y=self.jobIdLbl.frame.origin.y+self.jobIdLbl.frame.size.height+20
                        frame.size.height = self.fareTblView.contentSize.height+20;
                        self.fareTblView.frame = frame;
                        if(needPaymentStr=="1"){
                            self.fareScrollView.contentSize=CGSizeMake(self.fareScrollView.frame.size.width, self.fareScrollView.frame.origin.y+self.fareTblView.frame.size.height+70)
                        }else{
                           self.fareScrollView.contentSize=CGSizeMake(self.fareScrollView.frame.size.width, self.fareScrollView.frame.origin.y+self.fareTblView.frame.size.height)
                        }
                        
                        self.fareScrollView.scrollEnabled = true
                    }
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
                else
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
            
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fareSummaryArr.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        
        let cell:FareDetailTableViewCell = tableView.dequeueReusableCellWithIdentifier("fareDetailCellIdentifier") as! FareDetailTableViewCell
      cell.loadFareTableCell(fareSummaryArr.objectAtIndex(indexPath.row) as! JobDetailRecord)
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        
        
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func viewDidDisappear(animated: Bool) {
        
        self.view.hideToast(toast: self.view)
    }
    /*
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
