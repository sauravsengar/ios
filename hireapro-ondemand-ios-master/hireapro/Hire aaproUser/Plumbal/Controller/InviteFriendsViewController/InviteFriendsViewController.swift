//
//  InviteFriendsViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 08/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

//import SwiftyJSON

import Social

import MessageUI
import FBSDKShareKit




class InviteFriendsViewController: RootViewController,FBSDKSharingDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet var invite_scroll: UIScrollView!

    @IBOutlet var twitterBtn: UIButton!
    @IBOutlet var Invite_Lbl: UILabel!
    
    @IBOutlet var refer_lbl: UILabel!

    @IBOutlet var Refer_hide: UIView!

    @IBOutlet var Let_World: UILabel!
    @IBOutlet var Let_World1: UILabel!
    
    @IBOutlet var Share_Ref: UILabel!
    @IBOutlet var SlideinMenu_But: UIButton!
    
    @IBOutlet var Amount: UILabel!
    @IBOutlet var Your_Amt: UILabel!
    
    
    @IBOutlet var Referral_Label: UILabel!
    
    
    
    var URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()
    var inviteDisc1 = String()
    var inviteDisc2 = String()
    var inviteDisc3 = String()
    var inviteDisc4 = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPage()
        //            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
        refer()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setPage(){
        inviteDisc1 = themes.setLang("invite_disc1")
        inviteDisc2 = themes.setLang("invite_disc2")
        inviteDisc3 = themes.setLang("invite_disc3")
        inviteDisc4 = themes.setLang("invite_disc4")
        Invite_Lbl.text=themes.setLang("invite_friends")
        Share_Ref.text=themes.setLang("share_referal")
        Let_World.text=themes.setLang("world_know")
        Let_World1.text=themes.setLang("world_know")
        self.invite_scroll.hidden = false

        getInviteData()

        
        invite_scroll.contentSize = CGSizeMake(invite_scroll.frame.size.width,self.twitterBtn.frame.origin.y+self.twitterBtn.frame.size.height+20)
        
        
        

    }
    
    
    func getInviteData(){
        
        let param:NSDictionary=["user_id":"\(themes.getUserID())","username":"\(themes.getUserName())"]
        
        self.showProgress()
        
        URL_handler.makeCall(constant.Invite_Friends.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), param: param, completionHandler: { (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                self.invite_scroll.hidden = true
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
                
            else
            {
                
                
                if(responseObject != nil)
                {
                    
                    self.DismissProgress()
                    let json = JSON(responseObject!)
                    
                    Invite.Status = json["status"].string!

                    let dict:NSDictionary=responseObject!.objectForKey("response") as! NSDictionary
                    if(Invite.Status == "1")
                    {
                        self.invite_scroll.hidden = false

                        Invite.Currency = json["response"]["details"]["currency"].string!
                        
                        Invite.Referral = json["response"]["details"]["referral_code"].string!
                        
                        Invite.urlstring = json["response"]["details"]["link"].string!
                        Invite.ImageUrl = json["response"]["details"]["image_url"].string!
//                        Invite.ref_status = self.themes.CheckNullValue(json["response"]["details"]["referral_status"]!)!
                        Invite.ref_status = self.themes.CheckNullValue(dict.objectForKey("details")!.objectForKey("referral_status"))!

                        print("chinna",Invite.ref_status)
                        
                        
                        if Invite.ref_status == "0" {
                            
                            self.refer_lbl.hidden = false
                            self.refer_lbl.text = "Invite Friends option Blocked by Admin"
                            self.Refer_hide.hidden = false
                        }
                        else {
                            //            getInviteData()
                            self.Refer_hide.hidden = true
                            
                            
                        }
                        
                        Invite.Friends_earn = json["response"]["details"]["friends_earn_amount"].intValue

                        
                        Invite.you_Earn = json["response"]["details"]["your_earn_amount"].intValue
                        
                        
                        
                        
                        
                        Invite.Currency_Sym=self.themes.Currency_Symbol(Invite.Currency as String)
                        
                        
                        self.Amount.text="\(self.themes.setLang("frnds_join")) \(Invite.Currency_Sym)\(Invite.Friends_earn)"
                        
                        self.Your_Amt.text="\(self.themes.setLang("frnds_share")) \(Invite.Currency_Sym)\(Invite.you_Earn)"
                        
                        self.Referral_Label.text="\(Invite.Referral)"
                        
                        
                        
                    }
                    
                    
                }
                else
                {
                    self.invite_scroll.hidden = true

                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    
                    // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                    
                }
            }
        })
        

    }
    
    
    func refer()
    {
    }
    
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        
        
        
        
    }
    
    
    @IBAction func DidClickoption(sender: UIButton) {
        
        
        if(sender.tag == 0)
        {
            
            let urlString = "Refer and Earn, when your friend signup with your referral code, you earn \(Invite.Currency_Sym)\(Invite.you_Earn), in your wallet and your friend earns \(Invite.Currency_Sym)\(Invite.Friends_earn).\n  Signup using the code \(Invite.Referral) and earn money in your wallet.Click on the below link \(Invite.urlstring)"
            let urlStringEncoded = urlString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
            let whatsappURL  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)")
            if (UIApplication.sharedApplication().canOpenURL(whatsappURL!)) {
                UIApplication.sharedApplication().openURL(whatsappURL!)
            } else {
                themes.AlertView(themes.setLang("app_not_found"), Message: themes.setLang("install_app"), ButtonTitle: kOk)
            }
        }
        
        
        
        
        if(sender.tag == 1)
        {
            if (themes.canSendText()) {
                // Obtain a configured MFMessageComposeViewController
                let messageComposeVC = themes.configuredMessageComposeViewController( "Refer and Earn, when your friend signup with your referral code, you earn \(Invite.Currency_Sym)\(Invite.you_Earn), in your wallet and your friend earns \(Invite.Currency_Sym)\(Invite.Friends_earn).\n Signup using the code \(Invite.Referral) and earn money in your wallet.Click on the below link \(Invite.urlstring)",number:"")
                // Present the configured MFMessageComposeViewController instance
                // Note that the dismissal of the VC will be handled by the messageComposer instance,
                // since it implements the appropriate delegate call-back
                presentViewController(messageComposeVC, animated: true, completion: nil)
            } else {
                // Let the user know if his/her device isn't able to send text messages
                
                themes.AlertView(themes.setLang("cannot_send_text"), Message: themes.setLang("not_able_to_send_text"), ButtonTitle: kOk)
                
            }
        }
        
        
        if(sender.tag == 2)
        {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
            
        }
        
        
        if(sender.tag == 3)
        {
            
            let shareContent = FBSDKShareLinkContent()
            shareContent.contentTitle = "Refer and Earn, when your friend signup with your referral code, you earn \(Invite.Currency_Sym)\(Invite.you_Earn), in your wallet and your friend earns \(Invite.Currency_Sym)\(Invite.Friends_earn).\n Signup using the code \(Invite.Referral) and earn money in your wallet"
            shareContent.contentURL = NSURL(string:"\(Invite.urlstring)")!
            let messageDialog = FBSDKMessageDialog()
            messageDialog.delegate = self
            messageDialog.shareContent = shareContent
            if messageDialog.canShow() {
                messageDialog.show()
            }
            else {
                
            }
        }
        
        
        if(sender.tag == 4)
        {
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
                
                let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                twitterSheet.setInitialText( "\(inviteDisc1)  \(Appname) \(inviteDisc2) \(Invite.Currency_Sym)\(Invite.Friends_earn), \(inviteDisc3) \(Invite.Referral)./n  \(inviteDisc4) \n \(Invite.urlstring)")
                twitterSheet.addURL(NSURL(string: "\(Invite.urlstring))"))
                self.presentViewController(twitterSheet, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: themes.setLang("accounts"), message: themes.setLang("login_twitter"), preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: kOk, style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        if(sender.tag == 5)
        {
            
            
            //            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            //                let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            //                twitterSheet.addURL(NSURL.init(string:"\(Invite.urlstring)"))
            //                twitterSheet.setInitialText( "I have  \(Appname) Coupon Code, worth \(Invite.Friends_earn)\(Invite.Currency_Sym), When a new friend sign's up, they can avail my Coupon Code \(Invite.Referral)Click on the below link \n \(Invite.urlstring)")
            //                self.presentViewController(twitterSheet, animated: true, completion: nil)
            //            } else {
            //                let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            //                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            //                self.presentViewController(alert, animated: true, completion: nil)
            //            }
            
            let content: FBSDKShareLinkContent = FBSDKShareLinkContent()
            content.contentURL = NSURL(string: "\(Invite.urlstring))")
            content.contentTitle = themes.setLang("share_ur_frnd")
            //            content.contentDescription = "\(themes.setLang("coupen_disc1")) \(Appname) \(themes.setLang("coupen_disc2")) \(Invite.Friends_earn)\(Invite.Currency_Sym) \(themes.setLang("coupen_disc3"))"
            
            let dialog = FBSDKShareDialog()
            dialog.fromViewController = self;
            
            
            dialog.mode = .Native
            // if you don't set this before canShow call, canShow would always return YES
            if !dialog.canShow() {
                // fallback presentation when there is no FB app
                dialog.mode = .Browser
            }
            dialog.shareContent = content
            dialog.delegate = self;
            dialog.show()
            
            //            content.contentTitle = "Share Your friends about us"
            //            content.contentDescription = "Refer and Earn, when your friend signup with your referral code, you earn \(Invite.Currency_Sym)\(Invite.you_Earn), in your wallet and your friend earns \(Invite.Currency_Sym)\(Invite.Friends_earn).\n Signup using the code \(Invite.Referral) and earn money in your wallet"
            //
            //            content.imageURL =  NSURL(string:"http://www.fnordware.com/superpng/pnggradHDrgba.png")
            //            FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: nil)
            
            
        }
    }
    
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    
    
    
    
    
    
    
    
    //Delegate Function For mail composing
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        //        mailComposerVC.setToRecipients(["info@zoplay.com"])
        mailComposerVC.setSubject(themes.setLang("share_ur_frnd"))
        mailComposerVC.setMessageBody("\(inviteDisc1)  \(Appname) \(inviteDisc2) \(Invite.Currency_Sym)\(Invite.Friends_earn), \(inviteDisc3) \(Invite.Referral)./n  \(inviteDisc4) \n \(Invite.urlstring)", isHTML: false)
        
        return mailComposerVC
    }
    
    
    
    
    func showSendMailErrorAlert() {
        
        
        themes.AlertView(themes.setLang("not_send_email"), Message: themes.setLang("device_not_send_email"), ButtonTitle: kOk)
        
        
        
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject: AnyObject]) {
        print(results)
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        print("sharer NSError")
        print(error.description)
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        print("sharerDidCancel")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
