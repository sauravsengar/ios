//
//  TermsViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 30/12/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class TermsViewController: RootViewController,UIWebViewDelegate {
    let themes:Themes=Themes()
    var getterms: NSString!
    
    @IBOutlet var Webload_progress: UIProgressView!
    @IBOutlet var termwebview: UIWebView!
    @IBOutlet var back_btn: UIButton!
    @IBOutlet var title_lable: CustomLabelWhite!
    var theBool: Bool=Bool()
    var myTimer: NSTimer=NSTimer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title_lable.text = getterms as String
        title_lable.text = themes.setLang(getterms as String)
        Webload_progress.tintColor=themes.ThemeColour()
        
        var  url: NSURL = NSURL()
        // Do any additional setup after loading the view, typically from a nib.
        if getterms == "Terms and Conditions"
        {
           url = NSURL (string:constant.termcondi_url as String)!;
        }
        else{
             url = NSURL (string:constant.privacy_policy as String)!;
        }
        
        let requestObj = NSURLRequest(URL: url);
        termwebview.loadRequest(requestObj);
        self.Webload_progress.progress = 0.0
        termwebview.delegate=self
        
        let transform:CGAffineTransform = CGAffineTransformMakeScale(1.0, 2.0);
        Webload_progress.transform = transform;
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        
        return true
    }
    
 
    func webViewDidStartLoad(webView: UIWebView) {
        funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        funcToCallCalledWhenUIWebViewFinishesLoading()
        
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.theBool = false
        self.myTimer = NSTimer.scheduledTimerWithTimeInterval(0.01667, target: self, selector: #selector(TransactionPopupViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.Webload_progress.progress >= 1 {
                self.Webload_progress.hidden = true
                self.myTimer.invalidate()
            } else {
                self.Webload_progress.progress += 0.1
            }
        } else {
            self.Webload_progress.progress += 0.05
            if self.Webload_progress.progress >= 0.95 {
                self.Webload_progress.progress = 0.95
            }
        }
    }
    
    
    @IBAction func didClickoption(sender: AnyObject) {
        
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
