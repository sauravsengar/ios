//
//  FacebookRegisterViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 14/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class FacebookRegisterViewController: RootViewController,UITextFieldDelegate {
    
    @IBOutlet var firstname: UITextField!
    @IBOutlet var lastname: UITextField!
    @IBOutlet var username: UITextField!
    //   @IBOutlet var ShowPas_Lbl: UILabel!
    @IBOutlet var Signup_Btn: UIButton!
    @IBOutlet var Email_TextField: UITextField!
    @IBOutlet var CountryCode_Picker: UIPickerView!
    @IBOutlet var UserImage: UIImageView!
    @IBOutlet var Picker_Wrapper: UIView!
    
    @IBOutlet var SignupEmail_Btn: UIButton!
    @IBOutlet var FB_ScrollView: UIScrollView!
    @IBOutlet var CountryCode_TextField: UITextField!
    @IBOutlet var ContactField: UITextField!
    var URL_handler:URLhandler=URLhandler()
    
    var themes:Themes=Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
        //Status For paging.
        
        FB_ScrollView.scrollEnabled = true;
        
        FB_ScrollView.contentSize = CGSizeMake(self.FB_ScrollView.frame.size.width, SignupEmail_Btn.frame.origin.y+SignupEmail_Btn.frame.size.height+30)
        
        if(themes.getCounrtyphone() != ""){
            CountryCode_TextField.text = "+ \(themes.getCounrtyphone())"
            
        }
        Email_TextField.isMandatory()
        firstname.isMandatory()
        lastname.isMandatory()
        username.isMandatory()
        ContactField.isMandatory()
        
        //Text Field Delegate
        
        Email_TextField.autocapitalizationType = .None;
        
        //        CountryCode_Picker.showsSelectionIndicator = true
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.frame.size.width, 25))
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        //        let doneButton = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(FacebookRegisterViewController.donePicker))
        //        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        //        doneButton.tintColor=themes.ThemeColour()
        //        toolBar.setItems([spaceButton, doneButton], animated: false)
        //        toolBar.userInteractionEnabled = true
        //        Picker_Wrapper.addSubview(toolBar)
        
        
        
        firstname.attributedPlaceholder=NSAttributedString(string:themes.setLang("firstname"), attributes: [NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        lastname.attributedPlaceholder=NSAttributedString(string:themes.setLang("lastname"), attributes: [NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        username.attributedPlaceholder=NSAttributedString(string:themes.setLang("user_name"), attributes: [NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        Email_TextField.attributedPlaceholder=NSAttributedString(string:themes.setLang("email_id_smal"), attributes: [NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        ContactField.attributedPlaceholder=NSAttributedString(string: themes.setLang("phone_no"), attributes:[NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        //        CountryCode_TextField.attributedPlaceholder=NSAttributedString(string: "", attributes:[NSForegroundColorAttributeName:UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0) ])
        Signup_Btn.setTitle(themes.setLang("register"), forState: UIControlState.Normal)
        //        ShowPas_Lbl.text=themes.setLang("show_password")
        SignupEmail_Btn.setTitle(themes.setLang("sign_with_email"), forState: UIControlState.Normal)
        
        
        
        
        if(FB_Regis.FB_mailid != "")
        {
            Email_TextField.text=FB_Regis.FB_mailid as String
            
        }
        
        if(FB_Regis.FB_Firstname != "")
        {
            firstname.text = FB_Regis.FB_Firstname as String
        }
        
        if(FB_Regis.FB_lastname != "")
        {
            lastname.text = FB_Regis.FB_lastname as String
        }
        if(FB_Regis.FB_Username != "")
        {
            username.text = FB_Regis.FB_Username as String
        }
        
        
        if(FB_Regis.FB_Picture != "")
        {
            
            self.UserImage.sd_setImageWithURL(NSURL(string:"\(FB_Regis.FB_Picture)"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            
        }
        
        UserImage.layer.cornerRadius=themes.RoundView(UserImage.frame.size.width)
        
        UserImage.clipsToBounds=true
        UserImage.layer.borderWidth=3.0
        
        UserImage.layer.borderColor=themes.ThemeColour().CGColor
        
        //        Picker_Wrapper.removeFromSuperview()
        
        print(self.SignupEmail_Btn.frame);
        print(self.FB_ScrollView.contentSize);
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(FacebookRegisterViewController.DismissKeyboard(_:)))
        
        view.addGestureRecognizer(tapgesture)
        
    }
    
    
    
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        OTP_sta.OTP_Paging="FacebookSignup"
        if(themes.getCounrtyphone() != ""){
            CountryCode_TextField.text = "+ \(themes.getCounrtyphone())"
            
        }
        if(signup.selectedCode != ""){
            let indexCode = themes.codename.indexOfObject(signup.selectedCode)
            CountryCode_TextField.text = themes.code[indexCode] as? String
        }
        
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        
        if(textField == firstname)
        {
            
            firstname.resignFirstResponder()
            lastname.becomeFirstResponder()
            
            
        }
        else if (textField == lastname){
            
            lastname.resignFirstResponder()
            username.becomeFirstResponder()
            
            
        }
        else if (textField == username){
            
            username.resignFirstResponder()
            Email_TextField.becomeFirstResponder()
            
            
        }
        else if (textField == Email_TextField){
            
            Email_TextField.resignFirstResponder()
            CountryCode_TextField.becomeFirstResponder()
            
            
        }
        else if (textField == CountryCode_TextField){
            
            CountryCode_TextField.resignFirstResponder()
            ContactField.becomeFirstResponder()
            
            
        }
        
        
        
        
        
        
        
        return true
    }
    
    
    
    func DismissKeyboard(sender:UITapGestureRecognizer)
    {
        
        //        self.donePicker()
        view.endEditing(true)
        
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == Email_TextField
            
        {
            if let _ = string.rangeOfCharacterFromSet(NSCharacterSet.uppercaseLetterCharacterSet())
            {
                // Do not allow upper case letters
                return false
            }
            return true
        }
            
            
            
        else
        {
            return true
            
        }
        
    }
    
    
    
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        
        
        
        if(textField == username)
        {
            
            
            //                self.donePicker()
            
            
        }
        
        if(textField == Email_TextField)
        {
            
            //                self.donePicker()
            
            
        }
        
        if(textField == CountryCode_TextField) {
            view.endEditing(true)
            let navig = self.storyboard?.instantiateViewControllerWithIdentifier("SearchBarViewControllerID") as! SearchBarViewController
            self.navigationController?.pushViewController(navig, animated: true)
            return false
        }
        
        
        
        
        
        
        
        return true
    }
    
    //    func donePicker()
    //    {
    //
    //        UIView.animateWithDuration(0.2, animations: {
    //
    //            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 260.0)
    //
    //            }, completion: { _ in
    //
    //                self.Picker_Wrapper.removeFromSuperview()
    //
    //        })
    //
    //
    //    }
    //
    
    
    
    func showPicker()
    {
        //        view.addSubview(self.Picker_Wrapper)
        //
        //        UIView.animateWithDuration(0.2, animations: {
        //
        //            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height - 260.0, UIScreen.mainScreen().bounds.size.width, 260.0)
        //
        //            } , completion: { _ in
        //
        //
        //
        //        })
        
    }
    
    
    @IBAction func didClickoptions(sender: UIButton) {
        
        
        
        if(sender.tag == 1)
        {
            
            
            self.FB_Signup()
            
        }
        
        
        if(sender.tag == 2)
        {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        if(sender.tag == 10)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    func FB_Signup()
    {
        
        print(Email_TextField.text!)
        let validateemail:Bool=themes.isValidEmail(Email_TextField.text!)
        
        let Contact:NSString=ContactField.text!
        
        
        if (username.text == ""){
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_username"),ButtonTitle: kOk)
        }
        
        if(Email_TextField.text == "")
        {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_email_alert"),ButtonTitle: kOk)
        }
            
        else if(validateemail == false)
        {
            themes.AlertView("\(Appname)",Message: themes.setLang("valid_email_alert"),ButtonTitle: kOk)
            
        }
            
        else if(ContactField.text == "")
        {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_ur_num"),ButtonTitle: kOk)
        }
        else if(Contact.length >= 15 || Contact.length < 7)
        {
            themes.AlertView("\(Appname)",Message:themes.setLang( "enter_the_validnum"),ButtonTitle: kOk
            )
        }
            
            
        else if(CountryCode_TextField.text == "")
        {
            themes.AlertView("\(Appname)",Message: themes.setLang("Kindly enter the Country Code"),ButtonTitle: kOk)
        }
            
            
        else
        {
            self.Signup_Btn.enabled=false
            
            
            self.showProgress()
            
            
            let parameter=["user_name":"\(username.text!)","email_id":"\(Email_TextField.text!)","country_code":"\(CountryCode_TextField.text!)","phone":"\(ContactField.text!)","deviceToken":"\(Device_Token)","gcm_id":""]
            
            
            URL_handler.makeCall(constant.Social_Check, param: parameter, completionHandler: { (responseObject, error) -> () in
                self.Signup_Btn.enabled=true
                
                self.DismissProgress()
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    
                    //  self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: kOK)
                }
                    
                else
                {
                    
                    
                    if(responseObject != nil)
                    {
                        
                        let dict:NSDictionary=responseObject!
                        
                        signup.status=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        
                        
                        if (signup.status == "1")
                        {
                            
                            signup.firstname = self.firstname.text!
                            signup.lastname = self.lastname.text!
                            signup.username=dict.objectForKey("user_name") as! NSString
                            signup.Email=dict.objectForKey("email") as! NSString
                            
                            signup.Contact_num=dict.objectForKey("phone_number") as! NSString
                            signup.OTP=dict.objectForKey("otp") as! NSString
                            signup.otpstatus=dict.objectForKey("otp_status") as! NSString
                            signup.Country_Code=dict.objectForKey("country_code") as! NSString
                            
                            
                            
                            self.DismissProgress()
                            if(signup.OTP.length>0){
                                
                                OTP_sta.OTP=signup.OTP
                                OTP_sta.OTP_Status=signup.otpstatus
                                
                                let otpview : OTPViewController = OTPViewController()
                                otpview.otpstring = "\(self.themes.CheckNullValue(dict.objectForKey("otp"))!)"
                                otpview.otpstatus_str = self.themes.CheckNullValue(dict.objectForKey("otp_status"))!
                                self.performSegueWithIdentifier("OTP", sender: nil)
                            }
                            
                            
                        }
                        else
                        {
                            self.DismissProgress()
                            
                            signup.message=dict.objectForKey("message") as! NSString
                            
                            self.themes.AlertView("\(Appname)",Message: "\(signup.message)",ButtonTitle: kOk)
                            
                            
                        }
                        
                    }
                        
                        
                        
                    else
                    {
                        self.DismissProgress()
                        
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        
                        
                        
                    }
                    
                    return
                }
                
            })
            
            
        }
        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return themes.codename.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (themes.codename[row] as! String)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        self.CountryCode_TextField.text="\(themes.code[row])"
        
    }
    
    
}

extension FacebookRegister:UIPickerViewDelegate
{
    
}
 