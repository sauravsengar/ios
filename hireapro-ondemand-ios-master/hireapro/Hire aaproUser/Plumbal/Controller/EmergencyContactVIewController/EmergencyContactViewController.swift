//
//  EmergencyContactViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 06/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class EmergencyContactViewController: RootViewController,CLLocationManagerDelegate {
    @IBOutlet var Code_Field: UITextField!

    @IBOutlet weak var emergencyScroll: UIScrollView!
    @IBOutlet var Reset_Btn: UIButton!
    @IBOutlet var Email_Field: UITextField!
    @IBOutlet var Mobile_Field: UITextField!
    @IBOutlet var Name_Field: UITextField!
    @IBOutlet var Wrapper_View: UIView!
    @IBOutlet var SlideinMenu_But: UIButton!
    @IBOutlet var emergency_button: UIButton!

    @IBOutlet var EmergencyCnt_Lbl: UILabel!
    
    @IBOutlet var Note_Your: UILabel!
    @IBOutlet var List_Your: UILabel!
    @IBOutlet var save_Btn: UIButton!
    @IBOutlet var Picker_Wrapper: UIView!
    @IBOutlet var Country_PickerView: UIPickerView!
    var Contact:NSString=NSString()
    var themes:Themes=Themes()
    
    var URL_handler:URLhandler=URLhandler()
    let locationManager = CLLocationManager()
    var latitude = String()
    var longitude = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        emergencyScroll.hidden = false
        EmergencyCnt_Lbl.text=themes.setLang("emergency_contact1")
        save_Btn.setTitle(themes.setLang("save"), forState: UIControlState.Normal)
        Reset_Btn.setTitle(themes.setLang("reset_contact"), forState: UIControlState.Normal)
        List_Your.text=themes.setLang("emergency_disc1")
        Note_Your.text=themes.setLang("emergency_disc2")
        Code_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("code"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        Name_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("enter_name"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        Mobile_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("emter_mbl"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        emergency_button.setTitle(themes.setLang("send_mail"), forState: UIControlState.Normal)
        Email_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("enter_email"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
Reset_Btn.hidden = true
        emergency_button.hidden = true

        Wrapper_View.layer.borderWidth=1.0
        Wrapper_View.layer.borderColor=themes.Lightgray().CGColor
        Wrapper_View.layer.cornerRadius=5.0
        Reset_Btn.layer.cornerRadius=5.0
        emergency_button.layer.cornerRadius = 5.0
        
        
        Name_Field.delegate=self
        Mobile_Field.delegate=self
        Email_Field.delegate=self
        Code_Field.delegate=self
       
        if(themes.getCounrtyphone() != ""){
            Code_Field.text = "+ \(themes.getCounrtyphone())"
            
        }
        let TapGesture:UITapGestureRecognizer=UITapGestureRecognizer(target: self, action: #selector(EmergencyContactViewController.DismissKeyboard))
        self.view.addGestureRecognizer(TapGesture)
        // Do any additional setup after loading the view.
        
        //Tool Bar for Picker View
        
        
        Country_PickerView.showsSelectionIndicator = true
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.frame.size.width, 25))
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(EmergencyContactViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        doneButton.tintColor=themes.ThemeColour()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        Picker_Wrapper.addSubview(toolBar)
        
        Picker_Wrapper.removeFromSuperview()
        
        
        //ADD Done button for Contact Field
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor=UIColor.whiteColor()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Done, target: self, action: #selector(EmergencyContactViewController.doneButtonAction))
        
        
        doneToolbar.items = [flexSpace,done]
        
        doneToolbar.sizeToFit()
        
        Mobile_Field.inputAccessoryView = doneToolbar
       self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
       

    }
    
    // MARK: - Location Delegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D? = manager.location!.coordinate
        if(locValue != nil){
            print("locations = \(locValue!.latitude) \(locValue!.longitude)")
            latitude="\(locValue!.latitude)"
            longitude="\(locValue!.longitude)"
            self.locationManager.stopUpdatingLocation()
        }
    }
    

    
     func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        EmergencyCnt_Lbl.text=themes.setLang("emergency_contact1")
        save_Btn.setTitle(themes.setLang("save"), forState: UIControlState.Normal)
        Reset_Btn.setTitle(themes.setLang("reset_contact"), forState: UIControlState.Normal)
        List_Your.text=themes.setLang("emergency_disc1")
        Note_Your.text=themes.setLang("emergency_disc2")
        Code_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("code"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        Name_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("enter_name"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        Mobile_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("emter_mbl"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        emergency_button.setTitle(themes.setLang("send_mail"), forState: UIControlState.Normal)
        Email_Field.attributedPlaceholder=NSAttributedString(string: themes.setLang("enter_email"), attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        
    }

    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }

    func doneButtonAction()
    {
        Mobile_Field.resignFirstResponder()
        
        donePicker()
        
        
     }
     
    
    override func viewWillAppear(animated: Bool) {
        
        
        self.ViewContact()
        

    }
    
    
    
    func ViewContact()
    {
        
        self.showProgress()
        
        let Param:NSDictionary=["user_id":"\(themes.getUserID())"]
        
        URL_handler.makeCall(constant.view_emergency, param: Param) { (responseObject, error) -> () in
    
    self.DismissProgress()

    if(error != nil)
    {
        self.emergencyScroll.hidden = true
        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

        //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
    }
        
    else
    {
        
        
        if(responseObject != nil)
        {
            
            print("\(responseObject)")
            self.emergencyScroll.hidden = false

            let dict:NSDictionary=responseObject!
            
            let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
            
            if(Status == "1")
            {
                self.Reset_Btn.hidden=false
                self.emergency_button.hidden = false

                 let emergency_contactArray:NSDictionary=responseObject?.objectForKey("emergency_contact") as! NSDictionary
                
                
                    let name:NSString=emergency_contactArray.objectForKey("name") as! NSString
                    
                    self.Name_Field.text="\(name)"
                    
                     let email:NSString=emergency_contactArray.objectForKey("email") as! NSString
                    
                    self.Email_Field.text="\(email)"

                     
                    let mobile:NSString=self.themes.CheckNullValue(emergency_contactArray.objectForKey("mobile"))!
                    
                    self.Mobile_Field.text="\(mobile)"

                    let country_code:NSString=self.themes.CheckNullValue(emergency_contactArray.objectForKey("code"))!

                    self.Code_Field.text="\(country_code)"
                
            }
                
            else
            {
                self.Reset_Btn.hidden=true
             }
            
            
        }
        else
        {
            self.emergencyScroll.hidden = true

            self.themes.AlertView(self.themes.setLang("Sorry for the inconvenience"), Message: self.themes.setLang("Please try again"), ButtonTitle: self.themes.setLang("ok"))
            
        }
        
    }
    
        }


    }


    func showPicker()
    {
        view.addSubview(self.Picker_Wrapper)
        
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height - 260.0, UIScreen.mainScreen().bounds.size.width, 260.0)
            
            } , completion: { _ in
                
                
                
        })
        
    }
    
    func donePicker()
    {
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 260.0)
            
            }, completion: { _ in
                
                self.Picker_Wrapper.removeFromSuperview()
                
        })
        
        
        
        
    }
    
    
    
    //PickerVView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return themes.codename.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (themes.codename[row] as! String)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        self.Code_Field.text="\(themes.code[row])"
        
    }
    
    
     func DismissKeyboard()
    {
        self.donePicker()

        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        
        if(textField == Name_Field)
        {
            Name_Field.resignFirstResponder()
            self.showPicker()

 
            
        }
        
        if(textField == Code_Field)
        {
            view.endEditing(true)
            
            return false
            
        }
        if(textField == Mobile_Field)
        {
            Mobile_Field.resignFirstResponder()

            Email_Field.becomeFirstResponder()
            
        }
        if(textField == Email_Field)
        {
            Email_Field.resignFirstResponder()
            
            
        }



        return true
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        
            
            if(textField == Name_Field)
            {
                
                self.donePicker()
                
                
            }
            
            if(textField == Code_Field)
            {
                self.showPicker()
                
                view.endEditing(true)
                return false
 
            }
            if(textField == Mobile_Field)
            {
                
                
                self.donePicker()

                
                
            }
        if(textField == Email_Field)
        {
            
            
            self.donePicker()
            
            
            
        }

        return true
        
            
        }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == Name_Field)
        {
            
            let aSet = NSCharacterSet(charactersInString: ACCEPTABLE_CHARACTERS).invertedSet
            let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            
            
            
            
            
            return string == numberFiltered
        }
            
            
            
        else
        {
            return true
            
        }
        
    }

    
    
    @IBAction func didClickOption(sender: UIButton) {
        
        view.endEditing(true)
        
        
        if(sender.tag == 0)
        {
            
            Contact = Mobile_Field.text!
            
            let validateemail:Bool=themes.isValidEmail(Email_Field.text!)

            if(Mobile_Field.text == "" || Name_Field.text == "" || Email_Field.text == "" || Code_Field.text == "" )
            
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("enter_all_fields"), ButtonTitle: kOk)
            }
            else if(Mobile_Field.text == "")
            {
                themes.AlertView("\(Appname)",Message: themes.setLang("enter_the_num"),ButtonTitle: kOk)
            }
            else if(Contact.length >= 15 || Contact.length < 7)
            {
                themes.AlertView("\(Appname)",Message:themes.setLang("enter_the_validnum"),ButtonTitle: kOk)
            }

            else if(validateemail == false)
            {
                themes.AlertView("\(Appname)", Message:themes.setLang("valid_email_alert"), ButtonTitle: kOk)
                
            }
                
                

            else
            {

            
            self.showProgress()
            
            let Param:NSDictionary=["user_id":"\(themes.getUserID())","em_name":"\(Name_Field.text!)","em_email":"\(Email_Field.text!)","em_mobile_code":"\(Code_Field.text!)","em_mobile":"\(Mobile_Field.text!)"]
                
                print("\(Param)......\(constant.add_emergency)")
            
            URL_handler.makeCall(constant.add_emergency, param: Param) { (responseObject, error) -> () in
                
                self.DismissProgress()
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

                   // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                }
                    
                else
                {
                    
                    
                    if(responseObject != nil)
                    {
                        
                        let dict:NSDictionary=responseObject!
                        
                        let Status:NSString = self.themes.CheckNullValue(dict.objectForKey("status"))!
                        let response:NSString=dict.objectForKey("response") as! NSString

                        
                        if(Status == "1")
                        {
                            self.emergency_button.hidden = false

                            self.themes.AlertView("\(Appname)", Message:"\(self.themes.CheckNullValue(dict.objectForKey("response"))!)", ButtonTitle: self.themes.setLang("ok"))
                            self.Reset_Btn.hidden=false

                        }
                            
                            
                        else
                        {
                            
                            self.themes.AlertView("\(Appname) ", Message: "\(response)", ButtonTitle: "Ok")

                        }
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        
                    }
                    
                }
                
            }
            
            }
            
        }
        if(sender.tag == 1)
        {

            
            if(Mobile_Field.text == "" || Name_Field.text == "" || Email_Field.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("enter_all_fields"), ButtonTitle: kOk)
            }
            else
            {

            
            self.showProgress()
            
            let Param:NSDictionary=["user_id":"\(themes.getUserID())"]
            
            URL_handler.makeCall(constant.Delete_emergency, param: Param) { (responseObject, error) -> () in
                
                self.DismissProgress()
                
                if(error != nil)
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

                   // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                }
                    
                else
                {
                    
                    
                    if(responseObject != nil)
                    {
                        
                        let dict:NSDictionary=responseObject!
                        
                        let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        
                        if(Status == "1")
                        {
                            
                            
                            self.themes.AlertView("\(Appname)", Message:"\(self.themes.CheckNullValue(dict.objectForKey("response"))!)", ButtonTitle: "Ok")
                            self.Reset_Btn.hidden=true
                            self.emergency_button.hidden = true
                            self.Mobile_Field.text = ""
                            self.Name_Field.text = ""
                            if(self.themes.getCounrtyphone() != ""){
                                self.Code_Field.text = "+ \(self.themes.getCounrtyphone())"
                                
                            }
                            else{
                                self.Code_Field.text = ""
                            }

                            self.Email_Field.text = ""
                        }
                        else
                        {
                         }
                        
                        
                    }
                    else
                    {
                        self.themes.AlertView(self.themes.setLang("Sorry for the inconvenience"), Message: self.themes.setLang("Please try again"), ButtonTitle: "Ok")
                        
                    }
                    
                }
                
            }
            }

            
        }

    }
    
    
    @IBAction func didClickEmergency(sender: AnyObject) {
        self.showProgress()
        let Param:NSDictionary=["user_id":"\(themes.getUserID())","latitude":latitude,"longitude":longitude]
        URL_handler.makeCall(constant.contact_emergency, param: Param) { (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil) {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }else {
                if(responseObject != nil)  {
                    let dict:NSDictionary=responseObject!
                    let Status:NSNumber=dict.objectForKey("status") as! NSNumber
                    let response:NSString=dict.objectForKey("response") as! NSString
                    
                    if(Status == 1)  {
                        self.themes.AlertView(Appname, Message: response, ButtonTitle: self.themes.setLang("ok"))
                        
                    } else{
                        self.themes.AlertView("\(Appname) ", Message: "\(response)", ButtonTitle: self.themes.setLang("ok"))
                    }
                } else  {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension  EmergencyContactViewController: UITextFieldDelegate {
}

