//
//  OrderDetailViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 05/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import MapKit
protocol OrderDetailViewControllerDelegate {
    //    func pressOK(sender: MyPopupViewController)
    func ReloadData(sender: OrderDetailViewController)
}

class OrderDetailViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,MyPopupViewControllerDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UIAlertViewDelegate {
    var Window:UIWindow=UIWindow()
    var notification:MPGNotification=MPGNotification()
    var buttonArray:NSArray=NSArray()
    var tField: UITextField!
    var AlertView:JTAlertView=JTAlertView()
    var Is_alertshown:Bool=Bool()
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRectMake(0, 0, 0, 0),
                                                        type: .BallSpinFadeLoader)
    
    
    @IBOutlet var service_lbl: CustomLabel!
    @IBOutlet weak var lblProffes: UILabel!
    @IBOutlet weak var lblRequest: UILabel!
    
    @IBOutlet weak var lblServiceDel: UILabel!
    @IBOutlet weak var openinMapview: UIView!

    
    @IBOutlet var openInMapsBtn: UIButton!
    @IBOutlet var paymentBtn: UIButton!
    @IBOutlet var viewSummeryBtn: UIButton!
    var delegate:OrderDetailViewControllerDelegate?
    @IBOutlet var Location_lbl: UILabel!
    @IBOutlet var Date_Lbl: UILabel!
    @IBOutlet var Cancel_Btn: UIButton!
    @IBOutlet var No_data: UILabel!
    @IBOutlet var Date_Time_Lab: UILabel!
    @IBOutlet var service_type_lbl: UILabel!
    @IBOutlet weak var flatrateLbl: UILabel!
    @IBOutlet var Detail_Tableview: UITableView!
    @IBOutlet var Provider_Bio: UILabel!
    @IBOutlet var Provider_Image: UIImageView!
    @IBOutlet var Provider_Name: UILabel!
    @IBOutlet var Step1_detail: UIImageView!
    @IBOutlet var Step3_detail: UIImageView!
    @IBOutlet var Step2_detail: UIImageView!
    @IBOutlet var Address_Lab: UILabel!
    @IBOutlet var ratingView: TPFloatRatingView!
    var margin: CGFloat = 0.0
    @IBOutlet var MapView: GMSMapView!
    @IBOutlet var Call_btn: UIButton!
    @IBOutlet var Message_btn: UIButton!
    @IBOutlet var user_Image: UIImageView!
    @IBOutlet var Detail_WrapperView: UIView!
    @IBOutlet var Detail_View: UIView!
    @IBOutlet var Dot_View1: UIView!
    @IBOutlet var Dot_View3: UIView!
    @IBOutlet var Dot_View5: SetColorView!

    @IBOutlet var service_tax: CustomLabelGray!
    @IBOutlet weak var Dot_View4: UIView!
    @IBOutlet var Dot_View2: UIView!
    @IBOutlet var OrderDetail_ScrollView: UIScrollView!
    @IBOutlet var Back_bt: UIButton!
    var items = [String]()
    
    
    @IBOutlet var Header_Lab: UILabel!
    let locationManager = CLLocationManager()
    var CurLaat:Double!
    var CurLong:Double!
    var themes:Themes=Themes()
    var Timeline_Date:NSMutableArray=NSMutableArray()
    var Timeline_Time:NSMutableArray=NSMutableArray()
    var flatrate:NSMutableArray=NSMutableArray()
    var hourrate:NSMutableArray=NSMutableArray()

    var Timeline_title:NSMutableArray=NSMutableArray()
    var Detail_ProcessArray:NSMutableArray=NSMutableArray()
    var ReasonDetailArray:NSMutableArray=NSMutableArray()
    var  ReasonidArray:NSMutableArray=NSMutableArray()
    var  ChoosedReasonid:NSString=NSString()
    @IBOutlet var Response_View: UIView!
    var BlackWarpper_View:UIView=UIView()
    var URL_Handler:URLhandler=URLhandler()
    override func viewDidLoad() {
        service_lbl.text = themes.setLang("ser_typ")
        self.openinMapview.hidden = true
        super.viewDidLoad()
        items = [themes.setLang("detail")
            ,themes.setLang("response")
        ]

     lblProffes.text = themes.setLang("request_submitted")
     lblServiceDel.text = themes.setLang("service_delivery")
     lblRequest.text = themes.setLang("professional_assigned")
        //   self.locationManager.requestAlwaysAuthorization()
        openInMapsBtn.setTitle(themes.setLang("open_maps")
            , forState: UIControlState.Normal)
        Date_Lbl.text = themes.setLang("date_time")
        Location_lbl.text = themes.setLang("location")
        Call_btn.setTitle(themes.setLang("call"), forState: UIControlState.Normal)
        Message_btn.setTitle(themes.setLang("chat_space"), forState: UIControlState.Normal)

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        OrderDetail_ScrollView.delegate=self
        Schedule_Data.Schedule_header=self.themes.setLang("reason")
        
        
        Provider_Image.layer.cornerRadius = Provider_Image.frame.size.height / 2;
        Provider_Image.clipsToBounds=true
        Provider_Image.layer.masksToBounds = true;
        Provider_Image.layer.borderWidth = 0;
        Provider_Image.contentMode = UIViewContentMode.ScaleAspectFill
        //Provider_Image.layer.borderWidth=2.0
        Provider_Image.layer.borderColor=UIColor.whiteColor().CGColor
        
        
        Detail_WrapperView.layer.borderColor=UIColor.whiteColor().CGColor
        Detail_WrapperView.layer.borderWidth=1.0
        Detail_WrapperView.layer.cornerRadius=5.0
        
        //Back_bt.setTitle("\(Workname)", forState: UIControlState.Normal)
        
        BlackWarpper_View.backgroundColor=UIColor.blackColor()
        
        BlackWarpper_View.frame=CGRectMake(0, 0, MapView.frame.size.width, MapView.frame.size.height+60)
        BlackWarpper_View.alpha=0.5
        MapView.addSubview(BlackWarpper_View)
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        Back_bt.addSubview(themes.Back_ImageView)
        Dot_View3.layer.cornerRadius=Dot_View3.frame.width/2
        Dot_View5.layer.cornerRadius=Dot_View5.frame.width/2
        Dot_View4.layer.cornerRadius=Dot_View4.frame.width/2

        Dot_View1.layer.cornerRadius=Dot_View1.frame.width/2
        Dot_View2.layer.cornerRadius=Dot_View2.frame.width/2
        let nibName = UINib(nibName: "DetailTableViewCell", bundle:nil)
        self.Detail_Tableview.registerNib(nibName, forCellReuseIdentifier: "DetailCell")
        Detail_Tableview.estimatedRowHeight = 990
        Detail_Tableview.rowHeight = UITableViewAutomaticDimension
        
        
        let TapGesture:UITapGestureRecognizer=UITapGestureRecognizer(target: self, action: #selector(OrderDetailViewController.ProviderInfo))
        TapGesture.numberOfTapsRequired=1
        TapGesture.delegate=self
        self.Detail_WrapperView.addGestureRecognizer(TapGesture)
        
        let Selection_Segment: CustomSegmentControl=CustomSegmentControl(items: items)
        
        
        Selection_Segment.frame=CGRect(x: self.margin, y:MapView.frame.size.height+MapView.frame.origin.y, width: self.view.frame.size.width - self.margin*2, height: 40)
        Selection_Segment.selectedSegmentIndex=0
        Selection_Segment.tintColor=themes.ThemeColour()
        Selection_Segment.setTitleTextAttributes([NSFontAttributeName: PlumberMediumFont!, NSForegroundColorAttributeName: PlumberThemeColor], forState: .Normal)
        Selection_Segment.addTarget(self, action: #selector(OrderDetailViewController.SegmentAction(_:)), forControlEvents: .ValueChanged)
        self.OrderDetail_ScrollView.addSubview(Selection_Segment);
        
        // Do any additional setup after loading the view.
        
        
        OrderDetail_ScrollView.hidden=true
        Cancel_Btn.hidden=true
        
        Selection_Segment.layer.borderColor=themes.ThemeColour().CGColor;
        Selection_Segment.layer.cornerRadius = 0.0;
        Selection_Segment.layer.borderWidth = 1.5;
        
        No_data.text=themes.setLang("no_response")
        
        
    }
    
    
    
    @IBAction func didClickOpenInMapsBtn(sender: AnyObject) {
        
        moveToLocVc(false)
    }
    
    func moveToLocVc(isShowArriveBtn:Bool){//LocationVCSID
        let ObjLocVc=self.storyboard!.instantiateViewControllerWithIdentifier("LocationVCSID")as! LocationViewController
        ObjLocVc.isShowArriveBtn=isShowArriveBtn
        ObjLocVc.mapLaat=OrderDetail_data.lat
        ObjLocVc.mapLong=OrderDetail_data.lon
        trackingDetail.userLat = (OrderDetail_data.provider_lat as NSString).doubleValue
        trackingDetail.userLong = (OrderDetail_data.provider_long as NSString).doubleValue
        ObjLocVc.addressStr = Address_Lab.text!
        ObjLocVc.phoneStr = OrderDetail_data.provider_mobile
        ObjLocVc.getUsername =  OrderDetail_data.provider_name
        ObjLocVc.jobId=OrderDetail_data.task_id as String
        ObjLocVc.providerId = OrderDetail_data.provider_id as String
        self.navigationController?.pushViewController(ObjLocVc, animated: true)
    }
    
    @IBAction func paymentAct(sender: AnyObject) {
        
        if sender.tag == 1{
        Root_Base.Job_ID=Order_data.job_id
        Root_Base.task_id = OrderDetail_data.task_id
        
        
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        }else if sender.tag == 2{
            let objFarevc = self.storyboard!.instantiateViewControllerWithIdentifier("FareSummaryVCSID") as! FareSummaryViewController
            objFarevc.jobIDStr=Order_data.job_id
            self.navigationController!.pushViewController(objFarevc, animated: true)

        }
        
        
        
    }
    
    func paymentAction(){
        
    }
    @IBAction func summeryAct(sender: AnyObject) {
        if sender.tag == 1{
        let objFarevc = self.storyboard!.instantiateViewControllerWithIdentifier("FareSummaryVCSID") as! FareSummaryViewController
        objFarevc.jobIDStr=Order_data.job_id
        self.navigationController!.pushViewController(objFarevc, animated: true)
        }else if sender.tag == 2{
            Root_Base.Job_ID=Order_data.job_id

            let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
            self.navigationController?.pushViewController(Controller, animated: true)

        }
        
    }
    
    func SegmentAction(sender: CustomSegmentControl) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;
        
        if(segmentIndex == 0)
        {
            UIView.transitionFromView(Response_View, toView: self.Detail_View, duration: 1, options: UIViewAnimationOptions.TransitionFlipFromLeft.union(UIViewAnimationOptions.ShowHideTransitionViews), completion: nil)
            SetFrameAccordingToSegmentIndex(0)
            
        }
        if(segmentIndex == 1)
        {
            UIView.transitionFromView(Detail_View, toView: self.Response_View, duration: 1, options: UIViewAnimationOptions.TransitionFlipFromRight.union(UIViewAnimationOptions.ShowHideTransitionViews), completion: nil)
            SetFrameAccordingToSegmentIndex(1)
            
            
        }
        
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)


        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self,name: Language_Notification as String, object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    
    func Show_rating(notification: NSNotification)
    {
        
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(RatingsViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(themes.setLang("ok"), action: {
                    
                    let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
    }
    
    func showPopup(notification: NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(PaymentViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(themes.setLang("ok"), action: {
                    
                    let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
    }
    
    
    func methodOfReceivedMessagePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            
            Message_details.taskid = taskid!
            Message_details.providerid = check_userid
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        
        
    }
    
    
    func methodOfReceivedMessageNotification(notification: NSNotification){
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                    
                }else{
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("msg_from_provider"))
                    alertView.addButton(themes.setLang("ok"), action: {
                        Message_details.taskid = taskid!
                        Message_details.providerid = check_userid
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                        
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                        
                        
                    })
                    alertView.show()
                    
                }
                
            }
        }
        
        
        
        
        
    }
    
    
    func Show_Alert(notification:NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        let action:NSString! = userInfo["Action"]

        let alertView = UNAlertView(title: Appname, message:messageString as String)
        alertView.addButton(themes.setLang("ok"), action: {
            if action != "admin_notification"{
            Order_data.job_id=Order_id
            self.GetDetail()
            }
            
            
            
        })
        alertView.show()
        
        
    }
    
    
    
    
    func GetDetail()
    {
        self.showProgress()
        let Param=["user_id":"\(themes.getUserID())","job_id":"\(Order_data.job_id)"]
        
        URL_Handler.makeCall(constant.GetOrderdetail, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.OrderDetail_ScrollView.hidden=false
            self.Cancel_Btn.hidden=false
            
            
            
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //  self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    if(Status != nil)
                    {
                        
                        
                        if(Status == "1")
                        {
                            
                            let Response:NSDictionary?=Dict.objectForKey("response")!.objectForKey("info") as? NSDictionary
                            
                            
                            if(Response != nil)
                            {

                                let CancelbtnStatus:NSString=Response!.objectForKey("do_cancel") as! NSString
                                let need_payment:NSString=Response!.objectForKey("need_payment") as! NSString
                                OrderDetail_data.task_id = Response!.objectForKey("task_id") as! NSString;
                                let submittratingStatus = Response!.objectForKey("submit_ratings") as! NSString
                                OrderDetail_data.userJobLocation = self.themes.CheckNullValue(Response!.objectForKey("booking_address"))!.stringByReplacingOccurrencesOfString(", ,", withString: ",")
                                
                                OrderDetail_data.provider_lat = self.themes.CheckNullValue(Response!.valueForKeyPath("provider_location.provider_lat"))!
                                OrderDetail_data.provider_long = self.themes.CheckNullValue(Response!.valueForKeyPath("provider_location.provider_lng"))!
                                
                                
                                
                                OrderDetail_data.job_id=Response!.objectForKey("job_id") as! NSString
                                OrderDetail_data.job_status=Response!.objectForKey("job_status") as! NSString
                                OrderDetail_data.jobntn_grpStatus=self.themes.CheckNullValue(Response!.objectForKey("botton_group"))!
                                OrderDetail_data.lat=self.themes.CheckNullValue(Response!.objectForKey("lat"))!
                                
                                
                                OrderDetail_data.lon=self.themes.CheckNullValue(Response!.objectForKey("lng"))!
                                OrderDetail_data.time=self.themes.CheckNullValue(Response!.objectForKey("time"))!
                                OrderDetail_data.date=self.themes.CheckNullValue(Response!.objectForKey("date"))!
                                OrderDetail_data.flat=self.themes.CheckNullValue(Response!.objectForKey("type"))!
                                OrderDetail_data.serv_tax=self.themes.CheckNullValue(Response!.objectForKey("service_tax"))!
                                
                                if(CancelbtnStatus == "Yes")
                                {
                                    
                                    
                                    if (OrderDetail_data.job_status .isEqualToString("StartOff"))
                                    {
                                        self.openinMapview.hidden = false
                                        
                                    }
                                    else
                                    {
                                        self.openinMapview.hidden = true
                                    }
                                    
                                    self.Cancel_Btn.enabled=true
                                    self.Call_btn.hidden = false
                                    self.Cancel_Btn.tag=3
                                    self.Cancel_Btn.setTitle(self.themes.setLang("cancel_caps"), forState: UIControlState.Normal)
                                    self.paymentBtn.enabled = false
                                    self.viewSummeryBtn.enabled = false
                                    self.paymentBtn.hidden = true
                                    self.viewSummeryBtn.hidden = true
                                }
                                    
                                else if(need_payment == "Yes")
                                {
                                    self.openinMapview.hidden = true
                                    
                                    
                                    self.Cancel_Btn.enabled=false
                                    self.Cancel_Btn.hidden = true
                                    self.Cancel_Btn.tag=4
                                    self.paymentBtn.setTitle(self.themes.setLang("payment")
                                        , forState: UIControlState.Normal)
                                    self.paymentBtn.tag = 1
                                     self.viewSummeryBtn.tag = 1
                                    self.viewSummeryBtn.setTitle(self.themes.setLang("view_summary")
                                        , forState: UIControlState.Normal)
                                    self.paymentBtn.enabled = true
                                    self.viewSummeryBtn.enabled = true
                                    self.paymentBtn.hidden = false
                                    self.viewSummeryBtn.hidden = false
                                    
                                } else if submittratingStatus == "No" && need_payment == "NO"{
                                    self.openinMapview.hidden = true
                                    
                                    self.paymentBtn.tag = 2
                                    self.viewSummeryBtn.tag = 2
                                    
                                    self.Cancel_Btn.enabled=false
                                    self.Cancel_Btn.hidden = true
                                    self.Cancel_Btn.tag=4
                                    self.paymentBtn.setTitle(self.themes.setLang("more_info")
                                        , forState: UIControlState.Normal)
                                    self.viewSummeryBtn.setTitle(self.themes.setLang("RATING"), forState: UIControlState.Normal)
                                    self.paymentBtn.enabled = true
                                    self.viewSummeryBtn.enabled = true
                                    self.paymentBtn.hidden = false
                                    self.viewSummeryBtn.hidden = false
                                }

                                else
                                {
                                    self.openinMapview.hidden = true
                                    
                                    self.paymentBtn.enabled = false
                                    self.viewSummeryBtn.enabled = false
                                    self.paymentBtn.hidden = true
                                    self.viewSummeryBtn.hidden = true
                                    
                                    self.Cancel_Btn.enabled=true
                                    self.Call_btn.hidden = false
                                    if(OrderDetail_data.job_status  .isEqualToString("Completed"))
                                    {
                                        self.Cancel_Btn.setTitle(self.themes.setLang("more_info")
                                            , forState: UIControlState.Normal)
                                        
                                        self.Cancel_Btn.tag=6
                                    }
                                    else if (OrderDetail_data.job_status  .isEqualToString("StartJob"))
                                        
                                    {
                                        self.Cancel_Btn.setTitle(self.themes.setLang("started_job"), forState: UIControlState.Normal)
                                        
                                        self.Cancel_Btn.tag=5
                                    }
                                        
                                   
                                    else
                                    {
                                        self.Cancel_Btn.tag=5
                                        
                                        self.Cancel_Btn.setTitle("\(OrderDetail_data.job_status)", forState: UIControlState.Normal)
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            let Response_Timeline:NSArray?=Dict.objectForKey("response")!.objectForKey("timeline") as? NSArray
                            if(self.Timeline_Date.count != 0)
                            {
                                self.Timeline_Date.removeAllObjects()
                                self.Timeline_Time.removeAllObjects()
                                self.Timeline_title.removeAllObjects()
                            }
                            
                            if(Response_Timeline != nil)
                            {
                                for  Dict in Response_Timeline!
                                {
                                    
                                    self.Timeline_Date.addObject(Dict.objectForKey("date") as! NSString)
                                    self.Timeline_Time.addObject(Dict.objectForKey("time") as! NSString)
                                    self.Timeline_title.addObject(Dict.objectForKey("title") as! NSString)
                                    
                                }
                                
                                print("the text is \(self.Timeline_Date.count)")
                                
                                self.Detail_Tableview.reloadData()
                                
                            }
                            //                        else
                            //                        {
                            //                             self.Timeline_Date.addObject("Your Order has been Sucessfully Submitted")
                            //                             self.Timeline_Time.addObject("")
                            //                             self.Timeline_title.addObject("")
                            //                            self.Detail_Tableview.reloadData()
                            //
                            //
                            //                        }
                            
                            
                            
                            
                            
                            
                            let Provide_Response:NSDictionary?=Response!.objectForKey("provider") as? NSDictionary
                            
                            
                            
                            
                            if(Provide_Response != nil)
                            {
                                
                                OrderDetail_data.provider_name=Provide_Response!.objectForKey("provider_name") as! NSString
                                
                                if(OrderDetail_data.provider_name != "")
                                {
                                    
                                    self.No_data.hidden=true
                                    self.Detail_WrapperView.hidden=false
                                    OrderDetail_data.Provider_minrate=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_fixedrate"))!
                                    OrderDetail_data.Provider_hourlyrate=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_hourlyrate"))!
                                    OrderDetail_data.cat_type=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_category_type"))!

                                    OrderDetail_data.provider_email=Provide_Response!.objectForKey("provider_email") as! NSString
                                    OrderDetail_data.provider_image=Provide_Response!.objectForKey("provider_image") as! NSString
                                    OrderDetail_data.provider_mobile=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_mobile"))!
                                    
                                    //                                let ratingInt : Float = Provide_Response!.objectForKey("provider_ratings") as! Float
                                    //                                let strProviderRating = "\(ratingInt)"
                                    OrderDetail_data.avg_rating=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_ratings"))!
                                    //OrderDetail_data.avg_rating=self.themes.convertIntToString(Provide_Response!.objectForKey("provider_ratings") as! Int)
                                    //OrderDetail_data.avg_rating=Provide_Response!.objectForKey("provider_ratings") as! NSString
                                    OrderDetail_data.provider_email=Provide_Response!.objectForKey("provider_email") as! NSString
                                    OrderDetail_data.provider_id=Provide_Response!.objectForKey("provider_id") as! NSString
                                    OrderDetail_data.Provider_service_type=Response!.objectForKey("service_type")  as! NSString
                                    if self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_fixedrate"))! == ""
                                    {
                                        OrderDetail_data.hour=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_hourlyrate"))!

                                    }
                                    else{
                                    OrderDetail_data.hour=self.themes.CheckNullValue(Provide_Response!.objectForKey("provider_fixedrate"))!
                                    }

                                }
                                else
                                {
                                    self.No_data.hidden=false
                                    
                                    self.Detail_WrapperView.hidden=true
                                }
                                //      let Locationarray:NSArray=responseObject?.objectForKey("response")!.objectForKey("locations") as! NSArray
                                
                            }
                            else
                            {
                                self.Detail_WrapperView.hidden=true
                            }
                            
                            self.set_Data()
                            
                            
                        }
                        else
                        {
                            let Response:NSString=Dict.objectForKey("response") as! NSString
                            
                            self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                            
                        }
                    }
                    else
                    {
                        
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("cant_cancel"), ButtonTitle: kOk)
                        
                        
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("cant_cancel"), ButtonTitle: kOk)
                }
            }
            
            self.SetFrameAccording_ToSegmentIndex(0)
            
        }
    }
    
    func ProviderInfo()
    {
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MYProfileVCSID") as! MyProfileViewController
        secondViewController.providerid = OrderDetail_data.provider_id
        secondViewController.min_amount = OrderDetail_data.Provider_minrate
        secondViewController.hour_amount = OrderDetail_data.Provider_hourlyrate
        secondViewController.cat_type = OrderDetail_data.cat_type
         if OrderDetail_data.jobntn_grpStatus == "7" || OrderDetail_data.jobntn_grpStatus == "8" || OrderDetail_data.jobntn_grpStatus == "1"
         {
               secondViewController.getjob_status = "hide"
         }
        else
         {
            secondViewController.getjob_status = "show"
        }
        
     

        self.navigationController?.pushViewController(secondViewController, animated: true)
        
//        let Storyboard:UIStoryboard=UIStoryboard(name: "Main", bundle: nil)
//        let vc = Storyboard.instantiateViewControllerWithIdentifier("ProviderVC")
//        self.presentViewController(vc, animated: true, completion: nil)
        
        //        self.performSegueWithIdentifier("ProviderVC", sender: nil)
    }
    
    
    func set_Data()
    {
        Header_Lab.text="\(themes.setLang("order_id")): \(OrderDetail_data.job_id)"
        service_type_lbl.text="\(OrderDetail_data.Provider_service_type)"
        service_tax.text = "\(OrderDetail_data.serv_tax)% \(themes.setLang("service_tax_txt")))"

        Date_Time_Lab.text="\(OrderDetail_data.date) & \(OrderDetail_data.time)"
        flatrateLbl.text="\(OrderDetail_data.flat) : \(themes.getCurrencyCode())\(OrderDetail_data.hour)"
        let longitude :CLLocationDegrees =  (OrderDetail_data.lon as NSString).doubleValue
        let latitude :CLLocationDegrees =  (OrderDetail_data.lat as NSString).doubleValue
        let location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
        
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                print(pm.locality)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
        
        
        // Address_Lab.text="\( themes.CheckNullValue(OrderDetail_data.booking_address.valueForKey("city"))!),\(themes.CheckNullValue(OrderDetail_data.booking_address.valueForKey("line1"))!),\(themes.CheckNullValue(OrderDetail_data.booking_address.valueForKey("line2"))!),\( themes.CheckNullValue(OrderDetail_data.booking_address.valueForKey("state"))!),\(themes.CheckNullValue(OrderDetail_data.booking_address.valueForKey("zipcode"))!)"
        Address_Lab.text = getAddressForLatLng(OrderDetail_data.lat as String, longitude: OrderDetail_data.lon as String)
        if(OrderDetail_data.provider_name != "")
        {
            Provider_Name.text="\(OrderDetail_data.provider_name)"
            self.Provider_Image.sd_setImageWithURL(NSURL(string:"\(OrderDetail_data.provider_image)"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            
            //Provider_Image.sd_setImageWithURL(NSURL(string: "\(OrderDetail_data.provider_image)"), completed: themes.block)
            Provider_Bio.text="\(OrderDetail_data.provider_email)"
            let doubleval:Double = Double("\(OrderDetail_data.avg_rating)")!
            let doubleStr = String(format: "%.1f", doubleval)
            let n = NSNumberFormatter().numberFromString(doubleStr)
            
            Set_StarRating(CGFloat(n!))
        }
        
        self.set_mapView()
        
        if(OrderDetail_data.job_status == "Cancelled")
        {
            Step1_detail.image=UIImage(named: "circle")
            Step2_detail.image=UIImage(named: "circle")
            Step3_detail.image=UIImage(named: "circle")
            
        }
        else if(OrderDetail_data.job_status == "Completed")
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "tick_green")
            Step3_detail.image=UIImage(named: "tick_green")
        }
            
        else if(OrderDetail_data.job_status == "Payment")
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "tick_green")
            Step3_detail.image=UIImage(named: "circle")
        }
            
            
        else if(OrderDetail_data.job_status == "Closed")
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "tick_green")
            Step3_detail.image=UIImage(named: "tick_green")
        }
        else if(OrderDetail_data.job_status == "Booked")
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "circle")
            Step3_detail.image=UIImage(named: "circle")
        }
        else if(OrderDetail_data.job_status == "Accepted")
            
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "tick_green")
            Step3_detail.image=UIImage(named: "circle")
        }
        else
        {
            Step1_detail.image=UIImage(named: "tick_green")
            Step2_detail.image=UIImage(named: "circle")
            Step3_detail.image=UIImage(named: "circle")
            
        }
        
        // circle
        // tick_green
        
    }
    func getAddressForLatLng(latitude: String, longitude: String)->String {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)")
        let data = NSData(contentsOfURL: url!)
        var fullAddress = ""
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address = result[0]["address_components"] as? NSArray {
                        print("get current location \(result[0]["address_components"])")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in address{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                            fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)"
                            if let address = result[0]["formatted_address"] as? String{
                                return address
                            }else{
                                return fullAddress
                            }
                        }
                    }
                }
            }
        }
        return ""
    }

    override func viewDidAppear(animated: Bool) {
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.showPopup(_:)), name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.Show_rating(_:)), name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("ConfigureNotification:"), name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushPayment", object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OrderDetailViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)


        self.GetDetail()
        
        
        
    }
    
    func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                            Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
                    alertView.addButton(themes.setLang("ok"), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }

    func methodofReceivePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
       

        
        let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    
    func methodofReceivePaymentNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
    }
    func methodofReceiveRatingNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(RatingsViewController){
                
            }else{
                
                let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                self.navigationController?.pushViewController(Controller, animated: true)
                
                
            }
            
        }
        
    }
    
    
    
    
    
    func Set_StarRating(Rating:CGFloat)
    {
        self.ratingView.emptySelectedImage = UIImage(named: "Star")
        self.ratingView.fullSelectedImage = UIImage(named: "StarSelected")
        self.ratingView.contentMode = UIViewContentMode.ScaleAspectFill
        self.ratingView.maxRating = 5
        self.ratingView.minRating = 1
        self.ratingView.halfRatings = false
        self.ratingView.floatRatings = false
        self.ratingView.rating = Rating
        self.ratingView.editable = false;
        self.ratingView.halfRatings = true;
        self.ratingView.floatRatings = false;
        
    }
    
    
    
    func set_mapView()
    {
        let latitude = (OrderDetail_data.lat as NSString).doubleValue
        let longitude = (OrderDetail_data.lon as NSString).doubleValue
        let camera = GMSCameraPosition.cameraWithLatitude(latitude,
                                                          longitude:longitude, zoom:10)
        
        MapView.camera=camera
        MapView.frame=MapView.frame
        MapView.delegate=self
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "MapPin")
        marker.map = MapView
        // MapView.settings.setAllGesturesEnabled(false)
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       
        if manager.location!.coordinate.latitude != 0

        {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        CurLaat=locValue.latitude
        CurLong=locValue.longitude
        locationManager.stopUpdatingLocation()
       }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .NotDetermined, .Restricted, .Denied:
                themes.AlertView("", Message: "\(themes.setLang("location_service_disabled"))\n \(themes.setLang("to_reenable_location")) ", ButtonTitle: kOk)
                break
                
            case .AuthorizedAlways, .AuthorizedWhenInUse: break
                
            }
        } else {
            themes.AlertView("", Message: "\(themes.setLang("location_service_disabled"))\n \(themes.setLang("to_reenable_location")) ", ButtonTitle: kOk)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func SetFrameAccordingToSegmentIndex(IndexPath:Int){
        
        
        
        dispatch_async(dispatch_get_main_queue()) {
            
            
            
            //This code will run in the main thread:
            var frame: CGRect = self.Detail_Tableview.frame
            frame.size.height = self.Detail_Tableview.contentSize.height;
            //            frame.origin.y=
            self.Detail_Tableview.frame = frame;
            self.Detail_View.frame = CGRectMake(self.Detail_View.frame.origin.x, self.Detail_View.frame.origin.y, self.Detail_View.frame.size.width, self.Detail_Tableview.frame.origin.y+self.Detail_Tableview.frame.size.height);
            if(IndexPath==0){
                self.OrderDetail_ScrollView.contentSize=CGSizeMake(self.OrderDetail_ScrollView.frame.size.width, self.Detail_View.frame.origin.y+self.Detail_View.frame.size.height+5)
            }else{
                self.OrderDetail_ScrollView.contentSize=CGSizeMake(self.OrderDetail_ScrollView.frame.size.width, self.Response_View.frame.origin.y+self.Response_View.frame.size.height+5)
            }
            
            
        }
    }
    
    func SetFrameAccording_ToSegmentIndex(IndexPath:Int){
        
        
        
        dispatch_async(dispatch_get_main_queue()) {
            
            
            
            //This code will run in the main thread:
            var frame: CGRect = self.Detail_Tableview.frame
            frame.size.height = self.Detail_Tableview.contentSize.height;
            //            frame.origin.y=
            self.Detail_Tableview.frame = frame;
            self.Detail_View.frame = CGRectMake(self.Detail_View.frame.origin.x, self.Detail_View.frame.origin.y, self.Detail_View.frame.size.width, self.Detail_Tableview.frame.origin.y+self.Detail_Tableview.frame.size.height);
            if(IndexPath==0){
                self.OrderDetail_ScrollView.contentSize=CGSizeMake(self.OrderDetail_ScrollView.frame.size.width, self.Detail_View.frame.origin.y+self.Detail_View.frame.size.height)
            }
            
        }
    }
    
    
    //TableViewDelegate
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return  Timeline_title.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell:DetailTableViewCell  = tableView.dequeueReusableCellWithIdentifier("DetailCell") as! DetailTableViewCell
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
//        Cell.Detail_Lab.numberOfLines=0
        Cell.Detail_Lab.text="\(Timeline_title[indexPath.row])"
        Cell.Time_Lab.text="\(Timeline_Date[indexPath.row])  \(Timeline_Time[indexPath.row])"
        Cell.Time_Lab.sizeToFit()
        if(indexPath.row == Timeline_title.count-1)
        {
            Cell.Last_VertLine.hidden=true
        }
        return Cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView == OrderDetail_ScrollView)
        {
            //             let offset: CGFloat = scrollView.contentOffset.y
            //            let percentage: CGFloat = (offset / CGFloat(223))
            //            let value: CGFloat = CGFloat(223) * percentage
            //            MapView.frame = CGRectMake(0, value, MapView.bounds.size.width, CGFloat(223) - value)
            //            BlackWarpper_View.frame.size.height=MapView.frame.size.height+60
            
            
        }
        //            let alphaValue: CGFloat = 1 - fabs(percentage)
        //            userInfoTopView.alpha = alphaValue * alphaValue * alphaValue
        
    }
    
    
    
    func ShowReason()
    {
        
        self.showProgress()
        
        
        let param=["user_id":"\(themes.getUserID())"]
        
        URL_Handler.makeCall(constant.Get_Reasons, param: param) { (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    
                    self.ReasonDetailArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressNameArray.removeAllObjects()
                    
                    self.ReasonidArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressArray.removeAllObjects()
                    
                    
                    
                    if(Status == "1")
                    {
                        //let ReasonArray:NSArray=Dict.objectForKey("response")!.objectForKey("reason") as! NSArray
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let ReasonArray : NSArray = ResponseDic.objectForKey("reason") as! NSArray
                        
                        for ReasonDict in ReasonArray
                        {
                            let Reason_Str:NSString=self.themes.CheckNullValue(ReasonDict.objectForKey("reason"))!
                            self.ReasonDetailArray.addObject(Reason_Str)
                            Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                            let Reasonid:NSString=self.themes.CheckNullValue(ReasonDict.objectForKey("id"))!
                            self.ReasonidArray.addObject(Reasonid)
                            Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                            
                            
                        }
                        
                        
                        let Reason_Str:NSString="Others"
                        self.ReasonDetailArray.addObject(Reason_Str)
                        Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                        let Reasonid:NSString="1"
                        self.ReasonidArray.addObject(Reasonid)
                        Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                        self.displayViewController(.BottomBottom)
                    }
                    else
                    {
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_reasons_available"), ButtonTitle: kOk)
                        
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_reasons_available"), ButtonTitle: kOk)
                }
            }
            
        }
        
        
    }
    
    
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        if(View.tag == 1)
        {
            
            switch buttonIndex{
                
            case 0:
                self.cancelRequest()
                break;
            default:
                break;
                //Some code here..
                
            }
        }
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        let myPopupViewController:MyPopupViewController = MyPopupViewController(nibName:"MyPopupViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.isDetailViewcontroller = true
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    func pressCancel(sender: MyPopupViewController) {
        self.dismissPopupViewController(.BottomBottom)
    }
    func pressAdd(sender: MyPopupViewController) {
        
    }
    
    func PassSelectedAddress(Address: NSString, AddressIndexvalue: Int, latitudestr: NSString, longtitudestr: NSString,localitystr:NSString) {
        
        self.dismissPopupViewController(.BottomBottom)
        if (Address == "Others")
        {
            
            
            let alert = UIAlertController(title: self.themes.setLang("Reason"), message: "", preferredStyle: .Alert)
            
            alert.addTextFieldWithConfigurationHandler(configurationTextField)
            alert.addAction(UIAlertAction(title: self.themes.setLang("Cancel"), style: .Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: self.themes.setLang("Done"), style: .Default, handler:{ (UIAlertAction) in
                
                
                self.ChoosedReasonid = self.tField.text!
                self.cancelRequest()
                
            }))
            self.presentViewController(alert, animated: true, completion: {
                print("completion block")
            })
            
            
        }
        else
        {
            
            
            ChoosedReasonid=Address
            
            let AlertView:UIAlertView=UIAlertView()
            AlertView.delegate=self
            AlertView.tag=1
            AlertView.title=themes.setLang("cancel_confirmation")
            AlertView.addButtonWithTitle("Yes")
            AlertView.addButtonWithTitle("No")
            AlertView.show()
        }
        
    }
    
    
    
    
    func configurationTextField(textField: UITextField!)
    {
        
        textField.placeholder = "Enter Your Reason"
        
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    
    func cancelRequest()
    {
        
        self.showProgress()
        let param=["user_id":"\(themes.getUserID())","reason":"\(ChoosedReasonid)","job_id":"\(OrderDetail_data.job_id)"]
        
        
        
        URL_Handler.makeCall(constant.Cancel_Reasons, param: param) { (responseObject, error) -> () in
            
            print("the param is \(param)....\(constant.Cancel_Reasons)...\(responseObject)...\(error)")
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    if(Status == "1")
                    {
                        self.dismissViewControllerAnimated(true, completion: nil)
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        self.delegate?.ReloadData(self)
                        
                    }
                    else
                    {
                        let Response:NSString=Dict.objectForKey("response") as! NSString
                        
                        self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                        
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("cant_cancel"), ButtonTitle: kOk)
                }
            }
            
        }
        
        
        
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func didClickoption(sender: UIButton) {
        if(sender.tag == 0)
        {
            
            self.navigationController?.popViewControllerAnimated(true)
            
            self.dismissViewControllerAnimated(true, completion: nil)
            self.delegate?.ReloadData(self)
            
        }
        if(sender.tag == 1)
        {
            var getproviderarray : NSMutableArray = NSMutableArray()
            
            getproviderarray = dbfileobj.arr("Provider_Table")
            if getproviderarray.count != 0
            {
                
                let providerid : NSString = getproviderarray.objectAtIndex(0).objectForKey("providerid") as! NSString
                
                Message_details.providerid = providerid
            }
            Message_details.taskid = OrderDetail_data.task_id
            Message_details.providerid = OrderDetail_data.provider_id
            
            
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
            
            
            
        }
        if(sender.tag == 2)
        {
            
        print("get mobile no\(OrderDetail_data.provider_mobile)")
            UIApplication.sharedApplication().openURL(NSURL(string:"telprompt:\(OrderDetail_data.provider_mobile)")!)
            
            
        }
        if(sender.tag == 3)
        {
            ShowReason()
        }
        if(sender.tag == 4)
        {
            
            
            
            
        }
        if(sender.tag == 5)
        {
            
            /*Root_Base.Job_ID=Order_data.job_id
             Root_Base.task_id = OrderDetail_data.task_id
             
             
             var mainView: UIStoryboard!
             mainView = UIStoryboard(name: "Main", bundle: nil)
             let presentingController: UIViewController = mainView.instantiateViewControllerWithIdentifier("ReviewPoup")
             //        Window.rootViewController=presentingController
             let popup: CCMPopupTransitioning = CCMPopupTransitioning.sharedInstance()
             if self.view.bounds.size.height <= 568 {
             popup.destinationBounds = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width-20, UIScreen.mainScreen().bounds.size.height-50)
             }
             else {
             popup.destinationBounds = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width-20, UIScreen.mainScreen().bounds.size.height-50)
             }
             
             popup.presentedController = presentingController
             popup.presentingController = self
             
             //            self.popupController = presentingController
             self.presentViewController(presentingController, animated: true, completion: nil)*/
        }
        
        if (sender.tag == 6)
        {
//            
//            Root_Base.Job_ID=Order_data.job_id
//            
//            let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
//            self.navigationController?.pushViewController(Controller, animated: true)
            
        
            let objFarevc = self.storyboard!.instantiateViewControllerWithIdentifier("FareSummaryVCSID") as! FareSummaryViewController
            objFarevc.jobIDStr=Order_data.job_id
            self.navigationController!.pushViewController(objFarevc, animated: true)
            
            
        }
        
        if(sender.tag == 100)
        {
            
            
        }
        
        
    }
    func showProgress()
    {
        self.activityIndicatorView.color = PlumberThemeColor
        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        self.activityIndicatorView.startAnimation()
        self.view.addSubview(activityIndicatorView)
    }
    func DismissProgress()
    {
        self.activityIndicatorView.stopAnimation()
        
        self.activityIndicatorView.removeFromSuperview()
        
    }
    
    
    
    
    
}

class CustomViewFlowLayout : UICollectionViewFlowLayout {
    
    let cellSpacing:CGFloat = 0
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if let attributes = super.layoutAttributesForElementsInRect(rect) {
            for (var i = 1; i < attributes.count; ++i) {
                let currentLayoutAttributes = attributes[i]
                let prevLayoutAttributes = attributes[i - 1]
                let maxSpacing = cellSpacing
                let origin = CGRectGetMaxX(prevLayoutAttributes.frame)
                if (origin + maxSpacing + currentLayoutAttributes.frame.size.width < self.collectionViewContentSize().width) {
                    var frame = currentLayoutAttributes.frame
                    frame.origin.x = origin + maxSpacing
                    currentLayoutAttributes.frame = frame
                }
            }
            return attributes
        }
        return nil
    }
}
