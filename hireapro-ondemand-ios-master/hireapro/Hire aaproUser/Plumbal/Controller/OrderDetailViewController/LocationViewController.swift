//
//  LocationViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/12/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import CoreLocation


class LocationViewController: RootViewController,CLLocationManagerDelegate {
    var isShowArriveBtn:Bool!
    var addressStr:NSString!
    var phoneStr:NSString!
    var mapLaat:NSString!
    var isPolyLineDrawn:Bool!
    var getUsername : NSString!
    var mapLong:NSString!
    let partnerMarker = GMSMarker()
    let userMarker = GMSMarker()
    var jobId : String!
    var providerId : String!
    var model : MapRequestModel!
    @IBOutlet weak var lblStartLoc: UILabel!
    @IBOutlet var title_lbl: CustomLabelWhite!

    @IBOutlet var desc_lbl: UILabel!
    @IBOutlet var distancelabl: UILabel!
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet var startFrom_lbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    let locationManager = CLLocationManager()
    let URL_Handler:URLhandler=URLhandler()
    let marker1 = GMSMarker()
    
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title_lbl.text = themes.setLang("location")
        desc_lbl.text = themes.setLang("destination")
        startFrom_lbl.text = themes.setLang("start_frm")
        
        self.showProgress()
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        backBtn.addSubview(themes.Back_ImageView)
       // self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
              
        //        if CLLocationManager.locationServicesEnabled() {
        //            locationManager.delegate = self
        //            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //            locationManager.startUpdatingLocation()
        //        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
        setDataToLocationView()
        
    }
    
    func set_mapView(loc:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.cameraWithLatitude(OrderDetail_data.provider_lat.doubleValue,
                                                          longitude:OrderDetail_data.provider_long.doubleValue, zoom:15)
        
        mapView.camera=camera
        mapView.frame=mapView.frame
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "StartPin")
        
        partnerMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        partnerMarker.appearAnimation = kGMSMarkerAnimationPop
        partnerMarker.icon = UIImage(named: "CarIcon")
        partnerMarker.title = getUsername as String
        partnerMarker.map = mapView
        marker.map = mapView

        
        marker1.position = CLLocationCoordinate2DMake(mapLaat.doubleValue, mapLong.doubleValue)
        marker1.appearAnimation = kGMSMarkerAnimationPop
        marker1.icon = UIImage(named: "FinishPin")
        marker1.title = themes.getUserName()
        marker1.map = mapView
        mapView.settings.setAllGesturesEnabled(true)
        
        //Partner Location Moving
        
        //User Location Moving
        //        userMarker.position = camera.target
        //        userMarker.appearAnimation = kGMSMarkerAnimationPop
        //        userMarker.icon = UIImage(named: "CarIcon")
        //        userMarker.title = getUsername as String
        //        userMarker.map = mapView
        //        mapView.settings.setAllGesturesEnabled(true)
    }
    
    
    func setAnimatedMapView(loc:CLLocationCoordinate2D){
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        partnerMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        // userMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        CATransaction.commit()
        //        let user : CLLocation = CLLocation.init(latitude: trackingDetail.userLat, longitude: trackingDetail.userLong)
        //        let partner : CLLocation = CLLocation.init(latitude: loc.latitude, longitude: loc.longitude)
        //        let bearing = getBearingBetweenTwoPoints1(user,point2: partner)
        // SocketIOManager.sharedInstance.emitTracking(themes.getUserID(), tasker: providerId, task: jobId, lat:"\(loc.latitude)" , long: "\(loc.longitude)", bearing: "\(bearing)")
    }
    
    
    
    
    
    func setDataToLocationView(){
        addressLbl.text="\(addressStr)"
        if(isShowArriveBtn==true){
            cancelBtn.hidden=false
            
            
            //stepProgress.currentIndex=1
        }else{
        }
    }
    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(point1.coordinate.latitude)
        let lon1 = degreesToRadians(point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(point2.coordinate.latitude)
        let lon2 = degreesToRadians(point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radiansBearing)
    }
    override func viewDidAppear(animated: Bool) {
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "Tracking", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationViewController.methodOfReceivedTrackNotification(_:)), name:"Tracking", object: nil)
        
        
        
        
    }
    
     func methodOfReceivedTrackNotification(notification: NSNotification){
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        partnerMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        if trackingDetail.lastDriving == ""{
            trackingDetail.lastDriving = "0.0"
        }
        if trackingDetail.bearing  == ""{
            trackingDetail.bearing = "0.0"
        }
        partnerMarker.rotation = Double( trackingDetail.lastDriving)! - Double (trackingDetail.bearing)!
        CATransaction.commit()
        let coord = CLLocation.init(latitude:trackingDetail.userLat, longitude: trackingDetail.userLong)
        
        let kilometer : Double = self.kilometersfromPlace(coord.coordinate)*0.6213711922
        print ("get distance \(kilometer)")
        
       
        distancelabl.text = "\(round(kilometer))Miles"
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "Tracking", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "Tracking", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }

    
    func degreesToRadians(degrees: Double) -> Double { return degrees * M_PI / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / M_PI }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
          self.navigationController?.popViewControllerAnimated(true)
       // self.dismissViewControllerAnimated(true, completion: nil)
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // upDateLocation()
        
        if((isPolyLineDrawn) == nil){
            
            drawRoadRouteBetweenTwoPoints(manager.location!.coordinate)
            let kilometer : Double = self.kilometersfromPlace(manager.location!.coordinate)
            print ("get distance \(kilometer)")
            
            
            distancelabl.text = "\(round(kilometer))KM"
            self.set_mapView(manager.location!.coordinate)

        } else{
            setAnimatedMapView(manager.location!.coordinate)
            
            
        }
        
        let kilometer : Double = self.kilometersfromPlace(manager.location!.coordinate)*0.6213711922
        print ("get distance \(kilometer)")
        
        var speed: Double = manager.location!.speed
        speed =  (speed * 3600 )/100
        //converting speed from m/s to knots(unit)
        print("Speed \(speed)")
        distancelabl.text = "\(round(kilometer))Miles"
        
    }
    func kilometersfromPlace(from: CLLocationCoordinate2D) -> Double {
        let userloc : CLLocation = CLLocation(latitude:mapLaat.doubleValue, longitude: mapLong.doubleValue)
        let dest:CLLocation = CLLocation(latitude: (OrderDetail_data.provider_lat).doubleValue, longitude: (OrderDetail_data.provider_long).doubleValue)
        let dist: CLLocationDistance = userloc.distanceFromLocation(dest)
        
        let distanceKM = dist / 1000
        let roundedTwoDigit = round(100 * distanceKM) / 100
        //        return roundedTwoDigit
        //        let distance: NSString = "\(dist)"
        return roundedTwoDigit
    }
    
    @IBAction func didclickgoogleMapbutton(sender: AnyObject) {
        
        let  startSelLocation :CLLocation = CLLocation.init(latitude: OrderDetail_data.provider_lat.doubleValue, longitude: OrderDetail_data.provider_long.doubleValue)
        let dropSelLocation : CLLocation = CLLocation.init(latitude:mapLaat.doubleValue, longitude: mapLong.doubleValue)
        if self.model == nil {
            self.model = MapRequestModel()
            // And let's set our callback URL right away!
            OpenInGoogleMapsController.sharedInstance().callbackURL = NSURL(string: constant.kOpenGoogleMapScheme)
            OpenInGoogleMapsController.sharedInstance().fallbackStrategy = GoogleMapsFallback.ChromeThenAppleMaps
        }
        
        if OpenInGoogleMapsController.sharedInstance().googleMapsInstalled == false {
            print("Google Maps not installed, but using our fallback strategy")
        }
        
        if mapLaat != 0 {
            // [self.model useCurrentLocationForGroup:kLocationGroupStart];
            self.model.setQueryString(nil, center:startSelLocation.coordinate, forGroup: LocationGroup.Start)
            self.model.setQueryString(nil, center: dropSelLocation.coordinate, forGroup: LocationGroup.End)
            self.openDirectionsInGoogleMaps()
        }
        else {
            self.view.makeToast(message: "There is no destination selected")
        }
    }
    func  openDirectionsInGoogleMaps()  {
        
        let directionsDefinition = GoogleDirectionsDefinition()
        if self.model.startCurrentLocation {
            directionsDefinition.startingPoint = nil
        }
        else {
            let startingPoint = GoogleDirectionsWaypoint()
            startingPoint.queryString = self.model.startQueryString
            startingPoint.location = self.model.startLocation
            directionsDefinition.startingPoint = startingPoint
        }
        if self.model.destinationCurrentLocation {
            directionsDefinition.destinationPoint = nil
        }
        else {
            let destination = GoogleDirectionsWaypoint()
            destination.queryString = self.model.destinationQueryString
            destination.location = self.model.desstinationLocation
            directionsDefinition.destinationPoint = destination
        }
        directionsDefinition.travelMode = self.travelMode(asGoogleMapsEnum: self.model.travelMode)
        OpenInGoogleMapsController.sharedInstance().openDirections(directionsDefinition)
        
        
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .NotDetermined, .Restricted, .Denied:
                themes.AlertView("", Message: "\(themes.setLang("location_service_disabled"))\n \(themes.setLang("to_reenable_location")) ", ButtonTitle: kOk)
                break
                
            case .AuthorizedAlways, .AuthorizedWhenInUse: break
                
            }
        } else {
            themes.AlertView("", Message: "\(themes.setLang("location_service_disabled"))\n \(themes.setLang("to_reenable_location")) ", ButtonTitle: kOk)
        }
    }
    func travelMode(asGoogleMapsEnum appTravelMode: TravelMode) -> GoogleMapsTravelMode {
        switch appTravelMode {
        case TravelMode.Bicycling:
            return GoogleMapsTravelMode.Biking
        case TravelMode.Driving:
            return GoogleMapsTravelMode.Driving
        case TravelMode.PublicTransit:
            return GoogleMapsTravelMode.Transit
        case TravelMode.Walking:
            return GoogleMapsTravelMode.Walking
        case TravelMode.NotSpecified:
            return GoogleMapsTravelMode.init(rawValue: 0)!
        }
    }
    
    
    func drawRoadRouteBetweenTwoPoints(loc:CLLocationCoordinate2D) {
        print("Current location of lattitude\(mapLaat) and longtitude \(mapLong)")
        lblStartLoc.text = themes.getAddressForLatLng(OrderDetail_data.provider_lat as! String, longitude: OrderDetail_data.provider_long as! String)
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\((OrderDetail_data.provider_lat).doubleValue),\((OrderDetail_data.provider_long).doubleValue)&destination=\(mapLaat.doubleValue),\(mapLong.doubleValue)&sensor=true&key=\(constant.GooglemapAPI)"
        URL_Handler.makeGetCall(directionURL) { (responseObject) -> () in
            if(responseObject != nil)
            {
                print(responseObject)
                let routes_array = responseObject?.objectForKey("routes") as! NSArray
                
                if(routes_array.count > 0)
                {
                    self.DismissProgress()
                    self.isPolyLineDrawn=true
                    for Dict in routes_array
                    {
                        
                        let overviewPolyline = Dict.objectForKey("overview_polyline")!.objectForKey("points") as! String
                        let path:GMSPath=GMSPath(fromEncodedPath: overviewPolyline)
                        let SingleLine:GMSPolyline=GMSPolyline(path: path)
                        SingleLine.strokeWidth=10.0
                        SingleLine.strokeColor=PlumberBlueColor
                        SingleLine.map=self.mapView
                        
                        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(path: path)
                        let update: GMSCameraUpdate = GMSCameraUpdate.fitBounds(bounds, withPadding: 100.0)
                        //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
                        self.mapView.animateWithCameraUpdate(update)
                        
                    }
                    
                    self.set_mapView(loc)
                }else{
                    
                }
                
            }else{
                self.DismissProgress()
                self.isPolyLineDrawn=true
                themes.AlertView("\(Appname)", Message:themes.setLang("No Routes found. Please try again"), ButtonTitle: kOk)

            }
        }
    }

    
    //    func upDateLocation(){
    //
    //        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
    //        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
    //            "latitude":"\(lat)",
    //            "longitude":"\(lon)"]
    //        // print(Param)
    //
    //        url_handler.makeCall(updateProviderLocation, param: Param) {
    //            (responseObject, error) -> () in
    //
    //            if(error != nil)
    //            {
    //                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
    //            }
    //            else
    //            {
    //                if(responseObject != nil && responseObject?.count>0)
    //                {
    //                    let status:NSString=responseObject?.objectForKey("status") as! NSString
    //
    //                    if(status == "1")
    //                    {
    //
    //
    //                    }
    //                    else
    //                    {
    //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
    //                    }
    //                }
    //                else
    //                {
    //                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
    //                }
    //            }
    //
    //        }
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickPhoneBtn(sender: AnyObject) {
        callNumber(phoneStr as String)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string:"tel://"+"\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
