//
//  MenuViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 03/03/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//


import UIKit
import MessageUI


class MenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate  {
    @IBOutlet weak var tableView: UITableView!
    var segues = [String]()
    let Icons = ["Support_Black", "Purchase Order","Transaction","notification","review","Chatsupport","emergency_contact","Talk","Warning Shield","chaticon","About-30"]
    
    
    
    //,"Wallet Filled"
    
    
    var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate
    var URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()
    var GetReceipientMail : NSString = ""
    var Citylistarray:NSMutableArray=NSMutableArray()
    var Cityidarray:NSMutableArray=NSMutableArray()
    let activityTypes: [NVActivityIndicatorType] = [
        .BallPulse]
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRectMake(0, 0, 0, 0),
                                                        type: .BallSpinFadeLoader)
    var trimmed_Location:String=String()
    
    
    @IBOutlet var Email_But: UIButton!
    @IBOutlet var User_Name: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var UserImage: UIImageView!
    @IBOutlet var signImage: UIImageView!
    @IBOutlet var Arrow_Indicator: UIImageView!
    
    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignIn.setTitle(themes.setLang("signin"), forState: UIControlState.Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        segues = [themes.setLang("home"),
                  themes.setLang("my_orders"),
                  //themes.setLang("my_money"),
            themes.setLang("transactions_caps"),
            themes.setLang("notifications_caps"),
            themes.setLang("reviews_caps"),
            themes.setLang("chat_support"),
            themes.setLang("emergency_contact"),
            themes.setLang("invite_friends"),
            themes.setLang("report_issues"),
            themes.setLang("chat"),
            themes.setLang("about_us")
           
        ]
        
        let nibName = UINib(nibName: "SlideCustomTableViewCell", bundle:nil)
        self.tableView.registerNib(nibName, forCellReuseIdentifier: "SlideCustomTableViewCell")
        let nibName1 = UINib(nibName: "LocationTableViewCell", bundle:nil)
        self.tableView.registerNib(nibName1, forCellReuseIdentifier: "Cell")
        tableView.estimatedRowHeight = 45
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView=UIView()
        tableView.separatorColor=UIColor.clearColor()
        Setdata()
        self.getAppinformation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: -  Function
    
    func Setdata() {
        
        //Divya
        Menu_dataMenu.Choose_Location=themes.getLocationname()
        if themes.getEmailID() == ""{
            btnSignIn.hidden = false
            signImage.hidden = false
            UserImage.hidden = true
            btnSignIn.setTitle(themes.setLang("login"), forState: UIControlState.Normal)
            btnSignIn.layer.cornerRadius = 5
        } else{
            btnSignIn.hidden = true
            signImage.hidden = true
            UserImage.hidden = false
            tableView.userInteractionEnabled = true
            Email_But.setTitle("(\(themes.getCountryCode()))\(themes.getMobileNum())", forState: UIControlState.Normal)
            Email_But.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping;
            UserImage.layer.cornerRadius=UserImage.frame.size.width/2
            UserImage.clipsToBounds=true
            User_Name.setTitle("\(themes.getUserName())", forState: UIControlState.Normal)
        }
        
        if themes.getMobileNum() ==  ""{
            Email_But.setTitle("", forState: UIControlState.Normal)
        }
        
        trimmed_Location=Menu_dataMenu.Choose_Location.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if themes.getuserDP().isEmpty{
            UserImage.image = UIImage(named:"user")!
        } else {
            UserImage.sd_setImageWithURL(NSURL(string: "\(themes.getuserDP())"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            //            User_Name.setTitle("\(themes.getUserName())", forState: UIControlState.Normal)
            //            User_Name.titleLabel?.sizeToFit()
        }
    }
    
    
    
    func  getAppinformation()
    {
        
        let URL_Handler:URLhandler=URLhandler()
        URL_Handler.makeCall(constant.Appinfo_url, param: [:]) {
            (responseObject, error) -> () in
            
            if(error != nil)
            {
                
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        
                        self.GetReceipientMail = self.themes.CheckNullValue(responseObject?.objectForKey("email_address"))!
                        
                        
                    }
                    else
                    {
                    }
                    
                    
                }
            }
        }
        
    }
    
    func showProgress() {
        self.activityIndicatorView.color = themes.DarkRed()
        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        self.activityIndicatorView.startAnimation()
        self.view.addSubview(activityIndicatorView)
    }
    
    func DismissProgress() {
        self.activityIndicatorView.stopAnimation()
        self.activityIndicatorView.removeFromSuperview()
    }
    
    func Logout(sender:UIButton){
        self.LogoutMethod()
    }
    
    func LogoutMethod(){
        self.showProgress()
        let Param: Dictionary = ["user_id":"\(themes.getUserID())","device_type":"ios"]
        URL_handler.makeCall(constant.Logout_url, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)  {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            } else {
                print("the response is \(responseObject)")
                if(responseObject != nil) {
                    let status:NSString?=self.themes.CheckNullValue(responseObject?.objectForKey("status")!)
                    if(status! == "0") {
                    }
                    self.Appdel.CheckDisconnect()
                    SocketIOManager.sharedInstance.LeaveRoom(self.themes.getUserID())
                    SocketIOManager.sharedInstance.LeaveChatRoom(self.themes.getUserID())
                    SocketIOManager.sharedInstance.RemoveAllListener();
                    dbfileobj.deleteUser("Provider_Table")
                    let appDomain: String = NSBundle.mainBundle().bundleIdentifier!
                    //  NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                    
                    //Divya
                    NSUserDefaults.standardUserDefaults().removeObjectForKey("userID")
                    NSUserDefaults.standardUserDefaults().removeObjectForKey("EmailID")
                    
                    
                 
                    self.Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")
                }  else {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }
    }
    
    func showSendMailErrorAlert() {
        themes.AlertView(themes.setLang("not_send_email"), Message: themes.setLang("device_not_send_email"), ButtonTitle: kOk)
    }
    //MARK: - Button Function
    
    @IBAction func didClickoptions(sender: UIButton) {
        if(sender.tag == 2) {
            if themes.getEmailID() == ""{
                Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")
            } else {
                Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "ProfileVCID")
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var Count:Int=Int()
        if themes.getEmailID() == "" {
            Count = 0;
        }  else{
            Count = segues.count+1
        }
        return Count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SlideCustomTableViewCell", forIndexPath: indexPath) as! SlideCustomTableViewCell
        if themes.getEmailID() == "" {
            cell.Menulist.hidden=true
            cell.MenuIcon.hidden=true
        } else {
            if(indexPath.row == segues.count) {
                cell.Menulist.hidden=true
                cell.MenuIcon.hidden=true
                cell.Logout_Btn.hidden=false
                cell.Logout_Btn.setTitle(themes.setLang("logout"), forState: UIControlState.Normal)
                //cell.Wallet_Amount.hidden=true
                cell.Logout_Btn.layer.cornerRadius=3.0
                cell.Logout_Btn.addTarget(self, action: #selector(MenuVC.Logout(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                cell.SeperatorLab.hidden=true
            }
            else  {
                cell.Menulist.hidden=false
                cell.MenuIcon.hidden=false
                cell.Logout_Btn.hidden=true
                cell.Menulist.text = segues[indexPath.row]
                cell.MenuIcon.image=UIImage(named:"\(Icons[indexPath.row])")
                cell.SeperatorLab.hidden=false
                if(indexPath.row == 2){
                    // cell.Wallet_Amount.hidden=false
                    NSLog("get currency name=%@ and value=%@ ",themes.getCurrencyCode(),themes.getCurrency() )
                    //                    if themes.getCurrency() == "" {
                    //                        cell.Wallet_Amount.text="\(themes.getCurrencyCode())0"
                    //                    }else {
                    //                        cell.Wallet_Amount.text="\(themes.getCurrencyCode())\(themes.getCurrency())"
                    //                    }
                }
                //                    else {
                //                    cell.Wallet_Amount.hidden=true
                //                }
            }
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.row == 0) {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "HomePageVCID")
        }
        if(indexPath.row == 1) {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "OrderVCID")
            
        }
        //        if(indexPath.row == 2) {
        //            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "WalletVCID") //PaymentVCID //ProfileVCID
        //        }
        
        if(indexPath.row == 2)
        {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "TransactionVCID")
            
            
            
            
        }
        
        if (indexPath.row == 3)
        {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "NotificationVCID")
        }
        
        if(indexPath.row == 4)
        {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "ReviewVCID")
        }
        if (indexPath.row ==  5)
        {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "ChatSupport")
        }
        
        if(indexPath.row == 6)  {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "EmergencyVCID")
        }
        if(indexPath.row == 7) {
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "InviteVCID")
        }
        if(indexPath.row == 8) {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        }
        if (indexPath.row == 9){
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "ChatList")
            //            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ChatList") as! ChatListViewController
            //            self.navigationController?.pushViewController(secondViewController, animated: true)
            
            
        }
        if (indexPath.row == 10){
            Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "AboutusVCID")
        }
    }
    
    //MARK: - MFMailComposeViewController
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["\(self.GetReceipientMail)"])
        mailComposerVC.setSubject("\(themes.setLang("report_on")) \(Appname) \(themes.setLang("ios_app"))")
        //            mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        return mailComposerVC
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewControllerWithIdentifier("HomePageVCID") as! DLHamburguerNavigationController
    }
    
    
}


