//
//  NotificationsVCViewController.swift
//  PlumberJJ
//
//  Created by Casperon on 08/02/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class NotificationsVCViewController:
RootViewController,UITableViewDelegate,UITableViewDataSource {
    let URL_Handler:URLhandler=URLhandler()

    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var notification_table: STCollapseTableView!
    var ResponseDict : NSMutableArray = NSMutableArray()
    var  CategoryArray : NSMutableArray = NSMutableArray()
    var bookingidArray : NSMutableArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title_lbl.text = themes.setLang("notification")
        notification_table.registerNib(UINib(nibName: "notificationVCTableViewCell", bundle: nil), forCellReuseIdentifier: "notification")
        notification_table.estimatedRowHeight = 90
        notification_table.rowHeight = UITableViewAutomaticDimension
       
        
        notification_table.tableFooterView = UIView()
       // [self.tableView setExclusiveSections:!self.tableView.exclusiveSections];
        self.notification_table.exclusiveSections = self.notification_table.exclusiveSections

       self.notification_table.openSection(0, animated: false)

        showProgress()
        self.GetNotifications()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func GetNotifications(){
        
        
        let Param: Dictionary = ["user_id":"\(themes.getUserID())",
                                 "role":"user"]
        // print(Param)
        
        URL_Handler.makeCall( constant.GetNotificationUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.notification_table.hidden=false
            self.notification_table.dg_stopLoading()
            if(error != nil)
            {
                self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict:NSDictionary=responseObject!
                    let status:NSString=themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        
                        self.ResponseDict = (Dict.valueForKey("response")! as? NSMutableArray)!
                        
                        
                        for  Dict in self.ResponseDict
                        {
                            
                            let category : String = themes.CheckNullValue(Dict.objectForKey("booking_id"))!
                            let jobid: String = themes.CheckNullValue(Dict.objectForKey("category"))!
                            self.CategoryArray.addObject(category)
                            self.bookingidArray.addObject(jobid)
                            
                        }
                                              self.notification_table.reloadData()
                        
                    }else{
                        let message:NSString=themes.CheckNullValue(responseObject!.objectForKey("response") as? NSString)!
                        themes.AlertView(Appname, Message: message, ButtonTitle: kOk)

                    }
                    
                    
                }
                else
                {
                    self.view.makeToast(message:constant.kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
                }
            }
        }
    }
    
    
    @IBAction func menubtnAct(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()

//        self.view.endEditing(true)
//        self.frostedViewController.view.endEditing(true)
//        // Present the view controller
//        //
//        self.frostedViewController.presentMenuViewController()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.CategoryArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        let cell3:UITableViewCell
        
        let cell:notificationVCTableViewCell = tableView.dequeueReusableCellWithIdentifier("notification") as! notificationVCTableViewCell
        cell.message.text = themes.CheckNullValue(ResponseDict.objectAtIndex(indexPath.section).objectForKey("messages")!.objectAtIndex(indexPath.row).objectForKey("message"))!
        cell.timelable.text = themes.CheckNullValue(ResponseDict.objectAtIndex(indexPath.section).objectForKey("messages")!.objectAtIndex(indexPath.row).objectForKey("createdAt"))!
        
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        cell3=cell
        
        
        return cell3
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let MessagesArray : NSArray = ResponseDict.objectAtIndex(section).objectForKey("messages")  as! NSArray
        return MessagesArray.count;
    }
    
    
    func tableView( tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView!, viewForHeaderInSection section: Int) -> UIView!{
        
        let mainview = UIView.init(frame: CGRectMake(self.notification_table.frame.origin.x,0,self.notification_table.frame.size.width, 50))
        let header = UIView(frame: CGRectMake(20, 10, self.notification_table.frame.size.width-40, 50))
        header.backgroundColor = UIColor.whiteColor()
        header.layer.borderColor=UIColor.grayColor().CGColor
        header.layer.borderWidth = 1.0;
        header.layer.cornerRadius = 10
        let btnimg :UIImageView = UIImageView(frame:CGRectMake(self.notification_table.frame.size.width-80,10,20,20))

        let lable : UILabel = UILabel(frame: CGRectMake(0, 5, header.frame.size.width, 40))
        btnimg.image = UIImage(named:"black_back")
        lable.text  = "\(themes.CheckNullValue(self.CategoryArray.objectAtIndex(section))!) - \(themes.CheckNullValue(self.bookingidArray.objectAtIndex(section))!)"
        lable.font = UIFont.init(name:"Roboto-Regular", size:14)
        lable.textAlignment = .Center
        lable.numberOfLines = 2
        header.addSubview(lable)
        header.addSubview(btnimg)
        mainview.addSubview(header)
        return mainview
        
    }
    
    deinit {
        if (self.notification_table != nil)
        {
         self.notification_table.delegate = nil
        }
    }

    
    
    //     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        return self.headers[section]
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
