//
//  ChooseCityViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 23/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit





class ChooseCityViewController: RootViewController {

    @IBOutlet var selectcity: UILabel!
    @IBOutlet var steptoFinish: CustomLabel!
    @IBOutlet var City_tableView: UITableView!
    let themes:Themes=Themes()
    var URL_handler:URLhandler=URLhandler()
    var Citylistarray:NSMutableArray=NSMutableArray()
    var Cityidarray:NSMutableArray=NSMutableArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        selectcity.text=themes.setLang("select_city")

        steptoFinish.text=themes.setLang("stepto_finish")
        City_tableView.tableFooterView=UIView()
        
        City_tableView.hidden=true
        
        City_tableView.backgroundColor=UIColor.clearColor()
        
        
        let nibName = UINib(nibName: "LocationTableViewCell", bundle:nil)
        self.City_tableView.registerNib(nibName, forCellReuseIdentifier: "Cell")
        reloadCityData()


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadCityData()
    {
        
        self.showProgress()
        
        //let Param: Dictionary<String, String> = [:]
        
        URL_handler.makeGetCall(constant.Get_Location){ (responseObject) -> () in
      //  URL_handler.makeCall(constant.Get_Location, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
           
//            if(error != nil)
//            {
//                self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
//            }
//                
//            else
//            {
            


            
            if(responseObject != nil)
            {
                let json = JSON(responseObject!)
                
                
                
                let status:NSString=json["status"].string!
                
                
                if(status == "0")
                {
                    
                    self.themes.AlertView("\(Appname)",Message: self.themes.setLang("no_location_found"),ButtonTitle: kOk)
                    
                }
                else
                {
                    self.City_tableView.hidden=false

                    
                    
                    let Locationarray:NSArray=responseObject?.objectForKey("response")!.objectForKey("locations") as! NSArray
                    
                    
                    
                    for  Dict in Locationarray
                    {
                        
                        let city_name=Dict.objectForKey("city") as! String
                        
                        let id=Dict.objectForKey("id") as! String
                        
                        self.Citylistarray.addObject(city_name)
                        self.Cityidarray.addObject(id)
                        
                        self.City_tableView.reloadData()
                        
                    }
                    
                }
                
                
                
                
            }
            else
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
            
            
            
        //}
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Citylistarray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell:LocationTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! LocationTableViewCell
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        Cell.City_Name_Lab.text=Citylistarray.objectAtIndex(indexPath.row)  as? String
        
        
        
        
        
        if(Menu_dataMenu.Location_Detail == Citylistarray.objectAtIndex(indexPath.row)  as? String)
        {
            
            Cell.markerView.hidden=false
            
            Cell.Check_Mark.hidden=false
            
            
        }
            
        else
            
        {
            Cell.markerView.hidden=true
            
            Cell.Check_Mark.hidden=true
            
            
            
        }
        
        Cell.backgroundColor=UIColor.clearColor()
        
        
        
        return Cell
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        signup.currency_Sym=self.themes.Currency_Symbol(signup.currency as String)
         self.themes.saveCountryCode(signup.Country_Code as String!)
        self.themes.saveLocationname(signup.Locationname as String)
        self.themes.saveCurrency(signup.Walletamt as String)
        self.themes.saveCurrencyCode(signup.currency_Sym as String)
        self.themes.saveUserID(signup.Userid as String)
        self.themes.saveUserPasswd(signup.Password as String)
        self.themes.saveUserName(signup.username as String)
        self.themes.saveEmailID(signup.Email as String)
        self.themes.saveuserDP(signup.Userimage as String)
        self.themes.saveMobileNum(signup.Contact_num as String)
        self.themes.saveWalletAmt(signup.Walletamt as String)
        self.themes.saveJaberID(signup.user_id as String)
        self.themes.saveJaberPassword(signup.soc_key as String)


        
        
        Menu_dataMenu.Location_Detail="\(Citylistarray.objectAtIndex(indexPath.row))"
        
        
        
        City_tableView.reloadData()
        
        UpdateLocation("\(Cityidarray.objectAtIndex(indexPath.row))")
        
        signup.Locationname="\(Citylistarray.objectAtIndex(indexPath.row))"

        self.themes.saveLocationname("\(Citylistarray.objectAtIndex(indexPath.row))")

         Menu_dataMenu.Location_Detail=""
        
     }
    func UpdateLocation(Cityid:NSString)
    
    {
        
        self.showProgress()
        let Param: Dictionary = ["user_id":"\(themes.getUserID())","location_id":"\(Cityid)"]
         URL_handler.makeCall(constant.Update_Location, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

            }
                
            else
            {
                
             if(responseObject != nil)
            {
                let json = JSON(responseObject!)
                
                
                
                let status:NSString=json["status"].string!
                
                
                if(status == "0")
                {
                    self.themes.AlertView("\(Appname)",Message: self.themes.setLang("no_location_found"),ButtonTitle: kOk)
                    
                }
                else
                {
                    
                    signup.Locationid=json["location_id"].string!
                    self.themes.saveLocationID(signup.Locationid as String)
              
                    //Home
//                    self.performSegueWithIdentifier("HomeVC", sender: nil)
                   Appdel.MakeRootVc("RootVCID")

                    
                }
   
            }
            else
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
            
            
            
        }
        }

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


