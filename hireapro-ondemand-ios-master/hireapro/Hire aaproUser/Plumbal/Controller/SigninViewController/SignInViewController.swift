//
//  SignInViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 01/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class SignInViewController: RootViewController,UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet var Forgot_Btn: UIButton!
    @IBOutlet var Signupnow_Btn: UIButton!
    @IBOutlet var Not_Lbl: UILabel!
    @IBOutlet var Signin_ScrollView: UIScrollView!
    @IBOutlet var EmaiidTextfield: UITextField!
    @IBOutlet var PasswordTextfield: UITextField!
    @IBOutlet var Signin_But: UIButton!
    @IBOutlet weak var facebook_btn: UIButton!
    
    let facebookReadPermissions = ["public_profile", "email", "user_friends"]
    var themes:Themes=Themes()
    var Emailid:NSString=NSString()
    var Password:NSString=NSString()
    var validateemail:Bool=Bool()
    var validatepasswd : Bool = Bool()
    var URL_handler:URLhandler=URLhandler()
    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        Signin_But.enabled=true
        lblSignIn.text = themes.setLang("login")
        Forgot_Btn.setTitle(themes.setLang("forgot_password_btn"), forState: UIControlState.Normal)
        Signin_But.setTitle(themes.setLang("login"), forState: UIControlState.Normal)
        Signupnow_Btn.setTitle(themes.setLang("register"), forState: UIControlState.Normal)
        EmaiidTextfield.autocapitalizationType = .None;
        EmaiidTextfield.placeholder=themes.setLang("email_placeholder")
        PasswordTextfield.placeholder=themes.setLang("password_placeholder")
        facebook_btn.setTitle(themes.setLang("facebook"), forState: UIControlState.Normal)
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(SignInViewController.DismissKeyboard(_:)))
        view.addGestureRecognizer(tapgesture)
    }
    
    override func viewWillAppear(animated: Bool) {
        Signin_But.enabled=true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TextField Delegate
    

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == EmaiidTextfield) {
            EmaiidTextfield.resignFirstResponder()
            PasswordTextfield.becomeFirstResponder()
        } else {
            PasswordTextfield.resignFirstResponder()
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == EmaiidTextfield{
        let aSet = NSCharacterSet(charactersInString: ACCEPTABLE_CHARACTERS).invertedSet
        let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
        let numberFiltered = compSepByCharInSet.joinWithSeparator("")
        return string == numberFiltered
        }
      else if textField == EmaiidTextfield {
            if let _ = string.rangeOfCharacterFromSet(NSCharacterSet.uppercaseLetterCharacterSet()) {
                return false
            }
            return true
        }else {
            return true
        }
    }
    
    //MARK: - Button Action
    
    
    @IBAction func didclickFacebook(sender: AnyObject) {
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logOut()
        login.logInWithReadPermissions(facebookReadPermissions,
                                       fromViewController:self ,handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
                                        if (error == nil){
                                            self.returnUserData()
                                        }  else {
                                            let loginManager = FBSDKLoginManager()
                                            loginManager.logOut()
                                        }
        })
    }
    
    @IBAction func DidClickoptions(sender: UIButton) {
        self.view.endEditing(true)
        if(sender.tag == 2){
            self.login_account()
        }
        if(sender.tag == 4) {
            self.performSegueWithIdentifier("SignupVC", sender: nil)
        }
        
        if(sender.tag == 10){
            if self.themes.getaddresssegue() ==  "1" {
                self.performSegueWithIdentifier("ScheduleVC", sender: nil)
            } else{
                Appdel.MakeRootVc("RootVCID")
            }
        }
    }
    
    //MARK: - Function
    
    func DismissKeyboard(sender:UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func returnUserData(){
        Signin_But.enabled=false
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if ((error) != nil) {
                print("the rres asd")
            } else {
                let json = JSON(result!)
                print("the rres is \(json)")
                let userName : NSString = self.themes.CheckNullValue(result.valueForKey("name"))!
                let userEmail : NSString = self.themes.CheckNullValue(result.valueForKey("email"))!
                let firstName : NSString = self.themes.CheckNullValue(result.valueForKey("first_name"))!
                let lastName : NSString = self.themes.CheckNullValue(result.valueForKey("last_name"))!
                let userID : NSString = self.themes.CheckNullValue(result.valueForKey("id"))!
                let Profie_Pic:NSString?=json["picture"]["data"]["url"].string!
                let Pic_Status:NSString?=json["picture"]["data"]["url"].string!
                if(Profie_Pic != nil) {
                    FB_Regis.FB_Picture=Profie_Pic!
                }
                if(Pic_Status != nil){
                    FB_Regis.FB_Pic_Status=Pic_Status!
                }
                FB_Regis.FB_Firstname=firstName
                FB_Regis.FB_lastname=lastName
                FB_Regis.FB_Username=userName
                FB_Regis.FB_mailid=userEmail
                FB_Regis.FB_UserId=userID
                let param=["email_id":"\(FB_Regis.FB_mailid)","deviceToken":"\(Device_Token)","fb_id":"\(FB_Regis.FB_UserId)","prof_pic":"\(FB_Regis.FB_Picture)"]
                self.showProgress()
                self.URL_handler.makeCall(constant.Social_login, param: param, completionHandler:{ (responseObject, error) -> () in
                    self.Signin_But.enabled=true
                    self.DismissProgress()
                    if(error != nil) {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }  else {
                        if(responseObject != nil) {
                            let dict:NSDictionary=responseObject!
                            signup.status=self.themes.CheckNullValue(dict.objectForKey("status"))!
                            if(signup.status == "0")  {
                                let messagestr = self.themes.CheckNullValue(dict.objectForKey("message"))!
                                if messagestr == "Your account is currently unavailable" {
                                    self.performSegueWithIdentifier("FacebookVC", sender: nil)
                                }
                            }
                            
                            if (signup.status == "1")  {
                                signup.Check_Live=dict.objectForKey("is_alive_other") as! NSString
                                if(signup.Check_Live == "Yes"){
                                    self.themes.AlertView("\(Appname)", Message:self.themes.setLang("sign_out_all_device"), ButtonTitle: kOk)
                                }
                                signup.username=dict.objectForKey("user_name") as! NSString
                                signup.Email=dict.objectForKey("email") as! NSString
                                signup.Contact_num=self.themes.CheckNullValue(dict.objectForKey("phone_number"))!
                                signup.currency=dict.objectForKey("currency") as! NSString
                                signup.Walletamt = self.themes.CheckNullValue(dict.objectForKey("wallet_amount"))!
                                signup.Userimage=dict.objectForKey("prof_pic") as! NSString
                                signup.Userid=dict.objectForKey("user_id") as! NSString
                                signup.Locationname=dict.objectForKey("location_name") as! NSString
                                signup.Country_Code=self.themes.CheckNullValue(dict.objectForKey("country_code"))!
                                signup.currency_Sym=self.themes.Currency_Symbol(signup.currency as String)
                                // signup.soc_key=dict.objectForKey("soc_key") as! NSString
                                signup.user_id=dict.objectForKey("user_id") as! NSString
                                if (dict.objectForKey("location_id") != nil) {
                                    signup.Locationid=dict.objectForKey("location_id") as! NSString
                                }
                                
                                self.themes.saveCountryCode(signup.Country_Code as String!)
                                self.themes.saveLocationname(signup.Locationname as String)
                                self.themes.saveLocationname(signup.Locationname as String)
                                self.themes.saveCurrency(signup.Walletamt as String)
                                self.themes.saveCurrencyCode(signup.currency_Sym as String)
                                self.themes.saveUserID(signup.Userid as String)
                                self.themes.saveUserName(signup.username as String)
                                self.themes.saveEmailID(signup.Email as String)
                                self.themes.saveUserPasswd(signup.Password as String)
                                self.themes.saveuserDP(signup.Userimage as String)
                                self.themes.saveMobileNum(signup.Contact_num as String)
                                self.themes.saveWalletAmt(signup.Walletamt as String)
                                self.themes.saveLocationID(signup.Locationid as String)
                                self.themes.saveJaberID(signup.user_id as String)
                                self.themes.saveJaberPassword(signup.soc_key as String)
                                SocketIOManager.sharedInstance.establishConnection()
                                SocketIOManager.sharedInstance.establishChatConnection()
                                if self.themes.getaddresssegue() ==  "1" {
                                    self.performSegueWithIdentifier("ScheduleVC", sender: nil)
                                    
                                }else {
                                    Appdel.MakeRootVc("RootVCID")
                                }
                            }
                            if(signup.status == "2"){
                                self.performSegueWithIdentifier("FacebookVC", sender: nil)
                            }
                        } else {
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                    }
                })
            }
        })
    }
    
    func login_account(){
        Emailid=EmaiidTextfield.text!
        Password=PasswordTextfield.text!
        validateemail=themes.isValidEmail(Emailid as String)
        validatepasswd = themes.validpasword(Password)
        if(EmaiidTextfield.text == "") {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_email_alert"),ButtonTitle: kOk)
        }else if (PasswordTextfield.text == ""){
            themes.AlertView("\(Appname)",Message:themes.setLang("enter_password_alert") ,ButtonTitle: kOk)
        }  else if(Password.length < 6 ) {
            themes.AlertView("\(Appname)",Message: themes.setLang("valid_password"),ButtonTitle: kOk)
        } else{
            self.showProgress()
            Signin_But.enabled=false
            let parameter=[ "email":"\(Emailid)","password":"\(Password)" ,"deviceToken":"\(Device_Token)","gcm_id":""]
            print("\(constant.Login).....\(parameter)")
            NSLog("Login url=%@ and parameters=%@",constant.Login,parameter)
            URL_handler.makeCall(constant.Login.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), param: parameter, completionHandler: { (responseObject, error) -> () in
                self.Signin_But.enabled=true
                self.DismissProgress()
                if(error != nil) {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                } else {
                    if(responseObject != nil) {
                        let dict:NSDictionary=responseObject!
                        signup.status = self.themes.CheckNullValue(dict.objectForKey("status"))!
                        if (signup.status == "1") {
                            signup.Check_Live=dict.objectForKey("is_alive_other") as! NSString
                            if(signup.Check_Live == "Yes") {
                                self.themes.AlertView("\(Appname)", Message:self.themes.setLang("sign_out_all_device"), ButtonTitle: kOk)
                            }
                            
                            signup.username=dict.objectForKey("user_name") as! NSString
                            signup.Email=dict.objectForKey("email") as! NSString
                            signup.Password=self.Password
                            signup.Contact_num=self.themes.CheckNullValue(dict.objectForKey("phone_number"))!
                            signup.currency=dict.objectForKey("currency") as! NSString
                            signup.Walletamt = self.themes.CheckNullValue(dict.objectForKey("wallet_amount"))!
                            signup.Userimage=dict.objectForKey("user_image") as! NSString
                            signup.Userid=dict.objectForKey("user_id") as! NSString
                            signup.Locationname=dict.objectForKey("location_name") as! NSString
                            signup.Country_Code=self.themes.CheckNullValue(dict.objectForKey("country_code"))!
                            signup.currency_Sym=self.themes.Currency_Symbol(signup.currency as String)
                            // signup.soc_key=dict.objectForKey("soc_key") as! NSString
                            signup.user_id=dict.objectForKey("user_id") as! NSString
                            if (dict.objectForKey("location_id") != nil) {
                                signup.Locationid=dict.objectForKey("location_id") as! NSString
                            }
                            self.themes.saveCountryCode(signup.Country_Code as String!)
                            self.themes.saveLocationname(signup.Locationname as String)
                            self.themes.saveLocationname(signup.Locationname as String)
                            self.themes.saveCurrency(signup.Walletamt as String)
                            self.themes.saveCurrencyCode(signup.currency_Sym as String)
                            self.themes.saveUserID(signup.Userid as String)
                            self.themes.saveUserName(signup.username as String)
                            self.themes.saveEmailID(signup.Email as String)
                            self.themes.saveUserPasswd(signup.Password as String)
                            self.themes.saveuserDP(signup.Userimage as String)
                            self.themes.saveMobileNum(signup.Contact_num as String)
                            self.themes.saveWalletAmt(signup.Walletamt as String)
                            self.themes.saveLocationID(signup.Locationid as String)
                            self.themes.saveJaberID(signup.user_id as String)
                            self.themes.saveJaberPassword(signup.soc_key as String)
                            SocketIOManager.sharedInstance.establishConnection()
                            SocketIOManager.sharedInstance.establishChatConnection()
                            if self.themes.getaddresssegue() ==  "1" {
                                self.performSegueWithIdentifier("ScheduleVC", sender: nil)
                                
                            } else {
                                Appdel.MakeRootVc("RootVCID")
                            }
                        } else{
                            signup.message =  self.themes.CheckNullValue(dict.objectForKey("message"))!
                            self.themes.AlertView("\(Appname)",Message: signup.message  ,ButtonTitle: kOk)
                        }
                    } else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
                return
            })
        }
    }
    
}



