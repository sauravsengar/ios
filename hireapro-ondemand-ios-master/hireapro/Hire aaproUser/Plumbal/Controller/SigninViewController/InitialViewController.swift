//
//  InitialViewController.swift
//  Plumbal
//
//  Created by Casperon iOS on 17/2/2017.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

//Divya
import UIKit

class InitialViewController: RootViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MKMapViewDelegate,BookingViewDelegate,CategorySearchDelegate {
    
    
    @IBOutlet var book_now: UIButton!
    @IBOutlet var book_later_btn: UIButton!
    
    @IBOutlet var catBtn: UIButton!
    @IBOutlet var categorySearch: UIView!
    @IBOutlet var viewMap:GMSMapView!
    @IBOutlet var viewService:UIView!
    @IBOutlet var viewMenu:UIView!
    @IBOutlet var viewSearch:UIView!
    @IBOutlet var textLocation: UITextField!
    @IBOutlet var categoryCollectionView: UICollectionView!
    @IBOutlet var subCategoryCollectionView: UICollectionView!
    @IBOutlet var btnMarker: UIButton!
    @IBOutlet var btnLogin: UIButton!
    
    @IBOutlet var listviewbtn: UIButton!
    @IBOutlet var categoryview: UIView!
    var id:String = String()
    var serviceTax:String = String()
    var category:String = String()
    var slug:String = String()
    var parentid:String = String()
    var statusValue:String = "0"
    var nibView1:MarkerView!
    var locationManager = CLLocationManager()
    var currentLatitide:String!
    var currentLongitude:String!
    var selectedMarker:GMSMarker!
    var urlHandler = URLhandler()
    var refreshControl:UIRefreshControl=UIRefreshControl()
    var themes:Themes=Themes()
    var CategoryidArray:NSMutableArray=NSMutableArray()
    var ISSelectedArray : NSMutableArray = NSMutableArray()
    var CategoryimageArray:NSMutableArray=NSMutableArray()
    var CategoryInactiveImagArray : NSMutableArray = NSMutableArray()
    var Child_StatusArray:NSMutableArray=NSMutableArray()
    var CategorynameArray:NSMutableArray=NSMutableArray()
    var subCategoryImageArray:NSMutableArray=NSMutableArray()
    var subCategoryListArray:NSMutableArray=NSMutableArray()
    var subCategoryListidArray:NSMutableArray=NSMutableArray()
    var SubCategoryListStatusArray:NSMutableArray=NSMutableArray()
    var subCategoryListImageArray:NSMutableArray=NSMutableArray()
    var subCategoryListActiveImageArray:NSMutableArray=NSMutableArray()
    var subCategoryListInactiveImageArray:NSMutableArray=NSMutableArray()
    var nameArray:NSMutableArray=NSMutableArray()
    var BadgeArray:NSMutableArray=NSMutableArray()
    
    var imgArray:NSMutableArray=NSMutableArray()
    var labelArray:NSMutableArray=NSMutableArray()
    
    
    
    
    var providerList = NSArray()
    var fullAddress = String()
    var providerChosen = false
    var status : Int!
    var tempUserData = ProviderMapDetails()
    var layer = String()
    var selectedIndex = Int ()
    var search_Scenario:String = "None"
    var mainCat_ID:String = String()
    var subCat_ID:String = String()
    var subCat_Arrived:String = "No"
    var subCat_ArriveIndex:NSIndexPath = NSIndexPath()
    var searchCat_Title:String = String()
    var mainCatSel_Index:Int = Int()
    var subCatSel_Index:Int = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nibView1 = NSBundle.mainBundle().loadNibNamed("MarkerView", owner: self, options: nil)[0] as! MarkerView
        
        viewService.hidden = true
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
        self.categoryCollectionView.hidden = true
        subCategoryCollectionView.hidden = true
        self.btnLogin.hidden = true
        categoryview.hidden = true
        if  search_Scenario == "Yes"{
            
        }
        else{
            
            setPage()
            
        }
        
        // setPage()
    }
    override func viewDidAppear(animated: Bool) {
        
        
        if themes.getEmailID() == ""{
            btnLogin.hidden = false
            // Blink()
        }else{
            btnLogin.hidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: -Function
    
    func setPage(){
        viewMenu.roundOffBorder()
        viewSearch.roundOffBorder()
        
        viewMenu.roundOffBorder()
        viewService.roundOffBorder()
        textLocation.delegate = self
        viewMap.delegate = self
        self.showProgress()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(InitialViewController.getLocationId(_:)), name: "Location", object: nil)
        
        self.Home_Datafeed()
    }
    
    @IBAction func Didclicklistview(sender: AnyObject) {
        
        
        
        if currentLatitide != ""
        {
            cusProviderRec.searchlat = currentLatitide
            cusProviderRec.searchlng = currentLongitude
            let controller  = self.storyboard?.instantiateViewControllerWithIdentifier("Categorylist") as! CategorylistViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func DidclickPush(sender: AnyObject) {
        
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("CatVCID") as! CategorySearchViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func searchCategory(id:String,category:String,slug:String,parentID:String){
        
        search_Scenario = "Yes"
        print(CategoryidArray)
        print(subCategoryListidArray)
        self.subCat_Arrived = "No"
        Home_Data.Category_id = id
        searchCat_Title = category
        var check_Status:Int = Int()
        
        
        catBtn.setTitle("\(category)", forState: .Normal)
        if parentID == ""{
            status = 1
            check_Status = 1
            let getCat_ID = CategoryidArray.indexOfObject(id)
            let indexPath = NSIndexPath(forRow: getCat_ID, inSection: 0)
            mainCat_ID = id
            
            changeSelection(indexPath)
            Category_feed()
            
        }
            
        else{
            status = 2
            check_Status = 2
            let getCat_ID = CategoryidArray.indexOfObject(parentID)
            let indexPath = NSIndexPath(forRow: getCat_ID, inSection: 0)
            subCat_ID = id
            mainCat_ID = parentID
            changeSelection(indexPath)
            subCat_Arrived = "Yes"
            Category_feed()
        }
        viewMap.clear()
        mapProviders(check_Status)
    }
    
    func subCat_Selection(index:NSIndexPath){
        subCatSel_Index = index.row
        SubCategoryListStatusArray.removeAllObjects()
        for j in 0 ..< subCategoryListidArray.count
        {
            self.SubCategoryListStatusArray.addObject("0")
        }
        self.SubCategoryListStatusArray.replaceObjectAtIndex(index.row, withObject: "1")
        subCategoryCollectionView.reloadData()
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            
            let indexPath = NSIndexPath(forRow: self.subCatSel_Index, inSection: 0)
            self.subCategoryCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        }
    }
    
    func changeSelection(index:NSIndexPath){
        mainCatSel_Index = index.row
        var subCatSel_Index:Int = Int()
        search_Scenario = "Yes"
        print(index.row)
        ISSelectedArray.removeAllObjects()
        for i in 0 ..< CategoryidArray.count
        {
            self.ISSelectedArray.addObject("0")
        }
        self.ISSelectedArray.replaceObjectAtIndex(index.row, withObject: "1")
        categoryCollectionView.reloadData()
        
        
        
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            
            let indexPath = NSIndexPath(forRow: self.mainCatSel_Index, inSection: 0)
            self.categoryCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        }
        
    }
    
    func Blink(){
        btnLogin.alpha = 0.0
        UIButton.animateWithDuration(1, animations: {
            self.btnLogin.alpha = 1.0
            }, completion: {
                (value: Bool) in
                self.Blink()
        })
    }
    func set_mapView(lat:NSString,long:NSString){
        
        let UpdateLoc = CLLocationCoordinate2DMake(CLLocationDegrees(lat as String)!,CLLocationDegrees(long as String)!)
        let camera = GMSCameraPosition.cameraWithTarget(UpdateLoc, zoom: 15)
        viewMap.animateToCameraPosition(camera)
        viewMap.myLocationEnabled = true
        
        
        
        delay(0.5) { () -> () in
            let zoomOut = GMSCameraUpdate.zoomTo(13)
            self.viewMap.animateWithCameraUpdate(zoomOut)
            let Camera = GMSCameraUpdate.setTarget(UpdateLoc)
            self.viewMap.animateWithCameraUpdate(Camera)
            self.viewMap.myLocationEnabled = true
            self.delay(0.5, completion: { () -> () in
                let zoomIn = GMSCameraUpdate.zoomTo(13)
                self.viewMap.animateWithCameraUpdate(zoomIn)
            })
        }
        viewMap.settings.setAllGesturesEnabled(true)
        viewMap.settings.scrollGestures=true
    }
    
    
    func plotProviders(providers:NSArray,taskid:String){
        // var bounds = GMSCoordinateBounds()
        
        for item in providers{
            let dic = item as! NSDictionary
            let latitude:NSString = "\(dic.valueForKey("lat")!)"
            let longitude:NSString = "\(dic.valueForKey("lng")!)"
            let camera = GMSCameraPosition.cameraWithLatitude(latitude.doubleValue,
                                                              longitude: longitude.doubleValue, zoom:15)
            let userData = createProvidersData(dic,taskid:taskid )
            let marker = GMSMarker()
            marker.position = camera.target
            marker.appearAnimation = kGMSMarkerAnimationPop
            
            var Combineimage : UIImage = UIImage()
            
            
            
            
            SDWebImageManager.sharedManager().downloadImageWithURL(NSURL.init(string: userData.service_icon)!, options: [],progress: nil, completed: {[weak self] (image, error, cached, finished, url) in
                
                if let wSelf = self {
                    
                    //On Main Thread
                       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {() -> Void in
                      // let data0 = NSData(contentsOfURL: NSURL.init(string: userData.service_icon)!)
                   // if data0 != nil{
                  //  if let image = UIImage(data: data0!){
                          if image != nil{
                    dispatch_async(dispatch_get_main_queue()) {
                        if userData.badge.count > 0{
                            
                            //commentedj18
                          let getimage: UIImage = self!.imageByCombiningImage(UIImage(named: "marker-1")!, withImage:image)
                            Combineimage = self!.DiffimageByCombiningImage(getimage, withImage:UIImage(named:"diffmarker")!)
                            
                            let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 70))
                            let pinImageView = UIImageView.init()
                            pinImageView.frame = CGRectMake(0, 0, 70, 70)
                            pinImageView.image = Combineimage
                            
                            //pinImageView.sd_setImageWithURL(NSURL.init(string: userData.service_icon)!, placeholderImage: UIImage.init(named: "PlaceHolderSmall"))
                            //pinImageView.backgroundColor = UIColor.blackColor()
                            let label = UILabel()
                            label.frame = CGRectMake(58, -10, 90, 75)
                            label.text = userData.GetmainCategory
                            
                            label.font = plumberlableFont
                            label.textColor = PlumberThemeColor
                            // label.sizeToFit()
                            label.numberOfLines = 3
                            // label.adjustsFontSizeToFitWidth = true;
                            view.addSubview(pinImageView)
                            view.addSubview(label)
                            //i.e. customize view to get what you need
                            let markerIcon: UIImage? = self!.image(from: view)
                            marker.icon = markerIcon
                            //   marker.icon = self.DiffimageByCombiningImage(getimage, withImage:UIImage(named:"diffmarker")!)
                        }
                        else{
                            
                            //commentedj18
                            Combineimage = self!.imageByCombiningImage(UIImage(named: "marker-1")!, withImage:image)
                            
                            
                            let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 70))
                            let pinImageView = UIImageView.init()
                            pinImageView.frame = CGRectMake(0, 0, 70, 70)
                            pinImageView.image = Combineimage
                            //  pinImageView.sd_setImageWithURL(NSURL.init(string: userData.service_icon)!, placeholderImage: UIImage.init(named: "PlaceHolderSmall"))
                            //pinImageView.backgroundColor = UIColor.blackColor()
                            let label = UILabel()
                            label.frame = CGRectMake(58, -10, 90, 75)
                            label.text = userData.GetmainCategory
                            label.font = plumberlableFont
                            label.textColor = PlumberThemeColor
                            //label.sizeToFit()
                            //label.adjustsFontSizeToFitWidth = true;
                            label.numberOfLines = 3
                            view.addSubview(pinImageView)
                            view.addSubview(label)
                            //i.e. customize view to get what you need
                            let markerIcon: UIImage? = self!.image(from: view)
                            marker.icon = markerIcon
                            //   marker.icon = self.DiffimageByCombiningImage(getimage, withImage:UIImage(named:"diffmarker")!)
                            
                        }
                        
                    }
                        }
                   //  }
                  //   }
                     })
                }
                })
            
            
            
            
            
            
            marker.userData = userData
            marker.infoWindowAnchor = CGPointMake(0.5, 0.2)
            marker.map = viewMap
            //  bounds = bounds.includingCoordinate(marker.position)
            
        }
        //let update = GMSCameraUpdate.fitBounds(bounds, withPadding: 100)
        //viewMap.animateWithCameraUpdate(update)
        
    }
    
    func image(from view: UIView) -> UIImage {
        
        
        if (UIScreen.mainScreen().respondsToSelector(#selector(NSDecimalNumberBehaviors.scale)))
        {
            UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen().scale)
        }
        else {
            UIGraphicsBeginImageContext(view.frame.size)
        }
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func createProvidersData(providers:NSDictionary,taskid:String)->ProviderMapDetails{
        
        let providerObj = ProviderMapDetails()
        providerObj.tasker_id = providers.objectForKey("taskerid") as! String
        providerObj.Name = providers.objectForKey("name") as! String
        providerObj.service_icon = providers.objectForKey("image_url") as! String
        providerObj.available = providers.objectForKey("availability") as! String
        providerObj.company = providers.objectForKey("company") as! String
        providerObj.rating = self.themes.CheckNullValue(providers.objectForKey("rating"))!
        providerObj.min_amount = "\(providers.objectForKey("min_amount")!)"
        providerObj.hour_amount = "\(providers.objectForKey("hourly_amount")!)"
        providerObj.cate_type = "\(providers.objectForKey("category_type")!)"
        providerObj.GetmainCategory = self.themes.CheckNullValue(providers.objectForKey("main_category"))!
        
        providerObj.flat_amount = "\(providers.objectForKey("flat_amount")!)"
        providerObj.lat = "\(providers.objectForKey("lat")!)"
        providerObj.lng = "\(providers.objectForKey("lng")!)"
        providerObj.workLoc = "\(providers.objectForKey("worklocation")!)"
        providerObj.servicetax = "\(themes.CheckNullValue(providers.objectForKey("servixe_tax"))!)"
        serviceTax = providerObj.servicetax
        providerObj.badge = providers.objectForKey("badges")as! NSMutableArray
        
        providerObj.taskid = taskid
        let obj = providerObj
        
        return obj
    }
    
    
    
    func delay(seconds: Double, completion:()->()) {
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64( Double(NSEC_PER_SEC) * seconds ))
        dispatch_after(popTime, dispatch_get_main_queue()) {
            completion()
        }
    }
    
    func getLocationId(notify:NSNotification){
        let sampleArray = notify.object as! NSArray
        self.textLocation.text = sampleArray.objectAtIndex(2) as? String
        self.textLocation.endEditing(true)
        
        currentLatitide = "\(sampleArray.objectAtIndex(0))"
        currentLongitude = "\(sampleArray.objectAtIndex(1))"
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        self.set_mapView("\(sampleArray.objectAtIndex(0))", long: "\(sampleArray.objectAtIndex(1))")
        //        }
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        self.mapProviders(self.status)
        //        }
    }
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        let book:BookingView = BookingView(nibName:"BookingView", bundle: nil)
        book.delegate = self
        book.taskernamestr = self.nibView1.lblName.text!
        book.servicetax = serviceTax
        self.presentpopupViewController(book, animationType: animationType, completion: { () -> Void in
        })
    }
    func pressedCancel(sender: BookingView) {
        
        
        self.dismissPopupViewController(.BottomBottom)
    }
    //
    //    func markerview() {
    //        let param:Dictionary=["user_id":"\(self.themes.getUserID())","category":"\(Addaddress_Data.Category)","long":"\(Addaddress_Data.Longitude)"]
    //        urlHandler.makeCall(constant.Badge, param: param) { (responseObject, error) -> () in
    //
    //            dispatch_async(dispatch_get_main_queue(),{
    //
    //                if(error != nil){
    //                    //                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
    //                }  else {
    //                    if(responseObject != nil) {
    //                        let dict:NSDictionary=responseObject!
    //                        let Status:NSString?=self.themes.CheckNullValue(dict.objectForKey("status"))!
    //                        if(Status != nil){
    //                            if(Status! == "1")  {
    //                                let badgeArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("badges") as! NSArray
    //                                for Dictionary in badgeArray{
    //                                    let image:NSString=Dictionary.objectForKey("image") as! NSString
    //                                    self.BadgeArray.addObject(image)
    //                                    let name:NSString=Dictionary.objectForKey("name") as! NSString
    //                                    self.nameArray.addObject(name)
    //
    //                                }
    //
    //                            }
    //                        }
    //                    }
    //                }
    //            })
    //        }
    //
    //    }
    //
    
    func pressBooking(confimDate: NSString, Confirmtime: NSString, Instructionstr: NSString) {
        
        self.showProgress()
        if Instructionstr == themes.setLang("enter_instruc") || Instructionstr == ""{
            
            self.view.makeToast(message: "Kindly Enter Your Instructions", duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            
        }else{
            var markerData = selectedMarker.userData as! ProviderMapDetails
            
            let addr = getAddressForLatLng(currentLatitide, longitude: currentLongitude)
            let tempAddArray = fullAddress.componentsSeparatedByString("$")
            for item in tempAddArray{
            }
            
            let  param = ["user_id":"\(themes.getUserID())",
                          "street":"\(tempAddArray[0])",
                          "city":"\(tempAddArray[2])",
                          "state":"\(tempAddArray[3])",
                          "country":"\(tempAddArray[4])",
                          "zipcode":"\(tempAddArray[5])",
                          "lng":"\(currentLongitude)",
                          "lat":"\(currentLatitide)",
                          "locality":"\(addr)",
                          "taskerid":"\(markerData.tasker_id)",
                          "taskid":"\(markerData.taskid)",
                          "instruction" :"\(Instructionstr)",
                          "pickup_date":"\(confimDate)",
                          "pickup_time":"\(Confirmtime)"]
            
            urlHandler.makeCall(constant.MapOrder_confirm, param: param) { (responseObject, error) -> () in
                self.DismissProgress()
                if(error != nil){
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                } else{
                    
                    
                    if(responseObject != nil){
                        let Dict:NSDictionary=responseObject!
                        let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                        if(Status == "1"){
                            let response:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                            let jobID:NSString=response.objectForKey("job_id") as! NSString
                            Schedule_Data.JobID="\(jobID)"
                            Schedule_Data.orderDate = response.objectForKey("booking_date") as! NSString
                            Schedule_Data.service = response.objectForKey("service_type") as! NSString
                            Schedule_Data.jobDescription = response.objectForKey("description") as! NSString
                            Schedule_Data.service_tax = self.themes.CheckNullValue(response.objectForKey("service_tax"))!
                            
                            let providerid : NSString =  markerData.tasker_id
                            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmPageVCID") as! OrderConfirmationViewController
                            self.navigationController?.pushViewController(secondViewController, animated: true)
                            
                        }
                        else {
                            let Response = self.themes.CheckNullValue(Dict.objectForKey("response"))!
                            self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: "OK")
                        }
                    }
                    else {
                        self.themes.AlertView("\(Appname)", Message: "No Reasons available", ButtonTitle: "OK")
                    }
                }
            }
        }
        
    }
    
    
    
    func getAddressForLatLng(latitude: String, longitude: String)->String {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)")
        let data = NSData(contentsOfURL: url!)
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address = result[0]["address_components"] as? NSArray {
                        print("get current location \(result[0]["address_components"])")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in address{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                        }
                        fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)"
                        if let address = result[0]["formatted_address"] as? String{
                            return address
                        }else{
                            return fullAddress
                        }
                        
                    }
                }
            }
        }
        return ""
    }
    
    func Home_Datafeed() {
        let param:Dictionary=["location_id":"\(themes.getLocationID())"]
        urlHandler.makeCall(constant.Get_Categories, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.refreshControl.endRefreshing()
            dispatch_async(dispatch_get_main_queue(),{
                
                if(error != nil){
                    //self.settablebackground()
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }  else {
                    if(responseObject != nil) {
                        let dict:NSDictionary=responseObject!
                        let Status:NSString?=self.themes.CheckNullValue(dict.objectForKey("status"))!
                        if(Status != nil){
                            if(Status! == "1")  {
                                self.status = 0
                                self.mapProviders(self.status)
                                if(self.CategoryidArray.count != 0) {
                                    self.CategoryidArray.removeAllObjects()
                                    self.CategoryimageArray.removeAllObjects()
                                    self.CategoryInactiveImagArray.removeAllObjects()
                                    self.CategorynameArray.removeAllObjects()
                                    self.Child_StatusArray.removeAllObjects()
                                    self.ISSelectedArray.removeAllObjects()
                                }
                                let CategoryArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("category") as! NSArray
                                for Dictionary in CategoryArray{
                                    let categoryid1:NSString=Dictionary.objectForKey("cat_id") as! NSString
                                    self.CategoryidArray.addObject(categoryid1)
                                    let categoryimage1:NSString=Dictionary.objectForKey("inactive_icon") as! NSString
                                    self.CategoryimageArray.addObject(categoryimage1)
                                    let categoryinactiveimage:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("inactive_icon"))!
                                    self.CategoryInactiveImagArray.addObject(categoryinactiveimage)
                                    let categoryname1:NSString=Dictionary.objectForKey("cat_name") as! NSString
                                    self.CategorynameArray.addObject(categoryname1)
                                    let childstatus1:NSString=Dictionary.objectForKey("hasChild") as! NSString
                                    self.Child_StatusArray.addObject(childstatus1)
                                    
                                    let isselected : String = "0"
                                    
                                    self.ISSelectedArray.addObject(isselected)
                                }
                                self.categoryCollectionView.hidden=false
                                self.categoryview.hidden = false
                                self.categoryCollectionView.delegate = self
                                self.categoryCollectionView.dataSource = self
                                self.categoryCollectionView.reloadData()
                                //                            self.Home_tableView.backgroundView=nil
                            } else {
                                //                            self.settablebackground()
                                if (responseObject?.objectForKey("response") != nil) {
                                    let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                    self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                                }
                            }
                        }else {
                            // self.settablebackground()
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        }
                    }else {
                        //self.settablebackground()
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
            })
        }
    }
    
    func Category_feed() {
        var param :NSDictionary = NSDictionary()
        if search_Scenario == "Yes"{
            param=["category":"\(mainCat_ID)", "location_id":"\(themes.getLocationID())"]
        }
        else{
            param=["category":"\(Home_Data.Category_id)", "location_id":"\(themes.getLocationID())"]
        }
        
        
        
        urlHandler.makeCall("\(constant.Get_SubCategories)", param: param) { (responseObject, error) -> () in
            self.refreshControl.endRefreshing()
            self.DismissProgress()
            
            if(error != nil) {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
            else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    if(Status == "1") {
                        
                        self.subCategoryCollectionView.hidden = false
                        
                        if(self.subCategoryListidArray.count != 0){
                            self.SubCategoryListStatusArray.removeAllObjects()
                            self.subCategoryListidArray.removeAllObjects()
                            self.subCategoryListImageArray.removeAllObjects()
                            self.subCategoryListActiveImageArray.removeAllObjects()
                            self.subCategoryListInactiveImageArray.removeAllObjects()
                            
                            self.subCategoryImageArray.removeAllObjects()
                            self.subCategoryListArray.removeAllObjects()
                        }
                        let CategoryArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("category") as! NSArray
                        for Dictionary in CategoryArray{
                            let categoryid:NSString=Dictionary.objectForKey("cat_id") as! NSString
                            self.subCategoryListidArray.addObject(categoryid)
                            let categoryimage:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("icon"))!
                            self.subCategoryListImageArray.addObject(categoryimage)
                            let categoryname:NSString=Dictionary.objectForKey("cat_name") as! NSString
                            self.subCategoryListArray.addObject(categoryname)
                            _=Dictionary.objectForKey("hasChild") as! NSString
                            let categoryActiveImage:NSString=Dictionary.objectForKey("inactive_icon") as! NSString
                            self.subCategoryListActiveImageArray.addObject(categoryActiveImage)
                            let categoryInactivename:NSString=Dictionary.objectForKey("inactive_icon") as! NSString
                            self.subCategoryListInactiveImageArray.addObject(categoryInactivename)
                            
                            self.SubCategoryListStatusArray.addObject("0")
                            
                        }
                        self.subCategoryCollectionView.delegate = self
                        self.subCategoryCollectionView.dataSource = self
                        self.subCategoryCollectionView.reloadData()
                        if self.subCat_Arrived == "Yes"{
                            let getSubCat_ID = self.subCategoryListidArray.indexOfObject(self.subCat_ID)
                            let indexPathSub_Cat = NSIndexPath(forRow: getSubCat_ID, inSection: 0)
                            self.subCat_Selection(indexPathSub_Cat)
                        }
                    }
                    else {
                        self.subCategoryCollectionView.hidden = true
                        // self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_category"), ButtonTitle: kOk)
                    }
                }
                else{
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    //self.settablebackground()
                }
            }
        }
    }
    
    func mapProviders(status:Int){
        viewService.hidden = true
        self.showProgress()
        
        var param = NSDictionary()
        if (status == 0){
            param=["user_id":"\(themes.getUserID())","lat":"\(currentLatitide)","long":"\(currentLongitude)"]
        }else if (status == 1){
            if  search_Scenario == "yes" ||  subCat_Arrived  == "No"{
                
                
            }
            else{
                param=["user_id":"\(themes.getUserID())","lat":"\(currentLatitide)","long":"\(currentLongitude)","maincategory":"\(Home_Data.Category_id)"]
                
            }
        }else if (status == 2){
            
            param=["user_id":"\(themes.getUserID())","lat":"\(currentLatitide)","long":"\(currentLongitude)","category":"\(Home_Data.Category_id)"]
        }
        urlHandler.makeCall(constant.Map_Providers, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.refreshControl.endRefreshing()
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }  else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString?=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    if(Status != nil){
                        if(Status! == "1")  {
                            var taskId = String()
                            if(status == 2){
                                taskId=dict.objectForKey("task_id") as! NSString as String
                                
                            }else{
                                taskId = ""
                            }
                            
                            self.providerList = dict.objectForKey("response") as! NSArray
                            self.plotProviders(self.providerList,taskid: taskId)
                            
                            
                        } else {
                            self.providerList = NSArray()
                            self.viewService.hidden = false
                            if (responseObject?.objectForKey("response") != nil) {
                                if (responseObject?.objectForKey("response"))! as! String == "User ID is Required" && status == 1
                                {
                                }
                                
                                //                                let Response:NSString=responseObject?.objectForKey("response") as! NSString
                                //                                self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: kOk)
                            }
                        }
                    }else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }else {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                }
            }
        }
        
    }
    
    
    
    
    func DiffimageByCombiningImage(firstImage: UIImage, withImage secondImage: UIImage) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(firstImage.size, false, 0.0)
        var resultImage: UIImage? = nil
        // Get the graphics context
        let context = UIGraphicsGetCurrentContext()
        // Draw the first image
        firstImage.drawInRect(CGRectMake(0, 0, firstImage.size.width, firstImage.size.height))
        // Get the frame of the second image
        let rect = CGRectMake(26.5, 40.5, 20 , 20)
        // Add the path of an ellipse to the context
        // If the rect is a square the shape will be a circle
        CGContextAddEllipseInRect(context, rect)
        // Clip the context to that path
        CGContextClip(context)
        // Do the second image which will be clipped to that circle
        secondImage.drawInRect(rect)
        // Get the result
        resultImage = UIGraphicsGetImageFromCurrentImageContext()
        // End the image context
        UIGraphicsEndImageContext()
        return resultImage!
        
        
        
    }
    
    
    
    func imageByCombiningImage(firstImage: UIImage, withImage secondImage: UIImage) -> UIImage {
        
        
        UIGraphicsBeginImageContextWithOptions(firstImage.size, false, 0.0)
        var resultImage: UIImage? = nil
        // Get the graphics context
        let context = UIGraphicsGetCurrentContext()
        // Draw the first image
        firstImage.drawInRect(CGRectMake(0, 0, firstImage.size.width, firstImage.size.height))
        // Get the frame of the second image
        let rect = CGRectMake(16.5, 3.5, 37, 37)
        // Add the path of an ellipse to the context
        // If the rect is a square the shape will be a circle
        CGContextAddEllipseInRect(context, rect)
        // Clip the context to that path
        CGContextClip(context)
        // Do the second image which will be clipped to that circle
        secondImage.drawInRect(rect)
        // Get the result
        resultImage = UIGraphicsGetImageFromCurrentImageContext()
        // End the image context
        UIGraphicsEndImageContext()
        return resultImage!
        
    }
    
    
    
    //MARK: - Button Action
    
    @IBAction func didClickMenu(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
        
    }
    
    @IBAction func didClickLocation(sender: AnyObject) {
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func didClickLogin(sender: AnyObject) {
        Appdel.Make_RootVc("DLDemoRootViewController", RootStr: "signinVCID")
    }
    
    
    @IBAction func didClickbookNow(sender: AnyObject) {
        
        
        
        if(self.status == 2) && providerChosen == true{
            
            
            self.displayViewController(.BottomBottom)
            
            //            let AlertView:UIAlertView=UIAlertView()
            //            AlertView.delegate=self
            //            AlertView.title="Are you sure you want to book?"
            //            AlertView.addButtonWithTitle("Confirm")
            //            AlertView.addButtonWithTitle("Cancel")
            //            AlertView.show()
            //            AlertView.tag = 0
            
        }else{
            
            if status == 0 {
                themes.AlertView("", Message: "Please Choose The Category", ButtonTitle: kOk)
                
            }else if status == 1{
                themes.AlertView("", Message: "Please Choose The sub Category", ButtonTitle: kOk)
                
            }else if status == 2{
                if self.providerList.count == 0
                {
                    themes.AlertView("", Message: "Providers are not available", ButtonTitle: kOk)
                }
                else
                {
                    themes.AlertView("", Message: "Please Choose The Provider From The Map.", ButtonTitle: kOk)
                    
                }
                
            }
        }
        
    }
    
    
    
    @IBAction func didClickBookLater(sender: AnyObject) {
        
        let Prefcategory : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        Prefcategory.setObject(Home_Data.Category_id, forKey:"maincategory")
        if status == 0{
            themes.AlertView("", Message: "Please Choose The Category", ButtonTitle: kOk)
            
        }else if status == 1{
            themes.AlertView("", Message: "Please Choose The sub Category", ButtonTitle: kOk)
            
        }else{
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ScheduleViewControllerID") as! ScheduleViewController
            secondViewController.Latitude = currentLatitide
            secondViewController.Longitude = currentLongitude
            secondViewController.CustomList = "2"
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    func didClickMarkerView(sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MYProfileVCSID") as! MyProfileViewController
        secondViewController.providerid = tempUserData.tasker_id
        secondViewController.hour_amount = tempUserData.hour_amount
        secondViewController.min_amount = tempUserData.flat_amount
        secondViewController.cat_type = tempUserData.cate_type
        secondViewController.getjob_status = "hide"
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    func didClickChat(sender: AnyObject) {
        Message_details.taskid = tempUserData.taskid
        Message_details.providerid = tempUserData.tasker_id
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    func didClickClose(sender: AnyObject) {
        
  
        
        btnMarker.hidden = false
        nibView1.removeFromSuperview()
        providerChosen = false
    }
    
    func didClickAccept(sender: AnyObject) {
        providerChosen = true
        
        self.nibView1.removeFromSuperview()
        
        themes.AlertView("", Message: "You Have Selected \(self.nibView1.lblName.text!) As Your Provider To Perform The Service.", ButtonTitle: kOk)
        
        
        self.btnMarker.hidden = false
        self.providerChosen = true
        status = 2
     
        self.nibView1.removeFromSuperview()
            }
    
    
    //MARK: - LocationManager Delegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _ = CLGeocoder()
        let current = locations[0]
        if current.coordinate.latitude != 0 {
            
            
            CLGeocoder().reverseGeocodeLocation(current, completionHandler:
                {(placemarks, error) in
                    if placemarks == nil {
                        return
                    }
                    let currentLocPlacemark = placemarks![0]
                    var code = currentLocPlacemark.ISOcountryCode
                    let dictCodes : NSDictionary = self.themes.getCountryList()
                    code = (dictCodes.valueForKey(code!)as! NSArray)[1] as? String
                    print("\(code)")
                    self.themes.saveCounrtyphone(code!)
                    self.locationManager.stopUpdatingLocation()
            })
            
            currentLatitide = "\(current.coordinate.latitude)"
            currentLongitude = "\(current.coordinate.longitude)"
            locationManager.stopUpdatingLocation()
            textLocation.text = getAddressForLatLng("\(currentLatitide)", longitude: "\(current.coordinate.longitude)")
            set_mapView(currentLatitide, long: currentLongitude)
            
        }
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        //textLocation.text = getAddressForLatLng("\(mapView.camera.target.latitude)", longitude: "\(mapView.camera.target.longitude)")
    }
    
    //    func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView! {
    //        let userData = marker.userData as! ProviderMapDetails
    //        let nibView = NSBundle.mainBundle().loadNibNamed("MarkerView", owner: self, options: nil)[0] as! MarkerView
    //
    //        nibView.userImage.sd_setImageWithURL(NSURL.init(string: userData.service_icon), placeholderImage: UIImage(named: "PlaceHolderSmall"))
    //        nibView.minCOst.text = "Min Cost : \(userData.min_amount)"
    //        nibView.lblAdd.text = ""
    //        tempUserData = userData
    //        nibView.btnChat.addTarget(self, action: #selector(InitialViewController.didClickChat(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    //
    //        nibView.btnViewDetails.addTarget(self, action: #selector(InitialViewController.didClickMarkerView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    //        nibView.roundOffBorder()
    //        return nibView
    //
    //    }
    
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        
        if status == 2{
            btnMarker.hidden = true
            
            selectedMarker = marker
            let temp = marker.userData as! ProviderMapDetails
            let markerAddress = getAddressForLatLng(temp.lat, longitude: temp.lng)
            let userData = marker.userData as! ProviderMapDetails
            let doubleLat = Double.init(userData.lat)
            let doubleLng = Double.init(userData.lng)
            
            _ = CLLocationCoordinate2D(latitude:doubleLat! , longitude:doubleLng!)
            self.nibView1.userImage.sd_setImageWithURL(NSURL.init(string: userData.service_icon), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            self.nibView1.service_tax.text = "\(userData.servicetax)% Service Fee Is Added To Every Job."
            let strRating = "\(userData.rating)"
            let n = NSNumberFormatter().numberFromString(strRating)
            
            if temp.cate_type == "Flat Rate"{
                self.nibView1.minCOst.text = "Flat Rate : \(self.themes.getCurrencyCode())\(userData.flat_amount)"
                
                
            }else if temp.cate_type == "Hourly Rate"{
                self.nibView1.minCOst.text = "Per Hour : \(self.themes.getCurrencyCode())\(userData.hour_amount)"
                
            }
            
            self.nibView1.providerRating.emptySelectedImage = UIImage(named: "Star")
            self.nibView1.providerRating.fullSelectedImage = UIImage(named: "StarSelected")
            self.nibView1.providerRating.contentMode = UIViewContentMode.ScaleAspectFill
            self.nibView1.providerRating.maxRating = 5
            self.nibView1.providerRating.minRating = 1
            self.nibView1.providerRating.halfRatings = false
            self.nibView1.providerRating.floatRatings = false
            self.nibView1.providerRating.rating = CGFloat(n!)
            self.nibView1.providerRating.editable = false;
            self.nibView1.providerRating.halfRatings = true;
            self.nibView1.providerRating.floatRatings = false;
            self.tempUserData = userData
            self.nibView1.lblName.text = temp.Name
            self.nibView1.lblAdd.numberOfLines = 4
            self.nibView1.lblAdd.text = temp.workLoc
            self.nibView1.btnChat.addTarget(self, action: #selector(InitialViewController.didClickChat(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.nibView1.center = CGPointMake(self.viewMap.frame.size.width  / 2,
                                               self.viewMap.frame.size.height / 2-35);
            self.nibView1.btnViewDetails.addTarget(self, action: #selector(InitialViewController.didClickMarkerView(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.nibView1.btnClose.addTarget(self, action: #selector(InitialViewController.didClickClose(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.nibView1.btnAccept.hidden = false
            self.nibView1.btnAccept.addTarget(self, action: #selector(InitialViewController.didClickAccept(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            BadgeArray = userData.badge
            
            for Dictionary in BadgeArray{
                let image:NSString=Dictionary.objectForKey("image") as! NSString
                self.imgArray.addObject(image)
                let name:NSString=Dictionary.objectForKey("name") as! NSString
                self.labelArray.addObject(name)
                
                
            }
            if  BadgeArray.count  == 0
            {
                self.nibView1.collectionView_heightCons.constant = 0
                self.nibView1.ratingview_topCons.constant = 0
                
                self.nibView1.frame.size.height = 420 - 77
            }
            else
            {
                self.nibView1.collectionView_heightCons.constant = 77
                self.nibView1.ratingview_topCons.constant = 2
                
                self.nibView1.frame.size.height = 420
            }
            
            let nibName = UINib(nibName: "BadgeCell", bundle:nil)
            nibView1.cv1.registerNib(nibName, forCellWithReuseIdentifier: "badgecell")
            self.nibView1.cv1.delegate = self
            self.nibView1.cv1.dataSource = self
            self.nibView1.cv1.reloadData()
            
            
            self.view.addSubview(self.nibView1)
            
            //                self.view.bringSubviewToFront(self.nibView1)
            
            
        }
        else if status == 1
            
        {
            themes.AlertView("", Message: "Please Choose The sub Category", ButtonTitle: kOk)
            
        }
        else{
            
            
            themes.AlertView("", Message: "Please Choose The Category", ButtonTitle: kOk)
            
        }
        
        return false
    }
    
    
    
    func mapView(mapView: GMSMapView!, didTapOverlay overlay: GMSOverlay!) {
        
    }
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        //        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MYProfileVCSID") as! MyProfileViewController
        //        secondViewController.providerid = tempUserData.job_id
        //        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    
    
    
    //MARK: - textField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.nibView1.removeFromSuperview()
        
        if currentLatitide != nil{
            let navig = self.storyboard?.instantiateViewControllerWithIdentifier("NormalViewController") as! ChooseLocationViewController
            navig.lat = currentLatitide
            navig.long = currentLongitude
            textField.endEditing(true)
            self.navigationController?.pushViewController(navig, animated: true)
            
        }
        
    }
    
    
    //MARK: - AlertViewDelegate
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        
        
        if(View.tag == 0){
            
            switch buttonIndex{
                
            case 0:
                
                break;
            default:
                break;
                //Some code here..
                
            }
        }
    }
    
    
    //MARK: - Collection View Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == categoryCollectionView{
            return CategoryimageArray.count
        }else if collectionView == subCategoryCollectionView{
            return subCategoryListidArray.count
        }
        else if collectionView == nibView1.cv1{
            return BadgeArray.count
        }
        
        return 0
    }
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        if collectionView == categoryCollectionView{
            search_Scenario = "None"
            subCat_Arrived = "No"
            
            if themes.getUserID() == ""
            {
                themes.AlertView("Sorry", Message: "Please Login to continue.", ButtonTitle: kOk)
                
            }
                
            else
                
            {
                let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell
                
                if(Child_StatusArray.objectAtIndex(indexPath.row) as! String == "No"){
                    Home_Data.Category_id = CategoryidArray.objectAtIndex(indexPath.row) as! NSString
                    Home_Data.Category_name = CategorynameArray.objectAtIndex(indexPath.row)as! NSString
                    // Home_Data.Category_image = CategoryimageArray.objectAtIndex(indexPath.row)as! NSString
                    Category_Data.CategoryID = CategoryidArray.objectAtIndex(indexPath.row) as! NSString
                    
                    viewMap.clear()
                    self.status = 1
                    self.ISSelectedArray.removeAllObjects()
                    for _ in 0 ..< CategorynameArray.count
                    {
                        self.ISSelectedArray.addObject("0")
                    }
                    self.ISSelectedArray.replaceObjectAtIndex(indexPath.row, withObject: "1")
                    self.categoryCollectionView.reloadData()
                    self.showProgress()
                    self.mapProviders(self.status)
                    Category_feed()
                }else{
                    subCategoryCollectionView.hidden = true
                }
            }
        }
        else if collectionView == subCategoryCollectionView{
            if themes.getUserID() == ""
            {
                themes.AlertView("Sorry", Message: "Please Login to continue.", ButtonTitle: kOk)
                
            }
            else
                
            {
                
                let cell = collectionView.cellForItemAtIndexPath(indexPath) as! SubCategoryCollectionViewCell
                //cell.subCategoryView.backgroundColor = UIColor.orangeColor()
                Home_Data.Category_id = subCategoryListidArray.objectAtIndex(indexPath.row) as! NSString
                Home_Data.subCategory_name = subCategoryListArray.objectAtIndex(indexPath.row) as! NSString
                //cell.lblSubCategoryTitle.textColor = PlumberThemeColor
                
                //                cell.backgroundColor = UIColor(red: CGFloat(255 / 255.0), green: CGFloat(255 / 255.0), blue: CGFloat(153 / 255.0), alpha: CGFloat(1))
                
                
                self.SubCategoryListStatusArray.removeAllObjects()
                for _ in 0 ..< subCategoryListidArray.count
                {
                    self.SubCategoryListStatusArray.addObject("0")
                }
                self.SubCategoryListStatusArray.replaceObjectAtIndex(indexPath.row, withObject: "1")
                
                self.subCategoryCollectionView.reloadData()
                
                viewMap.clear()
                self.status = 2
                self.mapProviders(self.status)
                
            }
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell1:UICollectionViewCell = UICollectionViewCell()
        if(collectionView == categoryCollectionView){
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CategoryCollectionViewCell
            //            cell.imgCategoryImage.layer.borderWidth = 0
            //            cell.imgCategoryImage.layer.cornerRadius = cell.imgCategoryImage.frame.width/2
            //            cell.imgCategoryImage.clipsToBounds = true
            
            cell.gradient.frame = cell.imgCategoryImage.bounds
            
            cell.gradient.colors = [UIColor.clearColor().CGColor, UIColor.blackColor().CGColor]
            
            cell.gradient.locations = [0.0, 1.5]
            cell.imgCategoryImage.layer.insertSublayer(cell.gradient, atIndex: 0)
            
            
            if ISSelectedArray.objectAtIndex(indexPath.row) as! String == "0"
            {
                cell.imgCategoryImage.layer.masksToBounds = false
                cell.imgCategoryImage.layer.shadowColor = UIColor.clearColor().CGColor
                cell.imgCategoryImage.layer.shadowOffset = CGSize(width: 0,height: 0.5)
                cell.imgCategoryImage.layer.shadowOpacity = 0.5
                cell.lblCategoryTitle.textColor = UIColor.whiteColor()
                cell.imgCategoryImage.sd_setImageWithURL(NSURL.init(string:self.CategoryInactiveImagArray.objectAtIndex(indexPath.row) as! String), completed: themes.block)
                
            }
            else{
                cell.imgCategoryImage.layer.masksToBounds = false
                cell.imgCategoryImage.layer.shadowColor = UIColor.blackColor().CGColor
                cell.imgCategoryImage.layer.shadowOffset = CGSize(width: 0,height: 0.5)
                cell.imgCategoryImage.layer.shadowOpacity = 0.5
                cell.lblCategoryTitle.textColor = PlumberThemeColor
                cell.imgCategoryImage.sd_setImageWithURL(NSURL.init(string:self.CategoryimageArray.objectAtIndex(indexPath.row) as! String), completed: themes.block)
                
            }
            
            cell.lblCategoryTitle.text = CategorynameArray.objectAtIndex(indexPath.row) as? String
            // cell.lblDuration.text = "1 min"
            cell1 = cell
            
            return cell
        }
        else if collectionView == subCategoryCollectionView{
            
            
            let subCell = collectionView.dequeueReusableCellWithReuseIdentifier("SubCategoryCell", forIndexPath: indexPath) as! SubCategoryCollectionViewCell
            subCell.subCategoryView.backgroundColor = UIColor.clearColor()
            
            subCell.gradient.frame = subCell.imgSubCategoryImage.bounds
            
            subCell.gradient.colors = [UIColor.clearColor().CGColor, UIColor.blackColor().CGColor]
            
            subCell.gradient.locations = [0.0, 1.5]
            subCell.imgSubCategoryImage.layer.insertSublayer(subCell.gradient, atIndex: 0)
            
            // subCell.imgSubCategoryImage.sd_setImageWithURL(NSURL.init(string: subCategoryListImageArray.objectAtIndex(indexPath.row) as! String), placeholderImage: UIImage.init(named:"PlaceHolderSmall"))
            subCell.lblSubCategoryTitle.text = subCategoryListArray.objectAtIndex(indexPath.row) as? String
            if SubCategoryListStatusArray.objectAtIndex(indexPath.row) as! String == "0"
            {
                subCell.imgSubCategoryImage.layer.masksToBounds = false
                subCell.imgSubCategoryImage.layer.shadowColor = UIColor.clearColor().CGColor
                subCell.imgSubCategoryImage.layer.shadowOffset = CGSize(width: 0,height: 0.5)
                subCell.imgSubCategoryImage.layer.shadowOpacity = 0.5
                
                subCell.imgSubCategoryImage.sd_setImageWithURL(NSURL.init(string: self.subCategoryListActiveImageArray.objectAtIndex(indexPath.row) as! String), completed: themes.block)
                subCell.lblSubCategoryTitle.textColor = PlumberThemeColor
                
            }
            else{
                subCell.imgSubCategoryImage.layer.masksToBounds = false
                subCell.imgSubCategoryImage.layer.shadowColor = UIColor.blackColor().CGColor
                subCell.imgSubCategoryImage.layer.shadowOffset = CGSize(width: 0,height: 0.5)
                subCell.imgSubCategoryImage.layer.shadowOpacity = 0.5
                
                
                subCell.imgSubCategoryImage.sd_setImageWithURL(NSURL.init(string: self.subCategoryListInactiveImageArray.objectAtIndex(indexPath.row) as! String), completed: themes.block)
                subCell.lblSubCategoryTitle.textColor = UIColor.whiteColor()
                
            }
            
            //            subCell.imgSubCategoryImage.layer.cornerRadius = subCell.imgSubCategoryImage.frame.width/2
            //            subCell.imgSubCategoryImage.clipsToBounds = true
            //            subCell.subCategoryView.layer.cornerRadius = 5
            cell1 = subCell
            return subCell
        }
        else if collectionView == nibView1.cv1{
            
            let Cell = collectionView.dequeueReusableCellWithReuseIdentifier("badgecell", forIndexPath: indexPath) as! BadgeCell
            Cell.lablebadge.setTitle(labelArray[indexPath.row] as?String, forState: .Normal)
            Cell.lablebadge.tag = indexPath.row
            Cell.lablebadge.addTarget(self, action: #selector(InitialViewController.ShowAlert(_:)), forControlEvents: .TouchUpInside)
            
            
            Cell.imgbadge.sd_setImageWithURL(NSURL.init(string: self.imgArray.objectAtIndex(indexPath.row) as! String), completed: themes.block)
            Cell.imgbadge.tag = indexPath.row
            
            Cell.imgbadge.userInteractionEnabled = true
            let tap :UITapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(InitialViewController.handleimgTap(_:)))
            Cell.imgbadge.addGestureRecognizer(tap)
            
            return Cell
            
        }
        
        
        return cell1
    }
    
    func  handleimgTap(recognizer:UITapGestureRecognizer)
    {
        // NSString *uid = testArray[recognizer.view.tag];
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title="Badge Description"
        AlertView.message = labelArray.objectAtIndex(recognizer.view!.tag) as? String
        AlertView.addButtonWithTitle("OK")
        AlertView.show()
        
        
    }
    
    
    func ShowAlert (sender:UIButton)  {
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title="Badge Description"
        AlertView.message = labelArray.objectAtIndex(sender.tag) as? String
        AlertView.addButtonWithTitle("OK")
        AlertView.show()
        
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        
        if collectionView == nibView1.cv1
        {
            if self.BadgeArray.count > 3
            {
                
                return UIEdgeInsetsMake(5, 5, 5, 5)
            }
            else{
                let viewWidth = nibView1.cv1.frame.size.width
                let totalCellWidth:CGFloat = 60 * CGFloat (BadgeArray.count)
                let totalSpacingWidth:CGFloat = 10 * (CGFloat(BadgeArray.count) - 1)
                let leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
                let rightInset = leftInset
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
                
            }
        }
        else
        {
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        
        book_now.setTitle(themes.setLang("book_now"), forState: .Normal)
        book_later_btn.setTitle(themes.setLang("book_later"), forState: .Normal)
        
        themes.Apply_layer(listviewbtn.layer)
        themes.Apply_layer(categorySearch.layer)
        
        if  catBtn.titleLabel?.text == "" ||  catBtn.titleLabel?.text == nil ||  catBtn.titleLabel?.text == "Search Category"{
            
            if searchCat_Title != ""{
                catBtn.setTitle("\(searchCat_Title)", forState: .Normal)
                catBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            }
            else{
                catBtn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
                catBtn.setTitle("Search Category", forState: .Normal)
            }
            
        }
            
        else{
            catBtn.setTitleColor( UIColor.blackColor(), forState: .Normal)
        }
        
        
    }
    
    func searchCategory(){
        
        
        if parentid == ""{
            
            if themes.getUserID() == ""
            {
                themes.AlertView("Sorry", Message: "Please Login to continue.", ButtonTitle: kOk)
                
            }
            else
                
            {
                // let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell
                var arrayindex = CategorynameArray.indexOfObject(category)
                if(parentid == ""){
                    Home_Data.Category_id = CategoryidArray.objectAtIndex(arrayindex) as! NSString
                    Home_Data.Category_name = CategorynameArray.objectAtIndex(arrayindex)as! NSString
                    //  Home_Data.Category_image = CategoryimageArray.objectAtIndex(indexPath.row)as! NSString
                    Category_Data.CategoryID = CategoryidArray.objectAtIndex(arrayindex) as! NSString
                    
                    viewMap.clear()
                    self.status = 1
                    self.ISSelectedArray.removeAllObjects()
                    for _ in 0 ..< CategorynameArray.count
                    {
                        self.ISSelectedArray.addObject("0")
                    }
                    self.ISSelectedArray.replaceObjectAtIndex(arrayindex, withObject: "1")
                    self.categoryCollectionView.reloadData()
                    self.showProgress()
                    self.mapProviders(self.status)
                    Category_feed()
                }else{
                    subCategoryCollectionView.hidden = true
                }
            }
        }
        else{
            if themes.getUserID() == ""
            {
                themes.AlertView("Sorry", Message: "Please Login to continue.", ButtonTitle: kOk)
                
            }
            else
                
            {
                var arrayindex = subCategoryListArray.indexOfObject(category)
                //let cell = collectionView.cellForItemAtIndexPath(indexPath) as! SubCategoryCollectionViewCell
                //cell.subCategoryView.backgroundColor = UIColor.orangeColor()
                Home_Data.Category_id = subCategoryListidArray.objectAtIndex(arrayindex) as! NSString
                Home_Data.subCategory_name = subCategoryListArray.objectAtIndex(arrayindex) as! NSString
                //cell.lblSubCategoryTitle.textColor = PlumberThemeColor
                
                //                cell.backgroundColor = UIColor(red: CGFloat(255 / 255.0), green: CGFloat(255 / 255.0), blue: CGFloat(153 / 255.0), alpha: CGFloat(1))
                
                
                self.SubCategoryListStatusArray.removeAllObjects()
                for _ in 0 ..< subCategoryListidArray.count
                {
                    self.SubCategoryListStatusArray.addObject("0")
                }
                self.SubCategoryListStatusArray.replaceObjectAtIndex(arrayindex, withObject: "1")
                
                self.subCategoryCollectionView.reloadData()
                
                viewMap.clear()
                self.status = 2
                self.mapProviders(self.status)
                
            }
        }
        
    }
    
}


extension UIView{
    func roundOffBorder(){
        self.layer.cornerRadius = 5
    }
}
