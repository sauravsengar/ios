//
//  ForgotPasswordViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 15/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: RootViewController {
    
    @IBOutlet weak var forg_desc: CustomLabelWhite!
    @IBOutlet var send_Btn: UIButton!
    @IBOutlet var EmailID_TextField: UITextField!
    @IBOutlet var Header_lbl: UILabel!
    @IBOutlet var Tellus: UILabel!
    @IBOutlet var ForgotPas_Lbl: UILabel!
    @IBOutlet var Close_bt: UIButton!
    
    let themes:Themes=Themes()
    var URL_handler:URLhandler=URLhandler()
    
    //MARK: -Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        send_Btn.setTitle(themes.setLang("reset_password"), forState: UIControlState.Normal)
        send_Btn.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        send_Btn.titleLabel?.numberOfLines = 2
        ForgotPas_Lbl.text=themes.setLang("forgot_password")
      forg_desc.text = themes.setLang("reset_instruct")
        EmailID_TextField.backgroundColor = UIColor.clearColor()
        

        
        EmailID_TextField.placeholder = themes.setLang("email_address")
        OTP_sta.OTP_Paging="ForgotPassword"
        //        EmailID_TextField.layer.borderColor=themes.Lightgray().CGColor
        //        EmailID_TextField.layer.borderWidth=0.8
        EmailID_TextField.delegate=self
        let paddingView : UIView = UIView.init(frame:CGRectMake(0, 0, 15, 20) )
        EmailID_TextField.leftView = paddingView
        EmailID_TextField.leftViewMode = .Always
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == EmailID_TextField) {
            EmailID_TextField.resignFirstResponder()
        }
        return true
    }
    
    //MARK: - Button Action
    
    
    @IBAction func didClickoption(sender: UIButton) {
        if(sender.tag == 10){
            self.navigationController?.popViewControllerAnimated(true)
        }else if(sender.tag == 1) {
            let Email_ID=themes.isValidEmail(EmailID_TextField.text!)
            if(EmailID_TextField.text == "") {
                themes.AlertView("\(Appname)", Message: themes.setLang("enter_email_alert"), ButtonTitle: kOk)
            }else if(Email_ID == false) {
                themes.AlertView("\(Appname)", Message: themes.setLang("valid_email_alert"), ButtonTitle: kOk)
            } else {
                self.send_Btn.enabled=false
                self.showProgress()
                let parameter=["email":"\(EmailID_TextField.text!)"]
                URL_handler.makeCall(constant.Reset_Password.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()), param: parameter, completionHandler: { (responseObject, error) -> () in
                    self.send_Btn.enabled=true
                    if(error != nil) {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    } else{
                        if(responseObject != nil){
                            let dict:NSDictionary=responseObject!
                            let response:NSString=self.themes.CheckNullValue(dict.objectForKey("response"))!
                            if(response == "Reset Code Sent Successfully!"){
                                let verification_code:NSString=dict.objectForKey("verification_code") as! NSString
                                let OTP_Status:NSString=dict.objectForKey("sms_status") as! NSString
                                let OTP_Email:NSString=dict.objectForKey("email_address") as! NSString
                                if(OTP_Status.length>0){
                                    OTP_sta.OTP_Status=OTP_Status
                                    OTP_sta.OTP=verification_code
                                    OTP_sta.OTP_EmaiID=OTP_Email
                                    self.DismissProgress()
                                    let otpview : OTPViewController = OTPViewController()
                                    otpview.otpstring = "\(self.themes.CheckNullValue(dict.objectForKey("verification_code"))!)"
                                    otpview.otpstatus_str = self.themes.CheckNullValue(dict.objectForKey("sms_status"))!
                                    otpview.otpemail = self.themes.CheckNullValue(dict.objectForKey("email_address"))!
                                    self.performSegueWithIdentifier("OTP", sender: nil)
                                }
                            }else{
                                self.themes.AlertView("\(Appname)",Message: "\(response)",ButtonTitle: kOk)
                                self.DismissProgress()
                            }
                        } else{
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                            self.DismissProgress()
                        }
                    }
                })
            }
        }
    }
}

extension ForgotPasswordViewController:UITextFieldDelegate
{
    
}
