//
//  WalletDetailViewController.swift
//  Plumbal
//
//  Created by Casperon on 06/09/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class WalletDetailViewController: RootViewController,MNMBottomPullToRefreshManagerClient,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var Walletsegment: CustomSegmentControl!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var Transactiontable: UITableView!
    
    @IBOutlet weak var titleBtn: UIButton!
    
    var themes:Themes=Themes()
    var pullToRefreshManager:MNMBottomPullToRefreshManager=MNMBottomPullToRefreshManager()
    var refreshControl:UIRefreshControl=UIRefreshControl()

     var URL_handler:URLhandler=URLhandler()
    
    var TransacTypeArray:NSMutableArray=NSMutableArray()
    var TransacAmountArray:NSMutableArray=NSMutableArray()
    var TransacTitleArray:NSMutableArray=NSMutableArray()
    var TransacDateArray:NSMutableArray=NSMutableArray()
    var TransacBalanceArray:NSMutableArray=NSMutableArray()
    var Trans_status: NSString = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleBtn.setTitle(themes.setLang("transaction"), forState: UIControlState.Normal)

        pullToRefreshManager = MNMBottomPullToRefreshManager(pullToRefreshViewHeight: 60.0, tableView: Transactiontable, withClient: self)
        
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        
        backbtn.addSubview(themes.Back_ImageView)
        Walletsegment.frame=CGRectMake(5, 92, view.frame.size.width-10, 43)
        Walletsegment.setTitle(themes.setLang("all"), forSegmentAtIndex: 0)
        Walletsegment.setTitle(themes.setLang("credit"), forSegmentAtIndex: 1)
        Walletsegment.setTitle(themes.setLang("debit"), forSegmentAtIndex: 2)
        Walletsegment.selectedSegmentIndex=0
        Walletsegment.tintColor=themes.ThemeColour()
        
        Walletsegment.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Roboto", size: 14.0)!, NSForegroundColorAttributeName: themes.DarkRed()], forState: .Normal)
        Trans_status = "all"
          let nibName = UINib(nibName: "TransTableViewCell", bundle:nil)
        self.Transactiontable.registerNib(nibName, forCellReuseIdentifier: "TransaCell")
        Transactiontable.estimatedRowHeight = 250
        Transactiontable.rowHeight = UITableViewAutomaticDimension
        Transactiontable.separatorColor=UIColor.clearColor()
        Transactiontable.dataSource = self
        Transactiontable.delegate = self
        
      
        GetTransactionDetails("\(Trans_status)", ShowProgress: true)
        configurePulltorefresh()

        
        GetTransactionDetails("\(Trans_status)", ShowProgress: true)

        // Do any additional setup after loading the view.
    }
    
    
    func configurePulltorefresh()
    {
        
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.addTarget(self, action: #selector(WalletDetailViewController.Order_dataFeed), forControlEvents: UIControlEvents.ValueChanged)
        self.Transactiontable.addSubview(refreshControl)
        
       }
    func Order_dataFeed()
    {
        GetTransactionDetails("\(self.Trans_status)", ShowProgress: false)
        
    }

    override func viewDidLayoutSubviews() {
        pullToRefreshManager.relocatePullToRefreshView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pullToRefreshManager.tableViewScrolled()
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        pullToRefreshManager.tableViewReleased()
        
    }
    func bottomPullToRefreshTriggered(manager: MNMBottomPullToRefreshManager!)
        
    {
        if (TransacTypeArray.count > 20)
        {
          
        }
        
    }


    @IBAction func didclickoption(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)

        
    }
    @IBAction func ChangeTransaction(sender: CustomSegmentControl) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;
        
        if(segmentIndex == 0)
        {
         
            Trans_status="all"
        GetTransactionDetails("\(Trans_status)", ShowProgress: true)
            
        }
        if(segmentIndex == 1)
        {
            
            Trans_status="credit"
            GetTransactionDetails("\(Trans_status)", ShowProgress: true)
            
            
        }
        if(segmentIndex == 2)
        {
            
            Trans_status="debit"
            GetTransactionDetails("\(Trans_status)", ShowProgress: true)

            
            
        }

        
    }
  
    func ReloadData(sender: OrderDetailViewController) {
       // self.GetOrderDetails(PageStatus, Page_Count: PageCount,ShowProgress: true)
        
    }

    func GetTransactionDetails(Type:NSString,ShowProgress:Bool)
     {
        
        let param=["user_id":"\(themes.getUserID())","type":"\(Type)"]
        if(ShowProgress)
        {
            self.showProgress()
        }
        self.Transactiontable.backgroundView=nil

        
        URL_handler.makeCall(constant.Get_TransactionDetail, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            
            self.Walletsegment.enabled=true
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)

               // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                nibView.frame = self.Transactiontable.bounds;
                self.Transactiontable.backgroundView=nibView
                
                self.pullToRefreshManager.tableViewReloadFinished()
                
            }
            else
            {
            let dict:NSDictionary=responseObject!
            
            let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
            
            if(Status == "1")
            {
                
                
                
                let TransDict:NSDictionary=responseObject?.objectForKey("response")  as! NSDictionary
                
                let TransArray:NSArray?=TransDict.objectForKey("trans") as? NSArray
                self.emptyArray()
                
                if(TransArray != nil)
                {
                    
                    if(TransArray?.count != 0)
                    {
                        
                        for Dictionary in TransArray!
                        {
                            let Transtype:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("type"))!
                            self.TransacTypeArray.addObject(Transtype)
                            let Transamount:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("trans_amount"))!
                             self.TransacAmountArray.addObject(Transamount)
                            let Transtitle:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("title"))!
                            self.TransacTitleArray.addObject(Transtitle)
                            let TransDate:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("trans_date"))!
                            self.TransacDateArray.addObject(TransDate)
                            let Transbalance:NSString=self.themes.CheckNullValue(Dictionary.objectForKey("balance_amount"))!
                            self.TransacBalanceArray.addObject(Transbalance)
                        

                    }
                         self.Transactiontable.backgroundView=nil
                    }
                    else
                    {
                        
                        let nibView = NSBundle.mainBundle().loadNibNamed("Nodata", owner: self, options: nil)[0] as! UIView
                        nibView.frame = self.Transactiontable.bounds;
                        self.Transactiontable.backgroundView=nibView
                        
                        if(self.TransacTypeArray.count != 0)
                        {
                            self.Transactiontable.backgroundView=nil
                            
                        }
                        
                        self.presentViewController(self.themes.Showtoast("No more Orders"), animated: false, completion: nil)
                    }
                }
                
                self.Transactiontable.reloadData()
                self.pullToRefreshManager.tableViewReloadFinished()
            }
                
            else
            {
                self.pullToRefreshManager.tableViewReloadFinished()
                
                let Response:NSString=responseObject?.objectForKey("response")  as! NSString
                self.themes.AlertView("\(Appname)", Message: "\(Response)", ButtonTitle: self.themes.setLang("ok"))
                self.emptyArray()
                self.Transactiontable.reloadData()
                }

                
                
            }
        }
    }
    
    
    func emptyArray()
    {
        
            if(self.TransacTypeArray.count != 0)
            {
                self.TransacTypeArray.removeAllObjects()
            }
            if(self.TransacAmountArray.count != 0)
            {
                self.TransacAmountArray.removeAllObjects()
            }
            if(self.TransacTitleArray.count != 0)
            {
                self.TransacTitleArray.removeAllObjects()
            }
            if(self.TransacDateArray.count != 0)
            {
                self.TransacDateArray.removeAllObjects()
            }
            if(self.TransacBalanceArray.count != 0)
            {
                self.TransacBalanceArray.removeAllObjects()
            }
        
        
        
    }
    
    
    override func showProgress()
    {
        self.activityIndicatorView.color = themes.DarkRed()
        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        self.activityIndicatorView.startAnimation()
        self.view.addSubview(activityIndicatorView)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //TableViewDelegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return TransacTypeArray.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 15
    }
    
    func  tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //
    //
    //        let height_Cell:CGFloat = self.themes.calculateHeightForString("\(OrderCompleteDetailArray.objectAtIndex(indexPath.section))")
    //
    //
    //        //         self.Height_Return(height_Cell)
    //        return height_Cell+135
    //
    //     }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell:TransTableViewCell = tableView.dequeueReusableCellWithIdentifier("TransaCell") as! TransTableViewCell
        
        
        
        Cell.frame.size.width=100.0
        
        
        Cell.selectionStyle = .None
        
        Cell.layer.cornerRadius = 5
        Cell.layer.shadowColor = UIColor.blackColor().CGColor
        Cell.layer.shadowOpacity = 0.5
        Cell.layer.shadowRadius = 2
        Cell.layer.shadowOffset = CGSizeMake(3.0, 3.0)

        
      
     NSLog("get cuurency symbol=%@", self.themes.getCurrencyCode())
        
        Cell.Transac_titlelabl.text="\(TransacTitleArray.objectAtIndex(indexPath.section))"
        Cell.Transac_Datelabl.text="\(themes.setLang("date")):\(TransacDateArray.objectAtIndex(indexPath.section))"
        Cell.Transac_amountlabl.text="\(themes.setLang("amount")):\(self.themes.getCurrencyCode())\(TransacAmountArray.objectAtIndex(indexPath.section))"
        Cell.Transac_Balancelabl.text="\(themes.setLang("balance")):\(self.themes.getCurrencyCode())\(TransacBalanceArray.objectAtIndex(indexPath.section))"
        
    
        
      
        
        
        
        return Cell
        
    }
    


}
