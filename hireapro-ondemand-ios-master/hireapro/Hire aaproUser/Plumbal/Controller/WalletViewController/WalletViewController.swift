//
//  WalletViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 01/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
//import SwiftyJSON



class WalletViewController: RootViewController
    
    
{
    
    var paypal_status:Int!
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet var displayWalletview: UIView!
    @IBOutlet var MyWallet_Lbl: UIButton!
    @IBOutlet var Recharge_Wallet_Lbl: UILabel!
    @IBOutlet var Slide_Menu_But: UIButton!
    @IBOutlet var Amount_1_But: UIButton!
    
    @IBOutlet var Amount_2_But: UIButton!
    
    @IBOutlet var Amount_3_But: UIButton!
    @IBOutlet var Wallet_View: UIView!
    
    @IBOutlet var paypalBtn: CustomButton!
    @IBOutlet var add_Amt_But: UIButton!
    @IBOutlet var Amount_textField: UITextField!
    @IBOutlet var Amount_Lab: UILabel!
    
    @IBOutlet var Wallet_ScrollView: UIScrollView!
    @IBOutlet weak var current_bal_lbl: UILabel!
    @IBOutlet weak var walletDisc: UILabel!
    
    
    var URL_handler:URLhandler=URLhandler()
    
    
    
    var themes:Themes=Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
        setPage()
        self.paypalBtn.hidden = true
        self.orLbl.hidden =  true
    }
    
    
    
    func setPage(){
        self.Wallet_ScrollView.hidden = false
        
        Wallet_ScrollView.contentSize.height = add_Amt_But.frame.origin.y+add_Amt_But.frame.height+20
        MyWallet_Lbl.setTitle(themes.setLang("wallet_money"), forState: UIControlState.Normal)
        Recharge_Wallet_Lbl.text=themes.setLang("recharge_wallet")
        Amount_textField.placeholder = "\(themes.setLang("wallet_amount")) $5 - $500"
        walletDisc.text = themes.setLang("cash_less")
        add_Amt_But.setTitle(themes.setLang("add_wallet"), forState: UIControlState.Normal)
        paypalBtn.setTitle(themes.setLang("add_paypal"), forState: UIControlState.Normal)
        current_bal_lbl.text = themes.setLang("current_bal")
        
        //MyWallet_Lbl.setTitle(themes.setLang("\(Appname) Money"), forState: UIControlState.Normal)
        StripeStatus="Wallet"
        title = "\(Appname) Paypal Gateway"
        // Set up payPalConfig
        
        
        
        
        
        
        
        //Shadow offset for buttons
        
        Amount_1_But.layer.cornerRadius = 5
        Amount_1_But.layer.shadowColor = UIColor.blackColor().CGColor
        Amount_1_But.layer.shadowOpacity = 0.5
        Amount_1_But.layer.shadowRadius = 2
        Amount_1_But.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        Amount_1_But.backgroundColor=UIColor.whiteColor()
        
        
        Amount_2_But.layer.cornerRadius = 5
        Amount_2_But.layer.shadowColor = UIColor.blackColor().CGColor
        Amount_2_But.layer.shadowOpacity = 0.5
        Amount_2_But.layer.shadowRadius = 2
        Amount_2_But.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        Amount_2_But.backgroundColor=UIColor.whiteColor()
        
        
        
        Amount_3_But.layer.cornerRadius = 5
        Amount_3_But.layer.shadowColor = UIColor.blackColor().CGColor
        Amount_3_But.layer.shadowOpacity = 0.5
        Amount_3_But.layer.shadowRadius = 2
        Amount_3_But.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        Amount_3_But.backgroundColor=UIColor.whiteColor()
        
        add_Amt_But.setTitle(themes.setLang("add_wallet"), forState: UIControlState.Normal)
        
        add_Amt_But.layer.cornerRadius = 5
        add_Amt_But.layer.shadowColor = UIColor.blackColor().CGColor
        add_Amt_But.layer.shadowOpacity = 0.5
        add_Amt_But.layer.shadowRadius = 2
        add_Amt_But.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        
        
        paypalBtn.layer.cornerRadius = 5
        paypalBtn.layer.shadowColor = UIColor.blackColor().CGColor
        paypalBtn.layer.shadowOpacity = 0.5
        paypalBtn.layer.shadowRadius = 2
        paypalBtn.layer.shadowOffset = CGSizeMake(3.0, 3.0)
        
        //Corner Radius for
        Wallet_View.layer.cornerRadius=themes.RoundView(Wallet_View.frame.size.width)
        Wallet_View.clipsToBounds=true
        Wallet_View.backgroundColor = UIColor.whiteColor()
        
        //        if(themes.screenSize.height == 480)
        //        {
        //            Wallet_ScrollView.contentSize.height=600
        //        }
        //
        //        if(themes.screenSize.height == 568)
        //        {
        //            Wallet_ScrollView.contentSize.height=500
        //        }
        //        if(themes.screenSize.height == 667)
        //        {
        //            Wallet_ScrollView.contentSize.height=600
        //        }
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(WalletViewController.DismissKeyboard(_:)))
        
        view.addGestureRecognizer(tapgesture)
        
        
        //
        Amount_textField.addTarget(self, action: #selector(WalletViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        Amount_textField.delegate=self
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor=UIColor.whiteColor()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: themes.setLang("done"), style: UIBarButtonItemStyle.Done, target: self, action: #selector(WalletViewController.doneButtonAction))
        
        
        doneToolbar.items = [flexSpace,done]
        
        doneToolbar.sizeToFit()
        
        Amount_textField.inputAccessoryView = doneToolbar
        
        
        let Tap:UITapGestureRecognizer=UITapGestureRecognizer()
        Tap.addTarget(self, action: #selector(WalletViewController.PushtoTransactionView(_:)))
        displayWalletview.addGestureRecognizer(Tap)
        // Do any additional setup after loading the view.
        
    }
    
    
    
    func PushtoTransactionView(sender:UITapGestureRecognizer)
    {
        self.performSegueWithIdentifier("WalletDetailVC", sender: nil)
        
    }
    func applicationLanguageChangeNotification(notification:NSNotification)
    {
        
        //        themes.setLang(
        
        //        themes.setLang("Full Name")
        MyWallet_Lbl.setTitle(themes.setLang("   My Wallet"), forState: UIControlState.Normal)
        Recharge_Wallet_Lbl.text=themes.setLang("Recharge Wallet money")
        add_Amt_But.setTitle(themes.setLang("Add to Wallet"), forState: UIControlState.Normal)
    }
    
    
    
    func doneButtonAction()
    {
        if(themes.screenSize.size.height > 480)
        {
            //            Wallet_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
        Amount_textField.resignFirstResponder()
        
        
    }
    
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    
    func DismissKeyboard(sender:UITapGestureRecognizer)
    {
        
        //        Wallet_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
        
        view.endEditing(true)
        
        
    }
    
    
    
    func textFieldDidChange(textField: UITextField) {
        
        if(textField == Amount_textField)
        {
            if(Amount_textField.text == "\(MyWallet.min_amount)")
            {
                
                Amount_1_But.backgroundColor=PlumberThemeColor
                
                
            }
            else
            {
                Amount_1_But.backgroundColor=UIColor.whiteColor()            }
            if(Amount_textField.text == "\(MyWallet.middle_amount)")
            {
                Amount_2_But.backgroundColor=PlumberThemeColor
                
                
                
            }
            else
            {
                Amount_2_But.backgroundColor=UIColor.whiteColor()
            }
            
            if(Amount_textField.text == "\(MyWallet.max_amount)")
            {
                Amount_3_But.backgroundColor=PlumberThemeColor
                
            }
            else
            {
                Amount_3_But.backgroundColor=UIColor.whiteColor()
            }
            
            
        }
        
        //your code
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        
        
        //        if(themes.screenSize.height == 480)
        //        {
        
        //            if(textField == Amount_textField)
        //            {
        //                Wallet_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 180), animated: true)
        //
        //            }
        //        }
        //        else
        //        {
        //            if(textField == Amount_textField)
        //            {
        //                Wallet_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 140), animated: true)
        //
        //            }
        //
        //        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        Amount_textField.text = ""
        
        
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == Amount_textField)
        {
            
            if(NSString(string: string).length  == 0)
            {
                return true
            }
            
            
            let textfield_Count:Int=Int((Amount_textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string))!
            
            return textfield_Count <= MyWallet.max_amount
            
        }
        
        return true
        
        
    }
    
    
    
    
    
    @IBAction func didClickoption(sender: UIButton) {
        
        if(sender.tag == 4)
        {
            
            doneButtonAction()
            
            
            Transaction_Stat.StripeStatus  = true
            Transaction_Stat.total_Amt=Amount_textField.text!
            
            if (Amount_textField.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("amunt_is_empty"), ButtonTitle: kOk)
                
            }
                
            else if(Int(Transaction_Stat.total_Amt as String) < 10)
            {
                
                themes.AlertView("\(Appname)", Message: themes.setLang("amunt_is_low"), ButtonTitle: kOk)
            }
            else
            {
                themes.amount = Amount_textField.text!
                
                constant.showProgress()
                
                let Param:NSDictionary=["user_id":"\(themes.getUserID())","total_amount":Transaction_Stat.total_Amt ]
                URL_handler.makeCall(constant.Wallet_Recharge_paypal, param: Param) { (responseObject, error) -> () in
                    
                    constant.DismissProgress()
                    
                    if(error != nil)
                    {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                        
                        // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                        
                    }
                    else
                    {
                        
                        
                        let Dict:NSDictionary=responseObject!
                        let Status:NSString?=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                        if(Status != nil)
                        {
                            
                            if(Status == "1")
                            {
                                //  Payment_Detail.Mobile_id=self.themes.CheckNullValue(Dict.objectForKey("mobile_id") )!
                                
                                Transaction_Stat.wallet_rechargeurl = self.themes.CheckNullValue(Dict.objectForKey("redirectUrl"))!
                                
                                self.performSegueWithIdentifier("TransactionVC", sender: nil)
                                
                            }
                            else
                            {
                                constant.DismissProgress()
                                let Response:NSString?=self.themes.CheckNullValue(Dict.objectForKey("errors"))!
                                self.themes.AlertView(self.themes.setLang("Message"), Message: "\(Response!)", ButtonTitle: self.themes.setLang("ok"))
                                
                            }
                        }
                    }
                }
                
                
                
            }
            
            
        }
        
        
        if(sender.tag == 0)
        {
            Amount_textField.text=nil
            Amount_textField.text="\(MyWallet.min_amount)"
            
            Amount_1_But.backgroundColor=PlumberThemeColor
            add_Amt_But.enabled=true
            
        }
        else
        {
            Amount_1_But.backgroundColor=UIColor.whiteColor()
        }
        
        if(sender.tag == 1)
        {
            Amount_textField.text=nil
            Amount_textField.text="\(MyWallet.middle_amount)"
            Amount_2_But.backgroundColor=PlumberThemeColor
            add_Amt_But.enabled=true
            
            
        }
        else
        {
            Amount_2_But.backgroundColor=UIColor.whiteColor()
        }
        
        if(sender.tag == 2)
        {
            Amount_textField.text=nil
            Amount_textField.text="\(MyWallet.max_amount)"
            Amount_3_But.backgroundColor=PlumberThemeColor
            add_Amt_But.enabled=true
            
            
        }
        else
        {
            Amount_3_But.backgroundColor=UIColor.whiteColor()
        }
        
        
        if(sender.tag == 3)
        {
            
            doneButtonAction()
            
            
            Transaction_Stat.StripeStatus = true
            Transaction_Stat.total_Amt=Amount_textField.text!
            
            if (Amount_textField.text == "")
            {
                themes.AlertView("\(Appname)", Message: themes.setLang("amunt_is_empty"), ButtonTitle: kOk)
                
            }
                
            else if(Int(Transaction_Stat.total_Amt as String) < 10)
            {
                
                themes.AlertView("\(Appname)", Message: themes.setLang("amunt_is_low"), ButtonTitle: kOk)
            }
            else
            {
                themes.amount = Amount_textField.text!
                
                self.performSegueWithIdentifier("TransactionVC", sender: nil)
                
            }
            
        }
        
        
        
        
        
    }
    
    
    func getStripeDetails()
    {
        add_Amt_But.enabled=false
        let param=[:]
        
        self.showProgress()
        let amount : String!
        amount = Amount_textField.text
        let paymentUrl : String = "\(constant.Wallet_Recharge)user_id=\(themes.getUserID())&total_amount=\(amount)"
        
        
        
        URL_handler.makeCall(paymentUrl, param: param) { (responseObject, error) -> () in
            self.add_Amt_But.enabled=true
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
                
            else
            {
                
                
                if(responseObject != nil)
                {
                    let dict:NSDictionary=responseObject!
                    let Staus:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    
                    
                    if (Staus == "1")
                    {
                        MyWallet.stripe_keys=dict.objectForKey("response")!.objectForKey("stripe_keys") as! NSDictionary
                        MyWallet.mode=MyWallet.stripe_keys.objectForKey("mode") as! NSString
                        MyWallet.secret_key=MyWallet.stripe_keys.objectForKey("secret_key") as! NSString
                        MyWallet.publishable_key=MyWallet.stripe_keys.objectForKey("publishable_key") as! NSString
                        MyWallet.cards=dict.objectForKey("response")!.objectForKey("cards") as! NSDictionary
                        
                        MyWallet.card_status=MyWallet.cards.objectForKey("card_status") as! NSString
                        if(MyWallet.card_status == "1")
                        {
                            MyWallet.result=MyWallet.cards.objectForKey("result") as! NSArray
                            
                            if(MyWallet.card_number.count != 0)
                            {
                                MyWallet.card_number.removeAllObjects()
                                MyWallet.exp_month.removeAllObjects()
                                MyWallet.exp_year.removeAllObjects()
                                MyWallet.card_type.removeAllObjects()
                                MyWallet.customer_id.removeAllObjects()
                                MyWallet.card_id.removeAllObjects()
                            }
                            
                            for Dic in MyWallet.result
                            {
                                MyWallet.card_number.addObject(Dic.objectForKey("card_number") as! NSString)
                                MyWallet.exp_month.addObject(Dic.objectForKey("exp_month") as! NSString)
                                MyWallet.exp_year.addObject(Dic.objectForKey("exp_year") as! NSString)
                                MyWallet.card_type.addObject(Dic.objectForKey("card_type") as! NSString)
                                MyWallet.customer_id.addObject(Dic.objectForKey("customer_id") as! NSString)
                                MyWallet.card_id.addObject(Dic.objectForKey("card_id") as! NSString)
                                
                                
                                
                            }
                            
                            let Controller:CardListViewController=self.storyboard?.instantiateViewControllerWithIdentifier("cardListVC") as! CardListViewController
                            self.navigationController?.pushViewController(Controller, animated: true)
                            
                            
                            
                        }
                        else
                        {
                            //                            let Storyboard:UIStoryboard=UIStoryboard(name: "Main", bundle: nil)
                            //                            let vc = Storyboard.instantiateViewControllerWithIdentifier("StripeVC")
                            //                            self.presentViewController(vc, animated: true, completion: nil)
                            
                            let Controller:StripeViewController=self.storyboard?.instantiateViewControllerWithIdentifier("StripeVC") as! StripeViewController
                            self.navigationController?.pushViewController(Controller, animated: true)
                            
                            
                        }
                        
                        
                        
                    }
                
                    else
                    {
                        self.themes.AlertView("\(Appname)",Message: "\(dict.objectForKey("response")!)",ButtonTitle: self.themes.setLang("ok"))
                        self.navigationController?.popViewControllerAnimated(true)
                        
                    }
 
                    
                }
                else
                {
                    self.DismissProgress()
                    
                    self.themes.AlertView("\(Appname)",Message: self.themes.setLang("Please try again"),ButtonTitle: self.themes.setLang("ok"))
                    
                    
                    
                }
                
            }
        }
        
        
    }
    override func viewDidAppear(animated: Bool) {
        
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.showPopup(_:)), name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.Show_rating(_:)), name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WalletViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushPayment", object: nil)
        
        walletData()
        
        
        
        add_Amt_But.enabled=true
      
        
        
    }
    
    
    
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: Language_Notification as String, object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
    }
    
    override func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    Message_details.support_chatid = refer_id!
                    Message_details.admin_id = admin_id!
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    override func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
                    alertView.addButton(self.themes.setLang("ok"), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }
    override func Show_rating(notification: NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(RatingsViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    
                    let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
        
    }
    
    override func showPopup(notification: NSNotification)
    {
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(PaymentViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    
                    let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                    
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
        
        
        
    }
    override func methodofReceivePaymentNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    override func methodofReceiveRatingNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        // let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
        self.navigationController?.pushViewController(Controller, animated: true)
        
        
        
        
        
        
    }
    
    
    
    
    override func methodOfReceivedMessagePushNotification(notification: NSNotification){
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            
            Message_details.taskid = taskid!
            Message_details.providerid = check_userid
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        
        
    }
    
    
    override func methodOfReceivedMessageNotification(notification: NSNotification){
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                    
                }else{
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_provider"))
                    alertView.addButton(self.themes.setLang("ok"), action: {
                        Message_details.taskid = taskid!
                        Message_details.providerid = check_userid
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                        
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                        
                        
                    })
                    alertView.show()
                    
                }
                
            }
        }
        
        
        
        
        
    }
    
    
    override func Show_Alert(notification:NSNotification)
    {
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        let action:NSString! = userInfo["Action"]
        
        
        if(Order_id != nil)
        {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
                
            }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(self.themes.setLang("ok"), action: {
                    if action != "admin_notification"{
                        
                        let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                        self.navigationController?.pushViewController(Controller, animated: true)
                        
                    }
                })
                alertView.show()
                
                
                
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    func walletData()
    {
        
        let param=["user_id":themes.getUserID()]
        
        self.showProgress()
        
        URL_handler.makeCall(constant.Mymoney, param: param) { (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.Wallet_ScrollView.hidden = true
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
            }
                
            else
            {
                
                
                if(responseObject != nil)
                {
                    
                    let json = JSON(responseObject!)
                    
                    
                    MyWallet.Status=json["status"].string!
                    
                    if(MyWallet.Status == "1")
                    {
                        self.DismissProgress()
                        
                        self.Wallet_ScrollView.hidden = false
                        
                        MyWallet.auto_charge_status = json["auto_charge_status"].string!
                        MyWallet.currency = self.themes.Currency_Symbol(json["response"]["currency"].string!)
                        if (json["response"]["current_balance"].string != nil)
                        {
                            MyWallet.current_balance = json["response"]["current_balance"].string!
                        }
                        //                    MyWallet.recharge_boundary = json["response"]["recharge_boundary"].intValue
                        MyWallet.max_amount = json["response"]["recharge_boundary"]["max_amount"].intValue
                        
                        MyWallet.middle_amount = json["response"]["recharge_boundary"]["middle_amount"].intValue
                        MyWallet.min_amount = json["response"]["recharge_boundary"]["min_amount"].intValue
                        MyWallet.paypal_status = json["response"]["paypal_status"].intValue
                        self.Amount_Lab.text="\(MyWallet.currency)\(MyWallet.current_balance)"
                        
                        self.themes.saveCurrencyCode("\(self.themes.Currency_Symbol(json["response"]["currency"].string!))")
                        self.themes.saveCurrency("\(MyWallet.current_balance)")
                        self.Amount_1_But.setTitle(" \(MyWallet.currency)\(MyWallet.min_amount)", forState: UIControlState.Normal)
                        self.Amount_2_But.setTitle(" \(MyWallet.currency)\(MyWallet.middle_amount)", forState: UIControlState.Normal)
                        self.Amount_3_But.setTitle(" \(MyWallet.currency)\(MyWallet.max_amount)", forState: UIControlState.Normal)
                        self.Amount_textField.placeholder = "\(self.themes.setLang("wallet_amount")) \(MyWallet.currency)0 - \(MyWallet.currency)\(MyWallet.max_amount)"
                        //
                        //self.Amount_Lab.sizeToFit()
                        
                        
                        if MyWallet.paypal_status == 0 {
                            self.paypalBtn.hidden = true
                            self.orLbl.hidden =  true
                        }
                        else
                        {
                            self.paypalBtn.hidden = false
                            self.orLbl.hidden =  false

                        }
                        
                        
                    }
                    else
                    {
                        
                        self.DismissProgress()
                        self.Wallet_ScrollView.hidden = true
                        
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_amount_credited"), ButtonTitle: kOk)
                        
                    }
                    
                    
                }
                    
                else
                {
                    self.DismissProgress()
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    
                }
            }
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}

extension WalletViewController:UITextFieldDelegate
{
    
}
