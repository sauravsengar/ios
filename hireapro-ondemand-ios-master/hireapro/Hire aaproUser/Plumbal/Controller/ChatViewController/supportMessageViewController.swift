//
//  supportMessageViewController.swift
//  Plumbal
//
//  Created by Casperon on 01/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class supportMessageViewController: RootViewController,InputbarDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,MessageGatewayDelegate{

    @IBOutlet var support_inputbar: Inputbar!
    @IBOutlet var supportmsg_table: UITableView!
    @IBOutlet var topic: UILabel!
    
    var ChatDetailsArr : NSMutableArray = NSMutableArray()
    var FromDetailArray: NSMutableArray = NSMutableArray()
    var textArray : NSMutableArray = NSMutableArray()
    var getDateArray :NSMutableArray = NSMutableArray ()
    var tableArray:TableArray=TableArray()
    var gateway:MessageGateway=MessageGateway()
    
    var type_str:NSString=NSString()
    var chat:Chat=Chat()
    var themes:Themes=Themes()
    var URL_Handler:URLhandler=URLhandler()
    
    
    var people = [NSManagedObject]()
    
    
    
    
    let App_Delegate=UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        topic.text = themes.setLang("support")
        support_inputbar.delegate=self;
        setInputbar()
        self.setTableView()
        GetDetails()
        

        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action:#selector(DismissKeyboard))
        tapgesture.delegate = self;
        
        view.addGestureRecognizer(tapgesture)
        setTest()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func didclickoption(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
   
    
    func DismissKeyboard()
    {
            
        support_inputbar.resignFirstResponder()
        support_inputbar.hideKeyboard()
            
            
    }
    
        func GetDetails()
        {
            topic.text = Message_details.topic_title as String
            constant.showProgress()
            let param=["user":themes.getUserID(),"type":"user","message":"\(Message_details.support_chatid)","admin":"\(Message_details.admin_id)"];
            
            
            URL_Handler.makeCall(constant.Chat_support_history, param: param) { (responseObject, error) -> () in
                constant.DismissProgress()
                if(error != nil)
                {
                    // self.themes.AlertView("", Message:"", ButtonTitle: "Ok")
                    
                }
                else
                {
                    
                    if(responseObject != nil)
                    {
                        let status = self.themes.CheckNullValue(responseObject!.objectForKey("status")!)
                        if status == "1"
                        {
                            
                        
                        let Dict:NSDictionary=responseObject!.objectForKey("response") as! NSDictionary
                        
                        
                        let MessageDetails : NSMutableArray = Dict.objectForKey("history") as! NSMutableArray
                        
                        let messageArray : NSMutableArray = MessageDetails.objectAtIndex(0).objectForKey("messages") as! NSMutableArray
                            
                            self.topic.text = self.themes.CheckNullValue(MessageDetails.objectAtIndex(0).objectForKey("topic")!)
                        
                        
                        if messageArray.count > 0
                        {
                            
                            for data  in messageArray
                            {
                                
                                self.textArray.addObject(self.themes.CheckNullValue(data.valueForKey("message"))!)
                                self.FromDetailArray.addObject(self.themes.CheckNullValue(data.valueForKey("from"))!)
                                self.getDateArray.addObject(self.themes.CheckNullValue(data.valueForKey("date"))!)
                            }
                            
                            
                            Message_details.job_id=Message_details.taskid as String
                            
                            
                            var k : Int
                            for k=0 ; k<self.textArray.count; k += 1
                            {
                                let mesg : Message = Message()
                                mesg.text = self.textArray.objectAtIndex(k) as! String;
                                
                                let text = self.getDateArray.objectAtIndex(k)  as! String
                                let types: NSTextCheckingType = .Date
                                var getdate = NSDate()
                                let detector = try? NSDataDetector(types: types.rawValue)
                                let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
                                for match in matches {
                                    getdate = (match.date!)
                                }
                                mesg.date = getdate
                                
                                if self.FromDetailArray.objectAtIndex(k) as! String == self.themes.getUserID()
                                {
                                    mesg.sender = MessageSender.Myself;
                                    
                                }
                                else{
                                    mesg.sender = MessageSender.Someone;
                                }
                                
                                
                                self.tableArray.addObject(mesg)
                            }
                            
                            
                            self.supportmsg_table.reloadData()
                            self.tableViewScrollToBottomAnimated(true)
                            
                        }
                    }
                    }
                    else
                    {
                        self.themes.AlertView("\(Appname)", Message: "No Reasons available", ButtonTitle: self.themes.setLang("ok"))
                    }
                }
                
                
            }
            
        }
        
        
    
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(true)
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChat", object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShowSupportMessage(_:)), name: "ReceiveSupportChat", object: nil)
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChat", object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShowSupportMessage(_:)), name: "ReceiveSupportPushChat", object: nil)

            let controller:supportMessageViewController=self
            self.view.keyboardTriggerOffset = support_inputbar.frame.size.height
            self.view!.addKeyboardPanningWithActionHandler({(keyboardFrameInView: CGRect, opening: Bool, closing: Bool) -> Void in
                var toolBarFrame = self.support_inputbar.frame
                toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height
                self.support_inputbar.frame = toolBarFrame
                var tableViewFrame = self.supportmsg_table.frame
                tableViewFrame.size.height = toolBarFrame.origin.y - 80
                self.supportmsg_table.frame = tableViewFrame
                controller.tableViewScrollToBottomAnimated(false)
            })
        }
      func ShowSupportMessage(notification: NSNotification)
     {
     //support_inputbar.resignFirstResponder()
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let getmessage:String = self.themes.CheckNullValue(userInfo["message"]!)!
        let date:String = self.themes.CheckNullValue(userInfo["date"]!)!
        let from:String = self.themes.CheckNullValue(userInfo["from"]!)!
        
        let getreference:String = self.themes.CheckNullValue(userInfo["reference"]!)!
        if Message_details.support_chatid == ""
        {
            let mesg : Message = Message()
            mesg.text = getmessage
            
            let text = date
            let types: NSTextCheckingType = .Date
            var getdate = NSDate()
            let detector = try? NSDataDetector(types: types.rawValue)
            let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
            for match in matches {
                getdate = (match.date!)
            }
            mesg.date = NSDate()
            
            if from == self.themes.getUserID()
            {
                mesg.sender = MessageSender.Myself;
                
            }
            else{
                mesg.sender = MessageSender.Someone;
            }
            
            
            self.tableArray.addObject(mesg)
            self.supportmsg_table.reloadData()
            self.tableViewScrollToBottomAnimated(true)
            
            Message_details.support_chatid = getreference
            Message_details.admin_id = from

        }
        else{
        if getreference ==  Message_details.support_chatid

        {
            let mesg : Message = Message()
            mesg.text = getmessage
            
            let text = date
            let types: NSTextCheckingType = .Date
            var getdate = NSDate()
            let detector = try? NSDataDetector(types: types.rawValue)
            let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
            for match in matches {
                getdate = (match.date!)
            }
            mesg.date = NSDate()
            
            if from == self.themes.getUserID()
            {
                mesg.sender = MessageSender.Myself;
                
            }
            else{
                mesg.sender = MessageSender.Someone;
            }
            
            
            self.tableArray.addObject(mesg)
            self.supportmsg_table.reloadData()
            self.tableViewScrollToBottomAnimated(true)

            
        }else{
            support_inputbar.resignFirstResponder()
            
            let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
            alertView.addButton(self.themes.setLang("ok"), action: {
                
                Message_details.support_chatid = getreference
                Message_details.admin_id = from
                self.ReloadMessageView()
            })
            alertView.show()

        }
        
        }
        
     }

    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    
    func   ReloadMessageView() {
        
        setInputbar()
        self.setTableView()
        
        ChatDetailsArr  = NSMutableArray()
        textArray  = NSMutableArray()
        FromDetailArray = NSMutableArray()
        self.tableArray = TableArray()
        gateway = MessageGateway()
        self.getDateArray = NSMutableArray()
        
        GetDetails()
        
        setTest()
        
    }
    
    
        func setTest()
        {
            chat = Chat()
            chat.sender_name = "Player 1"
            chat.receiver_id = "12345"
            chat.sender_id = "54321"
            let texts: NSArray = []
            var last_message: Message? = nil
            for text in texts {
                let message: Message = Message()
                message.text = text as! String
                message.sender = .Someone
                message.status = .Received
                message.chat_id = chat.identifier()
                LocalStorage.sharedInstance().storeMessage(message)
                last_message = message
            }
            chat.numberOfUnreadMessages = texts.count
            if(last_message != nil)
            {
                chat.last_message = last_message!
            }
            
        }
        
        
        func textViewDidChange(textView: UITextView) {
            
            
            NSLog("the textview=%@",textView.text)
            
        }
        @IBAction func didClickOptions(sender: AnyObject) {
            if(sender.tag == 0)
            {
                self.navigationController?.popViewControllerAnimated(true)
                self.dismissViewControllerAnimated(true, completion: nil)
                
                
            }
        }
        
    
    
        
        func setInputbar()
        {
            self.support_inputbar.placeholder = nil;
            self.support_inputbar.delegate = self;
            
            self.support_inputbar.rightButtonText = themes.setLang("send");
            self.support_inputbar.rightButtonTextColor = UIColor(red: 0, green: 124/255.0, blue: 1, alpha: 1)
        }
        
        func setTableView()
        {
            self.tableArray = TableArray()
            self.supportmsg_table.delegate = self;
            self.supportmsg_table.dataSource = self;
            self.supportmsg_table.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0,view.frame.size.width, 10.0))
            self.supportmsg_table.separatorStyle = UITableViewCellSeparatorStyle.None;
            self.supportmsg_table.backgroundColor = UIColor.clearColor()
            self.supportmsg_table.registerClass(MessageCell.classForCoder(), forCellReuseIdentifier: "MessageCell")
            
        }
        func setGateway()
        {
            gateway = MessageGateway()
            gateway.delegate = self;
            gateway.chat = self.chat;
            gateway.loadOldMessages()
            
            
        }
        
        //TableView Delegate
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return self.tableArray.numberOfSections()
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.tableArray.numberOfMessagesInSection(section)
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let CellIdentifier: String = "MessageCell"
            var cell: MessageCell! = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! MessageCell
            if (cell == nil) {
                cell = MessageCell(style: .Default, reuseIdentifier: CellIdentifier)
            }
            if(self.tableArray.objectAtIndexPath(indexPath) != nil){
                cell.message = self.tableArray.objectAtIndexPath(indexPath)
            }
            return cell
        }
        func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return self.tableArray.titleForSection(section)
        }
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            let message: Message = self.tableArray.objectAtIndexPath(indexPath)
            return message.heigh
        }
        
        func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 40.0
        }
        
        func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            
            let frame: CGRect = CGRectMake(0, 0, tableView.frame.size.width, 40)
            let view: UIView = UIView(frame: frame)
            view.backgroundColor = UIColor.clearColor()
            view.autoresizingMask = .FlexibleWidth
            let label: UILabel = UILabel()
            label.text = self.tableArray.titleForSection(section)
            label.textAlignment = .Center
            label.font = UIFont(name: "Helvetica", size: 20.0)
            label.sizeToFit()
            label.center = view.center
            label.font = UIFont(name: "Helvetica", size: 13.0)
            label.backgroundColor = UIColor(red: 207 / 255.0, green: 220 / 255.0, blue: 252.0 / 255.0, alpha: 1)
            label.layer.cornerRadius = 10
            label.layer.masksToBounds = true
            label.autoresizingMask = .None
            view.addSubview(label)
            return view
            
        }
        func tableViewScrollToBottomAnimated(animated: Bool) {
            let numberOfSections:NSInteger=tableArray.numberOfSections()
            let numberOfRows:NSInteger=tableArray.numberOfMessagesInSection(numberOfSections-1)
            if(numberOfRows != 0)
            {
                self.supportmsg_table.scrollToRowAtIndexPath(tableArray.indexPathForLastMessage(), atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
            }
        }
        func inputbarDidPressRightButton(inputbar: Inputbar!) {
            
            if (inputbar.text() == "")
            {
                
            }
            else
            {
                
  SocketIOManager.sharedInstance.sendChatSupportMessage(inputbar.text(), userid: themes.getUserID(), admin_id:Message_details.admin_id as String, from_id:themes.Check_userID() , topic_title:self.topic.text!, type:"user", reference_id:Message_details.support_chatid as String)
                //  self.GetDetails()
            }
            
            
        }
        
        func MessageFromIn(str: String) {
            
            let message: Message = Message()
            message.text = str
            message.date = NSDate()
            message.chat_id = "1"
            message.sender = .Someone
            
            //Store Message in memory
            self.tableArray.addObject(message)
            //Insert Message in UI
            do
            {
                
                try moveTable(message)
            }
            catch
            {
                print("there is an error")
            }
            //Send message to server
            // HI
            //[self.gateway sendMessage:message];
        }
        
        func moveTable (message:Message)throws
        {
            let indexPath: NSIndexPath =  tableArray.indexPathForMessage(message)
            self.supportmsg_table.beginUpdates()
            if self.tableArray.numberOfMessagesInSection(indexPath.section) == 1 {
                self.supportmsg_table.insertSections(NSIndexSet(index:indexPath.section), withRowAnimation: .None)
            }
            
            //        self.tableView.reloadData()
            self.supportmsg_table.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
            self.supportmsg_table.endUpdates()
            self.supportmsg_table.scrollToRowAtIndexPath(self.tableArray.indexPathForLastMessage(), atScrollPosition: .Bottom, animated: true)
            
        }
        func inputbarDidPressLeftButton(inputbar: Inputbar!) {
            //        let alertView: UIAlertView = UIAlertView(title: "Left Button Pressed", message: "", delegate: nil, cancelButtonTitle: "Ok")
            //        alertView.show()
            
        }
        func gatewayDidUpdateStatusForMessage(message: Message) {
            let indexPath: NSIndexPath = tableArray.indexPathForMessage(message)
            let cell: MessageCell = self.supportmsg_table.cellForRowAtIndexPath(indexPath) as! MessageCell
            cell.updateMessageStatus()
        }
        
        func gatewayDidReceiveMessages(array: [AnyObject]) {
            self.tableArray.addObjectsFromArray(array)
            self.supportmsg_table.reloadData()
        }
    
        
        func inputbarDidBecomeFirstResponder(inputbar: Inputbar!) {
            
            
        }
        func inputbarTextEndEditingChat(inputbar: Inputbar!) {
    
            
        }
        
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
