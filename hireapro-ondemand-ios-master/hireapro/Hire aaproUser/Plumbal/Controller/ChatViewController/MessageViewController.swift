//
//  MessageViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 20/01/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit


class MessageViewController: RootViewController,InputbarDelegate,MessageGatewayDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate{
    
    
    @IBOutlet var Provider_Image: UIImageView!
    @IBOutlet var Block_Lbl: UILabel!
    @IBOutlet var User_lbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var inputbar: Inputbar!
    
    var ChatDetailsArr : NSMutableArray = NSMutableArray()
    var textArray : NSMutableArray = NSMutableArray()
    var FromDetailArray: NSMutableArray = NSMutableArray()
    var tasker_statusarray : NSMutableArray = NSMutableArray()
    var messgaeidarray : NSMutableArray = NSMutableArray()
    var getDateArray :NSMutableArray = NSMutableArray ()
    var tableArray:TableArray=TableArray()
    var gateway:MessageGateway=MessageGateway()
    @IBOutlet var typingLbl: RSDotsView!
    @IBOutlet var Status_Lbl: UIButton!
    @IBOutlet var Status_View: UILabel!
    var type_str:NSString=NSString()
    var chat:Chat=Chat()
    var themes:Themes=Themes()
    var URL_Handler:URLhandler=URLhandler()
    
    
    var people = [NSManagedObject]()
    
    
    
    
    let App_Delegate=UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        
        
        
        inputbar.delegate=self;
        super.viewDidLoad()
        setInputbar()
        self.setTableView()
        GetDetails()
        
        
        setStatusView()
        //inputbar.hidden=true
        //Block_Lbl.hidden=false
        SetImageView()
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action:#selector(DismissKeyboard))
        tapgesture.delegate = self;
        
        view.addGestureRecognizer(tapgesture)
        setTest()
        
        
    }
    override func viewWillAppear(animated: Bool) {
        Common_Chatid = Order_data.job_id
        
        
    }
    
    
    func DismissKeyboard()
    {
        
        inputbar.resignFirstResponder()
        inputbar.hideKeyboard()
        
        
    }
    
    
    
    
    override func viewDidDisappear(animated: Bool) {
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readSinglemessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readmessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
        Common_Chatid = ""
    }
    func setStatusView()
    {
        Status_View.layer.cornerRadius = Status_View.frame.size.height / 2;
        Status_View.layer.masksToBounds = true;
        Status_View.layer.borderWidth = 0;
        Status_View.contentMode = UIViewContentMode.ScaleAspectFill
        
    }
    func GetDetails()
    {
        constant.showProgress()
        let param=["user":themes.getUserID(),"tasker":Message_details.providerid as String,"task":Message_details.taskid as String, "type":"user","read_status":"user"];
        
        
        URL_Handler.makeCall(constant.Chat_Details, param: param) { (responseObject, error) -> () in
            constant.DismissProgress()
            if(error != nil)
            {
                // self.themes.AlertView("", Message:"", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    
                    let taskerDetails = Dict.objectForKey("tasker") as! NSDictionary
                    
                    self.User_lbl.text = taskerDetails.objectForKey("username") as? String
                    
                    Message_details.name=taskerDetails.objectForKey("username") as! String
                    
                    Message_details.image=taskerDetails.objectForKey("avatar") as! String
                    
                    self.Provider_Image.sd_setImageWithURL(NSURL(string:taskerDetails.objectForKey("avatar") as! String), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                    
                    let MessageDetails : NSMutableArray = Dict.objectForKey("messages") as! NSMutableArray
                    
                    
                    if MessageDetails.count > 0
                    {
                        
                        for data  in MessageDetails
                        {
                            
                            
                            self.textArray.addObject(self.themes.CheckNullValue(data.valueForKey("message"))!)
                            self.FromDetailArray.addObject(self.themes.CheckNullValue(data.valueForKey("from"))!)
                            self.tasker_statusarray.addObject(self.themes.CheckNullValue(data.valueForKey("tasker_status"))!)
                            self.messgaeidarray.addObject(self.themes.CheckNullValue(data.valueForKey("_id"))!)
                            self.getDateArray.addObject(self.themes.CheckNullValue(data.valueForKey("date"))!)
                        }
                        
                        
                        Message_details.job_id=Message_details.taskid as String
                        
                        
                        var k : Int
                        for k=0 ; k<self.textArray.count; k += 1
                        {
                            let mesg : Message = Message()
                            mesg.text = self.textArray.objectAtIndex(k) as! String;
                            
                            let text = self.getDateArray.objectAtIndex(k)  as! String
                            let types: NSTextCheckingType = .Date
                            var getdate = NSDate()
                            let detector = try? NSDataDetector(types: types.rawValue)
                            let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
                            for match in matches {
                                getdate = (match.date!)
                            }
                            mesg.date = getdate
                            
                            
                            if self.FromDetailArray.objectAtIndex(k) as! String == self.themes.getUserID()
                            {
                                mesg.sender = MessageSender.Myself;
                                if (self.tasker_statusarray.objectAtIndex(k) as! String == "2")
                                {
                                    mesg.status = MessageStatus.Read
                                }
                                else if (self.tasker_statusarray.objectAtIndex(k) as! String == "1")
                                {
                                    mesg.status = MessageStatus.Received
                                }
                                
                            }
                            else{
                                mesg.sender = MessageSender.Someone;
                            }
                            
                            self.tableArray.addObject(mesg)
                        }
                        
                        SocketIOManager.sharedInstance.SendingMessagestatus("user", Userid: self.themes.getUserID(), taskerid:Message_details.providerid as String, taskid: Message_details.taskid as String)
                        
                        self.tableView.reloadData()
                        self.tableViewScrollToBottomAnimated(true)
                        
                    }
                    
                }
                else
                {
                    self.themes.AlertView("\(Appname)", Message: self.themes.setLang("No Reasons available"), ButtonTitle: self.themes.setLang("ok"))
                }
            }
            
            
        }
        
    }
    
    
    func SetImageView()
    {
        Provider_Image.layer.cornerRadius = Provider_Image.frame.size.height / 2;
        Provider_Image.clipsToBounds=true
        Provider_Image.layer.masksToBounds = true;
        // Provider_Image.layer.borderWidth = 0;
        Provider_Image.contentMode = UIViewContentMode.ScaleAspectFill
        //Provider_Image.layer.borderWidth=2.0
        Provider_Image.layer.borderColor=UIColor.whiteColor().CGColor
        
        
    }
    
    func SetData()
    {
        
        if(Message_details.chat_status == "open" && Message_details.receiver_status == "online")
        {
            inputbar.hidden=false
            Block_Lbl.hidden=true
            Status_Lbl.setTitle("online", forState: UIControlState.Normal)
            Status_View.backgroundColor=UIColor.greenColor()
        }
        else
        {
            inputbar.hidden=true
            Block_Lbl.hidden=false
            Status_Lbl.setTitle("offline", forState: UIControlState.Normal)
            Status_View.backgroundColor=UIColor.grayColor()
        }
        
        User_lbl.text=Message_details.name as String
        Provider_Image.sd_setImageWithURL(NSURL(string: Message_details.image as String), completed: themes.block)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChat", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showMessage(_:)), name: "ReceiveChat", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChat", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showPushMessage(_:)), name: "ReceivePushChat", object: nil)
        
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showTypingStatus(_:)), name: "ReceiveTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveStopTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(stopTypingStatus(_:)), name: "ReceiveStopTypingMessage", object: nil)
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readmessagestatus", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageViewController.ReadmessageStatus(_:)), name:"readmessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readSinglemessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageViewController.ReadSinglemessageStatus(_:)), name:"readSinglemessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "Dismisskeyboard", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DismissKeyboard), name:"Dismisskeyboard", object: nil)
        
        
        
        let controller:MessageViewController=self
        self.view.keyboardTriggerOffset = inputbar.frame.size.height
        self.view!.addKeyboardPanningWithActionHandler({(keyboardFrameInView: CGRect, opening: Bool, closing: Bool) -> Void in
            var toolBarFrame = self.inputbar.frame
            toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height
            self.inputbar.frame = toolBarFrame
            var tableViewFrame = self.tableView.frame
            tableViewFrame.size.height = toolBarFrame.origin.y - 80
            self.tableView.frame = tableViewFrame
            controller.tableViewScrollToBottomAnimated(false)
        })
    }
    
    func ReadSinglemessageStatus(notification: NSNotification){
        
        
        guard let url = notification.object else {
            return // or throw
        }
        
        let blob = url as! NSDictionary // or as! Sting or as! Int
        if(blob.count>0){
            
            let taskid : String = self.themes.CheckNullValue(blob.objectForKey("task"))!
            
            if taskid == Message_details.taskid
            {
                let mesg : Message = tableArray.lastObject()
                mesg.status = MessageStatus.Read
                
                tableView.reloadData()
                
            }
        }
    }
    
    func ReadmessageStatus(notification: NSNotification){
        
        guard let url = notification.object else {
            return // or throw
        }
        
        let blob = url as! NSDictionary // or as! Sting or as! Int
        if(blob.count>0){
            
            let taskid : String = themes.CheckNullValue(blob.objectForKey("task"))!
            
            if taskid == Message_details.taskid
            {
                
                ChatDetailsArr  = NSMutableArray()
                textArray  = NSMutableArray()
                FromDetailArray = NSMutableArray()
                tasker_statusarray  = NSMutableArray()
                messgaeidarray  = NSMutableArray()
                self.tableArray = TableArray()
                gateway = MessageGateway()
                self.getDateArray = NSMutableArray()
                
                
                if blob.count > 0
                {
                    
                    let MessageDetails : NSMutableArray = blob.objectForKey("messages") as! NSMutableArray
                    
                    for data  in MessageDetails
                    {
                        self.textArray.addObject(self.themes.CheckNullValue(data.valueForKey("message"))!)
                        self.FromDetailArray.addObject(self.themes.CheckNullValue(data.valueForKey("from"))!)
                        self.tasker_statusarray.addObject(self.themes.CheckNullValue(data.valueForKey("tasker_status"))!)
                        self.messgaeidarray.addObject(self.themes.CheckNullValue(data.valueForKey("_id"))!)
                        self.getDateArray.addObject(self.themes.CheckNullValue(data.valueForKey("date"))!)
                    }
                    
                    Message_details.job_id=Message_details.taskid as String
                    
                    var k : Int
                    for k=0 ; k<self.textArray.count; k += 1
                    {
                        let mesg : Message = Message()
                        mesg.text = self.textArray.objectAtIndex(k) as! String;
                        
                        let text = self.getDateArray.objectAtIndex(k)  as! String
                        let types: NSTextCheckingType = .Date
                        var getdate = NSDate()
                        let detector = try? NSDataDetector(types: types.rawValue)
                        let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
                        
                        for match in matches {
                            getdate = (match.date!)
                        }
                        mesg.date = getdate
                        
                        
                        
                        if self.FromDetailArray.objectAtIndex(k) as! String == self.themes.getUserID()
                        {
                            mesg.sender = MessageSender.Myself;
                            if (self.tasker_statusarray.objectAtIndex(k) as! String == "2")
                            {
                                mesg.status = MessageStatus.Read
                            }
                            else if (self.tasker_statusarray.objectAtIndex(k) as! String == "1")
                            {
                                mesg.status = MessageStatus.Received
                            }
                            
                        }
                        else{
                            mesg.sender = MessageSender.Someone;
                            
                        }
                        
                        
                        self.tableArray.addObject(mesg)
                    }
                    
                    
                    self.tableView.reloadData()
                    self.tableViewScrollToBottomAnimated(true)
                    
                }
                
            }
            
        }
        
        
    }
    func showTypingStatus(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        
        
        let taskid : NSString = userInfo["taskid"]!
        
        
//        if (taskid == Message_details.taskid as String)
//        {
        
            
            
            Status_View.text = "\(themes.setLang("typing"))"
       // }
        
        
    }
    
    func stopTypingStatus(notification: NSNotification) {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        
        
        let taskid : NSString = userInfo["taskid"]!
        
        
        if (taskid == Message_details.taskid as String)
        {
            
            
            Status_View.text = ""
            
        }
        
    }
    
    
    
    
    
    func showPushMessage(notification: NSNotification) {
        let message_array = notification.object as! NSArray
        
        inputbar.resignFirstResponder()
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        
        
        let check_userid: NSString = userInfo["from"]!
        let check_taskid = userInfo["task"]
        let messgeid = userInfo["msgid"]
        
        let taskerstatus  = userInfo["taskerstus"]
        
        let Chatmessage:NSString! = userInfo["message"]
        let gettaskerid = userInfo["tasker_id"]
        
        let getdate = userInfo["date"]
        
        
        
        let text = getdate
        let types: NSTextCheckingType = .Date
        var getdatefromweb = NSDate()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matchesInString(text!, options: .ReportCompletion, range: NSMakeRange(0, text!.characters.count))
        
        for match in matches {
            getdatefromweb = (match.date!)
        }
        
        
        
        if messgaeidarray.containsObject(messgeid!)
        {
            
        }
        else
        {
            messgaeidarray.addObject(messgeid!)
            if check_taskid! == Message_details.taskid && gettaskerid! == Message_details.providerid
            {
                
                let mesg : Message = Message()
                
                
                if (check_userid == self.themes.getUserID())
                {
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Myself;
                    mesg.date = getdatefromweb
                    
                    
                    if (taskerstatus  == "2")
                    {
                        mesg.status = MessageStatus.Read
                    }
                    else if (taskerstatus  == "1")
                    {
                        mesg.status = MessageStatus.Received
                    }
                    
                    
                }
                else
                {
                    
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        
                        SocketIOManager.sharedInstance.sendingSinglemessagStatus(Message_details.taskid as String, taskerid: Message_details.providerid as String, Userid:self.themes.getUserID(),usertype:"user",messagearray:message_array)
                    }
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Someone;
                    mesg.date = getdatefromweb
                    
                    
                }
                self.tableArray.addObject(mesg)
                
                self.tableView.reloadData()
                
                self.tableViewScrollToBottomAnimated(true)
            }
            else{
                
                Message_details.taskid = check_taskid!
                Message_details.providerid = check_userid
                self.ReloadMessageView()
                
                
            }
        }
        
        
        
        
    }
    
    func showMessage(notification: NSNotification)
    {
        
        inputbar.resignFirstResponder()
        let message_array = notification.object as! NSArray
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        
        
        let check_userid: NSString = userInfo["from"]!
        let check_taskid = userInfo["task"]
        let messgeid = userInfo["msgid"]
        let taskerstatus  = userInfo["taskerstus"]
        let getdate = userInfo["date"]
        let gettaskerid = userInfo["tasker_id"]
        
        
        
        let text = getdate
        let types: NSTextCheckingType = .Date
        var getdatefromweb = NSDate()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matchesInString(text!, options: .ReportCompletion, range: NSMakeRange(0, text!.characters.count))
        
        for match in matches {
            getdatefromweb = (match.date!)
        }
        
        
        let Chatmessage:NSString! = userInfo["message"]
        
        if messgaeidarray.containsObject(messgeid!)
        {
            
        }
        else
        {
            messgaeidarray.addObject(messgeid!)
            if check_taskid! == Message_details.taskid && gettaskerid! == Message_details.providerid
            {
                
                let mesg : Message = Message()
                
                
                
                if (check_userid == self.themes.getUserID())
                {
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Myself;
                    //mesg.date = NSDate()
                    mesg.date = getdatefromweb
                    
                    if (taskerstatus  == "2")
                    {
                        mesg.status = MessageStatus.Read
                    }
                    else if (taskerstatus  == "1")
                    {
                        mesg.status = MessageStatus.Received
                    }
                    
                    
                }
                else
                {
                    
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Someone;
                    //mesg.date = NSDate()
                    mesg.date = getdatefromweb
                    
                    SocketIOManager.sharedInstance.sendingSinglemessagStatus(Message_details.taskid as String, taskerid: Message_details.providerid as String, Userid: themes.getUserID(),usertype:"user",messagearray:message_array)
                    
                }
                self.tableArray.addObject(mesg)
                
                self.tableView.reloadData()
                
                self.tableViewScrollToBottomAnimated(true)
            }
            else{
                let alertView = UNAlertView(title: Appname, message:"You Have A Message From The Provider")
                alertView.addButton(self.themes.setLang("ok"), action: {
                    Message_details.taskid = check_taskid!
                    Message_details.providerid = check_userid
                    self.ReloadMessageView()
                    
                    
                })
                alertView.show()
                
                
                
                
            }
        }
        
        
        
    }
    
    func   ReloadMessageView() {
        
        setInputbar()
        self.setTableView()
        
        ChatDetailsArr  = NSMutableArray()
        textArray  = NSMutableArray()
        FromDetailArray = NSMutableArray()
        tasker_statusarray  = NSMutableArray()
        messgaeidarray  = NSMutableArray()
        self.tableArray = TableArray()
        gateway = MessageGateway()
        self.getDateArray = NSMutableArray()
        
        GetDetails()
        
        setStatusView()
        SetImageView()
        setTest()
        
        
        
    }
    
    
    func setTest()
    {
        chat = Chat()
        chat.sender_name = "Player 1"
        chat.receiver_id = "12345"
        chat.sender_id = "54321"
        let texts: NSArray = []
        var last_message: Message? = nil
        for text in texts {
            let message: Message = Message()
            message.text = text as! String
            message.sender = .Someone
            message.status = .Received
            message.chat_id = chat.identifier()
            LocalStorage.sharedInstance().storeMessage(message)
            last_message = message
        }
        chat.numberOfUnreadMessages = texts.count
        if(last_message != nil)
        {
            chat.last_message = last_message!
        }
        
    }
    
    
    func textViewDidChange(textView: UITextView) {
        
        
        NSLog("the textview=%@",textView.text)
        
    }
    @IBAction func didClickOptions(sender: AnyObject) {
        if(sender.tag == 0)
        {
            self.navigationController?.popViewControllerAnimated(true)
            self.dismissViewControllerAnimated(true, completion: nil)
            
            
        }
    }
    
    func receiveTypestaus(notification:NSNotification)
    {
        
        
        type_str=notification.object as! NSString
        
        
        
        if(type_str == "Type")
        {
            typingLbl.hidden=false
            typingLbl.startAnimating()
            Status_View.hidden=true
            Status_Lbl.hidden=true
        }
        else if(type_str == "StopType")
        {
            typingLbl.hidden=true
            typingLbl.stopAnimating()
            Status_View.hidden=false
            Status_Lbl.hidden=false
            
            
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setInputbar()
    {
        self.inputbar.placeholder = nil;
        self.inputbar.delegate = self;
        self.inputbar.leftButtonImage = UIImage(named: "share")
        self.inputbar.rightButtonText = themes.setLang("send");
        self.inputbar.rightButtonTextColor = UIColor(red: 0, green: 124/255.0, blue: 1, alpha: 1)
    }
    
    func setTableView()
    {
        self.tableArray = TableArray()
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0,view.frame.size.width, 10.0))
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.registerClass(MessageCell.classForCoder(), forCellReuseIdentifier: "MessageCell")
        
    }
    func setGateway()
    {
        gateway = MessageGateway()
        gateway.delegate = self;
        gateway.chat = self.chat;
        gateway.loadOldMessages()
        
        
    }
    
    //TableView Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.tableArray.numberOfSections()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableArray.numberOfMessagesInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let CellIdentifier: String = "MessageCell"
        var cell: MessageCell! = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! MessageCell
        if (cell == nil) {
            cell = MessageCell(style: .Default, reuseIdentifier: CellIdentifier)
        }
        if(self.tableArray.objectAtIndexPath(indexPath) != nil){
            cell.message = self.tableArray.objectAtIndexPath(indexPath)
        }
        return cell
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.tableArray.titleForSection(section)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let message: Message = self.tableArray.objectAtIndexPath(indexPath)
        return message.heigh
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame: CGRect = CGRectMake(0, 0, tableView.frame.size.width, 40)
        let view: UIView = UIView(frame: frame)
        view.backgroundColor = UIColor.clearColor()
        view.autoresizingMask = .FlexibleWidth
        let label: UILabel = UILabel()
        label.text = self.tableArray.titleForSection(section)
        label.textAlignment = .Center
        label.font = UIFont(name: "Helvetica", size: 20.0)
        label.sizeToFit()
        label.center = view.center
        label.font = UIFont(name: "Helvetica", size: 13.0)
        label.backgroundColor = UIColor(red: 207 / 255.0, green: 220 / 255.0, blue: 252.0 / 255.0, alpha: 1)
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        label.autoresizingMask = .None
        view.addSubview(label)
        return view
        
    }
    func tableViewScrollToBottomAnimated(animated: Bool) {
        let numberOfSections:NSInteger=tableArray.numberOfSections()
        let numberOfRows:NSInteger=tableArray.numberOfMessagesInSection(numberOfSections-1)
        if(numberOfRows != 0)
        {
            tableView.scrollToRowAtIndexPath(tableArray.indexPathForLastMessage(), atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
        }
    }
    func inputbarDidPressRightButton(inputbar: Inputbar!) {
        
        if (inputbar.text() == "")
        {
            
        }
        else
            
        {
            
            
            SocketIOManager.sharedInstance.sendMessage(inputbar.text(), withNickname:themes.getUserID(), Providerid:Message_details.providerid as String , taskid:Message_details.taskid as String)
            
            //  self.GetDetails()
        }
        
        
    }
    
    func MessageFromIn(str: String) {
        
        let message: Message = Message()
        message.text = str
        message.date = NSDate()
        message.chat_id = "1"
        message.sender = .Someone
        
        //Store Message in memory
        self.tableArray.addObject(message)
        //Insert Message in UI
        do
        {
            
            try moveTable(message)
        }
        catch
        {
            print("there is an error")
        }
        //Send message to server
        // HI
        //[self.gateway sendMessage:message];
    }
    
    func moveTable (message:Message)throws
    {
        let indexPath: NSIndexPath =  tableArray.indexPathForMessage(message)
        self.tableView.beginUpdates()
        if self.tableArray.numberOfMessagesInSection(indexPath.section) == 1 {
            self.tableView.insertSections(NSIndexSet(index:indexPath.section), withRowAnimation: .None)
        }
        
        //        self.tableView.reloadData()
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        self.tableView.endUpdates()
        self.tableView.scrollToRowAtIndexPath(self.tableArray.indexPathForLastMessage(), atScrollPosition: .Bottom, animated: true)
        
    }
    func inputbarDidPressLeftButton(inputbar: Inputbar!) {
        //        let alertView: UIAlertView = UIAlertView(title: "Left Button Pressed", message: "", delegate: nil, cancelButtonTitle: "Ok")
        //        alertView.show()
        
    }
    func gatewayDidUpdateStatusForMessage(message: Message) {
        let indexPath: NSIndexPath = tableArray.indexPathForMessage(message)
        let cell: MessageCell = self.tableView.cellForRowAtIndexPath(indexPath) as! MessageCell
        cell.updateMessageStatus()
    }
    
    func gatewayDidReceiveMessages(array: [AnyObject]) {
        self.tableArray.addObjectsFromArray(array)
        self.tableView.reloadData()
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func inputbarDidBecomeFirstResponder(inputbar: Inputbar!) {
        
        
        
        SocketIOManager.sharedInstance.sendStartTypingMessage(self.themes.getUserID(), taskerid: Message_details.providerid as String,taskid: Message_details.taskid as String)
        
        
        
    }
    func inputbarTextEndEditingChat(inputbar: Inputbar!) {
        
        
        SocketIOManager.sharedInstance.sendStopTypingMessage(self.themes.getUserID(), taskerid: Message_details.providerid as String,taskid: Message_details.taskid as String)
        
        
        
    }
    
    func checkTyping()
    {
        
        let body = DDXMLElement.elementWithName("body") as! DDXMLElement
        body.setStringValue("StopType")
        let message_sent = DDXMLElement.elementWithName("message") as! DDXMLElement
        message_sent.addAttributeWithName("type", stringValue: "Typing")
        //        let Details=["job_id":"\(Message_details.job_id)"] as NSDictionary
        //        message_sent.addAttributeWithName("Detail_id", stringValue: "\(Details)")
        message_sent.addAttributeWithName("to", stringValue: "\(Message_details.id)\(constant.DomainName)")
        message_sent.addAttributeWithName("jobid", stringValue: Message_details.job_id as String)
        
        message_sent.addChild(body)
        App_Delegate.xmppStream.sendElement(message_sent)
        
        
        
        
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}








