//
//  ViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 30/09/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import CoreTelephony

//import SwiftyJSON
class SignUpViewController: RootViewController,UITextFieldDelegate,NIDropDownDelegate,UITextViewDelegate{
    
    @IBOutlet var checkbox: UIButton!
   
    @IBOutlet var termtextview: UITextView!
    @IBOutlet var firstname_textfield: UITextField!
    @IBOutlet var lastname_testfield: UITextField!
    @IBOutlet var Signup_ScrollView: UIScrollView!
    
    @IBOutlet var cnfirmPasswordTextfield: CustomTextField!
    @IBOutlet var Register_But: UIButton!
    @IBOutlet var referraltextfield: UITextField!
    
    @IBOutlet var ContactnumberTextfield: UITextField!
    @IBOutlet var EmailidTextfield: UITextField!
    @IBOutlet var PasswordTextfield: UITextField!
    @IBOutlet var FullnameTextfield: UITextField!

    @IBOutlet var Country_Code_TextField: CustomTextField!
    @IBOutlet weak var facebook_but: UIButton!
    @IBOutlet weak var lblRegister: CustomLabelWhite!
     var dropDown:NIDropDown!
  
    @IBOutlet var Gender: UIButton!
    var Address:NSString=NSString()
    var GenderArray:NSArray = NSArray()
    var FullName:NSString=NSString()
    var Emailid:NSString=NSString()
    var Contact:NSString=NSString()
    var Password:NSString=NSString()
    var validateemail:Bool=Bool()
    var validatepasswd : Bool = Bool()
    var URL_handler:URLhandler=URLhandler()
    var themes:Themes=Themes()
    var searchObj = SearchBarViewController()
    let facebookReadPermissions = ["public_profile", "email", "user_friends"]

    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        checkbox.setImage(UIImage.init(named:"unchecked_checkbox") , forState: .Normal)
        GenderArray = ["Male","Female","Others"]

        if(themes.getCounrtyphone() != ""){
            Country_Code_TextField.text = "+ \(themes.getCounrtyphone())"
        }
        EmailidTextfield.autocapitalizationType = .None;
        if(Device_Token == ""){
            Device_Token="Simulator Signup"
        }
        signup.selectedCode = ""
        //let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(SignUpViewController.DismissKeyboard(_:)))
        //view.addGestureRecognizer(tapgesture)
        
        //Tool Bar for Picker View
        
        
//        termtextview.text=themes.setLang("termtext")
        
        
//        if kLanguage == "en"{
        
        let str:NSMutableAttributedString = NSMutableAttributedString.init(string:"I have read and agreed to the Terms and Conditions & Privacy Policy.")
        str.addAttribute(NSLinkAttributeName, value:"1", range: NSRange(location:30,length:20))
        str.addAttribute(NSLinkAttributeName, value:"2", range: NSRange(location:53,length:14))
        
        termtextview.attributedText = str;
        termtextview.userInteractionEnabled = true
        termtextview.font = UIFont(name: "Roboto-Regular", size: 14)
        termtextview.textAlignment = .Center
        termtextview.textColor = UIColor.blackColor()
        termtextview.linkTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        termtextview.delegate = self
        termtextview.sizeToFit()
//        }
//        else{
//            let str:NSMutableAttributedString = NSMutableAttributedString.init(string:"விதிமுறைகள் மற்றும் நிபந்தனைகள் மற்றும் தனியுரிமைக் கொள்கையை நான் படித்து ஒப்புக்கொண்டேன்.")
//            str.addAttribute(NSLinkAttributeName, value:"1", range: NSRange(location:0,length:11))
//            str.addAttribute(NSLinkAttributeName, value:"2", range: NSRange(location:24,length:16))
//            
//            termtextview.attributedText = str;
//            termtextview.userInteractionEnabled = true
//            termtextview.font = UIFont(name: "Roboto-Regular", size: 14)
//            termtextview.textAlignment = .Center
//            termtextview.textColor = UIColor.blackColor()
//            termtextview.linkTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
//            termtextview.delegate = self
//            termtextview.sizeToFit()
//
//        }
//
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.frame.size.width, 25))
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = themes.ThemeColour()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title:themes.setLang("done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(SignUpViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        doneButton.tintColor=themes.ThemeColour()
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
       // self.Gender.setTitle("Gender", forState: .Normal)
       // self.Gender.titleLabel!.textColor = UIColor.grayColor()
        

        LoadPage()
    }
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
    
        
        
        let get_String:NSString =  textView.text
        
        print("get character range\(get_String.substringWithRange(NSMakeRange(characterRange.location, characterRange.length)))")
        let getchastring: String  = get_String.substringWithRange(NSMakeRange(characterRange.location, characterRange.length))
        
       
            let Controller:TermsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("Termvc") as! TermsViewController
            Controller.getterms = getchastring
            self.navigationController?.pushViewController(Controller, animated: true)
       
        
        return false
    }
    override func viewWillAppear(animated: Bool) {
        
        LoadPage()
    }
    override func viewDidAppear(animated: Bool) {
        Gender.hidden = false
        Gender.titleLabel?.textColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.3)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Function
    
    func LoadPage(){
        
        if(themes.getCounrtyphone() != ""){
            Country_Code_TextField.text = "+ \(themes.getCounrtyphone())"
       
        }
        if(signup.selectedCode != "") {
            let indexCode = themes.codename.indexOfObject(signup.selectedCode)
            Country_Code_TextField.text = themes.code[indexCode] as? String
        }
        OTP_sta.OTP_Paging="SignUp"
        EmailidTextfield.placeholder = themes.setLang("email_address")
        PasswordTextfield.placeholder = themes.setLang("password_placeholder")
        ContactnumberTextfield.placeholder = themes.setLang("phone_no")
        referraltextfield.placeholder = themes.setLang("referral_code")
        FullnameTextfield.placeholder = themes.setLang("user_name")
        cnfirmPasswordTextfield.placeholder = themes.setLang("confirm_password")
        
        firstname_textfield.placeholder =  themes.setLang("firstname")
        lastname_testfield.placeholder = themes.setLang("lastname")
//        Country_Code_TextField.placeholder = themes.setLang("country")
        

        facebook_but.setTitle(themes.setLang("facebook"), forState: UIControlState.Normal)
        Register_But.setTitle(themes.setLang("register"), forState: UIControlState.Normal)
        lblRegister.text = themes.setLang("register")
        Gender.setTitle(themes.setLang("gender"), forState: UIControlState.Normal)
        EmailidTextfield.isMandatory()
        PasswordTextfield.isMandatory()
        ContactnumberTextfield.isMandatory()
        FullnameTextfield.isMandatory()
        firstname_textfield.isMandatory()
        lastname_testfield.isMandatory()
        cnfirmPasswordTextfield.isMandatory()
        Signup_ScrollView.contentSize.height = Register_But.layer.frame.origin.y+Register_But.frame.height+5
        
    }
    
    func nextPressed(){
        referraltextfield.becomeFirstResponder()
    }
    
    
    func showPicker(){
//        view.addSubview(self.Picker_Wrapper)
//        UIView.animateWithDuration(0.2, animations: {
//            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height - 260.0, UIScreen.mainScreen().bounds.size.width, 260.0)
//            } , completion: { _ in
//        })
    }
    @IBAction func didclickterm(sender: AnyObject) {
        
        if(checkbox.selected == true)
        {
            checkbox.selected = false
            checkbox.setImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
        }
        else
        {
            checkbox.selected = true
            checkbox.setImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Normal)
        }
        
        
    }
    
    func donePicker() {
//        UIView.animateWithDuration(0.2, animations: {
//            self.Picker_Wrapper.frame = CGRectMake(0, UIScreen.mainScreen().bounds.size.height, UIScreen.mainScreen().bounds.size.width, 260.0)
//            }, completion: { _ in
//                self.Picker_Wrapper.removeFromSuperview()
//        })
    }
    
    func DismissKeyboard(sender:UITapGestureRecognizer) {
        self.donePicker()
        view.endEditing(true)
    }
    
    func register() -> Void  {
        FullName=FullnameTextfield.text!
        Emailid=EmailidTextfield.text!
        Contact=ContactnumberTextfield.text!
        Password=PasswordTextfield.text!
        validateemail=themes.isValidEmail(Emailid as String)
        validatepasswd = themes.validpasword(Password)
        view.endEditing(true)
        if (firstname_textfield.text == ""){
            themes.AlertView("\(Appname)",Message: self.themes.setLang("Kindly enter the First name"),ButtonTitle: self.themes.setLang("ok"))
            return
        }
        else if (firstname_textfield.text?.characters.count >= 25){
       
            themes.AlertView("\(Appname)",Message: self.themes.setLang("Please enter the Firstname below 25 character"),ButtonTitle: self.themes.setLang("ok"))
            return
        }
        else if (lastname_testfield.text == ""){
            themes.AlertView("\(Appname)",Message: self.themes.setLang("Kindly enter the Lastname"),ButtonTitle: self.themes.setLang("ok"))
        }
        else if (firstname_textfield.text?.characters.count >= 25){
            themes.AlertView("\(Appname)",Message: self.themes.setLang("Please enter the Lastname below 25 character"),ButtonTitle: self.themes.setLang("ok"))
        }else if(EmailidTextfield.text == "") {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_emailid"),ButtonTitle: kOk)
        }else if(validateemail == false){
            themes.AlertView("\(Appname)",Message: themes.setLang("valid_email_alert"),ButtonTitle: kOk)
        }else if(Password.length < 6 ){
            themes.AlertView("\(Appname)",Message: themes.setLang("valid_password"),ButtonTitle: kOk)
        }else if (Password != cnfirmPasswordTextfield.text){
            themes.AlertView("\(Appname)",Message: themes.setLang("passwd_match_error"),ButtonTitle: kOk)

        }
            
        else if(FullnameTextfield.text == "") {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_username"),ButtonTitle: kOk)
        }
        else  if(FullName.length >= 25) {
            themes.AlertView("\(Appname)",Message: themes.setLang("username_below_30"),ButtonTitle: kOk)
        }else if(Country_Code_TextField.text == "") {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_country_code"),ButtonTitle: kOk)
        }else if(ContactnumberTextfield.text == ""){
            themes.AlertView("\(Appname)",Message:themes.setLang("enter_the_num"),ButtonTitle: kOk)
            return
        
        } else if(Contact.length >= 15 || Contact.length < 7)  {
            themes.AlertView("\(Appname)",Message: themes.setLang("enter_the_validnum"),ButtonTitle: kOk)
        }
//        if (Gender.titleLabel?.text == "Gender"){
//            themes.AlertView("\(Appname)",Message: "Kindly select the Gender",ButtonTitle: self.themes.setLang("ok"))
//        }
        else  if(checkbox.selected == false)
        {
            themes.AlertView("\(Appname)", Message: self.themes.setLang("Kindly accept the terms and conditions"), ButtonTitle: self.themes.setLang("ok"))
        }
        
            
        else {
            Register_But.enabled=false
            self.showProgress()
            let parameter=["first_name":"\(firstname_textfield.text!)","last_name":"\(lastname_testfield.text!)","user_name":"\(FullName)","gender":"\(Gender.titleLabel!.text)","email":"\(Emailid)","password":"\(Password)","country_code":"\(Country_Code_TextField.text!)","phone_number":"\(Contact)","referal_code":"\(referraltextfield.text!)","deviceToken":"\(Device_Token)","gcm_id":""]
            URL_handler.makeCall(constant.RegisterAccount, param: parameter, completionHandler: { (responseObject, error) -> () in
                self.Register_But.enabled=true
                self.DismissProgress()
                if(error != nil){
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                } else {
                    if(responseObject != nil){
                        let dict:NSDictionary=responseObject!
                        signup.status = self.themes.CheckNullValue(dict.objectForKey("status"))!
                        if (signup.status == "1"){
                            signup.firstname=self.firstname_textfield.text!
                            signup.lastname=self.lastname_testfield.text!
                            
                            signup.username=dict.objectForKey("user_name") as! NSString
                            signup.Email=dict.objectForKey("email") as! NSString
                            signup.Password=self.PasswordTextfield.text!
                            signup.Contact_num=dict.objectForKey("phone_number") as! NSString
                            signup.OTP=dict.objectForKey("otp") as! NSString
                            signup.otpstatus=dict.objectForKey("otp_status") as! NSString
                            signup.Country_Code=dict.objectForKey("country_code") as! NSString
                            signup.referralCode=self.referraltextfield.text!
                            signup.gender = self.Gender.titleLabel!.text!
                            if(signup.OTP.length>0){
                                OTP_sta.OTP=signup.OTP
                                OTP_sta.OTP_Status=signup.otpstatus
                                let otpview : OTPViewController = OTPViewController()
                                otpview.otpstring = "\(self.themes.CheckNullValue(dict.objectForKey("otp"))!)"
                                otpview.otpstatus_str = self.themes.CheckNullValue(dict.objectForKey("otp_status"))!
                                self.performSegueWithIdentifier("OTP", sender: nil)
                            }
                        } else {
                            if(dict.objectForKey("response") != nil) {
                                signup.message = dict.objectForKey("response") as! NSString
                                self.themes.AlertView("\(Appname)",Message: "\(signup.message)",ButtonTitle: kOk)
                                
                            } else {
                                signup.message = dict.objectForKey("errors") as! NSString
                                self.themes.AlertView("\(Appname)",Message: "\(signup.message)",ButtonTitle: kOk)
                                
                            }
                        }
                    }else {
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    }
                }
                return
            })
        }
    }
    
    func endEditingNow(){
        ContactnumberTextfield.endEditing(true)
    }
    
    func returnUserData() {
        Register_But.enabled=false
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil) {
                
            } else{
                
                let json = JSON(result!)
                print("the rres is \(json)")
                let userName : NSString = self.themes.CheckNullValue(result.valueForKey("name"))!
                let userEmail : NSString = self.themes.CheckNullValue(result.valueForKey("email"))!
                let firstName : NSString = self.themes.CheckNullValue(result.valueForKey("first_name"))!
                let lastName : NSString = self.themes.CheckNullValue(result.valueForKey("last_name"))!
                let userID : NSString = self.themes.CheckNullValue(result.valueForKey("id"))!
                let Profie_Pic:NSString?=json["picture"]["data"]["url"].string!
                let Pic_Status:NSString?=json["picture"]["data"]["url"].string!
                
                if(Profie_Pic != nil) {
                    FB_Regis.FB_Picture=Profie_Pic!
                }
                if(Pic_Status != nil){
                    FB_Regis.FB_Pic_Status=Pic_Status!
                }
                
                FB_Regis.FB_Firstname=firstName
                FB_Regis.FB_lastname=lastName
                FB_Regis.FB_Username=userName
                FB_Regis.FB_mailid=userEmail
                FB_Regis.FB_UserId=userID
                let param=["email_id":"\(FB_Regis.FB_mailid)","deviceToken":"\(Device_Token)","fb_id":"\(FB_Regis.FB_UserId)","prof_pic":"\(FB_Regis.FB_Picture)"]
                self.showProgress()
                self.URL_handler.makeCall(constant.Social_login, param: param, completionHandler:{ (responseObject, error) -> () in
                    self.Register_But.enabled=true
                    self.DismissProgress()
                    if(error != nil){
                        self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    } else {
                        if(responseObject != nil) {
                            let dict:NSDictionary=responseObject!
                            signup.status=self.themes.CheckNullValue(dict.objectForKey("status"))!
                            if(signup.status == "0") {
                                let messagestr = self.themes.CheckNullValue(dict.objectForKey("message"))!
                                if messagestr == "Your account is currently unavailable" {
                                    self.performSegueWithIdentifier("FacebookVC", sender: nil)
                                }
                            }
                            
                            if (signup.status == "1") {
                                signup.Check_Live=dict.objectForKey("is_alive_other") as! NSString
                                if(signup.Check_Live == "Yes") {
                                    self.themes.AlertView("\(Appname)", Message:self.themes.setLang("sign_out_all_device"), ButtonTitle: kOk)
                                }
                                
                                signup.username=dict.objectForKey("user_name") as! NSString
                                signup.Email=dict.objectForKey("email") as! NSString
                                signup.Contact_num=self.themes.CheckNullValue(dict.objectForKey("phone_number"))!
                                signup.currency=dict.objectForKey("currency") as! NSString
                                signup.Walletamt = self.themes.CheckNullValue(dict.objectForKey("wallet_amount"))!
                                signup.Userimage=dict.objectForKey("prof_pic") as! NSString
                                signup.Userid=dict.objectForKey("user_id") as! NSString
                                signup.Locationname=dict.objectForKey("location_name") as! NSString
                                signup.Country_Code=self.themes.CheckNullValue(dict.objectForKey("country_code"))!
                                signup.currency_Sym=self.themes.Currency_Symbol(signup.currency as String)
                                // signup.soc_key=dict.objectForKey("soc_key") as! NSString
                                signup.user_id=dict.objectForKey("user_id") as! NSString
                                
                                if (dict.objectForKey("location_id") != nil){
                                    signup.Locationid=dict.objectForKey("location_id") as! NSString
                                }
                                
                                self.themes.saveCountryCode(signup.Country_Code as String!)
                                self.themes.saveLocationname(signup.Locationname as String)
                                self.themes.saveLocationname(signup.Locationname as String)
                                self.themes.saveCurrency(signup.Walletamt as String)
                                self.themes.saveCurrencyCode(signup.currency_Sym as String)
                                self.themes.saveUserID(signup.Userid as String)
                                self.themes.saveUserName(signup.username as String)
                                self.themes.saveEmailID(signup.Email as String)
                                self.themes.saveUserPasswd(signup.Password as String)
                                self.themes.saveuserDP(signup.Userimage as String)
                                self.themes.saveMobileNum(signup.Contact_num as String)
                                self.themes.saveWalletAmt(signup.Walletamt as String)
                                self.themes.saveLocationID(signup.Locationid as String)
                                self.themes.saveJaberID(signup.user_id as String)
                                self.themes.saveJaberPassword(signup.soc_key as String)
                                SocketIOManager.sharedInstance.establishConnection()
                                SocketIOManager.sharedInstance.establishChatConnection()
                                
                                if self.themes.getaddresssegue() ==  "1" {
                                    self.performSegueWithIdentifier("ScheduleVC", sender: nil)
                                    
                                } else {
                                    Appdel.MakeRootVc("RootVCID")
                                }
                            }
                            if(signup.status == "2") {
                                self.performSegueWithIdentifier("FacebookVC", sender: nil)
                            }
                            
                        }  else {
                            self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                            
                        }
                    }
                    
                    
                })
            }
        })
    }
    
    
    
    
    //MARK: - TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == EmailidTextfield){
            EmailidTextfield.resignFirstResponder()
            PasswordTextfield.becomeFirstResponder()
        }
        if(textField == PasswordTextfield) {
            PasswordTextfield.resignFirstResponder()
            FullnameTextfield.becomeFirstResponder()
        }
        if(textField == FullnameTextfield) {
            FullnameTextfield.resignFirstResponder()
            Country_Code_TextField.becomeFirstResponder()
        }
        if(textField == ContactnumberTextfield) {
            ContactnumberTextfield.resignFirstResponder()
            referraltextfield.becomeFirstResponder()

        }
       

        if(textField == referraltextfield){
            referraltextfield.resignFirstResponder()
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if(textField == EmailidTextfield) {
            self.donePicker()
        }
        if(textField == ContactnumberTextfield)  {
            self.donePicker()
            let toolBar = UIToolbar(frame: CGRectMake(0, 0, view.frame.size.width, 25))
            toolBar.barStyle = UIBarStyle.Default
            toolBar.translucent = true
            toolBar.tintColor = themes.ThemeColour()
            toolBar.sizeToFit()
            let doneButton = UIBarButtonItem(title:themes.setLang("done"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(SignUpViewController.endEditingNow))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            doneButton.tintColor=themes.ThemeColour()
            toolBar.setItems([spaceButton, doneButton], animated: false)
            toolBar.userInteractionEnabled = true
            textField.inputAccessoryView = toolBar
            return true
            
        }
        if(textField == Country_Code_TextField) {
            view.endEditing(true)
            let navig = self.storyboard?.instantiateViewControllerWithIdentifier("SearchBarViewControllerID") as! SearchBarViewController
            self.navigationController?.pushViewController(navig, animated: true)
            return false
        }
        if(textField == referraltextfield){
            self.donePicker()
        }
        
        return true
    }
    
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == FullnameTextfield) {
            let aSet = NSCharacterSet(charactersInString: ACCEPTABLE_CHARACTERS).invertedSet
            let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            return string == numberFiltered
            
        }
       else if textField == EmailidTextfield {
            if let _ = string.rangeOfCharacterFromSet(NSCharacterSet.uppercaseLetterCharacterSet()) {
                return false
            }
            return true
        } else {
            return true
        }
    }
    
    //MARK: - Button Action
    
    @IBAction func clickoption(sender: AnyObject) {
        themes.AlertView(themes.setLang("referral_scheme"),Message:themes.setLang("enter_referal_code"),ButtonTitle: kOk)
        
    }
    
    @IBAction func didClickoptions(sender: AnyObject) {
        if(sender.tag == 3) {
            self.register()
        }
        if(sender.tag == 4){
            self.donePicker()
            if(PasswordTextfield.text == "")  {
                PasswordTextfield.becomeFirstResponder()
            }
        }
        if(sender.tag == 122) {
            if self.navigationController!.viewControllers.contains(SignInViewController()) {
                self.navigationController?.popToViewController(SignInViewController() as UIViewController, animated: true)
            } else{
                self.performSegueWithIdentifier("Signin_VC", sender: nil)
            }
        }
        if(sender.tag == 12) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func didclickfacebook(sender: AnyObject) {
        let login:FBSDKLoginManager = FBSDKLoginManager()
        login.logOut()
        login.logInWithReadPermissions(facebookReadPermissions,
                                       fromViewController:self ,handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
                                        if (error == nil){
                                            self.returnUserData()
                                        } else{
                                            let loginManager = FBSDKLoginManager()
                                            loginManager.logOut()
                                        }
        })
    }
    
    
    @IBAction func SelectGender(sender: AnyObject){
  
        if GenderArray.count>0
        {
           var float:CGFloat = 42 * CGFloat(self.GenderArray.count)
           dropDown = NIDropDown.init()
           dropDown.showDropDown(sender as! UIButton, &float, self.GenderArray as [AnyObject], [], "down")
           dropDown.delegate = self
        }
    }
    func  niDropDownDelegateMethod(sender: NIDropDown!) {
        print("get gender\(Gender.titleLabel?.text)")
//        Gender.titleLabel?.textColor = UIColor.whiteColor()
//         Gender.titleLabel?.tintColor = UIColor.whiteColor()
    
    }
    //MARK: - Picker View Delegate
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return themes.codename.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (themes.codename[row] as! String)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.Country_Code_TextField.text="\(themes.code[row])"
        
    }
    

}










