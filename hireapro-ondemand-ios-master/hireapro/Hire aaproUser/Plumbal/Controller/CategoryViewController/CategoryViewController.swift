//
//  CategoryViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 05/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import Alamofire


class CategoryViewController: RootViewController,UITableViewDelegate,UIScrollViewDelegate {
    
    @IBOutlet var Category_tableView: UITableView!
    @IBOutlet var Continue_Btn: CustomButton!
    @IBOutlet var Category_name: UILabel!
    @IBOutlet var Back_But: UIButton!
    @IBOutlet var headerView: UIView!
    
    var Category_ImageArray:NSMutableArray=NSMutableArray()
    var imageRect: CGRect=CGRect()
    var Category_ListArray:NSMutableArray=NSMutableArray()
    var Category_List_idArray:NSMutableArray=NSMutableArray()
    var Category_List_imageArray:NSMutableArray=NSMutableArray()
    var Checkmarkindex:NSString=NSString()
    var Check_ChildArray:NSMutableArray=NSMutableArray()
    var block: SDWebImageCompletionBlock!
    let themes:Themes=Themes()
    let URL_handler:URLhandler=URLhandler()
    let imageview:UIImageView=UIImageView()
    var refreshControl:UIRefreshControl=UIRefreshControl()
    var Parallax_HeaderView:ParallaxHeaderView=ParallaxHeaderView()
    
    //MARK: Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Continue_Btn.setTitle(themes.setLang("continue"), forState: UIControlState.Normal)

        themes.Back_ImageView.image=UIImage(named: "Back")
        Back_But.addSubview(themes.Back_ImageView)
        Category_name.text="\(Home_Data.Category_name)"
        themes.Back_ImageView.image=UIImage(named: "Back_White")
        let nibName = UINib(nibName: "CategoryTableViewCell", bundle:nil)
        Category_tableView.registerNib(nibName, forCellReuseIdentifier: "Category_Cell")
        Category_tableView.separatorColor=themes.Lightgray()
        block = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            if(image != nil) {
                self.Parallax_HeaderView = ParallaxHeaderView.parallaxHeaderViewWithImage(image, forSize: CGSizeMake(self.Category_tableView.frame.size.width, 150)) as! ParallaxHeaderView
                self.Category_tableView.tableHeaderView=self.Parallax_HeaderView
                let header: ParallaxHeaderView = self.Category_tableView.tableHeaderView as! ParallaxHeaderView
                header.refreshBlurViewForNewImage()
                self.Category_tableView.tableHeaderView = header
            }
        }
        self.Category_tableView.tableFooterView=UIView()
        self.showProgress()
        Category_feed()
        configurePulltorefresh()
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    //MARK: Function
    
    func settablebackground() {
        let nibView = NSBundle.mainBundle().loadNibNamed("RefreshView", owner: self, options: nil)[0] as! UIView
        nibView.frame = self.Category_tableView.bounds;
        self.Category_tableView.backgroundView=nibView
        
    }
    
    func configurePulltorefresh(){
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.addTarget(self, action: #selector(CategoryViewController.Category_feed), forControlEvents: UIControlEvents.ValueChanged)
        self.Category_tableView.addSubview(refreshControl)
    }
    
    func animateTable() {
        Category_tableView.reloadData()
        let cells = Category_tableView.visibleCells
        let tableHeight: CGFloat = Category_tableView.bounds.size.height
        for i in cells {
            let cell: CategoryTableViewCell = i as! CategoryTableViewCell
            cell.Category_ImageView.transform = CGAffineTransformMakeTranslation(0, tableHeight)
            cell.TitleLabel.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: CategoryTableViewCell = a as! CategoryTableViewCell
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionNone, animations: {
                cell.Category_ImageView.transform = CGAffineTransformMakeTranslation(0, 0)
                cell.TitleLabel.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: nil)
            index += 1
        }
    }
    
    func applicationLanguageChangeNotification(notification:NSNotification){
        Continue_Btn.setTitle(themes.setLang("continue"), forState: UIControlState.Normal)
    }
    
    func Category_feed() {
        let param=["category":"\(Home_Data.Category_id)", "location_id":"\(themes.getLocationID())"]
        Continue_Btn.enabled=false
        URL_handler.makeCall("\(constant.Get_SubCategories)", param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            self.Continue_Btn.enabled=true
            self.Category_tableView.hidden=false
            self.refreshControl.endRefreshing()
            if(error != nil) {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                self.settablebackground()
                if(self.Category_List_idArray.count == 0){
                    self.Continue_Btn.hidden=true
                    self.Parallax_HeaderView.hidden=true
                }
            }
            else {
                if(responseObject != nil) {
                    let dict:NSDictionary=responseObject!
                    let Status:NSString=self.themes.CheckNullValue(dict.objectForKey("status"))!
                    if(Status == "1") {
                        if(self.Category_List_idArray.count != 0){
                            self.Category_List_idArray.removeAllObjects()
                            self.Category_List_imageArray.removeAllObjects()
                            self.Category_List_imageArray.removeAllObjects()
                            self.Category_ListArray.removeAllObjects()
                            self.Check_ChildArray.removeAllObjects()
                        }
                        self.Continue_Btn.hidden=false
                        let CategoryArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("category") as! NSArray
                        for Dictionary in CategoryArray{
                            let categoryid:NSString=Dictionary.objectForKey("cat_id") as! NSString
                            self.Category_List_idArray.addObject(categoryid)
                            let categoryimage:NSString = self.themes.CheckNullValue(Dictionary.objectForKey("image"))!
                            self.Category_List_imageArray.addObject(categoryimage)
                            let categoryname:NSString=Dictionary.objectForKey("cat_name") as! NSString
                            self.Category_ListArray.addObject(categoryname)
                            let Check_Child:NSString=Dictionary.objectForKey("hasChild") as! NSString
                            self.Check_ChildArray.addObject(Check_Child)
                            
                        }
                        self.Category_tableView.reloadData()
                        self.animateTable()
                        self.imageview.sd_setImageWithURL(NSURL(string: "\(Home_Data.Category_image)"), completed: self.block)
                        self.Parallax_HeaderView.hidden=false
                    }   else {
                        if(self.Category_List_idArray.count == 0){
                            self.Continue_Btn.hidden=true
                            self.Parallax_HeaderView.hidden=true
                        }
                        self.themes.AlertView("\(Appname)", Message: self.themes.setLang("no_category"), ButtonTitle: kOk)
                        self.settablebackground()
                    }
                }else{
                    if(self.Category_List_idArray.count == 0){
                        self.Continue_Btn.hidden=true
                        self.Parallax_HeaderView.hidden=true
                    }
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: Appname)
                    self.settablebackground()
                }
            }
        }
    }
    
    //MARK: - TableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height_Cell:CGFloat = self.themes.calculateHeightForString("\(Category_ListArray.objectAtIndex(indexPath.row))")
        return height_Cell+50.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Category_List_idArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell:CategoryTableViewCell = tableView.dequeueReusableCellWithIdentifier("Category_Cell") as! CategoryTableViewCell
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        if(Checkmarkindex == "\(indexPath.row)"){
            Cell.Checkmark_Image_View.hidden=false
        }else {
            Cell.Checkmark_Image_View.hidden=true
        }
        
        Cell.TitleLabel.text="\(Category_ListArray.objectAtIndex(indexPath.row))"
        Cell.Category_ImageView.layer.cornerRadius=Cell.Category_ImageView.frame.size.width/2
        Cell.Category_ImageView.clipsToBounds=true
        Cell.Category_ImageView.sd_setImageWithURL(NSURL(string: "\(Category_List_imageArray.objectAtIndex(indexPath.row))"), completed: themes.block)
        
        return Cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Checkmarkindex="\(indexPath.row)"
        self.Category_tableView.reloadData()
        Category_Data.CategoryID="\(Category_List_idArray[indexPath.row])"
        let Prefcategory : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        Prefcategory.setObject((Category_List_idArray[indexPath.row]), forKey:"maincategory")
        if(Check_ChildArray.objectAtIndex(indexPath.row) as! NSString == "Yes") {
            Home_Data.Category_name="\(Category_ListArray.objectAtIndex(indexPath.row))"
            Home_Data.Category_id="\("\(Category_List_idArray[indexPath.row])")"
            let Controller:CategoryViewController=self.storyboard?.instantiateViewControllerWithIdentifier("Category1") as! CategoryViewController
            self.navigationController?.pushViewController(Controller, animated: true)
        }
    }
    
    
    //MARK: ScrollView Delegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(imageview.image != nil) {
            if scrollView == self.Category_tableView {
                let header: ParallaxHeaderView = self.Category_tableView.tableHeaderView as! ParallaxHeaderView
                header.layoutHeaderViewForScrollViewOffset(scrollView.contentOffset)
                self.Category_tableView.tableHeaderView = header
            }
        }
    }
    
    //MARK: - Button Action
    
    @IBAction func didCiickOption(sender: AnyObject) {
        if(sender.tag == 0) {
            self.navigationController?.popViewControllerAnimated(true)
        }
        if(sender.tag == 1) {
            if(Checkmarkindex != ""){
                self.performSegueWithIdentifier("ScheduleVC", sender: nil)
            } else {
                themes.AlertView("\(Appname)", Message: themes.setLang("choose_category"), ButtonTitle: kOk)
            }
        }
    }
    
}
