//
//  SearchBarViewController.swift
//  Plumbal
//
//  Created by Casperon iOS on 15/12/2016.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit



class SearchBarViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var countryTable: UITableView!
    @IBOutlet weak var countrySearchController: UISearchBar!
    
    var searchArray = [String]()
    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = PlumberThemeColor
        countryTable.delegate = self
        countryTable.dataSource = self
        countryTable.reloadData()
        
        countrySearchController.showsCancelButton = true
        countrySearchController.text = ""
        countrySearchController.searchResultsButtonSelected = true
        countrySearchController.delegate = self
        let cancelButtonAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [String : AnyObject], forState: UIControlState.Normal)
        countrySearchController.barTintColor = PlumberThemeColor
        for subView in countrySearchController.subviews {
            for secondLevelSubview in subView.subviews{
                if (secondLevelSubview.isKindOfClass(UITextField)) {
                    if let searchBarTextField:UITextField = secondLevelSubview as? UITextField  {
                        searchBarTextField.becomeFirstResponder()
                        searchBarTextField.placeholder = "Search"
                        searchBarTextField.textColor = PlumberThemeColor
                        break;
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(countrySearchController.text != ""){
            return searchArray.count
        }else{
            return themes.codename.count;
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = countryTable.dequeueReusableCellWithIdentifier("Cell") as! SearchBarTableViewCell
        cell.textLabel?.text = ""
      //  var font = UIFont.init(name: plumberMediumFont, size: <#T##CGFloat#>)
        cell.textLabel?.font = PlumberMediumFont
        cell.textLabel?.attributedText = NSAttributedString(string: "")
        
        if(countrySearchController.text != ""){
           cell.configureCellWith(searchTerm:countrySearchController.text!, cellText: searchArray[indexPath.row])
            return cell
        }else{
            cell.textLabel?.text! = themes.codename[indexPath.row] as! String
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        if(searchArray.count == 0){
            countrySearchController.text = themes.codename[indexPath.row] as? String
            signup.selectedCode = themes.codename[indexPath.row] as! String

        }else{
            countrySearchController.text = searchArray[indexPath.row]
            signup.selectedCode = searchArray[indexPath.row]

        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - SearchBar Delegate
    
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        searchArray.removeAll(keepCapacity: false)
        
        
        let NewText = (searchBar.text! as NSString).stringByReplacingCharactersInRange(range, withString:text)
        print(NewText);
         let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
        var searchString = String()
        (NewText as String).enumerateSubstringsInRange(range, options: .ByComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
            searchString.appendContentsOf(substring!)
            searchString.appendContentsOf("*")
        })
        let searchPredicate = NSPredicate(format: "SELF LIKE[c] %@", searchString)
        let array = (themes.codename as NSArray).filteredArrayUsingPredicate(searchPredicate)
        searchArray = array as! [String]
        countryTable.reloadData()
        return true
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
