//
//  PopupFilterViewController.swift
//  Plumbal
//
//  Created by Casperon on 31/08/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit
import CoreGraphics
protocol PopupFilterViewControllerDelegate {
    
    func pressedCancel(sender: PopupFilterViewController)
    
    func  passParametres(RatingValue:CGFloat,Priceval: NSString,MaxPrice: NSString)
    
    
    
   
    
    
}


class PopupFilterViewController: UIViewController,TPFloatRatingViewDelegate {
    
     var delegate:PopupFilterViewControllerDelegate?
    var ctg_type:String!
    var minimumpriceval : NSString!
    var maximumpriceval : NSString!
    @IBOutlet var upperLabel: UILabel!
    @IBOutlet var lowerLabel: UILabel!
    @IBOutlet weak var price_slider: UISlider!
    @IBOutlet weak var cancelbtn: UIButton!
    @IBOutlet weak var applybtn: UIButton!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblHourly: UILabel!
    @IBOutlet weak var lblSorting: UILabel!

    @IBOutlet var lableSlider: NMRangeSlider!
    @IBOutlet weak var RatingView: TPFloatRatingView!
    var Ratcount: CGFloat = CGFloat()
    @IBAction func sliderValueChanged(sender: AnyObject) {
        // self.updateSliderLabels()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if ctg_type == "Flat Rate"{
            
        
        lblHourly.text = themes.setLang("flat_Rating")
        }
        else{
            lblHourly.text = themes.setLang("hourly_pricing")
            
        }
        lblRating.text = themes.setLang("rating")
        lblSorting.text = themes.setLang("sorting")
        applybtn.setTitle(themes.setLang("apply"), forState: UIControlState.Normal)

        RatingView.emptySelectedImage = UIImage(named: "Star")
        RatingView.fullSelectedImage = UIImage(named: "StarSelected")
        RatingView.contentMode = UIViewContentMode.ScaleAspectFill
        RatingView.maxRating = 5
        RatingView.minRating = 0
        RatingView.rating = 0
        RatingView.editable = true;
        RatingView.halfRatings = false;
        RatingView.floatRatings = false;
        RatingView.tintColor = UIColor(red:232.0/255.0, green:101.0/255.0, blue:7.0/255.0, alpha:1.0)
      
        RatingView.delegate=self

        self.configureLabelSlider()
      //  self.updateSliderLabels()

        // Do any additional setup after loading the view.
    }
    func updateSliderLabels() {
        var lowerCenter: CGPoint = CGPoint()
        lowerCenter.x = (self.lableSlider.lowerCenter.x + self.lableSlider.frame.origin.x)
        lowerCenter.y = (self.lableSlider.center.y - 30.0)
        self.lowerLabel.center = lowerCenter
        self.lowerLabel.text! = "\(Int(self.lableSlider.lowerValue))"
        var upperCenter: CGPoint = CGPoint()
        upperCenter.x = (self.lableSlider.upperCenter.x + self.lableSlider.frame.origin.x)
        upperCenter.y = (self.lableSlider.center.y - 30.0)
        self.upperLabel.center = upperCenter
        self.upperLabel.text! = "\(Int(self.lableSlider.upperValue))"

        
        
    }

    override func viewDidAppear(animated: Bool) {
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func configureLabelSlider() {
        
        NSLog ("minimum price=%f and maximum price =%f",minimumpriceval.floatValue,maximumpriceval.floatValue)
        self.lableSlider.minimumValue = 0;
        self.lableSlider.maximumValue = maximumpriceval.floatValue;
        
        self.lableSlider.lowerValue = 0;
        self.lableSlider.upperValue = maximumpriceval.floatValue;
        self.upperLabel.text! = "\(Int(maximumpriceval.floatValue))"

        
        
        self.lableSlider.minimumRange = 5;
    }
    @IBAction func slidervaluechange(sender: NMRangeSlider) {
        
        self.updateSliderLabels()
    }
           @IBAction func didclickoption(sender: AnyObject) {
        if sender.tag == 0
        {
            self.Callcanceldelegate()
        }
else if sender.tag == 1
{
   // NSLog("Get maximum value=%@", "\(Int(self.lableSlider.upperValue))")
    self.delegate?.passParametres(Ratcount, Priceval:String( "\(Int(self.lableSlider.lowerValue))"),MaxPrice: String( "\(Int(self.lableSlider.upperValue))") )
    self.delegate?.pressedCancel(self)
}
        
    }
    func Callcanceldelegate()
    {
        self.delegate?.pressedCancel(self)
    }
    
    func floatRatingView(ratingView: TPFloatRatingView, ratingDidChange rating: CGFloat) {
        
        print("the item changed is \(rating)....\(ratingView.tag)")
        
        //        RatingArray.replaceObjectAtIndex(ratingView.tag, withObject: "\(rating)")
        
    }
    
    func floatRatingView(ratingView: TPFloatRatingView, continuousRating rating: CGFloat) {
        Ratcount = rating
     //   RatingArray.replaceObjectAtIndex(ratingView.tag, withObject: "\(rating)")
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
