//
//  PlumberCustomColor.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

let PlumberThemeColor = UIColor(red: 248/255.0, green: 130/255.0, blue: 4/255.0, alpha: 1)
let PlumberLightThemeColor = UIColor(red: 248/255.0, green: 130/255.0, blue: 4/255.0, alpha: 0.75)
let PlumberBackGroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
let PlumberLightGrayColor = UIColor(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1)
let PlumberGreenColor = UIColor(red: 46/255.0, green: 204/255.0, blue: 113/255.0, alpha: 1)
let PlumberBlueColor = UIColor(red: 40/255.0, green: 203/255.0, blue: 249/255.0, alpha: 1)
let plumberPlaceHolderColor = UIColor(red:213.0/255.0, green:212.0/255.0, blue:210.0/255.0, alpha: 1.0)
let markerlablecolr = UIColor(red:46.0/255.0, green:151.0/255.0, blue:137.0/255.0, alpha: 1.0)


//Font 

let plumberMediumFontStr = "Roboto"
let plumberBoldFontStr = "Roboto-Bold"

let PlumberSmallFont = UIFont.init(name: plumberMediumFontStr, size: 14)
let PlumberMediumFont = UIFont.init(name: plumberMediumFontStr, size: 15)
let PlumberLargeFont = UIFont.init(name: plumberMediumFontStr, size: 16)
let PlumberSmallBoldFont = UIFont.init(name: plumberBoldFontStr, size: 15)
let PlumberMediumBoldFont = UIFont.init(name: plumberBoldFontStr, size: 15)
let PlumberLargeBoldFont = UIFont.init(name: plumberBoldFontStr, size: 16)
let plumberlableFont  = UIFont.init(name: plumberBoldFontStr, size: 13)
