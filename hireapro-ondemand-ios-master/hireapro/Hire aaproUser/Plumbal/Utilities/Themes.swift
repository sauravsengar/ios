//
//  Themes.swift
//  Plumbal
//
//  Created by Casperon Tech on 07/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class Themes:NSObject,MFMessageComposeViewControllerDelegate{
    var amount : String = String()
    let Back_ImageView:UIImageView=UIImageView(frame: CGRectMake(0, 3, 25, 25))
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
    }
    let  codename:NSArray=["Afghanistan(+93)", "Albania(+355)","Algeria(+213)","American Samoa(+1684)","Andorra(+376)","Angola(+244)","Anguilla(+1264)","Antarctica(+672)","Antigua and Barbuda(+1268)","Argentina(+54)","Armenia(+374)","Aruba(+297)","Australia(+61)","Austria(+43)","Azerbaijan(+994)","Bahamas(+1242)","Bahrain(+973)","Bangladesh(+880)","Barbados(+1246)","Belarus(+375)","Belgium(+32)","Belize(+501)","Benin(+229)","Bermuda(+1441)","Bhutan(+975)","Bolivia(+591)","Bosnia and Herzegovina(+387)","Botswana(+267)","Brazil(+55)","British Virgin Islands(+1284)","Brunei(+673)","Bulgaria(+359)","Burkina Faso(+226)","Burma (Myanmar)(+95)","Burundi(+257)","Cambodia(+855)","Cameroon(+237)","Canada(+1)","Cape Verde(+238)","Cayman Islands(+1345)","Central African Republic(+236)","Chad(+235)","Chile(+56)","China(+86)","Christmas Island(+61)","Cocos (Keeling) Islands(+61)","Colombia(+57)","Comoros(+269)","Cook Islands(+682)","Costa Rica(+506)","Croatia(+385)","Cuba(+53)","Cyprus(+357)","Czech Republic(+420)","Democratic Republic of the Congo(+243)","Denmark(+45)","Djibouti(+253)","Dominica(+1767)","Dominican Republic(+1809)","Ecuador(+593)","Egypt(+20)","El Salvador(+503)","Equatorial Guinea(+240)","Eritrea(+291)","Estonia(+372)","Ethiopia(+251)","Falkland Islands(+500)","Faroe Islands(+298)","Fiji(+679)","Finland(+358)","France (+33)","French Polynesia(+689)","Gabon(+241)","Gambia(+220)","Gaza Strip(+970)","Georgia(+995)","Germany(+49)","Ghana(+233)","Gibraltar(+350)","Greece(+30)","Greenland(+299)","Grenada(+1473)","Guam(+1671)","Guatemala(+502)","Guinea(+224)","Guinea-Bissau(+245)","Guyana(+592)","Haiti(+509)","Holy See (Vatican City)(+39)","Honduras(+504)","Hong Kong(+852)","Hungary(+36)","Iceland(+354)","India(+91)","Indonesia(+62)","Iran(+98)","Iraq(+964)","Ireland(+353)","Isle of Man(+44)","Israel(+972)","Italy(+39)","Ivory Coast(+225)","Jamaica(+1876)","Japan(+81)","Jordan(+962)","Kazakhstan(+7)","Kenya(+254)","Kiribati(+686)","Kosovo(+381)","Kuwait(+965)","Kyrgyzstan(+996)","Laos(+856)","Latvia(+371)","Lebanon(+961)","Lesotho(+266)","Liberia(+231)","Libya(+218)","Liechtenstein(+423)","Lithuania(+370)","Luxembourg(+352)","Macau(+853)","Macedonia(+389)","Madagascar(+261)","Malawi(+265)","Malaysia(+60)","Maldives(+960)","Mali(+223)","Malta(+356)","MarshallIslands(+692)","Mauritania(+222)","Mauritius(+230)","Mayotte(+262)","Mexico(+52)","Micronesia(+691)","Moldova(+373)","Monaco(+377)","Mongolia(+976)","Montenegro(+382)","Montserrat(+1664)","Morocco(+212)","Mozambique(+258)","Namibia(+264)","Nauru(+674)","Nepal(+977)","Netherlands(+31)","Netherlands Antilles(+599)","New Caledonia(+687)","New Zealand(+64)","Nicaragua(+505)","Niger(+227)","Nigeria(+234)","Niue(+683)","Norfolk Island(+672)","North Korea (+850)","Northern Mariana Islands(+1670)","Norway(+47)","Oman(+968)","Pakistan(+92)","Palau(+680)","Panama(+507)","Papua New Guinea(+675)","Paraguay(+595)","Peru(+51)","Philippines(+63)","Pitcairn Islands(+870)","Poland(+48)","Portugal(+351)","Puerto Rico(+1)","Qatar(+974)","Republic of the Congo(+242)","Romania(+40)","Russia(+7)","Rwanda(+250)","Saint Barthelemy(+590)","Saint Helena(+290)","Saint Kitts and Nevis(+1869)","Saint Lucia(+1758)","Saint Martin(+1599)","Saint Pierre and Miquelon(+508)","Saint Vincent and the Grenadines(+1784)","Samoa(+685)","San Marino(+378)","Sao Tome and Principe(+239)","Saudi Arabia(+966)","Senegal(+221)","Serbia(+381)","Seychelles(+248)","Sierra Leone(+232)","Singapore(+65)","Slovakia(+421)","Slovenia(+386)","Solomon Islands(+677)","Somalia(+252)","South Africa(+27)","South Korea(+82)","Spain(+34)","Sri Lanka(+94)","Sudan(+249)","Suriname(+597)","Swaziland(+268)","Sweden(+46)","Switzerland(+41)","Syria(+963)","Taiwan(+886)","Tajikistan(+992)","Tanzania(+255)","Thailand(+66)","Timor-Leste(+670)","Togo(+228)","Tokelau(+690)","Tonga(+676)","Trinidad and Tobago(+1868)","Tunisia(+216)","Turkey(+90)","Turkmenistan(+993)","Turks and Caicos Islands(+1649)","Tuvalu(+688)","Uganda(+256)","Ukraine(+380)","United Arab Emirates(+971)","United Kingdom(+44)","United States(+1)","Uruguay(+598)","US Virgin Islands(+1340)","Uzbekistan(+998)","Vanuatu(+678)","Venezuela(+58)","Vietnam(+84)","Wallis and Futuna(+681)","West Bank(970)","Yemen(+967)","Zambia(+260)","Zimbabwe(+263)"];
    let code:NSArray=["+93", "+355","+213","+1684","+376","+244","+1264","+672","+1268","+54","+374","+297","+61","+43","+994","+1242","+973","+880","+1246","+375","+32","+501","+229","+1441","+975","+591"," +387","+267","+55","+1284","+673","+359","+226","+95","+257","+855","+237","+1","+238","+1345","+236","+235","+56","+86","+61","+61","+57","+269","+682","+506","+385","+53","+357","+420","+243","+45","+253","+1767","+1809","+593","+20","+503","+240","+291","+372","+251"," +500","+298","+679","+358","+33","+689","+241","+220"," +970","+995","+49","+233","+350","+30","+299","+1473","+1671","+502","+224","+245","+592","+509","+39","+504","+852","+36","+354","+91","+62","+98","+964","+353","+44","+972","+39","+225","+1876","+81","+962","+7","+254","+686","+381","+965","+996","+856","+371","+961","+266","+231","+218","+423","+370","+352","+853","+389","+261","+265","+60","+960","+223","+356","+692","+222","+230","+262","+52","+691","+373","+377","+976","+382","+1664","+212","+258","+264","+674","+977","+31","+599","+687","+64","+505","+227","+234","+683","+672","+850","+1670","+47","+968","+92","+680","+507","+675","+595","+51","+63","+870","+48","+351","+1","+974","+242","+40","+7","+250","+590","+290","+1869","+1758","+1599","+508","+1784","+685","+378","+239","+966","+221","+381","+248","+232","+65","+421","+386","+677","+252","+27","+82","+34","+94","+249","+597","+268","+46","+41","+963","+886","+992","+255","+66","+670","+228","+690","+676","+1868","+216","+90","+993","+1649","+688","+256","+380","+971","+44","+1","+598","+1340","+998","+678","+58","+84","+681","+970","+967","+260","+263"];
    
    //MARK:- Function
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func CheckNullValue(value : AnyObject?) -> String? {
        var  pureStr:String=""
        if value is NSNull || value == nil{
            return pureStr
        }
        pureStr = String(value!)
        return pureStr
    }
    func getAddressForLatLng(latitude: String, longitude: String)->String {
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(constant.GooglemapAPI)")
        var fullAddress = ""
       // let data = NSData(contentsOfURL: url!)
        var data:NSData=NSData()
        do {
            data = try NSData(contentsOfURL: url!, options: NSDataReadingOptions())
            print(data)
            let json = try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address = result[0]["address_components"] as? NSArray {
                        print("get current location \(result[0]["address_components"])")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in address{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                            fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)"
                            if let address = result[0]["formatted_address"] as? String{
                                return address
                            }else{
                                return fullAddress
                            }
                        }
                    }
                }
            }

        } catch {
            print(error)
            return fullAddress

        }
        return fullAddress
    }

    func getCountryList() -> (NSDictionary) {
        let dict = [
            "AF" : ["Afghanistan", "93"],
            "AX" : ["Aland Islands", "358"],
            "AL" : ["Albania", "355"],
            "DZ" : ["Algeria", "213"],
            "AS" : ["American Samoa", "1"],
            "AD" : ["Andorra", "376"],
            "AO" : ["Angola", "244"],
            "AI" : ["Anguilla", "1"],
            "AQ" : ["Antarctica", "672"],
            "AG" : ["Antigua and Barbuda", "1"],
            "AR" : ["Argentina", "54"],
            "AM" : ["Armenia", "374"],
            "AW" : ["Aruba", "297"],
            "AU" : ["Australia", "61"],
            "AT" : ["Austria", "43"],
            "AZ" : ["Azerbaijan", "994"],
            "BS" : ["Bahamas", "1"],
            "BH" : ["Bahrain", "973"],
            "BD" : ["Bangladesh", "880"],
            "BB" : ["Barbados", "1"],
            "BY" : ["Belarus", "375"],
            "BE" : ["Belgium", "32"],
            "BZ" : ["Belize", "501"],
            "BJ" : ["Benin", "229"],
            "BM" : ["Bermuda", "1"],
            "BT" : ["Bhutan", "975"],
            "BO" : ["Bolivia", "591"],
            "BA" : ["Bosnia and Herzegovina", "387"],
            "BW" : ["Botswana", "267"],
            "BV" : ["Bouvet Island", "47"],
            "BQ" : ["BQ", "599"],
            "BR" : ["Brazil", "55"],
            "IO" : ["British Indian Ocean Territory", "246"],
            "VG" : ["British Virgin Islands", "1"],
            "BN" : ["Brunei Darussalam", "673"],
            "BG" : ["Bulgaria", "359"],
            "BF" : ["Burkina Faso", "226"],
            "BI" : ["Burundi", "257"],
            "KH" : ["Cambodia", "855"],
            "CM" : ["Cameroon", "237"],
            "CA" : ["Canada", "1"],
            "CV" : ["Cape Verde", "238"],
            "KY" : ["Cayman Islands", "345"],
            "CF" : ["Central African Republic", "236"],
            "TD" : ["Chad", "235"],
            "CL" : ["Chile", "56"],
            "CN" : ["China", "86"],
            "CX" : ["Christmas Island", "61"],
            "CC" : ["Cocos (Keeling) Islands", "61"],
            "CO" : ["Colombia", "57"],
            "KM" : ["Comoros", "269"],
            "CG" : ["Congo (Brazzaville)", "242"],
            "CD" : ["Congo, Democratic Republic of the", "243"],
            "CK" : ["Cook Islands", "682"],
            "CR" : ["Costa Rica", "506"],
            "CI" : ["Côte d'Ivoire", "225"],
            "HR" : ["Croatia", "385"],
            "CU" : ["Cuba", "53"],
            "CW" : ["Curacao", "599"],
            "CY" : ["Cyprus", "537"],
            "CZ" : ["Czech Republic", "420"],
            "DK" : ["Denmark", "45"],
            "DJ" : ["Djibouti", "253"],
            "DM" : ["Dominica", "1"],
            "DO" : ["Dominican Republic", "1"],
            "EC" : ["Ecuador", "593"],
            "EG" : ["Egypt", "20"],
            "SV" : ["El Salvador", "503"],
            "GQ" : ["Equatorial Guinea", "240"],
            "ER" : ["Eritrea", "291"],
            "EE" : ["Estonia", "372"],
            "ET" : ["Ethiopia", "251"],
            "FK" : ["Falkland Islands (Malvinas)", "500"],
            "FO" : ["Faroe Islands", "298"],
            "FJ" : ["Fiji", "679"],
            "FI" : ["Finland", "358"],
            "FR" : ["France", "33"],
            "GF" : ["French Guiana", "594"],
            "PF" : ["French Polynesia", "689"],
            "TF" : ["French Southern Territories", "689"],
            "GA" : ["Gabon", "241"],
            "GM" : ["Gambia", "220"],
            "GE" : ["Georgia", "995"],
            "DE" : ["Germany", "49"],
            "GH" : ["Ghana", "233"],
            "GI" : ["Gibraltar", "350"],
            "GR" : ["Greece", "30"],
            "GL" : ["Greenland", "299"],
            "GD" : ["Grenada", "1"],
            "GP" : ["Guadeloupe", "590"],
            "GU" : ["Guam", "1"],
            "GT" : ["Guatemala", "502"],
            "GG" : ["Guernsey", "44"],
            "GN" : ["Guinea", "224"],
            "GW" : ["Guinea-Bissau", "245"],
            "GY" : ["Guyana", "595"],
            "HT" : ["Haiti", "509"],
            "VA" : ["Holy See (Vatican City State)", "379"],
            "HN" : ["Honduras", "504"],
            "HK" : ["Hong Kong, Special Administrative Region of China", "852"],
            "HU" : ["Hungary", "36"],
            "IS" : ["Iceland", "354"],
            "IN" : ["India", "91"],
            "ID" : ["Indonesia", "62"],
            "IR" : ["Iran, Islamic Republic of", "98"],
            "IQ" : ["Iraq", "964"],
            "IE" : ["Ireland", "353"],
            "IM" : ["Isle of Man", "44"],
            "IL" : ["Israel", "972"],
            "IT" : ["Italy", "39"],
            "JM" : ["Jamaica", "1"],
            "JP" : ["Japan", "81"],
            "JE" : ["Jersey", "44"],
            "JO" : ["Jordan", "962"],
            "KZ" : ["Kazakhstan", "77"],
            "KE" : ["Kenya", "254"],
            "KI" : ["Kiribati", "686"],
            "KP" : ["Korea, Democratic People's Republic of", "850"],
            "KR" : ["Korea, Republic of", "82"],
            "KW" : ["Kuwait", "965"],
            "KG" : ["Kyrgyzstan", "996"],
            "LA" : ["Lao PDR", "856"],
            "LV" : ["Latvia", "371"],
            "LB" : ["Lebanon", "961"],
            "LS" : ["Lesotho", "266"],
            "LR" : ["Liberia", "231"],
            "LY" : ["Libya", "218"],
            "LI" : ["Liechtenstein", "423"],
            "LT" : ["Lithuania", "370"],
            "LU" : ["Luxembourg", "352"],
            "MO" : ["Macao, Special Administrative Region of China", "853"],
            "MK" : ["Macedonia, Republic of", "389"],
            "MG" : ["Madagascar", "261"],
            "MW" : ["Malawi", "265"],
            "MY" : ["Malaysia", "60"],
            "MV" : ["Maldives", "960"],
            "ML" : ["Mali", "223"],
            "MT" : ["Malta", "356"],
            "MH" : ["Marshall Islands", "692"],
            "MQ" : ["Martinique", "596"],
            "MR" : ["Mauritania", "222"],
            "MU" : ["Mauritius", "230"],
            "YT" : ["Mayotte", "262"],
            "MX" : ["Mexico", "52"],
            "FM" : ["Micronesia, Federated States of", "691"],
            "MD" : ["Moldova", "373"],
            "MC" : ["Monaco", "377"],
            "MN" : ["Mongolia", "976"],
            "ME" : ["Montenegro", "382"],
            "MS" : ["Montserrat", "1"],
            "MA" : ["Morocco", "212"],
            "MZ" : ["Mozambique", "258"],
            "MM" : ["Myanmar", "95"],
            "NA" : ["Namibia", "264"],
            "NR" : ["Nauru", "674"],
            "NP" : ["Nepal", "977"],
            "NL" : ["Netherlands", "31"],
            "AN" : ["Netherlands Antilles", "599"],
            "NC" : ["New Caledonia", "687"],
            "NZ" : ["New Zealand", "64"],
            "NI" : ["Nicaragua", "505"],
            "NE" : ["Niger", "227"],
            "NG" : ["Nigeria", "234"],
            "NU" : ["Niue", "683"],
            "NF" : ["Norfolk Island", "672"],
            "MP" : ["Northern Mariana Islands", "1"],
            "NO" : ["Norway", "47"],
            "OM" : ["Oman", "968"],
            "PK" : ["Pakistan", "92"],
            "PW" : ["Palau", "680"],
            "PS" : ["Palestinian Territory, Occupied", "970"],
            "PA" : ["Panama", "507"],
            "PG" : ["Papua New Guinea", "675"],
            "PY" : ["Paraguay", "595"],
            "PE" : ["Peru", "51"],
            "PH" : ["Philippines", "63"],
            "PN" : ["Pitcairn", "872"],
            "PL" : ["Poland", "48"],
            "PT" : ["Portugal", "351"],
            "PR" : ["Puerto Rico", "1"],
            "QA" : ["Qatar", "974"],
            "RE" : ["Réunion", "262"],
            "RO" : ["Romania", "40"],
            "RU" : ["Russian Federation", "7"],
            "RW" : ["Rwanda", "250"],
            "SH" : ["Saint Helena", "290"],
            "KN" : ["Saint Kitts and Nevis", "1"],
            "LC" : ["Saint Lucia", "1"],
            "PM" : ["Saint Pierre and Miquelon", "508"],
            "VC" : ["Saint Vincent and Grenadines", "1"],
            "BL" : ["Saint-Barthélemy", "590"],
            "MF" : ["Saint-Martin (French part)", "590"],
            "WS" : ["Samoa", "685"],
            "SM" : ["San Marino", "378"],
            "ST" : ["Sao Tome and Principe", "239"],
            "SA" : ["Saudi Arabia", "966"],
            "SN" : ["Senegal", "221"],
            "RS" : ["Serbia", "381"],
            "SC" : ["Seychelles", "248"],
            "SL" : ["Sierra Leone", "232"],
            "SG" : ["Singapore", "65"],
            "SX" : ["Sint Maarten", "1"],
            "SK" : ["Slovakia", "421"],
            "SI" : ["Slovenia", "386"],
            "SB" : ["Solomon Islands", "677"],
            "SO" : ["Somalia", "252"],
            "ZA" : ["South Africa", "27"],
            "GS" : ["South Georgia and the South Sandwich Islands", "500"],
            "SS​" : ["South Sudan", "211"],
            "ES" : ["Spain", "34"],
            "LK" : ["Sri Lanka", "94"],
            "SD" : ["Sudan", "249"],
            "SR" : ["Suriname", "597"],
            "SJ" : ["Svalbard and Jan Mayen Islands", "47"],
            "SZ" : ["Swaziland", "268"],
            "SE" : ["Sweden", "46"],
            "CH" : ["Switzerland", "41"],
            "SY" : ["Syrian Arab Republic (Syria)", "963"],
            "TW" : ["Taiwan, Republic of China", "886"],
            "TJ" : ["Tajikistan", "992"],
            "TZ" : ["Tanzania, United Republic of", "255"],
            "TH" : ["Thailand", "66"],
            "TL" : ["Timor-Leste", "670"],
            "TG" : ["Togo", "228"],
            "TK" : ["Tokelau", "690"],
            "TO" : ["Tonga", "676"],
            "TT" : ["Trinidad and Tobago", "1"],
            "TN" : ["Tunisia", "216"],
            "TR" : ["Turkey", "90"],
            "TM" : ["Turkmenistan", "993"],
            "TC" : ["Turks and Caicos Islands", "1"],
            "TV" : ["Tuvalu", "688"],
            "UG" : ["Uganda", "256"],
            "UA" : ["Ukraine", "380"],
            "AE" : ["United Arab Emirates", "971"],
            "GB" : ["United Kingdom", "44"],
            "US" : ["United States of America", "1"],
            "UY" : ["Uruguay", "598"],
            "UZ" : ["Uzbekistan", "998"],
            "VU" : ["Vanuatu", "678"],
            "VE" : ["Venezuela (Bolivarian Republic of)", "58"],
            "VN" : ["Viet Nam", "84"],
            "VI" : ["Virgin Islands, US", "1"],
            "WF" : ["Wallis and Futuna Islands", "681"],
            "EH" : ["Western Sahara", "212"],
            "YE" : ["Yemen", "967"],
            "ZM" : ["Zambia", "260"],
            "ZW" : ["Zimbabwe", "263"]
        ]
        
        return dict
    }
    func validpasword(str: NSString) -> Bool {
        var lowerCaseLetter: Bool = false
        var digit: Bool = false
        for i in 0..<str.length {
            let c: unichar = str.characterAtIndex(i)
            if !lowerCaseLetter {
                lowerCaseLetter = NSCharacterSet.lowercaseLetterCharacterSet().characterIsMember(c)
            }
            if !digit {
                digit = NSCharacterSet.decimalDigitCharacterSet().characterIsMember(c)
            }
        }
        var isValid: Bool = false
        if digit && lowerCaseLetter  {
            isValid = true
        }
        return isValid
    }
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,1", "iPad5,3", "iPad5,4":           return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    func Currency_Symbol(Currency_code:String)->String{
        //        let localeComponents = [NSLocaleCurrencyCode: Currency_code]
        //        let localeIdentifier = NSLocale.localeIdentifierFromComponents(localeComponents)
        //        let locale = NSLocale(localeIdentifier: localeIdentifier)
        //        var currencySymbol = locale.objectForKey(NSLocaleCurrencySymbol) as! String
        //        let fmtr = NSNumberFormatter()
        //        fmtr.locale = locale
        //        fmtr.numberStyle = .CurrencyStyle
        //        currencySymbol = fmtr.currencySymbol!
        //        if currencySymbol.characters.count > 1 {
        //            currencySymbol = currencySymbol.substringFromIndex(currencySymbol.startIndex.advancedBy(currencySymbol.characters.count - 1))
        //        }
        //        return currencySymbol
        
        let currencyCode = Currency_code
        let currencySymbols  = NSLocale.availableLocaleIdentifiers()
            .map { NSLocale(localeIdentifier: $0) }
            .filter {
                if let localeCurrencyCode = $0.objectForKey(NSLocaleCurrencyCode) as? String {
                    return localeCurrencyCode == currencyCode
                } else {
                    return false
                }
            }
            .map {
                ($0.localeIdentifier, $0.objectForKey(NSLocaleCurrencySymbol)!)
        }
        print( currencySymbols)
        return CheckNullValue((currencySymbols.first?.1))!
    }
    
    func applyBlurEffect(image: UIImage)->UIImage{
        let imageToBlur = CIImage(image: image)
        let blurfilter = CIFilter(name: "CIGaussianBlur")
        blurfilter!.setValue(5, forKey: kCIInputRadiusKey)
        blurfilter!.setValue(imageToBlur, forKey: "inputImage")
        let resultImage = blurfilter!.valueForKey("outputImage")! as! CIImage
        var blurredImage = UIImage(CIImage: resultImage)
        let cropped:CIImage=resultImage.imageByCroppingToRect(CGRectMake(0, 0,imageToBlur!.extent.size.width, imageToBlur!.extent.size.height))
        blurredImage = UIImage(CIImage: cropped)
        return blurredImage
    }
    
    func addBlur(view:UIView)->UIView {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.alpha=0.8
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        view.addSubview(blurEffectView)
        return view
    }
    
    func RoundView(width:CGFloat)->CGFloat{
        return width/2
    }
    
    func Showtoast(ToastString:String)->SAWaveToast{
        let waveToast = SAWaveToast(text: "\(ToastString)", font: UIFont(name: "Raleway-Bold", size: 16.0), fontColor: UIColor.whiteColor())
        return waveToast
    }
    
    func saveCounrtyphone(countrycode: String) {
        NSUserDefaults.standardUserDefaults().setObject(countrycode, forKey: "countryphone")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getCounrtyphone() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("countryphone") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("countryphone") as! String
        }else{
            return ""
        }
    }
    
    func AlertView(title:NSString,Message:NSString,ButtonTitle:NSString){
        if(Message == kErrorMsg){
            RKDropdownAlert.title(Appname, message: Message as String, backgroundColor: UIColor.whiteColor() , textColor: UIColor.redColor())
          return
        }
        RKDropdownAlert.dismissAllAlert()
        RKDropdownAlert.title(Appname, message: Message as String, backgroundColor: UIColor.whiteColor() , textColor: PlumberThemeColor)
    }
    
    func calculateHeightForString(inString:String) -> CGFloat {
        let messageString = inString
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(16.0)])
        let rect:CGRect = attrString!.boundingRectWithSize(CGSizeMake(300.0,CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context:nil )//hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height  //to include button's in your tableview
    }
    
    func saveLocationname(Locationname: String) {
        NSUserDefaults.standardUserDefaults().setObject(Locationname, forKey: "LocationName")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getLocationname() -> String {
        if (NSUserDefaults.standardUserDefaults().objectForKey("LocationName") != nil){
            return NSUserDefaults.standardUserDefaults().objectForKey("LocationName") as! String
        } else{
            return ""
        }
    }
    
    func saveLocationID(LocationID: String) {
        NSUserDefaults.standardUserDefaults().setObject(LocationID, forKey: "LocationID")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getLocationID() -> String {
        if ( NSUserDefaults.standardUserDefaults().objectForKey("LocationID")  != nil) {
            return NSUserDefaults.standardUserDefaults().objectForKey("LocationID") as! String
        } else{
            return ""
        }
    }
    
    func saveJaberID(JaberID: String) {
        NSUserDefaults.standardUserDefaults().setObject(JaberID, forKey: "JaberID")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getJaberID() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("JaberID") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("JaberID") as! String
        } else {
            return ""
        }
    }
    
    func saveaddresssegue(presval: String) {
        NSUserDefaults.standardUserDefaults().setObject(presval, forKey: "PressAddaddress")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getaddresssegue() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("PressAddaddress") != nil {
            return NSUserDefaults.standardUserDefaults().objectForKey("PressAddaddress") as! String
        } else{
            return ""
        }
    }
    
    func removeRotation( image: UIImage) -> UIImage {
        if image.imageOrientation == .Up {
            return image
        }
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRect.init(origin:CGPoint(x: 10, y: 20), size:image.size ))
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }
    
    func Apply_layer(layer:CALayer) {
        
        layer.cornerRadius = 5
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 2
        layer.shadowOffset = CGSizeMake(3.0, 3.0)
    }
    
    func rotateImage(image: UIImage) -> UIImage {
        if (image.imageOrientation == UIImageOrientation.Up ) {
            return image
        }
        UIGraphicsBeginImageContext(image.size)
        image.drawInRect(CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return copy
    }
    
    func saveLanguage(str:NSString){
        var tempStr = str
        if(tempStr.isEqualToString("ta")){
            tempStr="ta"
        }else if(tempStr.isEqualToString("en")){
            tempStr="en"
        }
        NSUserDefaults.standardUserDefaults().setObject(tempStr, forKey: "LanguageName")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func SetLanguageToApp(){
        let savedLang=NSUserDefaults.standardUserDefaults().objectForKey("LanguageName") as! NSString
        if(savedLang == "ta"){
            Language_handler.setApplicationLanguage(Language_handler.TamilLanguageShortName)
        }else if(savedLang == "en") {
            Language_handler.setApplicationLanguage(Language_handler.EnglishUSLanguageShortName)
        }
    }
    
    func setLang(text:String)->String{
        return Language_handler.VJLocalizedString(text, comment: nil)
    }
    
    func convertIntToString(integer : Int) -> NSString {
        var str:NSString = NSString()
        str = "\(integer)"
        return str
    }
    
    func convertIntToJobStatus(integer : Int) -> NSString {
        var str:NSString = NSString()
        str = "\(integer)"
        if (str == "1") {
            str = "Accepted"
        }else if (str == "2") {
            str = "Ongoing"
        } else if (str == "4") {
            str = "Completed"
        } else if (str == "5") {
            str = "Cancelled"
        } else if (str == "6") {
            str = "Disputed"
        }
        return str
    }
    
    func convertFloatToString(floating : Float) -> NSString {
        var str:NSString = NSString()
        str = "\(floating)"
        return str
    }
    
    
    
    //MARK:- MFMessageComposeViewController Delegate
    
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    func configuredMessageComposeViewController(message:String,number:String) -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        // messageComposeVC.recipients = textMessageRecipients
        messageComposeVC.body = "\(message)"
        messageComposeVC.recipients = [number]
        
        return messageComposeVC
    }
    
    @objc func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - NSUserDefault Function
    
    func saveUserID(userID: String) {
        NSUserDefaults.standardUserDefaults().setObject(userID, forKey: "userID")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func saveUserPasswd(Passwd: String) {
        NSUserDefaults.standardUserDefaults().setObject(Passwd, forKey: "Password")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getUserPasswd() -> String! {
        if NSUserDefaults.standardUserDefaults().objectForKey("Password") != nil {
            return NSUserDefaults.standardUserDefaults().objectForKey("Password") as? String
        } else{
            return ""
        }
    }
    
    func getUserID() -> String! {
        if NSUserDefaults.standardUserDefaults().objectForKey("userID") != nil {
            return NSUserDefaults.standardUserDefaults().objectForKey("userID") as? String
        }else {
            return ""
        }
    }
    
    func Check_userID() -> String! {
        if NSUserDefaults.standardUserDefaults().objectForKey("userID") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("userID") as? String
        } else{
            return ""
        }
    }
    
    func saveUserName(UserName: String) {
        NSUserDefaults.standardUserDefaults().setObject(UserName, forKey: "UserName")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getUserName() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("UserName") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("UserName") as! String
        } else {
            return ""
        }
    }
    
    func saveCountryCode(CountryCode: String) {
        NSUserDefaults.standardUserDefaults().setObject(CountryCode, forKey: "CountryCode")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getCountryCode() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("CountryCode")  != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("CountryCode") as! String
        } else{
            return ""
        }
    }
    
    func saveMobileNum(MobileNum: String) {
        NSUserDefaults.standardUserDefaults().setObject(MobileNum, forKey: "MobileNum")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getMobileNum() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("MobileNum") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("MobileNum") as! String
        } else{
            return ""
        }
    }
    
    func saveGender(MobileNum: String) {
        NSUserDefaults.standardUserDefaults().setObject(MobileNum, forKey: "gender")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getGender() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("gender") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("gender") as! String
        } else{
            return ""
        }
    }
    
    func saveEmailID(EmailID: String) {
        NSUserDefaults.standardUserDefaults().setObject(EmailID, forKey: "EmailID")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getEmailID() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("EmailID") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("EmailID") as! String
        } else{
            return ""
        }
    }
    
    func saveuserDP(userDP: String) {
        NSUserDefaults.standardUserDefaults().setObject(userDP, forKey: "userDP")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getuserDP() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("userDP") != nil {
            return NSUserDefaults.standardUserDefaults().objectForKey("userDP") as! String
        } else{
            return ""
        }
    }
    
    func saveCategoryString(CategoryString: String) {
        NSUserDefaults.standardUserDefaults().setObject(CategoryString, forKey: "CategoryString")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getCategoryString() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("CategoryString") != nil {
            return NSUserDefaults.standardUserDefaults().objectForKey("CategoryString") as! String
        } else{
            return ""
        }
    }
    
    func saveWalletAmt(WalletAmt: String) {
        NSUserDefaults.standardUserDefaults().setObject(WalletAmt, forKey: "WalletAmt")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getWalletAmt() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("WalletAmt") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("WalletAmt") as! String
        } else{
            return ""
        }
    }
    
    func saveCurrencyCode(CurrencyCode: String) {
        NSUserDefaults.standardUserDefaults().setObject(CurrencyCode, forKey: "CurrencyCode")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getCurrencyCode() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("CurrencyCode") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("CurrencyCode") as! String
        }else{
            return ""
        }
    }
    
    func saveCurrency(Currency: String) {
        NSUserDefaults.standardUserDefaults().setObject(Currency, forKey: "Currency")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getCurrency() -> String {
        if NSUserDefaults.standardUserDefaults().objectForKey("Currency") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("Currency") as! String
        } else{
            return ""
        }
    }
    
    func saveJaberPassword(JaberPassword: String) {
        NSUserDefaults.standardUserDefaults().setObject(JaberPassword, forKey: "JaberPassword")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getJaberPassword() -> String? {
        if NSUserDefaults.standardUserDefaults().objectForKey("JaberPassword") != nil{
            return NSUserDefaults.standardUserDefaults().objectForKey("JaberPassword") as? String
        } else {
            return ""
        }
    }
    
    //MARK: - Color Coding
    
    func HeaderThemeColor()->UIColor{
        var HeaderColor:UIColor=UIColor()
        HeaderColor = UIColor(patternImage: UIImage(named:"TopImage")!)
        return HeaderColor
    }
    
    func ThemeColour()->UIColor {
        var Theme_Color:UIColor!=UIColor()
        Theme_Color=nil
        Theme_Color = UIColor(red:0.14, green:0.12, blue:0.13, alpha:0.8)
        return Theme_Color
    }
    
    func LightRed()->UIColor{
        return UIColor(red:0.97, green:0.51, blue:0.02, alpha:0.5)
    }
    
    func DarkRed()->UIColor{
        var loadercolor:UIColor=LightRed()
        loadercolor=loadercolor.colorWithAlphaComponent(1.0)
        return loadercolor
    }
    
    func PaleWhiteColour()->UIColor{
        return UIColor(red: 0.918, green: 0.914, blue: 0.914, alpha: 1)
    }
    
    func Lightgray()->UIColor{
        return UIColor(red:216.0/255.0, green:216.0/255.0 ,blue:216.0/255.0, alpha:1.0)
    }
    
    func buttomBGColor()->UIColor{
        return UIColor(red:255.0/255.0, green:106.0/255.0, blue:1.0/255.0, alpha:1.0)
    }
    
    
}
