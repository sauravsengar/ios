//


//     let AppbaseUrl:NSString="http://project.dectar.com/plumbal/v1"



//let MainbaseUrl:NSString = "http://192.168.1.251:3002/mobile"
//    let BaseUrl:NSString = "http://192.168.1.251:3002"

// http://handyforall.zoplay.com/

//"http://project.dectar.com/maidac/v2/"  //http://project.dectar.com/plumbal/ //http://192.168.1.251:8081/dectar/plumbal/



//  Constant.swift
//  Plumbal
//
//  Created by Casperon Tech on 08/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import Foundation
import UIKit

var kPageCount = "10"
let Workname="Maid"
var kTypeStatus = "TypingStatus"
var kNetworkErrorMsg = "No network connection..."
var kNetworkConnectedMsg = "Network available"
var StripeStatus:NSString=NSString()
var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate


class Constant{
    var RootBase:RootViewController=RootViewController()
    let progress = GradientCircularProgress()
    var kErrorMsg = "Error in network connection... Please try again"
    let Roomname:NSString = "join network"
    let SwitchRoomname : NSString = "switch room"
    let kGeoCodingString = "http://maps.google.com/maps/geo?q=%f,%f&output=csv"
    let Hostname:NSString="192.168.1.150"//192.168.1.150 //67.219.149.186
    let DomainName:NSString="@casp83"//@casp83 //@messaging.dectar.com
    
    let GooglemapAPI:NSString="AIzaSyAY5tzWbWOCsL4I6K2ZocvpAdFbDXnbGeM"
    let GoogleplacesAPI:NSString="AIzaSyAZzpMs7DfXYBxVMF_0KtADyoYe97BIjj8"

    let kOpenGoogleMapScheme = "comgooglemaps://"

   //let GooglemapAPI:NSString="AIzaSyCCdmjAMeiFsWRQpmIfHrXG7ZnEXsKuJew"
//    let MainbaseUrl:NSString = "http://handyforall.zoplay.com/mobile"
//    
//    let BaseUrl:NSString = "http://handyforall.zoplay.com"
//    
    let BaseUrl:NSString = "https://hireaproondemand.com"

   let MainbaseUrl:NSString = "https://hireaproondemand.com/mobile"
    
    
//    let BaseUrl:NSString = "http://192.168.0.85:3004"

//    let MainbaseUrl:NSString = "http://192.168.0.85:3004/mobile"

   
    
    var AppbaseUrl:NSString {
        get{
            return "\(MainbaseUrl)" //v1
        }
    }
    
    var AppbaseUrl1:NSString   {
        get{
            return "\(MainbaseUrl)" //v2
        }
    }
    
    var AppbaseUrl2:NSString{
        get{
            return "\(MainbaseUrl)" //v3
        }
    }
    
    var RegisterAccount: NSString {
        get{
            return "\(AppbaseUrl)/app/check-user"
        }
    }
    
    var Register: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/register")
        }
    }
    
    var Login: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/login")
        }
    }
    var viewProfile : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/provider/provider-info")
        }
    }
    
    var Map_Providers : NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/mapbook-job")
        }
 
    }
    var Apply_filter : NSString {
        get
        {
            return AppbaseUrl.stringByAppendingString("/app/tasker_filter")
        }
    }
    var GetBadgeList : NSString{
        get
        {
             return AppbaseUrl.stringByAppendingString("/app/user-badgelist")
            
        }
    }
    
    var reviewsUrl : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/provider/provider-rating")
        }
    }
    
    var Invite_Friends: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/get-invites")
        }
    }
    
    var Change_pass: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/change-password")
        }
    }
    
    var Change_Name: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/change-name")
        }
    }
    var view_Transaction_details :NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/userjob-transaction")
        }
    }
    
    var Get_Transaction :NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/user-transaction")
        }
    }
    
    var GetNotificationUrl :NSString{
        get {
            return AppbaseUrl.stringByAppendingString("/app/notification")
            
        }
        
    }
    var GetUserreviews:NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/get-reviews")
        }
    }
    var Mymoney: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/get-money-page")
        }
    }
    var termcondi_url : NSString
        {
        get{
            return "\(AppbaseUrl)/terms-condition"
        }
        
    }
    var privacy_policy : NSString{
        
            get{
               return "\(AppbaseUrl)/privacy-policy"
            }
    }
    var Chat_support_Details: NSString
        {
        get
        {
            return MainbaseUrl.stringByAppendingString("/support-chat-history")
        }
    }
    
    var Chat_support_history : NSString{
        get{
            return MainbaseUrl.stringByAppendingString("/support-messages")
        }
    }
    
    var Stripe_Check_Url: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/mobile/wallet-recharge/stripe-process")
        }
    }
    var Reset_Password: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/reset-password")
        }
    }
    
    var Wallet_Recharge : NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/mobile/wallet-recharge/payform?")
        }
    }
    
    var Wallet_Recharge_paypal: NSString
    {
        
        get{
            return AppbaseUrl.stringByAppendingString("/app/wallet-recharge/mobpaypal")
        }
        
    }
    
    var contact_emergency : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/alert-emergency-contact")
        }
    }
    
    var Social_Check: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/mobile/social-fbcheckUser")
        }
    }
    
    var Social_Register: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/mobile/social-register")
        }
    }
    
    var Social_login: NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/mobile/social-login")
        }
    }
    
    var Update_Password: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/update-reset-password")
        }
    }
    
    var Get_Location: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/get-location")
        }
    }
    var Update_Location: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/set-user-location")
        }
    }
    var Get_Categories: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/categories")
        }
    }
    
    var Badge: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/mapbook-job")
        }
    }

    
    var inser_address_map:NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/map/insert-address")
        }
    }
    var Get_SubCategories: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/categories")
        }
    }
    var Add_address: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/insert-address")
        }
    }
    
    var List_address: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/list_address")
        }
    }
    
    
    var Image_Edit: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user-profile-pic")
        }
    }
    
    var Coupon_Call: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/apply-coupon")
        }
    }
    
    var changemobilenumber: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/change-mobile")
        }
    }
    
    var view_emergency: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/view-emergency-contact")
        }
    }
    
    var add_emergency: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/set-emergency-contact")
        }
    }
    var Delete_emergency: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/delete-emergency-contact")
        }
    }
    
    var Logout_url : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/user/logout")
        }
    }
    var Delete_Address: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/delete_address")
        }
    }
    var Book_It: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/book-job")
        }
    }
    
    var Get_Orders: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/my-jobs-new")
        }
    }
    
    var Get_TransactionDetail: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/get-trans-list")
        }
    }
    
    var Get_SortingOrders: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/my-jobs-new")
        }
    }
    
    var Get_Todays_Orders : NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/user/recentuser-list")
        }
    }
    
    var MapOrder_confirm : NSString
    {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/mapuser-booking")
        }
    }
    var ListView_booklater : NSString
    {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/user-booklater")
        }
    }
    
    var LisviewBooking :NSString{
        get{
            return AppbaseUrl2.stringByAppendingString("/app/user-listviewbooking")
        }
    }
    var Order_Confirm: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/user-booking")
        }
    }
    
    var Get_Reasons: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/cancellation-reason")
        }
    }
    var Cancel_Reasons: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/cancel-job")
        }
    }
    var GetOrderdetail: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/user/view-job")
        }
    }
    var Get_Payment_Detail: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment-list")
        }
    }
    var Apply_Coupon_code :NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment/couponmob")
        }
        
    }
    var Get_Summary_Details : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/paymentlist/history")
        }
    }
    var Pay_Cash: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment/by-cash")
        }
    }
    var Pay_Autodetect: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/payment/by-auto-detect")
        }
    }
    var Pay_Wallet: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment/by-wallet")
        }
    }
    var Pay_Transaction: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment/by-gateway")
        }
    }
    
    var Pay_Paypal :NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/payment/paypalPayment")
        }
    }
    
    var Get_rating: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/get-rattings-options")
        }
    }
    
    var About_us : NSString{
        get{
            return AppbaseUrl.stringByAppendingString("/app/mobile/aboutus")
        }
    }
    
    var Pay_Creditcard : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/mobile/stripe-manual-payment-form?")
        }
    }
    
    var Pay_stripeconnect : NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/mobile/stripe-connect-payment-form?")
        }
    }
    
    var Post_rating: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/submit-rattings")
        }
    }
    
    var Get_CategoryInfo: NSString {
        get{
            return AppbaseUrl1.stringByAppendingString("/user/get-category-info")
        }
    }
    
    var Get_ProviderInfo: NSString {
        get{
            return AppbaseUrl1.stringByAppendingString("/user/get-provider-profile")
        }
    }
    var Get_Onlinestatus: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/chat/availablity")
        }
    }
    
    var GetStripeStatus: NSString {
        get{
            return AppbaseUrl.stringByAppendingString("/app/stripe-api-keys")
        }
    }
    
    var Pay_Stripe: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/stripe-wallet-recharge")
        }
    }
    var categorySearch: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/category-search-custom")
        }
    }
    
    
    
    var Pay_Stripe_Provider: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/stripe-fees-payment")
        }
    }
    
    var Delete_card: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/stripe-delete-card")
        }
    }
    
    var Chat_Details: NSString {
        get{
            return MainbaseUrl.stringByAppendingString("/chat/chathistory")
        }
    }
    
    var UpdateNotificationMode : NSString{
        get{
            return MainbaseUrl.stringByAppendingString("/user/notification_mode")
        }
    }
    
    var Show_ChatList: NSString {
        get{
            return AppbaseUrl2.stringByAppendingString("/app/getmessage")
        }
    }
    
    
    var Appinfo_url : NSString{
        get {
            return   AppbaseUrl.stringByAppendingString ("/app/mobile/appinfo")
        }
    }
    
    var Complete_Payment : NSString{
        get{
            return AppbaseUrl.stringByAppendingString ("/app/payment/zero")
        }
    }
    
    var search_Keyword : NSString{
        get{
            return AppbaseUrl.stringByAppendingString ("/search-keyword")
        }
    }

    
   
    //MARK: - Function
    
    func showProgress(){
        progress.show(message: "", style: BlueIndicatorStyle())
    }
    
    
    func DismissProgress() {
        progress.dismiss()
    }
}


//MARK: - Language Reuse

var Appname:String {
get {
    return themes.setLang("app_name")
}
}

var kOk : String{
get {
    return themes.setLang("ok")
}
}

var kErrorMsg:String {
get{
    return themes.setLang("error_msg")
}
}

var kLanguage : String{
get {
    return "en"
}
}

 