//
//  SocketIOManager.swift
//  SocketChat
//http://192.168.1.251:3002/notify

//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit


var iSSocketDisconnected:Bool=Bool()
var iSChatSocketDisconnected:Bool=Bool()

class SocketIOManager: NSObject {
      var themes:Themes=Themes()
     var url_handler:URLhandler=URLhandler()
    var notificationmode : NSString = NSString()
    static let sharedInstance = SocketIOManager()
    var nick_name:NSString=NSString()
    var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate
var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string:"\(constant.BaseUrl)")!, options: [.Log(true),.Nsp("/notify"), .ForcePolling(true)])
    var ChatSocket :SocketIOClient = SocketIOClient(socketURL: NSURL(string:"\(constant.BaseUrl)")!, options: [.Log(true), .Nsp("/chat"), .ForcePolling(true)])
    
    override init() {
        super.init()
    }
    
    
    func establishConnection() {
       socket.connect()
        notificationmode = "socket"
        self.UpdateNotificationMode()
        socket.on("connect") {data, ack in
           print("..Check Socket Connection.....\(data).........")
            
              iSSocketDisconnected=false;
            self.roomcreation(constant.Roomname as String, nickname: self.themes.getUserID())

            
       
            
        }
        
        
        socket.on("network disconnect") {data, ack in
            print("..Check Socket dis Connection.....\(data).........")
            
        
            
            
            iSSocketDisconnected=true;
            
        }


    }
    
    
    
    
    
    
    func  UpdateNotificationMode(){
        let Param: Dictionary = ["user":themes.getUserID(),"user_type":"user","mode":notificationmode,"type":"ios"]
        // print(Param)
        url_handler.makeCall(constant.UpdateNotificationMode, param: Param) {
            (responseObject, error) -> () in
            
            if(error != nil)
            {
               
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                    }
                    else
                    {
                    }
                    
                    
                }
            }
        }

    }
    
    
    
    
    func establishChatConnection() {
        
       ChatSocket.connect()
         ChatSocket.on("connect") {data, ack in
            print("..Check Socket Connection.....\(data).........")
            iSChatSocketDisconnected = false
            
            SocketIOManager.sharedInstance.ChatWithNickname(self.themes.getUserID())

           
        }
        
        ChatSocket.on("disconnect") {data, ack in
            print("..Check Socket dis Connection.....\(data).........")
              iSChatSocketDisconnected=true;
            
        }
        
        ChatSocket.on("error") {data, ack in
            
            print("the socket erroe .......\(data)")
            
        }

    }
    
    
    func sendChatSupportMessage(message: String,userid:String,admin_id:String,from_id:String,topic_title:String,type:String,reference_id:String ) {
        
        if(ChatSocket.status == .Connected)
        {
            
            let jsonString:NSDictionary = ["user":userid,"admin":admin_id,"from":from_id,"topic":topic_title,"message":message,"type":type,"reference":reference_id]
            ChatSocket.emit("new support message", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
        
        
    }
    
    func getChatSupportMessage(completionHandler: (messageInfo: [String: AnyObject]) -> Void) {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("supportmessage") {[weak self] data, ack in
            dictData["message"]=data[0]
            
            self?.Appdel.socketSupportNotification(dictData);
            
            
            
        }
        
    }
    
    func RemoveAllListener()
    {
        notificationmode = "apns"
        self.UpdateNotificationMode()

        //Removing Chat Listeners
        ChatSocket.off("roomcreated")
        ChatSocket.off("updatechat")
        ChatSocket.off("single message status")
        ChatSocket.off("message status")
        ChatSocket.off("start typing")
        ChatSocket.off("stop typing")
        ChatSocket.off("connect")
        ChatSocket.off("disconnect")
        ChatSocket.off("error")
        
        
        //Removing Socket job related Listeners

        socket.off("connect")
        socket.off("network disconnect")
        socket.off("network created")
        socket.off("notification")
        socket.off("tasker tracking")
        socket.off("push notification")
        
        ChatSocket.disconnect()

        socket.disconnect()





        


        
    }
    
    func LeaveRoom(providerid : String)
    {
        
    if socket.status == .Closed
        
    {
        
        let param = ["user":providerid];

        socket.emit("network disconnect", param)
        }
        print("iosjjjj \(self.nick_name)")
        print("***************SOCKET DISCONNECTED******************")
    }
    
    func LeaveChatRoom(userid : String)
    {
          if ChatSocket.status == .Closed
        
          {
        
            let param = ["user":userid];
  
        ChatSocket.emit("disconnect", param)
        print("iosjjjj \(self.nick_name)")
        }
        print("***************SOCKET DISCONNECTED******************")
    }
    
    

    //Job Related socket call

    func roomcreation (RoomName : String,nickname: String)
    {
        if (socket.status == .Connected)
        {
            let param = ["user":nickname];

            socket.emit(RoomName, param)
        }
        socket.on("network created") {data, ack in
            print("..new  Socket Connection.....\(data).........")
            self.connectToServerWithNickname(self.themes.getUserID(), completionHandler: { (userList) in
                
            })
            self.listenTracking()
            
            
        }

    }
    func connectToServerWithNickname(nickname: String ,completionHandler: (userList: [[String: AnyObject]]!) -> Void) {

           


        
        
        
        let dictData:NSMutableDictionary = NSMutableDictionary()
        
        socket.on("notification") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..TEST DATA.....\(dictData).........")
            self!.Appdel.socketNotification(dictData);
            
        }
    }
    
    
    
    func listenTracking(){
        let dictData:NSMutableDictionary = NSMutableDictionary()
        socket.on("tasker tracking") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Start tracking message....\(dictData).........")
            let Message:NSDictionary=(dictData["message"] as? NSDictionary)!
            trackingDetail.taskId = Message.objectForKey("task") as! NSString as String
            trackingDetail.userLat = Double(Message.objectForKey("lat") as! String)!
            trackingDetail.userLong = Double(Message.objectForKey("lng")  as! String)!
            let lastdrive : String = self!.themes.CheckNullValue(Message.objectForKey("lastdriving"))!
            let bearing : String = self!.themes.CheckNullValue(Message.objectForKey("bearing"))!
            trackingDetail.lastDriving =   lastdrive
            trackingDetail.bearing = bearing
            NSNotificationCenter.defaultCenter().postNotificationName("Tracking", object: nil, userInfo: nil)
            
        }
        
    }

    func connectToServerWithprovidername(providername: NSMutableArray, completionHandler: (userList: [[String: AnyObject]]!) -> Void) {
        
        var k : Int = Int ()
        for k=0; k < providername.count; k += 1
        {
        self.nick_name = providername.objectAtIndex(k) as! NSString
        
        
        
        
        socket.emit("switch room", providername.objectAtIndex(k) as! NSString)
        
        let dictData:NSMutableDictionary = NSMutableDictionary()
        
        socket.on("push notification") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..TEST DATA.....\(dictData).........")
            self!.Appdel.socketNotification(dictData);
            
            }
            
                  }
    }

    
    
    func ChatWithNickname(nickname: String) {
        
        
        if (ChatSocket.status == .Connected)

        {
        let param = ["user":nickname];
        
        ChatSocket.emit("create room",param)
       // let dictData:NSMutableDictionary = NSMutableDictionary()
        }

        ChatSocket.on("roomcreated") {[weak self] data, ack in
          //  dictData["message"]=data[0]
            print("..TEST DATA.....\(data).........")
            
            
            
            self!.listenstartTyping()
            self!.listenstoptTyping()
            self!.listenSingleMessageStatus()
            self!.ListeningMessageStatus()
            self!.getChatMessage { (messageInfo) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    NSLog("The Chat message information=%@", messageInfo)
                })
            }
            

           // self?.Appdel.socketChatNotification(dictData);
        }
        
    }
    
    
    func sendMessage(message: String, withNickname nickname: String, Providerid: String, taskid: String) {
        
        if(ChatSocket.status == .Connected)
        {

        let jsonString:NSDictionary = ["user":nickname,"tasker":Providerid,"message":message,"task":taskid,"from":nickname]
        ChatSocket.emit("new message", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
       

       
    }
    
    
    func getChatMessage(completionHandler: (messageInfo: [String: AnyObject]) -> Void) {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("updatechat") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..TEST DATA.....\(dictData).........")
            self?.Appdel.socketChatNotification(dictData);
        }
        
    }
    

    
    private func listenForOtherMessages() {
        
        
    }
    
    func sendingSinglemessagStatus (taskid : String ,taskerid : String , Userid : String,usertype: String,messagearray:NSArray)
        
    {
        if(ChatSocket.status == .Connected)
        {
        let jsonString:NSDictionary = ["task":taskid,"tasker":taskerid,"user":Userid,"usertype" :usertype,"user_status":"2","messages":messagearray];
        ChatSocket.emit("single message status", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
    }
    
    func listenSingleMessageStatus() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("single message status") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Singlemessage message....\(dictData).........")
  NSNotificationCenter.defaultCenter().postNotificationName("readSinglemessagestatus", object: data[0])
        }
        
    }

    
    
    
    func SendingMessagestatus (typeofapp: String, Userid: String, taskerid : String,taskid : String)
    {
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["task":taskid,"tasker":taskerid,"user":Userid,"type":typeofapp];
            ChatSocket.emit("message status", jsonString)

        }
        else
        {
            ChatSocket.connect()
        }
    
      

    }
    
    
    func ListeningMessageStatus ()
    {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("message status") {[weak self] data, ack in
            dictData["message"]=data[0]
            print(". message.Status...\(dictData).........")
            //self?.Appdel.socketTypeNotification(data[0] as! NSDictionary) 
            NSNotificationCenter.defaultCenter().postNotificationName("readmessagestatus", object: data[0])

        }
        

    }
    func sendStopTypingMessage(Userid: String,taskerid: String,taskid : String) {
        
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["to":taskerid,"from":Userid,"user":Userid,"tasker":taskerid,"task":taskid
                ,"type":"user"]
            ChatSocket.emit("stop typing", jsonString)
            
        }
        else
        {
            ChatSocket.connect()
        }
    }
    func sendStartTypingMessage(Userid: String,taskerid: String,taskid : String ) {
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["to":taskerid,"from":Userid,"user":Userid,"tasker":taskerid,"task":taskid
                ,"type":"user"]
            ChatSocket.emit("start typing", jsonString)
            
        }
        else
        {
            ChatSocket.connect()
        }
        
    }
    
    func listenstartTyping() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("start typing") {[weak self] data, ack in
            dictData["message"]=data[0]
             print("..Start typing message....\(dictData).........")
            self?.Appdel.socketTypeNotification(data[0] as! NSDictionary)
        }
        
    }
   


    func listenstoptTyping() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("stop typing") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Stop typing message....\(dictData).........")
             self?.Appdel.socketStopTypeNotification(data[0] as! NSDictionary)
        }
        
    }

    
    
    
   }
