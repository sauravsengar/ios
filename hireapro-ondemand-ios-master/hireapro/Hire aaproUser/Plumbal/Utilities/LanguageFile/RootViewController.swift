//
//  RootViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 30/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    
    var Window:UIWindow=UIWindow()
    var notification:MPGNotification=MPGNotification()
    var buttonArray:NSArray=NSArray()
    let activityTypes: [NVActivityIndicatorType] = [
        .BallPulse]
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRectMake(0, 0, 0, 0),
                                                        type: .BallSpinFadeLoader)
    var AlertView:JTAlertView=JTAlertView()
    var Is_alertshown:Bool=Bool()
    
    //MARK: - Override Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        if(themes.Check_userID() != "") {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)
            
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.showPopup(_:)), name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.Show_rating(_:)), name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ConfigureNotification(_:)), name: "Message_notify", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushRating", object: nil)
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
     
        if(themes.Check_userID() != "") {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "Message_notify", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)

            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushRating", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self,name: "ShowPushPayment", object: nil)

            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.showPopup(_:)), name: "ShowPayment", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.Show_Alert(_:)), name: "ShowNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.Show_rating(_:)), name: "ShowRating", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ConfigureNotification(_:)), name: "Message_notify", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: Language_Notification as String, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodOfReceivedMessagePushNotification(_:)), name:"ReceivePushChatToRootView", object: nil)

            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivePushNotification(_:)), name:"ShowPushNotification", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceiveRatingNotification(_:)), name:"ShowPushRating", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.methodofReceivePaymentNotification(_:)), name:"ShowPushRating", object: nil)
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
       // NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
    //MARK: - Function
    
    func showProgress(){
        self.activityIndicatorView.color = themes.DarkRed()
        self.activityIndicatorView.size = CGSize(width: 75, height: 100)
        self.activityIndicatorView.center=CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2);
        self.activityIndicatorView.startAnimation()
        self.view.addSubview(activityIndicatorView)
    }
    
    func DismissProgress(){
        self.activityIndicatorView.stopAnimation()
        self.activityIndicatorView.removeFromSuperview()
    }
    
    func Logout(){
        Appdel.CheckDisconnect()
        self.DismissProgress()
        let appDomain: String = NSBundle.mainBundle().bundleIdentifier!
       // NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
      
        Appdel.MakeRootVc("SplashPage")
    }
    
    
    //MARK: - Notification Function
    
    func ConfigureNotification(notif:NSNotification)  {
        if(Is_alertshown == false) {
            Is_alertshown=true
            let userInfo:Dictionary<String,String!> = notif.userInfo as! Dictionary<String,String!>
            let Job_Id:NSString! = userInfo["Order_id"]
            let username:NSString! = userInfo["username"]
            let MessageString:NSString! = userInfo["Message"]
            buttonArray=["Reply"]
            notification=MPGNotification()
            notification = MPGNotification(title: "\(username)", subtitle: "\(MessageString)", backgroundColor: themes.ThemeColour(), iconImage: UIImage(named:"chaticon"))
            notification.setButtonConfiguration(MPGNotificationButtonConfigration(rawValue: buttonArray.count)! , withButtonTitles: buttonArray as! [NSArray])
            notification.duration = 4.0;
            notification.swipeToDismissEnabled = true;
            notification.titleColor=UIColor.whiteColor()
            notification.subtitleColor=UIColor.whiteColor()
            notification.animationType=MPGNotificationAnimationType.Drop
            notification.buttonHandler = {(notification: MPGNotification!, buttonIndex: Int) -> Void in
                Order_data.job_id=Job_Id
                var mainView: UIStoryboard!
                mainView = UIStoryboard(name: "Main", bundle: nil)
                let secondViewController = mainView.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
            notification.show()
        }
    }
    
    
    func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    Message_details.support_chatid = refer_id!
                    Message_details.admin_id = admin_id!
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_admin"))
                    alertView.addButton(themes.setLang("ok"), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }

    
    func methodOfReceivedMessagePushNotification(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        // or as! Sting or as! Int
        
        
        
        let check_userid: NSString = userInfo["from"]!
        let Chatmessage:NSString! = userInfo["message"]
        let taskid=userInfo["task"]
        
        
        
        if (check_userid == themes.getUserID())
        {
            
        }
        else
        {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                    
                }else{
                    
                    
                    if activeController.isKindOfClass(MessageViewController){
                        NSNotificationCenter.defaultCenter().postNotificationName("Dismisskeyboard", object: nil, userInfo: ["message":"","from":"","task":"","msgid":"" ,"taskerstus":""])
                        
                    }
                    
                    
                    Message_details.taskid = taskid!
                    Message_details.providerid = check_userid
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                    
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                    
                    
                    
                }
                
            }
        }
        
        
        
        
        
        
        
    }

    func methodofReceivePaymentNotification(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let Order_id:NSString! = userInfo["Order_id"]
        if(Order_id != nil) {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
        self.navigationController?.pushViewController(Controller, animated: true)
    }
    
    func methodofReceiveRatingNotification(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let Order_id:NSString! = userInfo["Order_id"]
        if(Order_id != nil) {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
        self.navigationController?.pushViewController(Controller, animated: true)
    }
    
    
    func methodofReceivePushNotification(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let Order_id:NSString! = userInfo["Order_id"]
        if(Order_id != nil) {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
            }else{
                let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                self.navigationController?.pushViewController(Controller, animated: true)
            }
        }
    }
    
    func methodOfReceivedMessageNotification(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        let _ = userInfo["message"]
        let taskid=userInfo["task"]
        if (check_userid != themes.getUserID())  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(MessageViewController){
                }else{
                    if activeController.isKindOfClass(MessageViewController){
                        NSNotificationCenter.defaultCenter().postNotificationName("Dismisskeyboard", object: nil, userInfo: ["message":"","from":"","task":"","msgid":"" ,"taskerstus":""])
                    }
                    let alertView = UNAlertView(title: Appname, message:themes.setLang("message_from_provider"))
                    alertView.addButton(themes.setLang("ok"), action: {
                        Message_details.taskid = taskid!
                        Message_details.providerid = check_userid
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }
    
    func Show_rating(notification: NSNotification) {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        if(Order_id != nil) {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(RatingsViewController){
            }else{
                if activeController.isKindOfClass(MessageViewController){
                    NSNotificationCenter.defaultCenter().postNotificationName("Dismisskeyboard", object: nil, userInfo: ["message":"","from":"","task":"","msgid":"" ,"taskerstus":""])
                }
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(themes.setLang("ok"), action: {
                    let Controller:RatingsViewController=self.storyboard?.instantiateViewControllerWithIdentifier("ReviewPoup") as! RatingsViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    
                })
                alertView.show()
            }
        }
    }
    
    func showPopup(notification: NSNotification) {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        if(Order_id != nil){
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(PaymentViewController){
                
            }else{
                if activeController.isKindOfClass(MessageViewController){
                    NSNotificationCenter.defaultCenter().postNotificationName("Dismisskeyboard", object: nil, userInfo: ["message":"","from":"","task":"","msgid":"" ,"taskerstus":""])
                }
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(themes.setLang("ok"), action: {
                    let Controller:PaymentViewController=self.storyboard?.instantiateViewControllerWithIdentifier("payment") as! PaymentViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                })
                alertView.show()
            }
        }
    }
    
    func Show_Alert(notification:NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let messageString:NSString! = userInfo["Message"]
        let Order_id:NSString! = userInfo["Order_id"]
        let action:NSString! = userInfo["Action"]

        if(Order_id != nil) {
            Root_Base.Job_ID=Order_id!
            Order_data.job_id = Order_id!
        }
        if let activeController = navigationController?.visibleViewController {
            if activeController.isKindOfClass(OrderDetailViewController){
                
            }else{
                if activeController.isKindOfClass(MessageViewController){
                    let alertView = UNAlertView(title: Appname, message:messageString as String)
                    alertView.addButton(themes.setLang("ok"), action: {
                        if action != "admin_notification"{

                        let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                        self.navigationController?.pushViewController(Controller, animated: true)
                    
                        }
                    })
                    alertView.show()

                    NSNotificationCenter.defaultCenter().postNotificationName("Dismisskeyboard", object: nil, userInfo: ["message":"","from":"","task":"","msgid":"" ,"taskerstus":""])
                }else if activeController.isKindOfClass(LocationViewController){
                    let alertView = UNAlertView(title: Appname, message:messageString as String)
                    alertView.addButton(themes.setLang("ok"), action: {
                        if action != "admin_notification"{

                        self.navigationController!.popViewControllerAnimated(true)
                        }
                    })
                    alertView.show()
                    
                }else{
                let alertView = UNAlertView(title: Appname, message:messageString as String)
                alertView.addButton(themes.setLang("ok"), action: {
                    if action != "admin_notification"{

                    let Controller:OrderDetailViewController=self.storyboard?.instantiateViewControllerWithIdentifier("OrderDetail") as! OrderDetailViewController
                    self.navigationController?.pushViewController(Controller, animated: true)
                    }
                    })
                alertView.show()
                }
            }
        }
    }
    
    
    func notificationView(notification: MPGNotification, didDismissWithButtonIndex buttonIndex: Int) {
        NSLog("Button Index = %ld", Int(buttonIndex))
    }
    
    //MARK: - Button Action
    
    @IBAction func didDismissSegue(segue: UIStoryboardSegue) {
        
    }
    
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
}

extension UITextField {
    func isMandatory(){
        let label = UILabel()
        label.frame = CGRectMake(0, 0, 10,self.frame.height)
        label.text = "*"
        label.textColor = UIColor.redColor()
        self.rightView = label
        self.rightViewMode = UITextFieldViewMode .Always
    }
}
