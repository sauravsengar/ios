
//
//  URLhandler.swift
//  Plumbal
//
//  Created by Casperon Tech on 07/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

//import SwiftyJSON



class URLhandler
{
 
    
    func makeCall(url: NSString,param:NSDictionary, completionHandler: (responseObject: NSDictionary?,error:NSError? ) -> ())
    
    
    {
        
//        var alamoFireManager : Alamofire.Manager?
//        
//        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
//        configuration.timeoutIntervalForRequest = 4 // seconds
//        configuration.timeoutIntervalForResource = 4
//        self.alamoFireManager = Alamofire.Manager(configuration: configuration)
        
        var user_id:NSString=NSString()
        
        if(themes.Check_userID() != "")
        {
           user_id=themes.getUserID()!
        }
        
    
        Alamofire.request(.POST, "\(url)", parameters: param  as? [String : AnyObject], headers: ["apptype": "ios", "apptoken":"\(Device_Token)", "userid":"\(user_id)"])
            .response { request, response, data, error in
                
                var Dictionay:NSDictionary?=NSDictionary()
                  do {
                    
                    Dictionay = try NSJSONSerialization.JSONObjectWithData(
                        
                        data!,
                        
                        options: NSJSONReadingOptions.MutableContainers
                        
                        ) as? NSDictionary
                    
                    
                    let status:NSString?=Dictionay!.objectForKey("is_dead") as? NSString


                    if(status == nil)
                    {
                        
                        print("the dictionary is \(param)....\(url)...\(Dictionay)")

                        completionHandler(responseObject: Dictionay as NSDictionary?, error: error )
                    }
                    else
                    {
                        print("the dictionary is \(url)........>>>\(param)>>>>>.......\(Dictionay)")

                        themes.AlertView(themes.setLang("log_out1"), Message: themes.setLang("log_out2"), ButtonTitle:  kOk)
                        let RootBase:RootViewController=RootViewController()
                        RootBase.Logout()
                    }

                }

                 
                catch let error as NSError {
                    print("the dictionary is \(param)....\(url)...\(Dictionay)")

                    // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
                    print("A JSON parsing error occurred, here are the details:\n \(error)")
                    Dictionay=nil
                    completionHandler(responseObject: Dictionay as NSDictionary?, error: error )

                }
                
 
                Dictionay=nil
                
  
//
//        Alamofire.request(.POST, "\(url)", parameters: param  as? [String : AnyObject])
//            .responseJSON { response in
//                
//                
//                
//
//                
          }

        
        
        
    }
    
    func makeGetCall(url: NSString, completionHandler: (responseObject: NSDictionary? ) -> ())
    {
        
        Alamofire.request(.GET, "\(url)")
            .responseJSON { response in
                
                completionHandler(responseObject: response.result.value as? NSDictionary )

        }
    
    }
    
 
    
  /*  func make_UploadImage(url: NSString, param:NSDictionary, completionHandler: (responseObject: NSDictionary?,error:NSError? ) -> ())
    {
        
        var user_id:NSString=NSString()
        
        if(themes.Check_userID() != nil)
        {
            user_id=themes.getUserID()!
        }
        
        
        Alamofire.upload(.POST, URLString: url,
            multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(fileURL: param .objectForKey("image")as! NSData, name: "photo")
                                multipartFormData.appendBodyPart(data: Constants.AuthKey.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"authKey")
                multipartFormData.appendBodyPart(data: "\(16)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"idUserChallenge")
                multipartFormData.appendBodyPart(data: "comment".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"comment")
                multipartFormData.appendBodyPart(data:"\(0.00)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"latitude")
                multipartFormData.appendBodyPart(data:"\(0.00)".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"longitude")
                multipartFormData.appendBodyPart(data:"India".dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, name :"location")
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { request, response, JSON, error in
                        
                        
                    }
                case .Failure(let encodingError): break
                    
                }
            })
        
        
        
    }*/

    
    
//    
//    
//    func uploadImage(urlString:NSString, parameters:NSDictionary, imgData:NSData) -> (NSURLResponse, NSError) {
//        
//        // create url request to send
//        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString as String)!)
//        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
//        let boundaryConstant = "myRandomBoundary12345";
//        let contentType = "multipart/form-data;boundary="+boundaryConstant
//        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
//        
//        
//        
//        // create upload data to send
//        let uploadData = NSMutableData()
//        
//        // add image
//        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//        uploadData.appendData("Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//        uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//        uploadData.appendData(imgData)
//        
//        // add parameters
//        for (key, value) in parameters {
//            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//            uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
//        }
//        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
//        
//        
//        
//        // return URLRequestConvertible and NSData
//        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
//    }
    



}

 
