//
//  DLDemoRootViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit

class DLDemoRootViewController: DLHamburguerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DLDemoRootViewController.SetRootView(_:)), name: "MakerootView", object: nil)
        
        // Do any additional setup after loading the view.
    }
    func SetRootView(notifi:NSNotification)
    {
        print("the obj is \(notifi.object)")
        self.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("\(notifi.object!)")
        self.menuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVCID")

    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func awakeFromNib() {
        self.contentViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("HomePageVCID"))! as UIViewController
        self.menuViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("MenuVCID"))! as UIViewController
    }
}
