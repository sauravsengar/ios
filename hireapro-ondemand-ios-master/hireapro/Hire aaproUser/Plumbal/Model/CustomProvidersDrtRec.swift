//
//  CustomProvidersDrtRec.swift
//  Plumbal
//
//  Created by Casperon on 22/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CustomProvidersDrtRec: NSObject {
    
    var searchlat : NSString = NSString()
    var searchlng : NSString = NSString()
    var SubCatid  : NSString = NSString()
    var MainCatid : NSString = NSString()
    var SubCatname: NSString = NSString()
    var Cat_type  : NSString = NSString()
    var Task_id   : NSString = NSString()
    var Tasker_id : NSString = NSString()
    var GetMax_radius : NSString = NSString()
    
    var ProvidersNameArray : NSMutableArray = NSMutableArray()
    var ProviderimageArray : NSMutableArray = NSMutableArray()
    var ProvideridArray : NSMutableArray = NSMutableArray()
    var ProRadiusArray : NSMutableArray = NSMutableArray()
    var ProAmountArray : NSMutableArray = NSMutableArray()
    var ProRatingArray : NSMutableArray = NSMutableArray()
    var ProBadgesArray  : NSMutableArray = NSMutableArray()
    var ServiceTaxArray : NSMutableArray = NSMutableArray()
    var ProminAmountArray : NSMutableArray = NSMutableArray()
    var ProvCatTypeArray : NSMutableArray = NSMutableArray()
    
    

}
