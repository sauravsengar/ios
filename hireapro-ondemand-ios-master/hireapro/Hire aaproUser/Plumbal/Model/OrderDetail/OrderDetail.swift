//
//  OrderDetail.swift
//  Plumbal
//
//  Created by Casperon Tech on 27/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class OrderDetail: NSObject {
    
    var avg_rating:NSString=NSString()
    var Provider_Email:NSString=NSString()
    
    var userJobLocation : NSString = NSString()

    var bio:NSString=NSString()
     var booking_address:NSDictionary=NSDictionary()
    var date:NSString=NSString()
    var hour:NSString=NSString()
    var flat:NSString=NSString()
    var serv_tax:NSString=NSString()
    var job_id:NSString=NSString()
    var lat:NSString=NSString()
    var lon:NSString=NSString()
    var job_status:NSString=NSString()
    var jobntn_grpStatus:NSString=NSString()
     var provider_email:NSString=NSString()
    var provider_image:NSString=NSString()
     var provider_mobile:NSString=NSString()
    var provider_name:NSString=NSString()
    var time:NSString=NSString()
    var location:NSString=NSString()
    var provider_lat = NSString()
    var provider_long = NSString()

    var provider_id:NSString=NSString()
    var task_id: NSString = NSString()
    var Provider_service_type:NSString = NSString()
    
    var Provider_minrate:NSString = NSString()
    var Provider_hourlyrate:NSString = NSString()

var cat_type = NSString()
 

}
class TrackingDetail:NSObject{
    var userLat = Double()
    var userLong = Double()
    var taskId = String()
    var lastDriving = String()
    var bearing = String()
    
    
}
