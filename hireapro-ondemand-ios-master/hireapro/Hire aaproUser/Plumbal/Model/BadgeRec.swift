//
//  BadgeRec.swift
//  Plumbal
//
//  Created by Casperon on 24/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class BadgeRec: NSObject {

    var badge_id:String = ""
    var badge_name:String = ""
    var badge_image:String = ""
    var update_status:String = ""
    
    
    init(badgeid:String,badgename:String,badgeimage:String,upstatus:String) {
        badge_id = badgeid
        badge_name = badgename
         update_status = upstatus
        badge_image = badgeimage
       
        super.init()
        
    }
    

}
