//
//  DBFile.swift
//  CoredataLibrary
//
//  Created by CASPERON on 01/08/16.
//  Copyright © 2016 CASPERON. All rights reserved.
//

import UIKit
import CoreData

class DBFile: NSObject {

    var managedObjectContext: NSManagedObjectContext!
    var appDelegate : AppDelegate!
  var dictValues : NSMutableDictionary!
    var dataArray : NSMutableArray!
    
    
    
    
    
    func saveData(entityName: String, ValueStr valueArr:NSMutableArray) {
        appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.managedObjectContext = appDelegate.managedObjectContext
        //Setting Entity to be Queried
        if valueArr.count > 0 {
            for i in 0..<valueArr.count {
                let request: NSFetchRequest = NSFetchRequest()
                //Setting Entity to be Queried
                let entity: NSEntityDescription = NSEntityDescription.entityForName(entityName, inManagedObjectContext: self.managedObjectContext)!
                request.entity = entity
                let pred: NSPredicate = NSPredicate(format:"providerid=%@", valueArr[i].valueForKey("providerid") as! String)
                request.predicate = pred
               let error: NSError? = nil
                let matches: NSArray = try! managedObjectContext.executeFetchRequest(request)
                if (entityName == "Provider_Table") {
                    if  (matches.count >= 1) || error != nil {
                        // Abnormal
                        print("Error accessing database:")
                    }
                    else if 0 == matches.count {
                        
                        if (matches.valueForKey("providerid").containsObject(valueArr[i].valueForKey("providerid"))) {
                            //notify duplicates
                            NSLog("does not allow duplicates")

                            return;
                         }
                        else
                        {
                           let new: Providertable = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext:managedObjectContext) as! Providertable
                            new.providerid = valueArr[i]["providerid"] as! String
                            do {
                                try managedObjectContext.save()
                            } catch let error {
                                print("Could not cache the response \(error)")
                            }                }
                           }
                        }
                       
            }
        }
        
}

    
    
    
func arr(entityName: String) -> NSMutableArray {
    
    appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    self.managedObjectContext = appDelegate.managedObjectContext
    let fetchRequest: NSFetchRequest = NSFetchRequest()
    dictValues = NSMutableDictionary()
        //Setting Entity to be Queried
    let entity: NSEntityDescription = NSEntityDescription.entityForName(entityName, inManagedObjectContext: self.managedObjectContext)!
    
    
    fetchRequest.entity = entity
    
    
    var error : NSError
    dataArray = NSMutableArray()
    
    let results: NSArray = try! self.managedObjectContext.executeFetchRequest(fetchRequest) as NSArray
    for data  in results as! [Providertable]  {
        dictValues = NSMutableDictionary ()
        dictValues["providerid"] = data.providerid as String
       
        dataArray.addObject(dictValues)
        dictValues = nil
    }

    return dataArray
    
    }
    
    func UpdateData (updateval:String ) {
        
        appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.managedObjectContext = appDelegate.managedObjectContext
        //Setting Entity to be Queried
        
        let request: NSFetchRequest = NSFetchRequest()
        //Setting Entity to be Queried
        let entity: NSEntityDescription = NSEntityDescription.entityForName("Provider_Table", inManagedObjectContext: self.managedObjectContext)!
        request.entity = entity
        let fetchedProducts = try! managedObjectContext.executeFetchRequest(request)
        for data in fetchedProducts as! [Providertable] {
            data.providerid = updateval
        }
        
        
        do {
            try managedObjectContext.save()
        } catch let error {
            print("Could not cache the response \(error)")
        }
    }

    
    
    func deleteUser(entityName: String) {
        appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        self.managedObjectContext = appDelegate.managedObjectContext
        let productEntity: NSEntityDescription = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedObjectContext)!
        let fetch: NSFetchRequest = NSFetchRequest()
        fetch.entity = productEntity
        
        let fetchedProducts: [AnyObject] = try! managedObjectContext.executeFetchRequest(fetch)
        for data in fetchedProducts as! [Providertable] {
            managedObjectContext.deleteObject(data)
        }
    }
}
