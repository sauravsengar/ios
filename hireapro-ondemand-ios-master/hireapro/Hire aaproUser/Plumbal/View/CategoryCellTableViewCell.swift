//
//  CategoryCellTableViewCell.swift
//  Plumbal
//
//  Created by Casperon on 17/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CategoryCellTableViewCell: UITableViewCell {

    @IBOutlet var cat_img: UIImageView!
    @IBOutlet var cat_lable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCategoryTableCell(objOpenRec:SearchCategory){
        cat_lable.text=objOpenRec.cat as String
        cat_img.sd_setImageWithURL(NSURL.init(string:objOpenRec.cat_icon ), completed: themes.block)
       
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
