//
//  CustomProlistTableViewCell.swift
//  Plumbal
//
//  Created by Casperon on 22/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class CustomProlistTableViewCell: UITableViewCell {

    @IBOutlet var ratingcount: UILabel!
    @IBOutlet var RatView: UIView!
    @IBOutlet var badgeCollection: UICollectionView!
    @IBOutlet var booknow: CustomButton!
    @IBOutlet var booklater: UIButton!
    @IBOutlet var chatbtn: UIButton!
    @IBOutlet var prov_detail: UILabel!
    @IBOutlet var prov_name: UILabel!
    @IBOutlet var providerimg: UIImageView!
  
    
    @IBOutlet var badgecoll_bottomConst: NSLayoutConstraint!
    @IBOutlet var badgecol_heightconst: NSLayoutConstraint!
    func setCollectionViewDataSourceDelegate
        <D: protocol<UICollectionViewDataSource, UICollectionViewDelegate>>
        (dataSourceDelegate: D, forRow row: Int) {
        let regnib = UINib.init(nibName: "custombadgeCollectionViewCell", bundle: nil)
        badgeCollection.registerNib(regnib, forCellWithReuseIdentifier: "Custombadge")
        badgeCollection.delegate = dataSourceDelegate
        badgeCollection.dataSource = dataSourceDelegate
        badgeCollection.tag = row
        badgeCollection.reloadData()
    }

    override func awakeFromNib() {
     
        

           super.awakeFromNib()
    }

      override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
   

        // Configure the view for the selected state
    }
    
}
