//
//  BadgeCell.swift
//  Plumbal
//
//  Created by Casperon on 04/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class BadgeCell: UICollectionViewCell {

    @IBOutlet var lablebadge: UIButton!
    @IBOutlet var imgbadge: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
