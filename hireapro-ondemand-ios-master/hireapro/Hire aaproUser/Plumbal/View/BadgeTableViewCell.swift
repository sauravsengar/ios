//
//  BadgeTableViewCell.swift
//  Plumbal
//
//  Created by Casperon iOS on 22/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class BadgeTableViewCell: UITableViewCell {

    @IBOutlet var imgbatch: UIImageView!
    @IBOutlet weak var badge_image: UIImageView!
    @IBOutlet weak var select_btn: UIButton!
    @IBOutlet weak var badge_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
