//
//  ListBookingView.swift
//  Plumbal
//
//  Created by Casperon on 29/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit
protocol ListBookingViewDelegate {
    
    func pressedCancel(sender: ListBookingView)
    func pressBooking (confimDate: NSString, Confirmtime : NSString, Instructionstr: NSString)
    
}
class ListBookingView: UIViewController,UITextViewDelegate {
    var servicetax : String!
    var taskernamestr : NSString!
    
    @IBOutlet weak var service: UILabel!
    
    
    @IBOutlet var cancel: UIButton!
    @IBOutlet var confirm: UIButton!
    @IBOutlet var bottomview: SetColorView!
    @IBOutlet var instruction: UITextView!
    @IBOutlet var bookingdate: UILabel!
    @IBOutlet var bookingtime: UILabel!
    @IBOutlet var taskername: UILabel!
    var tag : Int!
    var bookingTime : String = String()
    var TextViewPlaceHolder = ""
    var delegate:ListBookingViewDelegate?
    
    @IBAction func confirmBooking(sender: AnyObject) {
        tag = tag + 1
        if tag == 1
        {
        self.delegate?.pressedCancel(self)
        self.delegate?.pressBooking(bookingdate.text!,Confirmtime: bookingTime,Instructionstr: instruction.text!)
        }
        else{
            
        }
        
    }
    
    @IBAction func cancelbooking(sender: AnyObject) {
        self.delegate?.pressedCancel(self)
        
    }
    override func viewWillAppear(animated: Bool) {
        tag = 0
        self.view.layer.cornerRadius = 5
        view.layer.borderWidth=1.0
        view.layer.borderColor=themes.ThemeColour().CGColor
        self.view.layer.masksToBounds = true
        
        TextViewPlaceHolder=themes.setLang("enter_instruc")
        
        let date = NSDate()
        let formatter = NSDateFormatter()
        
        formatter.dateFormat = "MM/dd/yyyy"
        
        let result = formatter.stringFromDate(date)
        
        formatter.dateFormat = "HH:mm"
        let time  = formatter.stringFromDate(date)
        
        bookingTime = time
        formatter.dateFormat = "h:mm a"
        let timenew  = formatter.stringFromDate(date)
        
        bookingdate.text = result
        self.bookingtime.text = timenew
        self.taskername.text = taskernamestr as String
        self.service.text = servicetax + "%"
        
        
        if( self.instruction.text == "") {
            self.instruction.text=TextViewPlaceHolder
        }
        instruction.delegate=self
        instruction.layer.borderWidth=1.0
        instruction.layer.cornerRadius = 5
        instruction.layer.borderColor=PlumberThemeColor.CGColor
        instruction.font=PlumberMediumFont
        
        bottomview.layer.cornerRadius = 5
        bottomview.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
        
        
    }
    //
    //    func todaysEventString(dateString: String) -> String {
    //        formatter.dateFormat = "HH:mm"
    //        let time  = formatter.stringFromDate(date)
    //    }
    
    override func viewDidLoad() {
        
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        
        if(instruction.text == TextViewPlaceHolder) {
            instruction.textColor=UIColor.blackColor()
            instruction.text=""
        }
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        
        if(instruction.text == "") {
            instruction.textColor=PlumberThemeColor
            instruction.text=TextViewPlaceHolder
        }
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
}
