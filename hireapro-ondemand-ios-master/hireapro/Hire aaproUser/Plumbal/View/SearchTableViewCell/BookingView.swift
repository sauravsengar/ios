//
//  BookingView.swift
//  Plumbal
//
//  Created by Casperon iOS on 15/3/2017.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit
protocol BookingViewDelegate {
    
    func pressedCancel(sender: BookingView)
    func pressBooking (confimDate: NSString, Confirmtime : NSString, Instructionstr: NSString)
    
}


class BookingView: UIViewController,UITextViewDelegate {
    var servicetax : String!
    var taskernamestr : NSString!
    
    @IBOutlet weak var service: UILabel!
  
    @IBOutlet var title_lbl: UILabel!
    @IBOutlet var provi_lbl: UILabel!
    @IBOutlet var time_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var serviceFee_lbl: UILabel!
    @IBOutlet var cancel: UIButton!
    @IBOutlet var confirm: UIButton!
    
    @IBOutlet var bottomview: SetColorView!
    @IBOutlet var instruction: UITextView!
    @IBOutlet var bookingdate: UILabel!
    @IBOutlet var bookingtime: UILabel!
    @IBOutlet var taskername: UILabel!
    var bookingTime : String = String()
    var TextViewPlaceHolder = ""
    var delegate:BookingViewDelegate?

    @IBAction func confirmBooking(sender: AnyObject) {
        self.delegate?.pressedCancel(self)
        self.delegate?.pressBooking(bookingdate.text!,Confirmtime: bookingTime,Instructionstr: instruction.text!)
        

    }
    
    @IBAction func cancelbooking(sender: AnyObject) {
        self.delegate?.pressedCancel(self)

    }
    override func viewWillAppear(animated: Bool) {
        
        
        self.view.layer.cornerRadius = 5
        view.layer.borderWidth=1.0
        view.layer.borderColor=themes.ThemeColour().CGColor
        self.view.layer.masksToBounds = true

        TextViewPlaceHolder=themes.setLang("enter_instruc")

        let date = NSDate()
        let formatter = NSDateFormatter()
        
        formatter.dateFormat = "MM/dd/yyyy"
        
        let result = formatter.stringFromDate(date)
        
        formatter.dateFormat = "HH:mm"
        let time  = formatter.stringFromDate(date)
        
        bookingTime = time
        formatter.dateFormat = "h:mm a"
        let timenew  = formatter.stringFromDate(date)
        
        bookingdate.text = result
        self.bookingtime.text = timenew
        self.taskername.text = taskernamestr as String
        self.service.text = servicetax + "%"
      
        
        if( self.instruction.text == "") {
            self.instruction.text=TextViewPlaceHolder
        }
        instruction.delegate=self
        instruction.layer.borderWidth=1.0
        instruction.layer.cornerRadius = 5
        instruction.layer.borderColor=PlumberThemeColor.CGColor
        instruction.font=PlumberMediumFont
        
         bottomview.layer.cornerRadius = 5
        bottomview.layer.masksToBounds = true
        

        // Do any additional setup after loading the view.
        
        
    }
//    
//    func todaysEventString(dateString: String) -> String {
//        formatter.dateFormat = "HH:mm"
//        let time  = formatter.stringFromDate(date)
//    }
    
    override func viewDidLoad() {
        
        self.title_lbl.text = themes.setLang("job_conf")
        self.provi_lbl.text = themes.setLang("Provider")
        self.time_lbl.text = themes.setLang("time")
        self.date_lbl.text = themes.setLang("date")
        self.serviceFee_lbl.text = themes.setLang("service_fee")
        self.confirm.setTitle(themes.setLang("confirm_book"), forState: .Normal)
        self.cancel.setTitle(themes.setLang("cancle_book"), forState: .Normal)
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
      
        if(instruction.text == TextViewPlaceHolder) {
            instruction.textColor=UIColor.blackColor()
            instruction.text=""
        }
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
     
        if(instruction.text == "") {
            instruction.textColor=PlumberThemeColor
            instruction.text=TextViewPlaceHolder
        }
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
