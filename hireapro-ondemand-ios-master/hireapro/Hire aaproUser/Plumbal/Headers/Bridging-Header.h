//
//  Bridging-Header.h
//  Plumbal
//
//  Created by Casperon Tech on 30/09/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

#ifndef Bridging_Header_h
#define Bridging_Header_h

//Importing Contant Header File
#import "NMRangeSlider.h"
#import "RatingView.h"

#import "Constant.h"
#import "HCSStarRatingView.h"


//For custom Slide menu
#import "SWRevealViewController.h"
//For Facebook login
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//For calendar View in schedule an appointment page
#import "CGCalendarView.h"
#import "CGCalendarCell.h"

#import "MapRequestModel.h"
#import "OpenInGoogleMapsController.h"
#import "UIImageView+WebCache.h"

//Share kit for sharing post in facebook

#import <FBSDKShareKit/FBSDKShareKit.h>


//Messenger kit for sharing messages

#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <MapKit/MKFoundation.h>
#import <MapKit/MapKit.h>
//parallax ScrollView

//#import "A3ParallaxScrollView.h"


//Parallax Scroll

#import "ParallaxHeaderView.h"


//Horizontal Picker View
#import "AKPickerView.h"

//Star Rating View

#import "TPFloatRatingView.h"



//Google map

#import <GoogleMaps/GoogleMaps.h>

//Xmpp Framework

#import "XMPPRosterCoreDataStorage.h"
#import "XMPP.h"
#import "XMPPRoster.h"
#import "DDLog.h"
#import "DDTTYLogger.h"

//Pull To refresh at bottom
#import "MNMBottomPullToRefreshManager.h"

//XML Reader
#import "XMLReader.h"

//Chat ViewController
#import "JSQMessages.h"
#import "JSQMessageData.h"

//PopUp

#import "CCMPopupSegue.h"
#import "CCMBorderView.h"
#import "CCMPopupTransitioning.h"
#import "STCollapseTableView.h"




//AlertView
#import "JTAlertView.h"
#import "RKDropdownAlert.h"

#import "CardIO.h"

@import AudioToolbox;
@import AVFoundation;
@import CoreMedia;
@import CoreVideo;
@import MobileCoreServices;

#import "PayPalMobile.h"

//Drop Down
#import "NIDropDown.h"


#import "Stripe.h"

//Chat Page Header Files

#import "Message.h"

#import "Chat.h"
#import "MessageCell.h"
#import "TableArray.h"
#import "MessageGateway.h"
#import "ChatCell.h"
#import "Chat.h"
#import "LocalStorage.h"
#import "Inputbar.h"
#import "DAKeyboardControl.h"

#import "Reachability.h"


#import "MPGNotification.h"





#endif /* Bridging_Header_h */

