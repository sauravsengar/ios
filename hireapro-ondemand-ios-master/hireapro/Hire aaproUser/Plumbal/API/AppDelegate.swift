//
//  AppDelegate.swift
//  Plumbal
//
//  Created by Casperon Tech on 30/09/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
import CoreData
//import HockeySDK

let dbfileobj:DBFile = DBFile()
let signup:Signup=Signup()
var constant:Constant=Constant()
let Invite:Invite_Freinds=Invite_Freinds()
let Changepass:ChangePassword=ChangePassword()
let MyWallet:Wallet=Wallet()
let Transaction_Stat:Transaction=Transaction()
let OTP_sta:OTP=OTP()
let Menu_dataMenu=Menu()
var Addaddress_Data=Addaddress()
var Schedule_Data:ScheduleView=ScheduleView()
var Category_Data:Category_=Category_()
var Order_data:Order=Order()
var OrderDetail_data:OrderDetail=OrderDetail()
var trackingDetail : TrackingDetail = TrackingDetail()
var provider_map_details = ProviderMapDetails()
var themes:Themes=Themes()
var Root_Base:RootBase=RootBase()
var Payment_Detail:Payment=Payment()
var Language_handler:Languagehandler=Languagehandler()
var cusProviderRec:CustomProvidersDrtRec=CustomProvidersDrtRec()
var FilterRec : SaveFilterRecords = SaveFilterRecords()
var Terms_Details:Terms=Terms()
var Provider_Detail:ProviderDetail=ProviderDetail()
var Message_details:MessageView=MessageView()
var Nodatastatus:NSString=NSString()
let Edit_Prof:EditProfile=EditProfile()
var FB_Regis:FacebookRegister=FacebookRegister()
var Home_Data:HomePage=HomePage()
var Device_Token:NSString=NSString()
var Common_Chatid:NSString=NSString()
var Getpagestr : NSString = NSString()
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,XMPPRosterDelegate, XMPPStreamDelegate {
    //XMPP
    var isSocketConnected : Bool = Bool()
    var ConnectionTimer : NSTimer = NSTimer()
    
    let xmppStream = XMPPStream()
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster
    var reach: Reachability?
    var Root_base:RootViewController=RootViewController()
    var window: UIWindow?
    override init() {
        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
    }
    var job_id:NSString=NSString()
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().idleTimerDisabled = true
        
        
        //Paypal
        themes.saveLanguage(kLanguage)
        themes.SetLanguageToApp()
        
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let rootView: UINavigationController = sb.instantiateViewControllerWithIdentifier("RootVCID") as! UINavigationController
        UIView.transitionWithView(self.window!, duration: 0.2, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            appDel.window?.rootViewController=rootView
            }, completion: nil)
        
        
        PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction:"YOUR_CLIENT_ID_FOR_PRODUCTION",PayPalEnvironmentSandbox:"Ae5Sbc0BdkIcFxJyfU18jFLSxywi7XZg3ZnVIN0iMxcb7tabr9YPEL1abLWcRW3oARVeZIqmlB4Kip2x"])
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "methodOfChatFromApp", object: nil)
        
        //Xmpp
        DDLog.addLogger(DDTTYLogger.sharedInstance())
        
        //  setupStream()
        window?.backgroundColor=UIColor.clearColor()
        GMSServices.provideAPIKey("\(constant.GooglemapAPI)")
        let types: UIUserNotificationType = UIUserNotificationType.Alert.union(UIUserNotificationType.Badge).union(UIUserNotificationType.Sound)
        
        let settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
        
        application.registerUserNotificationSettings( settings )
        application.registerForRemoteNotifications()
        
        // application.registerForRemoteNotificationTypes(types: UIRemoteNotificationType)
        
//        BITHockeyManager.sharedHockeyManager().configureWithIdentifier("bd11d5de32f74810a710511f6580222f")
//        BITHockeyManager.sharedHockeyManager().startManager()
//        BITHockeyManager.sharedHockeyManager().authenticator.authenticateInstallation()
//      
        if(launchOptions != nil)
        {
            
            let localNotif = launchOptions![UIApplicationLaunchOptionsRemoteNotificationKey]
                as? Dictionary<NSObject,AnyObject>
            if  (localNotif != nil)
                
            {
                
                let checkuserid = themes.CheckNullValue(localNotif!["user"])!
                let userid_fromsupport = themes.CheckNullValue(localNotif!["reference"])!
                if userid_fromsupport != ""
                {
                    
                    let getreferenceid : String = themes.CheckNullValue(localNotif!["reference"])!
                    
                    let SupportMessage: NSArray = localNotif!["messages"] as! NSArray
                    let getsupportmsg = themes.CheckNullValue(SupportMessage[0].objectForKey("message")!)
                    
                    let getdate = themes.CheckNullValue(SupportMessage[0].objectForKey("date")!)
                    let from_id:NSString = themes.CheckNullValue(SupportMessage[0].objectForKey("from")!)!
                   
                    
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","admin":"\(from_id)","reference":"\( getreferenceid)"])
                    }
                }

               else  if (themes.getUserID() == checkuserid)
                {
                    var Message_Notice:NSString=NSString()
                    var taskid:NSString=NSString()
                    var messageid : NSString = NSString()
                    var tasker_status : NSString = NSString()
                    var dateStr : NSString = NSString()
                    var taskerid : NSString = NSString()
                    
                    let status : NSString = themes.CheckNullValue(localNotif!["status"])!
                    if status == "1"
                    {
                        let ChatMessage:NSArray =  localNotif!["messages"] as! NSArray
                        taskid = localNotif!["task"] as! NSString
                        taskerid = localNotif!["tasker"] as! NSString
                        
                        tasker_status = themes.CheckNullValue(ChatMessage[0].objectForKey("tasker_status"))!
                        
                        Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
                        messageid = ChatMessage[0].objectForKey("_id") as! NSString
                        dateStr = ChatMessage[0].objectForKey("date") as! NSString
                        
                        let userid : NSString = themes.CheckNullValue(ChatMessage[0].objectForKey("from"))!
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            
                            NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChatToRootView", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                        }
                    }
                }
                else{
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        self.APNSNotification(localNotif! as NSDictionary)
                    }
                }
                
            }
            else{
                
                
            }
            //}
        }
        //CheckReachability()
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func ReconnectMethod()
    {
        
        if(themes.Check_userID() != "")
        {
            NSThread.detachNewThreadSelector(#selector(EstablishCohnnection), toTarget: self, withObject: nil)
        }
    }
    
    
    func EstablishCohnnection()
    {
        if(SocketIOManager.sharedInstance.ChatSocket.status == .NotConnected || SocketIOManager.sharedInstance.ChatSocket.status ==  .Closed)
            
        {
            if(themes.Check_userID() != "")
            {
                //Listen To server side Chat related notification
                
                SocketIOManager.sharedInstance.establishChatConnection()
                
            }
            
            
        }
        
        if(SocketIOManager.sharedInstance.socket.status == .NotConnected || SocketIOManager.sharedInstance.socket.status ==  .Closed)
        {
            
            
            //Listen To server side Job related notification
            
            if(themes.Check_userID() != "")
            {  SocketIOManager.sharedInstance.establishConnection()
                
                
            }
            
        }
        
    }
    
    
    
    
    func urlPathToDictionary(path:String) -> [String:String]? {
        //Get the string everything after the :// of the URL.
        let stringNoPrefix = path.componentsSeparatedByString("://").last
        
        //Get all the parts of the url
        if var parts = stringNoPrefix?.componentsSeparatedByString("/") {
            //Make sure the last object isn't empty
            if parts.last == "" {
                parts.removeLast()
            }
            
            if parts.count % 2 != 0 { //Make sure that the array has an even number
                return nil
            }
            
            var dict = [String:String]()
            var key:String = ""
            
            //Add all our parts to the dictionary
            for (index, part) in parts.enumerate() {
                if index % 2 != 0 {
                    key = part
                } else {
                    dict[key] = part
                }
            }
            
            return dict
        }
        
        return nil
    }
    
    func CheckReachability()
    {
        
        
        // Allocate a reachability object
        self.reach = Reachability.reachabilityForInternetConnection()
        
        // Set the blocks
        //        let activeController = navigationController!.visibleViewController
        //
        self.reach!.reachableBlock = {
            
            (let reach: Reachability!) -> Void in
            
            // keep in mind this is called on a background thread
            // and if you are updating the UI it needs to happen
            // on the main thread, like this:
            dispatch_async(dispatch_get_main_queue()) {
                print("REACHABLE!")
                //                let image = UIImage(named: "NoNetworkConn")
                //                activeController!.view.makeToast(message:kNetworkConnectedMsg, duration: 3, position:HRToastActivityPositionDefault, title: "Hurray", image: image!)
                
                //                 self.setupStream()
                self.CheckConnect()
                
            }
        }
        
        self.reach!.unreachableBlock = {
            (let reach: Reachability!) -> Void in
            print("UNREACHABLE! ")
            //          let image = UIImage(named: "NoNetworkConn")
            //          activeController!.view.makeToast(message:kNetworkErrorMsg, duration: 3, position:HRToastActivityPositionDefault, title: "Oops !!!!", image: image!)
            
        }
        
        self.reach!.startNotifier()
    }
    
    func methodOfChatFromApp(notification: NSNotification){
        
        
    }
    
    func socketTypeNotification(dict:NSDictionary)
    {
        
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = themes.CheckNullValue(chatDict.objectForKey("task"))!
        let Userid:NSString = dict.objectForKey("user") as! NSString
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveTypingMessage", object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
        
    }
    
    func socketStopTypeNotification(dict:NSDictionary)
    {
        
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = themes.CheckNullValue(chatDict.objectForKey("task"))!
        
        let Userid:NSString = dict.objectForKey("user") as! NSString
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveStopTypingMessage", object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
        
    }
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        if(View.tag == 1)
        {
            
            switch buttonIndex{
                
            case 0:
                NSNotificationCenter.defaultCenter().postNotificationName("ShowPayment", object: nil, userInfo: ["job_id":"\(job_id)"])
                break;
            default:
                break;
                //Some code here..
                
            }
            if(View.tag == 2)
            {
                switch buttonIndex{
                case 0:
                    NSNotificationCenter.defaultCenter().postNotificationName("ShowRating", object: nil, userInfo: ["job_id":"\(job_id)"])
                    break;
                default:
                    break;
                    //Some code here..
                }
            }
        }
    }
    
    
    func application(application: UIApplication,
                     openURL url: NSURL,
                             sourceApplication: String?,
                             annotation: AnyObject) -> Bool {
        print("Launched with URL: \(url.absoluteString)")
        
        _ = self.urlPathToDictionary(url.absoluteString)
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            application,
            openURL: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
    }
    
    func MakeRootVc(ViewIdStr:NSString){
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let rootView: UINavigationController = sb.instantiateViewControllerWithIdentifier(ViewIdStr as String) as! UINavigationController
        UIView.transitionWithView(self.window!, duration: 0.2, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            appDel.window?.rootViewController=rootView
            }, completion: nil)
    }
    
    
    
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {
        
        
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>" )
        
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        print("the device token is \(deviceTokenString)")
        
        Device_Token = "\(deviceTokenString)"
        
        
        if(Device_Token == "")
        {
            Device_Token="Simulator Signup"
        }
        
        
        
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        
        let userInfoDict:NSDictionary?=userInfo
        
        print("get userinformation=\(userInfoDict)")
        let checkuserid = themes.CheckNullValue(userInfoDict!.objectForKey("user"))!
        let userid_fromsupport = themes.CheckNullValue(userInfoDict!.objectForKey("reference"))!

        
        if userInfoDict != nil
        {
            
            if userid_fromsupport != ""
            {
                
                
                let getrefernceid : String = (themes.CheckNullValue(userInfoDict!.objectForKey("reference")!))!
                
                let SupportMessage: NSArray = userInfoDict!.objectForKey("messages") as! NSArray
                let getsupportmsg = themes.CheckNullValue(SupportMessage[0].objectForKey("message")!)
                
                let getdate = themes.CheckNullValue(SupportMessage[0].objectForKey("date")!)
                let from_id:NSString = themes.CheckNullValue(SupportMessage[0].objectForKey("from")!)!
                
                
                NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChat", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","reference":"\(getrefernceid)"])
                NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","admin":"\(from_id)","reference":"\( getrefernceid)"])
                
            }
           else if (themes.getUserID() == checkuserid)
            {
                
                var Message_Notice:NSString=NSString()
                var taskid:NSString=NSString()
                var messageid : NSString = NSString()
                var tasker_status : NSString = NSString()
                var dateStr : NSString = NSString()
                var gettaskerid : NSString = NSString()
                
                
                
                let status : NSString = themes.CheckNullValue(userInfoDict!.objectForKey("status"))!
                if status == "1"
                {
                    let ChatMessage:NSArray = userInfoDict!.objectForKey("messages") as! NSArray
                    taskid = userInfoDict!.objectForKey("task") as! NSString
                    gettaskerid = themes.CheckNullValue(userInfoDict!.objectForKey("tasker"))!
                    tasker_status = themes.CheckNullValue(ChatMessage[0].objectForKey("tasker_status"))!
                    
                    
                    
                    Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
                    messageid = ChatMessage[0].objectForKey("_id") as! NSString
                    dateStr = ChatMessage[0].objectForKey("date") as! NSString
                    
                    let userid : NSString = themes.CheckNullValue(ChatMessage[0].objectForKey("from"))!
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChat", object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)" ,"taskerstus":tasker_status,"date" : dateStr,"tasker_id":gettaskerid])
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChatToRootView", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                    
                }
            }
        else{
                self.APNSNotification(userInfo as NSDictionary)
                
            }
        }
        
        
    }
    
    
    func APNSNotification(dict : NSDictionary)
    {
        
        let userInfo:NSDictionary = dict
        
        var Message_Notice:NSString=NSString()
        var Action:NSString = NSString()
        
        
        
        
        Message_Notice = userInfo.objectForKey("message") as! NSString
        Action = userInfo.objectForKey("action") as! NSString
        
        
        
        
        
        if(Action == "requesting_payment")
        {
            
            let Order_id:NSString=userInfo.objectForKey("key0") as! NSString
            
            NSNotificationCenter.defaultCenter().postNotificationName("ShowPushPayment", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
            
        }
            
        else if(Action == "admin_notification")
        {
            
        }
            
        else if(Action == "payment_paid")
        {
            let Order_id:NSString=userInfo.objectForKey("key0") as! NSString
            NSNotificationCenter.defaultCenter().postNotificationName("ShowPushRating", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
            
        }
        else if (Action == "job_request")
        {
            
        }
            
        else
        {
            let Order_id:NSString=userInfo.objectForKey("key0") as! NSString
            NSNotificationCenter.defaultCenter().postNotificationName("ShowPushNotification", object: nil, userInfo: ["Order_id":"\(Order_id)"])
        }
        
        
    }
    
    
    
    
    
    func socketNotification(dict:NSDictionary)
    {
        var messageArray:NSMutableDictionary=NSMutableDictionary()
        var Message_Notice:NSString=NSString()
        var Action:NSString = NSString()
        let Message:NSDictionary?=dict["message"] as? NSDictionary
        print("the user info is \(dict)...\(Message)")
        
        
        if(Message != nil)
        {
            messageArray=(Message?.objectForKey("message") as? NSMutableDictionary)!
            Message_Notice=(messageArray.objectForKey("message") as? NSString)!
            Action=(messageArray.objectForKey("action") as? NSString)!
            
            
            
            
            
            if(Action == "requesting_payment")
            {
                if (Message?.objectForKey("message")) is NSString {
                    let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ShowPayment", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
                    
                }
                else
                {
                    let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ShowPayment", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
                }
                
            }
                
            else if(Action == "payment_paid")
            {
                let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                NSNotificationCenter.defaultCenter().postNotificationName("ShowRating", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
            }
            else if (Action == "job_request")
            {
                
            }
            else if (Action == "rejecting_task")
            {
                let Order_id:NSString=messageArray.objectForKey("key1") as! NSString
                NSNotificationCenter.defaultCenter().postNotificationName("ShowNotification", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])
            }
            else if (Action == "job_cancelled")
            {
                
            }
                
                
            else
            {
                
                let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                NSNotificationCenter.defaultCenter().postNotificationName("ShowNotification", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)","Action":"\(Action)"])
                
            }
            
            
            
        }
        
    }
    
    
    
    
    func socketSupportNotification(dict:NSDictionary)
    {
        
        let Message:NSDictionary=(dict as? NSDictionary!)!
        
        
       // Message_details.admin_id = (themes.CheckNullValue(Message.objectForKey("admin")!))!
       // Message_details.support_chatid = (themes.CheckNullValue(Message.objectForKey("reference")!))!
        
        let SupportMessage: NSArray = Message.objectForKey("messages") as! NSArray
        let getsupportmsg = themes.CheckNullValue(SupportMessage[0].objectForKey("message")!)
        
        let getdate = themes.CheckNullValue(SupportMessage[0].objectForKey("date")!)
        let from_id = themes.CheckNullValue(SupportMessage[0].objectForKey("from")!)
        let admin_id = themes.CheckNullValue(Message.objectForKey("admin"))!
        let reference_id = themes.CheckNullValue(Message.objectForKey("reference"))!
        
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportChat", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id!)","date":"\(getdate!)","reference":"\(reference_id)"])
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id!)","date":"\(getdate!)","admin":"\(admin_id)","reference":"\(reference_id)"])
        
        
    }

    
    
    
    func socketChatNotification(dict:NSDictionary)
    {
        
        
        
        let Message:NSDictionary=(dict["message"] as? NSDictionary)!
        
        
        var Message_Notice:NSString=NSString()
        var taskid:NSString=NSString()
        var messageid : NSString = NSString()
        var tasker_status : NSString = NSString()
        var dateStr : NSString = NSString()
        var gettaskerid : NSString = NSString()
        
        
        let status : NSString = themes.CheckNullValue(Message.objectForKey("status"))!
        if status == "1"
        {
            let ChatMessage:NSArray = Message.objectForKey("messages") as! NSArray
            taskid = Message.objectForKey("task") as! NSString
            gettaskerid = themes.CheckNullValue(Message.objectForKey("tasker"))!
            tasker_status = themes.CheckNullValue(ChatMessage[0].objectForKey("tasker_status"))!
            
            
            
            Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
            messageid = ChatMessage[0].objectForKey("_id") as! NSString
            dateStr = ChatMessage[0].objectForKey("date") as! NSString
            let userid : NSString = themes.CheckNullValue(ChatMessage[0].objectForKey("from"))!
            
            NSNotificationCenter.defaultCenter().postNotificationName("ReceiveChat", object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)" ,"taskerstus":tasker_status,"date" : dateStr ,"tasker_id":gettaskerid])
            NSNotificationCenter.defaultCenter().postNotificationName("ReceiveChatToRootView", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
        }
        
        
        
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        if(themes.Check_userID() != "")
        {
            
            //SocketIOManager.sharedInstance.RemoveAllListener()
            ConnectionTimer.invalidate()
            ConnectionTimer = NSTimer()
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(themes.getUserID())
            SocketIOManager.sharedInstance.LeaveRoom(themes.getUserID())
            SocketIOManager.sharedInstance.RemoveAllListener()
        }
        
        
        
        self.CheckDisconnect()
        
        
    }
    
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        ConnectionTimer.invalidate()
        ConnectionTimer = NSTimer()
        
        if(themes.Check_userID() != "")
        {
            
            //SocketIOManager.sharedInstance.RemoveAllListener()
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(themes.getUserID())
            SocketIOManager.sharedInstance.LeaveRoom(themes.getUserID())
            SocketIOManager.sharedInstance.RemoveAllListener()
        }
        
        
    }
    
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    
    
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //   CheckConnect()
        
        
        
        
        ConnectionTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(ReconnectMethod), userInfo: nil, repeats: true)
        
        
        if(themes.Check_userID() != "")
        {
            
            SocketIOManager.sharedInstance.establishConnection()
            
            SocketIOManager.sharedInstance.establishChatConnection()
            
            
            
            
            
        }
        
        
        
        FBSDKAppEvents.activateApp()
        
    }
    
    func applicationWillTerminate(application: UIApplication) {
        if(themes.Check_userID() != "")
        {
            
            //SocketIOManager.sharedInstance.RemoveAllListener()
            ConnectionTimer.invalidate()
            ConnectionTimer = NSTimer()
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(themes.getUserID())
            SocketIOManager.sharedInstance.LeaveRoom(themes.getUserID())
            SocketIOManager.sharedInstance.RemoveAllListener()
        }
        
        
        
        self.CheckDisconnect()
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.Casperon.Plumbal" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Plumbal", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitima/Users/casperontechnologies1/Desktop/Launch screen/lanuchimage 320x480.pngte error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //MARK: Private Methods
    private func setupStream() {
        //		xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        xmppRoster.activate(xmppStream)
        xmppStream.addDelegate(self, delegateQueue: dispatch_get_main_queue())
        xmppRoster.addDelegate(self, delegateQueue: dispatch_get_main_queue())
        xmppStream.hostName=constant.Hostname as String
        xmppStream.hostPort=5222
    }
    
    private func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream.myJID.domain
        
        print("the domain is is \(domain).....")
        
        let priority = DDXMLElement.elementWithName("priority", stringValue: "24") as! DDXMLElement
        presence.addChild(priority)
        
        // 			let priority = DDXMLElement.elementWithName("192.168.1.148", stringValue: "5222") as! DDXMLElement
        //			presence.addChild(priority)
        xmppStream.sendElement(presence)
    }
    
    private func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream.sendElement(presence)
    }
    
    func connect() -> Bool {
        
        
        //            themes.saveJaberID("5630701bcae2aaa80700002c@casp83")
        //            themes.saveJaberPassword("4b01f338bf8d22a55299b369dc3a8287")
        
        
        if(themes.getJaberPassword() != nil)
        {
            
            let jabberID:String? = "\(themes.getJaberID())\(constant.DomainName)"
            let myPassword:String?  = "\(themes.getJaberPassword()!)"
            print("the username is \(jabberID).....\(jabberID)")
            if !xmppStream.isDisconnected() {
                return true
            }
            if jabberID == nil && myPassword == nil {
                return false
            }
            xmppStream.myJID = XMPPJID.jidWithString(jabberID!)
            do {
                try xmppStream.connectWithTimeout(XMPPStreamTimeoutNone)
                print("Connection success")
                return true
            } catch {
                print("Something went wrong!")
                return false
            }
        } else {
            return true
        }
    }
    
    func disconnect() {
        goOffline()
        xmppStream.disconnect()
    }
    
    //MARK: XMPP Delegates
    func xmppStreamDidConnect(sender: XMPPStream!) {
        if(themes.getJaberPassword() != nil)
        {
            
            do {
                
                try	xmppStream.authenticateWithPassword("\(themes.getJaberPassword()!)")
            } catch {
                print("Could not authenticate")
            }
        }
    }
    
    func xmppStreamDidAuthenticate(sender: XMPPStream!) {
        goOnline()
    }
    
    func xmppStream(sender: XMPPStream!, didReceiveIQ iq: XMPPIQ!) -> Bool {
        print("Did receive IQ")
        return false
    }
    
    func xmppStream(sender: XMPPStream!, didReceiveMessage message: XMPPMessage!) {
        
        
        
        
        
        //        var parseError: NSErrorPointer? = nil
        //        var xmlDictionary: [NSObject : AnyObject] = XMLReader.dictionaryForXMLString(testXMLString, error: &parseError)
        
        //        var ParseError:NSErrorPointer?=nil
        //        var xmlDictionary:NSDictionary=
        print("Did receive message \(message)")
        
        
        var MessageString="\(message)"
        var errorStr:NSString=""
        var Job_id:NSString?=""
        var user_id:NSString=NSString()
        var userimage:NSString=NSString()
        var username:NSString=NSString()
        var time:NSDate=NSDate()
        
        // Parse the XML into a dictionary
        do
        {
            
            let xmlDictionary:NSDictionary = try XMLReader.dictionaryForXMLString(MessageString)
            print("The parsed message is  \(xmlDictionary)")
            errorStr=(xmlDictionary.objectForKey("message")?.objectForKey("type"))! as! NSString
            Job_id=xmlDictionary.objectForKey("message")?.objectForKey("jobid") as? NSString
            
            if(errorStr != "error")
            {
                
                if(Job_id == nil)
                {
                    
                    MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                    let xmppRecMsg:NSString = MessageString.stringByReplacingOccurrencesOfString("+", withString: " ").stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                    
                    
                    let data:NSData = xmppRecMsg.dataUsingEncoding(NSUTF8StringEncoding)!
                    var Dictionay:NSDictionary?=NSDictionary()
                    do {
                        
                        Dictionay = try NSJSONSerialization.JSONObjectWithData(
                            
                            data,
                            
                            options: NSJSONReadingOptions.MutableContainers
                            
                            ) as? NSDictionary
                        
                        
                        
                        
                        
                        if(Dictionay != nil && Dictionay?.count>0)
                        {
                            let Message_Notice:NSString?=Dictionay?.objectForKey("message") as? NSString
                            
                            NSLog("%@",Dictionay!);
                            
                            let Action:NSString?=Dictionay?.objectForKey("action") as? NSString
                            if(Action! == "requesting_payment")
                            {
                                let job_id:NSString=Dictionay?.objectForKey("key1") as! NSString
                                NSNotificationCenter.defaultCenter().postNotificationName("ShowPayment", object: nil, userInfo: ["job_id":"\(job_id)"])
                            }
                                
                            else if(Action! == "payment_paid")
                            {
                                let job_id:NSString=Dictionay?.objectForKey("key1") as! NSString
                                NSNotificationCenter.defaultCenter().postNotificationName("ShowRating", object: nil, userInfo: ["job_id":"\(job_id)"])
                            }
                            else
                            {
                                
                                let Order_id:NSString=Dictionay?.objectForKey("key1") as! NSString
                                NSNotificationCenter.defaultCenter().postNotificationName("ShowNotification", object: nil, userInfo: ["Message":"\(Message_Notice!)","Order_id":"\(Order_id)"])
                                
                            }
                            
                            
                        }
                        
                        
                        
                    }
                        
                        
                        
                        
                    catch let error as NSError {
                        
                        // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
                        print("A JSON parsing error occurred, here are the details:\n \(error)")
                        Dictionay=nil
                        
                    }
                    
                    
                    
                }
                else
                {
                    
                    
                    
                    
                    MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                    
                    if(errorStr=="cancel"){
                        print("The parsed message is  \(xmlDictionary).....\(MessageString)")
                    }else if(MessageString != "Type" && MessageString != "StopType" && errorStr == "chat") {
                        //                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                        
                        Job_id=(xmlDictionary.objectForKey("message")?.objectForKey("jobid"))! as? NSString
                        
                        if(Common_Chatid == Job_id!)
                        {
                            MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                            NSNotificationCenter.defaultCenter().postNotificationName("ChatFromOthersNotification", object: MessageString)
                        }
                        else
                        {
                            
                            
                            //                    Cha_Detail_Table
                            //                    chatid
                            //                    userid
                            //                    userimage
                            //                    username
                            
                            //                    Chat_Table
                            //                    jobid
                            //                    message
                            //                    time
                            //                    userid
                            
                            //                    message_sent.addAttributeWithName("image", stringValue: "\(themes.getuserDP())")
                            //                    message_sent.addAttributeWithName("username", stringValue: "\(themes.getUserName())")
                            //                    message_sent.addAttributeWithName("userid", stringValue: "\(themes.getUserID())")
                            //                    message_sent.addAttributeWithName("jobid", stringValue: "\(Message_details.job_id)")
                            //                    message_sent.addAttributeWithName("to", stringValue: "\(Message_details.id)@casp83")
                            
                            MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                            user_id=xmlDictionary.objectForKey("message")?.objectForKey("userid") as! NSString as String
                            userimage=xmlDictionary.objectForKey("message")?.objectForKey("image") as! NSString as String
                            username=xmlDictionary.objectForKey("message")?.objectForKey("username") as! NSString as String
                            //for Notification
                            //                    NSNotificationCenter.defaultCenter().postNotificationName("Message_notify", object: nil, userInfo: ["Message":"\(MessageString)","username": "\(username)","Order_id":"\(Job_id)"])
                            
                            //                    Root_base.ConfigureNotification(Job_id, username: username, Usermessage: MessageString)
                            //                     time=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("username") as! NSString as String
                            
                            
                            //saving message to Chat_Details
                            //                    let managedContext = managedObjectContext
                            
                            //                    let Chat_Detail_Entity = NSEntityDescription.entityForName("Cha_Detail_Table", inManagedObjectContext: managedContext)
                            //
                            //                    let Chat_Detail = NSManagedObject(entity: Chat_Detail_Entity!,
                            //                        insertIntoManagedObjectContext: managedContext)
                            //                    Chat_Detail.setValue(user_id, forKey: "userid")
                            //                    Chat_Detail.setValue("", forKey: "chatid")
                            //                    Chat_Detail.setValue(userimage, forKey: "userimage")
                            //                    Chat_Detail.setValue(username, forKey: "username")
                            //                       //Saving message to chat table
                            //                      let Chat_entity =  NSEntityDescription.entityForName("Chat_Table",
                            //                        inManagedObjectContext:managedContext)
                            //                      let Chat_Tab = NSManagedObject(entity: Chat_entity!,
                            //                        insertIntoManagedObjectContext: managedContext)
                            //                      Chat_Tab.setValue(MessageString, forKey: "message")
                            //                    Chat_Tab.setValue(time, forKey: "time")
                            //                    Chat_Tab.setValue(user_id, forKey: "userid")
                            //                    Chat_Tab.setValue(Job_id, forKey: "jobid")
                            
                            //
                            //                     do {
                            //
                            //                        try managedContext.save()
                            //
                            //                        print("Successfully Saved")
                            //
                            //                        //5
                            //                    } catch let error as NSError  {
                            //                        print("Chat_Table not save \(error), \(error.userInfo)")
                            //                    }
                            
                        }
                    }
                    else if(errorStr == "Typing")
                    {
                        
                        Job_id=(xmlDictionary.objectForKey("message")?.objectForKey("jobid"))! as? NSString
                        
                        if(Common_Chatid == Job_id!)
                        {
                            
                            
                            MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                            //   var Job_Id=xmlDictionary.objectForKey("message")?.objectForKey("jobid")! as! NSString
                            NSNotificationCenter.defaultCenter().postNotificationName(kTypeStatus, object: MessageString)
                        }
                    }
                }
            }else{
                let Alert:UIAlertView=UIAlertView()
                Alert.title="Receiver is not get authenticated"
                Alert.addButtonWithTitle(themes.setLang("ok"))
                Alert.show()
            }
            
        }
        catch {
            let Alert:UIAlertView=UIAlertView()
            Alert.title="\(MessageString)"
            Alert.addButtonWithTitle(themes.setLang("ok"))
            Alert.show()
        }
        
    }
    
    
    func xmppStream(sender: XMPPStream!, didSendMessage message: XMPPMessage!) {
        print("Did send message \(message)")
    }
    
    func xmppStream(sender: XMPPStream!, didReceivePresence presence: XMPPPresence!) {
        let presenceType = presence.type()
        let myUsername = sender.myJID.user
        let presenceFromUser = presence.from().user
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(presenceFromUser)")
            if presenceType == "available" {
                
            } else if presenceType == "unavailable" {
            }
        }
    }
    
    func xmppRoster(sender: XMPPRoster!, didReceiveRosterItem item: DDXMLElement!) {
        print("Did receive Roster item")
    }
    
    func CheckDisconnect()
    {
        if(themes.Check_userID() != "")
        {
            let URL_Handler:URLhandler=URLhandler()
            let param=["user_type":"user","id":"\(themes.getUserID())","mode":"unavailable"]
            URL_Handler.makeCall(constant.Get_Onlinestatus, param: param, completionHandler: { (responseObject, error) -> () in
                
            })
            let messageControl:MessageViewController=MessageViewController()
            messageControl.checkTyping()
            
            
            disconnect()
        }
        
    }
    
    func CheckConnect()
    {
        if(themes.Check_userID() != "")
        {
            let URL_Handler:URLhandler=URLhandler()
            let param=["user_type":"user","id":"\(themes.getUserID())","mode":"available"]
            URL_Handler.makeCall(constant.Get_Onlinestatus, param: param, completionHandler: { (responseObject, error) -> () in
                
            })
            
            connect()
        }
        
        
    }
    func Make_RootVc(ViewIdStr:NSString,RootStr:NSString){
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let rootView: UIViewController = sb.instantiateViewControllerWithIdentifier(ViewIdStr as String) as! UIViewController
        UIView.transitionWithView(window!, duration: 0.2, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            appDel.window?.rootViewController=rootView
            }, completion: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("MakerootView", object: RootStr)
        
        
    }
    
    
    
    
}

