//
//  SearchBarTableViewCell.swift
//  Plumbal
//
//  Created by Casperon iOS on 15/12/2016.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

import UIKit

class SearchBarTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Function
    
    func configureCellWith(searchTerm searchTerm:String, cellText:String){
        var pattern = searchTerm.stringByReplacingOccurrencesOfString(" ", withString: "|")
        pattern.insert("(", atIndex: pattern.startIndex)
        pattern.insert(")", atIndex: pattern.endIndex)
        
        do {
            let regEx = try NSRegularExpression(pattern: pattern, options: [.CaseInsensitive,
                .AllowCommentsAndWhitespace])
            let range = NSRange(location: 0, length: cellText.characters.count)
            let displayString = NSMutableAttributedString(string: cellText)
            let highlightColour = PlumberThemeColor

            regEx.enumerateMatchesInString(cellText, options: .WithTransparentBounds, range: range, usingBlock: { (result, flags, stop) in
                if result?.range != nil {
                 // displayString.setAttributes([NSBackgroundColorAttributeName:highlightColour], range: result!.range)
                }
                
            })
            
            self.textLabel?.attributedText = displayString
            
        } catch
        {
            self.textLabel?.text = cellText
        }
    }
    
    

    
}


