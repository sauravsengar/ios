//
//  UserInfoRecord.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/19/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class UserInfoRecord: NSObject {
    var providerName:NSString=""
    var providerId:NSString=""
    var providerImage:NSString=""
    var providerEmail:NSString=""
    var providerContactNo:NSString=""
    
    init(prov_name: NSString, prov_ID: NSString, prov_Image: NSString, prov_Email: NSString, prov_mob: NSString) {
        providerName = prov_name
        providerId = prov_ID
        providerImage = prov_Image
        providerEmail = prov_Email
        providerContactNo = prov_mob
        super.init()
    }
}
