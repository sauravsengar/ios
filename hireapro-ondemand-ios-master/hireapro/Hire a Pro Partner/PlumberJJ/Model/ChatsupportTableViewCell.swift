//
//  ChatsupportTableViewCell.swift
//  Plumbal
//
//  Created by Casperon on 01/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class ChatsupportTableViewCell: UITableViewCell {

    @IBOutlet var date_labl: UILabel!
    @IBOutlet var lastmsg_labl: UILabel!
    @IBOutlet var topic_labl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
