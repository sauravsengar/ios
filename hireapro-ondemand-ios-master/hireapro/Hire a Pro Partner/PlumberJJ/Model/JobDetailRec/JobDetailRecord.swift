//
//  JobDetailRecord.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class JobDetailRecord: NSObject {
    var jobIdentity:NSString=""
    var Currency:NSString=""
    var jobDate:NSString=""
    var jobTime:NSString=""
    var jobTitle:NSString=""
    var jobDesc:String=""
    var jobStatus:NSString=""
    var jobUserName:NSString=""
    var jobEmail:NSString=""
    var jobPhone:NSString=""
    var jobLocation:NSString=""
    var jobLat:NSString=""
    var jobLong:NSString=""
    var jobBtnStatus:NSString=""
    var Userid: NSString=""
    var RequireJobId: NSString = ""
    var userimage:NSString = ""
    var hourrate:NSString = ""
    var cat_type:NSString = ""
    var serv_tax:NSString = ""
    
    var cashoption:NSString = ""
    var currency_symbol: NSString = ""
    
    
}
class TrackingDetails : NSObject{
    var userLat = Double()
    var userLong = Double()
    var partnerLat = Double()
    var partnerLong = Double()
    var taskId = String()
    var selectedPage : Int = 0;
}