//
//  ScheduleView.swift
//  Plumbal
//
//  Created by Casperon Tech on 04/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class ScheduleView: NSObject {
    
    
    var ScheduleAddressArray:NSMutableArray=NSMutableArray()
    var tasker_lat: NSString = NSString()
    var tasker_lng : NSString = NSString()
    var service:NSString=NSString()
    var orderDate:NSString=NSString()
    var JobID:NSString=NSString()
    var jobDescription:NSString=NSString()
    var service_tax : NSString=NSString()
    var TaskID:NSString=NSString()
    var RquiredAddressid : NSString = NSString()
    var PickupDate : NSString = NSString()
    var pickupTime : NSString = NSString()
    var GetScheduleIstr: NSString = NSString()
    var GetCoupenText : NSString = NSString()
    var getLatitude : NSString = NSString()
    var getLongtitude: NSString = NSString()
    
    
    var Schedule_header:NSString=NSString()
    
    var ProviderListIdArray:NSMutableArray=NSMutableArray()
    var ProviderListNameArray:NSMutableArray=NSMutableArray()
    var ProviderLisreviewsArray:NSMutableArray=NSMutableArray()
    var ProviderdistanceArray:NSMutableArray=NSMutableArray()

    
    var ProviderListAvailableArray:NSMutableArray=NSMutableArray()
    var ProviderListImageArray:NSMutableArray=NSMutableArray()
    var ProviderListCompanyArray:NSMutableArray=NSMutableArray()
    var ProviderListRatingArray:NSMutableArray=NSMutableArray()
    var ProviderListMinamountArray:NSMutableArray=NSMutableArray()
    var ProviderListHouramountArray:NSMutableArray=NSMutableArray()
    var ProviderListFlatamountArray:NSMutableArray=NSMutableArray()
    var ProviderListBadgeArray : NSMutableArray = NSMutableArray()
    var ProviderListCategoryTypeArray:NSMutableArray=NSMutableArray()

     var ScheduleAddressNameArray:NSMutableArray=NSMutableArray()
    var ScheduleLatitudeArray:NSMutableArray = NSMutableArray()
    var ScheduleLongtitudeArray:NSMutableArray = NSMutableArray()
  
    var scheduleAddressid:NSString = NSString()

}
