//
//  MyOrderOpenRecord.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/29/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit


class MyOrderOpenRecord: NSObject {
    var orderId:NSString=""
    var postedOn:NSString=""
    var UserName:NSString=""
    var userCatg:NSString=""
    var userLoc:NSString=""
    var userImg:NSString=""
    var orderstatus:NSString=""
    var ratehour:NSString=""
    var type:NSString=""

    
    init(order_id: String, post_on: String, user_Img: String, user_name: String, user_catg: String , user_Loc: String, order_sta: String, rate_hour: String, cat_type:String) {
        orderId = order_id
        postedOn = post_on
        userImg = user_Img
        UserName = user_name
        userCatg = user_catg
        userLoc = user_Loc
        orderstatus = order_sta
        ratehour =  rate_hour
        type  = cat_type
    
        super.init()
    }
}
