//
//  BadgeCollectionViewCell.swift
//  PlumberJJ
//
//  Created by Casperon on 03/08/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class BadgeCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imgBadge: UIImageView!
    @IBOutlet var nameBadge: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
