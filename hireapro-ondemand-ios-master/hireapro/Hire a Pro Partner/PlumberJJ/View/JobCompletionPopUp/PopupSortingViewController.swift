//
//  PopupSortingViewController.swift
//  PlumberJJ
//
//  Created by CASPERON on 10/08/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

import UIKit
protocol PopupSortingViewControllerDelegate {
   
    func pressedCancel(sender: PopupSortingViewController)
    
  
    
    func  passRequiredParametres(fromdate:NSString,todate: NSString,isAscendorDescend: Int,isToday: Int,isSortby: NSString)
    
    
}


class PopupSortingViewController: UIViewController {
    
    
    @IBOutlet var applyview: SetColorView!
    @IBOutlet var datecheckmark: UIImageView!
    @IBOutlet var namecheckmark: UIImageView!
    @IBOutlet var datebtn: UIButton!
    @IBOutlet var Namebtn: UIButton!
    var delegate:PopupSortingViewControllerDelegate?
    var Globalindex:NSString=NSString()
     var dates: NSMutableArray!
    var convertDatesArr : NSMutableArray!
    var statusoforder : Int = 0
    var statusofsorting : NSString = NSString()
    var fromBtnisClicked : Bool!
    var toBtnisClicked :Bool!
    var fromDateval : NSString = NSString()
    var todateval : NSString = NSString()
    var themes:Theme=theme
    var todayInt : Int = 3

    @IBOutlet var checkmark3: UIImageView!
    @IBOutlet var checkmark4: UIImageView!
    @IBOutlet var checkmark5: UIImageView!

    @IBOutlet var checkmark2: UIImageView!
    @IBOutlet var checkmark1: UIImageView!
    @IBOutlet var orderbylabl: UILabel!
    @IBOutlet var ascendingbtn: UIButton!
    @IBOutlet var cancel: UIButton!

    @IBOutlet var applybtn: UIButton!
    @IBOutlet var Desendingbtn: UIButton!
    
    @IBOutlet var btnTodays: UIButton!
    @IBOutlet var btnRecent: UIButton!
    @IBOutlet var btnUpcoming: UIButton!

    @IBOutlet var todate: UIButton!
    @IBOutlet var fromdate: UIButton!
    
    @IBOutlet weak var lblSorting: UILabel!
 @IBOutlet weak var Orderby: UILabel!
    @IBOutlet var datepicker: UIPickerView!
    @IBAction func didclickoption(sender: AnyObject) {
        
        if sender.tag == 0
        {
           self.Callcanceldelegate()
        }
     else   if sender.tag == 1
        {
            datepicker.reloadAllComponents()
            fromBtnisClicked = true
            toBtnisClicked = false
            
            datepicker.hidden = false
            orderbylabl.hidden = true;
            applyview.hidden = true;

        }
         else    if sender.tag == 10
            {
                statusofsorting = "name"
                namecheckmark.hidden = false
                datecheckmark.hidden = true

        }
      else   if   sender.tag == 11
    {
        statusofsorting = "date"
        namecheckmark.hidden = true
        datecheckmark.hidden = false


    }
       else     if sender.tag == 2
      {
     datepicker.reloadAllComponents()
        toBtnisClicked = true
        fromBtnisClicked = false
        datepicker.hidden = false
        orderbylabl.hidden = true;
        applyview.hidden = true;
        }
        
         else    if sender.tag == 3
         {
            
            statusoforder = 1
            checkmark1.hidden = false
            checkmark2.hidden = true
        }
         else   if sender.tag == 4
        {
            
            statusoforder = -1
            checkmark1.hidden = true
            checkmark2.hidden = false
        }else if(sender.tag == 5){
            fromdate.enabled = false
            todate.enabled = false
            fromdate.alpha = 0.5
            todate.alpha = 0.5
            
            checkmark3.hidden = false
            checkmark4.hidden = true
            checkmark5.hidden = true
            todayInt = 0
        }else if(sender.tag == 6){
            fromdate.enabled = false
            todate.enabled = false
            fromdate.alpha = 0.5
            todate.alpha = 0.5
            
            checkmark3.hidden = true
            checkmark4.hidden = false
            checkmark5.hidden = true
            todayInt = 1
        }else if(sender.tag == 7){
            fromdate.enabled = false
            todate.enabled = false
            fromdate.alpha = 0.5
            todate.alpha = 0.5
            
            checkmark3.hidden = true
            checkmark4.hidden = true
            checkmark5.hidden = false
            todayInt = 2
        }




    }
    
    @IBAction func didapplybtnclick(sender: AnyObject) {
//        
//        if fromDateval == ""
//        {
//            
//          //  self.themes.AlertView("", Message: "Please select  date", ButtonTitle: "Ok")
//          self.view.makeToast(message: "Please select  date", duration: 3, position: HRToastPositionDefault, title: "")
//
//            
//        }
//        else if todateval == ""
//        {
//          self.view.makeToast(message: "Please select  date", duration: 3, position: HRToastPositionDefault, title: "")
//
//        }
       
//        else
//        {
            self.delegate?.passRequiredParametres(fromDateval, todate:todateval, isAscendorDescend: statusoforder,isToday:todayInt,isSortby :"date")
            self.delegate?.pressedCancel(self)
       //}

    }
    
     func applicationLanguageChangeNotification(notification: NSNotification) {
          }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        fromdate.enabled = true
        todate.enabled = true
        
        if (trackingDetail.selectedPage == 1) || (trackingDetail.selectedPage == 2) {
            btnTodays.enabled = false
            btnTodays.alpha = 0.5
            btnUpcoming.alpha = 0.5
            btnRecent.alpha = 0.5
            
            btnUpcoming.enabled = false
            btnRecent.enabled = false
            
        }else if (trackingDetail.selectedPage == 0){
            btnTodays.alpha = 1
            btnUpcoming.alpha = 1
            btnRecent.alpha = 1
            
            btnTodays.enabled = true
            btnUpcoming.enabled = true
            btnRecent.enabled = true
            
        }

        
        lblSorting.text = Language_handler.VJLocalizedString("sorting", comment: nil)
        todate.setTitle(Language_handler.VJLocalizedString("to_date", comment: nil), forState: UIControlState.Normal)
        fromdate.setTitle(Language_handler.VJLocalizedString("from_date", comment: nil), forState: UIControlState.Normal)
        orderbylabl.text = Language_handler.VJLocalizedString("order_by", comment: nil)
        ascendingbtn.setTitle(Language_handler.VJLocalizedString("ascending", comment: nil), forState: UIControlState.Normal)
        Desendingbtn.setTitle(Language_handler.VJLocalizedString("descending", comment: nil), forState: UIControlState.Normal)
        applybtn.setTitle(Language_handler.VJLocalizedString("apply", comment: nil), forState: UIControlState.Normal)
       
        btnTodays.setTitle(Language_handler.VJLocalizedString("btn_Todays", comment: nil), forState: UIControlState.Normal)
        btnUpcoming.setTitle(Language_handler.VJLocalizedString("btn_Upcoming", comment: nil), forState: UIControlState.Normal)
        btnRecent.setTitle(Language_handler.VJLocalizedString("btn_Recent", comment: nil), forState: UIControlState.Normal)
        Orderby.text = Language_handler.VJLocalizedString("Order_by", comment: nil)


        let startDate: NSDate = NSDate(timeIntervalSinceNow: -60 * 60 * 24 * 20)
        let endDate: NSDate = NSDate(timeIntervalSinceNow:60 * 60 * 24 * 19)
       dates = [startDate]
        let gregorianCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        let components: NSDateComponents = gregorianCalendar.components(.NSDayCalendarUnit, fromDate: startDate, toDate: endDate, options:[])
      //  gregorianCalendar.components(NSDayCalendarUnit, fromDate: startDate, toDate: endDate, options: []
        for i in 1..<components.day {
            let newComponents: NSDateComponents = NSDateComponents()
            newComponents.day = i
            let date: NSDate = gregorianCalendar.dateByAddingComponents(newComponents, toDate: startDate, options: [])!
            
            dates.addObject(date)
        }
        dates.addObject(endDate)
        convertDatesArr = NSMutableArray()
        
     for i in 0..<dates.count
     {
        convertDatesArr .addObject(self.stringDatePartOf(dates.objectAtIndex(i) as! NSDate))
        
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Callcanceldelegate()
    {
        self.delegate?.pressedCancel(self)
    }

    //    (NSString*) stringDatePartOf:(NSDate*)date
    //    {
    //    NSDateFormatter *formatter = [[NSDateFormatter new];
    //    [formatter setDateFormat:@"yyyy-MM-dd"];
    //
    //    return [formatter stringFromDate:date];
    //    }
    
    func stringDatePartOf (date :NSDate) -> NSString {
        let formatter: NSDateFormatter =
            NSDateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter.stringFromDate(date)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return convertDatesArr.count;
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView) -> UIView? {
        var tView: UILabel = (view as! UILabel)
      
            tView = UILabel()
        tView .text = convertDatesArr[row] as? String
        tView.font = UIFont(name:"Roboto",size:14 )
        tView.textAlignment = .Center
        return tView
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        datepicker.hidden = true
        orderbylabl.hidden = false;
        ascendingbtn.hidden = false;
        Desendingbtn.hidden = false;
        applyview.hidden = false;

        if fromBtnisClicked == true
        {
            fromDateval = (convertDatesArr[row] as? String)!
        fromdate.setTitle(convertDatesArr[row] as? String, forState:.Normal)
        }
        if toBtnisClicked == true
        {
            todateval = (convertDatesArr[row] as? String)!

            todate.setTitle(convertDatesArr[row] as? String, forState:.Normal)

        }
        
        
    }


}
