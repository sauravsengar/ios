

import UIKit
protocol DatePickerDelegate {
    func dateSelectedInDatePicker(datePicker:SUSDatePickerView,selectedDate:NSDate, button : UIButton)
}
class SUSDatePickerView: UIView {
    var button : UIButton?
    var delegate : DatePickerDelegate?
    var datePicker : UIDatePicker = UIDatePicker()
    var dateLbl = UILabel()
    var datePickerMode : UIDatePickerMode? {
        didSet {
            self.datePicker.datePickerMode = datePickerMode!
        }
    }
    
    convenience init(frame:CGRect, delegate:DatePickerDelegate, mode:UIDatePickerMode, button:UIButton) {
        self.init(frame:frame)
        self.delegate = delegate
        self.button = button
        self.datePickerMode = mode
        addDatePicker(mode)
    }
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addDatePicker(mode:UIDatePickerMode) {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        let subView = UIView(frame:
            CGRectMake(
                self.center.x,
                self.center.y,
                self.frame.size.width-10,
                self.frame.size.height/2+50)
        )
        
        subView.center = self.center
        subView.backgroundColor = PlumberThemeColor
        subView.layer.borderWidth = 1
        subView.layer.cornerRadius = 10.0
        
        let sep1 = UIView(frame: CGRectMake(1, subView.frame.width/6, subView.frame.width, 2))
        sep1.backgroundColor = UIColor .whiteColor()
        
        let sep2 = UIView(frame: CGRectMake(1,(subView.frame.width)-(subView.frame.width/4.5), subView.frame.width, 2))
        sep2.backgroundColor = UIColor .whiteColor()
        
        let sep3 = UIView(frame: CGRectMake(sep2.frame.width/2,sep2.center.y, 2,subView.frame.height-sep2.center.y))
        
        sep3.backgroundColor = UIColor.whiteColor()
        
        subView.addSubview(sep1)
        subView.addSubview(sep2)
        subView.addSubview(sep3)
        
        dateLbl.frame = CGRectMake(1,1, subView.frame.width,sep1.center.y)
        dateLbl.text = "Date"
        dateLbl.textAlignment = NSTextAlignment.Center
        dateLbl.textColor = UIColor.whiteColor()
        subView.addSubview(dateLbl)
        
        datePicker = UIDatePicker()
        datePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        print(self.subviews)

        datePicker.minimumDate = NSDate()
        datePicker.datePickerMode = mode
        if mode == UIDatePickerMode.Date {
            datePicker.minimumDate = NSDate()
        }
        
        datePicker.addTarget(self, action: #selector(SUSDatePickerView.didChange(_:)), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.frame = CGRectMake(0, sep1.center.y,subView.frame.width,sep2.center.y-sep1.center.y )
         datePicker.frame.size = CGSizeMake(frame.width,sep2.center.y-sep1.center.y)

        subView.addSubview(datePicker)
        
        let backBTn = UIButton(frame: CGRectMake(1, sep2.center.y, sep3.center.x,sep3.frame.height))
        backBTn.setTitle("Back", forState: UIControlState.Normal)
        backBTn.addTarget(self, action:#selector(SUSDatePickerView.cancelButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        subView.addSubview(backBTn)
        
        let setBTn = UIButton(frame: CGRectMake(sep3.center.x,sep2.center.y,sep3.center.x, sep3.frame.height))
        setBTn.setTitle("Set", forState: UIControlState.Normal)
        setBTn.addTarget(self, action:#selector(SUSDatePickerView.doneButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        subView.addSubview(setBTn)
        
        self.addSubview(subView)
    }

    func didChange(sender:UIDatePicker){
        _ = sender.date;
        let dateformater = NSDateFormatter()
        if self.datePickerMode == UIDatePickerMode.Date {
            dateformater.dateFormat = "dd-MM-yyyy"
        } else if self.datePickerMode == UIDatePickerMode.Time {
            dateformater.dateFormat = "hh:mm a"
        }
        let fdate = dateformater.stringFromDate(datePicker.date)
        dateLbl.text = fdate;
    }
 
    func doneButtonClicked(sender:UIButton) {
        if let deleg = self.delegate, button = self.button {
            deleg.dateSelectedInDatePicker(self, selectedDate: datePicker.date, button : button)
        }
    }
    func cancelButtonClicked(sender:UIButton) {
        self.removeFromSuperview()
    }
    //    func addDatePicker(mode:UIDatePickerMode) {
    //        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
    //        datePicker.backgroundColor = UIColor.whiteColor()
    //        datePicker.layer.borderColor = UIColor.darkGrayColor().CGColor
    //        datePicker.layer.borderWidth = 2.0
    //        datePicker.center = self.center
    //        datePicker.frame.size = CGSizeMake(frame.width,300)
    //        datePicker.datePickerMode = mode
    //        if mode == UIDatePickerMode.Date {
    //            datePicker.minimumDate = NSDate()
    //        }
    //
    //        var done = UIButton(frame: CGRectMake(datePicker.frame.origin.x, datePicker.frame.origin.y + datePicker.bounds.height, bounds.width/2 - 1, 40))
    //        done.setTitle("Done", forState: UIControlState.Normal)
    //        done.addTarget(self, action: Selector("doneButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
    //        done.backgroundColor = UIColor.darkGrayColor()
    //        addSubview(done)
    //
    //        var cancel = UIButton(frame: CGRectMake(bounds.width/2 + 1, datePicker.frame.origin.y + datePicker.bounds.height, bounds.width/2, 40))
    //        cancel.setTitle("Cancel", forState: UIControlState.Normal)
    //        cancel.addTarget(self, action: Selector("cancelButtonClicked:"), forControlEvents: UIControlEvents.TouchUpInside)
    //        cancel.backgroundColor = UIColor.darkGrayColor()
    //        addSubview(cancel)
    //
    //        addSubview(datePicker)
    //
    //    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
}

