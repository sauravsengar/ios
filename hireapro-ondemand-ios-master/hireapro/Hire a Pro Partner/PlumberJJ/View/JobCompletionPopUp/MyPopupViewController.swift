//
//  MyPopupViewController.swift
//  SLPopupViewControllerDemo
//
//  Created by Nguyen Duc Hoang on 9/13/15.
//  Copyright © 2015 Nguyen Duc Hoang. All rights reserved.
//

import UIKit

protocol MyPopupViewControllerDelegate {
    //    func pressOK(sender: MyPopupViewController)
    func pressedCancel(sender: MyPopupViewController)
    func PassSelectedAddress(Address:NSString,Addressid:NSString)
    func pressAdd (sender :MyPopupViewController)
    
    
}

class MyPopupViewController: RootBaseViewController {
    
    var getIndex : Int!
    var delegate:MyPopupViewControllerDelegate?
    var Globalindex:NSString=NSString()
    var isDetailViewcontroller : Bool = Bool()

    @IBOutlet var listbtn: UIButton!
    @IBAction func didclickoption(sender: AnyObject) {
        self.delegate?.pressAdd(self)
        
    }
    
    @IBOutlet var Header_lab: UILabel!
    @IBOutlet var Address_tableView: UITableView!
    var themes:Theme=Theme()
    @IBOutlet var Close_WrapperView: UIView!
    
    var URL_handler:URLhandler=URLhandler()
   
    
    override func viewWillAppear(animated: Bool) {
        
        
        
        
        super.viewDidLoad()
        getIndex = 0
        if isDetailViewcontroller == false
        {
            listbtn.hidden = false
        }
        else
        {
            listbtn.hidden = true
            
        }
        NSLog(isDetailViewcontroller ? "Yes" : "No");
        
        self.view.layer.cornerRadius = 22
        view.layer.borderWidth=1.0
        view.layer.borderColor=themes.ThemeColour().CGColor
        self.view.layer.masksToBounds = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyPopupViewController.loadList(_:)),name:"load", object: nil)
        
        Close_WrapperView.layer.cornerRadius=Close_WrapperView.frame.size.width/2
        
        Close_WrapperView.layer.borderWidth=1.0
        Close_WrapperView.layer.borderColor=themes.ThemeColour().CGColor
        
        
        let Registernib=UINib(nibName: "AddressTableViewCell", bundle: nil)
        Address_tableView.registerNib(Registernib, forCellReuseIdentifier: "AddressCell")
        
        
        let Tap:UITapGestureRecognizer=UITapGestureRecognizer(target: self, action: #selector(MyPopupViewController.Callcanceldelegate))
        Close_WrapperView.addGestureRecognizer(Tap)
        Header_lab.text=Schedule_Data.Schedule_header as String
        
        Address_tableView.tableFooterView=UIView()
        
        
        
        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewDidLoad() {
        
    }
    
    
    func Callcanceldelegate()
    {
        self.delegate?.pressedCancel(self)
    }
    
    func loadList(notification: NSNotification){
        //load data here
        
        print("loading")
        if(Schedule_Data.ScheduleAddressArray.count != 0)
        {
            
            Address_tableView.reloadData()
            
        }
    }
    
    
    
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
            return   60.0
        
    }
    
    func calculateHeightForString(inString:String) -> CGFloat
    {
        let messageString = inString
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(16.0)])
        let rect:CGRect = attrString!.boundingRectWithSize(CGSizeMake(300.0,CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context:nil )//hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height  //to include button's in your tableview
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Schedule_Data.ScheduleAddressArray.count > 0
        {
            
            return Schedule_Data.ScheduleAddressArray.count
        }
        else {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print("2 step")
        
        
        
        let Cell:AddressTableViewCell = tableView.dequeueReusableCellWithIdentifier("AddressCell") as! AddressTableViewCell
        
        
        Cell.More_address_btn.hidden=true
        Cell.More_address_btn.userInteractionEnabled=false
        
        
        Cell.selectionStyle = .None
        //  Cell.Separator.hidden=true
        Cell.Address_Label.hidden=false
        Cell.backgroundColor=UIColor.whiteColor()
        
        if   Schedule_Data.ScheduleAddressArray.count > 0
        {
            Cell.Address_Label.text="\(Schedule_Data.ScheduleAddressArray[indexPath.row])"
        }
        
        if isDetailViewcontroller == false
        {
            
            
            
            
            Cell.Address_Label.frame = CGRect(x: Cell.Address_Label.frame.origin.x,y: Cell.More_address_btn.frame.origin.y,width: Cell.Address_Label.frame.size.width,height: Cell.More_address_btn.frame.size.height)
            
            
//            if   Schedule_Data.ScheduleAddressArray.count > 0 {
//                let height:CGFloat = self.themes.calculateHeightForString("\(Schedule_Data.ScheduleAddressArray[indexPath.row])")
//                Cell.Address_Label.frame.size.height=height+40
//            }
            
            Cell.More_icon.hidden=true
            
            
            
            if (Schedule_Data.ScheduleAddressNameArray.count>0) {
               // Cell.DeleteIcon.addTarget(self, action: #selector(MyPopupViewController.DeleteAddress(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                Cell.DeleteIcon.tag=indexPath.row
                
                
                if Schedule_Data.ScheduleAddressNameArray.objectAtIndex(indexPath.row) as! NSString == Schedule_Data.scheduleAddressid
                {
                    Cell.Checkmark_ImageView.hidden=false
                }
                else
                {
                    Cell.Checkmark_ImageView.hidden=true
                }
                
                
            }
            
        }
            
        else
        {
            
//            Cell.Address_Label.frame = CGRect(x: Cell.Address_Label.frame.origin.x,y:Cell.frame.origin.y-5,width: Cell.Address_Label.frame.size.width,height: Cell.Address_Label.frame.size.height)
//            
            
            
//            if   Schedule_Data.ScheduleAddressArray.count > 0
//            {
//                let height:CGFloat = self.themes.calculateHeightForString("\(Schedule_Data.ScheduleAddressArray[indexPath.row])")
//                
//                Cell.Address_Label.frame.size.height=height+40
//            }
            
            
            Cell.More_icon.hidden=true
            Cell.DeleteIcon.hidden=true
            Cell.Checkmark_ImageView.hidden = true
        }
        return Cell
        
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isDetailViewcontroller == false
        {
            
          
        }
        else{
            
            
            let getIndex : Int
            let convertStr : NSString = Schedule_Data.ScheduleAddressNameArray.objectAtIndex(indexPath.row) as! NSString
            
            
            getIndex = convertStr.integerValue
            
            self.delegate?.PassSelectedAddress(Schedule_Data.ScheduleAddressArray.objectAtIndex(indexPath.row) as! NSString, Addressid:Schedule_Data.ScheduleAddressNameArray.objectAtIndex(indexPath.row) as! NSString )
        }
        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
