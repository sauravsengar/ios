//
//  ReviewsTableViewCell.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/6/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {
 @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet var job_idlable: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func loadReviewTableCell(objOpenRec:ReviewRecords,currentView:UIViewController){
        
        
//        reviewLbl.sizeToFit()
        reviewLbl.numberOfLines = 0
         timeLbl.text=objOpenRec.reviewTime as String
        let test = "\(objOpenRec.reviewDesc)"
        let trimmed = test.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        // In Swift 1.2 (Xcode 6.3):
         userNameLbl.text=objOpenRec.reviewName as String
        reviewLbl.text=trimmed
        userImgView.sd_setImageWithURL(NSURL(string:objOpenRec.reviewImage as String), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        userImgView.layer.cornerRadius=userImgView.frame.size.width/2
        userImgView.layer.masksToBounds=true
        ratingView.rating = Float(objOpenRec.reviewRate as NSString as String)!
        job_idlable.text=objOpenRec.reviewJobID as String
        
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    }
