//
//  MaterialCell.swift
//  PlumberJJ
//
//  Created by Casperon on 15/03/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class MaterialCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        setPadding()
    }
    @IBOutlet var toolrent: UITextField!
    @IBOutlet var addfieldbtn: UIButton!
    @IBOutlet var toolname: UITextField!
    @IBOutlet var cancelbtn: UIButton!
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
  addfieldbtn.setTitle(Language_handler.VJLocalizedString("Add_Field", comment: nil), forState: .Normal)
        // Configure the view for the selected state
    }
    
    func setPadding(){
           }
    
}
