//
//  BankTableViewCell.swift
//  PlumberJJ
//
//  Created by Natarajan on 14/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {
    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var totalview: UIView!
    @IBOutlet var makeDefault: UIButton!
    @IBOutlet var bankNameLbl: UILabel!

    @IBOutlet var typeLbl: UILabel!
    @IBOutlet var numberLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
