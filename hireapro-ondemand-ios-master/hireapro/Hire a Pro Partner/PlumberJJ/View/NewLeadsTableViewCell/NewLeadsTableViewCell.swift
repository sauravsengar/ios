//
//  NewLeadsTableViewCell.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/19/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit


class NewLeadsTableViewCell: UITableViewCell {
    let deleteIndex:NSIndexPath=NSIndexPath()
    
    
 
    
    @IBOutlet weak var hourlyrateLbl: UILabel!
    @IBOutlet var statuslabl: UILabel!
    
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var catgLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var totalView: UIView!
    var theme : Theme = Theme()
    @IBOutlet weak var timelabel: UILabel!
    func loadMyOrderNewLeadTableCell(objOpenRec:MyOrderOpenRecord){
        
        /*deleteBtn.layer.cornerRadius=deleteBtn.frame.size.width/2;
        deleteBtn.layer.borderWidth=1
        deleteBtn.layer.borderColor=PlumberThemeColor.CGColor
        deleteBtn.layer.masksToBounds=true*/
        
        userImgView.layer.cornerRadius = userImgView.frame.size.width / 2;
        userImgView.backgroundColor = UIColor.lightGrayColor()
        userImgView.clipsToBounds=true
        userImgView.layer.masksToBounds = true;
        userImgView.layer.borderWidth = 0;
        userImgView.contentMode = UIViewContentMode.ScaleAspectFill
        userImgView.layer.borderWidth=2.0
        userImgView.layer.borderColor=UIColor.whiteColor().CGColor
        totalView.layer.shadowOffset = CGSize(width: 2, height: 2)
        totalView.layer.cornerRadius=14;
        totalView.layer.shadowOpacity = 0.2
        totalView.layer.shadowRadius = 2
        
        orderIdLbl.text="Job ID : \(objOpenRec.orderId)"
       
       let dateStr = "\(objOpenRec.postedOn)"

        
        
        var GetDateArr :NSArray = dateStr.componentsSeparatedByString(",")
        if GetDateArr.count > 0
        {
           
       dateLbl.text="\(self.theme.CheckNullValue(GetDateArr.objectAtIndex(0))!)"
            if GetDateArr.count == 2 {
                timelabel.text = "\(self.theme.CheckNullValue(GetDateArr.objectAtIndex(1))!)"
            }else{
                timelabel.text = ""
            }
        }
        
        NSLog("Get Date array=%@", objOpenRec.userLoc)
        
        nameLbl.text="\(objOpenRec.UserName)"
        catgLbl.text="\(objOpenRec.userCatg)"
        locationLbl.text="\(objOpenRec.userLoc)"
        statuslabl.text="\(objOpenRec.orderstatus)"
        hourlyrateLbl.text="\(objOpenRec.type) : \(theme.getappCurrencycode())\(objOpenRec.ratehour)"

       
      userImgView.sd_setImageWithURL(NSURL(string:objOpenRec.userImg as String), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        
        
        if(objOpenRec.orderstatus  == "Confirmed")
        {
           statuslabl.textColor = UIColor.purpleColor()
        
        }
            
        else if(objOpenRec.orderstatus == "Completed")
        {
           statuslabl.textColor=UIColor.greenColor()
            
            // 0.0, 1.0, and 0.0 and whose alpha value is 1.0.
        }
            
        else if(objOpenRec.orderstatus == "Cancelled")
        {
           statuslabl.textColor=UIColor.redColor()
            // 1.0, 0.0, and 0.0 and whose alpha value is 1.0.
            
        }
            
        else  if(objOpenRec.orderstatus == "Closed")
        {
            statuslabl.textColor=UIColor(red: 34.0/255.0, green: 139/255.0, blue: 34/255.0, alpha: 1.0)
            
        }
            
        else
            
        {
           statuslabl.textColor=UIColor.orangeColor()
            
        }

    }
    @IBAction func didClickDeleteBtn(sender: AnyObject) {
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
