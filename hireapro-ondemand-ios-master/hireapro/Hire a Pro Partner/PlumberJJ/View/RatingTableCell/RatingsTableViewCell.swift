//
//  RatingsTableViewCell.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 12/12/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
protocol ratingsDelegate {
    
    func ratingsCount(withRateVal:Float , withIndex:NSIndexPath)
   
}
class RatingsTableViewCell: UITableViewCell,FloatRatingViewDelegate {
var delegate:ratingsDelegate?
    var objIndexPath:NSIndexPath=NSIndexPath()
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.delegate=self
        ratingView.halfRatings = false
        ratingView.floatRatings = false
        
        // Initialization code
    }
    func loadRateTableCell(objRateRec:RatingsRecord){
       
           ratingView.rating = Float (objRateRec.rateCount as String)!
    }
    
    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float){
        self.delegate?.ratingsCount(rating, withIndex: objIndexPath)
    }

 
    
//    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float){
//        self.delegate?.ratingsCount(rating, withIndex: objIndexPath)
//    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
