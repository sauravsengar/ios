//
//  SearchTableViewCell.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 8/3/2017.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    @IBOutlet var lblName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
