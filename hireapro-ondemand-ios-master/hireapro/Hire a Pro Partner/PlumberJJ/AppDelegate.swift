//
//  AppDelegate.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

var jobDetail : JobDetailRecord = JobDetailRecord()
var trackingDetail:TrackingDetails = TrackingDetails()
var Language_handler:Languagehandler=Languagehandler()
var Message_details:MessageView=MessageView()
var Schedule_Data:c=ScheduleView()
var DidEnterBackground : NSString=NSString()
var lastDriving = CLLocationDirection()
var bearing = Double()
var CurLaat:Double!
var CurLong:Double!


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , XMPPRosterDelegate, XMPPStreamDelegate,CLLocationManagerDelegate{
    var theme:Theme=Theme()
    var window: UIWindow?
    let locationManager = CLLocationManager()
    let xmppStream = XMPPStream()
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster
    var ConnectionTimer:NSTimer=NSTimer()
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var LocationTimer : NSTimer = NSTimer()

    override init() {
        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(hitMethodWhenAppActive), name:UIApplicationDidBecomeActiveNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(hitMethodWhenAppBackground), name:UIApplicationDidEnterBackgroundNotification, object: nil)
        

        UIApplication.sharedApplication().idleTimerDisabled = true

        NSUserDefaults.standardUserDefaults().removeObjectForKey("Availability")
        
        // Override point for customization after application launch.
       
     

        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        theme.saveLanguage("en")
        theme.SetLanguageToApp()
        
           NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.methodOfReceivedNotification(_:)), name:"didClickProfileBtnPostNotif", object: nil)
           NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.methodOfReceivedNotificationNetwork(_:)), name:kNoNetwork, object: nil)
        
//        BITHockeyManager.sharedHockeyManager().configureWithIdentifier("4a6d6242b4654b6c8b68c7c558b3f8e4")
//        BITHockeyManager.sharedHockeyManager().startManager()
//        BITHockeyManager.sharedHockeyManager().authenticator.authenticateInstallation()
        //Api Key for Google Map
        GMSServices.provideAPIKey(googleApiKey)
        
       if(theme.isUserLigin()){
            setInitialViewcontroller()
            //self.setXmppConnect()
        }
        
        if launchOptions != nil{
            var alert = UIAlertView()
            if theme.isUserLigin(){
                let localNotif = launchOptions![UIApplicationLaunchOptionsRemoteNotificationKey]
                    as? Dictionary<NSObject,AnyObject>
                
                if localNotif != nil
                {
                    
                    let objUserRecs:UserInfoRecord=theme.GetUserDetails()
                    
                    let providerid : NSString = objUserRecs.providerId
                    let checkuserid=theme.CheckNullValue(localNotif!["tasker"])!
                    let taskerid_fromsupport = theme.CheckNullValue(localNotif!["user"])!

                    if (providerid == checkuserid)
                    {
                        
                        let ChatMessage:NSArray = localNotif!["messages"] as! NSArray
                        
                        var Message_Notice:NSString=NSString()
                        var taskid:NSString=NSString()
                        var messageid : NSString = NSString()
                        
                        
                        let status : NSString = theme.CheckNullValue(localNotif!["status"])!
                        if status == "1"
                        {
                            
                            Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
                            taskid = localNotif!["task"] as! NSString
                            messageid = ChatMessage[0].objectForKey("_id") as! NSString
                            
                            let userid : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("from"))!
                            let user_status : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("user_status"))!
                            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                            dispatch_after(delayTime, dispatch_get_main_queue()) {
                                NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChat", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status])
                                NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChatToRootView", object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                            }
                            
                        }
                        
                        
                    }
                    else if  (taskerid_fromsupport == providerid)
                    {
                        
                        let reference_id: String = (theme.CheckNullValue(localNotif!["reference"]))!
                        
                        let SupportMessage: NSArray = localNotif!["messages"] as! NSArray
                        let getsupportmsg = theme.CheckNullValue(SupportMessage[0].objectForKey("message")!)
                        
                        let getdate = theme.CheckNullValue(SupportMessage[0].objectForKey("date")!)
                        let from_id :String = theme.CheckNullValue(SupportMessage[0].objectForKey("from")!)!
                        
                       
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChat", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\( from_id)","date":"\(getdate!)","reference":"\(reference_id)"])
                        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","admin":"\(  from_id)","reference":"\(reference_id)"])
                        }
                        
                    }
                    
                    else{
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            self.APNSNotification(localNotif! as NSDictionary)
                        }
                        
                    }
                }
            }
        }
        window?.backgroundColor=UIColor.whiteColor()
        window?.makeKeyAndVisible()
        
        
        return true

        }
    
    func hitMethodWhenAppBackground() {
        
        self.locationManager.startMonitoringSignificantLocationChanges()
        
    }
    
    func hitMethodWhenAppActive() {
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.activityType = .AutomotiveNavigation
        if #available(iOS 9.0, *) {
            self.locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.startUpdatingLocation()
        
    }

    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        let direct = newHeading.trueHeading
        lastDriving = direct
        
    }

    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        CurLaat=locValue.latitude
        CurLong=locValue.longitude
        
        NSNotificationCenter.defaultCenter().postNotificationName("UpdateLocationNotify", object: nil, userInfo:nil)
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .NotDetermined, .Restricted, .Denied:
                let alertView = UNAlertView(title:Language_handler.VJLocalizedString("location_disable_title", comment: nil), message: Language_handler.VJLocalizedString("location_disable_message", comment: nil))
                alertView.addButton(kOk, action: {
                    
                })
                alertView.show()
                break
                
            case .AuthorizedAlways, .AuthorizedWhenInUse: break
                
            }
        } else {
            let alertView = UNAlertView(title:Language_handler.VJLocalizedString("location_disable_title", comment: nil), message: Language_handler.VJLocalizedString("location_disable_message", comment: nil))
            alertView.addButton(kOk, action: {
                
            })
            alertView.show()
        }
    }

    func ReconnectMethod()
    {
        
        
        NSThread.detachNewThreadSelector(#selector(EstablishCohnnection), toTarget: self, withObject: nil)
    }
 

    func EstablishCohnnection()
    {
         if(SocketIOManager.sharedInstance.ChatSocket.status == .NotConnected || SocketIOManager.sharedInstance.ChatSocket.status ==  .Closed)
        {
            if(theme.isUserLigin())
            {
                //Listen To server side Chat related notification
                
                SocketIOManager.sharedInstance.establishChatConnection()
                
                
                
                
            }
            
            
            
        }
        
       if(SocketIOManager.sharedInstance.socket.status == .NotConnected || SocketIOManager.sharedInstance.socket.status ==  .Closed)
        {
            
            
            //Listen To server side Job related notification
            
            if(theme.isUserLigin())
            {  SocketIOManager.sharedInstance.establishConnection()
                
                
            }
            
        }
        
    }

    

    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for var i = 0; i < deviceToken.length; i++ {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        if tokenString == ""
        {
            tokenString = "Simulator Signup"

        }
        
        theme.SaveDeviceTokenString(tokenString)
        print("tokenString: \(tokenString)")
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        
        
        
        
        //  if userInfo.contains("tasker_status")
        
        let userInfoDict:NSDictionary?=userInfo
        

        
        print("get userinformation=\(userInfoDict)")
        let checkuserid = theme.CheckNullValue(userInfoDict!.objectForKey("tasker"))!
        
        let taskerid_fromsupport = theme.CheckNullValue(userInfoDict!.objectForKey("user"))!

        if userInfoDict != nil
        {
            
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            if (providerid == checkuserid)
            {
                
                
                var Message_Notice:NSString=NSString()
                var taskid:NSString=NSString()
                var messageid : NSString = NSString()
                
                
                let status : NSString = theme.CheckNullValue(userInfoDict!.objectForKey("status"))!
                if status == "1"
                {
                    let ChatMessage:NSArray = userInfoDict!.objectForKey("messages") as! NSArray
                    
                    
                    Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
                    taskid = userInfoDict!.objectForKey("task") as! NSString
                    messageid = ChatMessage[0].objectForKey("_id") as! NSString
                    
                    
                    let userid : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("from"))!
                    let user_status : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("user_status"))!
                    NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChat", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status])
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("ReceivePushChatToRootView", object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                    
                }
                
                
            }
            else if (taskerid_fromsupport == providerid)
            {
                
                let Refernce_id : String = (theme.CheckNullValue(userInfoDict!.objectForKey("reference")!))!
                
                let SupportMessage: NSArray = userInfoDict!.objectForKey("messages") as! NSArray
                let getsupportmsg = theme.CheckNullValue(SupportMessage[0].objectForKey("message")!)
                
                let getdate = theme.CheckNullValue(SupportMessage[0].objectForKey("date")!)
                let from_id:String = theme.CheckNullValue(SupportMessage[0].objectForKey("from")!)!
                
                
                
                NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChat", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","reference":"\( Refernce_id)"])
                NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportPushChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id)","date":"\(getdate!)","admin":"\(from_id)","reference":"\( Refernce_id)"])

            }
            else{
                
                APNSNotification(userInfo as NSDictionary)
                
                
            }
        }
        
        
    }
    
    func APNSNotification(dict : NSDictionary)
    {
      
        var Message_Notice:NSString=NSString()
        var Action:NSString = NSString()
        var key : NSString = NSString()
        var key0 : NSString = NSString()
        var key3 :NSString = NSString()
        var key4 : NSString = NSString()
        let Message:NSDictionary=dict
        
        
      
        
            Message_Notice=Message.objectForKey("message") as! NSString
            Action=Message.objectForKey("action")as! NSString
        key = theme.CheckNullValue(Message.objectForKey("key1"))!
        
        key0 = theme.CheckNullValue(Message.objectForKey("key0"))!
        
        key3 = theme.CheckNullValue(Message.objectForKey("key3"))!
        key4 = theme.CheckNullValue(Message.objectForKey("key4"))!
        
        
            
            
            
            if (Action == "job_request")
            {
                NSNotificationCenter.defaultCenter().postNotificationName(kPushNotification, object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key0)","action":"\(Action)"])
                
            }
                
            else if (Action == "job_cancelled")
            {
              NSNotificationCenter.defaultCenter().postNotificationName(kPushNotification, object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key0)","action":"\(Action)"])
                
            }
                
            else if(Action == "admin_notification")
            {
                
            }
                else if (Action == "receive_cash" )
            {
                NSNotificationCenter.defaultCenter().postNotificationName(kPushNotification, object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key)","action":"\(Action)" ,"price":"\(key3)" ,"currency":"\(key4)"])

            }
                
                
            else
            {
                
                let keyval: NSString = theme.CheckNullValue(Message.objectForKey("key0"))!
                
                NSNotificationCenter.defaultCenter().postNotificationName(kPushNotification, object:nil,userInfo :["message":"\(Message_Notice)","key":"\(keyval)","action":"\(Action)"])
                
            }
            
            
            
        }
        
        
        
    

    
    func  socketTypeNotification(dict:NSDictionary)  {
        
        let Userid:NSString = dict.objectForKey("user") as! NSString
        
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = self.theme.CheckNullValue(chatDict.objectForKey("task"))!
        
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveTypingMessage", object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
    }
    
    
    
    func socketStopTypeNotification(dict:NSDictionary)
    {    let Userid:NSString = dict.objectForKey("user") as! NSString
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = self.theme.CheckNullValue(chatDict.objectForKey("task"))!
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveStopTypingMessage", object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
    }
    
    
    
    func socketSupportNotification(dict:NSDictionary)
    {
        
        let Message:NSDictionary=(dict as? NSDictionary!)!
        
        //Message_details.admin_id = (theme.CheckNullValue(Message.objectForKey("admin")!))!
       // Message_details.support_chatid = (theme.CheckNullValue(Message.objectForKey("reference")!))!
        
        let SupportMessage: NSArray = Message.objectForKey("messages") as! NSArray
        let getsupportmsg = theme.CheckNullValue(SupportMessage[0].objectForKey("message")!)
        
        let getdate = theme.CheckNullValue(SupportMessage[0].objectForKey("date")!)
        let from_id = theme.CheckNullValue(SupportMessage[0].objectForKey("from")!)
        let admin_id = theme.CheckNullValue(Message.objectForKey("admin"))!
        let reference_id = theme.CheckNullValue(Message.objectForKey("reference"))!
        
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportChat", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id!)","date":"\(getdate!)","reference":"\(reference_id)"])
        NSNotificationCenter.defaultCenter().postNotificationName("ReceiveSupportChatRootView", object: nil, userInfo: ["message":"\(getsupportmsg!)","from":"\(from_id!)","date":"\(getdate!)","admin":"\(admin_id)","reference":"\(reference_id)"])
        
        
    }
    func socketChatNotification(dict:NSDictionary)
    {
        
        let Message:NSDictionary = dict.objectForKey("message") as! NSDictionary
        var Message_Notice:NSString=NSString()
        var taskid:NSString=NSString()
        var messageid : NSString = NSString()
        var dateStr : NSString = NSString()
        var getuserid : NSString = NSString ()


      
        let status : NSString = theme.CheckNullValue(Message.objectForKey("status"))!
        if status == "1"
        {
            let ChatMessage:NSArray = Message.objectForKey("messages") as! NSArray
            
            
            Message_Notice = ChatMessage[0].objectForKey("message") as! NSString
            taskid = Message.objectForKey("task") as! NSString
            getuserid = self.theme.CheckNullValue(Message.objectForKey("user"))!
            
            messageid = ChatMessage[0].objectForKey("_id") as! NSString
            dateStr = ChatMessage[0].objectForKey("date") as! NSString
            
            
            let userid : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("from"))!
            let user_status : NSString = theme.CheckNullValue(ChatMessage[0].objectForKey("user_status"))!
            NSNotificationCenter.defaultCenter().postNotificationName("ReceiveChat", object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status,"date" : dateStr,"user":getuserid])
            
            
            
            NSNotificationCenter.defaultCenter().postNotificationName("ReceiveChatToRootView", object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
            
        }
        

    }

    
    func socketNotification(dict:NSDictionary)
    {
        var messageArray:NSMutableDictionary=NSMutableDictionary()
        var Message_Notice:NSString=NSString()
        var Action:NSString = NSString()
        let Message:NSDictionary?=dict["message"] as? NSDictionary
        print("the user info is \(dict)...\(Message)")
        
        
        if(Message != nil)
        {
            messageArray=(Message?.objectForKey("message") as? NSMutableDictionary)!
            Message_Notice=(messageArray.objectForKey("message") as? NSString)!
            Action=(messageArray.objectForKey("action") as? NSString)!
            
            
            
            if (Action == "job_request")
            {
                NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Message)
                
            }
                
            else if (Action == "job_cancelled")
            {
                NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Message)
                
            }
                
                
            else
            {
                
                /*let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                 NSNotificationCenter.defaultCenter().postNotificationName("ShowNotification", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])*/
                NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Message)
                
                
            }
            
            
            
        }

        
    }

    
    func applicationWillResignActive(application: UIApplication) {
        
        ConnectionTimer.invalidate()
        ConnectionTimer = NSTimer()
        
        
        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
            SocketIOManager.sharedInstance.RemoveAllListener()
            
        }
        
       

        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        if(theme.isUserLigin()){
            let URL_Handler:URLhandler=URLhandler()
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let param=["user_type":"provider","id":"\(objUserRecs.providerId)","mode":"unavailable"]
            URL_Handler.makeCall(UserAvailableUrl, param: param, completionHandler: { (responseObject, error) -> () in
                
            })
        }
        disconnect()
    }

    func applicationDidEnterBackground(application: UIApplication) {
        ConnectionTimer.invalidate()
        ConnectionTimer = NSTimer()
        
        backgroundTaskIdentifier = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        
        self.locationManager.startMonitoringSignificantLocationChanges()
        LocationTimer.invalidate()
        LocationTimer = NSTimer()
        LocationTimer = NSTimer.scheduledTimerWithTimeInterval(30, target: self, selector: #selector(ReconnectLocationMethod), userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(LocationTimer, forMode: NSRunLoopCommonModes)
        DidEnterBackground = "Yes"
        
        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
           
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
             SocketIOManager.sharedInstance.RemoveAllListener()
            
        }

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(applßication: UIApplication) {
        
     
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        LocationTimer.invalidate()
        LocationTimer = NSTimer()
        
      
        DidEnterBackground = "No"

        ConnectionTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(ReconnectMethod), userInfo: nil, repeats: true)
        
     
        
        
        if(theme.isUserLigin())
        {
            SocketIOManager.sharedInstance.establishConnection()
            
            SocketIOManager.sharedInstance.establishChatConnection()
        }
        
    }
    
    func ReconnectLocationMethod()
    {
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        print("call hitted")
    }

    func applicationWillTerminate(application: UIApplication) {
        ConnectionTimer.invalidate()
        ConnectionTimer = NSTimer()
        
        LocationTimer.invalidate()
        LocationTimer = NSTimer()
        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
            SocketIOManager.sharedInstance.RemoveAllListener()
            
        }

        
        
        
             //SocketIOManager.sharedInstance.closeConnection()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func methodOfReceivedNotification(notification: NSNotification){
        guard let url = notification.object else {
            return // or throw
        }
        let blob = url as! NSString // or as! Sting or as! Int
        if(blob .isEqualToString("1")){
             MakeRootVc("HomeVCSID")
        }
        else if(blob .isEqualToString("2")){
            MakeRootVc("MyJobsVcSegue")
        }
        else if(blob .isEqualToString("3")){
            MakeRootVc("BankingDetailSegue")
        }
        else if(blob .isEqualToString("4")){
            MakeRootVc("MyProfileSegue")
        }
        else if(blob .isEqualToString("5")){
            MakeRootVc("InitialVCSEGUe")
            let objDict:NSDictionary=NSDictionary()
            theme.saveUserDetail(objDict)
        }
    }
    
    func MakeRootVc(ViewIdStr:NSString){
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      let initialVc = self.window!.rootViewController as! UINavigationController
        let rootViewController:UIViewController = storyboard.instantiateViewControllerWithIdentifier("\(ViewIdStr)") as UIViewController
       initialVc.navigationController?.pushViewController(rootViewController, animated: true)
        
//        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let rootView:UIViewController = sb.instantiateViewControllerWithIdentifier("\(ViewIdStr)")
//        
//         self.window!.rootViewController = rootView
   
    }
    
    func setInitialViewcontroller(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objLoginVc:DEMORootViewController = mainStoryboard.instantiateViewControllerWithIdentifier("rootController") as! DEMORootViewController
        let navigationController: UINavigationController = UINavigationController(rootViewController: objLoginVc)
        self.window!.rootViewController = navigationController
        self.window!.backgroundColor = UIColor.whiteColor()
        navigationController.setNavigationBarHidden(true, animated: true)
        self.window!.makeKeyAndVisible()
    }
    
    func setInitailLogOut(){
        disconnect()
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let rootView: UINavigationController = sb.instantiateViewControllerWithIdentifier("StarterNavSid" as String) as! UINavigationController
        UIView.transitionWithView(self.window!, duration: 0.2, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            appDel.window?.rootViewController=rootView
            }, completion: nil)
         //[loginController.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
    
  
    
    func methodOfReceivedNotificationNetwork(notification: NSNotification){
        let navigationController = window?.rootViewController as? UINavigationController
        if let activeController = navigationController!.visibleViewController {
        
            let image = UIImage(named: "NoNetworkConn")
            activeController.view.makeToast(message:kErrorMsg, duration: 5, position:HRToastActivityPositionDefault, title: "Oops !!!!", image: image!)
        }
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
  /////////////// XMPP
    
    
    func setXmppConnect(){
        DDLog.addLogger(DDTTYLogger.sharedInstance())
        setupStream()
    }
    
    //MARK: Private Methods
    private func setupStream() {
        //		xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        xmppRoster.activate(xmppStream)
        xmppStream.addDelegate(self, delegateQueue: dispatch_get_main_queue())
        xmppRoster.addDelegate(self, delegateQueue: dispatch_get_main_queue())
        xmppStream.hostName=xmppHostName //192.168.1.150  //67.219.149.186
        xmppStream.hostPort=5222
        
    }
    
    private func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream.myJID.domain
        
        print("the domain is is \(domain).....")
        
        let priority = DDXMLElement.elementWithName("priority", stringValue: "24") as! DDXMLElement
        presence.addChild(priority)
        
        // 			let priority = DDXMLElement.elementWithName("192.168.1.148", stringValue: "5222") as! DDXMLElement
        //			presence.addChild(priority)
        xmppStream.sendElement(presence)
    }
    
    private func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream.sendElement(presence)
    }
    
    func connect() -> Bool {
        
        
        if !xmppStream.isConnected() {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
             var myPassword:String? = String()
            let jabberID:String? = "\(objUserRecs.providerId)@\(xmppJabberId)" //messaging.dectar.com // casp83
            if(theme.getJaberPassword() != nil)
            {
           myPassword = "\(theme.getJaberPassword()!)"
            }
            
            
           
            //            xmppStream.hostName="192.168.1.148"
            //            xmppStream.hostPort=5222
            print("the username is \(jabberID).....\(jabberID)")
            if !xmppStream.isDisconnected() {
                return true
            }
            if jabberID == nil && myPassword == nil {
                return false
            }
            xmppStream.myJID = XMPPJID.jidWithString(jabberID!)
            //            let domain = xmppStream.myJID.domain
            do {
                try xmppStream.connectWithTimeout(XMPPStreamTimeoutNone)
                print("Connection success")
                return true
            } catch {
                print("Something went wrong!")
                return false
            }
        } else {
            return true
        }
    }
    
    func disconnect() {
        goOffline()
        xmppStream.disconnect()
    }
    
    //MARK: XMPP Delegates
    func xmppStreamDidConnect(sender: XMPPStream!) {
        
        do {
            try	xmppStream.authenticateWithPassword("\(theme.getJaberPassword()!)")
        } catch {
            print("Could not authenticate")
        }
    }
    
    func xmppStreamDidAuthenticate(sender: XMPPStream!) {
        goOnline()
    }
    
    func xmppStream(sender: XMPPStream!, didReceiveIQ iq: XMPPIQ!) -> Bool {
        print("Did receive IQ")
        return false
    }
    
    func xmppStream(sender: XMPPStream!, didReceiveMessage message: XMPPMessage!) {
        
        //        var parseError: NSErrorPointer? = nil
        //        var xmlDictionary: [NSObject : AnyObject] = XMLReader.dictionaryForXMLString(testXMLString, error: &parseError)
        
        //        var ParseError:NSErrorPointer?=nil
        //        var xmlDictionary:NSDictionary=
        print("Did receive message \(message)")
        var Job_id:NSString?=""

        
        var MessageString="\(message)"
        var errorStr:NSString=""
        // Parse the XML into a dictionary
        do
        {
            
            let xmlDictionary:NSDictionary = try XMLReader.dictionaryForXMLString(MessageString)
            
            print("The parsed message is  \(xmlDictionary)")
            errorStr=(xmlDictionary.objectForKey("message")?.objectForKey("type"))! as! NSString
            
            Job_id=xmlDictionary.objectForKey("message")?.objectForKey("jobid") as? NSString
            
            if(Job_id == nil)
            {
                 MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String

                
                let xmppRecMsg:NSString = MessageString.stringByReplacingOccurrencesOfString("+", withString: " ").stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                
                
                let data:NSData = xmppRecMsg.dataUsingEncoding(NSUTF8StringEncoding)!
                var Dictionay:NSDictionary?=NSDictionary()
                do {
                    
                    Dictionay = try NSJSONSerialization.JSONObjectWithData(
                        
                        data,
                        
                        options: NSJSONReadingOptions.MutableContainers
                        
                        ) as? NSDictionary
                    
                    
                    if(Dictionay != nil && Dictionay?.count>0)
                    {
                        
                        let Message_Notice:NSString?=Dictionay?.objectForKey("message") as? NSString
                        let Action:NSString?=Dictionay?.objectForKey("action") as? NSString
                        let alertView = UNAlertView(title: appNameJJ, message: "\(Message_Notice)")
                        alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                            
                        })
                        alertView.show()

                        
                        if(Action! == "job_request")
                        {
                            NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Dictionay)
                        }
                        else  if(Action! == "receive_cash")          {
                            
                            NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Dictionay)
                            
                        }
                        else  if(Action! == "payment_paid")          {
                            
                            NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Dictionay)
                            
                        }
                        else  if(Action! == "job_cancelled")          {
                            NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Dictionay)
                        }else  if(Action! == "job_assigned")          {
                            NSNotificationCenter.defaultCenter().postNotificationName(kPaymentPaidNotif, object: Dictionay)
                        }
                        else{
                            let alertView = UNAlertView(title: appNameJJ, message: "\(Message_Notice)")
                            alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                
                            })
                            alertView.show()
                        }
                        
                        
                    }
                    else{
                        
                    }
                    

                    
                }
                    
                    
                catch let error as NSError {
                    
                    
                    // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
                    print("A JSON parsing error occurred, here are the details:\n \(error)")
                    Dictionay=nil
                    
                }
                
                
                
                
                
             }
                else
            {
                
                if(errorStr=="cancel"){
                    print("The parsed message is  \(xmlDictionary).....\(MessageString)")
                }
                
              else  if(MessageString != "Type" && MessageString != "StopType" && errorStr == "chat")
            {
                MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                
                if(errorStr=="error"){
                    print("The parsed message is  \(xmlDictionary).....\(MessageString)")
                }else{
                    MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                    NSNotificationCenter.defaultCenter().postNotificationName(kChatFromOthers, object: MessageString)
                }
            }

            else if(errorStr == "Typing")
            {
                
                Job_id=(xmlDictionary.objectForKey("message")?.objectForKey("jobid"))! as? NSString
                
                
                    
                    MessageString=xmlDictionary.objectForKey("message")?.objectForKey("body")?.objectForKey("text") as! NSString as String
                    //   var Job_Id=xmlDictionary.objectForKey("message")?.objectForKey("jobid")! as! NSString
                    NSNotificationCenter.defaultCenter().postNotificationName(kTypeStatus, object: MessageString)
             }
            }

            
        }
        catch {
            let Alert:UIAlertView=UIAlertView()
            Alert.title="\(MessageString)"
            Alert.addButtonWithTitle(Language_handler.VJLocalizedString("ok", comment: nil))
            Alert.show()
        }
        
        
        
    }

    func xmppStream(sender: XMPPStream!, didSendMessage message: XMPPMessage!) {
        print("Did send message \(message)")
    }
    
    func xmppStream(sender: XMPPStream!, didReceivePresence presence: XMPPPresence!) {
        let presenceType = presence.type()
        let myUsername = sender.myJID.user
        let presenceFromUser = presence.from().user
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(presenceFromUser)")
            if presenceType == "available" {
            } else if presenceType == "unavailable" {
            }
        }
    }
    
    func xmppRoster(sender: XMPPRoster!, didReceiveRosterItem item: DDXMLElement!) {
        print("Did receive Roster item")
    }


}

