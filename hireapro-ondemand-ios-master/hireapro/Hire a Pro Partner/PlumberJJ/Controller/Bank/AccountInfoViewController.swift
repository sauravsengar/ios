//
//  AccountInfoViewController.swift
//  PlumberJJ
//
//  Created by Natarajan on 13/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit



class AccountInfoViewController: RootBaseViewController, UITextFieldDelegate  {
    @IBOutlet weak var accountTitle: UILabel!
    
    @IBOutlet var selectView: UIView!
    @IBOutlet weak var menu: UIButton!

    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var routingNumberTxt: UITextField!

    @IBOutlet var accountType: IQDropDownTextField!
       @IBOutlet weak var accountNumberTxt: UITextField!
    @IBOutlet weak var SAVE: UIButton!
    
    @IBOutlet weak var accountNumber: SMIconLabel!
    @IBOutlet weak var routingNumber: SMIconLabel!
    @IBOutlet weak var accountName: SMIconLabel!
    @IBOutlet weak var accountNameTxt: UITextField!
    @IBOutlet var accounttype: SMIconLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tooBar: UIToolbar = UIToolbar()
        tooBar.barStyle = UIBarStyle.Default
        tooBar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "donePressed")]
        tooBar.sizeToFit()
        accountNumberTxt.inputAccessoryView = tooBar
        routingNumberTxt.inputAccessoryView = tooBar

        accountNumberTxt.keyboardType = UIKeyboardType.DecimalPad
        routingNumberTxt.keyboardType = UIKeyboardType.DecimalPad
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(AccountInfoViewController.DismissKeyboard(_:)))
        
        view.addGestureRecognizer(tapgesture)
        
        let paddingViewname = UIView(frame: CGRectMake(10, 0, 15, self.accountNameTxt.frame.height))
        accountNameTxt.leftView = paddingViewname
        accountNameTxt.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewnumber = UIView(frame: CGRectMake(10, 0, 15, self.accountNumberTxt.frame.height))
        accountNumberTxt.leftView = paddingViewnumber
        accountNumberTxt.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewrouting = UIView(frame: CGRectMake(10, 0, 15, self.routingNumberTxt.frame.height))
        routingNumberTxt.leftView = paddingViewrouting
        routingNumberTxt.leftViewMode = UITextFieldViewMode.Always
        
        let paddingViewtype = UIView(frame: CGRectMake(10, 0, 15, self.accountType.frame.height))
        accountType.leftView = paddingViewtype
        accountType.leftViewMode = UITextFieldViewMode.Always
        
         accountType.showDismissToolbar = true;
        accountType.itemList = ["Individual","Company"];
        SAVE.setTitle(Language_handler.VJLocalizedString("save", comment: nil), forState: .Normal)
        accountTitle.text = Language_handler.VJLocalizedString("account_info", comment: nil)
         accountName.text = Language_handler.VJLocalizedString("account_name", comment: nil)
        accountNumber.text = Language_handler.VJLocalizedString("account_number", comment: nil)
        routingNumber.text = Language_handler.VJLocalizedString("routing_number", comment: nil)
        accounttype.text = Language_handler.VJLocalizedString("account_type", comment: nil)
        
        
        // Do any additional setup after loading the view.
    }
    func donePressed () {
        routingNumberTxt.resignFirstResponder()
        accountNumberTxt.resignFirstResponder()

    }
    func textField(textField: IQDropDownTextField, didSelectItem item: String?) {
        print("selected item=\(item)")
    }

    func textFieldDidBeginEditing(textField: UITextField) {
           }
    
    func textFieldDidEndEditing(textField: UITextField) {
       
    }
    func textField(textField: IQDropDownTextField, canSelectItem item: String?) -> Bool {
//print("\(NSStringFromSelector(#function)): \(item)")
        return true
    }
    
    func doneClicked(button: UIBarButtonItem) {
        view.endEditing(true)
    }


    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetDatasForBanking(){
        showProgress()
        let objBankRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["userId":"\(objBankRecs.providerId)"]
        
        url_handler.makeCall(GetBankDetails, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil)
                {
                    
                    
                    self.setDatasToBankingView(responseObject!)
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
        }
    }
    
    func setDatasToBankingView(Dict:NSDictionary){
        let status:NSString=self.theme.CheckNullValue(Dict["status"])!
        
        if(status == "1")
        {
            let resDict: NSDictionary = Dict.objectForKey("response")?.objectForKey("banking") as! NSDictionary
            
            accountName.text=theme.CheckNullValue(resDict.objectForKey( "account_name" )as! NSString as String)
            
            accountNumber.text=theme.CheckNullValue(resDict.objectForKey( "account_number" ) as! NSString as String)
            routingNumber.text=theme.CheckNullValue(resDict.objectForKey( "routing_number" ) as! NSString as String)
            accounttype.text=theme.CheckNullValue(resDict.objectForKey( "account_type" ) as! NSString as String)
            
            
            
        }
        else
        {
            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
        }
    }

    @IBAction func didClickSaveBtn(sender: AnyObject) {
        if(validateTxtFields()){
            showProgress()
            let objBankRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["userId":"\(objBankRecs.providerId)",
                                     "acc_holder_name":"\(accountNameTxt.text!)",
                                     
                                     "acc_number":"\(accountNumberTxt.text!)",
                                     "routing_number":"\(routingNumberTxt.text!)",
                                     "account_holder_type":"\(accountType.selectedItem!)"
                                    ]
            
            url_handler.makeCall(SaveBankDetails, param: Param) {
                (responseObject, error) -> () in
                self.DismissProgress()
                if(error != nil)
                {
                    print("error responded with: \(error)")
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
                else
                {
                    if(responseObject != nil)
                    {
                        let status : String = self.theme.CheckNullValue(responseObject!.objectForKey("status"))!
                        if status == "1"
                        {
                            let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("bankVCID") as! BankViewController
                            objChatVc.check = 1
                        self.navigationController!.pushViewController(objChatVc, animated: true)
                        }
                            
                        else{
                            
                            let response = self.theme.CheckNullValue(responseObject!.objectForKey("message"))!
                            self.theme.AlertView("\(appNameJJ)", Message: response, ButtonTitle: kOk)
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
            }
        }
    }
   
    func validateTxtFields () -> Bool{
        var isOK:Bool=true
        if(accountNameTxt.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("holder_name_mand", comment: nil))
            isOK=false
        }
        else if(accountNumberTxt.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("account_number_mand", comment: nil))
            isOK=false
        }
            
        else if(routingNumberTxt.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("routing_number_mand", comment: nil))
            isOK=false
        }
        else if(accountType.selectedItem!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("account_type_mand", comment: nil))
            isOK=false
        }
             return isOK
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        accountNameTxt.resignFirstResponder()
        accountNumberTxt.resignFirstResponder()
        routingNumberTxt.resignFirstResponder()
        selectView.resignFirstResponder()
        return true
    }
    
func ValidationAlert(alertMsg:NSString){
    let popup = NotificationAlertView.popupWithText("\(alertMsg)")
    popup.hideAfterDelay = 3
    //popup.position = NotificationAlertViewPosition.Bottom
    popup.animationDuration = 1
    popup.show()
}
    func DismissKeyboard(sender:UITapGestureRecognizer)
    {
        
        scrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
        
        view.endEditing(true)
        
        
    }
//    func keyboardWillShow(notification:NSNotification){
//        
//        var userInfo = notification.userInfo!
//        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
//        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
//        
//        var contentInset:UIEdgeInsets = bankscrollview.contentInset
//        contentInset.bottom = keyboardFrame.size.height+30
//        bankscrollview.contentInset = contentInset
//    }
//    func keyboardWillHide(notification:NSNotification){
//        
//        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
//        bankscrollview.contentInset = contentInset
//    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(range.location==0 && string==" "){
            return false
        }
        
        if string == "."{
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
