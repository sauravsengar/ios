//
//  BankViewController.swift
//  PlumberJJ
//
//  Created by Natarajan on 13/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class BankViewController: RootBaseViewController {
   

    @IBOutlet weak var header_Title: UILabel!
    @IBOutlet weak var addAccount: UIButton!
    
    @IBOutlet var bankTblView: UITableView!
    @IBOutlet weak var menu: UIButton!
    

    let themes:Theme=Theme()
    var check = 0
    var AccnNameArray : NSMutableArray = NSMutableArray()
    var AccnNumberArray : NSMutableArray = NSMutableArray()
    var AccnTypeArray : NSMutableArray = NSMutableArray()
    var AccnIdArray : NSMutableArray = NSMutableArray()
    var AccnDefaultArray : NSMutableArray = NSMutableArray()
    var URL_handler:URLhandler=URLhandler()
    var checkStatus:Bool = false
    override func viewDidLoad() {

        self.bankTblView.hidden = true
        header_Title.text = Language_handler.VJLocalizedString("bank_details", comment: nil)
        
        addAccount.setTitle(Language_handler.VJLocalizedString("add_account", comment: nil), forState: UIControlState.Normal)
        
        
        super.viewDidLoad()
        self.GetTransaction()
          menu.addTarget(self, action: #selector(BankViewController.openmenu), forControlEvents: .TouchUpInside)
         bankTblView.registerNib(UINib(nibName: "BankTableViewCell", bundle: nil), forCellReuseIdentifier: "BankDetailsCell")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didclickpush(sender: AnyObject) {
        
        
        if  checkStatus == true{
            let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("AccountVCID") as! AccountInfoViewController
            self.navigationController!.pushViewController(objChatVc, animated: true)

        }
        else{
           // GetTransaction()
            
            let alert = UIAlertController(title: Language_handler.VJLocalizedString("app_name", comment: nil), message: Language_handler.VJLocalizedString("Please_contact_the_administrator", comment: nil)
, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("Close", comment: nil), style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }

    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
       
        self.frostedViewController.presentMenuViewController()
    }
    
    func deleteDetails(sender:UIButton) {
        self.showProgress()
        let objBankRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objBankRecs.providerId)","account":"\(AccnIdArray.objectAtIndex(sender.tag))"]
        URL_handler.makeCall(DeleteBankDetails, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {

                        self.GetTransaction()
                    
                         self.view.makeToast(message:"Deleted Successfully", duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                        }
                          }
                else
                {
                    self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                }
            }
            
        }
        
    }
    
    func makeDefault(sender:UIButton) {
        self.showProgress()
        let objBankRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objBankRecs.providerId)","account":"\(AccnIdArray.objectAtIndex(sender.tag))"]
        URL_handler.makeCall(MakeDefault, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {
                         self.AccnDefaultArray.removeAllObjects()
                        self.GetTransaction()
                        self.view.makeToast(message:"Made as Default", duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                        
                    }
                        

                    
                }
                else
                {
                    self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                }
            }
            
        }
        
        
        
    }
    
    
    func GetTransaction() {
        self.showProgress()
       
        let objBankRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objBankRecs.providerId)"]

        URL_handler.makeCall(GetBankDetails, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                   self.AccnIdArray.removeAllObjects()
                   self.AccnNumberArray.removeAllObjects()
                    self.AccnTypeArray.removeAllObjects()
                    self.AccnNameArray.removeAllObjects()
                    
                    if(Status == "1")
                    {
                        self.checkStatus = true
                        let ResponseDic:NSDictionary=Dict.objectForKey("stripe") as! NSDictionary
                        let totalBankDetail : NSArray = ResponseDic.objectForKey("data") as! NSArray
                        
                        for BankDict in totalBankDetail
                        {
                            
                            
                            let accnName:NSString=self.themes.CheckNullValue(BankDict.objectForKey("bank_name"))!
                            self.AccnNameArray.addObject(accnName)
                            let accnNumber:NSString=self.themes.CheckNullValue(BankDict.objectForKey("last4"))!
                            self.AccnNumberArray.addObject(accnNumber)
                            let accnType:NSString=self.themes.CheckNullValue(BankDict.objectForKey("account_holder_type"))!
                            self.AccnTypeArray.addObject(accnType)
                            
                            let accnId:NSString=self.themes.CheckNullValue(BankDict.objectForKey("id"))!
                            self.AccnIdArray.addObject(accnId)
                            
                            let accndefault:NSString=self.themes.CheckNullValue(BankDict.objectForKey("default_for_currency"))!
                            self.AccnDefaultArray.addObject(accndefault)
                        
                        }
                        
                        if (totalBankDetail.count == 0)
                        {
                            self.view.makeToast(message:Language_handler.VJLocalizedString("Account_Details_unavailable", comment: nil), duration: 5, position: HRToastPositionDefault, title: "\(appNameJJ)")
                        }
                        
                        
                        self.bankTblView.reloadData()
                        self.bankTblView.hidden = false
                        
                    }
                    else if(Status == "2")
                    {
                         self.checkStatus = false
                       // self.addAccount.enabled = false
//                        let Response:NSString = self.theme.CheckNullValue(Dict.objectForKey("response"))!
//                        self.view.makeToast(message:"\(Response)", duration: 3, position: HRToastPositionDefault, title: "Hire A Pro Provider")
                        
                    }

                    
                }
                else
                {
                    self.view.makeToast(message:Language_handler.VJLocalizedString(Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                }
                
            }
            
        }
       
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AccnNameArray.count > 0{
            return self.AccnNameArray.count
            
        }
        else{
            return 0
            
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("BankDetailsCell") as! BankTableViewCell
        
        Cell.totalview.layer.shadowOffset = CGSize(width: 2, height: 2)
        // Cell.totalview.layer.cornerRadius=14;
        Cell.totalview.layer.shadowOpacity = 0.2
        Cell.totalview.layer.shadowRadius = 2
        Cell.totalview.layer.cornerRadius = 5.0
        if AccnNameArray.count > 0
        {
            
            Cell.bankNameLbl.text = self.AccnNameArray.objectAtIndex(indexPath.row) as? String
            Cell.numberLbl.text =  "********"+(self.AccnNumberArray.objectAtIndex(indexPath.row) as! String) as? String
            Cell.typeLbl.text = self.AccnTypeArray.objectAtIndex(indexPath.row) as? String
       
        }
        Cell.deleteBtn.tag = indexPath.row
        Cell.makeDefault.tag = indexPath.row
        
        Cell.deleteBtn.addTarget(self, action: #selector(BankViewController.deleteDetails(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        Cell.makeDefault.addTarget(self, action: #selector(BankViewController.makeDefault(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        if AccnDefaultArray[indexPath.row] as! NSObject == "1" {
            Cell.deleteBtn.enabled = false
            Cell.makeDefault.enabled = false
            Cell.deleteBtn.backgroundColor = UIColor.grayColor()
            Cell.makeDefault.backgroundColor = UIColor.grayColor()

        }
        else {
            Cell.deleteBtn.enabled = true
            Cell.makeDefault.enabled = true
            Cell.deleteBtn.backgroundColor = UIColor(red: 71/255.0, green: 51/255.0, blue: 150/255.0, alpha: 1)
            Cell.makeDefault.backgroundColor = UIColor(red: 71/255.0, green: 51/255.0, blue: 150/255.0, alpha: 1)
        }
        
        return Cell
    }


}
