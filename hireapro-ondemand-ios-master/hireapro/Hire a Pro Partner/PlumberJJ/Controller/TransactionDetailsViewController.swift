//
//  TransactionDetailsViewController.swift
//  Plumbal
//
//  Created by Casperon on 07/02/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class TransactionDetailsViewController: RootBaseViewController,UITableViewDelegate,UITableViewDataSource {
    var themes:Theme=Theme()
    let URL_Handler:URLhandler=URLhandler()
    var GetJob_id : NSString!
    @IBOutlet var transacTableView: UITableView!
    var titleArray = NSMutableArray()
    var descArray = NSMutableArray()
   @IBOutlet var ViewTaskDetails: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet var taxamount: UILabel!
    @IBOutlet var taskamount: UILabel!
    @IBOutlet var Bookingidlabl: UILabel!
    @IBOutlet var misNameLbl: UILabel!
    @IBOutlet var misPriceLbl: UILabel!
    
    @IBOutlet var total_lable: UILabel!
    @IBOutlet var mis_amount: UILabel!
    @IBOutlet var mis_lable: UILabel!
    @IBOutlet var tax_lable: UILabel!
    @IBOutlet var transcroll: UIScrollView!
    @IBOutlet var mainview: UIView!
    @IBOutlet var tasktime: UILabel!
    @IBOutlet var minimum_rate: UILabel!
    @IBOutlet var perhour: UILabel!
    @IBOutlet var totalhours: UILabel!
    @IBOutlet var taskaddress: UILabel!
    @IBOutlet var taskername: UILabel!
    @IBOutlet var categoryname: UILabel!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var totalView: UIView!
    
   @IBOutlet var BookingId: UILabel!
   @IBOutlet var TaskCategory: UILabel!
    @IBOutlet var UserName: UILabel!
    @IBOutlet var TaskAddress: UILabel!
    @IBOutlet var TotalHours: UILabel!
    @IBOutlet var TaskTime: UILabel!
    
    //@IBOutlet var categoryname: UILabel!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BookingId.text = Language_handler.VJLocalizedString("BookingId", comment: nil)
        TaskCategory.text = Language_handler.VJLocalizedString("TaskCategory", comment: nil)
        UserName.text = Language_handler.VJLocalizedString("UserName", comment: nil)
        TaskAddress.text = Language_handler.VJLocalizedString("TaskAddress", comment: nil)
        TotalHours.text = Language_handler.VJLocalizedString("TotalHours", comment: nil)
        TaskTime.text = Language_handler.VJLocalizedString("TaskTime", comment: nil)

        
        
        ViewTaskDetails.text = Language_handler.VJLocalizedString("View_Task_Details", comment: nil)
        transacTableView.estimatedRowHeight = 80
        let Nb=UINib(nibName: "TransactionDetailTableViewCell", bundle: nil)
        self.transacTableView.registerNib(Nb, forCellReuseIdentifier: "TransactionDetailTableViewCell")

   
        totalView.layer.shadowOffset = CGSize(width: 2, height: 2)
        totalView.layer.shadowOpacity = 0.2
        totalView.layer.shadowRadius = 2
        mainview.layer.shadowOffset = CGSize(width: 2, height: 2)
        mainview.layer.shadowOpacity = 0.2
        mainview.layer.shadowRadius = 2
        
        totalView.hidden = true
        mainview.hidden  = true
    
        self.showProgress()
        self.GetTransaction()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAct(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetTransaction()  {
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        
        let param=["provider_id":"\(objUserRecs.providerId)","booking_id":"\(GetJob_id)"]
        print("newnlsdknfglsdfg",param)
        URL_Handler.makeCall(View_Transaction_Detail, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString = self.themes.CheckNullValue( Dict.objectForKey("status"))!
                    
                    if(Status == "1")
                    {
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let TotalJobsArray : NSArray = ResponseDic.objectForKey("jobs") as! NSArray
                        let categoryName = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("category_name"))!
                        let taskerName = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("user_name"))!
                        
                        let provider_lat = self.theme.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("lat_provider"))!
                        let provider_lng = self.theme.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("lng_provider"))!
                        
                        
                        let taskAddress =  self.theme.CheckNullValue(self.theme.getAddressForLatLng(provider_lat, longitude: provider_lng, status: "short"))
                        //                        self.taskaddress.text = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("location"))!
                        let totalHour = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("total_hrs"))!
                        let perHour = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("per_hour"))!)"
                        let basePrice = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("min_hrly_rate"))!)"
                        let taskTime = self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("booking_time"))!
                        
                        let bookingId = "\(self.GetJob_id as String)"
                        let serviceAMt = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("task_amount"))!)"
                        let commision = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("admin_commission"))!)"
                        
                        let total = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("total_amount"))!)"
                        
                         let materialarray : NSArray = TotalJobsArray.objectAtIndex(0).objectForKey("meterial_details") as! NSArray
                      
                        
                        var get_mis_amount = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("meterial_fee"))!)"
                        
                        let mode = self.themes.CheckNullValue(self.themes.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("payment_mode")))!

                        if get_mis_amount == self.themes.getappCurrencycode(){
                            get_mis_amount = "---"
                        }
                        
                        
                        let Category_type = self.theme.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("category_type"))!
                        let flat_amount = "\(self.themes.getappCurrencycode())\(self.theme.CheckNullValue(TotalJobsArray.objectAtIndex(0).objectForKey("flat_amount"))!)"
                        
                        
                        if Category_type == "Flat Rate"
                        {
                            
                            self.titleArray = ["Booking_Id",
                                          "Task_Category",
                                          "Category_Type",
                                          "User_Name",
                                          "Task_Address",
                                          "Total_Hours",
                                          "Flat_Price",
                                          "Task_Time",
                                          "Task_Amount",
                                          
//
                                
                                
                            ]
                            
                            self.descArray = [bookingId,
                                              categoryName,
                                              Category_type,
                                              taskerName,
                                              taskAddress!,
                                              totalHour,
                                              flat_amount,
                                              taskTime,
                                              serviceAMt,
                                             
                            ]
                            

                            
                        }
                        else
                        {
                            
                            
                            self.titleArray = ["Booking_Id",
                                          "Task_Category",
                                          "Category_Type",
                                          "User_Name",
                                          "Task_Address",
                                          "Total_Hours",
                                          "Price_per_Hour",
                                          "Task_Time",
                                          "Task_Amount",
                                          
//                                          "Name",
                                         // "Price"
                                
                            ]
                            
                            self.descArray = [bookingId,
                                              categoryName,
                                              Category_type,
                                              taskerName,
                                              taskAddress!,
                                              totalHour,
                                              perHour,
                                             
                                              taskTime,
                                              serviceAMt,
                                              
                                            
                            ]
                            
                        }
                        

                        
                       for (index, element) in materialarray.enumerate() {
                            
                        
                            var category = element as! NSDictionary
                            


                            let name = self.themes.CheckNullValue(category.valueForKey("name"))!
                            let price = "\(self.themes.getappCurrencycode())\(self.themes.CheckNullValue(category.valueForKey("price"))!)"
                        self.titleArray.addObject("\(self.theme.setLang("Tool")) \(index+1): \(name)")
                        self.descArray.addObject("\(price)")
                        
                        
                        }
                        
                        let TotalCalArray : NSArray = ["Material_Fee",
                                                       "Payment_Mode",
                                                       "Admin_Commission",
                                                       "Total_Amount"]
                        self.titleArray.addObjectsFromArray(TotalCalArray as [AnyObject])
                        
                        let TotalDecArray : NSArray = [ get_mis_amount,
                                                         mode,
                                                         commision,
                                                         total]
                        self.descArray.addObjectsFromArray(TotalDecArray as [AnyObject])
                        
                        
                        self.transacTableView.delegate = self
                        self.transacTableView.dataSource = self
                        self.transacTableView.reloadData()
                        
                    }
                    else
                    {
                    }
                    
                    self.transcroll.contentSize = CGSizeMake(self.view.frame.size.width,self.mainview.frame.size.height+self.totalView.frame.size.height+30)
                    
                    
                    self.totalView.hidden = false
                    self.mainview.hidden  = false
                    
                    
                }
                else
                {
                    self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "\(appNameJJ)")
                }
            }
            
        }
    }
    
   func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCellWithIdentifier("TransactionDetailTableViewCell") as! TransactionDetailTableViewCell
        Cell.lblTitle.text = self.theme.setLang((titleArray.objectAtIndex(indexPath.row) as? String)!)
        Cell.lblDescL.text = self.theme.setLang((descArray.objectAtIndex(indexPath.row) as? String)!)
         Cell.lblTitle.sizeToFit()
        Cell.lblTitle.numberOfLines = 0
        if indexPath.row == 0 || indexPath.row == titleArray.count-1{
            
            
            Cell.lblTitle.textColor = UIColor.init(red: 32/250, green: 109/250, blue: 22/250, alpha: 1)
            Cell.lblDescL.textColor = UIColor.init(red: 32/250, green: 109/250, blue: 22/250, alpha: 1)
        }else{
            Cell.lblTitle.textColor = UIColor.darkGrayColor()
            Cell.lblDescL.textColor = UIColor.grayColor()
 
        }
        return Cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
