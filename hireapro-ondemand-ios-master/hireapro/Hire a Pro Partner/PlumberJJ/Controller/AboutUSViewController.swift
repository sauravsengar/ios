//
//  AboutUSViewController.swift
//  PlumberJJ
//
//  Created by Casperon on 03/10/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

import UIKit

class AboutUSViewController: RootBaseViewController {
   @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet var versionlabl: UILabel!
    @IBOutlet var titlelabl: UILabel!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var aboutWebview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url : NSURL = NSURL(string: Aboutus_url as String)!
        let request : NSURLRequest = NSURLRequest.init(URL: url)
        self.aboutWebview.loadRequest(request)

   menubtn.addTarget(self, action: #selector(AboutUSViewController.openmenu), forControlEvents: .TouchUpInside)
        // Do any additional setup after loading the view.
        let version = ( NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as! String)
        titleHeader.text = Language_handler.VJLocalizedString("about_us", comment: nil)
        versionlabl.text = "\(Language_handler.VJLocalizedString("version", comment: nil)) : \(version)"
        titlelabl.text = "\(Language_handler.VJLocalizedString("powered_by", comment: nil)) : \(appNameJJ)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
