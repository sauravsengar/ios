//
//  PlumberOTPViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 12/11/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class PlumberOTPViewController: RootBaseViewController,UITextFieldDelegate {
    //var theme:Theme=Theme()
    @IBOutlet weak var otpDiscLbl: UILabel!
    @IBOutlet weak var titleHeader: UILabel!
    var otpStr:NSString!
    var currencyStr:NSString!
    var priceStr:NSString!
    var jobIDStr:NSString!
    @IBOutlet weak var btnSubmit: ButtonColorView!

    @IBOutlet weak var otpTxtField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        otpTxtField.layer.cornerRadius=5
        otpTxtField.layer.borderWidth=0.75
        otpTxtField.layer.borderColor=PlumberLightGrayColor.CGColor
        otpTxtField.layer.masksToBounds=true
        otpTxtField.textAlignment = .Center
        
        
        otpDiscLbl.text = Language_handler.VJLocalizedString("otp_disc", comment: nil)
        titleHeader.text = Language_handler.VJLocalizedString("one_time_pwd", comment: nil);
        otpTxtField.placeholder = Language_handler.VJLocalizedString("otp_placeholder", comment: nil);
        btnSubmit.setTitle(Language_handler.VJLocalizedString("submit", comment: nil), forState: UIControlState.Normal)

        GetOTPDetails()
        // Do any additional setup after loading the view.
    }
    
    func GetOTPDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(jobIDStr)"]
        print(Param)
        self.showProgress()
        url_handler.makeCall(RequestCashUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString = self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        self.otpStr=self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("otp_string"))
                        self.currencyStr=self.theme.getCurrencyCode((responseObject?.objectForKey("response")?.objectForKey("currency"))! as! String)
                        let prStr:NSString!=self.theme.CheckNullValue(responseObject!.objectForKey("response")!.objectForKey("receive_amount"))!
                        self.priceStr="\(self.currencyStr)\(prStr)"
                        
                if(self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("otp_status"))=="development"){
                            self.otpTxtField.text="\(self.otpStr)";
                    self.otpTxtField.userInteractionEnabled = false
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKindOfClass(MyOrderOpenDetailViewController) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    @IBAction func didClickSubmitBtn(sender: AnyObject) {
        self.view.endEditing(true)
        if(otpStr==otpTxtField.text){
            let objReceiveCashvc = self.storyboard!.instantiateViewControllerWithIdentifier("ReceiveCashVCSID") as! ReceiveCashViewController
            objReceiveCashvc.otpStr = self.otpStr
            objReceiveCashvc.priceString=priceStr
            objReceiveCashvc.jobIDStr=jobIDStr
            self.navigationController!.pushViewController(objReceiveCashvc, animated: true)
        }else{
            ValidationAlert(Language_handler.VJLocalizedString("otp_alert", comment: nil))

        }
    }
    func ValidationAlert(alertMsg:NSString){
        let popup = NotificationAlertView.popupWithText("\(alertMsg)")
        popup.hideAfterDelay = 3
        //popup.position = NotificationAlertViewPosition.Bottom
        popup.animationDuration = 1
        popup.show()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
