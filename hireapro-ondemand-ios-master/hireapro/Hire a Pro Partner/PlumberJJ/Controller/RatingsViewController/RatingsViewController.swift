//
//  RatingsViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 12/11/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//


import UIKit
import AssetsLibrary
import Foundation
import Alamofire



class RatingsViewController: RootBaseViewController, UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ratingsDelegate {
    
    @IBOutlet var Review_title: UILabel!
    
    @IBOutlet weak var titleHeader: UILabel!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet var Provider_image: UIImageView!
    
    @IBOutlet var addservicebtn: UIButton!
    
    @IBOutlet var service_image: UIImageView!
     var jobIDStr:NSString!
    @IBOutlet weak var ratingBtn: UIButton!
    let imagePicker = UIImagePickerController()
    //var theme:Theme=Theme()
    var get_imagedata : NSData = NSData()
    var get_pickerimage: UIImage?

    var ratingsOptArr:NSMutableArray = [];
    @IBOutlet weak var reviewTxtView: UITextView!
    @IBOutlet weak var reviewTblView: UITableView!
    override func viewDidLoad() {
        self.ratingBtn.hidden=true
        super.viewDidLoad()
        
        
        btnSkip.setTitle(Language_handler.VJLocalizedString("skip", comment: nil), forState: UIControlState.Normal)
        titleHeader.text = Language_handler.VJLocalizedString("rating", comment: nil)
        ratingBtn.setTitle(Language_handler.VJLocalizedString("submit", comment: nil), forState: UIControlState.Normal)
        addservicebtn.setTitle(Language_handler.VJLocalizedString("add_image", comment: nil), forState: UIControlState.Normal)
        
        Provider_image.layer.cornerRadius=Provider_image.frame.size.width/2
        Provider_image.clipsToBounds=true
       // Provider_image.layer.borderWidth=1.0
       // Provider_image.layer.borderColor = theme.Lightgray().CGColor
        

            imagePicker.delegate=self
        reviewTxtView.layer.cornerRadius=5
        reviewTxtView.layer.borderWidth=1
        //reviewTxtView.layer.borderColor=PlumberLightGrayColor.CGColor
        reviewTxtView.layer.masksToBounds=true
        reviewTblView.registerNib(UINib(nibName: "RatingsTableViewCell", bundle: nil), forCellReuseIdentifier: "RatingCellIdentifier")
        reviewTblView.estimatedRowHeight = 125
        reviewTblView.rowHeight = UITableViewAutomaticDimension
        reviewTblView.tableFooterView = UIView()
        GetRatingsOption()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didclickimage(sender: AnyObject) {
        let ImagePicker_Sheet = UIAlertController(title: nil, message: "\(Language_handler.VJLocalizedString("select_image", comment: nil))", preferredStyle: .ActionSheet)
        
        let Camera_Picker = UIAlertAction(title: "\(Language_handler.VJLocalizedString("camera", comment: nil))", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.Camera_Pick()
        })
        let Gallery_Picker = UIAlertAction(title: "\(Language_handler.VJLocalizedString("gallery", comment: nil))", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            //
            self.Gallery_Pick()
            
        })
        
        let cancelAction = UIAlertAction(title: "\(Language_handler.VJLocalizedString("cancel", comment: nil))", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        ImagePicker_Sheet.addAction(Camera_Picker)
        ImagePicker_Sheet.addAction(Gallery_Picker)
        ImagePicker_Sheet.addAction(cancelAction)
        
        self.presentViewController(ImagePicker_Sheet, animated: true, completion: nil)
        
    }
    
    func Camera_Pick()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .Camera
            self.imagePicker.modalPresentationStyle = .Popover
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
            
        else
        {
            Gallery_Pick()
        }
    }
    
    func Gallery_Pick()
    {
        
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .PhotoLibrary
        self.imagePicker.modalPresentationStyle = .Popover
        self.presentViewController(self.imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        //handle media here i.e. do stuff with photo
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        let url = info[UIImagePickerControllerReferenceURL]
        
        addservicebtn.setTitle("", forState:UIControlState.Normal)
        

        if (url !=  nil)
        {
            
            //let pickimage = self.theme.removeRotation(info[UIImagePickerControllerOriginalImage] as! UIImage)
         //  get_pickerimage =  UIImage.init(CGImage: pickimage.CGImage!, scale: 0.25 , orientation:.DownMirrored)
            
            get_imagedata = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage] as! UIImage ,0.1)!
            
            service_image .image = info[UIImagePickerControllerOriginalImage] as! UIImage
            //data!.writeToFile(localPath, atomically: true)
            
        }
        else
        {
            
           // let pickimage = self.theme.removeRotation(info[UIImagePickerControllerOriginalImage] as! UIImage)
         //  get_pickerimage =  UIImage.init(CGImage: pickimage.CGImage!, scale: 0.25 , orientation:.DownMirrored)
            
            get_imagedata = UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage] as! UIImage ,0.1)!
            
            service_image .image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            
        }
        
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //what happens when you cancel
        //which, in our case, is just to get rid of the photo picker which pops up
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    
    func GetRatingsOption(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["holder_type":"provider", "user":objUserRecs.providerId,"job_id":jobIDStr]
       
        self.showProgress()
        url_handler.makeCall(GetRatingsOptionUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!;                    if(status == "1")
                    {
                        let  listArr:NSArray=(responseObject?.objectForKey("review_options") as? NSArray)!
                        self.Review_title.text = self.theme.CheckNullValue(listArr[0].objectForKey("option_name"))!
                        if(listArr.count>0){
                             self.ratingBtn.hidden=false
                            
                            for (_, element) in listArr.enumerate() {
                                let result1:RatingsRecord=RatingsRecord()
                                result1.title=self.theme.CheckNullValue(element.objectForKey("option_title"))!
                                let optionInt : Int = (element.objectForKey("option_id")) as! Int
                                let strOptionId = "\(optionInt)"
                                result1.optionId=self.theme.CheckNullValue(strOptionId)!
                                
                                self.Provider_image.sd_setImageWithURL(NSURL(string:self.theme.CheckNullValue(element.objectForKey("image"))!), placeholderImage: UIImage(named: "PlaceHolderSmall"))

                                //result1.optionId=self.theme.CheckNullValue(element.objectForKey("option_id"))!
                                result1.rateCount = String(0)
                                self.ratingsOptArr .addObject(result1)
                            }
                            
                            
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                        }
                        self.reviewTblView.reloadData()
                        //This code will run in the main thread:
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    func SaveUserRatings(){
       
        
            
                
                let param : NSDictionary =  self.dictForrating()
                
                 NSLog("getDevicetoken =%@", self.dictForrating())
                self.showProgress()
                let objUserRecs:UserInfoRecord=theme.GetUserDetails()
                
                Alamofire.upload(.POST, "\(SaveRatingsOptionUrl)", headers:  ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(objUserRecs.providerId)"],multipartFormData: {
                    multipartFormData in
               
                        multipartFormData.appendBodyPart(data: self.get_imagedata, name: "file", fileName: "file.png", mimeType: "")
                    
                    for (key, value) in param {
                        print("get Dictionary value=\(value)")
                        multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key as! String)
                    }
                    }, encodingCompletion: {
                        encodingResult in
                        
                        switch encodingResult {
                            
                        case .Success(let upload, _, _):
                            print("s")
                            
                            upload.responseJSON { response in
                                
                                
                                if let JSON = response.result.value {
                                    
                                    self.DismissProgress()
                                    print("JSON: \(JSON)")
                                    let Status:NSString = self.theme.CheckNullValue(JSON.objectForKey("status"))!
                                   let response:NSDictionary = JSON.objectForKey("response") as! NSDictionary
                                    if(Status == "1")
                                    {
                                        
                                        
                                        //        self.view.makeToast(message:self.themes.CheckNullValue(response.objectForKey("msg"))!, duration: 4, position: HRToastPositionDefault, title: "")
                                        
                                        self.navigationController?.popToRootViewControllerAnimated(true)
                                        
                                    }
                                    else
                                    {
                                        
                                        //  self.themes.AlertView("Image Upload Failed", Message: "Please try again", ButtonTitle: "Ok")
                                        self.view.makeToast(message:self.theme.CheckNullValue(response.objectForKey("msg"))!, duration: 4, position: HRToastPositionDefault, title: "")
                                        
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                            }
                            
                        case .Failure(let encodingError):
                            self.DismissProgress()
                            print(" the encodeing error is \(encodingError)")
                            
                            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                        }
                })
                
            
            
            
            
            
            
            
          /*  //let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            self.showProgress()
            url_handler.makeCall(SaveRatingsOptionUrl, param: dictForrating()) {
                (responseObject, error) -> () in
                
                self.DismissProgress()
                if(error != nil)
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
                }
                else
                {
                    if(responseObject != nil && responseObject?.count>0)
                    {
                        let status:NSString=responseObject?.objectForKey("status") as! NSString
                        if(status == "1")
                        {
                            self.navigationController?.popToRootViewControllerAnimated(true)
                        }
                        else
                        {
                            self.view.makeToast(message:responseObject?.objectForKey("response") as! NSString as String, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
                    }
                }
                
            }*/
       
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ratingsOptArr.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        let cell:RatingsTableViewCell = tableView.dequeueReusableCellWithIdentifier("RatingCellIdentifier") as! RatingsTableViewCell
        cell.objIndexPath=indexPath
        cell.loadRateTableCell(ratingsOptArr.objectAtIndex(indexPath.row) as! RatingsRecord)
        cell.delegate=self
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        return cell
    }
    @IBAction func didClickBackbtn(sender: AnyObject) {
    
         self.navigationController?.popToRootViewControllerAnimated(true)
    }
    @IBAction func didClickRateUserBtn(sender: AnyObject) {
        SaveUserRatings()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dictForrating()->NSDictionary{
        var cmtstr:NSString=""
        if(reviewTxtView.text=="Review"){
            cmtstr=""
        }else{
            cmtstr=reviewTxtView.text
        }
        
        let reviewDict:NSMutableDictionary=NSMutableDictionary()
        for i in 0 ..< ratingsOptArr.count {
            let str1: String = "ratings[\(i)][option_title]"
            let str2: String = "ratings[\(i)][option_id]"
            let str3: String = "ratings[\(i)][rating]"
            let objRatingsRecs: RatingsRecord = ratingsOptArr[i] as! RatingsRecord
            reviewDict.setValue(objRatingsRecs.title, forKey:str1)
            reviewDict.setValue(objRatingsRecs.optionId,forKey:str2)
            reviewDict.setValue(objRatingsRecs.rateCount, forKey: str3)
        }
        reviewDict.setValue("tasker", forKey: "ratingsFor")
        reviewDict.setValue(jobIDStr, forKey: "job_id")
        reviewDict.setValue(cmtstr, forKey: "comments")
        reviewDict.setValue("ios", forKey: "type") 

        

        return reviewDict
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(range.location==0 && text==" "){
            return false
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func ratingsCount(withRateVal:Float , withIndex:NSIndexPath){
        
        let objRatingsRecs: RatingsRecord = ratingsOptArr[withIndex.row] as! RatingsRecord
        objRatingsRecs.rateCount = String (withRateVal)
        ratingsOptArr.replaceObjectAtIndex(withIndex.row, withObject: objRatingsRecs)
       
        self.reviewTblView.beginUpdates()
        self.reviewTblView.reloadRowsAtIndexPaths([withIndex], withRowAnimation: .None)
        self.reviewTblView.endUpdates()
    }
    func validateTxtFields () -> Bool{
    var isOK:Bool=true
        if(reviewTxtView.text.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("review_mand", comment: nil))
            isOK = false
            return isOK
        }
//        for var i = 0; i < ratingsOptArr.count; i++ {
//            let objRatingsRecs: RatingsRecord = ratingsOptArr[i] as! RatingsRecord
//            let rateVal: Float = Float(objRatingsRecs.rateCount as String)!
//            if rateVal == 0 {
//                ValidationAlert("\(objRatingsRecs.title) is mandatory")
//                isOK = false
//                 break;
//            }
//        }
        
        
        
        
        
        
        
        
    return isOK
    }
    
    func ValidationAlert(alertMsg:NSString){
        let popup = NotificationAlertView.popupWithText("\(alertMsg)")
        popup.hideAfterDelay = 3
        //popup.position = NotificationAlertViewPosition.Bottom
        popup.animationDuration = 1
        popup.show()
    }
    
 
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
