//
//  MiscellaneousVC.swift
//  PlumberJJ
//
//  Created by Casperon on 15/03/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

protocol MiscellaneousVCDelegate {
    
    func pressedCancelMaterial(sender: MiscellaneousVC)
    
}

class MiscellaneousVC: RootBaseViewController,UITextFieldDelegate ,UIGestureRecognizerDelegate{
    
    @IBOutlet var MiscellaneousDetails: UILabel!
    
    @IBOutlet var noteaddmaterial: UILabel!
    @IBOutlet var Note: UILabel!
     @IBOutlet var addmaterial: UIButton!
     @IBOutlet var ok: UIButton!
     @IBOutlet var cancel: UIButton!
    @IBOutlet var borderview: UIView!
    @IBOutlet var cancelbtn: ButtonColorView!
    @IBOutlet var okview: UIView!
    var tablecount : NSInteger!
    var materialArray : NSMutableArray = NSMutableArray()
    var jobIDStr:NSString!
    var currency:NSString!
    var allDetails = NSMutableArray()
    var isName = false
    var isRent = false
    var materilDetailDict = ["name":"","price":"","row":""]
    var details = materialDetails()
    var isSelected = false
    var delegate:MiscellaneousVCDelegate?

    @IBOutlet var addMaterialImgView:UIImageView!
    

    
    struct materialDetails{
        var name :String!
        var price:String!
    }
    
    @IBAction func didClickAddMaterial(sender: AnyObject) {
        if isSelected == false{
            isSelected = true
        addMaterialImgView.image = UIImage.init(named: "Checked Checkbox 2-26")
        materialTableview.hidden = false
        }else{
            isSelected = false
            addMaterialImgView.image = UIImage.init(named: "Unchecked Checkbox-48")
            materialTableview.hidden = true
            allDetails = NSMutableArray()

        }
    }
    

    @IBAction func didclickapply(sender: AnyObject) {
        self.view.endEditing(true)
        
        
                self.showProgress()
        
        
                url_handler.makeCall(JobCompletedUrl, param: formDict()) {
                    (responseObject, error) -> () in
                    self.DismissProgress()
        
                    if(error != nil)
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                    else
                    {
                        if(responseObject != nil && responseObject?.count>0)
                        {
                            let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                            if(status == "1")
                            {
        
                                let dict:NSDictionary=(responseObject?.objectForKey("response"))! as! NSDictionary
                                
                                self.delegate?.pressedCancelMaterial(self)

        
                            }
                            else
                            {//alertImg
                                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                            }
                        }
                        else
                        {
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                        }
                    }
                    
                }
        
        

    }
    
    @IBOutlet var materialTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       MiscellaneousDetails.text = Language_handler.VJLocalizedString("MiscellaneousDetails", comment: nil)
        noteaddmaterial.text = Language_handler.VJLocalizedString("note_add_material", comment: nil)
        Note.text = Language_handler.VJLocalizedString("Note", comment: nil)
        ok.setTitle(Language_handler.VJLocalizedString("ok", comment: nil), forState: .Normal)
        addmaterial.setTitle(Language_handler.VJLocalizedString("add_material", comment: nil), forState: .Normal)
        cancel.setTitle(Language_handler.VJLocalizedString("cancel", comment: nil), forState: .Normal)

        borderview.layer.cornerRadius = 5
        borderview.clipsToBounds=true
        borderview.layer.borderWidth = 1.0
        borderview.layer.borderColor = PlumberThemeColor.CGColor
        okview.layer.cornerRadius = 3
        okview.clipsToBounds=true
        cancelbtn.layer.cornerRadius = 3
        cancelbtn.clipsToBounds=true
        materialTableview.hidden = true

        tablecount = 2
        
        materialTableview.registerNib(UINib(nibName: "MaterialCell", bundle: nil), forCellReuseIdentifier: "materialcell")
        materialTableview.estimatedRowHeight = 50
        materialTableview.rowHeight = UITableViewAutomaticDimension
        materialTableview.tableFooterView = UIView()
             // Do any additional setup after loading the view.
    }

    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didclickback(sender: AnyObject) {
self.delegate?.pressedCancelMaterial(self)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tablecount
        
    }
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("materialcell", forIndexPath: indexPath) as! MaterialCell
               cell.selectionStyle=UITableViewCellSelectionStyle.None
        cell.cancelbtn.tag = indexPath.row
        cell.toolname.delegate = self
        cell.toolrent.delegate = self
        cell.toolrent.tag = indexPath.row
        cell.toolname.tag = indexPath.row
            cell.toolrent.placeholder = Language_handler.VJLocalizedString("Tool_Rent", comment: nil)
            cell.toolname.placeholder = Language_handler.VJLocalizedString("Tool_Name", comment: nil)
            let paddingView = UILabel.init(frame: CGRectMake(30, 2, 20, 20))
            
            
            paddingView.text = "   \(currency)"
            paddingView.font = UIFont.init(name: "Roboto", size: 14)
            cell.toolrent.leftView = paddingView;
            cell.toolrent.leftViewMode = UITextFieldViewMode.Always

            
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
            doneToolbar.barStyle = UIBarStyle.Default
            doneToolbar.backgroundColor=UIColor.whiteColor()
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(MiscellaneousVC.doneButtonAction))
            doneToolbar.items = [flexSpace,done]
            
            doneToolbar.sizeToFit()
            
            cell.toolrent.inputAccessoryView = doneToolbar
            

        if indexPath.row ==  tablecount-1
        {
            cell.toolname.hidden = true
            cell.toolrent.hidden = true
            cell.cancelbtn.hidden = true
            cell.addfieldbtn.hidden = false
            
            cell.addfieldbtn.addTarget(self, action: #selector(MiscellaneousVC.AddFieldAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
        }
        else
        {
            
            if (indexPath.row ==  tablecount-2)
            {
                if tablecount == 2{
                    cell.cancelbtn.hidden = true

                }else{
                 cell.cancelbtn.hidden = false
                cell.cancelbtn.addTarget(self, action: #selector(MiscellaneousVC.DeleteFieldAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                }

            }
            else
            {
                 cell.cancelbtn.hidden = true
            }
            
            
            cell.toolname.hidden = false
            cell.toolrent.hidden = false
           
            cell.addfieldbtn.hidden = true
        }
        
        
        return cell
    }
    
    func doneButtonAction()
    {
        view.endEditing(true)

          }

    
    func AddFieldAction(sender:UIButton){
        //self.view.endEditing(true)
        if isRent == true && isName == true{
            isRent = false
            isName = false
        tablecount = tablecount+1
        materialTableview.reloadData()
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                let indexPath = NSIndexPath.init(forRow: self.tablecount-1, inSection: 0)
//                self.materialTableview.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
//            })
        }
        
        }
        
        
    
    func DeleteFieldAction(sender:UIButton){
        self.view.endEditing(true)

        print("get row \(sender.tag)")
        if sender.tag < allDetails.count{
        allDetails.removeObjectAtIndex(sender.tag)
        }
        print(allDetails)
        isRent = true
        isName = true
        
        tablecount = tablecount-1
        materialTableview.reloadData()

        
    }

    func formDict()->NSDictionary{
        let dictRecord = NSMutableDictionary()
        for i in 0 ..< allDetails.count {
            let maerialDet = allDetails.objectAtIndex(i) as! NSDictionary
            dictRecord["miscellaneous[\(i)][name]"] = maerialDet.valueForKey("name")
            dictRecord["miscellaneous[\(i)][price]"] = maerialDet.valueForKey("price")
        }
        
        dictRecord["job_id"] = "\(jobIDStr)"
        let obj = theme.GetUserDetails()
        dictRecord["provider_id"] = "\(obj.providerId)"

        
        print(dictRecord)
        return dictRecord
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print(textField.tag)
        print(textField.text)
        let indexPathOfLastSelectedRow = NSIndexPath(forRow: textField.tag, inSection: 0)
        let tableViewCell = materialTableview.cellForRowAtIndexPath(indexPathOfLastSelectedRow) as! MaterialCell
        var isReturn = false
        for i in 0 ..< allDetails.count{
            let item = allDetails.objectAtIndex(i) as! NSDictionary
            if item.valueForKey("row") as! String == "\(textField.tag)"{
                isReturn = true
                if textField == tableViewCell.toolname{
                    materilDetailDict.updateValue(textField.text!, forKey:"name")
                    materilDetailDict.updateValue(item.valueForKey("price") as! String, forKey:"price")
                    materilDetailDict.updateValue("\(textField.tag)", forKey:"row")
                    allDetails.removeObjectAtIndex(i)
                    allDetails.addObject(materilDetailDict)
                }else if textField == tableViewCell.toolrent{
                    materilDetailDict.updateValue(item.valueForKey("name") as! String, forKey:"name")
                    materilDetailDict.updateValue(textField.text!, forKey:"price")
                    materilDetailDict.updateValue("\(textField.tag)", forKey:"row")
                    allDetails.removeObjectAtIndex(i)
                    allDetails.addObject(materilDetailDict)
                }
            }
        }
        
        if isReturn == false{
        if textField == tableViewCell.toolname{
            if textField.text != ""{
                isName = true
            }
            details.name = textField.text
            
        }else if textField == tableViewCell.toolrent{
            if textField.text != ""{
                isRent = true
            }
            details.price = textField.text
        }
        if isName == true && isRent == true{
            if details.name != nil && details.price != nil{
            materilDetailDict.updateValue(details.name, forKey:"name")
            materilDetailDict.updateValue(details.price, forKey:"price")
            materilDetailDict.updateValue("\(textField.tag)", forKey:"row")
            allDetails.addObject(materilDetailDict)
            details = materialDetails()
            }
        }
        }
        
        print(allDetails)
 
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
