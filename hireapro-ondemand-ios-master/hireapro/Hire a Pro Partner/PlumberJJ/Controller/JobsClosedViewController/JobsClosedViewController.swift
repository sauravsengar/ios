//
//  JobsClosedViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/20/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class JobsClosedViewController: RootBaseViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var tableViewFooter: MyFooter!
    @IBOutlet weak var jobsClosedTbl: UITableView!
    @IBOutlet var loading_Lbl: UILabel!
    var jobsClosedArr:NSMutableArray=NSMutableArray()
 //   var theme:Theme=Theme()
     var Param: NSDictionary = NSDictionary()
    var nextPageStr:NSInteger!
    var noDataView:NoDataView!
    private var loading = false {
        didSet {
            tableViewFooter.hidden = !loading
        }
    }
    override func viewWillAppear(animated: Bool) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loading_Lbl.text = Language_handler.VJLocalizedString("loading", comment: nil)

          NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(JobsClosedViewController.methodOfReceivedSortingNotificationNetworkDetail(_:)), name:"SortingJobNotification", object: nil)
        jobsClosedTbl.hidden=true
        nextPageStr=1
        refreshNewLeads()
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        Param = ["provider_id":"\(objUserRecs.providerId)",
                                 "type":"4",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]

        GetNewLeads()
        // Do any additional setup after loading the view.
    }
    
    func methodOfReceivedSortingNotificationNetworkDetail(notification: NSNotification){
        // loadNewFeed()
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        nextPageStr = 1;
        Param  = ["provider_id":"\(objUserRecs.providerId)",
                  "page":"\(nextPageStr)" as String,
                   "type":"4","perPage":kPageCount,"from":userInfo["Fromdate"]!,"to":userInfo["Todate"]!,"orderby":userInfo["asDes"]!,"sortby":userInfo["StatusforSort"]!]
        
        GetNewLeads()
        
        
        
    }

    func GetNewLeads(){
        
             // print(Param)
        
        url_handler.makeCall(myJobsUrl, param: Param) {
            (responseObject, error) -> () in
            self.jobsClosedTbl.hidden=false
            self.DismissProgress()
            self.jobsClosedTbl.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        if(self.nextPageStr==1){
                            self.jobsClosedArr.removeAllObjects()
                        }
                        if(responseObject?.objectForKey("response")?.objectForKey("jobs")!.count>0){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("jobs") as! NSArray
                           
                            for (_, element) in listArr.enumerate() {
                                
                                
                                let provider_lat = self.theme.CheckNullValue(element.objectForKey("location_lat"))!
                                let provider_lng = self.theme.CheckNullValue(element.objectForKey("location_lng"))!
                                
                                
                               
                                
                                let rec = MyOrderOpenRecord(order_id: self.theme.CheckNullValue(element.objectForKey("job_id"))!, post_on: self.theme.CheckNullValue(element.objectForKey("booking_time"))!, user_Img: self.theme.CheckNullValue(element.objectForKey("user_image"))!, user_name: self.theme.CheckNullValue(element.objectForKey("user_name"))!, user_catg: self.theme.CheckNullValue(element.objectForKey("category_name"))!, user_Loc:  self.theme.CheckNullValue(self.theme.getAddressForLatLng(provider_lat, longitude: provider_lng, status: "short"))!,order_sta: self.theme.CheckNullValue(element.objectForKey("job_status"))!,rate_hour: self.theme.CheckNullValue(element.objectForKey("hourly_rate"))!, cat_type: self.theme.CheckNullValue(element.objectForKey("type"))!)

                                [self.jobsClosedArr .addObject(rec)]
                            }
                           
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:Language_handler.VJLocalizedString("no_leads", comment: nil), duration: 3, position: HRToastPositionDefault, title:appNameJJ)
                            }
                        }
                         self.jobsClosedTbl.reloadData()
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if(jobsClosedArr.count>0){
            noDataView.hidden=true
            return 1
        }else{
            
            if(noDataView==nil){
                let subviewArray = NSBundle.mainBundle().loadNibNamed("NoDataView", owner: self, options: nil)
                noDataView = subviewArray[0] as! NoDataView
                noDataView.msgLbl.text = Language_handler.VJLocalizedString("You_Don’t_Have_Any_Completed_Jobs", comment: nil)
                
                noDataView.frame=self.view.frame
            }
            
            noDataView.hidden=false
            tableView.backgroundView = noDataView
        }
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobsClosedArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NewLeadsIdentifier", forIndexPath: indexPath) as! NewLeadsTableViewCell
        let objRec:MyOrderOpenRecord=self.jobsClosedArr.objectAtIndex(indexPath.row) as! MyOrderOpenRecord
        cell.loadMyOrderNewLeadTableCell(objRec)
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //MyOrderDetailOpenVCSID
        if(!loading){
            if(indexPath.row < jobsClosedArr.count){
                let objRec=jobsClosedArr.objectAtIndex(indexPath.row)
                NSNotificationCenter.defaultCenter().postNotificationName(kMyLeadsNotif, object: objRec)
            }
        }
    }
    
    
    
    
    ///////////////////// Infinite Scroll
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) == Int(scrollView.contentSize.height + scrollView.contentInset.bottom)) {
            if (maximumOffset - currentOffset) <= 52 {
                refreshNewLeadsandLoad()
            }
        }
        
    }
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    func refreshNewLeads(){
        
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        jobsClosedTbl.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
             let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
            self.Param = ["provider_id":"\(objUserRecs.providerId)",
                "type":"4",
                "page":"\(self.nextPageStr)" as String,
                "perPage":kPageCount]

            self.GetNewLeads()
            
            }, loadingView: loadingView)
        jobsClosedTbl.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        jobsClosedTbl.dg_setPullToRefreshBackgroundColor(jobsClosedTbl.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
             let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            Param = ["provider_id":"\(objUserRecs.providerId)",
                     "type":"4",
                     "page":"\(nextPageStr)" as String,
                     "perPage":kPageCount]

            GetNewLeads()
        }
    }
    deinit {
        jobsClosedTbl.dg_removePullToRefresh()
         NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
