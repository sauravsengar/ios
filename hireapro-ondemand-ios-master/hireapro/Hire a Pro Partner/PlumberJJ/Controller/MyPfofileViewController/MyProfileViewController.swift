//
//  MyProfileViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/6/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
let IMAGE_HEIGHT = 273
class MyProfileViewController: RootBaseViewController,SMSegmentViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    //   var theme:Theme=Theme()
    @IBOutlet var my_profile: UILabel!
    var availabilityDict : NSMutableArray = NSMutableArray()
    var AvailableDaysArray :NSMutableArray = NSMutableArray()
 var weekFullDay = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    var AvailableAllDaysArray :NSMutableArray = NSMutableArray()
    var nextPageStr:NSInteger!
    var noDataView:UIView!
    var segmentView: SMSegmentView!
    
    var getCatagoryArr : NSArray = NSArray()
    @IBOutlet var profileseegment: CustomSegmentControl!
    var badge:NSArray = NSArray()

    @IBOutlet var badgecollection: UICollectionView!
    @IBOutlet var badgeImg: UIImageView!
    @IBOutlet var availabletable: UITableView!
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topView: SetColorView!
    @IBOutlet weak var segmentContainerView: UIView!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var totalContainer: UIView!
    @IBOutlet weak var profileScrollView: UIScrollView!
    @IBOutlet weak var reviewsTblView: UITableView!
    @IBOutlet weak var userInfoTopView: UIView!
    @IBOutlet weak var myProfileTblView: UITableView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var userCatLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    var ProfileContentArray:NSMutableArray = [];
    var reviewsArray:NSMutableArray = [];
    var catagoryarrcount : NSMutableArray = [];
    
    @IBOutlet weak var profileTopView: UIView!
    private var loading = false {
        didSet {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editProfileBtn.setTitle(Language_handler.VJLocalizedString("edit_profile", comment: nil), forState: UIControlState.Normal)
        my_profile.text = Language_handler.VJLocalizedString("my_profile", comment: nil)

        profileseegment.setTitle(theme.setLang("Detail"), forSegmentAtIndex: 0)
        profileseegment.setTitle(theme.setLang("Availability"), forSegmentAtIndex: 1)
        profileseegment.selectedSegmentIndex=0
        profileseegment.tintColor=theme.additionalThemeColour()
        profileseegment.hidden = true
        
        profileseegment.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 14.0)!, NSForegroundColorAttributeName: PlumberThemeColor], forState: .Normal)

        

        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.hidden=false;
            barButton.hidden=true
        }else{
            
        }
        myProfileTblView.registerNib(UINib(nibName: "ProfileDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailTableIdentifier")
        myProfileTblView.estimatedRowHeight = 58
        myProfileTblView.rowHeight = UITableViewAutomaticDimension
        
        availabletable.registerNib(UINib(nibName:"AvailableDaysTableCell", bundle: nil), forCellReuseIdentifier: "availabledayscell")
        availabletable.estimatedRowHeight = 20
        availabletable.rowHeight = UITableViewAutomaticDimension
        
        
        barButton.addTarget(self, action: #selector(MyProfileViewController.openmenu), forControlEvents: .TouchUpInside)
        reviewsTblView.registerNib(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTblIdentifier")
        reviewsTblView.estimatedRowHeight = 120
        reviewsTblView.rowHeight = UITableViewAutomaticDimension
        myProfileTblView.tableFooterView = UIView()
        availabletable.tableFooterView = UIView()
        reviewsTblView.tableFooterView = UIView()

        badgecollection.registerNib(UINib.init(nibName:"BadgeCollectionViewCell" , bundle: nil), forCellWithReuseIdentifier: "cell")
        blurBannerImg()
        loadSegmentControl()
        
    }
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    
    
    
    @IBAction func segmentAction(sender: AnyObject) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;

        if(segmentIndex == 0)
        {
            self.availabletable.hidden=true
            self.myProfileTblView.hidden=false
        }
        if(segmentIndex == 1)
        {
            //self.bannerImg.frame = CGRectMake(self.bannerImg.frame.origin.x,self.userInfoTopView.frame.origin.y, self.bannerImg.frame.size.width, self.userInfoTopView.frame.size.height + 5)

            self.availabletable.hidden=false
            self.myProfileTblView.hidden=true
            
        }

    }
    override func viewWillAppear(animated: Bool) {
        reviewsTblView.hidden=true
        nextPageStr=1
        if(ProfileContentArray.count>0){
            ProfileContentArray.removeAllObjects()
            self.availabilityDict.removeAllObjects()
            self.AvailableDaysArray.removeAllObjects()
            self.reviewsArray.removeAllObjects()
            self.catagoryarrcount.removeAllObjects()
        }
        
        refreshNewLeads()
        showProgress()
        
        GetReviews()
        GetUserDetails()

    }
    func loadProfileTblView(){
        
        
        
        if badge.count == 0
        {
            self.profileTopView.frame = CGRectMake(self.profileTopView.frame.origin.x, self.profileTopView.frame.origin.y, self.profileTopView.frame.size.width, self.profileTopView.frame.size.height - self.badgecollection.frame.size.height-50)
             self.userInfoTopView.frame = CGRectMake(self.userInfoTopView.frame.origin.x, self.userInfoTopView.frame.origin.y, self.userInfoTopView.frame.size.width, self.userInfoTopView.frame.size.height - self.badgecollection.frame.size.height-50)
             self.bannerImg.frame = CGRectMake(self.bannerImg.frame.origin.x, self.bannerImg.frame.origin.y, self.bannerImg.frame.size.width, self.bannerImg.frame.size.height - self.badgecollection.frame.size.height-50)
            self.ratingView.frame = CGRectMake(self.ratingView.frame.origin.x, self.ratingView.frame.origin.y-50, self.ratingView.frame.size.width, self.ratingView.frame.size.height)
            self.profileseegment.frame = CGRectMake(self.profileseegment.frame.origin.x, self.profileTopView.frame.origin.y+self.profileTopView.frame.size.height+100, self.profileTopView.frame.size.width,  self.profileseegment.frame.size.height)
            self.myProfileTblView.frame = CGRectMake(self.myProfileTblView.frame.origin.x, self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, self.myProfileTblView.frame.size.width,  self.myProfileTblView.frame.size.height)
            self.availabletable.frame = CGRectMake(self.availabletable.frame.origin.x, self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, self.availabletable.frame.size.width,  self.availabletable.frame.size.height)
            
        }
        else{
            
        }
        
        self.profileScrollView.frame=CGRectMake(self.profileScrollView.frame.origin.x, self.profileScrollView.frame.origin.y, self.profileScrollView.frame.size.width, self.profileScrollView.frame.size.height);
        
        self.myProfileTblView.frame = CGRectMake(self.myProfileTblView.frame.origin.x, self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, self.myProfileTblView.frame.size.width, self.myProfileTblView.frame.size.height+self.myProfileTblView.contentSize.height+CGFloat(getCatagoryArr.count*5))
        self.availabletable.frame = CGRectMake(self.availabletable.frame.origin.x, self.profileseegment.frame.origin.y+self.profileseegment.frame.size.height, self.availabletable.frame.size.width, CGFloat(AvailableDaysArray.count*56))
        
        
        
        self.profileScrollView.contentSize=CGSizeMake( self.profileScrollView.frame.width,self.myProfileTblView.frame.origin.y + self.myProfileTblView.frame.size.height)
        
        self.myProfileTblView.hidden = false
        self.availabletable.hidden = true
        print(self.profileScrollView.frame);
        print(self.myProfileTblView.frame);
        print(self.availabletable.frame);
        print(self.profileScrollView.contentSize);
        
        
    }
    
    
    
    func blurBannerImg(){
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            bannerImg.backgroundColor = UIColor.clearColor()
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bannerImg.bounds
            blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            
            bannerImg.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        }
        else {
            bannerImg.backgroundColor = UIColor.blackColor()
        }
        editProfileBtn.backgroundColor=PlumberThemeColor
        userImg.layer.cornerRadius=userImg.frame.size.width/2
        //userImg.layer.borderWidth=0.75
        //userImg.layer.borderColor=PlumberThemeColor.CGColor
        userImg.layer.masksToBounds=true
    }
    func loadSegmentControl(){
        self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: segmentContainerView.frame.size.width, height: segmentContainerView.frame.size.height), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5, segmentProperties: [keySegmentTitleFont: UIFont.boldSystemFontOfSize(15.0), keySegmentOnSelectionColour: PlumberThemeColor,keySegmentOffSelectionTextColour:UIColor.darkGrayColor(), keySegmentOffSelectionColour: UIColor.whiteColor(), keyContentVerticalMargin: Float(10.0)])
        
        self.segmentView.delegate = self
        
        self.segmentView.layer.cornerRadius = 0.0
        self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).CGColor
        self.segmentView.layer.borderWidth = 1.0
        
        // Add segments
        self.segmentView.addSegmentWithTitle(Language_handler.VJLocalizedString("profiles", comment: nil), onSelectionImage: UIImage(named: "MyProfileSelect"), offSelectionImage: UIImage(named: "MyProfileUnSelect"))
        self.segmentView.addSegmentWithTitle(Language_handler.VJLocalizedString("reviews", comment: nil), onSelectionImage: UIImage(named: "ReviewsSelect"), offSelectionImage: UIImage(named: "ReviewsUnSelect"))
        
        
        // Set segment with index 0 as selected by default
        //segmentView.selectSegmentAtIndex(0)
        self.segmentView.selectSegmentAtIndex(0)
        segmentContainerView.addSubview(self.segmentView)
    }
    // SMSegment Delegate
    func segmentView(segmentView: SMSegmentView, didSelectSegmentAtIndex index: Int) {
        switch (index){
        case 0:
            swapViews(false)
            break
        case 1:
            swapViews(true)
            break
            
        default:
            break
        }
    }
    func swapViews(isReviews:Bool){
        
        let transition = CATransition()
        // transition.startProgress = 0;
        //transition.endProgress = 5.0;
        transition.type = "fade";
        //transition.subtype = "fromRight";
        transition.duration = 0.4;
        transition.repeatCount = 1;
        self.totalContainer.layer.addAnimation(transition, forKey: " ")
        if(isReviews==true){
            profileView.hidden=true
            reviewsView.hidden=false
            
            if self.reviewsArray.count == 0
            {
                
                self.view.makeToast(message:"You Have Not Received Any Reviews Yet", duration: 2, position: HRToastPositionDefault, title: "\(appNameJJ)")
                
            }
            
        }else{
            profileView.hidden=false
            reviewsView.hidden=true
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(scrollView==profileScrollView){
            let offset: CGFloat = scrollView.contentOffset.y
            let percentage: CGFloat = (offset / CGFloat(IMAGE_HEIGHT))
            let value: CGFloat = CGFloat(IMAGE_HEIGHT) * percentage
           // bannerImg.frame = CGRectMake(0, value, bannerImg.bounds.size.width, CGFloat(IMAGE_HEIGHT) - value)
            let alphaValue: CGFloat = 1 - fabs(percentage)
            userInfoTopView.alpha = alphaValue * alphaValue * alphaValue
            
        }
    }
    @IBAction func didClickEditProfile(sender: AnyObject) {
//        let alert = UIAlertController(title: "Update Profile", message: "Please login to www.hireaproondemand.com from a browser to update Profile", preferredStyle: UIAlertControllerStyle.Alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//        self.presentViewController(alert, animated: true, completion: nil)
        
        let objEditProfVc = self.storyboard!.instantiateViewControllerWithIdentifier("EditProfileVCSID") as! EditProfileViewController
             objEditProfVc.availabilityStorage = self.AvailableAllDaysArray
        self.navigationController!.pushViewController(objEditProfVc, animated: true)
    }
    @IBAction func didClickBackBtn(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 20))
        
        if tableView .isEqual(availabletable)
        {
            
            let headerlable : UILabel = UILabel.init(frame:CGRectMake(headerView.frame.origin.x+10,10,headerView.frame.size.width-10,25))
            if (AvailableDaysArray.count > 0)
            {
                headerlable.text = Language_handler.VJLocalizedString("available_days", comment: nil)            }
            headerlable.textColor = UIColor(red: 255/255.0, green: 70/255.0, blue: 63/255.0, alpha: 1)
            headerlable.font = UIFont.init(name: "Roboto-Regular", size:14.0)
            
            headerView.addSubview(headerlable)
            
            return headerView
        }
        else
        {
            return headerView
        }
    }
    
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView==myProfileTblView){
            
            return ProfileContentArray.count
        }else if(tableView==reviewsTblView){
            return reviewsArray.count
        }
        else if (tableView ==  availabletable)
        {
            
            return AvailableDaysArray.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        let cell3:UITableViewCell
        
        if(tableView==myProfileTblView){
            
            
            
            
            let cell1:ProfileDetailTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProfileDetailTableIdentifier") as! ProfileDetailTableViewCell
            if ProfileContentArray.count > 0
            {
                cell1.loadProfileTableCell(ProfileContentArray .objectAtIndex(indexPath.row) as! ProfileContentRecord)
            }
            cell1.selectionStyle=UITableViewCellSelectionStyle.None
            cell3=cell1
            
        }
        else if (tableView == availabletable)
        {
            let avialCell:AvailableDaysTableCell = tableView.dequeueReusableCellWithIdentifier("availabledayscell") as! AvailableDaysTableCell
            
            if indexPath.row == 0
            {
                avialCell.Morning.hidden = false
                avialCell.afternoon.hidden = false
                avialCell.Evning.hidden = false
                avialCell.mrnbtn.hidden = true
                avialCell.afternbtn.hidden = true
                avialCell.evebtn.hidden = true
            }
            else
            {
                avialCell.Morning.hidden = true
                avialCell.afternoon.hidden = true
                avialCell.Evning.hidden = true
                avialCell.mrnbtn.hidden = false
                avialCell.afternbtn.hidden = false
                avialCell.evebtn.hidden = false
            }
            
            
            if AvailableDaysArray.count > 0
            {
                avialCell.loadProfileTableCell(self.AvailableDaysArray .objectAtIndex(indexPath.row) as! AvailableRecord)
                
            }
            avialCell.selectionStyle=UITableViewCellSelectionStyle.None
            
            cell3=avialCell
            
            
        }
        else{
            let cell:ReviewsTableViewCell = tableView.dequeueReusableCellWithIdentifier("ReviewsTblIdentifier") as! ReviewsTableViewCell
            
            if (reviewsArray.count > 0)
            {
                cell.loadReviewTableCell((reviewsArray .objectAtIndex(indexPath.row) as! ReviewRecords), currentView:MyProfileViewController() as UIViewController)
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyle.None
            cell3=cell
        }
        
        return cell3
    }
    
    //    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    //        if(tableView==myProfileTblView){
    //        let cellSize:CGRect = cell.frame;
    //            if(indexPath.row+1==ProfileContentArray.count){
    //                var frame: CGRect = self.myProfileTblView.frame;
    //                frame.size.height = cellSize.origin.y + cellSize.height+10;
    //                self.myProfileTblView.frame = frame;
    ////                 self.profileScrollView.contentSize=CGSizeMake(self.profileScrollView.frame.size.width, self.myProfileTblView.frame.origin.y+self.myProfileTblView.frame.size.height)
    //            }
    //        }
    //    }
    func GetUserDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(viewProfile, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        
                        
                        self.ProfileContentArray.removeAllObjects()
                        self.availabilityDict.removeAllObjects()
                        self.AvailableDaysArray.removeAllObjects()
                        self.catagoryarrcount.removeAllObjects()
                        
                        self.profileTopView.hidden=false
                        
                        self.userNameLbl.text=self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("provider_name"))
                        
                        let doubleval:Double = Double("\(self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("avg_review"))!)")!
                        let doubleStr = String(format: "%.1f", doubleval)
                        
                        self.ratingView.rating = Float(doubleStr)!
                        
                        let Dict : NSDictionary = (responseObject?.objectForKey("response"))! as! NSDictionary
                        
                        let badgeArray:NSArray=responseObject?.objectForKey("response")!.objectForKey("badges") as! NSArray
                        self.badge = badgeArray
                       
                        
                        self.userImg.sd_setImageWithURL(NSURL(string:(Dict.objectForKey("image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                        
                       
                        
                        
                      
                        
                        self.theme.saveUserImage(self.theme.CheckNullValue(Dict.objectForKey("image")!)!)
                        
                        
                       // self.theme.saveUserImage(self.theme.CheckNullValue(Dict.objectForKey("badges")!)!)
                        
                        
                        self.bannerImg.sd_setImageWithURL(NSURL(string:(responseObject?.objectForKey("response")?.objectForKey("image"))as! String), placeholderImage: UIImage(named: "PlaceHolderBig"))
                     
                        
                        if(responseObject?.objectForKey("response")?.objectForKey("details")!.count>0){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("details") as! NSArray
                            
                            self.availabilityDict = responseObject?.objectForKey("response")?.objectForKey("availability_days") as! NSMutableArray
                              var justI = 1
                                    self.AvailableAllDaysArray = NSMutableArray()
                            for (_, element) in listArr.enumerate() {
        if   self.theme.CheckNullValue(element.objectForKey("desc"))! == "" || self.theme.CheckNullValue(element.objectForKey("title")) == "Bio"
                                {
                                    
                                    print("remove bio field")
                                }
                                else{
                                    
                                    let result1 = self.theme.CheckNullValue(element.objectForKey("desc"))!.stringByReplacingOccurrencesOfString("\n", withString:",")
                                    
                                    let rec = ProfileContentRecord(userTitle: self.theme.CheckNullValue(element.objectForKey("title"))!, desc: result1)
                                    
                                    
                                    if self.theme.CheckNullValue(element.objectForKey("title"))! == "Category"
                                    {
                                        
                                        self.getCatagoryArr = result1.componentsSeparatedByString(",")
                                        
                                    }
                                    //
                                    //
                                    
                                    print("category array count \(self.getCatagoryArr.count)")
                                    self.ProfileContentArray .addObject(rec)
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                            let record  = AvailableRecord (dayrec: Language_handler.VJLocalizedString("days", comment: nil)
                                ,mornigrec:Language_handler.VJLocalizedString("morning", comment: nil)
                                ,Afterrec:Language_handler.VJLocalizedString("afternoon", comment: nil)
                                ,eveningrec:Language_handler.VJLocalizedString("evening", comment: nil))
                            self.AvailableDaysArray.addObject(record)
                            
                            
                            for (_, element) in self.availabilityDict.enumerate() {
                                let result1 = self.theme.CheckNullValue(element.objectForKey("day"))!
                                
                                let avaialbletime  : String = self.theme.CheckNullValue((element.objectForKey("hour"))!.objectForKey("morning"))!
                                let avaialbleAftertime  : String =   self.theme.CheckNullValue((element.objectForKey("hour"))!.objectForKey("afternoon"))!
                                let avaialbleevetime  : String = self.theme.CheckNullValue((element.objectForKey("hour"))!.objectForKey("evening"))!
                                let record  = AvailableRecord (dayrec: result1,mornigrec: avaialbletime ,Afterrec: avaialbleAftertime,eveningrec: avaialbleevetime)
                                self.AvailableDaysArray.addObject(record)
                                
                                
                                
                            }
                            
                            for(var i=0;i<=6;i += 1){
                                let availableObj = self.AvailableDaysArray.objectAtIndex(justI) as! AvailableRecord
                                if availableObj.AvailDays == self.weekFullDay[i]{
                                    let record  = AvailableRecord (dayrec: availableObj.AvailDays as String,mornigrec: availableObj.AvailMornigtime as String,Afterrec: availableObj.AvailAftertime as String,eveningrec: availableObj.Availeveningtime as String)
                                    self.AvailableAllDaysArray.addObject(record)
                                    if  justI < self.AvailableDaysArray.count-1{
                                        justI += 1
                                    }
                                }else{
                                    let record  = AvailableRecord (dayrec: self.weekFullDay[justI],mornigrec: "0" ,Afterrec: "0",eveningrec: "0")
                                    self.AvailableAllDaysArray.addObject(record)
                                }
                            }
                            

                            
                            
                            
                        }else{
                            //self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                        }
                        
                        
                        self.myProfileTblView.reloadData()
                        self.availabletable.reloadData()
                        
                        self.profileseegment.hidden = false

                        
                        self.loadProfileTblView()
                        self.badgecollection.userInteractionEnabled =  true
                        self.badgecollection.scrollEnabled = true
                        
                        self.badgecollection.reloadData()
                     //   let indexpath : NSIndexPath = NSIndexPath(forRow: 2, inSection: 0)
                        
                 //self.badgecollection.scrollToItemAtIndexPath(indexpath, atScrollPosition: ./CenteredHorizontally, animated: true)

                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    func GetReviews(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["user_id":"\(objUserRecs.providerId)",
                                 "role":"tasker",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]
        // print(Param)
        
        url_handler.makeCall(Get_ReviewsURl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.reviewsTblView.hidden=false
            self.reviewsTblView.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict:NSDictionary=responseObject!.objectForKey("data") as! NSDictionary
                    let status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status") as? NSString)!
                    
                    if(status == "1")
                    {
                        if(Dict.objectForKey("response")?.objectForKey("reviews")!.count>0){
                            let  listArr:NSArray=Dict.objectForKey("response")?.objectForKey("reviews") as! NSArray
                            if(self.nextPageStr==1){
                                self.reviewsArray.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerate() {
                                let rec = ReviewRecords(name: self.theme.CheckNullValue(element.objectForKey("user_name"))!, time: self.theme.CheckNullValue(element.objectForKey("date"))!, desc: self.theme.CheckNullValue(element.objectForKey("comments"))!, rate: self.theme.CheckNullValue(element.objectForKey("rating"))!, img: self.theme.CheckNullValue(element.objectForKey("user_image"))!,ratting:self.theme.CheckNullValue(element.objectForKey("image"))!,jobid :self.theme.CheckNullValue(element.objectForKey("booking_id"))!)
                                
                                [self.reviewsArray .addObject(rec)]
                            }
                            self.reviewsTblView.reloadData()
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:Language_handler.VJLocalizedString("no_reviews", comment: nil), duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                            }
                        }
                    }
                    else
                    {
                        //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return badge.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let Cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! BadgeCollectionViewCell
        
        var valueFromArray = badge[indexPath.row]
        var imageFromValue = valueFromArray.objectForKey("image")
        
        var nameArray = badge[indexPath.row]
        var nameFromValue = valueFromArray.objectForKey("name")
        
        Cell.nameBadge.setTitle(nameFromValue as! String, forState:.Normal)
        Cell.nameBadge.tag = indexPath.row
        Cell.nameBadge.addTarget(self, action: #selector(MyProfileViewController.ShowAlert(_:)), forControlEvents: .TouchUpInside)

        
        Cell.imgBadge.sd_setImageWithURL(NSURL.init(string: imageFromValue as! String), completed: theme.block)
        Cell.imgBadge.tag = indexPath.row
        
        Cell.imgBadge.userInteractionEnabled = true
        let tap :UITapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(MyProfileViewController.handleimgTap(_:)))
        Cell.imgBadge.addGestureRecognizer(tap)
        return Cell
        
    }
    
    func  handleimgTap(recognizer:UITapGestureRecognizer)
    {
        // NSString *uid = testArray[recognizer.view.tag];
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title=Language_handler.VJLocalizedString("Badge_Description", comment: nil)

        AlertView.message = badge.objectAtIndex(recognizer.view!.tag).objectForKey("name") as? String
        AlertView.addButtonWithTitle(Language_handler.VJLocalizedString("ok", comment: nil))
        AlertView.show()
        
        
    }
    

    func ShowAlert (sender:UIButton)  {
        
        let AlertView:UIAlertView=UIAlertView()
        AlertView.title=Language_handler.VJLocalizedString("Badge_Description", comment: nil)
        AlertView.message = badge.objectAtIndex(sender.tag).objectForKey("name") as! String
        AlertView.addButtonWithTitle(Language_handler.VJLocalizedString("ok", comment: nil))
        AlertView.show()
        
    }

     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    
        if self.badge.count > 3
        {
            return UIEdgeInsetsMake(0, 0, 0, 0)

            
        }else{
        var viewWidth = badgecollection.frame.size.width
        var totalCellWidth:CGFloat = 88 * CGFloat (badge.count)
        var totalSpacingWidth:CGFloat = 10 * (CGFloat(badge.count) - 1)
        var leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
        var rightInset = leftInset
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
    }
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    func refreshNewLeads(){
        
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        reviewsTblView.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
            self.GetReviews()
            
            }, loadingView: loadingView)
        reviewsTblView.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        reviewsTblView.dg_setPullToRefreshBackgroundColor(reviewsTblView.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
            GetReviews()
        }
    }
    func UITableView_Auto_Height()
    {
        if(self.myProfileTblView.contentSize.height > self.myProfileTblView.frame.height){
            
        }
    }
    deinit {
        // reviewsTblView.dg_removePullToRefresh()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
