//
//  FareSummaryViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/30/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class FareSummaryViewController: RootBaseViewController,UITableViewDataSource,UITableViewDelegate {
    var jobIDStr:NSString!
    var cashoption:NSString!
   // var theme:Theme=Theme()
    var fareSummaryArr:NSMutableArray = [];
    
    
    @IBOutlet var receive_paymentbtn: ButtonColorView!
    @IBOutlet var receivebtn: ButtonColorView!
    @IBOutlet var receivecash_btn: ButtonColorView!
      @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var fareTblView: UITableView!
    @IBOutlet weak var jobIdLbl: UILabel!
    @IBOutlet weak var fareScrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleHeader.text = Language_handler.VJLocalizedString("fare_summary", comment: nil)
        receive_paymentbtn.setTitle(Language_handler.VJLocalizedString("request_payment", comment: nil), forState: UIControlState.Normal)
        receivebtn.setTitle(Language_handler.VJLocalizedString("receive_cash", comment: nil), forState: UIControlState.Normal)
        
        receivecash_btn.setTitle(Language_handler.VJLocalizedString("request_payment", comment: nil), forState: UIControlState.Normal)

        fareTblView.registerNib(UINib(nibName: "FareDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "fareDetailCellIdentifier")
        fareTblView.estimatedRowHeight = 56
        fareTblView.rowHeight = UITableViewAutomaticDimension
        fareTblView.tableFooterView = UIView()
         GetFareDetails()
      jobIdLbl.text="\(Language_handler.VJLocalizedString("job_id", comment: nil)) : \(jobIDStr)"
        
        
        if cashoption == "0"
        {
            
            receive_paymentbtn.hidden = true
             receivebtn.hidden = true
           receivecash_btn.hidden = false

            
        }
        else{
            
            receive_paymentbtn.hidden = false
            receivebtn.hidden = false
            receivecash_btn.hidden = true

        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
       print("OK")
    }
    func GetFareDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(jobIDStr)"]
         print(Param)
        self.showProgress()
        url_handler.makeCall(PaymentCumMoreInfoUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    var needPaymentStr:NSString="0"
                    if(status == "1")
                    {
                       
                        
                        if(responseObject?.objectForKey("response")?.objectForKey("job")!.count>0){
                            
                       
                             let currencyStr=self.theme.getCurrencyCode(responseObject?.objectForKey("response")?.objectForKey("job")?.objectForKey("currency") as! String)
                            NSLog("get currency=%@",currencyStr )
                            needPaymentStr=self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("job")?.objectForKey("need_payment"))!
                            if(needPaymentStr=="1"){
                                self.bottomView.hidden=false
                            }else{
                                self.bottomView.hidden=true
                            }
                           
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("job")?.objectForKey("billing") as! NSArray
                            
                            for (_, element) in listArr.enumerate() {
                                let result1:JobDetailRecord=JobDetailRecord()
                               let tit = self.theme.CheckNullValue(element.objectForKey("title"))!
                                result1.jobTitle="\(tit)"
                                result1.jobStatus=self.theme.CheckNullValue(element.objectForKey("dt"))!
                                if result1.jobStatus == "0"
                                {
                               
                                 result1.jobDesc=self.theme.CheckNullValue(element.objectForKey("amount"))!
                                }
                                
                                else
                                {
                result1.jobDesc="\(currencyStr)\(self.theme.CheckNullValue(element.objectForKey("amount"))!)"
                                    
                                }
                                self.fareSummaryArr .addObject(result1)
                            }
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                        }
                        self.fareTblView.reloadData()
                        //This code will run in the main thread:
                        
                        var frame: CGRect = self.fareTblView.frame
                        frame.origin.y=self.jobIdLbl.frame.origin.y+self.jobIdLbl.frame.size.height+20
                        frame.size.height = self.fareTblView.contentSize.height+20;
                        self.fareTblView.frame = frame;
                       
                        if(needPaymentStr=="1"){
                            self.fareScrollView.contentSize=CGSizeMake(self.fareScrollView.frame.size.width, self.fareScrollView.frame.origin.y+self.fareTblView.frame.size.height+70)

                        }else{
                            self.fareScrollView.contentSize=CGSizeMake(self.fareScrollView.frame.size.width, self.fareScrollView.frame.origin.y+self.fareTblView.frame.size.height)

                        }
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fareSummaryArr.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        
        let cell:FareDetailTableViewCell = tableView.dequeueReusableCellWithIdentifier("fareDetailCellIdentifier") as! FareDetailTableViewCell
      cell.loadFareTableCell(fareSummaryArr.objectAtIndex(indexPath.row) as! JobDetailRecord)
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        
        
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func didClickReceiveCashBtn(sender: AnyObject) {
        let objFarevc = self.storyboard!.instantiateViewControllerWithIdentifier("OTPVCSID") as! PlumberOTPViewController
        objFarevc.jobIDStr=jobIDStr
        self.navigationController!.pushViewController(objFarevc, animated: true)
    }
    @IBAction func didClickReceivePaymentBtn(sender: AnyObject) {
        RequestPayment()
    }
    func RequestPayment(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(jobIDStr)"]
        print(Param)
        self.showProgress()
        url_handler.makeCall(RequestPaymentUrl, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()

           // self.DismissProgress()
            if(error != nil)
            {
                self.DismissProgress()
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {   
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    //responseObject?.objectForKey("status") as! NSString
                    if(status == "1")
                    {
                         let resp:NSString=responseObject?.objectForKey("response") as! NSString
                       self.view.makeToast(message:resp as String, duration: 20, position: HRToastPositionDefault, title:appNameJJ)
                    self.showProgress()
                        
                    }
                    else
                    {
                        self.DismissProgress()
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.DismissProgress()
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    override func viewDidDisappear(animated: Bool) {
        
        self.view.hideToast(toast: self.view)
    }
    /*
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
