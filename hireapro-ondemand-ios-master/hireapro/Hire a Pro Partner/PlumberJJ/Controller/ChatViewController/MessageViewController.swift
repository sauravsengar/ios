//
//  MessageViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 20/01/16.
//  Copyright © 2016 Casperon Tech. All rights reserved.
//

//
//
//+(id)sharedInstance;
//-(void)loadOldMessages;
//-(void)sendMessage:(Message *)message;
//-(void)news;
//-(void)dismiss;

import UIKit

class MessageViewController: RootBaseViewController,InputbarDelegate,MessageGatewayDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet var User_lbl: UILabel!
    @IBOutlet var typingLbl: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var inputbar: Inputbar!
    @IBOutlet weak var onlinelbl: SMIconLabel!
    @IBOutlet weak var userImg: UIButton!
    
    
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var userOffDataLbl: UILabel!
    var messgaeidarray : NSMutableArray = NSMutableArray()
    var textArray : NSMutableArray = NSMutableArray()
    var FromDetailArray: NSMutableArray = NSMutableArray()
    var userstatusArray : NSMutableArray = NSMutableArray()
    var getDateArray :NSMutableArray = NSMutableArray ()
    var tableArray:TableArray=TableArray()
    var gateway:MessageGateway=MessageGateway()
    var chat:Chat=Chat()
    
    var jobId:NSString!
    var senderId:NSString!
    var username: NSString!
    var Userimg : NSString!
    var Userid : NSString!
    var RequiredJobid : NSString!
    let App_Delegate=UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        
        //  SocketIOManager.sharedInstance.send("tasker", Userid:Userid as String, taskerid:providerid as String, taskid: RequiredJobid as String)
        
        
        tableView.tableFooterView = UIView()
        super.viewDidLoad()
        //  setUserStatus(false)
        setInputbar()
        self.setTableView()
        self.SetImageView()
        self.showProgress()
        loadInitialchatView()
        
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action:#selector(DismissKeyboard))
        tapgesture.delegate = self;
        
        view.addGestureRecognizer(tapgesture)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receiveChatNotification:", name: "ChatFromOthersNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receiveTypestaus:", name: kTypeStatus, object: nil)
        // Do any additional setup after loading the view.
    }
    
    func SetImageView()
    {
        userImgView.layer.cornerRadius = userImgView.frame.size.height / 2;
        userImgView.clipsToBounds=true
        userImgView.layer.masksToBounds = true;
        userImgView.layer.borderWidth = 0;
        userImgView.contentMode = UIViewContentMode.ScaleAspectFill
        userImgView.layer.borderWidth=2.0
        userImgView.layer.borderColor=UIColor.whiteColor().CGColor
        
        
    }
    
    
    
    func loadInitialchatView(){
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let providerid : NSString = objUserRecs.providerId
        
        
        let param=["user":self.theme.CheckNullValue(Userid)!,"tasker":providerid as String,"task":self.theme.CheckNullValue(RequiredJobid)! , "type":"tasker","read_status":"tasker"];
        
        
        url_handler.makeCall(Chat_Details, param: param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                // self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "oops!!")
                //self.setUserStatus(false)
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    
                    
                    let taskerDetails = Dict.objectForKey("user") as! NSDictionary
                    
                    self.userImgView.sd_setImageWithURL(NSURL(string:
                        self.theme.CheckNullValue(taskerDetails.objectForKey("avatar"))!), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                    
                    // self.userImgView.sd_setImageWithURL(NSURL(string:imageurl), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                    self.User_lbl.text = self.theme.CheckNullValue(taskerDetails.objectForKey("username"))!
                    
                    let MessageDetails : NSMutableArray = Dict.objectForKey("messages") as! NSMutableArray
                    
                    if MessageDetails.count > 0
                    {
                        
                        for data  in MessageDetails
                        {
                            self.textArray.addObject(self.theme.CheckNullValue (data.valueForKey("message"))!)
                            self.FromDetailArray.addObject(self.theme.CheckNullValue(data.valueForKey("from"))!)
                            self.userstatusArray .addObject(self.theme.CheckNullValue(data.valueForKey("user_status"))!)
                            self.messgaeidarray.addObject(self.theme.CheckNullValue(data.valueForKey("_id"))!)
                            self.getDateArray.addObject(self.theme.CheckNullValue(data.valueForKey("date"))!)
                            
                        }
                        
                        
                        
                        
                        var k : Int
                        for k=0 ; k<self.textArray.count; k += 1
                        {
                            let mesg : Message = Message()
                            mesg.text = self.textArray.objectAtIndex(k) as! String;
                            
                            let text = self.getDateArray.objectAtIndex(k)  as! String
                            let types: NSTextCheckingType = .Date
                            var getdate = NSDate()
                            let detector = try? NSDataDetector(types: types.rawValue)
                            let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
                            
                            for match in matches {
                                
                                getdate = (match.date!)
                            }
                            mesg.date = getdate
                            
                            
                            if self.FromDetailArray.objectAtIndex(k) as! String == providerid
                            {
                                mesg.sender = MessageSender.Myself;
                                
                                if (self.userstatusArray.objectAtIndex(k) as! String == "2")
                                {
                                    mesg.status = MessageStatus.Read
                                }
                                else if (self.userstatusArray.objectAtIndex(k) as! String == "1")
                                {
                                    mesg.status = MessageStatus.Received
                                }
                                
                                
                            }
                            else{
                                mesg.sender = MessageSender.Someone;
                            }
                            
                            
                            
                            self.tableArray.addObject(mesg)
                        }
                        
                        
                        
                        
                        self.tableView.reloadData()
                        self.tableViewScrollToBottomAnimated(true)
                        
                        if self.textArray.count == 1
                        {
                            SocketIOManager.sharedInstance.sendingSinglemessagStatus(self.RequiredJobid as String, taskerid:providerid as String, Userid: self.Userid as String ,usertype:"tasker" ,messagearray: [])
                            
                        }
                        else
                        {
                            SocketIOManager.sharedInstance.SendingMessagestatus("tasker", Userid: self.Userid as String, taskerid: providerid as String, taskid: self.RequiredJobid as String)
                            
                        }
                        
                    }
                    
                    
                    
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "oops!!")
                    //self.setUserStatus(false)
                }
            }
            
        }
        
    }
    func setUserStatus(isAlive:Bool){
        
        if(isAlive==true){
            onlinelbl.text="Online"
            onlinelbl.icon = UIImage(named: "OnlineImg")
            userOffDataLbl.hidden=true
            inputbar.userInteractionEnabled=true
        }else{
            onlinelbl.text="Offline"
            
            onlinelbl.icon = UIImage(named: "OfflineImg")
            
            onlinelbl.clipsToBounds=true
            userOffDataLbl.hidden=false
            inputbar.userInteractionEnabled=false
        }
        onlinelbl.iconPadding = 5
        onlinelbl.iconPosition = SMIconLabelPosition.Left
        
        //  userImgView.layer.borderWidth=1
        userImgView.layer.borderColor=UIColor.whiteColor().CGColor
        userImgView.layer.cornerRadius=userImg.frame.width/2
        userImgView.layer.masksToBounds=true
        
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChat", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showMessage(_:)), name: "ReceiveChat", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChat", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showPushMessage(_:)), name: "ReceivePushChat", object: nil)
        
        
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(showTypingStatus(_:)), name: "ReceiveTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveStopTypingMessage", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(stopTypingStatus(_:)), name: "ReceiveStopTypingMessage", object: nil)
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readmessagestatus", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageViewController.ReadmessageStatus(_:)), name:"readmessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "readSinglemessagestatus", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageViewController.ReadSinglemessageStatus(_:)), name:"readSinglemessagestatus", object: nil)
        
        
        
        
        let controller:MessageViewController=self
        self.view.keyboardTriggerOffset = inputbar.frame.size.height;
        setTest()
        
        view.addKeyboardPanningWithActionHandler { (keyboardFrameInView:CGRect, opening:Bool, closing:Bool) -> Void in
            
            var toolBarFrame:CGRect=self.inputbar.frame
            toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
            self.inputbar.frame = toolBarFrame;
            
            var tableViewFrame:CGRect = self.tableView.frame;
            tableViewFrame.size.height = toolBarFrame.origin.y - 80;
            self.tableView.frame = tableViewFrame;
            
            controller.tableViewScrollToBottomAnimated(true)
        }
    }
    
    func ReadSinglemessageStatus(notification: NSNotification){
        
        
        guard let url = notification.object else {
            return // or throw
        }
        
        let blob = url as! NSDictionary // or as! Sting or as! Int
        if(blob.count>0){
            
            NSLog("get singlemessage =%@", blob)
            let taskid : String = theme.CheckNullValue(blob.objectForKey("task"))!
            
            if taskid == RequiredJobid as String
            {
                
                let mesg : Message = tableArray.lastObject()
                mesg.status = MessageStatus.Read
                
                tableView.reloadData()
                
                // NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
                
                
                
            }
            
            
        }
        
    }
    
    func ReadmessageStatus(notification: NSNotification){
        
        guard let url = notification.object else {
            return // or throw
        }
        
        let blob = url as! NSDictionary // or as! Sting or as! Int
        if(blob.count>0){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let providerid : NSString = objUserRecs.providerId
            
            let taskid : String = theme.CheckNullValue(blob.objectForKey("task"))!
            
            if taskid == RequiredJobid as String
            {
                textArray  = NSMutableArray()
                FromDetailArray = NSMutableArray()
                userstatusArray  = NSMutableArray()
                messgaeidarray  = NSMutableArray()
                self.tableArray = TableArray()
                gateway = MessageGateway()
                self.getDateArray = NSMutableArray()
                
                if blob.count > 0
                {
                    
                    let MessageDetails : NSMutableArray = blob.objectForKey("messages") as! NSMutableArray
                    
                    for data  in MessageDetails
                    {
                        self.textArray.addObject(self.theme.CheckNullValue (data.valueForKey("message"))!)
                        self.FromDetailArray.addObject(self.theme.CheckNullValue(data.valueForKey("from"))!)
                        self.userstatusArray .addObject(self.theme.CheckNullValue(data.valueForKey("user_status"))!)
                        self.messgaeidarray.addObject(self.theme.CheckNullValue(data.valueForKey("_id"))!)
                        self.getDateArray.addObject(self.theme.CheckNullValue(data.valueForKey("date"))!)
                        
                    }
                    
                    
                    var k : Int
                    for k=0 ; k<self.textArray.count; k += 1
                    {
                        let mesg : Message = Message()
                        mesg.text = self.textArray.objectAtIndex(k) as! String;
                        
                        let text = self.getDateArray.objectAtIndex(k)  as! String
                        let types: NSTextCheckingType = .Date
                        var getdate = NSDate()
                        let detector = try? NSDataDetector(types: types.rawValue)
                        let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
                        
                        for match in matches {
                            getdate = (match.date!)
                        }
                        mesg.date = getdate
                        
                        
                        if self.FromDetailArray.objectAtIndex(k) as! String == providerid
                        {
                            mesg.sender = MessageSender.Myself;
                            
                            if (self.userstatusArray.objectAtIndex(k) as! String == "2")
                            {
                                mesg.status = MessageStatus.Read
                            }
                            else if (self.userstatusArray.objectAtIndex(k) as! String == "1")
                            {
                                mesg.status = MessageStatus.Received
                            }
                            
                        }
                        else{
                            mesg.sender = MessageSender.Someone;
                        }
                        
                        self.tableArray.addObject(mesg)
                    }
                    
                    
                    self.tableView.reloadData()
                    self.tableViewScrollToBottomAnimated(true)
                    
                }
            }
        }
        
    }
    
    
    func showTypingStatus(notification: NSNotification)
    {
        //        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        //
        //
        //        let taskid : NSString = userInfo["taskid"]!
        //
        //
        //        if (taskid == RequiredJobid as String)
        //        {
        
        typingLbl.text = Language_handler.VJLocalizedString("typing....", comment: nil)
        //}
        
        
    }
    
    func stopTypingStatus(notification: NSNotification) {
        //        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        //
        //
        //        let taskid : NSString = userInfo["taskid"]!
        //
        //
        //        if (taskid == RequiredJobid as String)
        //        {
        
        
        typingLbl.text = ""
        //}
        
        
    }
    
    
    func showPushMessage(notification: NSNotification){
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>

        inputbar.resignFirstResponder()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        let message_Array = notification.object as! NSArray
        
        
        
        
        let check_userid: NSString = userInfo["from"]!
        
        let Chatmessage:NSString! = userInfo["message"]
        let check_taskid = userInfo["task"]
        let messgeid = userInfo["msgid"]
        let getdate = userInfo["date"]
        let getUserid = userInfo["user_id"]
        
        
        let text = getdate
        let types: NSTextCheckingType = .Date
        var getdatefromweb = NSDate()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matchesInString(text!, options: .ReportCompletion, range: NSMakeRange(0, text!.characters.count))
        
        for match in matches {
            getdatefromweb = (match.date!)
        }

        let userstats = userInfo ["usersts"]!
        
        if messgaeidarray.containsObject(messgeid!)
        {
            
        }
        else
        {
            messgaeidarray.addObject(messgeid!)
            if check_taskid! == RequiredJobid && getUserid! == Userid
            {
                
                
                let mesg : Message = Message()

                if (check_userid == providerid)
                {
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Myself;
                    mesg.date = getdatefromweb

                    if (userstats  == "2")
                    {
                        mesg.status = MessageStatus.Read
                    }
                    else if (userstats  == "1")
                    {
                        mesg.status = MessageStatus.Received
                    }
                    
                    
                    
                }
                else{
                    
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        
                        
                        SocketIOManager.sharedInstance.sendingSinglemessagStatus(self.RequiredJobid as String, taskerid:providerid as String, Userid: self.Userid as String ,usertype:"tasker",messagearray: message_Array)
                        
                    }
                    
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Someone;
                    mesg.date = getdatefromweb
                    // mesg.date = NSDate()
                    
                    
                }
                self.tableArray.addObject(mesg)
                
                
                tableView.reloadData()
                self.tableViewScrollToBottomAnimated(true)
                
            }
            else
            {
                let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("message_from_user", comment: nil))
                alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                    
                    self.Userid = check_userid
                    self.RequiredJobid = check_taskid
                    self.Reload_Messageview()
                    
                })
                alertView.show()
                
            }
        }
        
        
    }
    
    func showMessage(notification: NSNotification)
    {
        
        inputbar.resignFirstResponder()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let message_Array = notification.object as! NSArray
        
        let check_userid: NSString = userInfo["from"]!
        
        let Chatmessage:NSString! = userInfo["message"]
        let check_taskid = userInfo["task"]
        let messgeid = userInfo["msgid"]
        let getdate = userInfo["date"]
        let getUserid = userInfo["user"]
        
        
        let text = getdate
        let types: NSTextCheckingType = .Date
        var getdatefromweb = NSDate()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matchesInString(text!, options: .ReportCompletion, range: NSMakeRange(0, text!.characters.count))
        
        for match in matches {
            getdatefromweb = (match.date!)
        }
        
        
        
        
        let userstats = userInfo ["usersts"]!
        
        if messgaeidarray.containsObject(messgeid!)
        {
            
        }
        else
        {
            messgaeidarray.addObject(messgeid!)
            if check_taskid! == RequiredJobid && getUserid! == Userid
            {
                
                
                let mesg : Message = Message()
                
                if (check_userid == providerid)
                {
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Myself;
                    mesg.date = getdatefromweb
                    // mesg.date = NSDate()
                    
                    
                    if (userstats  == "2")
                    {
                        mesg.status = MessageStatus.Read
                    }
                    else if (userstats  == "1")
                    {
                        mesg.status = MessageStatus.Received
                    }
                    
                    
                    
                }
                else{
                    
                    
                    mesg.text = Chatmessage as String;
                    mesg.sender = MessageSender.Someone;
                    mesg.date = getdatefromweb
                    // mesg.date = NSDate()
                    SocketIOManager.sharedInstance.sendingSinglemessagStatus(RequiredJobid as String, taskerid:providerid as String, Userid: Userid as String ,usertype:"tasker",messagearray: message_Array)
                    
                    
                }
                self.tableArray.addObject(mesg)
                
                
                tableView.reloadData()
                self.tableViewScrollToBottomAnimated(true)
                
            }
            else
            {
                let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("message_from_user", comment: nil))
                alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                    
                    
                    
                    
                    self.Userid = check_userid
                    self.RequiredJobid = check_taskid
                    self.Reload_Messageview()
                    
                })
                alertView.show()
                
            }
        }
        
        
    }
    
    func   Reload_Messageview() {
        
        tableView.tableFooterView = UIView()
        //  setUserStatus(false)
        self.setTableView()
        
        self.showProgress()
        textArray  = NSMutableArray()
        FromDetailArray = NSMutableArray()
        userstatusArray  = NSMutableArray()
        messgaeidarray  = NSMutableArray()
        self.tableArray = TableArray()
        gateway = MessageGateway()
        self.getDateArray = NSMutableArray()
        
        
        loadInitialchatView()
        setInputbar()
        self.SetImageView()
        
        
    }
    
    
    @IBAction func didClickOptions(sender: AnyObject) {
        if(sender.tag == 0)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    func setTest()
    {
        chat = Chat()
        chat.sender_name = "Player 1"
        chat.receiver_id = "12345"
        chat.sender_id = "54321"
        let texts: NSArray = []
        var last_message: Message? = nil
        for text in texts {
            let message: Message = Message()
            message.text = text as! String
            message.sender = .Someone
            message.status = .Received
            message.chat_id = chat.identifier()
            LocalStorage.sharedInstance().storeMessage(message)
            last_message = message
        }
        chat.numberOfUnreadMessages = texts.count
        if(last_message != nil)
        {
            chat.last_message = last_message!
        }
        
    }
    func receiveTypestaus(notification:NSNotification)
    {
        let str:NSString!=notification.object as! NSString
        if(str == "Type")
        {
            onlinelbl.hidden=true
            typingLbl.hidden=false
            // typingLbl.startAnimating()
        }
        else if(str == "StopType")
        {
            onlinelbl.hidden=false
            typingLbl.hidden=true
            // typingLbl.stopAnimating()
            
        }
    }
    func receiveChatNotification(notification:NSNotification)
    {
        let str:NSString!=notification.object as! NSString
        if(str != nil)
        {
            MessageFromIn(str as String)
        }
        //  setUserStatus(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func DismissKeyboard()
    {
        
        inputbar.resignFirstResponder()
        
        
        
    }
    
    func setInputbar()
    {
        self.inputbar.placeholder = nil;
        self.inputbar.delegate = self;
        self.inputbar.leftButtonImage = UIImage(named: "share")
        self.inputbar.rightButtonText = Language_handler.VJLocalizedString("Send", comment: nil);
        self.inputbar.rightButtonTextColor = UIColor(red: 0, green: 124/255.0, blue: 1, alpha: 1)
    }
    
    func setTableView()
    {
        self.tableArray = TableArray()
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0,view.frame.size.width, 10.0))
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.registerClass(MessageCell.classForCoder(), forCellReuseIdentifier: "MessageCell")
        
    }
    func setGateway()
    {
        gateway = MessageGateway()
        gateway.delegate = self;
        gateway.chat = self.chat;
        gateway.loadOldMessages()
        
        
    }
    
    //TableView Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.tableArray.numberOfSections()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableArray.numberOfMessagesInSection(section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let CellIdentifier: String = "MessageCell"
        var cell: MessageCell! = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! MessageCell
        if (cell == nil) {
            cell = MessageCell(style: .Default, reuseIdentifier: CellIdentifier)
        }
        if(self.tableArray.objectAtIndexPath(indexPath) != nil){
            cell.message = self.tableArray.objectAtIndexPath(indexPath)
        }
        return cell
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.tableArray.titleForSection(section)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let message: Message = self.tableArray.objectAtIndexPath(indexPath)
        return message.heigh
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame: CGRect = CGRectMake(0, 0, tableView.frame.size.width, 40)
        let view: UIView = UIView(frame: frame)
        view.backgroundColor = UIColor.clearColor()
        view.autoresizingMask = .FlexibleWidth
        let label: UILabel = UILabel()
        label.text = self.tableArray.titleForSection(section)
        label.textAlignment = .Center
        label.font = UIFont(name: "Helvetica", size: 20.0)
        label.sizeToFit()
        label.center = view.center
        label.font = UIFont(name: "Helvetica", size: 13.0)
        label.backgroundColor = UIColor(red: 207 / 255.0, green: 220 / 255.0, blue: 252.0 / 255.0, alpha: 1)
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        label.autoresizingMask = .None
        view.addSubview(label)
        return view
        
    }
    func tableViewScrollToBottomAnimated(animated: Bool) {
        let numberOfSections:NSInteger=tableArray.numberOfSections()
        let numberOfRows:NSInteger=tableArray.numberOfMessagesInSection(numberOfSections-1)
        if(numberOfRows != 0)
        {
            tableView.scrollToRowAtIndexPath(tableArray.indexPathForLastMessage(), atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
        }
    }
    
    func inputbarDidPressRightButton(inputbar: Inputbar!) {
        
        
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        
        SocketIOManager.sharedInstance.sendMessage(inputbar.text(), withNickname:Userid as String, Providerid:providerid as String , taskid:RequiredJobid as String)
        
        
    }
    func MessageFromIn(str: String) {
        
        let message: Message = Message()
        message.text = str
        message.date = NSDate()
        message.chat_id = "1"
        message.sender = .Someone
        //Store Message in memory
        self.tableArray.addObject(message)
        //Insert Message in UI
        do
        {
            
            try moveTable(message)
        }
        catch
        {
            print("there is an error")
        }
        //Send message to server
        // HI
        //[self.gateway sendMessage:message];
    }
    
    func moveTable (message:Message)throws
    {
        let indexPath: NSIndexPath =  tableArray.indexPathForMessage(message)
        
        self.tableView.beginUpdates()
        if self.tableArray.numberOfMessagesInSection(indexPath.section) == 1 {
            self.tableView.insertSections(NSIndexSet(index:indexPath.section), withRowAnimation: .None)
        }
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        self.tableView.endUpdates()
        self.tableView.scrollToRowAtIndexPath(self.tableArray.indexPathForLastMessage(), atScrollPosition: .Bottom, animated: true)
        
        
    }
    
    
    func inputbarDidPressLeftButton(inputbar: Inputbar!) {
        //        let alertView: UIAlertView = UIAlertView(title: "Left Button Pressed", message: "", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
        //        alertView.show()
        
    }
    func gatewayDidUpdateStatusForMessage(message: Message) {
        let indexPath: NSIndexPath = tableArray.indexPathForMessage(message)
        let cell: MessageCell = self.tableView.cellForRowAtIndexPath(indexPath) as! MessageCell
        cell.updateMessageStatus()
    }
    
    func gatewayDidReceiveMessages(array: [AnyObject]) {
        self.tableArray.addObjectsFromArray(array)
        self.tableView.reloadData()
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func inputbarDidBecomeFirstResponder(inputbar: Inputbar!) {
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        
        SocketIOManager.sharedInstance.sendStartTypingMessage(Userid as String, taskerid: providerid as String,taskid: self.theme.CheckNullValue(RequiredJobid)!)
        
        
        
        
    }
    
    func inputbarTextEndEditingChat(inputbar: Inputbar!) {
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        
        SocketIOManager.sharedInstance.sendStopTypingMessage(Userid as String, taskerid: providerid as String,taskid: self.theme.CheckNullValue(RequiredJobid)!)
        
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
