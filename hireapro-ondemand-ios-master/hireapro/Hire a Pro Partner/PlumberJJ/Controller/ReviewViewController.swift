//
//  ReviewViewController.swift
//  PlumberJJ
//
//  Created by Casperon on 08/02/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class ReviewViewController: RootBaseViewController,UITextViewDelegate{
    @IBOutlet weak var reviewsTblView: UITableView!
    var nextPageStr:NSInteger!
    @IBOutlet var reviews: UILabel!
    var reviewsArray:NSMutableArray = [];
    
    private var loading = false {
        didSet {
            
        }
    }
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reviews.text = Language_handler.VJLocalizedString("reviews", comment: nil)
 
        reviewsTblView.registerNib(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTblIdentifier")
        reviewsTblView.estimatedRowHeight = 110
        reviewsTblView.rowHeight = UITableViewAutomaticDimension
        
        reviewsTblView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        reviewsTblView.hidden=true
        nextPageStr=1
        
        
        self.reviewsArray.removeAllObjects()
        
        showProgress()
        refreshNewLeads()
        GetReviews()
    }
    
    
    func refreshNewLeads(){
        
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        reviewsTblView.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
            self.GetReviews()
            
            }, loadingView: loadingView)
        reviewsTblView.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        reviewsTblView.dg_setPullToRefreshBackgroundColor(reviewsTblView.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
            GetReviews()
        }
    }
    
    func GetReviews(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["user_id":"\(objUserRecs.providerId)",
                                 "role":"tasker",
                                 "page":"\(nextPageStr)" as String,
                                 "perPage":kPageCount]
        // print(Param)
        
        url_handler.makeCall(Get_ReviewsURl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            self.reviewsTblView.hidden=false
            self.reviewsTblView.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let Dict:NSDictionary=responseObject!.objectForKey("data") as! NSDictionary
                    let status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status") as? NSString)!
                    
                    if(status == "1")
                    {
                        if(Dict.objectForKey("response")?.objectForKey("reviews")!.count>0){
                            let  listArr:NSArray=Dict.objectForKey("response")?.objectForKey("reviews") as! NSArray
                            if(self.nextPageStr==1){
                                self.reviewsArray.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerate() {
                                let rec = ReviewRecords(name: self.theme.CheckNullValue(element.objectForKey("user_name"))!, time: self.theme.CheckNullValue(element.objectForKey("date"))!, desc: self.theme.CheckNullValue(element.objectForKey("comments"))!, rate: self.theme.CheckNullValue(element.objectForKey("rating"))!, img: self.theme.CheckNullValue(element.objectForKey("user_image"))!,ratting:self.theme.CheckNullValue(element.objectForKey("image"))!,jobid :self.theme.CheckNullValue(element.objectForKey("booking_id"))!)
                                
                                [self.reviewsArray .addObject(rec)]
                            }
                            self.reviewsTblView.reloadData()
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:Language_handler.VJLocalizedString("no_reviews", comment: nil), duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                            }
                        }
                    }
                    else
                    {
                        let response : String = self.theme.CheckNullValue(Dict.objectForKey("response"))!
                self.view.makeToast(message:"\(response)", duration: 5, position: HRToastPositionDefault, title: "\(appNameJJ)")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reviewsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
        let cell3:UITableViewCell
        
        let cell:ReviewsTableViewCell = tableView.dequeueReusableCellWithIdentifier("ReviewsTblIdentifier") as! ReviewsTableViewCell
        
        if (reviewsArray.count > 0)
        {
            
            cell.loadReviewTableCell((reviewsArray .objectAtIndex(indexPath.row) as! ReviewRecords), currentView:MyProfileViewController() as UIViewController)
            
            
        }
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        cell3=cell
        
        
        return cell3
    }
    
    
    
    
    
    
    @IBAction func menuBtnAct(sender: AnyObject) {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
