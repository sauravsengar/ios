//
//  StepsTreeViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/28/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class StepsTreeViewController: RootBaseViewController {

    @IBOutlet weak var stepScrollView: UIScrollView!
     @IBOutlet weak var stepTableView: UITableView!
    var stepsArr:NSMutableArray=[]
    var JobIdStr:NSString!
  //  var theme:Theme=Theme()
    @IBOutlet weak var titleLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         titleLbl.text="\(appNameShortJJ)"
        stepTableView.registerNib(UINib(nibName: "StepTableViewCell", bundle: nil), forCellReuseIdentifier: "stepTblIdentifier")
        stepTableView.estimatedRowHeight = 55
        stepTableView.rowHeight = UITableViewAutomaticDimension
        stepTableView.tableFooterView = UIView()
        
        GetStepByStepDetails()
        
       
        // Do any additional setup after loading the view.
    }
   
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(false)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return stepsArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->     UITableViewCell {
        
      
            let cell:StepTableViewCell = tableView.dequeueReusableCellWithIdentifier("stepTblIdentifier") as! StepTableViewCell
            cell.loadStepTableCell(stepsArr.objectAtIndex(indexPath.row) as! StepByStepInfoRecord)
            cell.selectionStyle=UITableViewCellSelectionStyle.None
        if(indexPath.row==0){
          cell.firstLbl.hidden=true
        }
        if(indexPath.row==stepsArr.count-1){
             cell.lastLbl.hidden=true
        }
       
        
        return cell
    }
    func GetStepByStepDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(JobIdStr)"]
        print(Param)
        self.showProgress()
        url_handler.makeCall(stepByStepInfoUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        
                        
                        if(responseObject?.objectForKey("response")?.objectForKey("timeline")!.count>0){
                            
                            
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("timeline") as! NSArray
                            
                            for (_, element) in listArr.enumerate() {
                                let result1:StepByStepInfoRecord=StepByStepInfoRecord()
                                result1.title=self.theme.CheckNullValue(element.objectForKey("title"))!
                                result1.date=self.theme.CheckNullValue(element.objectForKey("date"))!
                                result1.Time=self.theme.CheckNullValue(element.objectForKey("time"))!
                                result1.status=self.theme.CheckNullValue(element.objectForKey("check"))!
                                self.stepsArr .addObject(result1)
                            }
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:appNameJJ)
                        }
                        self.stepTableView.reloadData()
                        //This code will run in the main thread:
                        
                        var frame: CGRect = self.stepTableView.frame
                        frame.size.height = self.stepTableView.contentSize.height+100;
                        self.stepTableView.frame = frame;
                        self.stepScrollView.contentSize=CGSizeMake(self.stepScrollView.frame.size.width, self.stepTableView.frame.origin.y+self.stepTableView.frame.size.height)
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
