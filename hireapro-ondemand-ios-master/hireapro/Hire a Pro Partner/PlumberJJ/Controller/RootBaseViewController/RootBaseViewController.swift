//
//  RootBaseViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class RootBaseViewController: UIViewController {
    let progress = GradientCircularProgress()
    var spinnerViews : MMMaterialDesignSpinner?
    var spinnerView : MMMaterialDesignSpinner?
    var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate
    
    var reachability : Reachability?
    var window: UIWindow?
    var theme:Theme=Theme()
    var url_handler : URLhandler = URLhandler()
    var sidebarMenuOpen:Bool=Bool()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        reachability = Reachability.reachabilityForInternetConnection()
        reachability?.startNotifier()
        let remoteHostStatus:NetworkStatus=(reachability?.currentReachabilityStatus())!
        if(remoteHostStatus == NotReachable) {
            if((self.view.window) != nil){
                let image = UIImage(named: "NoNetworkConn")
                self.view.makeToast(message:kErrorMsg, duration: 3, position:HRToastActivityPositionDefault, title: "Oops !!!!", image: image!)
            }
        }
        else {
            if(theme.isUserLigin()){
                let URL_Handler:URLhandler=URLhandler()
                let objUserRecs:UserInfoRecord=theme.GetUserDetails()
                let param=["user_type":"provider","id":"\(objUserRecs.providerId)","mode":"available"]
                URL_Handler.makeCall(UserAvailableUrl, param: param, completionHandler: { (responseObject, error) -> () in
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    if(appDelegate.xmppStream.isDisconnected())
                    {
                        //appDelegate.setXmppConnect()
                        
                    }
                })
            }
            
        }
        
    }
    
    
    override func viewDidAppear(animated: Bool) {
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("applicationLanguageChangeNotification:"), name: Language_Notification as String, object: nil)
        
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "didClickLogOutPostNotif", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNoNetwork, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kPaymentPaidNotif, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceivePushChatToRootView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportChatRootView", object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReceiveSupportPushChatRootView", object:nil)


        
        NSNotificationCenter.defaultCenter().removeObserver(self, name:"logout", object: nil)
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kPushNotification, object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationForLogOut(_:)), name:"didClickLogOutPostNotif", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationNetworkNet(_:)), name:kNoNetwork, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationNetworkKKKK(_:)), name:kPaymentPaidNotif, object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedMessageNotification(_:)), name:"ReceiveChatToRootView", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodofReceivedSupportMessageNotification(_:)), name: "ReceiveSupportChatRootView", object: nil)
      
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodofReceivedPushSupportMessageNotification(_:)), name: "ReceiveSupportPushChatRootView", object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedPushMessageNotification(_:)), name:"ReceivePushChatToRootView", object: nil)
        
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedPushNotification(_:)), name:kPushNotification, object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.logoutmethod(_:)), name:"logout", object: nil)
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootBaseViewController.applicationLanguageChangeNotification(_:)), name: Language_Notification as String, object: nil)
        
        theme.saveLanguage("en")
        theme.SetLanguageToApp()
        
    }
    
    
    func methodofReceivedPushSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        if (check_userid != providerid)  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    
                }
            }
        }
    }

    
    
    func methodofReceivedSupportMessageNotification(notification: NSNotification)
    {
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
        let check_userid: NSString = userInfo["from"]!
        
        let admin_id=userInfo["admin"]
        let refer_id = userInfo["reference"]
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()

        let providerid : NSString = objUserRecs.providerId

        if (check_userid != providerid)  {
            if let activeController = navigationController?.visibleViewController {
                if activeController.isKindOfClass(supportMessageViewController){
                }else{
                    
                    let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("message_from_admin", comment: nil))
                    alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                        
                        Message_details.support_chatid = refer_id!
                        Message_details.admin_id = admin_id!
                        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                        self.navigationController?.pushViewController(secondViewController, animated: true)
                    })
                    alertView.show()
                }
            }
        }
    }
    func applicationLanguageChangeNotification(notification : NSNotification){
        
        
    }
    
    func logoutmethod(notify:NSNotification){
        dispatch_async(dispatch_get_main_queue()) { 
            
        print("Succesfully logged out")
        
        self.showProgress()
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        let providerid : NSString = objUserRecs.providerId
        
        
        
        let Param: Dictionary = ["provider_id":"\(providerid as String)","device_type":"ios"]
        self.url_handler.makeCall(Logout_url, param: Param) { (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil), duration: 4, position: HRToastPositionDefault, title: "")
                
            }
                
            else
            {
                print("the response is \(responseObject)")
                if(responseObject != nil)
                {
                    
                    
                    
                    let status:NSString?=self.theme.CheckNullValue(responseObject?.objectForKey("status")!)
                    
                    if(status! == "0")
                    {
                        
                        
                    }
                    else
                    {
                        
                        
                    }
                    
                    SocketIOManager.sharedInstance.RemoveAllListener()
                    SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
                    SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
                    let dic: NSDictionary = NSDictionary()
                    self.theme .saveUserDetail(dic)
                    
                    //                        let loginController = self.storyboard!.instantiateViewControllerWithIdentifier("InitialVCSID") as! LoginViewController
                    //                        let navig: UINavigationController = UINavigationController.init(rootViewController: loginController)
                    //                        self.Appdel.window?.rootViewController = navig
                    //
                    //                        loginController.navigationController?.setNavigationBarHidden(true, animated: true)
                    
                    let loginController = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewControllerWithIdentifier("InitialVCSID") as! InitailVC
                    //or the homeController
                    let navController = UINavigationController(rootViewController: loginController)
                    self.Appdel.window!.rootViewController! = navController
                    loginController.navigationController!.setNavigationBarHidden(true, animated: true)
                    
                    
                    
                    
                    
                }
                else
                {
                    self.view.makeToast(message: "Please try again", duration:3, position:HRToastPositionDefault, title: "\(appNameJJ)")
                }
                
            }
            
        }
        
        
        
        
        
        
        //    UserInfoRecord * objrec=(UserInfoRecord *)[objTheme GetUserDetails];
        //
        //    NSString *providerid = objrec.providerId;
        //
        //    NSDictionary *Param = [NSDictionary dictionaryWithObjectsAndKeys:providerid,@"provider_id",@"ios",@"device_type", nil];
        //
        //  //  let Param: Dictionary = ["user_id":"\(themes.getUserID())","device_type":"ios"]
        //   // - (void)makeCall:(NSString * _Nonnull)url param:(NSDictionary * _Nonnull)param completionHandler:(void (^ _Nonnull)(NSDictionary * _Nullable, NSError * _Nullable))completionHandler;
        //
        //   [url_handler makeCall:[objTheme Logout_url] param:Param completionHandler:^(NSDictionary *responseObject, NSError *error) {
        //
        //       if(error != nil)
        //
        //       {
        //           //[self.view makeToastWithMessage:@"Network Failure" duration:4 position:];
        //           [self.view makeToastWithMessage:@"Network Failure" duration:4 position:@"bottom"];
        //
        //       }
        //       else{
        //           if (responseObject != nil)
        //           {
        //
        //               NSString *status = [objTheme CheckNullValue:[responseObject valueForKey:@"status"]];
        //
        //               if ([status  isEqual: @"1"])
        //
        //               {
        //
        //                   UserInfoRecord * objrec=(UserInfoRecord *)[objTheme GetUserDetails];
        //
        //                   NSString *providerid = objrec.providerId;
        //
        //                   connectSocket = [SocketIOManager sharedInstance];
        //
        //
        //                   [connectSocket RemoveAllListener];
        //
        //                   [connectSocket LeaveRoom:providerid];
        //                   [connectSocket LeaveChatRoom:providerid];
        //
        //
        //
        //
        //
        //
        //                   [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"Availability"];
        //                   // SocketIOManager.sharedInstance.LeaveRoom;
        //                   NSDictionary * dict=[[NSDictionary alloc]init];
        //                   [objTheme saveUserDetail:dict];
        //                   AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //
        //                   LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"InitialVCSID"]; //or the homeController
        //                   UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:loginController];
        //                   appDelegate.window.rootViewController=navController;
        //                   [loginController.navigationController setNavigationBarHidden:YES animated:YES];
        //
        //
        //               }
        //               else
        //
        //               {
        //
        //               }
        //
        //           }
        //           else
        //
        //           {
        //               [self.view makeToastWithMessage:@"Please try again" duration:4 position:@"bottom"];
        //
        //           }
        //       }
        //   }];
        }
        
    }
    func methodOfReceivedPushNotification(notification: NSNotification)  {
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            
            let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
            
            let Action:NSString! = userInfo["action"]
            let message : NSString! = userInfo["action"]
            let orderid : NSString! = userInfo ["key"]
            let pricestr :NSString! = userInfo ["price"]
            let currencystr : NSString! = userInfo["currency"]
            
            if(Action=="job_request"){
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKindOfClass(MyLeadsViewController){
                        
                        
                        let jobId:NSString!=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        let jobId:NSString!=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                        
                    }
                    
                }
            }
                
            else if(Action=="job_cancelled"){
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKindOfClass(MyLeadsViewController){
                        
                        
                        let jobId:NSString!=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        let jobId:NSString!=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                        
                    }
                    
                }
                
                
            }
                
            else if(Action=="receive_cash")
            {
                
                let jobId:NSString!=orderid
                let price:NSString! = pricestr
                let currency:NSString!=currencystr
                let priceStr="\(currency) \(price)"
                let objReceiveCashvc = self.storyboard!.instantiateViewControllerWithIdentifier("ReceiveCashVCSID") as! ReceiveCashViewController
                objReceiveCashvc.priceString=priceStr
                objReceiveCashvc.jobIDStr=jobId
                self.navigationController!.pushViewController(objReceiveCashvc, animated: true)
            }
                
            else if(Action=="payment_paid")
            {
                
                let jobId:NSString!=orderid
                let objRatingsvc = self.storyboard!.instantiateViewControllerWithIdentifier("RatingsVCSID") as! RatingsViewController
                //                        objReceiveCashvc.priceString=priceStr
                objRatingsvc.jobIDStr=jobId
                self.navigationController!.pushViewController(objRatingsvc, animated: true)
                
                
                
            }
            
        }
    }
    
    
    func methodOfReceivedPushMessageNotification(notification: NSNotification){

        
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
            // or as! Sting or as! Int
            
            let check_userid: NSString = userInfo["from"]!
            let Chatmessage :NSString! = userInfo["message"]
            let taskid = userInfo["task"]
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            
            
            if (check_userid == providerid)
            {
                
            }
            else
            {
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKindOfClass(MessageViewController){
                        
                        
                    }else{
                        

                        let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatVCSID") as! MessageViewController
                        
                        objChatVc.Userid = check_userid
                        objChatVc.RequiredJobid = taskid
                        self.navigationController!.pushViewController(objChatVc, animated: true)
                        
                        
                        
                    }
                    
                }
            }
            
            
            
            
        }
        
    }
    
    func methodOfReceivedMessageNotification(notification: NSNotification){
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>
            // or as! Sting or as! Int
            
            
            
            let check_userid: NSString = userInfo["from"]!
            let Chatmessage :NSString! = userInfo["message"]
            let taskid = userInfo["task"]
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            
            
            if (check_userid == providerid)
            {
                
            }
            else
            {
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKindOfClass(MessageViewController){
                        
                        
                        
                    }else{
                      let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("message_from_user", comment: nil))
                        alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                            
                            let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatVCSID") as! MessageViewController
                            
                            objChatVc.Userid = check_userid
                            objChatVc.RequiredJobid = taskid
                            self.navigationController!.pushViewController(objChatVc, animated: true)
                            
                        })
                        alertView.show()
                        
                    }
                    
                }
            }
            
            
            
            
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "didClickLogOutPostNotif", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNoNetwork, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kPaymentPaidNotif, object: nil)
        
        NSNotificationCenter.defaultCenter().removeObserver(self);
        
        
    }
    
    
    func methodOfReceivedNotificationForLogOut(notification: NSNotification){
        self.progress.dismiss()
        let objDict:NSDictionary=NSDictionary()
        theme.saveUserDetail(objDict)
        let objInitialVc = self.storyboard!.instantiateViewControllerWithIdentifier("InitialVCSID") as! InitailVC
        self.navigationController!.pushViewController(objInitialVc, animated: true)
        
    }
    func showProgress()
    {
        if spinnerView == nil {
            spinnerView = MMMaterialDesignSpinner(frame: CGRectZero)
            
        }
        
        
        
        spinnerViews = spinnerView
        self.spinnerViews!.bounds = CGRectMake(0, 0, 75, 75)
        self.spinnerViews!.tintColor = PlumberThemeColor
        
        if((self is JobsClosedViewController)||(self is MyOrderViewController)||(self is JobsCancelledViewController)||(self is MyJobsViewController)||(self is MyLeadsViewController)||(self is MissedLeadsViewController)||(self is BarChartViewController)||(self is PieChartViewController)||(self is NewLeadsViewController)){
            self.spinnerViews!.center = CGPointMake(CGRectGetMidX(self.view.bounds),CGRectGetMidY(self.view.bounds)-107)
        }else{
            self.spinnerViews!.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds))
        }
        
        
        
        print("display spinner frame=\(self.spinnerView?.frame)" )
        self.spinnerViews!.translatesAutoresizingMaskIntoConstraints = false
        self.view!.addSubview(self.spinnerViews!)
        
        self.spinnerView?.startAnimating();
        
    }
    func DismissProgress()
    {
        self.spinnerView?.stopAnimating();
        
        
    }
    func methodOfReceivedNotificationNetworkNet(notification: NSNotification){
        
        self.progress.dismiss()
        self.progress.dismiss()
        self.progress.dismiss()
    }
    func methodOfReceivedNotificationNetworkKKKK(notification: NSNotification){
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            guard let url = notification.object else {
                return // or throw
            }
            
            let blob = url as! NSDictionary // or as! Sting or as! Int
            if(blob.count>0){
                
                var dictPartner:NSMutableDictionary = NSMutableDictionary()
                dictPartner = blob.objectForKey("message") as! NSMutableDictionary
                
                let Action:NSString?=dictPartner.objectForKey("action") as? NSString
                
                if(Action=="job_request"){
                    if let activeController = navigationController?.visibleViewController {
                        if activeController.isKindOfClass(MyLeadsViewController){
                            
                            let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                            alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
                                let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                            })
                            alertView.show()
                            
                            
                        }else if activeController.isKindOfClass(LocationViewController){
                            let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                            alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                
                                self.navigationController!.popViewControllerAnimated(true)
                            })
                            alertView.show()
                            
                        }else{
                            let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                            alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                
                                let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
                                let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                            })
                            alertView.show()
                            
                        }
                        
                    }
                }
                else if(Action=="admin_notification")
                {
                    let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                    alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                        
                    })
                    alertView.show()
                    
                }
                    

                else if(Action=="job_cancelled"){
                   let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("cancel_by_user", comment: nil))
                    alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                        
                        
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKindOfClass(MyOrderOpenDetailViewController){
                                
                                NSNotificationCenter.defaultCenter().postNotificationName(kJobCancelNotif, object: nil)
                            }else if activeController.isKindOfClass(LocationViewController){
                                let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                                alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                    
                                    self.navigationController!.popViewControllerAnimated(true)
                                })
                                alertView.show()
                                
                            }else{
                                let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
                                let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                            }
                        }
                    })
                    alertView.show()
                }
                else if(Action=="receive_cash")
                {
                    
                    let jobId:NSString!=dictPartner.objectForKey("key1") as! NSString
                    let price:NSString!=dictPartner.objectForKey("key3") as! NSString
                    let currency:NSString!=self.theme.getCurrencyCode(dictPartner.objectForKey("key4") as! String)
                    let priceStr="\(currency) \(price)"
                    let objReceiveCashvc = self.storyboard!.instantiateViewControllerWithIdentifier("ReceiveCashVCSID") as! ReceiveCashViewController
                    objReceiveCashvc.priceString=priceStr
                    objReceiveCashvc.jobIDStr=jobId
                    self.navigationController!.pushViewController(objReceiveCashvc, animated: true)
                }
                else if(Action=="payment_paid")
                {
                    let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                    alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                        
                        
                        if dictPartner.objectForKey("message") as! String == "Billing amount Partially Paid"
                        {
                            
                                let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
                                let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                            
                        }
                        else
                        {
                        let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
                        let objRatingsvc = self.storyboard!.instantiateViewControllerWithIdentifier("RatingsVCSID") as! RatingsViewController
                        //                        objReceiveCashvc.priceString=priceStr
                        objRatingsvc.jobIDStr=jobId
                        self.navigationController!.pushViewController(objRatingsvc, animated: true)
                        }
                    })
                    
                    alertView.show()
                }
                else if(Action=="job_assigned"){
                   let alertView = UNAlertView(title:appNameJJ, message: Language_handler.VJLocalizedString("assigned_to_job", comment: nil))
                    alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                        
                    })
                    alertView.addButton("View", action: {
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKindOfClass(MyOrderOpenDetailViewController){
                                
                                NSNotificationCenter.defaultCenter().postNotificationName(kJobCancelNotif, object: nil)
                            }else if activeController.isKindOfClass(LocationViewController){
                                let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
                                alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil), action: {
                                    
                                    self.navigationController!.popViewControllerAnimated(true)
                                })
                                alertView.show()
                                
                            }else{
                                let jobId:NSString!=dictPartner.objectForKey("key1") as! NSString
                                let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(objMyOrderVc, animated: true)
                            }
                        }
                    })
                    alertView.show()
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension UITextField {
    func isMandatory(){
        let label = UILabel()
        label.frame = CGRectMake(0, 0, 10,self.frame.height)
        label.text = "*"
        label.textColor = UIColor.redColor()
        self.rightView = label
        self.rightViewMode = UITextFieldViewMode .Always
    }
}

