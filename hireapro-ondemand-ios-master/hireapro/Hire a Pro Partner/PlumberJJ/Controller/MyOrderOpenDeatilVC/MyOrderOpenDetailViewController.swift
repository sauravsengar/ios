//
//  MyOrderOpenDetailViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/29/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import MessageUI

protocol MyOrderOpenDetailViewControllerDelegate {
    
    func  passRequiredParametres(fromdate:NSString,todate: NSString,isAscendorDescend: Int,isSortby: NSString)
    

}

class MyOrderOpenDetailViewController: RootBaseViewController,GMSMapViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate, MyPopupViewControllerDelegate,UITextFieldDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,MiscellaneousVCDelegate {
     var delegate:MyOrderOpenDetailViewControllerDelegate?
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?

    @IBOutlet weak var workFlowBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var seperatorLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet var service_tax: UILabel!
    @IBOutlet weak var detailScrollView: UIScrollView!
    @IBOutlet weak var hourlyratLbl: UILabel!
     var tField: UITextField!
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var jobCancelLbl: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var viewInMapsView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var openInMapsBtn: UIButton!
    var ReasonDetailArray:NSMutableArray=NSMutableArray()
    var  ReasonidArray:NSMutableArray=NSMutableArray()
   
    var fullAddress : String = String()
    var CancelReasonArr:NSMutableArray=[]
    var CancelTitleArr:NSMutableArray=[]
    var objDetailRec:JobDetailRecord=JobDetailRecord()
    var myPopupViewController:MyPopupViewController=MyPopupViewController()
    var myMaterialView:MiscellaneousVC=MiscellaneousVC()
    var  ChoosedReasonid:NSString=NSString()


    // var theme:Theme=Theme()
    var jobID:NSString!

    var Getorderstatus : NSString!
    var reasonIdStr:NSString!
  
       var LocationTimer:NSTimer=NSTimer()
    @IBOutlet weak var singleBtn: UIButton!
    var actionButton: ActionButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    
    @IBOutlet weak var jobIdLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openInMapsBtn.setTitle(Language_handler.VJLocalizedString("open_in_maps", comment: nil), forState: UIControlState.Normal)
       
            

        // For use in foreground
        callBtn.layer.cornerRadius=callBtn.frame.size.width/2
        callBtn.layer.borderWidth=1
        callBtn.layer.borderColor=PlumberGreenColor.CGColor
        callBtn.layer.masksToBounds=true
      
        bottomView.hidden=true
        detailScrollView.hidden=true
        viewInMapsView.hidden = true
        openInMapsBtn.hidden = true

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyOrderOpenDetailViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyOrderOpenDetailViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyOrderOpenDetailViewController.methodOfReceivedNotificationNetworkDetail(_:)), name:kJobCancelNotif, object: nil)
        
        loadActionBtn()
        // Do any additional setup after loading the view.
    }
    func ReconnectMethod()
    {
        if CurLaat != nil{
        let user : CLLocation = CLLocation.init(latitude: objDetailRec.jobLat.doubleValue, longitude: objDetailRec.jobLong.doubleValue)
        let partner : CLLocation = CLLocation.init(latitude: CurLaat, longitude: CurLong)
        bearing = getBearingBetweenTwoPoints1(user,locationB: partner)
            
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
      
            
        
    SocketIOManager.sharedInstance.emitTracking("\(self.objDetailRec.Userid)", tasker:"\(objUserRecs.providerId)", task:"\(self.objDetailRec.jobIdentity)" , lat:"\(CurLaat)" , long: "\(CurLong)", bearing: "\(bearing)",lastdrive: "\(lastDriving)")
            
               }

    }
  
    func getBearingBetweenTwoPoints1( A: CLLocation, locationB B: CLLocation) -> Double {
        var dlon: Double = self.toRad((B.coordinate.longitude - A.coordinate.longitude))
        let dPhi: Double = log(tan(self.toRad(B.coordinate.latitude) / 2 + M_PI / 4) / tan(self.toRad(A.coordinate.latitude) / 2 + M_PI / 4))
        if fabs(dlon) > M_PI {
            dlon = (dlon > 0) ? (dlon - 2 * M_PI) : (2 * M_PI + dlon)
        }
        return self.toBearing(atan2(dlon, dPhi))
    }
    
    func toRad(degrees: Double) -> Double {
        return degrees * (M_PI / 180)
    }
    
    func toBearing(radians: Double) -> Double {
        return self.toDegrees(radians) + 360 % 360
    }
    
    func toDegrees(radians: Double) -> Double {
        return radians * 180 / M_PI
    }


    func loadActionBtn(){
        let callImg = UIImage(named: "ImgCall")!
        let chatImg = UIImage(named: "imgChat")!
        let msgImg = UIImage(named: "imgMsg")!
        let mailImg = UIImage(named: "imgMail")!
        
        let callBtn = ActionButtonItem(title: "", image: callImg)
        callBtn.action = { item in
            self.callNumber(self.objDetailRec.jobPhone as String)
        }
        
                let chatBtn = ActionButtonItem(title: "", image: chatImg)
                chatBtn.action = { item in
                    let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatVCSID") as! MessageViewController
                    
                    NSLog("the task id =%@ and job id =%@",self.objDetailRec.jobIdentity,self.jobID )
                    objChatVc.jobId=self.objDetailRec.jobIdentity
                    objChatVc.Userid = self.objDetailRec.Userid
                    objChatVc.username = self.objDetailRec.jobUserName
                    objChatVc.Userimg = self.objDetailRec.userimage
                    objChatVc.RequiredJobid = self.objDetailRec.jobIdentity
                    self.navigationController!.pushViewController(objChatVc, animated: true)
        
                    }
        let messageBtn = ActionButtonItem(title: "", image: msgImg)
        messageBtn.action = { item in
            
            let messageComposeVC = self.configuredMessageComposeViewController("",number:"\(self.objDetailRec.jobPhone)")
            if MFMessageComposeViewController.canSendText() {
                self.presentViewController(messageComposeVC, animated: true, completion: nil)

            }
                   }
        
        let mailBtn = ActionButtonItem(title: "", image: mailImg)
        mailBtn.action = { item in
            
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }}
        
        actionButton = ActionButton(attachedToView: self.view, items: [callBtn,messageBtn,mailBtn,chatBtn ])
        actionButton.action = { button in button.toggleMenu() }
        actionButton.setTitle("+", forState: .Normal)
        
        actionButton.backgroundColor = UIColor(red: 51/255.0, green: 153/255.0, blue: 255/255.0, alpha:1.0)
    }
    func showSendMailErrorAlert() {
        self.view.makeToast(message:Language_handler.VJLocalizedString("not_send_email", comment: nil), duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("not_send_email_title", comment: nil))

    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["\(self.objDetailRec.jobEmail)"])
        mailComposerVC.setSubject("\(self.objDetailRec.jobTitle)")
        
        
        return mailComposerVC
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    func configuredMessageComposeViewController(message:String,number:String) -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        
        
        //  Make sure to set this property to self, so that the controller can be dismissed!
        //        messageComposeVC.recipients = textMessageRecipients
        messageComposeVC.body = "\(message)"
        messageComposeVC.recipients = [number]
        
        return messageComposeVC
    }
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    @objc func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func methodOfReceivedNotificationNetworkDetail(notification: NSNotification){
        loadDetail()
    }
    override func viewWillAppear(animated: Bool) {
        loadDetail()
    }
    func loadDetail(){
        self.showProgress()
        GetJobDetails()
    }
    
    func set_mapView()
    {
        let latitude = (objDetailRec.jobLat as NSString).doubleValue
        let longitude = (objDetailRec.jobLong as NSString).doubleValue
        let camera = GMSCameraPosition.cameraWithLatitude(latitude,
            longitude:longitude, zoom:15)
        
        MapView.camera=camera
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "MapPin")
        marker.map = MapView
        MapView.settings.setAllGesturesEnabled(false)
        
    }
    
       func setDatasForDescription(){
        
        callBtn.layer.cornerRadius=callBtn.frame.size.width/2
        callBtn.layer.borderWidth=1
        callBtn.layer.borderColor=PlumberThemeColor.CGColor
        callBtn.layer.masksToBounds=true
        titleLbl.numberOfLines=0
        
        titleLbl.sizeToFit()
        seperatorLbl.frame=CGRectMake(seperatorLbl.frame.origin.x, titleLbl.frame.origin.y+titleLbl.frame.size.height+3, seperatorLbl.frame.size.width, seperatorLbl.frame.size.height);

        descLbl.frame=CGRectMake(descLbl.frame.origin.x, service_tax.frame.origin.y+service_tax.frame.size.height , descLbl.frame.size.width, descLbl.frame.size.height);
        descLbl.numberOfLines=0
        
        descLbl.sizeToFit()
        topView.frame=CGRectMake(topView.frame.origin.x, topView.frame.origin.y, topView.frame.size.width, descLbl.frame.size.height + descLbl.frame.origin.y + 10);
       
        

        addressLbl.numberOfLines=0
        
        
        contentView.frame=CGRectMake(contentView.frame.origin.x, topView.frame.origin.y+topView.frame.size.height, contentView.frame.size.width, contentView.frame.size.height);
        detailScrollView.contentSize=CGSizeMake(self.view.frame.size.width, contentView.frame.size.height+contentView.frame.origin.y+40);
        MapView.frame=CGRectMake(MapView.frame.origin.x, MapView.frame.origin.y, MapView.frame.size.width, contentView.frame.size.height+35)
    }
    func GetJobDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(jobID)"]
        print(jobID)
        
        url_handler.makeCall(JobDetailUrl, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        self.detailScrollView.hidden=false
                        self.bottomView.hidden=false
                        
                        
                    
                        if(responseObject?.objectForKey("response")?.objectForKey("job")!.count>0){
                    let dict:NSDictionary=responseObject?.objectForKey("response")?.objectForKey("job") as! NSDictionary
                            
                            let ResponseDict : NSDictionary = responseObject?.objectForKey("response") as! NSDictionary
                            
                            self.objDetailRec.serv_tax = self.theme.CheckNullValue(ResponseDict.objectForKey("service_tax"))!
                            self.objDetailRec.jobIdentity=self.theme.CheckNullValue(dict.objectForKey("task_id"))!
                            
                            self.objDetailRec.Currency=self.theme.CheckNullValue(dict.objectForKey("currency"))!
                            
                            self.objDetailRec.jobDate=self.theme.CheckNullValue(dict.objectForKey("job_date"))!
                            
                            self.objDetailRec.jobTime=self.theme.CheckNullValue(dict.objectForKey("job_time"))!
                            
                            self.objDetailRec.jobTitle=self.theme.CheckNullValue(dict.objectForKey("job_type"))!
                            
                            self.objDetailRec.jobDesc=self.theme.CheckNullValue(dict.objectForKey("instruction"))!
                            
                            self.objDetailRec.jobStatus=self.theme.CheckNullValue(dict.objectForKey("btn_group"))!
                            
                            self.objDetailRec.Userid = self.theme.CheckNullValue(dict.objectForKey("user_id"))!
                            self.objDetailRec.RequireJobId = self.theme.CheckNullValue(self.jobID)!
                            self.objDetailRec.jobUserName=self.theme.CheckNullValue(dict.objectForKey("user_name"))!
                            
                            self.objDetailRec.currency_symbol = self.theme.getCurrencyCode(self.theme.CheckNullValue(dict.objectForKey("currency_code"))!)
                            
                            self.objDetailRec.jobEmail=self.theme.CheckNullValue(dict.objectForKey("user_email"))!
                            
                            self.objDetailRec.jobPhone=self.theme.CheckNullValue(dict.objectForKey("user_mobile"))!
                            
                            self.objDetailRec.hourrate=self.theme.CheckNullValue(dict.objectForKey("hourly_rate"))!
                            self.objDetailRec.cat_type=self.theme.CheckNullValue(dict.objectForKey("type"))!
                            
                          let location =  self.theme.CheckNullValue(dict.objectForKey("job_location"))!.stringByReplacingOccurrencesOfString(", ,", withString: ",")
                        self.objDetailRec.jobLocation=location
                            
                            self.objDetailRec.jobLat=self.theme.CheckNullValue(dict.objectForKey("location_lat"))!
                           self.objDetailRec.userimage = self.theme.CheckNullValue(dict.objectForKey("user_image"))!
                            //CurLaat = self.objDetailRec.job
                            
                            self.objDetailRec.jobLong=self.theme.CheckNullValue(dict.objectForKey("location_lon"))!
                            
                            self.objDetailRec.jobBtnStatus = self.theme.CheckNullValue(dict.objectForKey("btn_group"))!
                self.objDetailRec.cashoption = self.theme.CheckNullValue(dict.objectForKey("cash_option"))!
                            
                            
                            
                            self.SetDatasToDetail()
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    func SetDatasToDetail(){
        
        
        dateLbl.text="\(Language_handler.VJLocalizedString("date", comment: nil)) : \(objDetailRec.jobDate)"
        timeLbl.text="\(Language_handler.VJLocalizedString("time", comment: nil)) : \(objDetailRec.jobTime)"
        titleLbl.text="\(objDetailRec.jobTitle)"
        
        service_tax.text = "\(objDetailRec.serv_tax)% service fee is added to every job"
        

        let getString = objDetailRec.jobDesc.condensedWhitespace
        descLbl.text="\(getString)"
        jobIdLbl.text="\(Language_handler.VJLocalizedString("job_id", comment: nil)) : \(jobID)"
        
        hourlyratLbl.text="\(objDetailRec.cat_type) : \(theme.getappCurrencycode())\(objDetailRec.hourrate)"
        
        addressLbl.text = self.theme.CheckNullValue(theme.getAddressForLatLng( self.objDetailRec.jobLat as String, longitude: self.objDetailRec.jobLong as String, status :""))
        
        
        //NSLog( "The user id =%@",objDetailRec.jobIdentity);
      //  addressLbl.text="\(objDetailRec.jobLocation)"
        setDatasForDescription()
        set_mapView()
        setBottomViewWithButton(objDetailRec.jobBtnStatus)
        
    }
    
       @IBAction func didClickBackBtn(sender: AnyObject) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("SortingNotification", object:self,userInfo: nil )
        NSNotificationCenter.defaultCenter().postNotificationName("SortingJobNotification", object:self,userInfo: nil )


        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    @IBAction func didClickOpenInMapsBtn(sender: AnyObject) {
        moveToLocVc(false)
    }
    
    func moveToLocVc(isShowArriveBtn:Bool){//LocationVCSID
        let ObjLocVc=self.storyboard!.instantiateViewControllerWithIdentifier("LocationVCSID")as! LocationViewController
        ObjLocVc.isShowArriveBtn=isShowArriveBtn
        ObjLocVc.jobStatus = self.objDetailRec.jobBtnStatus
        ObjLocVc.mapLaat=(objDetailRec.jobLat as NSString).doubleValue
        ObjLocVc.mapLong=(objDetailRec.jobLong as NSString).doubleValue
        trackingDetail.userLat = (objDetailRec.jobLat as NSString).doubleValue
        trackingDetail.userLong = (objDetailRec.jobLong as NSString).doubleValue

        ObjLocVc.addressStr=addressLbl.text
        ObjLocVc.phoneStr=objDetailRec.jobPhone
        ObjLocVc.getUsername = objDetailRec.jobUserName
        ObjLocVc.jobId=self.objDetailRec.jobIdentity as String
        ObjLocVc.userId = self.objDetailRec.Userid as String
        self.navigationController?.pushViewController(ObjLocVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setBottomViewWithButton(statStr:NSString){
        if (statStr=="1" || statStr=="2" || statStr=="3"){
            openInMapsBtn.hidden=false
            viewInMapsView.hidden = false
        }
        else{
            openInMapsBtn.hidden=true
            viewInMapsView.hidden = true
            
        }
       
        if(statStr.length>0){
            bottomView.hidden=false
            if(statStr=="1"){
                
                
                if (Getorderstatus == "Missed Job")
                {
                    rejectBtn.hidden=true
                    acceptBtn.hidden=true
                    singleBtn.hidden=false
     singleBtn.setTitle(Language_handler.VJLocalizedString("miss_job", comment: nil), forState: UIControlState.Normal)
                }
                else
                {
                rejectBtn.hidden=false
                acceptBtn.hidden=false
                singleBtn.hidden=true
                //rejectBtn.setTitle("Reject", forState: UIControlState.Normal)
 acceptBtn.setTitle(Language_handler.VJLocalizedString("accept", comment: nil), forState: UIControlState.Normal)
    acceptBtn.frame=CGRectMake(rejectBtn.frame.origin.x + rejectBtn.frame.size.width, acceptBtn.frame.origin.y, rejectBtn.frame.size.width , acceptBtn.frame.size.height);
 rejectBtn.setTitle(Language_handler.VJLocalizedString("reject", comment: nil), forState: UIControlState.Normal)
                
                }
                
                
            }
            else if(statStr=="2"){
                rejectBtn.hidden=false
                acceptBtn.hidden=false
                singleBtn.hidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel_caps", comment: nil), forState: UIControlState.Normal)
                acceptBtn.setTitle(Language_handler.VJLocalizedString("start_off", comment: nil), forState: UIControlState.Normal)
                acceptBtn.frame=CGRectMake(rejectBtn.frame.origin.x + rejectBtn.frame.size.width, acceptBtn.frame.origin.y, rejectBtn.frame.size.width , acceptBtn.frame.size.height);
                
            }
            else if(statStr=="3"){
                rejectBtn.hidden=false
                acceptBtn.hidden=false
                singleBtn.hidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel_caps", comment: nil), forState: UIControlState.Normal)
                acceptBtn.setTitle(Language_handler.VJLocalizedString("arrived", comment: nil), forState: UIControlState.Normal)
               acceptBtn.frame=CGRectMake(rejectBtn.frame.origin.x + rejectBtn.frame.size.width, acceptBtn.frame.origin.y, rejectBtn.frame.size.width , acceptBtn.frame.size.height);
            }
            else if(statStr=="4"){
                rejectBtn.hidden=false
                acceptBtn.hidden=false
                singleBtn.hidden=true
                openInMapsBtn.hidden=true
                viewInMapsView.hidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel_caps", comment: nil), forState: UIControlState.Normal)
                acceptBtn.setTitle(Language_handler.VJLocalizedString("start_job", comment: nil), forState: UIControlState.Normal)

               acceptBtn.frame=CGRectMake(rejectBtn.frame.origin.x + rejectBtn.frame.size.width, acceptBtn.frame.origin.y, rejectBtn.frame.size.width , acceptBtn.frame.size.height);
            }
            else if(statStr=="5"){
                openInMapsBtn.hidden=true
                viewInMapsView.hidden=true
                rejectBtn.hidden=true
                acceptBtn.hidden=true
                singleBtn.hidden=false
                singleBtn.setTitle(Language_handler.VJLocalizedString("complete_job", comment: nil), forState: UIControlState.Normal)
                
            }
            else if(statStr=="6"){
                rejectBtn.hidden=true
                acceptBtn.hidden=true
                singleBtn.hidden=false
                openInMapsBtn.hidden=true
                viewInMapsView.hidden=true
                singleBtn.setTitle(Language_handler.VJLocalizedString("payment", comment: nil), forState: UIControlState.Normal)
                
            }
            else if(statStr=="7"){
                jobCancelLbl.hidden=false
                rejectBtn.hidden=true
                acceptBtn.hidden=true
                singleBtn.hidden=true
                openInMapsBtn.hidden=true
                viewInMapsView.hidden=true
                jobCancelLbl.text=Language_handler.VJLocalizedString("job_cancelled", comment: nil)
            }
            else {
                rejectBtn.hidden=true
                acceptBtn.hidden=true
                singleBtn.hidden=false
                singleBtn.setTitle(Language_handler.VJLocalizedString("more_info", comment: nil), forState: UIControlState.Normal)
            }
            //if()
        }else{
            bottomView.hidden=true
        }
        
        
        
        
        if statStr == "3"
        {
            backgroundTaskIdentifier = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            UIApplication.sharedApplication().endBackgroundTask(self.backgroundTaskIdentifier!)
        })

//            LocationTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(MyOrderOpenDetailViewController.ReconnectMethod), userInfo: nil, repeats: true)
            
           oo
           
            LocationTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(MyOrderOpenDetailViewController.ReconnectMethod), userInfo: nil, repeats: true)
            NSRunLoop.currentRunLoop().addTimer(LocationTimer, forMode: NSRunLoopCommonModes)

        }
        else{
            LocationTimer.invalidate()
        }
    }
    
    @IBAction func didClickSingleBtn(sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("complete_job", comment: nil)){
            // self.displayViewController(.BottomTop)
            self.sendJobDetails()
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("payment", comment: nil)){
            moveToPaymentOrMoreInfoVC()
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("more_info", comment: nil)){
            moveToPaymentOrMoreInfoVC()
        }
        
    }
    func moveToPaymentOrMoreInfoVC(){
        
//        let objRatingsvc = self.storyboard!.instantiateViewControllerWithIdentifier("RatingsVCSID") as! RatingsViewController
//        //                        objReceiveCashvc.priceString=priceStr
//        objRatingsvc.jobIDStr=jobID
//        self.navigationController!.pushViewController(objRatingsvc, animated: true)
        
        let objFarevc = self.storyboard!.instantiateViewControllerWithIdentifier("FareSummaryVCSID") as! FareSummaryViewController
        objFarevc.jobIDStr=jobID
        objFarevc.cashoption = self.objDetailRec.cashoption
       self.navigationController!.pushViewController(objFarevc, animated: true)
    }
    @IBAction func didClickRejectBtn(sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("reject", comment: nil)){
            
          getCancelReasonForJob()
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("cancel_caps", comment: nil)){
            getCancelReasonForJob()
        }
    }
    @IBAction func didClickAcceptBtn(sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("accept", comment: nil)){
            jobUpdateStatus(AcceptRideUrl)
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("start_off", comment: nil)){
            jobUpdateStatus(StartDestinationUrl)
            
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("arrived", comment: nil)){
            jobUpdateStatus(ArrivedDestinationUrl)
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("start_job", comment: nil)){
            jobUpdateStatus(JobStartUrl)
        }
    }
    @IBAction func didClickWorkFlowBtn(sender: UIButton) {//
        
        let ObjStepsVc=self.storyboard!.instantiateViewControllerWithIdentifier("StepsTreeVCSID")as! StepsTreeViewController
        ObjStepsVc.JobIdStr=jobID
        self.navigationController?.pushViewController(ObjStepsVc, animated: false)
    }
    
    func jobUpdateStatus(statusType:NSString){
        if CurLaat != nil{

        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                 "job_id":"\(jobID)",
                                 "provider_lat":"\(CurLaat)",
                                 "provider_lon":"\(CurLong)"]
        // print(Param)
        if statusType == StartDestinationUrl{
            trackingDetail.partnerLat = CurLaat
                trackingDetail.partnerLong = CurLong
        }
        acceptBtn.userInteractionEnabled=false
        rejectBtn.userInteractionEnabled=false
        url_handler.makeCall(statusType, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            self.acceptBtn.userInteractionEnabled=true
            self.rejectBtn.userInteractionEnabled=true
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                
                    if(status == "1")
                    {
                        self.detailScrollView.hidden=false
                        self.bottomView.hidden=false
                        
                        let dict:NSDictionary=(responseObject?.objectForKey("response"))! as! NSDictionary
                        self.objDetailRec.jobBtnStatus=self.theme.CheckNullValue(dict.objectForKey("btn_group"))!
                        
                        NSLog("Get btnGroup Response=%@", self.objDetailRec.jobBtnStatus)
           self.view.makeToast(message:self.theme.CheckNullValue(dict.objectForKey("message"))!, duration: 2, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("success", comment: nil))
                        var messagestr : String = self.theme.CheckNullValue(dict.objectForKey("message"))!
                        self.setBottomViewWithButton(self.objDetailRec.jobBtnStatus)

                        if (messagestr == "Job Rejected Successfully" )
                        {
                            self.GetJobDetails()
                            
                        }
                      
                        
                        
                    }
                    else
                    {//alertImg
                        let image = UIImage(named: "alertImg")
                        
                        self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject?.objectForKey("response"))!, ButtonTitle: kOk)
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
        }
    }
    
    
    func jobUpdateCancelStatus(statusType:NSString){
        
    
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "job_id":"\(jobID)",
            "reason":"\(reasonIdStr)"]
        // print(Param)
        
        url_handler.makeCall(statusType, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName("reloadCurrentJob", object: nil)
                        self.detailScrollView.hidden=false
                        self.bottomView.hidden=false
                        let dict:NSDictionary=(responseObject?.objectForKey("response"))! as! NSDictionary
                        self.objDetailRec.jobBtnStatus=self.theme.CheckNullValue(dict.objectForKey("btn_group"))!
                        self.setBottomViewWithButton(self.objDetailRec.jobBtnStatus)
              
                        
                        self.theme.AlertView( Language_handler.VJLocalizedString("success", comment: nil), Message: self.theme.CheckNullValue(dict.objectForKey("message"))!, ButtonTitle: kOk)
                     
                        
                        
                    }
                    else
                    {//alertImg
                        let image = UIImage(named: "alertImg")
                        
                        self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject?.objectForKey("response"))!, ButtonTitle: kOk)
                       
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    
    func getCancelReasonForJob(){
        self.showProgress()
        acceptBtn.userInteractionEnabled=false
        rejectBtn.userInteractionEnabled=false

        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(CancelReasonUrl, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            self.acceptBtn.userInteractionEnabled=true
            self.rejectBtn.userInteractionEnabled=true

            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    
                    
                    self.ReasonDetailArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressNameArray.removeAllObjects()
                    
                    self.ReasonidArray.removeAllObjects()
                    Schedule_Data.ScheduleAddressArray.removeAllObjects()
                    let Dict:NSDictionary=responseObject!

                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        //let ReasonArray:NSArray=Dict.objectForKey("response")!.objectForKey("reason") as! NSArray
                        let ResponseDic:NSDictionary=Dict.objectForKey("response") as! NSDictionary
                        let ReasonArray : NSArray = ResponseDic.objectForKey("reason") as! NSArray
                        
                        for ReasonDict in ReasonArray
                        {
                            let Reason_Str:NSString=self.theme.CheckNullValue(ReasonDict.objectForKey("reason"))!
                            self.ReasonDetailArray.addObject(Reason_Str)
                            Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                            let Reasonid:NSString=self.theme.CheckNullValue(ReasonDict.objectForKey("id"))!
                            self.ReasonidArray.addObject(Reasonid)
                            Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                            
                            
                        }
                        
                        
                        let Reason_Str:NSString="Others"
                        self.ReasonDetailArray.addObject(Reason_Str)
                        Schedule_Data.ScheduleAddressArray.addObject(Reason_Str)
                        let Reasonid:NSString="1"
                        self.ReasonidArray.addObject(Reasonid)
                        Schedule_Data.ScheduleAddressNameArray.addObject(Reasonid)
                        self.displayViewController(.BottomBottom)
                    }
                    else
                    {//alertImg
                        
                        let messagestr :String = self.theme.CheckNullValue(responseObject?.objectForKey("response"))!
                        self.theme.AlertView("\(appNameJJ)", Message: messagestr, ButtonTitle: kOk)
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    
    func displayViewController(animationType: SLpopupViewAnimationType) {
        let myPopupViewController:MyPopupViewController = MyPopupViewController(nibName:"MyPopupViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.isDetailViewcontroller = true
        self.presentpopupViewController(myPopupViewController, animationType: animationType, completion: { () -> Void in
            
        })
    }
    func pressCancel(sender: MyPopupViewController) {
        self.dismissPopupViewController(.BottomBottom)
    }
    func pressAdd(sender: MyPopupViewController) {
        
    }
    
    
    func PassSelectedAddress(Address:NSString,Addressid:NSString)
 {
        
        self.dismissPopupViewController(.BottomBottom)
        if (Address == "Others")
        {
            
            
            let alert = UIAlertController(title: "Reason", message: "", preferredStyle: .Alert)
            
            alert.addTextFieldWithConfigurationHandler(configurationTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
                
                
                self.ChoosedReasonid = self.tField.text!
                self.cancelRequest()
                
                
            }))
            self.presentViewController(alert, animated: true, completion: {
                print("completion block")
            })
            
            
        }
        else
        {
            
            
            ChoosedReasonid=Addressid
            
            let AlertView:UIAlertView=UIAlertView()
            AlertView.delegate=self
            AlertView.tag=1
            AlertView.title=theme.setLang("cancel_confirmation")
            AlertView.addButtonWithTitle(Language_handler.VJLocalizedString("Yes", comment: nil))
            AlertView.addButtonWithTitle(Language_handler.VJLocalizedString("no", comment: nil))
            AlertView.show()
        }
        
    }
    
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
        if(View.tag == 1)
        {
            
            switch buttonIndex{
                
            case 0:
                self.cancelRequest()
                break;
            default:
                break;
                //Some code here..
                
            }
        }
    }
    
    
    
    
    func cancelRequest()
    {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        self.showProgress()
        let param=["provider_id":"\(objUserRecs.providerId)","reason":"\(ChoosedReasonid)","job_id":"\(jobID)"]
        
        
        
        url_handler.makeCall(CancelJobUrl, param: param) { (responseObject, error) -> () in
            
           
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: appNameJJ)
                
                //self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    if(Status == "1")
                    {
                        NSNotificationCenter.defaultCenter().postNotificationName("reloadCurrentJob", object: nil)
                        self.detailScrollView.hidden=false
                        self.bottomView.hidden=false
                        let dict:NSDictionary=(responseObject?.objectForKey("response"))! as! NSDictionary
                        self.objDetailRec.jobBtnStatus=self.theme.CheckNullValue(dict.objectForKey("btn_group"))!
                        self.setBottomViewWithButton(self.objDetailRec.jobBtnStatus)
                        
                        
                        self.theme.AlertView( Language_handler.VJLocalizedString("success", comment: nil), Message: self.theme.CheckNullValue(dict.objectForKey("message"))!, ButtonTitle: kOk)
                        
                        
                        
                    }
                    else
                    {
                        let Response:NSString=Dict.objectForKey("response") as! NSString
                        
                        self.theme.AlertView("\(appNameJJ)", Message: "\(Response)", ButtonTitle: Language_handler.VJLocalizedString("ok", comment: nil))
                        
                    }
                    
                }
                else
                {
                    self.theme.AlertView("\(appNameJJ)", Message: self.theme.setLang("cant_cancel"), ButtonTitle: kOk)
                }
            }
            
        }
        
        
            
        
        
    }

    
    func configurationTextField(textField: UITextField!)
    {
      
        textField.placeholder = Language_handler.VJLocalizedString("reason_placeholder", comment: nil)
        
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }

        func keyboardWillShow(notification:NSNotification){
//        if(( self.view.window) != nil){
//            var userInfo = notification.userInfo!
//            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
//            keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
//            
//            var frame:CGRect = myPopupViewController.view.frame
//            
//            frame.origin.y = self.view.frame.origin.y-75
//            myPopupViewController.view.frame = frame
//        }
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        if(( self.view.window) != nil){
            myPopupViewController.view.center=self.view.center
        }
    }
    
   
    
    func submitJob(sender: MyPopupViewController, withDesc: NSString, withCost: NSString) {
        self.dismissPopupViewController(.Fade)
       // sendJobDetails(withDesc, cost: withCost)
    }
    
    func pressedCancel(sender: MyPopupViewController) {
        self.dismissPopupViewController(.Fade)
    }
    func sendJobDetails(){
        myPopupViewController.removeFromParentViewController()
        self.displayViewController1(.BottomBottom)
    }
    
    
    func dismissPopupViewController1(animationType: SLpopupViewAnimationType) {
        let sourceView:UIView = self.getTopView()
        let popupView:UIView = sourceView.viewWithTag(kpopupViewTag)!
        let overlayView:UIView = sourceView.viewWithTag(kOverlayViewTag)!
        switch animationType {
        case .BottomTop, .BottomBottom, .TopTop, .TopBottom, .LeftLeft, .LeftRight, .RightLeft, .RightRight:
            self.slideViewOut(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
        default:
            fadeViewOut(popupView, sourceView: sourceView, overlayView: overlayView)
        }
        
        
    }

func displayViewController1(animationType: SLpopupViewAnimationType) {
    myMaterialView = MiscellaneousVC(nibName:"MaterialDesignView", bundle: nil)
    myMaterialView.delegate = self
    myMaterialView.jobIDStr=jobID
    myMaterialView.currency=self.objDetailRec.currency_symbol
    
    
    //self.navigationController!.pushViewController(objFarevc, animated: true)
    self.presentpopupViewController(myMaterialView, animationType: animationType, completion: { () -> Void in
        
    })
}

    
    func pressedCancelMaterial(sender: MiscellaneousVC) {
        self.dismissPopupViewController1(.Fade)
        loadDetail()
    }


    @IBAction func didClickPhoneBtn(sender: AnyObject) {
        
        //        callNumber(objDetailRec.jobPhone as String)
    }
    private func callNumber(phoneNumber:String) {
        
        print("\(phoneNumber) get mnuber")
        
        let Number:String="\(phoneNumber)"
        let trimmedString = Number.stringByReplacingOccurrencesOfString("+ ", withString: "")
        
        
        if let phoneCallURL:NSURL = NSURL(string:"tel://"+"\(trimmedString)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

extension String {
    var condensedWhitespace: String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        return components.filter { !$0.isEmpty }.joinWithSeparator(" ")
    }
}
