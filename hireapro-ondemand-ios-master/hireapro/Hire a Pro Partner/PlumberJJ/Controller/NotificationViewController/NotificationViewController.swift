//
//  NotificationViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/29/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var notifications: UILabel!
    @IBOutlet weak var notificationTblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
 notifications.text = Language_handler.VJLocalizedString("notifications", comment: nil)
        // Do any additional setup after loading the view.
    }

    @IBAction func didClickBackBtn(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotifTableCellIdentifier", forIndexPath: indexPath) as! NotificationTableViewCell
        cell.loadNotificationTableCell("")
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //MyOrderDetailOpenVCSID
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
