//
//  LoginViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/18/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import CoreLocation

class LoginViewController: RootBaseViewController {
  //  var theme:Theme=Theme()
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var loginDesc: UILabel!
    @IBOutlet var back_btn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginScrollView: UIScrollView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        decorateTxtField()
      
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func applicationLanguageChangeNotification(notification: NSNotification) {
        
        welcomeLabel.text = Language_handler.VJLocalizedString("welcome", comment: nil)
        loginDesc.text = Language_handler.VJLocalizedString("login_disc", comment: nil)
        forgotPasswordBtn.setTitle(Language_handler.VJLocalizedString("forgot_password_btn", comment: nil), forState: UIControlState.Normal)
        loginBtn.setTitle(Language_handler.VJLocalizedString("login", comment: nil), forState: UIControlState.Normal)
        userNameTxtField.placeholder = Language_handler.VJLocalizedString("email_placeholder", comment: nil)
        passwordTxtField.placeholder = Language_handler.VJLocalizedString("password_placeholder", comment: nil)
    }
    

    func decorateTxtField(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name:UIKeyboardWillHideNotification, object: nil)
       
        //Changing the corner radius of textView
        
        userNameTxtField.layer.cornerRadius=20
        userNameTxtField.layer.borderWidth=1
        userNameTxtField.layer.borderColor=UIColor.whiteColor().CGColor
        userNameTxtField.layer.masksToBounds=true
        userNameTxtField.textAlignment = .Center
        
        passwordTxtField.layer.cornerRadius=20
        passwordTxtField.layer.borderWidth=1
        passwordTxtField.layer.borderColor=UIColor.whiteColor().CGColor
        passwordTxtField.layer.masksToBounds=true
        passwordTxtField.textAlignment = .Center
        
        loginBtn.layer.cornerRadius=20
        loginBtn.layer.masksToBounds=true
        loginScrollView.contentSize=CGSizeMake(loginScrollView.frame.size.width, forgotPasswordBtn.frame.origin.y+forgotPasswordBtn.frame.size.height+30)
    }

    @IBAction func didClickLoginBtn(sender: AnyObject) {
        self.view.endEditing(true)
        if(validateTxtFields()){
            showProgress()
            let Param: Dictionary = ["email":"\(userNameTxtField.text!)",
                "password":"\(passwordTxtField.text!)","deviceToken":"\(theme.GetDeviceToken())"]
            
            url_handler.makeCall(LoginUrl, param: Param) {
                (responseObject, error) -> () in
                self.DismissProgress()
                if(error != nil)
                {
                    print("error responded with: \(error)")
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionTop, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
                else
                {
                    if(responseObject != nil)
                    {
                        let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!;
                        
                        if(status == "1")
                        {
                            self.theme.saveUserDetail((responseObject?.objectForKey("response"))! as! NSDictionary)
                            
                            
                            let ResponseDict  : NSDictionary = responseObject?.objectForKey("response") as! NSDictionary
                            let currencycode: String = self.theme.getCurrencyCode(self.theme.CheckNullValue(ResponseDict.objectForKey("currency"))!) as String
                            
                            self.theme.saveappCurrencycode( currencycode as String )
                            

                            let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
                         
                            
                            SocketIOManager.sharedInstance.establishConnection()
                            SocketIOManager.sharedInstance.establishChatConnection()
                      
                            if(self.theme.isUserLigin()){
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                appDelegate.setInitialViewcontroller()
                                let socStr:NSString = ""//dict.objectForKey("soc_key") as! NSString
                                if(socStr.length>0){
                                    self.theme.saveJaberPassword(socStr as String)
                                }
                                //appDelegate.setXmppConnect()
                            }

                           
                        }
                        else
                        {
                            if self.theme.CheckNullValue(responseObject?.objectForKey("response")) != ""
                            {
                                self.theme.AlertView("\(appNameJJ)",Message:self.theme.CheckNullValue(responseObject?.objectForKey("response"))!,ButtonTitle: kOk)

                            }
                            else{
                   self.theme.AlertView("\(appNameJJ)",Message:self.theme.CheckNullValue(responseObject?.objectForKey("errors"))!,ButtonTitle: kOk)
                            }
                        }
                    }
                    else
                    {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionTop, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
            }
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = loginScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+100
        loginScrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification)
        
    {
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        loginScrollView.contentInset = contentInset
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(range.location==0 && string==" "){
            return false
        }
        
        return true
    }
    func validateTxtFields () -> Bool{
        var isOK:Bool=true
      if(userNameTxtField.text!.characters.count==0){
        
        theme.AlertView("\(appNameJJ)",Message: Language_handler.VJLocalizedString("enter_email_alert", comment: nil),ButtonTitle: kOk)
        

            isOK=false
        }
        else if(passwordTxtField.text!.characters.count==0){
        theme.AlertView("\(appNameJJ)",Message: Language_handler.VJLocalizedString("enter_password_alert", comment: nil),ButtonTitle: kOk)

        isOK=false
        }
            
        
        return isOK
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
