//
//  MyLeadsViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/23/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class MyLeadsViewController: RootBaseViewController,PopupSortingViewControllerDelegate,MyOrderOpenDetailViewControllerDelegate {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var topView: SetColorView!
    
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func applicationLanguageChangeNotification(notification: NSNotification) {
        titleHeader.text = Language_handler.VJLocalizedString("new_leads", comment: nil)
        btnFilter.setTitle(Language_handler.VJLocalizedString("filter", comment: nil), forState: UIControlState.Normal)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        
        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.hidden=false;
            barButton.hidden=true
        }else{
        }
        /*     let swiftPagesView : SwiftPages!
         swiftPagesView = SwiftPages(frame: CGRectMake(0, 0, containerView.frame.width, containerView.frame.height))
         
         //Initiation
         let VCIDs : [String] = ["NewLeadsVCSID"]
         let titles : [String] = [Language_handler.VJLocalizedString("new_leads", comment: nil)]
         
         //Sample customization
         swiftPagesView.initializeWithVCIDsArrayAndButtonTitlesArray(VCIDs, buttonTitlesArray: titles)
         swiftPagesView.setTopBarBackground(PlumberLightGrayColor)
         swiftPagesView.setAnimatedBarColor(PlumberThemeColor)
         containerView.addSubview(swiftPagesView) */
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyLeadsViewController.MoveToDetail(_:)), name: kNewLeadsNotif, object: nil)
        barButton.addTarget(self, action: #selector(MyLeadsViewController.openmenu), forControlEvents: .TouchUpInside)
        
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func MoveToDetail(notification:NSNotification){
        let rec:MyOrderOpenRecord = notification.object as! MyOrderOpenRecord
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
        objMyOrderVc.delegate = self
        objMyOrderVc.jobID=rec.orderId
        objMyOrderVc.Getorderstatus = rec.orderstatus
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    @IBAction func didclickFilterOption(sender: AnyObject) {
        self.displayViewController(.BottomBottom)
        
    }
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let Popupsortcontroller : PopupSortingViewController = PopupSortingViewController(nibName:"PopupSortingViewController",bundle: nil)
        
        Popupsortcontroller.delegate = self;
        self.presentpopupViewController(Popupsortcontroller, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func  pressedCancel(sender: PopupSortingViewController) {
        
        
        
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int,isToday:Int,isSortby:NSString) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("SortingNotification", object:nil,userInfo: ["Fromdate":"\(fromdate)","Todate":"\(todate)","asDes":"\(isAscendorDescend)","isToday":"\(isToday)","StatusforSort":"\(isSortby)"] )
        
        
        
    }
    
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int, isSortby: NSString) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
