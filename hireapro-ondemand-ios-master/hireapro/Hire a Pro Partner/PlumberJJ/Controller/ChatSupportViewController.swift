//
//  ChatSupportViewController.swift
//  Plumbal
//
//  Created by Casperon on 01/08/17.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class ChatSupportViewController: RootBaseViewController,UIGestureRecognizerDelegate {

    @IBOutlet var chatsupport_table: UITableView!
    @IBOutlet var chat_support: UILabel!
    @IBOutlet var chat_start: UILabel!
    var ReferenceIdArray : NSMutableArray = NSMutableArray()
    var TopicArray : NSMutableArray = NSMutableArray()
    var FrommsgArray:NSMutableArray = NSMutableArray()
    var lastmsgArray : NSMutableArray = NSMutableArray()
    var dateArray : NSMutableArray = NSMutableArray()
    let URL_Handler:URLhandler=URLhandler()
    var themes:Theme=Theme()
    var tField: UITextField!
    @IBOutlet var chat_view: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        chat_support.text = Language_handler.VJLocalizedString("chat_support", comment: nil)
        chat_start.text = Language_handler.VJLocalizedString("chat_start", comment: nil)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(ChatSupportViewController.GotoChat(_:)))
        
        tapgesture.delegate=self
        
        self.chat_view.addGestureRecognizer(tapgesture)
        let Nb=UINib(nibName: "ChatsupportTableViewCell", bundle: nil)
        chatsupport_table.registerNib(Nb, forCellReuseIdentifier: "chatsupport")
        chatsupport_table.estimatedRowHeight=120
        
        chatsupport_table.rowHeight = UITableViewAutomaticDimension
        chatsupport_table.separatorColor=UIColor.lightGrayColor()
        chatsupport_table.tableFooterView=UIView()
        self.showProgress()
        self.GetDetails()
    }
    func GotoChat(sender:UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: Language_handler.VJLocalizedString("support", comment: nil), message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("cancel", comment: nil), style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("Send", comment: nil), style: .Default, handler:{ (UIAlertAction) in
            if self.tField.text == ""
            {
                
            }
            else{
                Message_details.support_chatid = ""
                Message_details.admin_id = ""
                Message_details.topic_title = self.tField.text!
                let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
        
    }

    
    func GetDetails()
    {
        
        
        ReferenceIdArray=NSMutableArray()
        TopicArray=NSMutableArray()
        FrommsgArray=NSMutableArray()
        lastmsgArray=NSMutableArray()
        dateArray=NSMutableArray()
        
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId
        
        let param=["type":"tasker","user":"\(providerid)"]
        
        
        URL_Handler.makeCall(Chat_support_Details, param: param) { (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: appNameJJ)
                
                // self.themes.AlertView("Network Failure", Message: "Please try again", ButtonTitle: "Ok")
                
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.themes.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    
                    if(Status == "1")
                    {
                        
                        let ChatList: NSMutableArray = (Dict.objectForKey("response")!.objectForKey("history") as? NSMutableArray)!
                        if(ChatList.count > 0 )
                        {
                            self.chatsupport_table.hidden=false
                            for  Dict in ChatList
                            {
                                
                                let refer_id=self.themes.CheckNullValue(Dict.objectForKey("reference"))!
                                self.ReferenceIdArray.addObject(refer_id)
                                let topic=self.themes.CheckNullValue(Dict.objectForKey("topic"))!
                                self.TopicArray.addObject(topic)
                                let from_id=self.themes.CheckNullValue(Dict.objectForKey("from"))!
                                self.FrommsgArray.addObject(from_id)
                                let last_msg=self.themes.CheckNullValue(Dict.objectForKey("message"))!
                                self.lastmsgArray.addObject(last_msg)
                                let date=self.themes.CheckNullValue(Dict.objectForKey("date"))!
                                self.dateArray.addObject(date)
                                
                                
                             
                            }
                            self.chatsupport_table.reloadData()
                        }
                        else
                        {
                            self.chatsupport_table.hidden=true
                        }
                    }
                    else
                    {
                        let Response:NSString = self.themes.CheckNullValue(Dict.objectForKey("response"))!
                        
                        self.themes.AlertView("\(appNameJJ)", Message: "\(Response)", ButtonTitle: kOk)
                        
                    }
                    
                }
                else
                {
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault, title: appNameJJ)
                    
                }
            }
            
            
        }
        
        
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ReferenceIdArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("chatsupport") as! ChatsupportTableViewCell
        Cell.selectionStyle = .None
        Cell.topic_labl.text = self.TopicArray.objectAtIndex(indexPath.row) as? String
        Cell.lastmsg_labl.text = self.lastmsgArray.objectAtIndex(indexPath.row) as? String
        Cell.date_labl.text = self.dateArray.objectAtIndex(indexPath.row) as? String
     
        return Cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        Message_details.support_chatid = self.ReferenceIdArray[indexPath.row] as! NSString
        Message_details.admin_id = self.FrommsgArray[indexPath.row] as! NSString
        
        let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
        
        self.navigationController?.pushViewController(secondViewController, animated: true)
        //        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func didclickOption(sender: AnyObject) {
        if sender.tag == 0
        {
            self.view.endEditing(true)
            self.frostedViewController.view.endEditing(true)
            // Present the view controller
            //
            self.frostedViewController.presentMenuViewController()    }
        else if sender.tag == 1
       {
        let alert = UIAlertController(title: "Support", message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("Send", comment: nil), style: .Default, handler:{ (UIAlertAction) in
            if self.tField.text == ""
            {
               self.themes.AlertView("\(appNameJJ)", Message:"Kindly enter a Topic", ButtonTitle:kOk)
            }
            else{
            Message_details.support_chatid = ""
            Message_details.admin_id = ""
            Message_details.topic_title = self.tField.text!
            let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("supportChat") as! supportMessageViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })

        }
    }
    
    
    func configurationTextField(textField: UITextField!)
    {
        
        textField.placeholder = Language_handler.VJLocalizedString("Topic_you_like_to_start_discuss_with...", comment: nil)
        
        tField = textField
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
