//
//  LocationViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/12/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import CoreLocation


class LocationViewController: RootBaseViewController {
    var jobStatus: NSString!
    var isShowArriveBtn:Bool!
    var addressStr:NSString!
    var phoneStr:NSString!
    var mapLaat:Double!
    var isPolyLineDrawn:Bool!
    var getUsername : NSString!
    var mapLong:Double!
    var jobId : String!
    var userId:String!
    let partnerMarker = GMSMarker()
    let userMarker = GMSMarker()
    var providerId = String()
    
    var getCurrentLocation : CLLocation = CLLocation()
    var model : MapRequestModel!
    
    @IBOutlet weak var Destination: UILabel!
    @IBOutlet weak var StartedFrom: UILabel!
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var lblStartLoc: UILabel!

    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet var distancelabl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
  
    let URL_Handler:URLhandler=URLhandler()
    let marker1 = GMSMarker()
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgress()
        NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(LocationViewController.updatelocation), name:"UpdateLocationNotify", object: nil)
        titleHeader.text = Language_handler.VJLocalizedString("location", comment: nil)
          Destination.text = Language_handler.VJLocalizedString("Destination", comment: nil)
          StartedFrom.text = Language_handler.VJLocalizedString("Started_From", comment: nil)
      
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        providerId = objUserRecs.providerId as String
        
        
        let currentLocation = CLLocation.init(latitude:CurLaat, longitude: CurLong)
            //locationManager.startUpdatingLocation()
            
            // upDateLocation()
            if((isPolyLineDrawn) == nil){
                drawRoadRouteBetweenTwoPoints(currentLocation.coordinate)
                
            }else{
                setAnimatedMapView(currentLocation.coordinate)
                
                
            }
            
            
            
            let kilometer : Double = self.kilometersfromPlace(currentLocation.coordinate)*0.6213711922
            print ("get distance \(kilometer)")
            
            var speed: Double = currentLocation.speed
            speed =  (speed * 3600 )/100
            //converting speed from m/s to knots(unit)
            print("Speed \(speed)")
            // distancelabl.text = "\(round(kilometer))Miles"
            distancelabl.text = "\(round(kilometer))\(Language_handler.VJLocalizedString("Miles", comment: nil))"
            // print()
            //Language_handler.VJLocalizedString("Miles", comment: nil)
            
            //locationManager.stopUpdatingLocation()
        
        // For use in foreground
        
        
    }
   
    
    
    func updatelocation()
    {
        let currentLocation = CLLocation.init(latitude:CurLaat, longitude: CurLong)
        setAnimatedMapView(currentLocation.coordinate)
        
        
        let kilometer : Double = self.kilometersfromPlace(currentLocation.coordinate)*0.6213711922
        print ("get distance \(kilometer)")
        
        var speed: Double = currentLocation.speed
        speed =  (speed * 3600 )/100
        //converting speed from m/s to knots(unit)
        print("Speed \(speed)")
        // distancelabl.text = "\(round(kilometer))Miles"
        distancelabl.text = "\(round(kilometer))\(Language_handler.VJLocalizedString("Miles", comment: nil))"

        
        
    }
    @IBAction func didclickgooglemap(sender: AnyObject) {
        
            
            let  startSelLocation :CLLocation = CLLocation.init(latitude:  getCurrentLocation.coordinate.latitude, longitude:  getCurrentLocation.coordinate.longitude)
            let dropSelLocation : CLLocation = CLLocation.init(latitude:trackingDetail.userLat, longitude: trackingDetail.userLong)
            if self.model == nil {
                self.model = MapRequestModel()
                // And let's set our callback URL right away!
                OpenInGoogleMapsController.sharedInstance().callbackURL = NSURL(string:kOpenGoogleMapScheme)
                OpenInGoogleMapsController.sharedInstance().fallbackStrategy = GoogleMapsFallback.ChromeThenAppleMaps
            }
            
            if OpenInGoogleMapsController.sharedInstance().googleMapsInstalled == false {
                print("Google Maps not installed, but using our fallback strategy")
            }
            if mapLaat != 0 {
                // [self.model useCurrentLocationForGroup:kLocationGroupStart];
                self.model.setQueryString(nil, center:startSelLocation.coordinate, forGroup: LocationGroup.Start)
                self.model.setQueryString(nil, center: dropSelLocation.coordinate, forGroup: LocationGroup.End)
                self.openDirectionsInGoogleMaps()
            }
            else {
                self.view.makeToast(message: "There is no destination selected")
            }
            
        
    }
    
    func  openDirectionsInGoogleMaps()  {
        
        let directionsDefinition = GoogleDirectionsDefinition()
        if self.model.startCurrentLocation {
            directionsDefinition.startingPoint = nil
        }
        else {
            let startingPoint = GoogleDirectionsWaypoint()
            startingPoint.queryString = self.model.startQueryString
            startingPoint.location = self.model.startLocation
            directionsDefinition.startingPoint = startingPoint
        }
        if self.model.destinationCurrentLocation {
            directionsDefinition.destinationPoint = nil
        }
        else {
            let destination = GoogleDirectionsWaypoint()
            destination.queryString = self.model.destinationQueryString
            destination.location = self.model.desstinationLocation
            directionsDefinition.destinationPoint = destination
        }
        directionsDefinition.travelMode = self.travelMode(asGoogleMapsEnum: self.model.travelMode)
        OpenInGoogleMapsController.sharedInstance().openDirections(directionsDefinition)
        
        
    }
    
    func travelMode(asGoogleMapsEnum appTravelMode: TravelMode) -> GoogleMapsTravelMode {
        switch appTravelMode {
        case TravelMode.Bicycling:
            return GoogleMapsTravelMode.Biking
        case TravelMode.Driving:
            return GoogleMapsTravelMode.Driving
        case TravelMode.PublicTransit:
            return GoogleMapsTravelMode.Transit
        case TravelMode.Walking:
            return GoogleMapsTravelMode.Walking
        case TravelMode.NotSpecified:
            return GoogleMapsTravelMode.init(rawValue: 0)!
        }
        
    }
    

    
    override func viewWillAppear(animated: Bool) {
        
        
    
        setDataToLocationView()
        
        
        
    }
    
       func set_mapView(loc:CLLocationCoordinate2D){
        
        let camera = GMSCameraPosition.cameraWithLatitude(loc.latitude,
                                                          longitude:loc.longitude, zoom:15)
        print("Current location lattitude  =\(loc.latitude) Longtitude =\(loc.longitude) ")
        
        mapView.camera=camera
        mapView.frame=mapView.frame
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.icon = UIImage(named: "StartPin")
        
        
        
        partnerMarker.position = camera.target
        partnerMarker.appearAnimation = kGMSMarkerAnimationPop
        partnerMarker.icon = UIImage(named: "CarIcon")
        partnerMarker.title = getUsername as String
        partnerMarker.map = mapView
        marker.map = mapView
        
        marker1.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        marker1.appearAnimation = kGMSMarkerAnimationPop
        marker1.icon = UIImage(named: "FinishPin")
        marker1.title = getUsername as String
        marker1.map = mapView
        mapView.settings.setAllGesturesEnabled(true)
        
        //Partner Location Moving
       
        
        //User Location Moving
        //        userMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        //        userMarker.appearAnimation = kGMSMarkerAnimationPop
        //        userMarker.icon = UIImage(named: "CarIcon")
        //        userMarker.title = getUsername as String
        //        userMarker.map = mapView
        //        mapView.settings.setAllGesturesEnabled(true)
        
    }
    
    func setAnimatedMapView(loc:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.cameraWithLatitude(loc.latitude,
                                                          longitude:loc.longitude, zoom:15)
        
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        partnerMarker.position = camera.target
        // userMarker.position = CLLocationCoordinate2DMake(trackingDetail.userLat, trackingDetail.userLong)
        CATransaction.commit()
        let user : CLLocation = CLLocation.init(latitude: trackingDetail.userLat, longitude: trackingDetail.userLong)
        let partner : CLLocation = CLLocation.init(latitude: loc.latitude, longitude: loc.longitude)
        bearing = getBearingBetweenTwoPoints1(user,locationB: partner)
        
               partnerMarker.rotation = lastDriving - bearing
    }
    
    
    func getBearingBetweenTwoPoints1( A: CLLocation, locationB B: CLLocation) -> Double {
        var dlon: Double = self.toRad((B.coordinate.longitude - A.coordinate.longitude))
        let dPhi: Double = log(tan(self.toRad(B.coordinate.latitude) / 2 + M_PI / 4) / tan(self.toRad(A.coordinate.latitude) / 2 + M_PI / 4))
        if fabs(dlon) > M_PI {
            dlon = (dlon > 0) ? (dlon - 2 * M_PI) : (2 * M_PI + dlon)
        }
        return self.toBearing(atan2(dlon, dPhi))
    }
    
    func toRad(degrees: Double) -> Double {
        return degrees * (M_PI / 180)
    }
    
    func toBearing(radians: Double) -> Double {
        return self.toDegrees(radians) + 360 % 360
    }
    
    func toDegrees(radians: Double) -> Double {
        return radians * 180 / M_PI
    }
    func setDataToLocationView(){
        
        addressLbl.text="\(addressStr)"
        lblStartLoc.text = self.theme.CheckNullValue(theme.getAddressForLatLng( "\(CurLaat)", longitude: "\(CurLong)", status :""))

        if(isShowArriveBtn==true){
            cancelBtn.hidden=false
            
            
            //stepProgress.currentIndex=1
        }else{
            
        }
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
       // self.dismissViewControllerAnimated(true, completion: nil)
    }
      func kilometersfromPlace(from: CLLocationCoordinate2D) -> Double {
        let userloc : CLLocation = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let dest:CLLocation = CLLocation(latitude: trackingDetail.userLat, longitude: trackingDetail.userLong)
        let dist: CLLocationDistance = userloc.distanceFromLocation(dest)
        
        let distanceKM = dist / 1000
        let roundedTwoDigit = round(100 * distanceKM) / 100
        //        return roundedTwoDigit
        //        let distance: NSString = "\(dist)"
        return roundedTwoDigit
    }

     func drawRoadRouteBetweenTwoPoints(loc:CLLocationCoordinate2D) {
        lblStartLoc.text = self.theme.getAddressForLatLng("\(loc.latitude)", longitude: "\(loc.longitude)", status: "")
        print("Current location of lattitude\(loc.latitude) and longtitude \(loc.longitude)")
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(loc.latitude),\(loc.longitude)&destination=\(trackingDetail.userLat),\(trackingDetail.userLong)&sensor=true&key=\(googleApiKey)"
        URL_Handler.makeGetCall(directionURL) { (responseObject) -> () in
            if(responseObject != nil)
            {
                print(responseObject)
                let routes_array = responseObject?.objectForKey("routes") as! NSArray
                
                if(routes_array.count > 0)
                {
                    self.DismissProgress()
                    self.isPolyLineDrawn=true
                    for Dict in routes_array
                    {
                        
                        let overviewPolyline = Dict.objectForKey("overview_polyline")!.objectForKey("points") as! String
                        let path:GMSPath=GMSPath(fromEncodedPath: overviewPolyline)
                        let SingleLine:GMSPolyline=GMSPolyline(path: path)
                        SingleLine.strokeWidth=10.0
                        SingleLine.strokeColor=PlumberBlueColor
                        SingleLine.map=self.mapView
                        
                        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(path: path)
                        let update: GMSCameraUpdate = GMSCameraUpdate.fitBounds(bounds, withPadding: 100.0)
                        //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
                        self.mapView.animateWithCameraUpdate(update)
                        
                    }
                    
                    self.set_mapView(loc)
                }else{
                    
                }
                
            }else{
                self.DismissProgress()
                self.isPolyLineDrawn=true
                self.view.makeToast(message:Language_handler.VJLocalizedString("no_route", comment: nil), duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("sorry", comment: nil))
            }
        }
    }
    
    //    func upDateLocation(){
    //
    //        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
    //        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
    //            "latitude":"\(lat)",
    //            "longitude":"\(lon)"]
    //        // print(Param)
    //
    //        url_handler.makeCall(updateProviderLocation, param: Param) {
    //            (responseObject, error) -> () in
    //
    //            if(error != nil)
    //            {
    //                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
    //            }
    //            else
    //            {
    //                if(responseObject != nil && responseObject?.count>0)
    //                {
    //                    let status:NSString=responseObject?.objectForKey("status") as! NSString
    //
    //                    if(status == "1")
    //                    {
    //
    //
    //                    }
    //                    else
    //                    {
    //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
    //                    }
    //                }
    //                else
    //                {
    //                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: "Network Failure !!!")
    //                }
    //            }
    //
    //        }
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didClickPhoneBtn(sender: AnyObject) {
        callNumber(phoneStr as String)
    }
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string:"tel://"+"\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
