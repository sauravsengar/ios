//
//  InitialViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/18/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var _pageViewController: UIPageViewController?
    var _pageTitles = ["Welcome", "Welcome", "Welcome", "Welcome"]
    let _pageImages = ["background", "background", "background", "background"]
    let _descImages = ["Plumb1", "Plumb2", "Plumb3", "Plumb4"]
    var _descMsgs = ["Our rich & responsive interface will give you the best user experience.", "Customers are looking to hire experienced professionals just like you !!", "Connect with customers to get hired and earn more money !!!", "Carry your ToolKit and help the people around the world !!!"]
   
    var lat:NSString!
    var lon:NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        _pageTitles = [Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil), Language_handler.VJLocalizedString("welcome", comment: nil)]
        loginBtn.setTitle(Language_handler.VJLocalizedString("sign_in", comment: nil), forState: UIControlState.Normal)
        //registerBtn.setTitle(Language_handler.VJLocalizedString("register", comment: nil), forState: UIControlState.Normal)
        registerBtn.setTitle(Language_handler.VJLocalizedString("register", comment: nil), forState: UIControlState.Normal)
        _descMsgs = [Language_handler.VJLocalizedString("desc_msgs1", comment: nil), Language_handler.VJLocalizedString("desc_msgs2", comment: nil), Language_handler.VJLocalizedString("desc_msgs3", comment: nil), Language_handler.VJLocalizedString("desc_msgs4", comment: nil)]
        loadSplashVideo()
        
    
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didClickLoginBtn(sender: AnyObject) {
        
        let loginController = self.storyboard!.instantiateViewControllerWithIdentifier("LoginVCSID") as! LoginViewController
        self.navigationController!.pushViewController(loginController, animated: true)
    }
    @IBAction func didClickRegisterBtn(sender: AnyObject) {
        
    }
    func loadSplashVideo(){
      
        
       
        
        _pageViewController = storyboard!.instantiateViewControllerWithIdentifier("WalkthroughPageView") as? UIPageViewController
        _pageViewController!.dataSource = self
        _pageViewController!.delegate = self
        
        let startingViewController = viewControllerAtIndex(0)
        let viewControllers = [startingViewController!]
        _pageViewController!.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        
        // Change the size of page view controller
        _pageViewController!.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        
        addChildViewController(_pageViewController!)
        view.addSubview(_pageViewController!.view)
        _pageViewController!.didMoveToParentViewController(self)
        
        registerBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        registerBtn.layer.cornerRadius=5;
        registerBtn.layer.shadowOpacity = 0.4
        registerBtn.layer.shadowRadius = 4
        
        loginBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        loginBtn.layer.cornerRadius=5;
        loginBtn.layer.shadowOpacity = 0.4
        loginBtn.layer.shadowRadius = 4
        self.view.bringSubviewToFront(registerBtn)
        self.view.bringSubviewToFront(loginBtn)
        
        
    }
    
    
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed) {
            let pageContentViewController = pageViewController.viewControllers![0] as! WalkthroughPageContentViewController
            let index = pageContentViewController.pageIndex
            let backgroundImageName = _pageImages[index] as NSString
            
            UIView.transitionWithView(self.backgroundImage, duration: 0.5, options: UIViewAnimationOptions.TransitionCrossDissolve,
                                      animations: { () -> Void in
                                        self.backgroundImage.image = UIImage(named: backgroundImageName as String)
                }, completion: { (Bool) -> Void in
                    
                }
            )
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]) {
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let pageContentViewController = viewController as! WalkthroughPageContentViewController
        var index = pageContentViewController.pageIndex;
        
        if ((index == 0) || (index == NSNotFound)) {
            return nil;
        }
        
        index--;
        return self.viewControllerAtIndex(index);
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let pageContentViewController = viewController as! WalkthroughPageContentViewController
        var index = pageContentViewController.pageIndex;
        
        if (index == NSNotFound) {
            return nil
        }
        
        index++;
        if (index == _pageTitles.count) {
            return nil
        }
        
        return viewControllerAtIndex(index);
    }
    
    func viewControllerAtIndex(index: NSInteger) -> UIViewController? {
        if ((_pageTitles.count == 0) || (index >= _pageTitles.count)) {
            return nil
        }
        
        let pageContentViewController = storyboard!.instantiateViewControllerWithIdentifier("WalkthroughPageContent") as! WalkthroughPageContentViewController
        pageContentViewController.titleText = _pageTitles[index] as NSString
        pageContentViewController.descText = _descMsgs[index] as NSString
        pageContentViewController.imgText = _descImages[index] as NSString
        pageContentViewController.pageIndex = index
        
        return pageContentViewController
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> NSInteger {
        return _pageTitles.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> NSInteger {
        return 0
    }
    override func viewWillAppear(animated: Bool) {
        
    }
    override func viewWillDisappear(animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
