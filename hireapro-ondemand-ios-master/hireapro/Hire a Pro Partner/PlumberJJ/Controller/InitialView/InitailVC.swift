//
//  InitailVC.swift
//  PlumberJJ
//
//  Created by Natarajan on 19/06/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class InitailVC: UIViewController, UIScrollViewDelegate  {
    @IBOutlet weak var register_Btn: UIButton!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var slideScroll: UIScrollView!
    @IBOutlet weak var slideImage: UIImageView!
    @IBOutlet weak var slideLable: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        login.setTitle(Language_handler.VJLocalizedString("sign_in", comment: nil), forState: .Normal)
register_Btn.setTitle(Language_handler.VJLocalizedString("register", comment: nil), forState: UIControlState.Normal)
//         slideLable.text = "Our rich & responsive interface will give you the best user experience."
        //1
        self.slideScroll.delegate = self
        self.slideScroll.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:slideScroll.frame.size.height)
        let slideScrollWidth:CGFloat = self.slideScroll.frame.width
       // let slideScrollHeight:CGFloat = self.slideScroll.frame.height
      
        let imgOne = UIImageView(frame: CGRect(x:0, y:-20,width:slideScroll.frame.size.width, height:self.slideScroll.frame.height+20))
        imgOne.image = UIImage(named: "Plumb1")
        let imgTwo = UIImageView(frame: CGRect(x:slideScrollWidth, y:-20,width:slideScroll.frame.size.width, height:self.slideScroll.frame.height+20))
        imgTwo.image = UIImage(named: "Plumb2")
        let imgThree = UIImageView(frame: CGRect(x:slideScrollWidth*2, y:-20,width:slideScroll.frame.size.width, height:self.slideScroll.frame.height+20))
        imgThree.image = UIImage(named: "Plumb3")
        let imgFour = UIImageView(frame: CGRect(x:slideScrollWidth*3, y:-20,width:slideScroll.frame.size.width, height:self.slideScroll.frame.height+20))
        imgFour.image = UIImage(named: "Plumb4")
     //   self.slideImage.frame = CGRect(x: 0, y:-20 , width: UIScreen.mainScreen().bounds.width, height: slideScroll.frame.size.height)
        self.slideScroll.addSubview(imgOne)
        self.slideScroll.addSubview(imgTwo)
        self.slideScroll.addSubview(imgThree)
        self.slideScroll.addSubview(imgFour)
        //4
        
        self.slideScroll.contentSize = CGSize(width:self.slideScroll.frame.width * 4, height:self.slideScroll.frame.height-20)
        self.slideScroll.delegate = self
        self.pageControl.currentPage = 0
        NSTimer.scheduledTimerWithTimeInterval( 4,  target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
       
    }
   
    @IBAction func didClickLoginBtn(sender: AnyObject) {
        
    }
    @IBAction func didClickRegisterBtn(sender: AnyObject) {
        
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
    
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((slideScroll.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(currentPage) == 0{
//            slideLable.text = "Our rich & responsive interface will give you the best user experience."
        }else if Int(currentPage) == 1{
//            slideLable.text = "Customers are looking to hire experienced professionals just like you !!"
        }else if Int(currentPage) == 2{
//            slideLable.text = "Connect with customers to get hired and earn more money !!!"
        }else{
//            slideLable.text = "Carry your ToolKit and help the people around the world !!!"
            // Show the "Let's Start" button in the last slide (with a fade in animation)                     UIView.animate(withDuration: 1.0, animations: { () -> Void in
//            self.startButton.alpha = 1.0
        }
    }


func moveToNextPage (){
    
    let pageWidth:CGFloat = self.slideScroll.frame.width
    let maxWidth:CGFloat = pageWidth * 4
    let contentOffset:CGFloat = self.slideScroll.contentOffset.x
    
    var slideToX = contentOffset + pageWidth
    
    if  contentOffset + pageWidth == maxWidth
    {
        slideToX = 0
    }
    self.slideScroll.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.slideScroll.frame.height), animated: true)
}

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if slideScroll.contentOffset.x < scrollView.contentOffset.x {
        
        }
        else{
            
        }
//        let pageWidth:CGFloat = scrollView.frame.width
//        let currentPage:CGFloat = floor((slideScroll.contentOffset.x-pageWidth/2)/pageWidth)+1
//        // Change the indicator
//        self.pageControl.currentPage = Int(currentPage);

    }
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((slideScroll.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
