//
//  BankingInfoViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/30/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class BankingInfoViewController: RootBaseViewController, UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var topView: SetColorView!
    @IBOutlet weak var bankingScrollView: UIScrollView!
    @IBOutlet weak var routingTxtField: UITextField!
    @IBOutlet weak var AccHolderNameTxtField: UITextField!
    
    @IBOutlet weak var AccHolderTxtView: UITextView!
    @IBOutlet weak var AccNumberTxtField: UITextField!
    @IBOutlet weak var BankNameTxtField: UITextField!
    @IBOutlet weak var BranchNameTxtField: UITextField!
    @IBOutlet weak var BranchAddressTxtView: UITextView!
    @IBOutlet weak var SwiftCodeTxtField: UITextField!
    @IBOutlet weak var lblHolderName: SMIconLabel!
    @IBOutlet weak var lblHolderAdd: SMIconLabel!
    @IBOutlet weak var lblHolderNum: SMIconLabel!
    @IBOutlet weak var lblBranchName: SMIconLabel!
    @IBOutlet weak var lblBankName: SMIconLabel!
    @IBOutlet weak var lblBranchAdd: SMIconLabel!
 
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var lblIfcCode: SMIconLabel!
    
    @IBOutlet weak var titleHeader: UILabel!

    @IBOutlet weak var routingLbl: SMIconLabel!
   // var theme:Theme=Theme()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHolderName.text = Language_handler.VJLocalizedString("account_holder_name", comment: nil)
        lblHolderAdd.text = Language_handler.VJLocalizedString("account_holder_address", comment: nil)
        lblHolderNum.text = Language_handler.VJLocalizedString("account_number", comment: nil)
        lblBranchName.text = Language_handler.VJLocalizedString("bank_name", comment: nil)
        lblBankName.text = Language_handler.VJLocalizedString("branch_name", comment: nil)
        lblBranchAdd.text = Language_handler.VJLocalizedString("branch_address", comment: nil)
        lblIfcCode.text = Language_handler.VJLocalizedString("ifsc_code", comment: nil)
        routingLbl.text = Language_handler.VJLocalizedString("routing_number", comment: nil)
        titleHeader.text = Language_handler.VJLocalizedString("banking_details", comment: nil)
        saveButton.setTitle(Language_handler.VJLocalizedString("save", comment: nil), forState: UIControlState.Normal)
        setViewForBanking()
        GetDatasForBanking()
        barButton.addTarget(self, action: #selector(BankingInfoViewController.openmenu), forControlEvents: .TouchUpInside)
       
        // Do any additional setup after loading the view.
    }
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    func setViewForBanking(){
        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.hidden=false;
            barButton.hidden=true
        }else{
         
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BankingInfoViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BankingInfoViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil)
        AccHolderTxtView.layer.borderWidth=1;
        AccHolderTxtView.layer.borderColor=PlumberLightGrayColor.CGColor;
        BranchAddressTxtView.layer.borderWidth=1;
        BranchAddressTxtView.layer.borderColor=PlumberLightGrayColor.CGColor;
        bankingScrollView.contentSize=CGSizeMake(bankingScrollView.frame.size.width, routingTxtField.frame.origin.y+50);
        setMandatoryFields()
    }
    
    func GetDatasForBanking(){
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        
        url_handler.makeCall(GetBankingDetails, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil)
              {

                
                    self.setDatasToBankingView(responseObject!)
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
        }
    }
    @IBAction func didClickSaveBtn(sender: AnyObject) {
        if(validateTxtFields()){
            showProgress()
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "acc_holder_name":"\(AccHolderNameTxtField.text!)",
                "acc_holder_address":"\(AccHolderTxtView.text!)",
                "acc_number":"\(AccNumberTxtField.text!)",
                "bank_name":"\(BankNameTxtField.text!)",
                "branch_name":"\(BranchNameTxtField.text!)",
                "branch_address":"\(BranchAddressTxtView.text!)",
                "swift_code":"\(SwiftCodeTxtField.text!)",
                "routing_number":"\(routingTxtField.text!)"]
            
            url_handler.makeCall(SaveBankingDetails, param: Param) {
                (responseObject, error) -> () in
                self.DismissProgress()
                if(error != nil)
                {
                    print("error responded with: \(error)")
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
                else
                {
                    if(responseObject != nil)
                    {
                        
                      
                        let resDict: NSDictionary = responseObject!.objectForKey("response") as! NSDictionary
            self.view.makeToast(message:self.theme.CheckNullValue(resDict.objectForKey("message"))!, duration: 5, position: HRToastPositionDefault, title: "\(appNameJJ)")

                        self.setDatasToBankingView(responseObject!)
                        
                                                                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
            }
        }
    }
    func setDatasToBankingView(Dict:NSDictionary){
        let status:NSString=self.theme.CheckNullValue(Dict["status"])!
        
        if(status == "1")
        {
             let resDict: NSDictionary = Dict.objectForKey("response")?.objectForKey("banking") as! NSDictionary
            AccHolderNameTxtField.text=theme.CheckNullValue(resDict.objectForKey( "acc_holder_name" )as! NSString as String)
            AccHolderTxtView.text=theme.CheckNullValue(resDict.objectForKey( "acc_holder_address" ) as! NSString as String)
            AccNumberTxtField.text=theme.CheckNullValue(resDict.objectForKey( "acc_number" ) as! NSString as String)
            BankNameTxtField.text=theme.CheckNullValue(resDict.objectForKey( "bank_name" ) as! NSString as String)
            BranchNameTxtField.text=theme.CheckNullValue(resDict.objectForKey( "branch_name" ) as! NSString as String)
            BranchAddressTxtView.text=theme.CheckNullValue(resDict.objectForKey( "branch_address" ) as! NSString as String)
           
            SwiftCodeTxtField.text=theme.CheckNullValue(resDict.objectForKey( "swift_code" ) as! NSString as String)
            routingTxtField.text=theme.CheckNullValue(resDict.objectForKey( "routing_number" )as! NSString as String)
            
            
                   }
        else
        {
           self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
        }
    }

    @IBAction func didClickBackBtn(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
  

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setMandatoryFields(){
        for subView:UIView in bankingScrollView.subviews{
            if(subView.isKindOfClass(SMIconLabel)){
                 let lbl: SMIconLabel = (subView as? SMIconLabel)!
                if lbl.isEqual(lblIfcCode) || lbl.isEqual(routingLbl) {
                   
                }
                else {
                    lbl.icon = UIImage(named: "MandatoryImg")
                    lbl.iconPadding = 5
                    lbl.iconPosition = SMIconLabelPosition.Right
                }
               
            }else if(subView.isKindOfClass(UITextField)){
                let txtField: UITextField = (subView as? UITextField)!
                let arrow: UIView = UILabel()
                arrow.frame = CGRectMake(0, 0, 20, 20)
                txtField.leftView = arrow
                txtField.leftViewMode = UITextFieldViewMode.Always
            }
        }
    }

    func validateTxtFields () -> Bool{
        var isOK:Bool=true
        if(AccHolderNameTxtField.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("holder_name_mand", comment: nil))
            isOK=false
        }else if(AccHolderTxtView.text.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("address_mand", comment: nil))
            isOK=false
        }
        else if(AccNumberTxtField.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("account_number_mand", comment: nil))
            isOK=false
        }
            
        else if(BankNameTxtField.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("bank_name_mand", comment: nil))
            isOK=false
        }
            
        else if(BranchNameTxtField.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("branch_name_mand", comment: nil))
            isOK=false
        }
            
        else if(BranchAddressTxtView.text!.characters.count==0){
            ValidationAlert(Language_handler.VJLocalizedString("branch_address_mand", comment: nil))
            isOK=false
        }
//        else if(SwiftCodeTxtField.text!.characters.count==0){
//            ValidationAlert(Language_handler.VJLocalizedString("ifsc_code_mand", comment: nil))
//            isOK=false
//        }
        return isOK
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = bankingScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+30
        bankingScrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        bankingScrollView.contentInset = contentInset
    }
    
    func ValidationAlert(alertMsg:NSString){
        let popup = NotificationAlertView.popupWithText("\(alertMsg)")
        popup.hideAfterDelay = 3
        //popup.position = NotificationAlertViewPosition.Bottom
        popup.animationDuration = 1
        popup.show()
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(range.location==0 && string==" "){
            return false
        }
        
        if string == "."{
            return false
        }
        return true
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(range.location==0 && text==" "){
            return false
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
