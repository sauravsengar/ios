//
//  BarChartViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/26/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class BarChartViewController: RootBaseViewController {
var barController:StackedBarsExample=StackedBarsExample()
   // var theme:Theme=Theme()
    var barArr:NSMutableArray=[]
    var barTitleArr:NSMutableArray=[]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgress()
        getDataForBarChart()
        
        // Do any additional setup after loading the view.
    }
    func getDataForBarChart(){
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
         print("new param",Param)
        print(EarningStatsUrl)
        url_handler.makeCall(EarningStatsUrl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        if(responseObject?.objectForKey("response")?.objectForKey("earnings")!.count>=6){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("earnings") as! NSArray
                            let caption:NSString=responseObject?.objectForKey("response")?.objectForKey("unit") as! NSString
                            for element in listArr {
                                let str:NSString = element.objectForKey("month")as! NSString
                                let str1:NSString = self.theme.CheckNullValue(element.objectForKey("amount"))!
                                
                                self.barArr.addObject(str1)
                                self.barTitleArr.addObject(str)
                            }
                    let str3:NSString = self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("interval"))!
                            
                    let str4:NSString = self.theme.CheckNullValue(responseObject?.objectForKey("response")?.objectForKey("max_earnings"))!
                            var inter = Int(str3 as String)
                            if(inter==0){
                              inter=1
                            }
                            
                            var max=Int(str4 as String)
                            if(max==0){
                              max=1
                            }
                             let cCode1=responseObject?.objectForKey("response")?.objectForKey("currency_code") as! NSString
                            let cCode=self.theme.getCurrencyCode(cCode1 as String)
                            self.addChildViewController(self.barController)
                            self.barController.interval=inter
                            self.barController.maxEarnings=max
                            self.barController.currencyCode="\(cCode) \(caption)"
                            self.barController.barArrSet=self.barArr
                            self.barController.barTitleArrset=self.barTitleArr
                            self.barController.showChart(horizontal: false)
                            self.view.addSubview(self.barController.view)
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
