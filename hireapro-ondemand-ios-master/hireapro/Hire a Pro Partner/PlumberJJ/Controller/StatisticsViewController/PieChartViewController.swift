//
//  PieChartViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/26/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class PieChartViewController: RootBaseViewController, MDRotatingPieChartDelegate, MDRotatingPieChartDataSource {
   
    var slicesData:Array<Data> = Array<Data>()
    
    
    @IBOutlet weak var lblClosedJob: UILabel!
    @IBOutlet weak var lblOnGoing: UILabel!
    @IBOutlet weak var lblUserCanceled: UILabel!
    @IBOutlet weak var lblTotlalJob: UILabel!
    
    @IBOutlet weak var lblCancelledByMe: UILabel!

    @IBOutlet weak var hintView: UIView!
    var pieChart:MDRotatingPieChart!

  //  var theme:Theme=Theme()
     var pieArr:NSMutableArray=[]
    var pieTitleArr:NSMutableArray=[]
    override func viewDidLoad() {
        super.viewDidLoad()
       
        loadPieChart()
        getDataForPieChart()
        // Do any additional setup after loading the view.
    }
    
    func loadPieChart(){
       
        

    }
    
     override func applicationLanguageChangeNotification(notification: NSNotification) {
        
        lblClosedJob.text = Language_handler.VJLocalizedString("closed_jobs", comment: nil);
        lblOnGoing.text = Language_handler.VJLocalizedString("ongoing_jobs", comment: nil);
        lblUserCanceled.text = Language_handler.VJLocalizedString("user_cancelled_job", comment: nil);
        lblTotlalJob.text = Language_handler.VJLocalizedString("total_jobs", comment: nil);
        lblCancelledByMe.text = Language_handler.VJLocalizedString("job_cancelled_me", comment: nil);
        
    }

    func didOpenSliceAtIndex(index: Int) {
        print("Open slice at \(index)")
    }
    
    func didCloseSliceAtIndex(index: Int) {
        print("Close slice at \(index)")
    }
    
    func willOpenSliceAtIndex(index: Int) {
        print("Will open slice at \(index)")
    }
    
    func willCloseSliceAtIndex(index: Int) {
        print("Will close slice at \(index)")
    }
    
    //Datasource
    func colorForSliceAtIndex(index:Int) -> UIColor {
        return slicesData[index].color
    }
    
    func valueForSliceAtIndex(index:Int) -> CGFloat {
        return slicesData[index].value
    }
    
    func labelForSliceAtIndex(index:Int) -> String {
        return slicesData[index].label
    }
    
    func numberOfSlices() -> Int {
        return slicesData.count
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       // refresh()
    }
    
    func refresh()  {
        pieChart.build()
    }
   
    func getDataForPieChart(){
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
         print(Param)
        print(JobsStatsUrl)
        
        url_handler.makeCall(JobsStatsUrl, param: Param) {
            (responseObject, error) -> () in
           
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                          let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("jobs") as! NSArray
                            for element  in listArr{
                                 let str:NSString = element.objectForKey("title")as! NSString
                                 let str1:NSString = element.objectForKey("jobs_count")as! NSString
                                
                                self.pieArr.addObject(str1)
                                self.pieTitleArr.addObject(str)
                            }

                          self.loaddataInPie(self.pieArr, pieTitleArray:  self.pieTitleArr)
                        
                    }
                    else
                    {
                        
                let errormsg : String = self.theme.CheckNullValue(responseObject?.objectForKey("response"))!
                        self.view.makeToast(message:errormsg, duration: 5, position: HRToastPositionDefault, title: "\(appNameJJ)")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
    func loaddataInPie(pieArray:NSMutableArray ,pieTitleArray:NSMutableArray){
        pieChart = MDRotatingPieChart(frame: CGRectMake(0, 0, view.frame.width, view.frame.width))

            self.slicesData = [
                Data(myValue: CGFloat((pieArr.objectAtIndex(0) as! NSString).floatValue), myColor: UIColor(red: (255/255), green: (55/255), blue: (56/255), alpha: 1), myLabel:pieTitleArr.objectAtIndex(0) as! String),
                Data(myValue: CGFloat((pieArr.objectAtIndex(1) as! NSString).floatValue), myColor: UIColor(red: (77/255), green: (206/255), blue: (255/255), alpha: 1), myLabel:pieTitleArr.objectAtIndex(1) as! String),
                Data(myValue: CGFloat((pieArr.objectAtIndex(2) as! NSString).floatValue), myColor: UIColor(red: (132/255), green: (194/255), blue:(37/255), alpha: 1), myLabel:pieTitleArr.objectAtIndex(2) as! String),
                Data(myValue: CGFloat((pieArr.objectAtIndex(3) as! NSString).floatValue), myColor: UIColor(red: (255/255), green: (81/255), blue:(243/255), alpha: 1), myLabel:pieTitleArr.objectAtIndex(3) as! String),
                Data(myValue: CGFloat((pieArr.objectAtIndex(4) as! NSString).floatValue), myColor: UIColor(red: (255/255), green:(197/255), blue:(148/255), alpha: 1), myLabel:pieTitleArr.objectAtIndex(4) as! String)]
        
       
        
        pieChart.delegate = self
        pieChart.datasource = self

        view.addSubview(pieChart)
          refresh()
       /* var properties = Properties()
        
        properties.smallRadius = 50
        properties.bigRadius = 120
        properties.expand = 25
        
        
        properties.displayValueTypeInSlices = .Percent
        properties.displayValueTypeCenter = .Label
        
        properties.fontTextInSlices = UIFont(name: "Arial", size: 12)!
        properties.fontTextCenter = UIFont(name: "Arial", size: 10)!
        
        properties.enableAnimation = true
        properties.animationDuration = 0.5
        
        
        let nf = NSNumberFormatter()
        nf.groupingSize = 3
        nf.maximumSignificantDigits = 2
        nf.minimumSignificantDigits = 2
        
        properties.nf = nf
        
        pieChart.properties = properties
        pieChart.build()
        hintView.hidden=false
        self.view.bringSubviewToFront(hintView)*/
        
    }
    class Data {
        var value:CGFloat
        var color:UIColor = UIColor.grayColor()
        var label:String = ""
        
        init(myValue:CGFloat, myColor:UIColor, myLabel:String) {
            value = myValue
            color = myColor
            label = myLabel
        }
    }
}
