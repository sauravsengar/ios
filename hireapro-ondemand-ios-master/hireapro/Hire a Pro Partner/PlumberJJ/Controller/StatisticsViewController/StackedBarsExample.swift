//
//  StackedBarsExample.swift
//  Examples
//
//  Created by ischuetz on 15/05/15.
//  Copyright (c) 2015 ivanschuetz. All rights reserved.
//

import UIKit



class StackedBarsExample: RootBaseViewController {
    var barArrSet:NSMutableArray=[]
    var barTitleArrset:NSMutableArray=[]
    var maxEarnings:NSInteger!
    var interval:NSInteger!
    var currencyCode:NSString!
    private var chart: Chart? // arc
    
    let sideSelectorHeight: CGFloat = 50

    private func chart(horizontal horizontal: Bool) -> Chart {
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
       
        let color2 = PlumberThemeColor.colorWithAlphaComponent(0.8)
       
        
        let zero = ChartAxisValueDouble(0)
        let barModels = [
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(5) as! String, order: 1, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(5) as! NSString).doubleValue, bgColor: color2)
                ]),
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(4) as! String, order: 2, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(4) as! NSString).doubleValue, bgColor: color2)
                ]),
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(3) as! String, order: 3, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(3) as! NSString).doubleValue, bgColor: color2)
                ]),
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(2) as! String, order: 4, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(2) as! NSString).doubleValue, bgColor: color2)
                ]),
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(1) as! String, order: 5, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(1) as! NSString).doubleValue, bgColor: color2)
                ]),
            ChartStackedBarModel(constant: ChartAxisValueString(barTitleArrset.objectAtIndex(0) as! String, order: 6, labelSettings: labelSettings), start: zero, items: [
                ChartStackedBarItemModel(quantity: (barArrSet.objectAtIndex(0) as! NSString).doubleValue, bgColor: color2)
                ])
        ]
        
        let (axisValues1, axisValues2) = (
            0.stride(through: maxEarnings, by: interval).map {ChartAxisValueFloat(CGFloat($0), labelSettings: labelSettings)},
            [ChartAxisValueString("", order: 0, labelSettings: labelSettings)] + barModels.map{$0.constant} + [ChartAxisValueString("", order: 7, labelSettings: labelSettings)]
        )
        let (xValues, yValues) = horizontal ? (axisValues1, axisValues2) : (axisValues2, axisValues1)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: Language_handler.VJLocalizedString("months", comment: nil), settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "\(Language_handler.VJLocalizedString("earning_in", comment: nil)) \(currencyCode)", settings: labelSettings.defaultVertical()))
        
        let frame = ExamplesDefaults.chartFrame(self.view.bounds)
        let chartFrame = self.chart?.frame ?? CGRectMake(frame.origin.x, frame.origin.y-60, frame.size.width, frame.size.height-70)
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: ExamplesDefaults.chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        let chartStackedBarsLayer = ChartStackedBarsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, barModels: barModels, horizontal: horizontal, barWidth: 30, animDuration: 0.5)
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.blackColor(), linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
        
        return Chart(
            frame: chartFrame,
            layers: [
                xAxis,
                yAxis,
                guidelinesLayer,
                chartStackedBarsLayer
            ]
        )
    }
    
     func showChart(horizontal horizontal: Bool) {
        self.chart?.clearView()
        
        let chart = self.chart(horizontal: horizontal)
        self.view.addSubview(chart.view)
        self.chart = chart
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.showChart(horizontal: false)
       
    }
    
    class DirSelector: UIView {
        
        let horizontal: UIButton
        let vertical: UIButton
        
        weak var controller: StackedBarsExample?
        
        private let buttonDirs: [UIButton : Bool]
        
        init(frame: CGRect, controller: StackedBarsExample) {
            
            self.controller = controller
            
            self.horizontal = UIButton()
            self.horizontal.setTitle("Horizontal", forState: .Normal)
            self.vertical = UIButton()
            self.vertical.setTitle("Vertical", forState: .Normal)
            
            self.buttonDirs = [self.horizontal : true, self.vertical : false]
            
            super.init(frame: frame)
            
            self.addSubview(self.horizontal)
            self.addSubview(self.vertical)
            
            for button in [self.horizontal, self.vertical] {
                button.titleLabel?.font = ExamplesDefaults.fontWithSize(14)
                button.setTitleColor(UIColor.blueColor(), forState: .Normal)
                button.addTarget(self, action: "buttonTapped:", forControlEvents: .TouchUpInside)
            }
        }
        
        func buttonTapped(sender: UIButton) {
            let horizontal = sender == self.horizontal ? true : false
            controller?.showChart(horizontal: horizontal)
        }
        
        override func didMoveToSuperview() {
            let views = [self.horizontal, self.vertical]
            for v in views {
                v.translatesAutoresizingMaskIntoConstraints = false
            }
            
            let namedViews = views.enumerate().map{index, view in
                ("v\(index)", view)
            }
            
            let viewsDict = namedViews.reduce(Dictionary<String, UIView>()) {(var u, tuple) in
                u[tuple.0] = tuple.1
                return u
            }
            
            let buttonsSpace: CGFloat = Env.iPad ? 20 : 10
            
            let hConstraintStr = namedViews.reduce("H:|") {str, tuple in
                "\(str)-(\(buttonsSpace))-[\(tuple.0)]"
            }
            
            let vConstraits = namedViews.flatMap {NSLayoutConstraint.constraintsWithVisualFormat("V:|[\($0.0)]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)}
            
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(hConstraintStr, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
                + vConstraits)
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}