//
//  ChangePasswordViewController.swift
//  Plumbal
//
//  Created by Casperon Tech on 12/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

class ChangePasswordViewController: RootBaseViewController {
    
    @IBOutlet var Done_Btn: UIButton!
    @IBOutlet var Change_Pass_ScrollView: UIScrollView!
        @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var old_TextField: UITextField!

    @IBOutlet var New_TextField: UITextField!
    @IBOutlet var Confirm_textField: UITextField!
    
    @IBOutlet var Back_But: UIButton!
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)

        
    }

    
    let themes:Theme=Theme()
    
    var URL_handler:URLhandler=URLhandler()
    override func viewDidLoad() {
        
        titleHeader.text = Language_handler.VJLocalizedString("change_password", comment: nil)
        Done_Btn.setTitle(Language_handler.VJLocalizedString("done", comment: nil), forState: UIControlState.Normal)
        old_TextField.placeholder = Language_handler.VJLocalizedString("old_password", comment: nil)
        New_TextField.placeholder = Language_handler.VJLocalizedString("new_password", comment: nil)
        Confirm_textField.placeholder = Language_handler.VJLocalizedString("confirm_password", comment: nil)
        if (self.navigationController!.viewControllers.count != 1) {
            menuButton.hidden=true
        }else{
        }
        
        menuButton.addTarget(self, action: #selector(ChangePasswordViewController.openmenu), forControlEvents: .TouchUpInside)


        super.viewDidLoad()
        
        

        

old_TextField.delegate=self
Confirm_textField.delegate=self
New_TextField.delegate=self
        
        let tapgesture:UITapGestureRecognizer=UITapGestureRecognizer(target:self, action: #selector(ChangePasswordViewController.DismissKeyboard(_:)))
        
        view.addGestureRecognizer(tapgesture)


        

        // Do any additional setup after loading the view.
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }

    
    func DismissKeyboard(sender:UITapGestureRecognizer)
    {
        
        Change_Pass_ScrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        
        
        
        view.endEditing(true)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func didClickoptions(sender: AnyObject) {
        
        if(sender.tag == 0)
        {
            
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        }
        if(sender.tag == 1)
        {
            
            if(old_TextField.text == "")
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("old_password_empty", comment: nil), duration: 3, position: HRToastPositionCenter, title: appNameJJ)
                
                
            }
            else if(New_TextField.text == "")
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("new_password_empty", comment: nil), duration: 3, position: HRToastPositionCenter, title: appNameJJ)
                
                
            }
            else if (Confirm_textField.text == "")
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("confirm_password_empty", comment: nil), duration: 3, position: HRToastPositionCenter, title: appNameJJ)
                
                
            }
                
            else if(New_TextField.text != Confirm_textField.text)
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("password_unmatch", comment: nil), duration: 3, position: HRToastPositionCenter, title: "")
                
                
                
            }
            else
                
            {
                self.ChangePassword()
            }
            
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func ChangePassword()
    {
      
        self.showProgress()
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let providerid : NSString = objUserRecs.providerId

        let param=["provider_id":"\(providerid)","password":"\(old_TextField.text!)","new_password":"\(New_TextField.text!)"]
        
        URL_handler.makeCall(ChangepasswdUrl, param: param, completionHandler: { (responseObject, error) -> () in
            self.Done_Btn.enabled=true
            self.DismissProgress()
            
            
            if(error != nil)
            {
               self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionCenter, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
                
            else
            {
                
                
                
                if(responseObject != nil)
                {
                    let dict:NSDictionary=responseObject!
                    
                    let responsemsg : NSString = self.theme.CheckNullValue(dict.objectForKey("response"))!
                    let status : NSString = self.theme.CheckNullValue(dict.objectForKey("status"))!
                   
                    
                    if(status == "1")
                    {
                        
                        
                         self.view.makeToast(message:responsemsg as String, duration: 3, position: HRToastPositionCenter, title: "Hurray")
                        
                        
//                        let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatVCSID") as! MessageViewController
//                        objChatVc.jobId=chatlist.chatJobId
//                        self.navigationController!.pushViewController(objChatVc, animated: true)

            let loginController = self.storyboard!.instantiateViewControllerWithIdentifier("LoginVCSID") as! LoginViewController
                      
                        
  self.navigationController!.pushViewController(loginController, animated: true)
                        self.old_TextField.text = ""
                        self.New_TextField.text = ""
                        self.Confirm_textField.text = ""
                        
                       
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message:responsemsg as String, duration: 3, position: HRToastPositionCenter, title: "")

                        
                    
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionCenter, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    
                }
                
            }
            
            
            
        })
        
        
    }


}


extension ChangePasswordViewController:UINavigationControllerDelegate
{
    
}
extension ChangePasswordViewController:UITextFieldDelegate
{
    
}
