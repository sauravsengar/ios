//
//  HomeViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import CoreLocation
import MessageUI


class HomeViewController: RootBaseViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet var chaticon: UIButton!
    @IBOutlet var availability: UISwitch!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var panicBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var topView: SetColorView!
   
    @IBOutlet weak var bgImgView: UIImageView!
    
    @IBOutlet weak var lblStatistic: UILabel!
    @IBOutlet weak var lblMyLeads: UILabel!
    @IBOutlet weak var lblMyJob: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    
    @IBOutlet var AppName: UILabel!
    
    
    // var theme:Theme=Theme()
    var lat:NSString!
    var lon:NSString!
    var available_status : String = ""
    var GetReceipientMail : NSString = ""
    
    
    
    @IBAction func didClickMyOrdersBtn(sender: UIButton) {
        
        let objMyJobsVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyJobsVCSID") as! MyJobsViewController
        self.navigationController!.pushViewController(objMyJobsVc, animated: true)
        
    }
    
    @IBAction func didClickChatBtn(sender: AnyObject) {
        let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatListVcSID") as! ChatListViewController
        self.navigationController!.pushViewController(objChatVc, animated: true)
    }
    
    
    @IBAction func didClickSupport(sender: AnyObject) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["\(self.GetReceipientMail)"])
        mailComposerVC.setSubject(appNameJJ)
        
        
        return mailComposerVC
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func  getAppinformation()
    {
        
        let URL_Handler:URLhandler=URLhandler()
        URL_Handler.makeCall(Appinfo_url, param: [:]) {
            (responseObject, error) -> () in
            
            if(error != nil)
            {
                
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        
                        self.GetReceipientMail = self.theme.CheckNullValue(responseObject?.objectForKey("email_address"))!
                        
                        
                    }
                    else
                    {
                    }
                    
                    
                }
            }
        }
        
    }
    
    
    
    
    func showSendMailErrorAlert() {
        
        self.theme.AlertView(Language_handler.VJLocalizedString("not_send_email_title", comment: nil), Message: Language_handler.VJLocalizedString("not_send_email", comment: nil), ButtonTitle: kOk)
        
        
    }
    
    @IBAction func didClickStatisticsBtn(sender: UIButton) {
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("StatisticsVCSID") as! StatisticsViewController
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    @IBAction func didClickNotificationBtn(sender: UIButton) {
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("NewLeadsVCSID") as! NewLeadsViewController
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.UpdateAvailabilityStatus()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAppinformation()
        AppName.text = Language_handler.VJLocalizedString("app_name", comment: nil)
        
        menuButton.addTarget(self, action: #selector(HomeViewController.openmenu), forControlEvents: .TouchUpInside)
        
        
        homeScrollView.contentSize=CGSizeMake(homeScrollView.frame.size.width, bottomView.frame.origin.y + bottomView.frame.size.height + 50)
        
        
        /*  let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.ExtraLight)
         let blurView = UIVisualEffectView(effect: blurEffect)
         blurView.frame = CGRectMake(topView.frame.origin.x, 0, bgImgView.frame.size.width, homeScrollView.frame.size.height);
         self.bgImgView.addSubview(blurView)
         if theme.getAvailable_satus() == ""
         {
         //self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
         availability.on = false
         }
         else
         {
         if theme.getAvailable_satus() == "GO ONLINE"
         {
         //  self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
         availability.on = false
         }
         else
         {
         availability.on = true
         //self.avialabilitybtn.setTitle("GO OFFLINE", forState:.Normal)
         }
         }*/
    }
    
    override func applicationLanguageChangeNotification(notification: NSNotification) {
        
        lblMyJob.text = Language_handler.VJLocalizedString("my_jobs", comment: nil)
        lblStatistic.text = Language_handler.VJLocalizedString("statistics", comment: nil)
        lblMyLeads.text = Language_handler.VJLocalizedString("new_leads", comment: nil)
        lblSupport.text = Language_handler.VJLocalizedString("support", comment: nil)
        
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
   
    
    
    
    func UpdateAvailabilityStatus(){
        
        
               
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(GettingAvailablty, param: Param) {
            (responseObject, error) -> () in
            
            
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    let CheckAvailability=self.theme.CheckNullValue(responseObject?.objectForKey("availability"))!
                    if(status == "1")
                    {
                        if CheckAvailability ==  "1"
                            
                        {
                            self.availability.setOn(true, animated: true)
                        }
                        else
                        {

                            self.availability.setOn(false, animated: true)
                            
                        }
                        
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
        }
        
    }
    
    
    @IBAction func didclickAvailablityStatus(sender: UISwitch) {
        
        if sender.on {
            available_status = "1"
        } else {
            available_status = "0"
        }
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","availability" :available_status]
        // print(Param)
        
        url_handler.makeCall(EnableAvailabilty, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    if(status == "1")
                    {
                        let resDict: NSDictionary = responseObject?.objectForKey("response") as! NSDictionary
                        let tasker_status : String = self.theme.CheckNullValue(resDict.objectForKey("tasker_status"))!
                        
                        
                        if (tasker_status == "1")
                        {
                            self.view.makeToast(message:"Your Availability Is ON", duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")

                            self.availability.on = true
                            // self.avialabilitybtn.setTitle("GO OFFLINE", forState:.Normal)
                            self.theme.saveAvailable_satus("GO OFFLINE")
                            
                            
                        }
                        else
                        {self.availability.on = false
                            self.view.makeToast(message:"Your Availability Is OFF", duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")

                            //  self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
                            self.theme.saveAvailable_satus("GO ONLINE")
                            
                            
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                    
                    
                }
            }
        }
        
        
    }
    
    
    @IBAction func didclickavailability(sender: AnyObject) {
        
        
        if  sender.titleLabel?!.text == "GO ONLINE"
        {
            available_status = "1"
        }
        else
        {
            available_status = "0"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
