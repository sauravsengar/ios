//
//  RegisterViewController.swift
//  PlumberJJ
//
//  Created by Aravind Natarajan on 30/12/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class RegisterViewController: RootBaseViewController,UIWebViewDelegate {
    var theBool: Bool=Bool()
    var myTimer: NSTimer=NSTimer()
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var exit: UIButton!
    @IBOutlet var Webload_progress: UIProgressView!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
         
        super.viewDidLoad()
        let url = NSURL(string:RegUrl )//RegUrl
        
        Webload_progress.tintColor=theme.ThemeColour()
        NSLog("the register url =%@",url!)
        let request = NSURLRequest(URL: url!)
        self.Webload_progress.progress = 0.0
        webView.delegate=self
        
        let transform:CGAffineTransform = CGAffineTransformMakeScale(1.0, 2.0);
        Webload_progress.transform = transform;
        
        webView.scalesPageToFit = true
        webView.loadRequest(request)
        //Swift 2.2 selector syntax
        //Swift <2.2 selector syntax
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func applicationLanguageChangeNotification(notification: NSNotification) {
        
        titleHeader.text = Language_handler.VJLocalizedString("register", comment: nil)
        exit.setTitle(Language_handler.VJLocalizedString("exit", comment: nil), forState: .Normal)
    }
    
    // must be internal or public.
    func update() {
        // Something cool
        let appDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
        let loginController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InitialVCSID")
        //or the homeController
        let navController = UINavigationController(rootViewController: loginController)
        appDelegate.window!.rootViewController! = navController
        loginController.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print("Error  : \(error)");
    }
    
    
    func webViewDidStartLoad(webView: UIWebView) {
        funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        funcToCallCalledWhenUIWebViewFinishesLoading()
        
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.theBool = false
        self.myTimer = NSTimer.scheduledTimerWithTimeInterval(0.01667, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.Webload_progress.progress >= 1 {
                self.Webload_progress.hidden = true
                //ew3 self.myTimer.invalidate()
            } else {
                self.Webload_progress.progress += 0.1
            }
        } else {
            self.Webload_progress.progress += 0.05
            if self.Webload_progress.progress >= 0.95 {
                self.Webload_progress.progress = 0.95
            }
        }
    }
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        let alert = UIAlertController(title: Language_handler.VJLocalizedString("app_name", comment: nil), message: Language_handler.VJLocalizedString("Are_You_Sure_You_Want_To_Exit?", comment: nil), preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("cancel", comment: nil), style: UIAlertActionStyle.Default, handler: nil))
        let defaultAction = UIAlertAction(title: Language_handler.VJLocalizedString("ok", comment: nil), style: .Default, handler: {action in
            self.navigationController?.popViewControllerAnimated(true)
        })
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)

    }
    
    func didclickok() {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let URL:NSString=(request.URL?.absoluteString)!
        NSLog("get url string =%@", URL)
        
        if(URL.containsString("/provider/register/success"))
            
        {
            self.theme.AlertView("", Message: "\(Language_handler.VJLocalizedString("signup_thanku", comment: nil)) \(appNameJJ) \(Language_handler.VJLocalizedString("signup_thanku1", comment: nil))", ButtonTitle:kOk)
            
        
            var timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: #selector(RegisterViewController.update), userInfo: nil, repeats: false)
            
        }
        else if(URL.containsString("/provider/register/cancel"))
        {
            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
        }
        else if (URL.containsString("/provider/register/failed"))
        {
            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
