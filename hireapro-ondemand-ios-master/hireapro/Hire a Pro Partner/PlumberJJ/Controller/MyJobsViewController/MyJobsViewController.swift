//
//  MyJobsViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/20/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class MyJobsViewController: RootBaseViewController,PopupSortingViewControllerDelegate,MyOrderOpenDetailViewControllerDelegate {
     @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topView: SetColorView!
    @IBOutlet weak var titleHeader: UILabel!
    
    @IBOutlet weak var lblFilter: UIButton!
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad();
        titleHeader.text = Language_handler.VJLocalizedString("my_jobs", comment: nil)
        lblFilter.setTitle(Language_handler.VJLocalizedString("filter", comment: nil), forState: UIControlState.Normal)
                }
    
    override func viewWillAppear(animated: Bool) {
        
        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.hidden=false;
            menuButton.hidden=true
        }else{
        }
        let swiftPagesView : SwiftPages!
        swiftPagesView = SwiftPages(frame: CGRectMake(0, 0, containerView.frame.width, containerView.frame.height))
        
        //Initiation
        let VCIDs : [String] = ["MyOrdersVCSID", "JobsClosedVCSID", "JobsCancelledVCSID"]
        let titles : [String] = [Language_handler.VJLocalizedString("open", comment: nil), Language_handler.VJLocalizedString("completed", comment: nil), Language_handler.VJLocalizedString("cancelled", comment: nil)]
        
        //Sample customization
        swiftPagesView.initializeWithVCIDsArrayAndButtonTitlesArray(VCIDs, buttonTitlesArray: titles)
        swiftPagesView.setTopBarBackground(PlumberLightGrayColor)
        swiftPagesView.setAnimatedBarColor(PlumberThemeColor)
        
        containerView.addSubview(swiftPagesView)
         NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyJobsViewController.MoveToDetail(_:)), name: kMyLeadsNotif, object: nil)
        // Do any additional setup after loading the view.
        menuButton.addTarget(self, action: #selector(MyJobsViewController.openmenu), forControlEvents: .TouchUpInside)
    }

    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    
    @IBAction func didclickFilterOption(sender: AnyObject) {

           self.displayViewController(.BottomBottom)
    }
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let Popupsortcontroller : PopupSortingViewController = PopupSortingViewController(nibName:"PopupSortingViewController",bundle: nil)
        
        Popupsortcontroller.delegate = self;
        self.presentpopupViewController(Popupsortcontroller, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func  pressedCancel(sender: PopupSortingViewController) {
        
        
        
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
      
        
      

    }
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int,isToday: Int,isSortby:NSString) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("SortingJobNotification", object:nil,userInfo: ["Fromdate":"\(fromdate)","Todate":"\(todate)","asDes":"\(isAscendorDescend)","isToday":"\(isToday)","StatusforSort":"\(isSortby)"] )
        
        
        
    }
    

    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int, isSortby: NSString) {
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func MoveToDetail(notification:NSNotification){
        let rec:MyOrderOpenRecord = notification.object as! MyOrderOpenRecord
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
        objMyOrderVc.delegate = self
        objMyOrderVc.jobID=rec.orderId
        objMyOrderVc.Getorderstatus = ""
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
