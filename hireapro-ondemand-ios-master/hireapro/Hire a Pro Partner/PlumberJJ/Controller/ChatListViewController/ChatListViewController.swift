//
//  ChatListViewController.swift
//  PlumberJJ
//
//  Created by Aravind Natarajan on 02/02/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

import UIKit

class ChatListViewController: RootBaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var chatListArr:NSMutableArray=NSMutableArray()
    var nameArray:NSMutableArray=NSMutableArray()
    var p_idArray:NSMutableArray=NSMutableArray()
    var job_idArray:NSMutableArray=NSMutableArray()
    var msgArray:NSMutableArray=NSMutableArray()
    var msg_timeArray:NSMutableArray=NSMutableArray()
    var imageArray:NSMutableArray=NSMutableArray()
    var user_idArray : NSMutableArray = NSMutableArray()
    var Category_idArray : NSMutableArray = NSMutableArray()
    var created_dateArray : NSMutableArray = NSMutableArray()
    var tasker_statusArray : NSMutableArray = NSMutableArray()
     @IBOutlet var chat: UILabel!
    
    @IBOutlet weak var chatListTblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        chat.text = Language_handler.VJLocalizedString("chat", comment: nil)

                // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        let Nb=UINib(nibName: "ChatListTableViewCell", bundle: nil)
        self.chatListTblView.registerNib(Nb, forCellReuseIdentifier: "ChatCell")
        self.chatListTblView.estimatedRowHeight=120
        self.chatListTblView.rowHeight = UITableViewAutomaticDimension
        self.chatListTblView.separatorColor=UIColor.lightGrayColor()
        self.chatListTblView.tableFooterView=UIView()
        
        self.chatListTblView.hidden=true
        self.showProgress()
        self.GetChatList()
        

    }
    func GetChatList(){
        
        
        chatListArr=NSMutableArray()
        nameArray=NSMutableArray()
        p_idArray=NSMutableArray()
        job_idArray=NSMutableArray()
        msgArray=NSMutableArray()
        msg_timeArray=NSMutableArray()
        imageArray=NSMutableArray()
        user_idArray   = NSMutableArray()
        Category_idArray = NSMutableArray()
        created_dateArray = NSMutableArray()
        tasker_statusArray = NSMutableArray()
        

        
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        let param=["type":"0","userId":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(ChatListUrl, param: param) {
            (responseObject, error) -> () in
            self.chatListTblView.hidden=false
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    
                    
                    
                    if(Status == "1")
                    {
                        
                        let ChatList: NSMutableArray = (Dict.objectForKey("response")!.objectForKey("message") as? NSMutableArray)!
                        if(ChatList.count > 0 )
                        {
                            self.chatListTblView.hidden=false
                            for  Dict in ChatList
                            {
                                
                                let image=self.theme.CheckNullValue(Dict.objectForKey("user_image"))!
                                let name=self.theme.CheckNullValue(Dict.objectForKey("user_name"))!
                                let p_id=self.theme.CheckNullValue(Dict.objectForKey("task_id"))!
                                let job_id=self.theme.CheckNullValue(Dict.objectForKey("booking_id"))!
                                let userid=self.theme.CheckNullValue(Dict.objectForKey("user_id"))!
                                let category=self.theme.CheckNullValue(Dict.objectForKey("category"))!
                                let created_date = self.theme.CheckNullValue(Dict.objectForKey("created"))!
                                let tasker_status = self.theme.CheckNullValue(Dict.objectForKey("tasker_status"))!

                                
                                self.nameArray.addObject(name)
                                self.p_idArray.addObject(p_id)
                                self.job_idArray.addObject(job_id)
                                //self.msgArray.addObject(msg)
                                //                            self.msg_timeArray.addObject(msg_time)
                                self.imageArray.addObject(image)
                                self.user_idArray.addObject(userid)
                                self.Category_idArray.addObject(category)
                                self.created_dateArray.addObject(created_date)
                                self.tasker_statusArray.addObject(tasker_status)

                            }
                            self.chatListTblView.reloadData()
                            self.chatListTblView.hidden = false
                        }
                        else
                        {
                            self.chatListTblView.hidden=true
                        }
                        
                    }
                    else
                    {
                        self.chatListTblView.hidden=true
                        
                        let Response:NSString = self.theme.CheckNullValue(Dict.objectForKey("response"))!
                        self.view.makeToast(message:"\(Response)", duration: 3, position: HRToastPositionDefault, title: "Oops!!!")
                        
                        
                    }
                    
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    
                }
            }
            
        }
    }
    
    @IBAction func didClickbackBtn(sender: AnyObject) {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.p_idArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCellWithIdentifier("ChatCell") as! ChatListTableViewCell
        
        Cell.Provider_image.sd_setImageWithURL(NSURL(string: "\(imageArray[indexPath.row])"), placeholderImage: UIImage(named: "PlaceHolderSmall"))
        
        //Cell.Provider_image.sd_setImageWithURL(NSURL(string: "\(imageArray[indexPath.row])"), completed: themes.block)
        Cell.Chat_Lbl.text="\(nameArray[indexPath.row])"
        Cell.Provider_image.layer.cornerRadius = Cell.Provider_image.frame.size.height / 2;
        Cell.Provider_image.layer.masksToBounds = true;
        Cell.Provider_image.layer.borderWidth = 0;
        Cell.Provider_image.contentMode = UIViewContentMode.ScaleAspectFill
        Cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        Cell.Time_Lab.text = "\(job_idArray[indexPath.row])"
        Cell.catagory_labl.text="\(Category_idArray[indexPath.row])"
        Cell.created_date.text = "\(created_dateArray[indexPath.row])"
        
        if (tasker_statusArray.objectAtIndex(indexPath.row) as! String == "1")
        {
            Cell.border_view.backgroundColor = PlumberThemeColor
        }
        else{
            Cell.border_view.backgroundColor = UIColor.clearColor()
        }
        Cell.border_view.layer.cornerRadius = Cell.border_view.frame.size.width/2
        Cell.border_view.clipsToBounds=true
        
        
        return Cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let objChatVc = self.storyboard!.instantiateViewControllerWithIdentifier("ChatVCSID") as! MessageViewController
        objChatVc.jobId=self.job_idArray[indexPath.row] as! NSString
        objChatVc.Userid = self.user_idArray[indexPath.row] as! NSString
        objChatVc.username = self.nameArray[indexPath.row] as! NSString
        objChatVc.Userimg = self.imageArray[indexPath.row] as! NSString
        objChatVc.RequiredJobid = self.p_idArray[indexPath.row] as! NSString
        self.navigationController!.pushViewController(objChatVc, animated: true)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
