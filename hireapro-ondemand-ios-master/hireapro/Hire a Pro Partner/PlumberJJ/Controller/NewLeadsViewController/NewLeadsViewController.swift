//
//  NewLeadsViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/19/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class NewLeadsViewController: RootBaseViewController,UITableViewDataSource,UITableViewDelegate,MyOrderOpenDetailViewControllerDelegate,PopupSortingViewControllerDelegate {
    @IBOutlet var tableViewFooter: MyFooter!
    @IBOutlet weak var newLeadsTblView: UITableView!
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var barButton: UIButton!
  @IBOutlet var loading_Lbl: UILabel!
    var NewLeadsArr:NSMutableArray=NSMutableArray()
    var Param: NSDictionary = NSDictionary()
   // var theme:Theme=Theme()
     var nextPageStr:NSInteger!
    var noDataView:UIView!
    private var loading = false {
        didSet {
            tableViewFooter.hidden = !loading
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         loading_Lbl.text = Language_handler.VJLocalizedString("loading", comment: nil)
        titleHeader.text = Language_handler.VJLocalizedString("new_leads", comment: nil)
        btnFilter.setTitle(Language_handler.VJLocalizedString("filter", comment: nil), forState: UIControlState.Normal)

        newLeadsTblView.hidden=true
        nextPageStr=1
        refreshNewLeads()
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        Param  = ["provider_id":"\(objUserRecs.providerId)",
                  "page":"\(nextPageStr)" as String,
                  "perPage":kPageCount]

        
        GetNewLeads()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewLeadsViewController.methodOfReceivedSortingNotificationNetworkDetail(_:)), name:"SortingNotification", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewLeadsViewController.methodOfReceivedNotificationNetworkDetail(_:)), name:kNewLeadsOpenNotifNotif, object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    
    func filterOrder(type:String,isAsc:String){
        
        
        var types = String()
        
        if(type == "0"){
            types = "today"
        }else if(type == "1"){
            types = "recent"
        }else if(type == "2"){
            types = "upcoming"
        }
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
        
        Param  = ["provider_id":"\(objUserRecs.providerId)",
                  "page":"\(nextPageStr)" as String,
                  "type":"\(types)","perPage":kPageCount,"orderby":"\(isAsc)","sortby":""]
        
        url_handler.makeCall(sortesList, param: Param) {
            (responseObject, error) -> () in
            self.newLeadsTblView.hidden=false
            self.DismissProgress()
            self.newLeadsTblView.dg_stopLoading()
            self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status = self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        if(self.nextPageStr==1){
                            self.NewLeadsArr.removeAllObjects()
                        }
                        if(responseObject?.objectForKey("response")?.objectForKey("jobs")!.count>0){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("jobs") as! NSArray
                            
                            for (_, element) in listArr.enumerate() {
                                let provider_lat = self.theme.CheckNullValue(element.objectForKey("location_lat"))!
                                let provider_lng = self.theme.CheckNullValue(element.objectForKey("location_lng"))!
                                

                                let rec = MyOrderOpenRecord(order_id: self.theme.CheckNullValue(element.objectForKey("job_id"))!, post_on: self.theme.CheckNullValue(element.objectForKey("booking_time"))!, user_Img: self.theme.CheckNullValue(element.objectForKey("user_image"))!, user_name: self.theme.CheckNullValue(element.objectForKey("user_name"))!, user_catg: self.theme.CheckNullValue(element.objectForKey("category_name"))!, user_Loc:  self.theme.CheckNullValue(self.theme.getAddressForLatLng(provider_lat, longitude: provider_lng, status: "short"))!,order_sta: self.theme.CheckNullValue(element.objectForKey("job_status"))!,rate_hour: self.theme.CheckNullValue(element.objectForKey("hourly_rate"))!, cat_type: self.theme.CheckNullValue(element.objectForKey("type"))!)
                                [self.NewLeadsArr .addObject(rec)]
                            }
                            
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:Language_handler.VJLocalizedString("no_leads", comment: nil), duration: 3, position: HRToastPositionDefault, title:appNameJJ)
                            }
                        }
                        self.newLeadsTblView.reloadData()
                    }
                    else
                    {
                        //                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
        
    }

    override func viewWillAppear(animated: Bool) {
        if (self.navigationController!.viewControllers.count != 1) {
            backBtn.hidden=false;
            barButton.hidden=true
        }else{
        }
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyLeadsViewController.MoveToDetail(_:)), name: kNewLeadsNotif, object: nil)
        barButton.addTarget(self, action: #selector(MyLeadsViewController.openmenu), forControlEvents: .TouchUpInside)

        //loadNewFeed()
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func MoveToDetail(notification:NSNotification){
        let rec:MyOrderOpenRecord = notification.object as! MyOrderOpenRecord
        let objMyOrderVc = self.storyboard!.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
        objMyOrderVc.delegate = self
        objMyOrderVc.jobID=rec.orderId
        objMyOrderVc.Getorderstatus = rec.orderstatus
        self.navigationController!.pushViewController(objMyOrderVc, animated: true)
    }
    deinit {
        newLeadsTblView.dg_removePullToRefresh()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    @IBAction func didclickFilterOption(sender: AnyObject) {
        self.displayViewController(.BottomBottom)
        
    }
    func displayViewController(animationType: SLpopupViewAnimationType) {
        
        let Popupsortcontroller : PopupSortingViewController = PopupSortingViewController(nibName:"PopupSortingViewController",bundle: nil)
        
        Popupsortcontroller.delegate = self;
        self.presentpopupViewController(Popupsortcontroller, animationType: animationType, completion: { () -> Void in
            
        })
    }
    
    func  pressedCancel(sender: PopupSortingViewController) {
        
        
        
        self.dismissPopupViewController(.BottomBottom)
        
    }
    
    
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int,isToday:Int,isSortby:NSString) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("SortingNotification", object:nil,userInfo: ["Fromdate":"\(fromdate)","Todate":"\(todate)","asDes":"\(isAscendorDescend)","isToday":"\(isToday)","StatusforSort":"\(isSortby)"] )
        
        
        
    }
    
    func passRequiredParametres(fromdate: NSString, todate: NSString, isAscendorDescend: Int, isSortby: NSString) {
        
    }
    

    func methodOfReceivedNotificationNetworkDetail(notification: NSNotification){
       loadNewFeed()
    }
    func methodOfReceivedSortingNotificationNetworkDetail(notification: NSNotification){
       // loadNewFeed()
        let userInfo:Dictionary<String,String!> = notification.userInfo as! Dictionary<String,String!>

        if(userInfo["isToday"]! == "4"){

        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
           nextPageStr = 1;     
        Param  = ["provider_id":"\(objUserRecs.providerId)",
                  "page":"\(nextPageStr)" as String,
                  "perPage":kPageCount,"from":userInfo["Fromdate"]!,"to":userInfo["Todate"]!,"orderby":userInfo["asDes"]!,"sortby":userInfo["StatusforSort"]!]
        
        GetNewLeads()
        }
            else{
            nextPageStr = 1;

                self.filterOrder("\(userInfo["isToday"]!)", isAsc: "\(userInfo["asDes"]!)")
            }

        
    }
    func loadNewFeed(){
        nextPageStr=1
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        
              Param  = ["provider_id":"\(objUserRecs.providerId)",
                  "page":"\(nextPageStr)" as String,
                  "perPage":kPageCount]

        GetNewLeads()
    }
    
    
    
   
    func GetNewLeads(){
        
             // print(Param)
       
        url_handler.makeCall(getNewLeads, param: Param) {
            (responseObject, error) -> () in
             self.newLeadsTblView.hidden=false
            self.DismissProgress()
            self.newLeadsTblView.dg_stopLoading()
             self.loading = false
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title:Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status = self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        if(self.nextPageStr==1){
                            self.NewLeadsArr.removeAllObjects()
                        }
                        if(responseObject?.objectForKey("response")?.objectForKey("jobs")!.count>0){
                            let  listArr:NSArray=responseObject?.objectForKey("response")?.objectForKey("jobs") as! NSArray
                            
                            for (_, element) in listArr.enumerate() {
                                
                                let provider_lat = self.theme.CheckNullValue(element.objectForKey("location_lat"))!
                                let provider_lng = self.theme.CheckNullValue(element.objectForKey("location_lng"))!
                                let rec = MyOrderOpenRecord(order_id: self.theme.CheckNullValue(element.objectForKey("job_id"))!, post_on: self.theme.CheckNullValue(element.objectForKey("booking_time"))!, user_Img: self.theme.CheckNullValue(element.objectForKey("user_image"))!, user_name: self.theme.CheckNullValue(element.objectForKey("user_name"))!, user_catg: self.theme.CheckNullValue(element.objectForKey("category_name"))!, user_Loc: self.theme.CheckNullValue(self.theme.getAddressForLatLng(provider_lat, longitude: provider_lng, status: "short"))!,order_sta: self.theme.CheckNullValue(element.objectForKey("job_status"))!,rate_hour:
                                    self.theme.CheckNullValue(element.objectForKey("hourly_rate"))!, cat_type: self.theme.CheckNullValue(element.objectForKey("type"))!)
                                [self.NewLeadsArr .addObject(rec)]
                            }
                            
                            self.nextPageStr=self.nextPageStr+1
                        }else{
                            if(self.nextPageStr>1){
                                self.view.makeToast(message:Language_handler.VJLocalizedString("no_leads", comment: nil), duration: 3, position: HRToastPositionDefault, title:appNameJJ)
                            }
                        }
                        self.newLeadsTblView.reloadData()
                    }
                    else
                    {
//                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: "Network Failure !!!")
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
    }
    
   

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        if(NewLeadsArr.count>0){
            noDataView.hidden=true
            return 1
        }else{
            
            if(noDataView==nil){
                let subviewArray = NSBundle.mainBundle().loadNibNamed("NoDataView", owner: self, options: nil)
                noDataView = subviewArray[0] as! NoDataView
                
                noDataView.frame=self.view.frame
            }
            
            noDataView.hidden=false
            tableView.backgroundView = noDataView
        }
        return 1
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewLeadsArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NewLeadsIdentifier", forIndexPath: indexPath) as! NewLeadsTableViewCell
        let objRec:MyOrderOpenRecord=self.NewLeadsArr.objectAtIndex(indexPath.row) as! MyOrderOpenRecord
        
        cell.loadMyOrderNewLeadTableCell(objRec)
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //MyOrderDetailOpenVCSID
        if(!loading){
            if(indexPath.row < NewLeadsArr.count){
                //openDetailNavSID
                let objRec=NewLeadsArr.objectAtIndex(indexPath.row)
                NSNotificationCenter.defaultCenter().postNotificationName(kNewLeadsNotif, object: objRec)
               
            }
        }
    }
    
    
    
    
    ///////////////////// Infinite Scroll
     func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (Int(scrollView.contentOffset.y + scrollView.frame.size.height) == Int(scrollView.contentSize.height + scrollView.contentInset.bottom)) {
            if (maximumOffset - currentOffset) <= 52 {
                refreshNewLeadsandLoad()
            }
        }
        
    }
     let loadingView = DGElasticPullToRefreshLoadingViewCircle()
    func refreshNewLeads(){
       
        loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
        newLeadsTblView.dg_addPullToRefreshWithActionHandler({
            self.nextPageStr=1
            let objUserRecs:UserInfoRecord = self.theme.GetUserDetails()
            
            self.Param  = ["provider_id":"\(objUserRecs.providerId)",
                "page":"\(self.nextPageStr)" as String,
                "perPage":kPageCount]

            self.GetNewLeads()
            
            }, loadingView: loadingView)
        newLeadsTblView.dg_setPullToRefreshFillColor(PlumberLightGrayColor)
        newLeadsTblView.dg_setPullToRefreshBackgroundColor(newLeadsTblView.backgroundColor!)
    }
    func refreshNewLeadsandLoad(){
        if (!loading) {
            loading = true
            let objUserRecs:UserInfoRecord = self.theme.GetUserDetails()
            
            self.Param  = ["provider_id":"\(objUserRecs.providerId)",
                           "page":"\(self.nextPageStr)" as String,
                           "perPage":kPageCount]

            GetNewLeads()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
