//
//  EditProfileViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/25/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import MobileCoreServices
import AssetsLibrary
import Foundation
import Alamofire
import SwiftyJSON


extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
        
        
    }
}

class EditProfileViewController: RootBaseViewController,UIActionSheetDelegate,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,WDImagePickerDelegate {
    @IBOutlet var Placesearch_tableview: UITableView!
    
    @IBOutlet var Location_tableview: UITableView!
    @IBOutlet var place_searchview: UIView!
    @IBOutlet var editAvailabilitytableview: UITableView!
    @IBOutlet var working_loca: UITextField!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var editProfileScrollView: UIScrollView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var cCodeTxtField: UITextField!
    let progressbar = GradientCircularProgress()
    @IBOutlet weak var countryTxtField: UITextField!
    @IBOutlet weak var postalCodeTxtField: UITextField!
    @IBOutlet weak var stateTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var lastView: UIView!
    @IBOutlet var category_TblView: UITableView!
    @IBOutlet weak var category_View: UIView!
    var selectedBtn = NSMutableArray()
    var getTextfieldtag : NSString = NSString()
    var availabilityStorage:NSArray!
    var category_Array:NSMutableArray = NSMutableArray()
    var editCatDtls_Array:NSMutableArray = NSMutableArray()
    var imagePicker = WDImagePicker()
    
    @IBOutlet weak var lblMyProfile: UILabel!
    @IBOutlet weak var btnUpdate1: UIButton!
    
    @IBOutlet weak var btnUpdate3: UIButton!
   
    @IBOutlet weak var btnUpdate2: UIButton!
    @IBOutlet weak var btnUpdate4: UIButton!
    @IBOutlet weak var btnUpdate5: UIButton!
     @IBOutlet weak var btnUpdate6: UIButton!
    
    @IBOutlet weak var WorkingLocation: UILabel!
       @IBOutlet weak var Availablity: UILabel!
    @IBOutlet weak var Category: UIButton!
    
    
    @IBOutlet weak var lblEmail: SMIconLabel!
    
    @IBOutlet weak var lblMobile: SMIconLabel!
    
    @IBOutlet weak var lblAddress: SMIconLabel!
    @IBOutlet weak var lbluserName: SMIconLabel!
    
    @IBOutlet weak var lblRadius: SMIconLabel!
    
    var availInc = Int()
    
    @IBOutlet var usernameTxtfield: UITextField!
    

    var weekDay = ["Days","Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var weekFullDay = ["Days","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    var weakTitle = ["Morning","Afternoon","Evening"]
    
    @IBOutlet var radiusTxtfield: UITextField!
    var tField: UITextField!
    var otpStr : NSString = NSString()
    var otp_status : NSString = NSString()
    var imagedata : NSData = NSData()
    var Contact:NSString=NSString()
    var Addaddress_Data : Addaddress = Addaddress()
    var categoryArray: NSMutableArray = NSMutableArray()
    
    enum PlaceType: CustomStringConvertible {
        case All
        case Geocode
        case Address
        case Establishment
        case Regions
        case Cities
        
        var description : String {
            switch self {
            case .All: return ""
            case .Geocode: return "geocode"
            case .Address: return "address"
            case .Establishment: return "establishment"
            case .Regions: return "regions"
            case .Cities: return "cities"
            }
        }
    }
    
    struct Place {
        let id: String
        let description: String
    }
    
    var places = [Place]()
    var setstatus:Bool=Bool()
    
    var placeType: PlaceType = .All
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbluserName.text = Language_handler.VJLocalizedString("user_name", comment: nil)
        lblRadius.text = Language_handler.VJLocalizedString("radius", comment: nil)
        WorkingLocation.text = Language_handler.VJLocalizedString("WorkingLocation", comment: nil)
        Availablity.text = Language_handler.VJLocalizedString("Availablity", comment: nil)
        Category.setTitle(Language_handler.VJLocalizedString("Category", comment: nil), forState: UIControlState.Normal)

        lblMyProfile.text = Language_handler.VJLocalizedString("edit_profile", comment: nil)
        lblEmail.text = Language_handler.VJLocalizedString("email", comment: nil)
        lblMobile.text = Language_handler.VJLocalizedString("mobile", comment: nil)
        lblAddress.text = Language_handler.VJLocalizedString("address", comment: nil)
        btnUpdate1.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)
        btnUpdate2.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)
        btnUpdate3.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)
        btnUpdate4.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)
        btnUpdate5.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)
        btnUpdate6.setTitle(Language_handler.VJLocalizedString("update", comment: nil), forState: UIControlState.Normal)

        
        imagePicker.delegate = self
        blurBannerImg()
        decorateTxtField()
        GetDatasForBanking()
        editAvailabilitytableview.registerNib(UINib(nibName: "EditAvailabilityTableViewCell", bundle: nil), forCellReuseIdentifier: "availabledayscell")
        category_TblView.registerNib(UINib(nibName: "CategoryListTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryListTableViewCell")
        editAvailabilitytableview.delegate = self
        editAvailabilitytableview.dataSource = self
        editAvailabilitytableview.reloadData()
        
        
        Placesearch_tableview.hidden=true
        Placesearch_tableview.tableFooterView=UIView()
        Placesearch_tableview.layer.borderWidth=1.0
        Placesearch_tableview.layer.borderColor=UIColor.blackColor().CGColor
        Placesearch_tableview.layer.cornerRadius=5.0
        
        
        Location_tableview.hidden=true
        Location_tableview.tableFooterView=UIView()
        Location_tableview.layer.borderWidth=1.0
        Location_tableview.layer.borderColor=UIColor.blackColor().CGColor
        Location_tableview.layer.cornerRadius=5.0
        // Do any additional setup after loading the view.
        
        addressTxtField.addTarget(self, action: #selector(TextfieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        working_loca.addTarget(self, action: #selector(TextfieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        
        
        Placesearch_tableview.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        Location_tableview.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
    }
    
    
    
    func TextfieldDidChange(textField:UITextField)
    {
        if(textField == addressTxtField)
        {
            if(addressTxtField.text != "")
            {
                if(places.count == 0)
                {
                    self.Placesearch_tableview.hidden=true
                    
                }
                    
                else
                {
                    self.Placesearch_tableview.hidden=false
                    
                }
                
                getTextfieldtag = "1"
                getPlaces(addressTxtField.text!)
                
                
                
            }
            else
            {
                
                places.removeAll()
                self.Placesearch_tableview.hidden=true
            }
        }
            
        else if (textField == working_loca)
        {
            if(working_loca.text != "")
            {
                if(places.count == 0)
                {
                    self.Location_tableview.hidden=true
                    
                }
                else
                {
                    self.Location_tableview.hidden=false
                    
                }
                getTextfieldtag = "2"
                getPlaces(working_loca.text!)
                
                
            }
            else
            {
                
                places.removeAll()
                self.Location_tableview.hidden=true
            }
            
        }
    }
    
    
    
    func getPlaces(searchString: String) {
        
        let request = requestForSearch(searchString)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request) { data, response, error in
            
            self.handleResponse(data, response: response as? NSHTTPURLResponse, error: error)
            
        }
        
        
        
        task.resume()
        
    }
    
    
    
    
    func handleResponse(data: NSData!, response: NSHTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            
            return
            
        }
        
        
        
        if response == nil {
            
            print("GooglePlacesAutocomplete Error: No response from API")
            
            return
            
        }
        
        
        
        if response.statusCode != 200 {
            
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            
            return
            
        }
        
        
        do {
            
            let json: NSDictionary = try NSJSONSerialization.JSONObjectWithData(
                
                data,
                
                options: NSJSONReadingOptions.MutableContainers
                
                ) as! NSDictionary
            
            dispatch_async(dispatch_get_main_queue(), {
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                
                
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        
                        return Place(
                            
                            id: prediction["id"] as! String,
                            
                            description: prediction["description"] as! String
                            
                        )
                        
                    }
                    self.Placesearch_tableview.reloadData()
                    self.Location_tableview.reloadData()
                    
                    
                }
                
            })
        }
            
            
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
        
        
        // Perform table updates on UI thread
        
        
        
    }
    
    func requestForSearch(searchString: String) -> NSURLRequest {
        
        
        
        let params = [
            
            "input": searchString,
            
            // "type": "(\(placeType.description))",
            
            //"type": "",
            
            "key": "\(googleApiKey)"
            
        ]
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")
        
        
        
        return NSMutableURLRequest(
            
            URL: NSURL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params))")!
            
            
        )
        
    }
    
    
    
    
    
    
    
    func query(parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sort(<)) {
            let value: AnyObject! = parameters[key]
            
            components += [(escape(key), escape("\(value)"))]
        }
        
        return components.map{"\($0)=\($1)"}.joinWithSeparator("&")
    }
    
    
    
    func escape(string: String) -> String {
        
        let legalURLCharactersToBeEscaped: CFStringRef = ":/?&=;+!@#$()',*"
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        
    }
    
    
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int{
        var coun = Int()
        if tableView == Placesearch_tableview{
            coun = places.count
        }else if tableView == Location_tableview
        {
            coun = places.count
            
        }
        else if tableView == editAvailabilitytableview{
            coun = weekDay.count
            //            coun = 0
        }
        else if tableView == category_TblView{
            coun = categoryArray.count
        }
        
        return coun
        
    }
    
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat
    {
        if tableView == Placesearch_tableview || tableView == Location_tableview{
            
            return 40.0
            
        }
        return 50
    }
    
    
    
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell{
        
        let cell = UITableViewCell()
        if tableView == Placesearch_tableview{
            
            let Placecell = (tableView.dequeueReusableCellWithIdentifier("cell") )!
            let place = self.places[indexPath.row]
            Placecell.textLabel?.textColor = UIColor.blackColor()
            Placecell.textLabel?.text=place.description
            return Placecell
            
        }else if tableView == Location_tableview
        {
            
            let cell:UITableViewCell = (tableView.dequeueReusableCellWithIdentifier("cell") )!
            
            
            let place = self.places[indexPath.row]
            
            cell.textLabel?.text=place.description
            
            
            return cell
            
            
        }
        else if tableView == editAvailabilitytableview{
            let availcell = (tableView.dequeueReusableCellWithIdentifier("availabledayscell")) as! EditAvailabilityTableViewCell
            
            availcell.btn1.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            availcell.btn2.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            availcell.btn3.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), forControlEvents: UIControlEvents.TouchUpInside)
           // print(availabilityStorage)
            if indexPath.row == 0{
                availInc = 0
                availcell.btn1.hidden = true
                availcell.btn2.hidden = true
                availcell.btn3.hidden = true
//                Cell.lblTitle.text = self.theme.setLang((titleArray.objectAtIndex(indexPath.row) as? String)!)
                availcell.lbl1.text = self.theme.setLang("\(weekDay[indexPath.row])")
               // availcell.lbl1.text = "\(weekDay[indexPath.row])"
                availcell.lbl2.text = Language_handler.VJLocalizedString("morning", comment: nil)
                availcell.lbl3.text = Language_handler.VJLocalizedString("afternoon", comment: nil)
                availcell.lbl4.text = Language_handler.VJLocalizedString("evening", comment: nil)
                
            }
            else {
                var record = self.availabilityStorage.objectAtIndex(indexPath.row-1) as! AvailableRecord
                availcell.btn1.hidden = false
                availcell.btn2.hidden = false
                availcell.btn3.hidden = false
                availcell.lbl2.hidden = true
                availcell.lbl3.hidden = true
                availcell.lbl4.hidden = true
                
                availInc += 1
                availcell.btn1.tag = availInc
                if record.AvailMornigtime == "1"{
                    availcell.btn1.setBackgroundImage(UIImage.init(named: "tick"), forState: UIControlState.Normal)
                    if !selectedBtn.containsObject(availInc){
                        selectedBtn.addObject(availInc)
                    }
                    
                }else{
                    availcell.btn1.setBackgroundImage(UIImage.init(named: "tick_gray"), forState: UIControlState.Normal)
                    
                }
                
                availInc += 1
                availcell.btn2.tag = availInc
                if record.AvailAftertime == "1"{
                    availcell.btn2.setBackgroundImage(UIImage.init(named: "tick"), forState: UIControlState.Normal)
                    if !selectedBtn.containsObject(availInc){
                        selectedBtn.addObject(availInc)
                    }
                    
                }else{
                    availcell.btn2.setBackgroundImage(UIImage.init(named: "tick_gray"), forState: UIControlState.Normal)
                    
                }
                
                
                availInc += 1
                availcell.btn3.tag = availInc
                if record.Availeveningtime == "1"{
                    availcell.btn3.setBackgroundImage(UIImage.init(named: "tick"), forState: UIControlState.Normal)
                    if !selectedBtn.containsObject(availInc){
                        selectedBtn.addObject(availInc)
                    }
                    
                }else{
                    availcell.btn3.setBackgroundImage(UIImage.init(named: "tick_gray"), forState: UIControlState.Normal)
                    
                }
                
                
                availcell.lbl1.text = "\(weekDay[indexPath.row])"
                
            }
            return availcell
            
            
        }
            
        else if tableView == category_TblView{
            let cat_Cell = (tableView.dequeueReusableCellWithIdentifier("CategoryListTableViewCell")) as! CategoryListTableViewCell
            let currentCat_Dtl = categoryArray[indexPath.row] as! CategoryList
            cat_Cell.categotyName_Lbl.text = currentCat_Dtl.categoryName
            cat_Cell.edit_Btn.tag = indexPath.row
            cat_Cell.delete_Btn.tag = indexPath.row
            cat_Cell.delete_Btn.addTarget(self, action: #selector(EditProfileViewController.category_DltAction), forControlEvents: .TouchUpInside)
            cat_Cell.edit_Btn.addTarget(self, action: #selector(EditProfileViewController.category_EditAction), forControlEvents: .TouchUpInside)
            category_TblView.frame = CGRectMake(tableView.frame.origin.x, 46, tableView.frame.size.width, tableView.contentSize.height+18)
            //category_View.frame = category_TblView.frame
            category_View.frame = CGRectMake(category_View.frame.origin.x , category_View.frame.origin.y , category_View.frame.width, category_TblView.frame.height+100)
            
            return cat_Cell
        }
        return cell
        
        
        
    }
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)
    {
        if tableView .isEqual(Placesearch_tableview)
        {
            
            if  getTextfieldtag == "1"
                
            {
                let place = self.places[indexPath.row]
                
                self.showProgress()
                Addaddress_Data.YourLocality = "\(place.description)"
                
                let geocoder: CLGeocoder = CLGeocoder()
                
                geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                    self.DismissProgress()
                    if (error != nil) {
                        print("Error \(error!)")
                    } else if let placemark = placemarks?[0] {
                        
                        
                        print("the responsadsase is .......\(placemark)")
                        
                        //                let CityName:String?=placemark.subAdministrativeArea
                        let Locality:String?=placemark.subLocality
                        
                        let ZipCode:String?=placemark.postalCode
                        
                        
                        if (placemark.administrativeArea != nil)
                        {
                            self.stateTxtField.text = placemark.administrativeArea!
                        }
                        
                        if(placemark.country != nil){
                            self.countryTxtField.text = placemark.country!
                        }
                        
                        if (placemark.subAdministrativeArea != nil)
                        {
                            self.cityTxtField.text = self.theme.CheckNullValue(placemark.subAdministrativeArea!)
                        }
                        
                        if(ZipCode != nil)
                        {
                            self.postalCodeTxtField.text=""
                            
                            self.postalCodeTxtField.text=ZipCode!
                            
                        }
                        else
                        {
                            self.postalCodeTxtField.text=""
                            
                        }
                    }
                })
                
                
                
                url_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(googleApiKey)&address=\(place.description.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)") { (responseObject) -> () in
                    
                    
                    if(responseObject != nil)
                    {
                        
                        
                        let results:NSArray=(responseObject?.objectForKey("results"))! as! NSArray
                        if results.count > 0
                        {
                            let firstItem: NSDictionary = results.objectAtIndex(0) as! NSDictionary
                            let geometry: NSDictionary = firstItem.objectForKey("geometry") as! NSDictionary
                            let locationDict:NSDictionary = geometry.objectForKey("location") as! NSDictionary
                            let lat:NSNumber = locationDict.objectForKey("lat") as! NSNumber
                            let lng:NSNumber = locationDict.objectForKey("lng") as! NSNumber
                            
                            
                            self.Addaddress_Data.Latitude="\(lat)"
                            
                            self.Addaddress_Data.Longitude="\(lng)"
                        }
                        
                        
                    }
                    else
                    {
                        self.Addaddress_Data.Latitude="0"
                        self.Addaddress_Data.Longitude="0"
                        
                    }
                    
                    
                    
                }
                
                addressTxtField.text=Addaddress_Data.YourLocality as String
                
                
                Placesearch_tableview.hidden=true
                
            }
        }
            
        else if tableView == Location_tableview{
            
            let place = self.places[indexPath.row]
            
            self.showProgress()
            
            let geocoder: CLGeocoder = CLGeocoder()
            
            geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                self.DismissProgress()
                if (error != nil) {
                    print("Error \(error!)")
                } else if let placemark = placemarks?[0] {
                    
                }
            })
            
            
            
            url_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(googleApiKey)&address=\(place.description.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)") { (responseObject) -> () in
                
                
                if(responseObject != nil)
                {
                    
                    
                    let results:NSArray=(responseObject?.objectForKey("results"))! as! NSArray
                    if results.count > 0
                    {
                        let firstItem: NSDictionary = results.objectAtIndex(0) as! NSDictionary
                        let geometry: NSDictionary = firstItem.objectForKey("geometry") as! NSDictionary
                        let locationDict:NSDictionary = geometry.objectForKey("location") as! NSDictionary
                        let lat:NSNumber = locationDict.objectForKey("lat") as! NSNumber
                        let lng:NSNumber = locationDict.objectForKey("lng") as! NSNumber
                        
                        
                        self.Addaddress_Data.working_Latitude="\(lat)"
                        
                        self.Addaddress_Data.working_Longitude="\(lng)"
                    }
                    
                    
                }
                else
                {
                    self.Addaddress_Data.working_Latitude="0"
                    self.Addaddress_Data.working_Longitude="0"
                    
                }
                
            }
            
            working_loca.text="\(place.description)"
            Location_tableview.hidden=true
            
        }
        
    }
    
    
    
    
    
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        if (NSStringFromClass((touch.view?.classForCoder)!)=="UITableViewCellContentView")
        {
            
            
            
            return false
        }
            
        else{
            return true
        }
    }
    
    func category_DltAction(sender:UIButton){
        self.showProgress()
        print(sender.tag)
        let getCurrent_Val = categoryArray[sender.tag] as! CategoryList
        let getCat_ID =  getCurrent_Val.category_ID
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","category":"\(getCat_ID)"]
        url_handler.makeCall(deleteCategory, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1") {
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject?.objectForKey("response"))!, duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                        self.categoryArray.removeObjectAtIndex(sender.tag)
                        self.category_TblView.reloadData()
                        
                    }else{//alertImg
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject?.objectForKey("message"))!, duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                    }
                    
                }
            }
        }
    }
    
    func category_EditAction(sender:UIButton){
        print(sender.tag)
        
        // getSubCategory_Dtl
        let getSelectCat_Dtl = categoryArray[sender.tag] as! CategoryList
        let subCat_ID = getSelectCat_Dtl.category_ID
        getSubcategory_Dtls(subCat_ID)
        //let categoryVC = storyboard?.instantiateViewControllerWithIdentifier("CategoryEditVC") as! CategoryEditVC
        //categoryVC.checkCategory = "Edit Category"
        // self.navigationController?.pushViewController(categoryVC, animated: true)
        
        
    }
    
    func getSubcategory_Dtls(subCat_ID:String){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","category":"\(subCat_ID)"]
        url_handler.makeCall(getEditCategory_Detail, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        self.editCatDtls_Array.removeAllObjects()
                        let getReponse = responseObject?.objectForKey("response") as! NSDictionary
                        let subCat_ID = self.theme.CheckNullValue(getReponse.objectForKey("child_id"))
                        let subCateg_Name = self.theme.CheckNullValue(getReponse.objectForKey("child_name"))
                        let experience_ID = self.theme.CheckNullValue(getReponse.objectForKey("experience_id"))
                        let experience_Name = self.theme.CheckNullValue(getReponse.objectForKey("experience_name"))
                        let hour_Rate = self.theme.CheckNullValue(getReponse.objectForKey("hour_rate"))
                        let minHour_Rate = self.theme.CheckNullValue(getReponse.objectForKey("min_hourly_rate"))
                        let mainCat_ID = self.theme.CheckNullValue(getReponse.objectForKey("parent_id"))
                        let mainCate_Name = self.theme.CheckNullValue(getReponse.objectForKey("parent_name"))
                        let quick_Pitch = self.theme.CheckNullValue(getReponse.objectForKey("quick_pitch"))
                        let category_type = self.theme.CheckNullValue(getReponse.objectForKey("category_type"))!
                       self.editCatDtls_Array.addObject(EditCategoryDetails.init(experience_Lvl:experience_Name! , subCat_ID: subCat_ID!, subCat_Name: subCateg_Name!, experience_ID: experience_ID!, expereince_Name: experience_Name!, hour_Rate: hour_Rate!, min_Rate: minHour_Rate!, mainCat_ID: mainCat_ID!, mainCat_Name: mainCate_Name!, quickPinch: quick_Pitch!,category:category_type))
                        
                        let categoryVC = self.storyboard?.instantiateViewControllerWithIdentifier("CategoryEditVC") as! CategoryEditVC
                        categoryVC.checkCategory = "Edit_Category"
                        categoryVC.editCatDtl_Array = self.editCatDtls_Array
                        self.navigationController?.pushViewController(categoryVC, animated: true)
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                
            }
            
        }
        
    }
    func GetDatasForBanking(){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        
        url_handler.makeCall(GetEditInfoUrl, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    
                    self.setDatasToEditView(responseObject!)
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                }
            }
        }
    }
    func setDatasToEditView(Dict:NSDictionary){
        let status:NSString=Dict["status"] as! NSString
        
        if(status == "1")
        {
            let resDict: NSDictionary = Dict.objectForKey("response") as! NSDictionary
            
            self.userImg.sd_setImageWithURL(NSURL(string:(resDict.objectForKey("image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            
            self.bannerImg.sd_setImageWithURL(NSURL(string:(resDict.objectForKey("image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderBig"))
            
            
            phoneTxtField.text=theme.CheckNullValue(resDict.objectForKey( "mobile_number" ))!
            phoneTxtField.placeholder = Language_handler.VJLocalizedString("mobile", comment: nil)
            cCodeTxtField.text=theme.CheckNullValue(resDict.objectForKey( "dial_code" ))!
            emailTxtField.text=theme.CheckNullValue(resDict.objectForKey( "email" ) )!
            emailTxtField.placeholder = Language_handler.VJLocalizedString("email", comment: nil)
            
            addressTxtField.text=theme.CheckNullValue(resDict.objectForKey( "address" ))!
            addressTxtField.placeholder =  Language_handler.VJLocalizedString("address", comment: nil)
            working_loca.text=theme.CheckNullValue(resDict.objectForKey( "working_location" ))!
            working_loca.placeholder =  Language_handler.VJLocalizedString("working_loc", comment: nil)
            
            
            stateTxtField.text=theme.CheckNullValue(resDict.objectForKey( "state" ))!
            stateTxtField.placeholder = Language_handler.VJLocalizedString("state", comment: nil)
            cityTxtField.text=theme.CheckNullValue(resDict.objectForKey( "city" ))!
            cityTxtField.placeholder = Language_handler.VJLocalizedString("city", comment: nil)
            
            countryTxtField.text=theme.CheckNullValue(resDict.objectForKey( "country" ))!
            countryTxtField.placeholder = Language_handler.VJLocalizedString("country", comment: nil)
            postalCodeTxtField.text=theme.CheckNullValue(resDict.objectForKey( "postal_code" ))!
            postalCodeTxtField.placeholder = Language_handler.VJLocalizedString("pincode", comment: nil)
            usernameTxtfield.text = theme.CheckNullValue(resDict.objectForKey( "username" ))!
            usernameTxtfield.placeholder =
                Language_handler.VJLocalizedString("pincode", comment: nil)
            radiusTxtfield.text = theme.CheckNullValue(resDict.objectForKey( "radius" ))!
            radiusTxtfield.placeholder = Language_handler.VJLocalizedString("radius", comment: nil)
            
        }
        else
        {
            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: kNetworkFail)
        }
    }
    
    
    @IBAction func EditProfileClickOption(sender: AnyObject) {
        
        
        
        
        let ImagePicker_Sheet = UIAlertController(title: nil, message: "Select Image", preferredStyle: .ActionSheet)
        
        let Camera_Picker = UIAlertAction(title: "Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.Camera_Pick()
        })
        let Gallery_Picker = UIAlertAction(title: "Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            //
            self.Gallery_Pick()
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        ImagePicker_Sheet.addAction(Camera_Picker)
        ImagePicker_Sheet.addAction(Gallery_Picker)
        ImagePicker_Sheet.addAction(cancelAction)
        
        self.presentViewController(ImagePicker_Sheet, animated: true, completion: nil)
    }
    
    
    func Camera_Pick()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            self.imagePicker = WDImagePicker.init(sourceType: .Camera)
            self.imagePicker.cropSize = CGSizeMake(280, 280)
            self.imagePicker.delegate = self
            self.imagePicker.resizableCropArea = true
            self.presentViewController(self.imagePicker.imagePickerController, animated: true, completion: nil)
        }
            
        else
        {
            Gallery_Pick()
        }
    }
    
    func Gallery_Pick()
    {
        
        self.imagePicker = WDImagePicker.init(sourceType: .PhotoLibrary)
        self.imagePicker.cropSize = CGSizeMake(280, 280)
        self.imagePicker.delegate = self
        self.imagePicker.resizableCropArea = true
        self.presentViewController(self.imagePicker.imagePickerController, animated: true, completion: nil)
        
    }
    
    
    func imagePicker(imagePicker: WDImagePicker, pickedImage: UIImage) {
        let image = UIImage.init(CGImage: pickedImage.CGImage!, scale: 0.25 , orientation:.Up)
        imagedata = UIImageJPEGRepresentation(image, 0.1)!;
        self.userImg.image = image
        self.hideImagePicker()
        self.uploadImageAndData()
    }
    
    func hideImagePicker() {
        self.imagePicker.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func blurBannerImg(){
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            bannerImg.backgroundColor = UIColor.clearColor()
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bannerImg.bounds
            blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            
            bannerImg.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        }
        else {
            bannerImg.backgroundColor = UIColor.blackColor()
        }
        userImg.layer.cornerRadius=userImg.frame.size.width/2
        //userImg.layer.borderWidth=0.75
        //userImg.layer.borderColor=PlumberThemeColor.CGColor
        userImg.layer.masksToBounds=true
    }
    
    func decorateTxtField(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name:UIKeyboardWillHideNotification, object: nil)
        
        phoneTxtField=roundTheViews(phoneTxtField) as! UITextField
        emailTxtField=roundTheViews(emailTxtField) as! UITextField
        
        emailTxtField.userInteractionEnabled = false
        usernameTxtfield=roundTheViews(usernameTxtfield) as! UITextField
        working_loca=roundTheViews(working_loca) as! UITextField
        
        
        radiusTxtfield=roundTheViews(radiusTxtfield) as! UITextField
        
        radiusTxtfield.userInteractionEnabled = true
        
        
        cCodeTxtField=roundTheViews(cCodeTxtField) as! UITextField
        addressTxtField=roundTheViews(addressTxtField) as! UITextField
        countryTxtField=roundTheViews(countryTxtField) as! UITextField
        postalCodeTxtField=roundTheViews(postalCodeTxtField) as! UITextField
        stateTxtField=roundTheViews(stateTxtField) as! UITextField
        cityTxtField=roundTheViews(cityTxtField) as! UITextField
        
        theme.SetPaddingView(usernameTxtfield)
        theme.SetPaddingView(radiusTxtfield)
        theme.SetPaddingView(working_loca)
        
        
        setMandatoryFields()
        
    }
    func setMandatoryFields(){
        for subView:UIView in editProfileScrollView.subviews{
            if(subView.isKindOfClass(SMIconLabel)){
                //                let lbl: SMIconLabel = (subView as? SMIconLabel)!
                //               // if !lbl.isEqual(routingLbl) {
                //                    lbl.icon = UIImage(named: "MandatoryImg")
                //                    lbl.iconPadding = 5
                //                    lbl.iconPosition = SMIconLabelPosition.Right
                //              //  }
            }else if(subView.isKindOfClass(UITextField)){
                let txtField: UITextField = (subView as? UITextField)!
                let arrow: UIView = UILabel()
                arrow.frame = CGRectMake(0, 0, 20, 20)
                txtField.leftView = arrow
                txtField.leftViewMode = UITextFieldViewMode.Always
            }
        }
    }
    
    func roundTheViews(oTxt:AnyObject)->AnyObject{
        oTxt.layer.cornerRadius=3
        oTxt.layer.borderWidth=0.5
        oTxt.layer.borderColor=UIColor.lightGrayColor().CGColor
        oTxt.layer.masksToBounds=true
        return oTxt
    }
    
    @IBAction func didClickEditImage(sender: AnyObject) {
        //        let actionSheet = UIActionSheet(title: "Select Image from", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        //        actionSheet.showInView(self.view)
    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex{
        case 1:
            Camera_Pick()
            break;
        case 2:
            Gallery_Pick()
            break;
        default:
            NSLog("Default");
            break;
        }
    }
    
    //    @IBAction func didClickBioUpdate(sender: AnyObject) {
    //        if(validateTxtFields("1")){
    //            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
    //            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
    //                "bio":"\(bioTxtView.text!)"]
    //            updateUserProfile(UpdateBioUrl, withDict: Param)
    //        }
    //    }
    
    @IBAction func didClickEmailUpdate(sender: AnyObject) {
        if(validateTxtFields("2")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "email":"\(emailTxtField.text!)"]
            updateUserProfile(UpdateEmailUrl, withDict: Param)
        }
    }
    
    @IBAction func didClickMobileUpdate(sender: AnyObject) {
        Contact=phoneTxtField.text!
        
        if(validateTxtFields("3")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "country_code":"\(cCodeTxtField.text!)",
                                     "mobile_number":"\(phoneTxtField.text!)"]
            updateUserProfile(UpdateMobileUrl, withDict: Param)
        }
    }
    
    @IBAction func didClickAddressUpdate(sender: AnyObject) {
        if(validateTxtFields("4")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId )",
                                     "address":"\(addressTxtField.text!)",
                                     "city":"\(cityTxtField.text!)",
                                     "state":"\(stateTxtField.text!)",
                                     "country":"\(countryTxtField.text!)",
                                     "postal_code":"\(postalCodeTxtField.text!)"]
            updateUserProfile(UpdateAddressUrl, withDict: Param)
        }
    }
    
    @IBAction func didclickWorkingLocUpdate(sender: AnyObject) {
        
        if(validateTxtFields("7")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "lat":"\(Addaddress_Data.working_Latitude)",
                                     "long":"\(Addaddress_Data.working_Longitude)"]
            updateUserProfile(UpdateWorkingLocation, withDict: Param)
        }
        
    }
    
    @IBAction func didClickAvailabilityUpdate(sender: AnyObject) {
        var daysCount:Int = 0
        var availabilityDict: [String : String] = Dictionary()
        let availArray = NSMutableArray()
        for (var j=1;j<=7;j += 1){
            let availObj = Availability()
            availObj.day = "\(weekFullDay[j])"
            availObj.isAfternoon = false
            availObj.isMorning = false
            availObj.isEvening = false
            availArray.addObject(availObj)
        }
        for (var i=1;i<=21;i += 1){
            let first = i
            let second = i+1
            let third = i+2
            i = third
            var arr = [Int]()
            if(selectedBtn.containsObject(first)){
                arr.append(first)
            }
            if(selectedBtn.containsObject(second)){
                arr.append(second)
            }
            if(selectedBtn.containsObject(third)){
                arr.append(third)
            }
            daysCount += 1
            for item in arr{
                switch (item%3){
                case 0:
                    let obj = availArray.objectAtIndex(daysCount-1) as! Availability
                    obj.isEvening = true
                    break;
                case 1:
                    let obj = availArray.objectAtIndex(daysCount-1) as! Availability
                    obj.isMorning = true
                    break;
                    
                case 2:
                    let obj = availArray.objectAtIndex(daysCount-1) as! Availability
                    obj.isAfternoon = true
                    break;
                default:
                    break;
                }
            }
        }
        var increment = 0
        for item  in availArray{
            
            let obj1 = item as! Availability
            
            if obj1.isMorning == true || obj1.isAfternoon == true || obj1.isEvening == true{
                availabilityDict["working_days[\(increment)][day]"] = "\(obj1.day)"
                if obj1.isMorning == true{
                    availabilityDict["working_days[\(increment)][hour][morning]"] = "1"
                }
                if obj1.isEvening == true{
                    availabilityDict["working_days[\(increment)][hour][evening]"] = "1"
                }
                if obj1.isAfternoon == true{
                    availabilityDict["working_days[\(increment)][hour][afternoon]"] = "1"
                }
                increment += 1
            }
        }
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        
        availabilityDict["provider_id"]="\(objUserRecs.providerId)"
        
        print(availabilityDict)
        updateWorkingDays(availabilityDict as NSDictionary)
        
        
    }
    
    func showProgressbar()
    {
        progressbar.show(message: "", style: BlueIndicatorStyle())
    }
    
    func updateWorkingDays(param:NSDictionary){
        self.showProgress()
        url_handler.makeCall(UpdateWorkingDays, param: param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil) {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else {
                if(responseObject != nil && responseObject?.count>0){
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1") {
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject?.objectForKey("message"))!, duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                        
                    }else{//alertImg
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject?.objectForKey("message"))!, duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)")
                    }
                    
                }else{
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                }
            }
            
        }
        
    }
    
    func DismissProgressbar()
    {
        progressbar.dismiss()
    }
    
    
    
    func uploadImageAndData(){
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        
        let param : NSDictionary =  ["provider_id"  :objUserRecs.providerId]
        let imageData = imagedata
        self.showProgressbar()
        
        Alamofire.upload(.POST, UpdateImageUrlUrl, headers:  ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(objUserRecs.providerId)"],multipartFormData: {
            multipartFormData in
            
            multipartFormData.appendBodyPart(data: imageData, name: "file", fileName: "file.png", mimeType: "")
            
            for (key, value) in param {
                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key as! String)
            }
            }, encodingCompletion: {
                encodingResult in
                
                switch encodingResult {
                    
                case .Success(let upload, _, _):
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let JSON = response.result.value {
                            self.DismissProgressbar()
                            print("JSON: \(JSON)")
                            
                            
                            let Status:NSString = self.theme.CheckNullValue(JSON.objectForKey("status"))!
                            let response:NSDictionary = JSON.objectForKey("response") as! NSDictionary
                            if(Status == "1")
                            {
                                
                                self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(response.objectForKey("msg"))!, ButtonTitle: kOk)
                                
                                self.userImg.sd_setImageWithURL(NSURL(string:(self.theme.CheckNullValue(response.objectForKey("image")!)!)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                                
                                
                                
                                
                            }
                            else
                            {
                                
                                self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(response.objectForKey("msg"))!, ButtonTitle: kOk)
                                
                                
                                
                            }
                            
                        }
                    }
                    
                case .Failure(let encodingError):
                    
                    //                      self.themes.AlertView("Image Upload Failed", Message: "Please try again", ButtonTitle: "Ok")
                    print(" the encodeing error is \(encodingError)")
                }
        })
    }
    
    
    
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //what happens when you cancel
        //which, in our case, is just to get rid of the photo picker which pops up
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func uploadImg(dict: NSDictionary){
        updateUserProfile(UpdateImageUrlUrl, withDict: dict)
    }
    
    
    @IBAction func didClickBackBtn(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func validateTxtFields (condition:NSString ) -> Bool{
        self.view.endEditing(true)
        var isOK:Bool=true
        if(condition=="1"){
            /*if(bioTxtView.text.characters.count==0){
             ValidationAlert(Language_handler.VJLocalizedString("bio_mand", comment: nil))
             isOK=false
             return isOK
             }*/
        }
        if(condition=="2"){
            if(emailTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("email_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }else if(theme.isValidEmail(emailTxtField.text!)){
                
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("valid_email_alert", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
        }
        if(condition=="3"){
            if(cCodeTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("country_code_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
            else if(phoneTxtField.text!.characters.count==0){
                
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("mobile_number_mand", comment: nil), ButtonTitle: kOk)
                
                
                isOK=false
                return isOK
            }
            
        }
        if(condition=="4"){
            if(addressTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("address_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            if(cityTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("city_mand", comment: nil), ButtonTitle: kOk)
                
                
                
                isOK=false
                return isOK
            }
            if(stateTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("state_mand", comment: nil), ButtonTitle: kOk)
                
                
                isOK=false
                return isOK
            }
            if(countryTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("country_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
            if(postalCodeTxtField.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("postal_code_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
        }
        
        if (condition == "5"){
            if(usernameTxtfield.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("username_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            
        }
        if (condition == "6")
        {
            if(radiusTxtfield.text!.characters.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("radius_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            if(condition=="7"){
                
                if(working_loca.text!.characters.count==0){
                    self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("working_mand", comment: nil), ButtonTitle: kOk)
                    isOK=false
                    return isOK
                }
                
                
            }
            
            
            
            
        }
        return isOK
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = editProfileScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+150
        editProfileScrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        editProfileScrollView.contentInset = contentInset
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(range.location==0 && string==" "){
            return false
        }
        return true
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(range.location==0 && text==" "){
            return false
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    
    func didClickenable(sender: UIButton) {
        
        if (selectedBtn.containsObject(sender.tag) ){
            selectedBtn.removeObject(sender.tag)
            sender.setBackgroundImage(UIImage.init(named: "tick_gray"), forState: UIControlState.Normal)
        }else{
            selectedBtn.addObject(sender.tag)
            sender.setBackgroundImage(UIImage.init(named: "tick"), forState: UIControlState.Normal)
        }
        print(selectedBtn)
        
    }
    @IBAction func didclickusername(sender: AnyObject) {
        if(validateTxtFields("5")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "user_name":"\(usernameTxtfield.text!)"]
            updateUserProfile(UpdateUsernameUrl, withDict: Param)
        }
        
    }
    
    @IBAction func didupdateradius(sender: AnyObject) {
        if(validateTxtFields("6")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "radius":"\(radiusTxtfield.text!)"]
            updateUserProfile(UpdateRadiusUrl, withDict: Param)
        }
        
    }
    
    
    
    @IBAction func didclickCategoryview(sender: AnyObject) {
        
        let categoryVC = storyboard?.instantiateViewControllerWithIdentifier("CategoryEditVC") as! CategoryEditVC
        categoryVC.checkCategory = "Add_Category"
        self.navigationController?.pushViewController(categoryVC, animated: true)
        
        
    }
    
    func updateUserProfile(statusType:NSString, withDict:NSDictionary){
        
        NSLog("Getsataus type=%@", withDict)
        self.showProgress()
        
        // print(Param)
        
        url_handler.makeCall(statusType, param: withDict) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        
                        if(statusType==UpdateImageUrlUrl){
                            self.userImg.sd_setImageWithURL(NSURL(string:(responseObject!.objectForKey( "response" )as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
                            
                            self.bannerImg.sd_setImageWithURL(NSURL(string:(responseObject!.objectForKey( "response" )as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderBig"))
                            
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("profile_image_update", comment: nil), ButtonTitle: kOk)
                            
                            
                            self.theme.saveUserImage(responseObject!.objectForKey( "response" ) as! NSString)
                            
                        }
                        if(statusType==UpdateBioUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("bio_update", comment: nil), ButtonTitle: kOk)
                            
                            
                        }
                        if(statusType==UpdateEmailUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("email_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType==UpdateWorkingLocation){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("location_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType==UpdateAddressUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("address_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType==UpdateMobileUrl){
                            
                            self.updateMob(responseObject!)
                            
                            
                            
                        }
                        
                        if (statusType == UpdateUsernameUrl)
                        {
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("username_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if (statusType == UpdateRadiusUrl)
                        {
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("radius_update", comment: nil), ButtonTitle: kOk)
                        }
                    }
                    else
                    {//alertImg
                        let image = UIImage(named: "alertImg")
                        
                        self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject?.objectForKey("response"))!, ButtonTitle: kOk)
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                }
            }
            
        }
    }
    
    
    
    func  ValidateOTP() {
        self.showProgress()
        
        // print(Param)
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                 "country_code":"\(cCodeTxtField.text!)",
                                 "mobile_number":"\(phoneTxtField.text!)",
                                 "otp":"\(tField.text!)"]
        
        url_handler.makeCall(UpdateMobileUrl, param: Param) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("mobile_number_update", comment: nil), ButtonTitle: kOk)
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        
        
    }
    
    func updateMob(responseObject:NSDictionary){
        
        let response  = self.theme.CheckNullValue(responseObject.objectForKey("response"))
        
        let otp:NSString = self.theme.CheckNullValue(responseObject.objectForKey( "otp" )as! NSString)!
        
        let otpStatus:NSString=self.theme.CheckNullValue(responseObject.objectForKey( "otp_status" )as! NSString)!
        
        otpStr = otp
        otp_status = otpStatus
        let alert = UIAlertController(title:Language_handler.VJLocalizedString("otp_placeholder", comment: nil), message: Language_handler.VJLocalizedString("otp_mobileupdate_desc", comment: nil), preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title:Language_handler.VJLocalizedString("cancel", comment: nil), style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title:Language_handler.VJLocalizedString("ok", comment: nil), style: .Default, handler:{ (UIAlertAction) in
            if self.tField.text == ""
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("OTP is mandatory", comment: nil), duration: 3, position: HRToastPositionDefault, title: "\(appNameJJ)");
            }
                
            else if self.tField.text == self.otpStr
            {
                self.ValidateOTP()
                
                
            }
            else{
                
                self.theme.AlertView("\(appNameJJ)", Message: Language_handler.VJLocalizedString("otp_alert", comment: nil), ButtonTitle: kOk)
            }
            
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    
    func getProviderCategory_Dtl(){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        url_handler.makeCall(viewProfile, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        
                        self.categoryArray.removeAllObjects()
                        if(responseObject?.objectForKey("response")?.objectForKey("details")!.count>0){
                            let categoryArr = responseObject?.objectForKey("response")?.objectForKey("category_details") as! NSArray
                            
                            for category_Arr in categoryArr{
                                let id = self.theme.CheckNullValue(category_Arr["_id"])
                                let name = self.theme.CheckNullValue(category_Arr["name"])
                                self.categoryArray.addObject(CategoryList.init(categoryName:name!,category_ID:id!))
                                //
                            }
                            self.category_TblView.reloadData()
                        }
                        else
                        {
                            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                
            }
            
        }
    }
    
    
    func configurationTextField(textField: UITextField!)
    {
        
        textField.placeholder = Language_handler.VJLocalizedString("otp_placeholder", comment: nil)
        
        tField = textField
        if otp_status == "development"
        {
            tField.text = self.otpStr as String
            tField.userInteractionEnabled = false
        }
        else{
            tField.text = ""
            tField.userInteractionEnabled = true
        }
    }
    
    
    
    
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    override func viewDidAppear(animated: Bool) {
        category_TblView.reloadData()
        self.editProfileScrollView.frame.size.height = self.view.frame.size.height - self.editProfileScrollView.frame.origin.y
        editProfileScrollView.contentSize=CGSizeMake(editProfileScrollView.frame.size.width,self.category_View.frame.origin.y+category_View.frame.size.height)
        self.view.addSubview(editProfileScrollView)
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        getProviderCategory_Dtl()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
}


