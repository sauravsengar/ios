//
//  CategoryEditVC.swift
//  PlumberJJ
//
//  Created by Casperon on 19/05/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class CategoryEditVC: RootBaseViewController,NIDropDownDelegate,UITextViewDelegate,UITableViewDelegate {
    @IBOutlet weak var edit_cat: UILabel!
    @IBOutlet weak var parent_cat: CustomLabel!
    @IBOutlet weak var cat_name: CustomLabel!
    @IBOutlet weak var exe_lev: CustomLabel!
    @IBOutlet weak var quik_pinch: CustomLabel!
    @IBOutlet var category_scroll: UIScrollView!
    @IBOutlet var quickPinch_TxtView: UITextView!
    @IBOutlet var expLevel_Btn: UIButton!
    @IBOutlet var hourly_Txt: UITextField!
    @IBOutlet var subCat_Btn: UIButton!
    @IBOutlet var mainCat_Btn: UIButton!
    @IBOutlet var addCatgry_Btn: CustomButton!
    @IBOutlet var title_Lbl: UILabel!

    @IBOutlet var hourlyRate_Lbl: CustomLabel!
   
    @IBOutlet var selectCat_tbl: UITableView!
    var dropDown :  NIDropDown = NIDropDown()
    var category_Array : NSMutableArray = NSMutableArray()
    var category_IdArray: NSMutableArray = NSMutableArray()
    var experience_Array:NSMutableArray = NSMutableArray()
    var experienceID_Array:NSMutableArray = NSMutableArray()
    var subCategory_Array : NSMutableArray = NSMutableArray()
    var subCategory_IdArray : NSMutableArray = NSMutableArray()
   
    var checkCategory:String = String()
     var selectedCat_ID : NSString = NSString()
    var selectSubCat_ID:String = String()
    var selectExp_ID:String = String()
    var drop_Selection:String = String()
    var category_Type :String = String()
    var min_Rate:Int = Int()
    var editCatDtl_Array:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        parent_cat.text=Language_handler.VJLocalizedString("Main_Category", comment: nil)
        hourlyRate_Lbl.text=Language_handler.VJLocalizedString("Hourly_Rate", comment: nil)
        exe_lev.text=Language_handler.VJLocalizedString("Experience_level", comment: nil)
        quik_pinch.text=Language_handler.VJLocalizedString("Quick_pinch", comment: nil)
        
      
        selectCat_tbl.hidden = true
        category_scroll.contentSize = CGSizeMake(self.view.frame.size.width, self.expLevel_Btn.frame.origin.y+self.expLevel_Btn.frame.size.height+150)
        cat_name.text = Language_handler.VJLocalizedString("Sub_Category", comment: nil)
      self.Done_Toolbar()
//        self.showProgress()
        self.GetCategoryList()
        selectCat_tbl.layer.borderWidth = 1.0
        selectCat_tbl.layer.cornerRadius = 5
        selectCat_tbl.layer.borderColor = UIColor.darkGrayColor().CGColor
    
      selectCat_tbl.registerNib(UINib(nibName: "CategorySelectTableViewCell", bundle: nil), forCellReuseIdentifier: "CategorySelect")
        // Do any additional setup after loading the view.
    }
    
    

    func Done_Toolbar()
    {
        
        //ADD Done button for Contatct Field
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor=UIColor.whiteColor()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: theme.setLang("done"), style: UIBarButtonItemStyle.Done, target: self, action: #selector(CategoryEditVC.doneButtonAction))
        doneToolbar.items = [flexSpace,done]
        
        doneToolbar.sizeToFit()
        
        hourly_Txt.inputAccessoryView = doneToolbar
        
    }
    
    
    func doneButtonAction()
    {
        hourly_Txt.resignFirstResponder()
        
      
    }
    
    func GetCategoryList(){
        let parameters:Dictionary=["":""]
        
        // print(Param)
        
        url_handler.makeCall(get_mainCategory, param: parameters) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                   
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    if Status == "1"
                         {
                            self.category_Array.removeAllObjects()
                            self.category_IdArray.removeAllObjects()
                            self.experience_Array.removeAllObjects()
                            self.experienceID_Array.removeAllObjects()
                            
                        let ResponseArr = responseObject!.objectForKey("response")as! NSArray
                        let experience_Arr = responseObject!.objectForKey("experiencelist")as! NSArray
                        for experience in experience_Arr{
                            
                        let exp_ID  = self.theme.CheckNullValue(experience["id"])
                            
                        let exp_Lvl = self.theme.CheckNullValue(experience["name"])
                            
                        self.experience_Array.addObject(exp_Lvl!)
                        self.experienceID_Array.addObject(exp_ID!)
                            
//                        self.experience_Array.addObject(CategoryExperience.init(experience_Lvl: exp_Lvl!, experience_ID: exp_ID!))
                        }
                           
                             for (_, element) in ResponseArr.enumerate() {
                            
                                
                                let category = self.theme.CheckNullValue(element.objectForKey("name"))!
                                let category_id = self.theme.CheckNullValue(element.objectForKey("id"))!
                                self.category_Array.addObject(category)
                                self.category_IdArray.addObject(category_id)
                                
                            }
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                    
                }
            }
            
        }
    }


   // pragma mark - MKDropdownMenuDataSource
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didclick_BtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    
    @IBAction func categoryBtn_Act(sender: AnyObject) {

        
      
        drop_Selection = "Main Category"
        if category_Array.count>0
        {
            selectCat_tbl.hidden = false
            selectCat_tbl.reloadData()
            
            selectCat_tbl.frame = CGRectMake(self.selectCat_tbl.frame.origin.x,self.mainCat_Btn.frame.origin.y+self.subCat_Btn.frame.size.height+5, self.selectCat_tbl.frame.size.width, self.expLevel_Btn.frame.minY - 20)
            var tableCellHeight = 44
            selectCat_tbl.frame.size.height = CGFloat(category_Array.count*tableCellHeight)
            if selectCat_tbl.frame.size.height > 250{
                selectCat_tbl.frame.size.height = 250
            }

        }
        else{
            self.view.makeToast(message: Language_handler.VJLocalizedString("There_are_no_one_main_category_to_show", comment: nil), duration: 3, position: HRToastPositionDefault, title: appNameJJ)
        }

    }
    
    func niDropDownDelegateMethod(sender: NIDropDown!, _ position: Int) {
    
      if drop_Selection == "Main Category"{
        print("\(sender.tag)")
        subCat_Btn.setTitle("Select Sub Category", forState: .Normal)
        selectedCat_ID =  self.category_IdArray[position] as! NSString
        subCat_API(selectedCat_ID as String)
        }
        else if drop_Selection == "Sub Category"{
        
        print("\(sender.tag)")
       // subCat_Btn.setTitle("Select Sub Category", forState: .Normal)
        selectSubCat_ID =  self.subCategory_IdArray[position]  as! String
        getSubcategory_Dtls(selectSubCat_ID)
       // subCat_API(selectedCat_ID as String)

       
        }
         else if drop_Selection == "Experience Category"{
          selectExp_ID =  self.experienceID_Array[position]  as! String
        }
    }

    
//    func niDropDownDelegateMethod(sender: NIDropDown!,position:Int) {
//        
//        print("\(sender.tag)")
//        print(mainCat_Btn.titleLabel!.text!)
//        
//        category_Array.containsObject(mainCat_Btn.titleLabel!.text!)
//        selectedCat_ID =  self.category_IdArray[sender.tag] as! NSString
//        subCat_API(selectedCat_ID as String)
//
//    //
//    }
    
    func getSubcategory_Dtls(subCat_ID:String){
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["subcategory_id":"\(subCat_ID)"]
        url_handler.makeCall(getSubCategory_Dtl, param: Param) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
            }
            else
            {
                if(responseObject != nil && responseObject?.count>0)
                {
                    let status:NSString=self.theme.CheckNullValue(responseObject?.objectForKey("status"))!
                    
                    if(status == "1")
                    {
                        let response = responseObject?.objectForKey("response") as! NSArray
                        if  response.count>0{
                            for responseDtl in response{
                                let getMin_Rate = self.theme.CheckNullValue(responseDtl.objectForKey("minrate"))
                                self.category_Type = self.theme.CheckNullValue(responseDtl.objectForKey("category_type"))!
                                self.min_Rate = Int(getMin_Rate!)!
                                if self.category_Type == "Flat Rate"
                                {
                                      self.hourlyRate_Lbl.text = "You Have Selected The Category As Flat Rate Type"
                                       self.hourly_Txt.text = "\(self.min_Rate)"
                                    self.hourly_Txt.userInteractionEnabled = false
                                    
                                }
                                else{
                                     self.hourly_Txt.text = ""
                                    self.hourlyRate_Lbl.text = "Hourly rate (Min rate $\(self.min_Rate))"
                                      self.hourly_Txt.placeholder = Language_handler.VJLocalizedString("Enter_Hourly_Rate", comment: nil)
                                     self.hourly_Txt.userInteractionEnabled = true
                                }
                              
                        }
               }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: Language_handler.VJLocalizedString("Network_Failure!!!", comment: nil))
                    }
                }
                
            }
            
        }
        
    }

    func subCat_API(selectedCat_ID:String){
        
        let parameters:Dictionary=["category_id":"\(selectedCat_ID)"]
        
        // print(Param)
        
        url_handler.makeCall(Get_SubCategory, param: parameters) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    if Status == "1"
                    {
                        self.subCategory_Array.removeAllObjects()
                        self.subCategory_IdArray.removeAllObjects()
                        
                        let ResponseArr = responseObject!.objectForKey("response")as! NSArray
                        
                        for (_, element) in ResponseArr.enumerate() {
                            
                            let category = self.theme.CheckNullValue(element.objectForKey("name"))!
                            let category_id = self.theme.CheckNullValue(element.objectForKey("id"))!
                            self.subCategory_Array.addObject(category)
                            self.subCategory_IdArray.addObject(category_id)
                           
                        }
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault, title: appNameJJ)
                    
                }
            }
            
        }

        
    }
    @IBAction func subCategoryBtn_Act(sender: AnyObject) {
//         sender.hideDropDown(mainCat_Btn)
//        sender.hideDropDown(subCat_Btn)
//        if  dropDown.hidden != true {
//             dropDown.hideDropDown(sender as! UIButton)
//        }
         drop_Selection = "Sub Category"
        if subCategory_Array.count>0
        {
            selectCat_tbl.hidden = false
            selectCat_tbl.reloadData()
            
            selectCat_tbl.frame = CGRectMake(self.selectCat_tbl.frame.origin.x,self.subCat_Btn.frame.origin.y+self.subCat_Btn.frame.size.height, self.selectCat_tbl.frame.size.width, self.subCat_Btn.frame.maxY)
            var tableViewCellHeight = 44
            selectCat_tbl.frame.size.height = CGFloat(subCategory_Array.count*tableViewCellHeight)
            if selectCat_tbl.frame.size.height > 250{
                selectCat_tbl.frame.size.height = 250
            }
        }
        else{
            self.view.makeToast(message: Language_handler.VJLocalizedString("There_are_no_one_subcategory_to_show", comment: nil), duration: 3, position: HRToastPositionDefault, title: appNameJJ)
        }
    }
    
    @IBAction func expLevel_Action(sender: UIButton) {
//        if  dropDown.hidden != true {
//            dropDown.hideDropDown(sender as! UIButton)
//        }
        drop_Selection = "Experience Category"
        if experience_Array.count>0
        {
            selectCat_tbl.hidden = false
            selectCat_tbl.reloadData()
            
            selectCat_tbl.frame = CGRectMake(self.selectCat_tbl.frame.origin.x,self.expLevel_Btn.frame.origin.y+self.subCat_Btn.frame.size.height, self.selectCat_tbl.frame.size.width, self.subCat_Btn.frame.maxY)
        
        }
    }
    
    
    @IBAction func addCatgry_Action(sender: UIButton) {
        
        
        let rate = Int(hourly_Txt.text!)
        if mainCat_Btn.titleLabel!.text! == "Select Category"{
            
            self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("Please_select_main_category", comment: nil)
, ButtonTitle: kOk)
            
            
        }
            
        else if subCat_Btn.titleLabel!.text! == "Select Sub Category"{
            self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("Please_select_sub_category", comment: nil)
, ButtonTitle: kOk)
            
           
        }
        else if quickPinch_TxtView.text == "" ||  quickPinch_TxtView.text == "Enter quick pinch"{
            self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("Quick_Pinch_field_was_empty", comment: nil)
, ButtonTitle: kOk)
            
        }
        else if hourly_Txt.text == "" {
            self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("Hourly_Rate_is_required", comment: nil)
, ButtonTitle: kOk)
        }
        else if rate<min_Rate{
            self.theme.AlertView("\(appNameJJ)", Message:"\(Language_handler.VJLocalizedString("Minimun_hourly_rate_is", comment: nil)) \(self.theme.getappCurrencycode())\(min_Rate)", ButtonTitle: kOk)
           
        }
        else if expLevel_Btn.titleLabel!.text! == "Select Level"{
            self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("Kindly_Select_your_Experience_Level.", comment: nil)
, ButtonTitle: kOk)
        }
        else{
       // let parameters:Dictionary=["category_id":"\(selectedCat_ID)"]
         let objUserRecs:UserInfoRecord=theme.GetUserDetails()
         let parameters:Dictionary=["tasker":"\(objUserRecs.providerId)","quickpitch":"\(quickPinch_TxtView.text!)","childid":"\(selectSubCat_ID)","parentcategory":"\(selectedCat_ID)","hourrate":"\(hourly_Txt.text!)","experience":"\(selectExp_ID)"]
       
        // print(Param)
         var url:String = String()
         if  checkCategory ==  "Edit_Category"{
            url =  update_Category
            }
         else{
             url =  add_Category
            }
        url_handler.makeCall(url, param: parameters) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                 self.theme.AlertView("\(appNameJJ)", Message:HRToastPositionDefault, ButtonTitle: kOk)
                           }
            else
            {
                if(responseObject != nil)
                {
                    let Dict:NSDictionary=responseObject!
                    
                    
                    let Status:NSString=self.theme.CheckNullValue(Dict.objectForKey("status"))!
                    if Status == "1"
                    {
                        let getResponse = responseObject?.objectForKey("response") as! String
                         self.theme.AlertView("\(appNameJJ)", Message:getResponse, ButtonTitle: kOk)
                       
                        self.navigationController?.popViewControllerAnimated(true)

                        
                    }
                    else{
                        let getResponse = responseObject?.objectForKey("response") as! String
                       self.theme.AlertView("\(appNameJJ)", Message:getResponse, ButtonTitle: kOk)
                    }
                }
                else
                {
                    self.theme.AlertView("\(appNameJJ)", Message:kErrorMsg, ButtonTitle: kOk)
                }
            }
            
        }
        }
        
    }
    
    
    func setEditCat_Details(){
        
        mainCat_Btn.userInteractionEnabled = false
        subCat_Btn.userInteractionEnabled = false
        self.hourly_Txt.userInteractionEnabled = false
        addCatgry_Btn.setTitle("\(Language_handler.VJLocalizedString("update", comment: nil))", forState: .Normal)
         title_Lbl.text =  Language_handler.VJLocalizedString("Edit_Category", comment: nil)
        let getCurrentEdit_Dtl = editCatDtl_Array[0] as! EditCategoryDetails
        mainCat_Btn.setTitle(getCurrentEdit_Dtl.mainCat_Name, forState: .Normal)
        subCat_Btn.setTitle(getCurrentEdit_Dtl.subCat_Name, forState: .Normal)
        quickPinch_TxtView.text = getCurrentEdit_Dtl.quickPinch
        min_Rate = Int(getCurrentEdit_Dtl.min_Rate)!
        
          self.hourly_Txt.text = "\(min_Rate)"
        if getCurrentEdit_Dtl.Category_type == "Flat Rate"
        {
            self.hourlyRate_Lbl.text = "You Have Selected The Category As Flat Rate Type"
          
            
            
        }
        else{
            self.hourlyRate_Lbl.text = "Hourly rate (Min rate \(self.theme.getappCurrencycode())\(self.min_Rate))"
            
        }

      //  self.hourlyRate_Lbl.text = "Hourly rate (Min rate $\(self.min_Rate))"
      //  hourly_Txt.placeholder = "Min rate $\(getCurrentEdit_Dtl.min_Rate)"
        
        hourly_Txt.text = getCurrentEdit_Dtl.hour_Rate
        expLevel_Btn.setTitle(getCurrentEdit_Dtl.expereince_Name, forState: .Normal)
        selectExp_ID = getCurrentEdit_Dtl.experience_ID
        selectSubCat_ID = getCurrentEdit_Dtl.subCat_ID
        selectedCat_ID = getCurrentEdit_Dtl.mainCat_ID
        hourly_Txt.backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        hourly_Txt.userInteractionEnabled = false
    }
    
    func setAddCatTxtAlign(){
        
        addCatgry_Btn.setTitle(Language_handler.VJLocalizedString("submit", comment: nil), forState: .Normal)
      
        mainCat_Btn.setTitle(Language_handler.VJLocalizedString("Select_Category", comment: nil), forState: .Normal)
        subCat_Btn.setTitle(Language_handler.VJLocalizedString("Select_Sub_Category", comment: nil), forState: .Normal)
        expLevel_Btn.setTitle(Language_handler.VJLocalizedString("Select_level", comment: nil), forState: .Normal)
        
        mainCat_Btn.userInteractionEnabled = true
        subCat_Btn.userInteractionEnabled = true
        title_Lbl.text = Language_handler.VJLocalizedString("Add_Category", comment: nil)
     quickPinch_TxtView.text = Language_handler.VJLocalizedString("pinch_manditory", comment: nil)
        
        quickPinch_TxtView.textColor = UIColor.lightGrayColor()
       
    }
    
    func setUI_Alignments(){
        
        quickPinch_TxtView.layer.borderWidth = 1
        quickPinch_TxtView.layer.cornerRadius = 8
        hourly_Txt.layer.cornerRadius = 8
        hourly_Txt.layer.borderWidth = 1
        mainCat_Btn.layer.cornerRadius = 8
        mainCat_Btn.layer.borderWidth = 1
        mainCat_Btn.layer.borderColor = PlumberThemeColor.CGColor
        subCat_Btn.layer.cornerRadius = 8
        subCat_Btn.layer.borderWidth = 1
        subCat_Btn.layer.borderColor = PlumberThemeColor.CGColor
        

        expLevel_Btn.layer.borderWidth = 1
        expLevel_Btn.layer.cornerRadius = 8
        expLevel_Btn.layer.borderColor = PlumberThemeColor.CGColor
        
    }
    
    override func viewWillAppear(animated: Bool) {
        setUI_Alignments()
        let paddingView: UIView = UIView(frame: CGRectMake(0, 0, 25, hourly_Txt.frame.size.height))
        let curency_lbl: UILabel = UILabel(frame: CGRectMake(10, 0, 25, hourly_Txt.frame.size.height))
       
        curency_lbl.text = theme.setLang("currency_Syb")
        paddingView.addSubview(curency_lbl)
       print("111111" , curency_lbl.text)
        hourly_Txt.leftView = paddingView
        hourly_Txt.leftViewMode = UITextFieldViewMode.Always;
        if checkCategory ==  "Edit_Category"{
           
            setEditCat_Details()
           
        }
        else{
            setAddCatTxtAlign()
            }
    
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Language_handler.VJLocalizedString("pinch_manditory", comment: nil)

            textView.textColor = UIColor.blackColor()
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  drop_Selection == "Main Category"
        {
            return self.category_Array.count
            
        }
        else if drop_Selection == "Sub Category"{
            
            return  self.subCategory_Array.count
        }
        else if  drop_Selection == "Experience Category"
        {
            return  self.experience_Array.count

        }
      else{
            return 0
            
        }
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
    let Cell = tableView.dequeueReusableCellWithIdentifier("CategorySelect") as! CategorySelectTableViewCell
          if  drop_Selection == "Main Category"
          {
       Cell.categorylable.text = category_Array.objectAtIndex(indexPath.row) as? String
        }
         else if drop_Selection == "Sub Category"{
               Cell.categorylable.text = subCategory_Array.objectAtIndex(indexPath.row) as? String
        }
          else if  drop_Selection == "Experience Category"
          {
            Cell.categorylable.text = experience_Array.objectAtIndex(indexPath.row) as? String
            
        }
        return Cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
      
            
            if  drop_Selection == "Main Category"
            {
                mainCat_Btn.setTitle("\(category_Array.objectAtIndex(indexPath.row))", forState:.Normal)
                selectCat_tbl.hidden = true
                selectedCat_ID = category_IdArray.objectAtIndex(indexPath.row) as! String
                  subCat_API(category_IdArray.objectAtIndex(indexPath.row) as! String)
            }
            else if drop_Selection == "Sub Category"{
                subCat_Btn.setTitle("\(subCategory_Array.objectAtIndex(indexPath.row))", forState:.Normal)
                selectCat_tbl.hidden = true
                selectSubCat_ID = subCategory_IdArray.objectAtIndex(indexPath.row) as! String
                getSubcategory_Dtls(subCategory_IdArray.objectAtIndex(indexPath.row) as! String)

                
            }
            else if  drop_Selection == "Experience Category"
            {
                expLevel_Btn.setTitle("\(experience_Array.objectAtIndex(indexPath.row))", forState:.Normal)
               selectExp_ID = experienceID_Array.objectAtIndex(indexPath.row) as! String
                selectCat_tbl.hidden = true
                
            }
    }
    
//    func textViewDidBeginEditing(textView: UITextView) {
//        code
//    }
    /*
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
