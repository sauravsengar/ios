//
//  CustomButton.swift
//  Plumbal
//
//  Created by Casperon Tech on 07/11/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit

//MARK: - Custom Button

class CustomButton: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
         self.backgroundColor = PlumberThemeColor
         self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberLargeFont

     }
}

class CustomButtonThemeColor: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberLargeBoldFont
        
    }
}
class CustomButtonTitle: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.backgroundColor = PlumberLightGrayColor
        self.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberSmallFont
        
    }
}

class TextColorButton: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(themes.ThemeColour(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberSmallFont

    }
}

class TextColorButtonWhite: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberMediumFont
        
    }
}

class CustomButtonBold: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberMediumBoldFont
        
    }
}

class CustomButtonRed: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberMediumBoldFont
        
    }
}

class CustomButtonGray: UIButton {
    var themes:Theme=Theme()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        self.titleLabel?.font = PlumberLargeBoldFont
        
    }
}


//MARK: - Custom TextField

class CustomTextField:UITextField{
    var themes:Theme=Theme()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = UIColor.whiteColor()
        self.font = PlumberMediumFont

     }
}

class CustomTextFieldBlack:UITextField{
    var themes:Theme=Theme()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = themes.ThemeColour()
        self.layer.borderColor=themes.Lightgray().CGColor
        self.layer.borderWidth=0.8
        self.font = PlumberMediumFont
        
    }
}

class CustomTextBlack:UITextField{
    var themes:Theme=Theme()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor = themes.ThemeColour()
        self.font = PlumberMediumFont
        
    }
}
//MARK: - Custom Label

class CustomLabel: UILabel {
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        self.textColor=theme.ThemeColour()
        self.font = PlumberMediumBoldFont

    }
}

class CustomLabelThemeColor: UILabel {
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        self.textColor=PlumberThemeColor
        self.font = PlumberMediumBoldFont
        
    }
}

class CustomLabelGray: UILabel {
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        self.textColor=UIColor.darkGrayColor()
        self.font = PlumberMediumFont
        
    }
}

class CustomLabelGraySmall: UILabel {
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        self.textColor=UIColor.darkGrayColor()
        self.font = PlumberSmallFont
        
    }
}

class CustomLabelLightGray: UILabel {
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)!
        self.textColor=UIColor.lightGrayColor()
        self.font = PlumberMediumFont
        
    }
}

class CustomLabelWhite:UILabel{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor=UIColor.whiteColor()
        self.font = PlumberMediumFont
    }
}

class CustomLabelRed:UILabel{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.textColor=UIColor.redColor()
        self.font = PlumberMediumFont
    }
}
