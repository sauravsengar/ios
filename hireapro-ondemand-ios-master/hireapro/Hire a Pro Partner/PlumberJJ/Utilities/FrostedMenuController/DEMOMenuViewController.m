//
//  DEMOMenuViewController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOMenuViewController.h"


#import "UIViewController+REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "MenuTableViewCell.h"
#import <PlumberJJ-Swift.h>
@interface DEMOMenuViewController (){
    Theme * objTheme;
    
    SocketIOManager *connectSocket;
      Languagehandler * objLanguagehandler;
    
    
}

@end

@implementation DEMOMenuViewController
@synthesize titleArray;
@synthesize ImgArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LogoutMethod1:) name:@"Logout" object:nil];
    
}









-(void) viewWillAppear:(BOOL)animated
{
    
       objLanguagehandler=[[Languagehandler alloc]init];


    
    titleArray=[[NSMutableArray alloc]initWithObjects:[objLanguagehandler VJLocalizedString:@"home" comment:nil],[objLanguagehandler VJLocalizedString:@"my_jobs" comment:nil],[objLanguagehandler VJLocalizedString:@"transactions" comment:nil],[objLanguagehandler VJLocalizedString:@"notifications" comment:nil],[objLanguagehandler VJLocalizedString:@"reviews" comment:nil],[objLanguagehandler VJLocalizedString:@"chat_support" comment:nil],[objLanguagehandler VJLocalizedString:@"change_password" comment:nil],[objLanguagehandler VJLocalizedString:@"banking_details" comment:nil],[objLanguagehandler VJLocalizedString:@"chat" comment:nil], [objLanguagehandler VJLocalizedString:@"about" comment:nil],nil];
    
    
    
//    titleArray=[[NSMutableArray alloc]initWithObjects:[objLanguagehandler VJLocalizedString:@"home" comment:nil],[objLanguagehandler VJLocalizedString:@"my_jobs" comment:nil],[objLanguagehandler VJLocalizedString:@"transactions" comment:nil],[objLanguagehandler VJLocalizedString:@"notifications" comment:nil],[objLanguagehandler VJLocalizedString:@"reviews" comment:nil],[objLanguagehandler VJLocalizedString:@"change_password" comment:nil],[objLanguagehandler VJLocalizedString:@"banking_details" comment:nil],[objLanguagehandler VJLocalizedString:@"chat" comment:nil], [objLanguagehandler VJLocalizedString:@"about" comment:nil],nil];

    
    ImgArray=[[NSMutableArray alloc]initWithObjects:@"MenuHome",@"MenuOrders",@"Transaction",@"notification",@"review",@"Chatsupport",@"MenuProfile",@"MenuBanking",@"ChatImg",@"About-30", nil];
////
//    ImgArray=[[NSMutableArray alloc]initWithObjects:@"MenuHome",@"MenuOrders",@"Transaction",@"notification",@"review",@"MenuProfile",@"MenuBanking",@"ChatImg",@"About-30", nil];
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorColor = [UIColor colorWithRed:210/255.0f green:210/255.0f blue:210/255.0f alpha:1.0f];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    objTheme=[[Theme alloc]init];
    
//    Theme * objrec=(Theme *)[objTheme GetUserName];
    UserInfoRecord * objrec=(UserInfoRecord *)[objTheme GetUserDetails];
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 184.0f)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, 100, 100)];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        NSString * str=[self getUserImage];
       // NSString * new=[self getUserName];
        
        NSLog(@"get userimage=%@",str);
        
        
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"PlaceHolderSmall"]];
        
        //        [imageView loadImageFromURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"PlaceHolderSmall"] cachingKey:trimmedString];
        
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 50.0;
        imageView.layer.borderColor = [UIColor colorWithRed:248/255.0f green:130/255.0f blue:4/255.0f alpha:1.0f].CGColor;
        imageView.layer.borderWidth = 3.0f;
        imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
        imageView.layer.shouldRasterize = YES;
        imageView.clipsToBounds = YES;
        UIButton *EditprofilePic = [[UIButton alloc]initWithFrame:CGRectMake(0, 40, 100, 100)];
        EditprofilePic.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [EditprofilePic addTarget:self action:@selector(EditbtnTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 150, 0, 24)];
 //       label.text = objrec.GetUserName;
        label.text = objrec.providerName;
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        [label sizeToFit];
        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [view addSubview:imageView];
        [view addSubview:label];
        [view addSubview:EditprofilePic];
        view;
    });
    
}
-(void)viewDidAppear:(BOOL)animated
{
    
}


-(IBAction)EditbtnTapped:(id)sender{
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];

    MyProfileViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MYProfileVCSID"];
    navigationController.viewControllers = @[homeViewController];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Delegate


-(void)buttonClicked:(id)sender{
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];

    MyProfileViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MYProfileVCSID"];
    navigationController.viewControllers = @[homeViewController];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];

}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return nil;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 34)];
    view.backgroundColor = [UIColor colorWithRed:167/255.0f green:167/255.0f blue:167/255.0f alpha:0.6f];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
    label.text = @"Friends Online";
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [label sizeToFit];
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];
    
    if ( indexPath.row == 0) {
        HomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVCSID"];
        navigationController.viewControllers = @[homeViewController];
    } else if(indexPath.row==1) {
        MyJobsViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyJobsVCSID"];
        navigationController.viewControllers = @[homeViewController];
    }
    else if (indexPath.row == 2)
    {
        
        TransactionVC *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionVC"];
        navigationController.viewControllers = @[homeViewController];

    }
    else if (indexPath.row == 3)
    {
        NotificationsVCViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Notification"];
        navigationController.viewControllers = @[homeViewController];
    }
    else if (indexPath.row == 4)
    {
        ReviewViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"review"];
        navigationController.viewControllers = @[homeViewController];

    }
    else if(indexPath.row==5) {
        ChatSupportViewController *homeview = [self.storyboard instantiateViewControllerWithIdentifier:@"chatsupport"];
        navigationController.viewControllers = @[homeview];
        
    }
    
    else if(indexPath.row==6) {
        
        ChangePasswordViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Changepassword"];
        navigationController.viewControllers = @[homeViewController];
    
       
    }
//    else if(indexPath.row==6) {
//        BankingInfoViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BankingDetailVCSID"];
//        navigationController.viewControllers = @[homeViewController];
//    }
    else if(indexPath.row==7) {
        BankingInfoViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"bankVCID"];
        navigationController.viewControllers = @[homeViewController];
    }
       else if(indexPath.row==8) {
        ChatListViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatListVcSID"];
        navigationController.viewControllers = @[homeViewController];

        
    } else if(indexPath.row==9) {
        AboutUSViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutusVCSID"];
        navigationController.viewControllers = @[homeViewController];
        
        
    }

    else if(indexPath.row==10) {
        [self performSelector:@selector(openEmailfeedback) withObject:self afterDelay:0.3];
    }
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}
-(void)openEmailfeedback{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"feedBackEmail"
     object:self userInfo:nil];
}


#pragma mark -
#pragma mark UITableView Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==[titleArray count]){
        return 120;
    }
    return 65;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [titleArray count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *lastCellIdentifier = @"LastCellIdentifier";
     static NSString *NormalCellIdentifier = @"MenuListIdentifier";
    
    if(indexPath.row==([titleArray count])){ //This is last cell so create normal cell
        UITableViewCell *lastcell = [tableView dequeueReusableCellWithIdentifier:lastCellIdentifier];
        if(!lastcell){
            lastcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:lastCellIdentifier];
            CGRect frame = CGRectMake((self.tableView.frame.size.width/2)-(200/2),40,200,50);
            UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [aButton addTarget:self action:@selector(btnAddRowTapped:) forControlEvents:UIControlEventTouchUpInside];
            aButton.frame = frame;
            aButton.layer.cornerRadius=5;
            aButton.layer.masksToBounds=YES;
             [aButton setTitle:[objLanguagehandler VJLocalizedString:@"logout" comment:nil] forState:UIControlStateNormal];
//            aButton=[Theme setBoldFontForButton:aButton];
            aButton.backgroundColor=[UIColor colorWithRed:248/255.0f green:130/255.0f blue:4/255.0f alpha:1.0f];
            aButton.titleLabel.textColor=[UIColor whiteColor];
            [lastcell addSubview:aButton];
            lastcell.separatorInset = UIEdgeInsetsMake(0.f, lastcell.bounds.size.width, 0.f, 0.f);
        }
        return lastcell;
    }else{
        MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NormalCellIdentifier];
        if (cell == nil) {
            cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:NormalCellIdentifier];
        }
        
        cell.titleLbl.text=[titleArray objectAtIndex:indexPath.row];
        cell.IconImgView.image=[UIImage imageNamed:[ImgArray objectAtIndex:indexPath.row]];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
  
        return cell;
    }
    
}
-(NSString*)getUserImage
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"userInfoImgDictKey"];
}
-(NSString*)getUserName{
      return [[NSUserDefaults standardUserDefaults]objectForKey:@"userNameKey"];
}




-(void)LogoutMethod
{
    

    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"logout" object:nil];
    
 
}

-(IBAction)btnAddRowTapped:(id)sender{
  
    [self LogoutMethod];
}
@end
