//
//  Theme.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/17/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

class Theme: NSObject {
    

    let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
    }
    
        
  

    func CheckNullValue(var value : AnyObject?) -> String? {
        var  pureStr:String=""
        if value is NSNull{
            return pureStr
        }
        else {
            if (value == nil){
                value=""
            }
            pureStr = String(value! )
            return pureStr
        }
        
        
    }
    
    func calculateHeightForString(inString:String) -> CGFloat {
        let messageString = inString
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(16.0)])
        let rect:CGRect = attrString!.boundingRectWithSize(CGSizeMake(300.0,CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, context:nil )//hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height  //to include button's in your tableview
    }

    
    func Lightgray()->UIColor{
        return UIColor(red:216.0/255.0, green:216.0/255.0 ,blue:216.0/255.0, alpha:1.0)
    }
    
    func ThemeColour()->UIColor
    {
        var Theme_Color:UIColor!=UIColor()
        Theme_Color=nil
        
        
        Theme_Color = UIColor(red:0.14, green:0.12, blue:0.13, alpha:0.8)
        
        return Theme_Color
        
    }

    func additionalThemeColour()->UIColor
    {
        var Theme_Color:UIColor!=UIColor()
        Theme_Color=nil
        
        
        Theme_Color = UIColor(red:0.14, green:0.12, blue:0.13, alpha:0.8)
        
        return Theme_Color
        
    }
    

    
    func removeRotation( image: UIImage) -> UIImage {
        if image.imageOrientation == .Up {
            return image
        }
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
    //[self drawInRect:(CGRect){0, 0, self.size}];
        image.drawInRect(CGRect.init(origin:CGPoint(x: 10, y: 20), size:image.size ))

        var normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }
     func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let range = testStr.rangeOfString(emailRegEx, options:.RegularExpressionSearch)
        let result = range != nil ? false : true
        return result
    }
    func SaveDeviceTokenString(dToken:NSString) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let akey = "deviceTokenKey"
        userDefaults.setValue(dToken, forKey: akey)
        userDefaults.synchronize()
    }
    
    func getAddressForLatLng(latitude: String, longitude: String , status : String)->String {
        var fullAddress : NSString = NSString()
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(googleApiKey)")
        print(url)
        let data = NSData(contentsOfURL: url!)
        if data != nil{
            let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address = result[0]["address_components"] as? NSArray {
                        print("get current location \(result[0]["address_components"])")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for item in address{
                            let item1 = item["types"] as! NSArray
                            
                            if((item1.objectAtIndex(0) as! String == "street_number") || (item1.objectAtIndex(0) as! String == "premise") || (item1.objectAtIndex(0) as! String == "route")) {
                                let number1 = item["long_name"]
                                streetNameStr.appendString("\(number1)")
                                street = streetNameStr  as String
                                
                            }else if(item1.objectAtIndex(0) as! String == "locality"){
                                let city1 = item["long_name"]
                                city = city1 as! String
                                locality = ""
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_2" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                locality = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "administrative_area_level_1" || item1.objectAtIndex(0) as! String == "political") {
                                let city1 = item["long_name"]
                                state = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "country")  {
                                let city1 = item["long_name"]
                                country = city1 as! String
                            }else if(item1.objectAtIndex(0) as! String == "postal_code" ) {
                                let city1 = item["long_name"]
                                zipcode = city1 as! String
                            }
                            
                            if status == "short"
                            {
                                 fullAddress = "\(street)$\(locality)"
                            }
                            else
                            {
                                 fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)"
                                
                            }
                            
                           
                            if let address = result[0]["formatted_address"] as? String{
                                return address
                            }else{
                                return fullAddress as String
                            }
                        }
                    }
                }
            }
        }
        return ""
    }
    
    
    
   
    func saveLanguage(var str:NSString) {
        
      //  str = "en"
        if(str.isEqualToString("ta"))      {
            str="ta"
        }
        if(str.isEqualToString("en")) {
            str="en"
        }
        NSUserDefaults.standardUserDefaults().setObject(str, forKey: "LanguageName")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    func setLang(str:String)->String {
        return Language_handler.VJLocalizedString(str, comment: nil)
    }
    
    func SetLanguageToApp(){
        let savedLang=NSUserDefaults.standardUserDefaults().objectForKey("LanguageName") as! NSString
        if(savedLang == "ta") {
            Language_handler.setApplicationLanguage(Language_handler.TamilLanguageShortName)
        }
        if(savedLang == "en"){
            
            Language_handler.setApplicationLanguage(Language_handler.EnglishUSLanguageShortName)
        }
    }
    
    func AlertView(title:NSString,Message:NSString,ButtonTitle:NSString){
        if(Message == kErrorMsg){
            RKDropdownAlert.title(appNameJJ, message: Message as String, backgroundColor: UIColor.whiteColor() , textColor: UIColor.redColor())
            return
        }
        RKDropdownAlert.dismissAllAlert()
        RKDropdownAlert.title(appNameJJ, message: Message as String, backgroundColor: UIColor.whiteColor() , textColor: PlumberThemeColor)
    }
    func GetDeviceToken()->NSString{
        
        let akey = "deviceTokenKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let data0 = userDefaults.valueForKey(akey)
        let deviceTokenStr = data0 as! NSString!
        return CheckNullValue(deviceTokenStr)!
        
    }
    func saveUserDetail(userDict:NSDictionary) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let akey = "userInfoDictKey"
        userDefaults.setObject(userDict, forKey: akey)
        userDefaults.synchronize()
        if(userDict.count>0){
            saveUserImage(userDict.objectForKey("provider_image") as! String)
            //saveUserName(userDict.objectForKey("provider_name") as! String)
            
            
        }
    }
    
    
    
    
//    func saveUserName( imgStr:NSString){
//        
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        let akey = "userNameKey"
//        userDefaults.setObject(imgStr, forKey: akey)
//        userDefaults.synchronize()
//    }
    
//    func GetUserName ()->NSString
//    {
//        let akey = "userNameKey"
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        var data0 = userDefaults.stringForKey(akey)
//        if(data0==nil){
//            data0=""
//        }
//        let userDict:String = data0!
//        return userDict
//    }

    
    func saveUserImage(var imgStr:NSString){
        if(imgStr.length==0){
            imgStr=""
        }
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let akey = "userInfoImgDictKey"
        userDefaults.setObject(imgStr, forKey: akey)
        userDefaults.synchronize()
    }
    
    func GetUserImage()->NSString{
        
        let akey = "userInfoImgDictKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        var data0 = userDefaults.stringForKey(akey)
        if(data0==nil){
            data0=""
        }
        let userDict:String = data0!
        return userDict
    }
    
    func isUserLigin()->Bool{
        let akey = "userInfoDictKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let data0 = userDefaults.dictionaryForKey(akey)
        if(data0==nil || data0?.count==0){
            return false
        }else{
            return true
            
        }
    }
    func GetUserDetails()->UserInfoRecord{
        
        let akey = "userInfoDictKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let data0 = userDefaults.dictionaryForKey(akey)
        let userDict = data0 as NSDictionary!
        
       
        
       let objUserRec:UserInfoRecord=UserInfoRecord(prov_name:self.CheckNullValue(userDict.objectForKey("provider_name"))! , prov_ID: self.CheckNullValue(userDict.objectForKey("provider_id"))!, prov_Image: self.CheckNullValue(userDict.objectForKey("provider_image"))!, prov_Email: self.CheckNullValue(userDict.objectForKey("email"))!, prov_mob: self.CheckNullValue(userDict.objectForKey("email"))!)
        
        return objUserRec
    }
    
    /*new*/
    func GetBankDetails()->BankRecord{
        
        let akey = "userInfoDictKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let data0 = userDefaults.dictionaryForKey(akey)
        let userDict = data0 as NSDictionary!
        
        
        
        let objBankRec:BankRecord=BankRecord(prov_id: self.CheckNullValue(userDict.objectForKey("userId"))!)
        
        return objBankRec
    }
    
    func saveBankDetails(userDict:NSDictionary) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let akey = "userInfoDictKey"
        userDefaults.setObject(userDict, forKey: akey)
        userDefaults.synchronize()
        if(userDict.count>0){
            //sav(userDict.objectForKey("provider_image") as! String)
            saveBankId(userDict.objectForKey("userId")as! String)
            //saveUserName(userDict.objectForKey("provider_name") as! String)
            
            
        }
    }
    
    func saveBankId( pro:NSString){
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let akey = "userNameKey"
        userDefaults.setObject(pro, forKey: akey)
        userDefaults.synchronize()
    }
    
    func GetBankId ()->NSString
    {
        let akey = "userNameKey"
        let userDefaults = NSUserDefaults.standardUserDefaults()
        var data0 = userDefaults.stringForKey(akey)
        if(data0==nil){
            data0=""
        }
        let userDict:String = data0!
        return userDict
    }
    /*new*/
    
   
    
    func getCurrencyCode(code:String)->NSString{
        
        
//        let localeComponents = [NSLocaleCurrencyCode: code]
//        let localeIdentifier = NSLocale.localeIdentifierFromComponents(localeComponents)
//        let locale = NSLocale(localeIdentifier: localeIdentifier)
//        var currencySymbol = locale.objectForKey(NSLocaleCurrencySymbol) as! String
////        
////
////        
////        
//        var fmtr = NSNumberFormatter()
////        var locale = self.findLocaleByCurrencyCode(currencyCode)
////        var currencySymbol = ""
////        if locale {
////
////        }
//        fmtr.locale = locale
//        fmtr.numberStyle = .CurrencyStyle
//        currencySymbol = fmtr.currencySymbol!
//        if currencySymbol.characters.count > 1 {
//            //currencySymbol = currencySymbol.substringToIndex(currencySymbol.characters.last)
//            currencySymbol = currencySymbol.substringFromIndex(currencySymbol.startIndex.advancedBy(currencySymbol.characters.count - 1))
//
//        }
//        return currencySymbol
        
        
        
        let currencyCode = code
        
        let currencySymbols  = NSLocale.availableLocaleIdentifiers()
            .map { NSLocale(localeIdentifier: $0) }
            .filter {
                if let localeCurrencyCode = $0.objectForKey(NSLocaleCurrencyCode) as? String {
                    return localeCurrencyCode == currencyCode
                } else {
                    return false
                }
            }
            .map {
                ($0.localeIdentifier, $0.objectForKey(NSLocaleCurrencySymbol)!)
                
                
        }
        
        print( currencySymbols.last?.1)
        return CheckNullValue((currencySymbols.first?.1))!

    }
    func saveJaberPassword(JaberPassword: String) {
        NSUserDefaults.standardUserDefaults().setObject(JaberPassword, forKey: "JaberPassword")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getJaberPassword() -> String? {
        return (NSUserDefaults.standardUserDefaults().objectForKey("JaberPassword") as? String)
        
    }
    
    func getAppName() -> String {
        return NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String

        
    }
    func convertIntToString(integer : Int) -> NSString {
        var str:NSString = NSString()
        str = "\(integer)"
        return str
    }
    
    func saveAvailable_satus(Available : String) {
        NSUserDefaults.standardUserDefaults().setObject(Available, forKey: "Availability")
        NSUserDefaults.standardUserDefaults().synchronize()

    }
    
    func getAvailable_satus() -> String {
        
        if NSUserDefaults.standardUserDefaults().objectForKey("Availability") != nil
        {
        return (NSUserDefaults.standardUserDefaults().objectForKey("Availability") as? String)!
        }
        else
        {
            return ""
        }
    }
    
    
    func saveappCurrencycode(code: String) {
        NSUserDefaults.standardUserDefaults().setObject(code, forKey: "currencyCode")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    func getappCurrencycode() -> String! {
        
        if NSUserDefaults.standardUserDefaults().objectForKey("currencyCode") != nil
        {
            return NSUserDefaults.standardUserDefaults().objectForKey("currencyCode") as? String
        }
        else
        {
            return ""
        }
    }

    func SetPaddingView (text:UITextField) -> UIView
    {
        let paddingView = UIView(frame:CGRectMake(0, 0, 20, 20))
        text.leftView=paddingView;
        text.leftViewMode = UITextFieldViewMode.Always
        return paddingView
        
    }
    
    
    
}