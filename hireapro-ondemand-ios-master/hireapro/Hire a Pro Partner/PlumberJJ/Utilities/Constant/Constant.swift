//
//  Constant.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/13/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
var theme:Theme=Theme()
var appNameJJ:String {
get {
    return Language_handler.VJLocalizedString("app_name", comment: nil)
}
}
var appNameShortJJ :String{
get {
    return Language_handler.VJLocalizedString("app_name", comment: nil)
}
}

var kOk : String{
get {
    return Language_handler.VJLocalizedString("ok", comment: nil)
}
}

var locationDisableTitle : String{
get {
    return Language_handler.VJLocalizedString("location_disable_title", comment: nil)
}
}

var locationDisableMessage : String{
get {
    return Language_handler.VJLocalizedString("location_disable_message", comment: nil)
}
}

var googleApiKey = "AIzaSyAY5tzWbWOCsL4I6K2ZocvpAdFbDXnbGeM"

var kPageCount = "10"

var kErrorMsg:String {
get{
    return Language_handler.VJLocalizedString("error_msg", comment: nil)
}
}

var kNetworkFail:String{
get{
    return Language_handler.VJLocalizedString("network_fail", comment: nil)
}
}


var koops:String{
get{
    return Language_handler.VJLocalizedString("oops", comment: nil)
}
}

var noLeads : String{
get{
    return Language_handler.VJLocalizedString("no_leads", comment: nil)
}
}
var kNetworkErrorMsg = "No network connection... Please connect to network and try again"
var kNoNetwork = "NoNetworkNotif"
var kNewLeadsNotif = "NewLeadsNotif"
var kMyLeadsNotif = "MyLeadsNotif"
var kPaymentPaidNotif = "paymentPaidNotification"
var kJobCancelNotif = "JobCancelNotificancion"
var kNewLeadsOpenNotifNotif = "kNewLeadsOpenNotifNotification"
var kChatFromOthers = "ChatFromOthersNotification"
var kTypeStatus = "TypingStatus"
var kPushNotification = "PushNotification"
let kOpenGoogleMapScheme = "comgooglemaps://"


var xmppHostName = "homegenieflorida.com"  //67.219.149.186 //192.168.1.150
var xmppJabberId = "homegenieflorida.com"  //messaging.dectar.com // casp83


//var BaseUrl = "http://handyforall.zoplay.com/mobile/"
//
//var MainUrl = "http://handyforall.zoplay.com/"


var MainUrl  = "https://hireaproondemand.com"
var BaseUrl = "\(MainUrl)/mobile/"
//var MainUrl  = "http://192.168.0.85:3004"
//var BaseUrl = "\(MainUrl)/mobile/"

let Roomname:NSString = "join network"

//http://162.243.206.138:3002/mobile/provider/register
var completionUrl = "\(BaseUrl)provider"
var RegUrl="\(BaseUrl)provider/register"
var LoginUrl="\(BaseUrl)provider/login"
var ForgotpasswdUrl="\(BaseUrl)provider/forgot-password"
var ChangepasswdUrl = "\(BaseUrl)provider/change-password"
var updateProviderLocation="\(BaseUrl)provider/update-provider-geo"
var GetBankingDetails="\(BaseUrl)provider/get-banking-info"
var SaveBankingDetails="\(BaseUrl)provider/save-banking-info"
var getNewLeads="\(BaseUrl)provider/new-job"
var getMissLeads="\(BaseUrl)provider/missed-jobs"
var myJobsUrl="\(BaseUrl)provider/jobs-list"
var sortesList = "\(BaseUrl)provider/recent-list"
var viewProfile="\(BaseUrl)provider/provider-info"
var reviewsUrl="\(BaseUrl)provider/provider-rating"
var EarningStatsUrl="\(BaseUrl)provider/earnings-stats"
var JobsStatsUrl="\(BaseUrl)provider/jobs-stats"
var JobDetailUrl="\(BaseUrl)provider/view-job"
var GetEditInfoUrl="\(BaseUrl)provider/get-edit-info"
var AcceptRideUrl="\(BaseUrl)provider/accept-job"
var RejectJobUrl="\(BaseUrl)provider/reject-job"
var CancelReasonUrl="\(BaseUrl)provider/cancellation-reason"
var CancelJobUrl="\(BaseUrl)provider/cancel-job"
var StartDestinationUrl="\(BaseUrl)provider/start-off"
var ArrivedDestinationUrl="\(BaseUrl)provider/arrived"
var JobStartUrl="\(BaseUrl)provider/start-job"
var JobCompletedUrl="\(BaseUrl)provider/job-completed"
var PaymentCumMoreInfoUrl="\(BaseUrl)provider/job-more-info"
var stepByStepInfoUrl="\(BaseUrl)provider/job-timeline"
var RequestCashUrl="\(BaseUrl)provider/receive-cash"
var CashReceivedUrl="\(BaseUrl)provider/cash-received"
var RequestPaymentUrl="\(BaseUrl)provider/request-payment"
var GetRatingsOptionUrl="\(BaseUrl)get-rattings-options"
var SaveRatingsOptionUrl="\(BaseUrl)submit-rattings"
var UpdateImageUrlUrl="\(BaseUrl)provider/update_image"
var UpdateBioUrl="\(BaseUrl)provider/update_bio"
var UpdateEmailUrl="\(BaseUrl)provider/update_email"
var UpdateUsernameUrl="\(BaseUrl)provider/update_username"
var UpdateRadiusUrl="\(BaseUrl)provider/update_radius"
var UpdateAddressUrl="\(BaseUrl)provider/update_address"
var UpdateMobileUrl="\(BaseUrl)provider/update_mobile"
var UpdateMobileOTPUrl="\(BaseUrl)provider/update_mobile"
var EnableAvailabilty="\(BaseUrl)provider/tasker-availability"
var GettingAvailablty="\(BaseUrl)provider/get-availability"
var Chat_Details="\(BaseUrl)chat/chathistory"
var UserAvailableUrl="\(BaseUrl)app/chat/availablity"
var ChatListUrl="\(BaseUrl)app/getmessage"
var UpdateMode="\(BaseUrl)user/notification_mode"
var Logout_url="\(BaseUrl)app/provider/logout"
var Appinfo_url="\(BaseUrl)app/mobile/appinfo"
var Aboutus_url="\(BaseUrl)app/mobile/aboutus"
var Get_TransactionUrl="\(BaseUrl)app/provider-transaction"
var View_Transaction_Detail="\(BaseUrl)app/providerjob-transaction"
var Get_NotificationsUrl="\(BaseUrl)app/notification"
var Get_ReviewsURl="\(BaseUrl)app/get-reviews"
var get_mainCategory="\(BaseUrl)provider/get-maincategory"
var Get_SubCategory="\(BaseUrl)provider/get-subcategory"
var deleteCategory="\(BaseUrl)tasker/delete-category"
var add_Category="\(BaseUrl)tasker/add-category"
var getSubCategory_Dtl="\(BaseUrl)provider/get-subcategorydetails"
var getEditCategory_Detail="\(BaseUrl)tasker/category-detail"
var update_Category="\(BaseUrl)tasker/update-category"

var GetBankDetails="\(BaseUrl)taskers/get-stripe-data"
var SaveBankDetails="\(BaseUrl)taskers/saveaccountinfo"
var UpdateBankDetails="\(BaseUrl)taskers/update-stripe-data"
var DeleteBankDetails="\(BaseUrl)taskers/delete-stripe-account"
var MakeDefault="\(BaseUrl)taskers/update-stripe-data"
var Chat_support_Details = "\(BaseUrl)support-chat-history"
var Chat_support_history =  "\(BaseUrl)support-messages"
var UpdateWorkingLocation="\(BaseUrl)provider/update_worklocation"
var UpdateWorkingDays = "\(BaseUrl)provider/update-workingdadys"



//http://162.243.206.138:3002/mobile/chat/chathistory


