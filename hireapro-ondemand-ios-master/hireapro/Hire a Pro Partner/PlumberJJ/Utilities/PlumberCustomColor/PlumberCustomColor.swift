//
//  PlumberCustomColor.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
let PlumberThemeColor = UIColor(red: 248/255.0, green: 130/255.0, blue: 4/255.0, alpha: 1)
let PlumberLightThemeColor = UIColor(red: 248/255.0, green: 130/255.0, blue: 4/255.0, alpha: 0.75)

let PlumberBackGroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
let PlumberLightGrayColor = UIColor(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1)
let PlumberGreenColor = UIColor(red: 46/255.0, green: 204/255.0, blue: 113/255.0, alpha: 1)
let PlumberBlueColor = UIColor(red: 40/255.0, green: 203/255.0, blue: 249/255.0, alpha: 1)


let plumberMediumFontStr = "Roboto"
let plumberBoldFontStr = "Roboto-Bold"



let PlumberSmallFont = UIFont.init(name: plumberMediumFontStr, size: 13)
let PlumberMediumFont = UIFont.init(name: plumberMediumFontStr, size: 14)
let PlumberLargeFont = UIFont.init(name: plumberMediumFontStr, size: 15)
let PlumberSmallBoldFont = UIFont.init(name: plumberBoldFontStr, size: 14)
let PlumberMediumBoldFont = UIFont.init(name: plumberBoldFontStr, size: 14)
let PlumberLargeBoldFont = UIFont.init(name: plumberBoldFontStr, size: 15)


class PlumberCustomColor: NSObject {

}
