
//
//  URLhandler.swift
//  Plumbal
//
//  Created by Casperon Tech on 07/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SystemConfiguration



var Dictionay:NSDictionary!=NSDictionary()


class URLhandler: NSObject
{
     func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        //print (isReachable)
       // print (needsConnection)
        return (isReachable && !needsConnection)
        
    }
    
    func makeCall(url: NSString,param:NSDictionary, completionHandler: (responseObject: NSDictionary?,error:NSError?  ) -> ())
    
    
    {
        var user_id:NSString!=""
       
        if(theme.isUserLigin()){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            user_id=objUserRecs.providerId
          
        }
        if isConnectedToNetwork() == true {
           
            Alamofire.request(.POST, "\(url)", parameters: param  as? [String : AnyObject], headers: ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(user_id)"])
                .response { request, response, data, error in
                    do {
                        
                        Dictionay = try NSJSONSerialization.JSONObjectWithData(
                          
                            data!,
                            
                            options: NSJSONReadingOptions.MutableContainers
                            
                            ) as! NSDictionary
                       
                        let status:NSString?=Dictionay.objectForKey("is_dead") as? NSString
                        if(status == nil)
                        {
                            print("the dictionary is \(url)........>>>\(param)>>>>>.......\(Dictionay)")
                            
                             completionHandler(responseObject: Dictionay as NSDictionary?, error: error )
                            
                        }
                        else
                        {
                            print("the dictionary is \(url)........>>>\(param)>>>>>.......\(Dictionay)")
                            
                            let alertView = UNAlertView(title: appNameJJ, message: "\(Language_handler.VJLocalizedString("log_out1", comment: nil))\n \(Language_handler.VJLocalizedString("log_out2", comment: nil))")
                            alertView.addButton(kOk, action: {
                                let dict: [NSObject : AnyObject] = [NSObject : AnyObject]()
                                
                                theme.saveUserDetail(dict)
                                let appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                                appDelegate.setInitailLogOut()
                            })
                            alertView.show()
                            
                        }
                        
                    }
                        
                    catch let error as NSError {
                        print("the dictionary is \(param)....\(url)...\(Dictionay)")
                        Dictionay=nil
                        completionHandler(responseObject: Dictionay as NSDictionary?, error: error )
                        print("A JSON parsing error occurred, here are the details:\n \(error)")
                    }
                    
                   
                    
                    Dictionay=nil

            }
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(kNoNetwork, object: nil)
        }
    }
    
    
    
    func makeGetCall(url: NSString, completionHandler: (responseObject: NSDictionary? ) -> ())
    {
        Alamofire.request(.GET, "\(url)")
            .responseJSON { response in
                
                completionHandler(responseObject: response.result.value as? NSDictionary )
                
        }
    }
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}