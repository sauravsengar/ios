//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit

var iSSocketDisconnected:Bool=Bool()

var iSChatSocketDisconnected:Bool=Bool()

class SocketIOManager: NSObject {
    
    var themes:Theme=Theme()
    var url_handler:URLhandler=URLhandler()
    var notificationmode : NSString = NSString()
  
  
    static let sharedInstance = SocketIOManager()
    
    var nick_name:NSString=NSString()
    var Appdel=UIApplication.sharedApplication().delegate as! AppDelegate
//    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string:"http://handyforall.zoplay.com")!, options: [.Log(true),.Nsp("/notify"), .ForcePolling(true)])
//     
//     
//     var ChatSocket :SocketIOClient = SocketIOClient(socketURL: NSURL(string:"http://handyforall.zoplay.com")!, options: [.Log(true), .Nsp("/chat"), .ForcePolling(true)])
var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string:"\(MainUrl)")!, options: [.Log(true),.Nsp("/notify"), .ForcePolling(true)])
var ChatSocket :SocketIOClient = SocketIOClient(socketURL: NSURL(string:"\(MainUrl)")!, options: [.Log(true), .Nsp("/chat"), .ForcePolling(true)])
    
    
    
    override init() {
        super.init()
    }
    
    
    func establishConnection() {
        socket.connect()
        notificationmode = "socket"
        self.UpdateNotificationMode()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let providerid : NSString = objUserRecs.providerId
        
        socket.on("connect") {data, ack in
            print("..Check Socket Connection.....\(data).........")
            
            iSSocketDisconnected=false;
            self.roomcreation(Roomname as String, nickname: providerid as String)
        }
        
        
        socket.on("network disconnect") {data, ack in
            print("..Check Socket dis Connection.....\(data).........")
            
            iSSocketDisconnected=true;
            
        }
        
        
    }
    
    
    func establishChatConnection() {
        
        ChatSocket.connect()
        
        if themes.isUserLigin()
        {
            
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            
            ChatSocket.on("connect") {data, ack in
                print("..Check Socket Connection.....\(data).........")
                iSChatSocketDisconnected = false
                
                SocketIOManager.sharedInstance.ChatWithNickname(providerid as String)
                
                
            }
            
            ChatSocket.on("disconnect") {data, ack in
                print("..Check Socket dis Connection.....\(data).........")
                iSChatSocketDisconnected=true;
                
                
                
            }
            
            ChatSocket.on("error") {data, ack in
                
                print("the socket erroe .......\(data)")
                
            }
        }
        
        
    }
    
    
    
    func RemoveAllListener()
    {
        notificationmode = "apns"
        self.UpdateNotificationMode()

        //Removing Chat Listeners
        ChatSocket.off("roomcreated")
        ChatSocket.off("updatechat")
        ChatSocket.off("single message status")
        ChatSocket.off("message status")
        ChatSocket.off("start typing")
        ChatSocket.off("stop typing")
        ChatSocket.off("connect")
        ChatSocket.off("disconnect")
        ChatSocket.off("error")
        
        
        //Removing Socket job related Listeners
        
        socket.off("connect")
        socket.off("network disconnect")
        socket.off("network created")
        socket.off("notification")
        socket.off("push notification")
        
        ChatSocket.disconnect()
        
        socket.disconnect()
        
        
    }
    
    
    
    
    func  UpdateNotificationMode(){
        
        if themes.isUserLigin()
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            let Param: Dictionary = ["user":providerid as String,"user_type":"tasker","mode":notificationmode,"type":"ios" ]
            // print(Param)
            url_handler.makeCall(UpdateMode, param: Param) {
                (responseObject, error) -> () in
                
                if(error != nil)
                {
                    
                }
                else
                {
                    if(responseObject != nil && responseObject?.count>0)
                    {
                        let status:NSString=self.themes.CheckNullValue(responseObject?.objectForKey("status"))!
                        if(status == "1")
                        {
                        }
                        else
                        {
                        }
                        
                        
                    }
                }
            }
        }
        
    }
    
    

    
    
    func LeaveRoom(nickname: String)
    {
        
  
        
        if socket.status == .Connected
            
        {
                let param = ["user":nickname];
            
            socket.emit("network disconnect", param)
            
         

        }
        print("iosjjjj \(self.nick_name)")
        print("***************SOCKET DISCONNECTED******************")
    }
    
    
    
    func LeaveChatRoom(nickname: String)
    {
        
        if ChatSocket.status == .Connected
            
        {
            
            let param = ["user":nickname];

            
            ChatSocket.emit("disconnect",param)
        }
        print("iosjjjj \(self.nick_name)")
        print("***************SOCKET DISCONNECTED******************")
    }
    
    
    
    func  reconnection () {
        socket.reconnect()
    }
    
    
    func emitTracking(user:String,tasker:String,task:String,lat:String,long:String,bearing:String,lastdrive:String){
      
        if socket.status == .Connected
        {
        let jsonString:NSDictionary =
["user":user,"tasker":tasker,"task":task,"lat":lat,"lng":long,"bearing":bearing,"lastdriving":lastdrive  ];
        socket.emit("tasker tracking", jsonString)
        }
        else
        {
            socket.connect()
            
            socket.on("connect") {data, ack in
                print("..Check Socket Connection.....\(data).........")
                
                iSSocketDisconnected=false;
                let objUserRecs:UserInfoRecord=theme.GetUserDetails()

                  let providerid : NSString = objUserRecs.providerId
                self.roomcreation(Roomname as String, nickname: providerid as String)
            }

        }
    }
    
    
    
    
    func ChatWithNickname(nickname: String) {
        
        
        if (ChatSocket.status == .Connected)
            
        {
            let param = ["user":nickname];
            
            ChatSocket.emit("create room",param)
            // let dictData:NSMutableDictionary = NSMutableDictionary()
        }
        
        ChatSocket.on("roomcreated") {[weak self] data, ack in
            //  dictData["message"]=data[0]
            print("..TEST DATA.....\(data).........")
            
            self!.listenstartTyping()
            self!.listenstoptTyping()
            self!.listenSingleMessageStatus()
            self!.ListeningMessageStatus()
            self!.getChatMessage { (messageInfo) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    NSLog("The Chat message information=%@", messageInfo)
                })
            }
            
            
            // self?.Appdel.socketChatNotification(dictData);
        }
        
    }
    
    func sendMessage(message: String, withNickname nickname: String, Providerid: String, taskid: String) {
        
        if(ChatSocket.status == .Connected)
        {
        // socket.emit("new message", nickname, message)
        let jsonString:NSDictionary = ["user":nickname,"tasker":Providerid,"message":message,"task":taskid,"from":Providerid]
        ChatSocket.emit("new message", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
    }
    
    
    func sendChatSupportMessage(message: String,userid:String,admin_id:String,from_id:String,topic_title:String,type:String,reference_id:String ) {
        
        if(ChatSocket.status == .Connected)
        {
            
            let jsonString:NSDictionary = ["tasker":userid,"admin":admin_id,"from":from_id,"topic":topic_title,"message":message,"type":type,"reference":reference_id]
            ChatSocket.emit("new support message", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
        
        
    }
    
    func getChatSupportMessage(completionHandler: (messageInfo: [String: AnyObject]) -> Void) {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("supportmessage") {[weak self] data, ack in
            dictData["message"]=data[0]
            
            self?.Appdel.socketSupportNotification(dictData);
            
            
            
        }
        
    }
    

    
    
    func getChatMessage(completionHandler: (messageInfo: [String: AnyObject]) -> Void) {
        
        let dictData:NSMutableDictionary = NSMutableDictionary()
        
        
        ChatSocket.on("updatechat") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..TEST DATA.....\(dictData).........")
            self?.Appdel.socketChatNotification(dictData);
            
        }
    }
    
    
    func roomcreation (RoomName : String,nickname: String)
    {
        if (socket.status == .Connected)
        {
            let param = ["user":nickname];
            
            socket.emit(RoomName, param)
        }
        socket.on("network created") {data, ack in
            print("..new  Socket Connection.....\(data).........")
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : NSString = objUserRecs.providerId
            self.connectToServerWithNickname(providerid as String, completionHandler: { (userList) in
                
            })
            
            
        }
        
    }
    func connectToServerWithNickname(nickname: String ,completionHandler: (userList: [[String: AnyObject]]!) -> Void) {
        
        let dictData:NSMutableDictionary = NSMutableDictionary()
        
        socket.on("notification") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..TEST DATA.....\(dictData).........")
            self!.Appdel.socketNotification(dictData);
            
        }
    }
    
    
    func sendingSinglemessagStatus (taskid : String ,taskerid : String , Userid : String ,usertype:String ,messagearray:NSArray)
        
    {
        
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["task":taskid,"tasker":taskerid,"user":Userid ,"usertype":usertype,"messages": messagearray];
            ChatSocket.emit("single message status", jsonString)
            
        }
        else
        {
            ChatSocket.connect()
        }
        
    }
    
    func listenSingleMessageStatus() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("single message status") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Singlemessage message....\(dictData).........")
            NSNotificationCenter.defaultCenter().postNotificationName("readSinglemessagestatus", object: data[0])
            
            // self?.Appdel.socketStopTypeNotification(data[0] as! NSDictionary)
        }
        
    }
    
    
    
    
    func SendingMessagestatus (typeofapp: String, Userid: String, taskerid : String,taskid : String)
    {
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["task":taskid,"tasker":taskerid,"user":Userid,"type":typeofapp];
            ChatSocket.emit("message status", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
        
        
        
    }
    
    
    func ListeningMessageStatus ()
    {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("message status") {[weak self] data, ack in
            dictData["message"]=data[0]
            print(". message.Status...\(dictData).........")
            NSNotificationCenter.defaultCenter().postNotificationName("readmessagestatus", object: data[0])
            
            //  self?.Appdel.socketTypeNotification(data[0] as! NSDictionary)
        }
        
        
    }
    private func listenForOtherMessages() {
        
        
    }
    
    func sendStartTypingMessage(Userid: String,taskerid: String,taskid : String ) {
        
        
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["to":Userid,"from":taskerid ,"task":taskid,"user":Userid,"tasker":taskerid,"task":taskid,"type":"tasker"]
            NSLog("the json string=%@", jsonString)
            ChatSocket.emit("start typing", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
        
        
    }
    func listenstartTyping() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("start typing") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Start typing message....\(dictData).........")
            self?.Appdel.socketTypeNotification(data[0] as! NSDictionary)
        }
        
    }
    
    
    func sendStopTypingMessage(Userid: String,taskerid: String,taskid : String) {
        
        if(ChatSocket.status == .Connected)
        {
            let jsonString:NSDictionary = ["to":Userid,"from":taskerid,"user":Userid,"tasker":taskerid,"task":taskid,"type":"tasker"]
            NSLog("the json string=%@", jsonString)
            ChatSocket.emit("stop typing", jsonString)
        }
        else
        {
            ChatSocket.connect()
        }
        
    }
    func listenstoptTyping() {
        let dictData:NSMutableDictionary = NSMutableDictionary()
        ChatSocket.on("stop typing") {[weak self] data, ack in
            dictData["message"]=data[0]
            print("..Stop typing message....\(dictData).........")
            self?.Appdel.socketStopTypeNotification(data[0] as! NSDictionary)
        }
        
    }
}
