//
//  Languagehandler.swift
//  Plumbal
//
//  Created by Casperon Tech on 10/12/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import UIKit
let Language_Notification:NSString="VJLanguageDidChage"

class Languagehandler:NSObject {
    var LocalisedString:NSBundle=NSBundle()
    
    
    
    let EnglishGBLanguageShortName:NSString="en-GB"
    let EnglishUSLanguageShortName:NSString="en"
    let FrenchLanguageShortName:NSString="fr"
    let SpanishLanguageShortName:NSString="es"
    let ItalianLanguageShortName:NSString="it"

    let JapaneseLanguageShortName:NSString="ja"

    let KoreanLanguageShortName:NSString="ko"
    let ChineseLanguageShortName:NSString="zh"
    
    let TurkishLanguageShortName:NSString="tr"
    let TamilLanguageShortName : NSString = "ta"
    
    let EnglishGBLanguageLongName:NSString="English(UK)"
    let EnglishUSLanguageLongName:NSString="English(US"
    let FrenchLanguageLongName:NSString="French"
    let SpanishLanguageLongName:NSString="Spanish"
    let ItalianLanguageLongName:NSString="Italian"

    let JapaneseLanguageLongName:NSString="Japenese"
    let KoreanLanguageLongName:NSString="한국어"

    let ChineseLanguageLongName:NSString="中国的"
    let TurkishLanguageLongName:NSString="Turkish"
    
    let TamilLanguageLongName : NSString = "Tamil"

    var  _languagesLong:NSArray!=NSArray()

     var _localizedBundle:NSBundle!=NSBundle()
    func localizedBundle()->NSBundle
    {
        if(_localizedBundle == nil)
        {
        _localizedBundle=NSBundle(path: NSBundle.mainBundle().pathForResource("\(ApplicationLanguage())", ofType: "lproj")!)!
        }
        return _localizedBundle

    }
    
    func ApplicationLanguage()->String
    {
         let languages:NSArray=NSUserDefaults.standardUserDefaults().objectForKey("AppleLanguages") as! NSArray
        return languages.firstObject as! String

    }
    
    
    func setApplicationLanguage(language:NSString)
    {
 
    let oldLanguage: NSString = ApplicationLanguage()
        
        print("\(oldLanguage)....\(ApplicationLanguage())")
//    if (oldLanguage.isEqualToString(language as String) == false)
//    {
    NSUserDefaults.standardUserDefaults().setObject([language], forKey: "AppleLanguages")
    NSUserDefaults.standardUserDefaults().synchronize()
        _localizedBundle=NSBundle(path: NSBundle.mainBundle().pathForResource("\(ApplicationLanguage())", ofType: "lproj")!)!
    NSNotificationCenter.defaultCenter().postNotificationName(Language_Notification as String, object: nil, userInfo: nil)
//    }
    }
    
  func applicationLanguagesLong()->NSArray
  {
    if _languagesLong == nil {
   _languagesLong = [ChineseLanguageLongName, EnglishGBLanguageLongName, EnglishUSLanguageLongName, FrenchLanguageLongName, KoreanLanguageLongName, ItalianLanguageLongName, SpanishLanguageLongName, TurkishLanguageLongName]
    }
    return _languagesLong
    }

    
    func VJLocalizedString(key:String!,comment:String!)->String
{
    
    print("\(localizedBundle())")
    _localizedBundle=NSBundle(path: NSBundle.mainBundle().pathForResource("\(ApplicationLanguage())", ofType: "lproj")!)!

    return _localizedBundle.localizedStringForKey(key, value: "", table: nil)
    }
    
    func shortLanguageToLong(shortLanguage:NSString)->NSString
    {
        
        if(shortLanguage.isEqualToString(EnglishGBLanguageLongName as String))
        {
            return EnglishGBLanguageLongName;

        }
        if(shortLanguage.isEqualToString(EnglishUSLanguageShortName as String))
        {
            return EnglishUSLanguageShortName;
            
        }

        if(shortLanguage.isEqualToString(EnglishGBLanguageShortName as String))
        {
            return EnglishGBLanguageLongName;
            
        }

        if(shortLanguage.isEqualToString(ChineseLanguageShortName as String))
        {
            return ChineseLanguageShortName;
            
        }

        if(shortLanguage.isEqualToString(FrenchLanguageShortName as String))
        {
            return FrenchLanguageShortName;
            
        }

        if(shortLanguage.isEqualToString(KoreanLanguageShortName as String))
        {
            return KoreanLanguageShortName;
            
        }

        if(shortLanguage.isEqualToString(ItalianLanguageShortName as String))
        {
            return ItalianLanguageShortName;
            
        }

        if(shortLanguage.isEqualToString(SpanishLanguageShortName as String))
        {
            return SpanishLanguageShortName;
            
        }
        if(shortLanguage.isEqualToString(TamilLanguageShortName as String))
        {
            return TamilLanguageShortName;
            
        }
            
        if(shortLanguage.isEqualToString(TamilLanguageLongName as String))
        {
            return TamilLanguageLongName;
            
        }
        else
        {
            
            return ""
            
        }

        
    }
    
    

}
